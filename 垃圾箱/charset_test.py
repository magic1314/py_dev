# encoding:utf-8

#
#
# print("--------开始--------")
#
#
# # 德语
# a = 'Gründe für die Invalidität'
#
# # 英语
# a1 = 'Reasons for invalidation'
#
# # 日语
# b = '廃棄の原因‘'
#
# # 中文
# b1 = '作废原因‘'
#
# # 韩语
# b2 = '무효 원인'
#
#
#
# all = a + "|" + a1 + "|" + b + "|" + b1 + "|" + b2
#
#
# print(all)
#
# utf_8 = all.encode('utf-8')
# print(utf_8)
#
#
# print(utf_8.decode('utf-8'))

import public

sql = """
select * from test_magic_charset
"""
result = public.run_sql(sql, dbFile='5.60_Test.txt',needResult=True,executemany=False,parameter_list=None,display=True)
print(result)

sql = """
insert into Test.test_magic_charset_result(id, content)
values(%s, %s)
"""
public.run_sql(sql_txt=sql, dbFile='5.60_Test.txt', needResult=False, executemany=True, parameter_list=result,
               display=True)

print("--- over ----")










