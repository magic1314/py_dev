import itertools

def permute(nums, n):
    # 确保n不大于nums的长度
    if n > len(nums):
        return []
    else:
        # 使用itertools.permutations生成所有排列
        return list(itertools.permutations(nums, n))

def axy(nums,n):
    # 使用星号(*)操作符将每个元组展开为列表
    result = permute(nums, n)
    # print(result)
    return [list(tup) for tup in result]

if __name__ == '__main__':
    # 测试
    nums = [1, 2, 2]
    n = 2
    print(axy(nums,n))