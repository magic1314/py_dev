import public
# 从x个数字中取y个(x>=y)
def cxy(nums, n):
    combinations = []
    backtrack(nums, n, [], combinations)
    return combinations

def backtrack(nums, n, current, combinations):
    if len(current) == n:
        combinations.append(current[:])
        return

    for i in range(len(nums)):
        current.append(nums[i])
        backtrack(nums[i+1:], n, current, combinations)
        current.pop()


if __name__ == '__main__':
    # 原始数字集合
    nums = [1, 2, 3]

    # 要取的数字个数
    n = 2

    # 生成所有组合
    result = cxy(nums, n)
    print(result)

    # 打印所有组合
    for combination in result:
        print(combination)