# encoding:utf-8
import re
import sys
import random
from Combination import cxy
import public
"""

给出任意5个正整数，使用加减乘除和括号，相互计算，有多少种可能？数字间没有前后关系，并且考虑相同数字的情况




"""

# 集合去重
def remove_duplicates(lst):
    # 将每个子列表转换为元组，并放入一个集合中
    unique_set = set(tuple(sublist) for sublist in lst)
    
    # 将唯一的子列表转换回列表形式
    unique_lst = [list(sublist) for sublist in unique_set]
    
    return unique_lst

# 生成所有的组合
def generate_combinations(numbers, display=False):
    if len(numbers) == 1:
        return [numbers]

    combinations = []
    for i, num in enumerate(numbers):
        remaining_nums = numbers[:i] + numbers[i+1:]
        sub_combinations = generate_combinations(remaining_nums)

        for sub_combination in sub_combinations:
            combinations.append([num] + sub_combination)

    return remove_duplicates(combinations)


# 生成表达式
def generate_expressions(numbers):
    if len(numbers) == 1:
        return [str(numbers[0])]

    expressions = []
    for i in range(1, len(numbers)):
        left_nums = numbers[:i]
        right_nums = numbers[i:]

        left_exprs = generate_expressions(left_nums)
        right_exprs = generate_expressions(right_nums)

        for left_expr in left_exprs:
            for right_expr in right_exprs:
                expressions.append(f"({left_expr} + {right_expr})")
                expressions.append(f"({left_expr} - {right_expr})")
                expressions.append(f"({left_expr} * {right_expr})")

                if right_expr != "0":
                    expressions.append(f"({left_expr} / {right_expr})")

    return expressions


# 计算表达式
def calc(numbers, dispalay=False):
    # 生成所有可能的表达式
    expressions = generate_expressions(numbers)

    # 打印所有表达式
    result_set = set()
    for expression in expressions:
        # print(expression)
        try:
            result = eval(expression)
        except ZeroDivisionError as error:
            # print(error, expression)
            pass
        # 正整数，结果为24
        # if result == 24:
        if abs(result-24) <= 0.00001:
            # print(expression)
            # print("结果", result, isinstance(result, int) ,result == 24)
            result_set.add(expression)
    
    return result_set


# 用户输入一串数字
def input_data():
    _numbers = input("请输入N个数字(用逗号或空格分隔)：")
    _number_list = re.sub(r"\s+",",",_numbers).split(',')
    # print(555, _number_list, len(_number_list))
    if len(_number_list) == 1 and _number_list[0] == '':
        _number_list = [7, 8, 9, 10, 1]
    return _number_list


def remove_invalid_parentheses(expression):
    stack = []
    result = ""

    for char in expression:
        if char == "(":
            stack.append(result)
            result = ""
        elif char == ")":
            if stack:
                result = stack.pop() + "(" + result + ")"
        else:
            result += char

    return result

# # 原始表达式
# expression = "((10 * (9 - 1)) - (7 * 8))"

# # 去除无效括号后的表达式
# result = remove_invalid_parentheses(expression)

# print(result)

if __name__ == '__main__':
    # 输入5个正整数
    numbers = input_data()
    generate_combinations = generate_combinations(numbers)
    # 所有的组合：
    # print("所有的组合：{}".format(generate_combinations))

    # 从列表中随机选择5个元素
    # random_selection = random.sample(generate_combinations, 6)
    random_selection = generate_combinations

    # print(random_selection)

    # 遍历

    # 需要的结果个数
    need_cnt = 0
    # 所有的可能性
    i = 0
    for rs in random_selection:
        result = tuple(calc(rs))
        i = i + len(result)
        if result:
            random_result = random.sample(result,1)
            formule = remove_invalid_parentheses(random_result)
            print("公式{}:  {}".format(need_cnt+1, ''.join(formule)))
            need_cnt = need_cnt + 1
        if need_cnt >= 10:
            break

    print(f"共有【{i}】种组合满足24点！")
    # print("共有{}种可能".format(len(expressions))) 共有3584种可能


 
 