# 计算概率
from calc_base import *
from public import write_log
from Combination import cxy
from Arrangement import axy


# 所有的数字
total = list(range(1,11)) * 4

# 随机5个数字的组合 
n5 = cxy(total,5)
# 共有658008种组合
print("共有{}种组合".format(len(n5)))

i = 0
# 遍历所有的组合
for numbers in range(1, len(n5)+1):
    if numbers % 1000 == 0:
        write_log("进度：numbers={},i={},比例：{}%".format(numbers,i,round(i/numbers*100,2)))
    # 一串数字打乱的所有组合
    # print(66666666, n5[numbers])
    gcs = generate_combinations(n5[numbers-1])
    for gc in gcs:
        # 计算所有符合条件的表达式
        result = tuple(calc(gc))
        if len(result):
            i = i + 1
            break

print("最终进度：numbers={},i={},比例：{}%".format(numbers,i,round(i/numbers*100,2)))