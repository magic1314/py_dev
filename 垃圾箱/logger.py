#!/usr/bin/python
# -*- coding: UTF-8 -*-
import logging,logging.handlers

class Logger(object):
    CRITICAL = 50
    FATAL = CRITICAL
    ERROR = 40
    WARNING = 30
    WARN = WARNING
    INFO = 20
    DEBUG = 10
    NOTSET = 0
    def __init__(self, name, logfilename):
        self.__logger = logging.getLogger(name)
        self.__logger.setLevel(logging.ERROR)
        # self.__logger.setLevel(logging.INFO)
        """
        logging.basicConfig(level = "INFO",
        filename = "Scheduler.log",
        filemode="w",
        datefmt = "%Y-%m-%d-%H:%M:%S",
        format="%[asctime]s %[filename]s [Line:%(lineno)d]-%[levelname]s:%(message)s")
        """
        # handler = logging.FileHandler(filename,"a")
        hdlr = logging.handlers.TimedRotatingFileHandler(logfilename, when="D", interval=1, backupCount=7)
        # hdlr.setLevel(logging.INFO)
        hdlr.suffix = "%Y-%m-%d.log"
        datefmt = "%Y-%m-%d-%H:%M:%S"
        fmt = "%(asctime)s %(filename)s (Line:%(lineno)d)-%(levelname)s:%(message)s"
        formatter = logging.Formatter(fmt,datefmt)
        hdlr.setFormatter(formatter)
        self.__logger.addHandler(hdlr)

    def getlogger(self):
        return self.__logger

    def setlevel(self,level:int):
        self.__logger.setLevel(level)


