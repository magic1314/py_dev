# encodint:utf-8
import sys
if sys.platform == 'linux':
    sys.path.append("/opt/module/kettle/etl_job/demo/T_60/python/share_function")
import public
import re
# import dingding

# sql = """
# select
# 	etl_id
# 	,source_table_name
# 	,target_table_name
# 	,etl_sql
# from etl_config_60
# where etl_id in (1617772873,1617772871)
#
# """
# 参考文件：D:\work\28-直发\直发BSD表字段mapping.sql
# 解析源表到BSD层的字段依赖关系
# 需要把etl_id写出来
# etl_id_list = '1617772873,1617772871,1617772872,1617772874,1617772851,1617772852,1617772853,1617772854,1617772855,1617772856,1617772857,1617772858,1617772859,1617772860,1616058494,1616058495,1617772863,1602298836,1617772869,1617772870,1623132092,1617772875,1617772876,1617772877,1617772878,1617772879,1617772880,1617772881,1617772882,1617772883,1617772884,1617772863'
# etl_id_list = '1623840281,1623840282,1623840283,1623840284,1595297277,1623840286,1609383138,1608875981,1619419292,1610535283,1610535287,1602298845,1614161063,1618195575,1608884823,1608884824,1604306658,1604306656,1604306661,1595311147,1623840287,1623840288,1623840289,1623840290,1617772869,1619314091'


# etl_id_list = '1617772873'
# etl_id_list = '1602298836'

key_select = 'select '
key_from = 'from '
# sql关键字
sql_key_word = ['substring','date_format']


### 相关的符号转换
symbol_comma = '\000'

# Left & Right bracket
symbol_lb = '@lb'
symbol_rb = '@rb'

# 空格
symbol_space = '@space'



def get_etl_id():
    # 获得cfg配置表的etl_id
    sql = """
        select etl_id from Test.etl_mapping_cfg
        """

    result = public.run_sql(sql_txt=sql, dbFile='5.60_Test.txt', needResult=True, display=True)
    result_list = [str(i[0]) for i in result]
    etl_id_str = ','.join(result_list)
    return etl_id_str


def read_config(etl_id_str):
    sql = """
    select 
       etl_id
       ,source_jndi
        ,source_database
        ,source_table_name
        -- ,source_column_name
        ,target_database
        ,target_table_name
        -- ,target_column_name
        -- ,column_comment
        ,etl_sql
from etl_config_60
    where etl_id in ({})
    
    """.format(etl_id_str)
    result = public.run_sql(sql,dbFile='etl_config_95.txt', needResult=True, display=False)
    return result



def deal_mapping(etl_id,source_jndi,source_database,source_table_name,target_database,target_table_name,etl_sql,count):
    # 删除备注信息
    etl_sql = re.sub('--.*', '', etl_sql)
    # 去掉换行符之类的
    etl_sql = etl_sql.replace('\r', ' ').replace('\n', ' ').lower()
    # 多个空格转换成单个空格
    etl_sql = re.sub(' +', ' ', etl_sql)

    cnt_select = etl_sql.count(key_select)
    cnt_from = etl_sql.count(key_from)

    for skw in sql_key_word:
        skw = skw + '('
        if skw in etl_sql:
            pos_end_skw = etl_sql.find(skw) + len(skw) - 1
            #  最后一个右括号： Right bracket
            pos_start_rb = etl_sql.rfind(')', pos_end_skw)  + 1

            # print(66666666666, pos_end_skw, pos_start_rb)
            # 函数部分,临时替换掉逗号,括号
            function_part = etl_sql[pos_end_skw : pos_start_rb]
            new_function_part = re.sub(',', symbol_comma, function_part)
            new_function_part = re.sub('\(', symbol_lb, new_function_part)
            new_function_part = re.sub('\)', symbol_rb, new_function_part)
            new_function_part = re.sub(' ', symbol_space, new_function_part)
            # new_function_part = symbol_comma.replace(',', '\000')#.replace('(', symbol_lb).replace(')', symbol_rb)
            # new_function_part = symbol_comma.replace(',', '\000')#.replace('(', symbol_lb).replace(')', symbol_rb)

            public.write_log("函数的字符串替换：【{}】 ->【{}】".format(function_part, new_function_part), tip='MAJOR')

            etl_sql = etl_sql[:pos_end_skw] + new_function_part + etl_sql[pos_start_rb:]

    brackets = etl_sql.count('(')

    # 括号
    if brackets > 0:
        public.write_log("【etl_id={}】etl_sql中有括号，暂不支持！etl_sql为【{}】".format(etl_id, etl_sql), tip='ERROR')

    if cnt_select != 1 or cnt_from != 1:
        public.write_log("【etl_id={}】select和from出现的次数分别为{}次/{}次, 必须都为1才是正常的，请检查!etl_sql为【{}】".format(etl_id,cnt_select, cnt_from, etl_sql), tip='ERROR')

    # 开始找必备的关键字 select 和 from
    pos_end_select = etl_sql.find(key_select) + len(key_select)
    pos_start_from = etl_sql.find(key_from) - 1

    # print(pos_end_select, pos_start_from)

    column_list = etl_sql[pos_end_select:pos_start_from]
    # column_list = column_list.replace(' ','')
    print("column_list:【{0}】".format(column_list))

    column_list_result = column_list.split(',')
    # print(column_list_result)

    mapping_list = list()
    sce = None
    tce = None

    # 从column_list_result:里面解析对应的mapping关系
    for column_name in column_list_result:
        # print(i)
        # 多个空格换成一个空格（前面的SQL删除）
        # column_name = re.sub(' +', ' ', column_name)
        # 如果column里面有空格
        # 去掉前后的空格
        column_name = column_name.strip()
        old_column_name = column_name

        # 把替换的符号替换回来
        # 函数部分,临时替换掉逗号,括号
        column_name = re.sub(symbol_comma,',', column_name)
        column_name = re.sub(symbol_lb,'(', column_name)
        column_name = re.sub(symbol_rb,')', column_name)

        # 除了空格之外的空格字符，恢复
        if old_column_name != column_name:
            public.write_log("逗号/括号字符恢复：【{}】 -> 【{}】".format(old_column_name , column_name), tip='MAJOR')


        # 比如：账户名称 as account_name
        # 比如：account[空格]
        if ' ' in column_name:
            element = column_name.split(' ')
            sce = element[0]
            tce = element[-1]
            if tce == '':
                tce = sce
        # 如果column里面有空格
        # 比如: account
        else:
            sce = column_name
            tce = column_name
        # mapping_list.append((source_table_name, sce, target_table_name, tce))
        source_sce = sce
        sce = re.sub(symbol_space, ' ', sce)
        tce = tce.replace('[','').replace(']','').replace('"','')
        if source_sce != sce:
            public.write_log("空格字符恢复：【{}】->【{}】".format(source_sce, sce), tip='MAJOR')


        # mapping_list.append((etl_id,source_jndi,source_database,sce,target_database,tce))
        insert_column_list = (etl_id, target_database, target_table_name, tce, source_jndi, source_database, source_table_name, sce)
        mapping_list.append(insert_column_list)
        # etl_id, target_database, target_table_name, target_column_name, source_jndi, source_database, source_table_name, source_column_name





        # info = "{}\t{}\t{}\t{}\t{}\n".format(etl_id,target_table_name, tce, source_table_name, sce)
        # public.write_str(info, file_name=write_file_name, write_type='a', encoding='gbk', display=False)



    mapping_tuple = tuple(mapping_list)
    # print('66666666666', mapping_tuple)
    insert_sql = """
    -- insert into Test.etl_mapping_source_to_bsd(etl_id,source_jndi,source_database,source_table_name,target_database,target_table_name)
    insert into Test.etl_mapping_source_to_bsd(etl_id,target_database,target_table_name,target_column_name,source_jndi,source_database,source_table_name,source_column_name)
    values(%s,%s,%s,%s,%s,%s,%s,%s)
    """
    public.run_sql(insert_sql, dbFile='5.60_Test.txt', needResult=False, executemany=True, parameter_list=mapping_tuple, display=False)
    public.write_log("[第{}张表]【etl_id={}】已插入【Test.etl_mapping_source_to_bsd】表中！".format(count, etl_id))




def main():

    etl_id_str = get_etl_id()
    sql_result = read_config(etl_id_str)

    # 删除已有的记录
    delete_sql = """
    delete from Test.etl_mapping_source_to_bsd
    where etl_id in ({})
    """.format(etl_id_str)
    public.run_sql(delete_sql, dbFile='5.60_Test.txt', needResult=False, executemany=False,  display=False)

    i = 1
    for line in sql_result:
        # print("etl_sql:", line)
        (etl_id, source_jndi, source_database, source_table_name, target_database, target_table_name,etl_sql) = line
        deal_mapping(etl_id,source_jndi,source_database,source_table_name,target_database,target_table_name,etl_sql, i)
        i += 1

    public.write_log("********  任务已完成! ********")




if __name__ == '__main__':
    main()
