import pandas as pd
import sys
from public import run_sql
from public import get_parameter

"""
参数1(必选)：excel名称，可以是绝对路径
参数2(可选)：目标表表名 默认是【月度季度汇总表_{当天}】
参数3(无效)：是否需要清空表（默认为不清空）-功能暂时不开发


更新日志：
2024-01-18 创建

"""

args = len(sys.argv)-1
print(f"参数个数：{args}")
print(sys.argv)


if args not in (1,2):
    print("参数必须是1个或2个！")
    sys.exit(1)


# 读取 Excel 文件：【【报科技】0112上半年考核结果汇总-新增.xlsx】
excel_file = sys.argv[1]
xls = pd.ExcelFile(excel_file)
# 获取所有工作表名称
sheet_names = xls.sheet_names
# 从第2行开始读取数据
start_row = 2


# 将数据插入到 MySQL 表中：【月度季度汇总表_20240118】
if args == 2:
    table_name = sys.argv[2]  # MySQL 表名
else:
    table_name = '月度季度汇总表_{0}'.format(get_parameter('YYYYMMDD'))



# 创建目标表
create_sql = """
CREATE TABLE if not exists `{0}` (
  `考核周期` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `人事关系部门` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `工号` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `姓名` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `所属考核组` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `考核权所在部门` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `部门考核权重` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `绩效指标合计得分` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `调整项得分` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `最终得分` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci
""".format(table_name)
run_sql(create_sql,dbFile='localhost_hrjx.txt',needResult=False,executemany=False,parameter_list=None,group_concat_max_len=0,display=True)


# 打印工作表名称
for sheet_name in sheet_names:
    
    df = pd.read_excel(excel_file, sheet_name=sheet_name, skiprows=start_row-1)
    # print(df)

    # 获取记录数
    record_count = len(df)

    # 打印记录数
    print("当前读取到的sheet:{},记录数：{}".format(sheet_name,record_count))

    # 插入记录
    if record_count:
        for row in df.itertuples(index=False):
            values = ', '.join("'" + str(value) + "'" for value in row)
            query = f"INSERT INTO {table_name} VALUES ({values})"
            run_sql(query,dbFile='localhost_hrjx.txt',needResult=False,executemany=False,parameter_list=None,group_concat_max_len=0,display=False)


sql = 'select count(1) cnt from {}'.format(table_name)
cnt = run_sql(sql,dbFile='localhost_hrjx.txt',needResult='oneValue',executemany=False,parameter_list=None,group_concat_max_len=0,display=False)
print("当前{0}表记录数为：{1}".format(table_name,cnt))

print("数据导入完成！")