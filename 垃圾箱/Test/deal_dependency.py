#!/usr/bin/python
# -*- coding: UTF-8 -*-
#################################
# 名称：Dump_Data_MySQL.py
# 作者：石大峰
# 版本：v1.0
# 创建时间：20200331
# 功能简述：
#######################################################################################
# 20200415石大峰更新: tmp.find(":")改为tmp.find(":DATA_DT")
# 20200421石大峰更新: 判断表名是否输入的处理写错了, if table is None: -> if table:
# 20200426石大峰更新：DATA_PUMP_SQL_CONF中IMP_EXP_FLAG = 'IMP' -> 'EXP'
#                     sqllist中s参数为list， 改为s[0]
# 20200702: 导出数据时，先删除文件
# 20200706: 从Dump_Data_MySQL.py中分离为Dump_Data_MySQL_frs.py
#           dburl.txt 切换成 dburl_frsdb.txt; 目前主要是同步frs库中rpt_query_conf表数据
# 20200720: 预处理语句实际在获取导出文件语句后面，位置存在问题, 已调整
########################################################################################
from to_linux.properties import Properties
import sys
import pymysql
from to_linux.logger import Logger
import os
import datetime

# get current work directory
cwd = os.path.split(os.path.realpath(__file__))[0]
fileName = os.path.split(os.path.realpath(__file__))[1]


print("5555555555555555555555555555")

pos = fileName.rfind(".", 0)
if pos > 0:
    fileName = fileName[0:pos]
fileName = cwd + "/log/" + fileName + ".log"
log = Logger(__name__, fileName)
log.setlevel(log.INFO)
logger = log.getlogger()

FIELD_SEPERATOR = "\001"
LINE_TERMINATOR = "\n"

def getdbconnect(dbprop: dict):
    try:
        # db = connect()
        # db.connect(**dbconfig)
        # conn = pymysql.connect(host=host,user=user,password=password,database=db,charset="utf8mb4")
        db = pymysql.connect(
           host=dbprop.get("host"),
           user=dbprop.get("user"),
           password=dbprop.get("password"),
           database=dbprop.get("database"),
           charset=dbprop.get("charset"),
           autocommit=bool(dbprop.get("autocommit", True)),
           port=int(dbprop.get("port"))
        )

    except Exception as err:
        print(err)
        logger.error(err)
        sys.exit(1)
    return db

def get_last_day_for_last_month(pdate):
    if isinstance(pdate, datetime.date):
        adate = pdate
    if isinstance(pdate, str):
        if pdate.find("-") > 0 or pdate.find("/") > 0:
            adate = datetime.date.fromisoformat(pdate.replace("/", "-"))
        else:
            adate = datetime.datetime.strptime(pdate, "%Y%m%d").date()
    else:
        return ""

    next_month = adate - datetime.timedelta(days=adate.day)
    return next_month


if __name__=="__main__":

    try:
        prop = Properties(cwd + "/dburl_test.txt")
        dbconfig = prop.getpropeties()
    except Exception as err:
        print(err)
        logger.error(err)
        sys.exit(1)
    

    try:
        db = getdbconnect(dbconfig)
    except Exception as err:
        print(err)
        logger.error(err)
        sys.exit(1)
    print("connect to db successfully!")

    cur = db.cursor()

    sql = """select source_table, dependency_tables from sdf_table_dependencies"""
    cur.execute(sql)
    all_result = cur.fetchall()
    cur.close()
    
    
    #print("all_result:", all_result)
    
    # 获得返回结果
    new_result = list()
    for line in all_result:
        source_table = line[0]
        dependency_tables = line[1]
        #print("source_table,dependency_tables:", source_table, dependency_tables)
        dependency_tables_list = line[1].split(',')
        #print(dependency_tables_list)
        for i in dependency_tables_list:
            new_result.append([source_table, i])
            print(source_table, i)
    #print("new_result:", new_result)
    
    
    
    #sql = """insert into sdf_table_dependencies_result(source_table, dependency_tables) values(%s,%s)"""
    #
    #
    #cur = db.cursor()
    #cur.execute(sql, new_result)
    #db.commit()
    cur.close()
    
    
    
    


    db.close()

    sys.exit(0)













