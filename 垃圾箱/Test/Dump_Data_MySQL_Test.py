#!/usr/bin/python
# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：Dump_Data_MySQL.py
# 作者：石大峰
# 版本：v1.0
# 创建时间：20201229
# 参数：para1: 数据日期(必须)
#       para2: 表名(可带库名，非必须，默认为全量导出)
# 功能简述：从mysql库导出表的数据
#######################################################################################
# 更新记录
# 20201229: 完善脚本
# 
#######################################################################################
from to_linux.properties import Properties
import sys
import pymysql
from to_linux.logger import Logger
import gzip
import time
import os
import datetime
from concurrent.futures import ProcessPoolExecutor

# 参数设置
url_file_name = "dburl_test.txt"

# get current work directory
cwd = os.path.split(os.path.realpath(__file__))[0]
fileName = os.path.split(os.path.realpath(__file__))[1]

pos = fileName.rfind(".", 0)
if pos > 0:
    fileName = fileName[0:pos]
fileName = cwd + "/log/" + fileName + ".log"
log = Logger(__name__, fileName)
log.setlevel(log.INFO)
logger = log.getlogger()

FIELD_SEPERATOR = "\001"
LINE_TERMINATOR = "\n"


def getdbconnect(dbprop: dict):
    try:
        db = pymysql.connect(
            host=dbprop.get("host"),
            user=dbprop.get("user"),
            password=dbprop.get("password"),
            database=dbprop.get("database"),
            charset=dbprop.get("charset"),
            autocommit=bool(dbprop.get("autocommit", True)),
            port=int(dbprop.get("port"))
        )

    except Exception as err:
        print(err)
        logger.error(err)
        sys.exit(1)
    return db


def dumptable(dbprop: dict, tb: str, data_dt: str):
    try:
        try:
            # db = connect()
            # db.connect(**dbprop)
            db = getdbconnect(dbprop)
        except Exception as e:
            print(e)
            logger.error(e)
            return
        sql = """SELECT TABLE_NAME,WHERE_CLAUSE,FILE_PATH
                FROM DATA_PUMP_CONF  
                WHERE TABLE_NAME = %s AND IMP_EXP_FLAG = 'EXP' AND ENABLE_STATUS = '1' ORDER BY ORDER_NO """

        cur = db.cursor()
        cur.execute(sql, (tb,))
        tables = cur.fetchall()
        if cur.rowcount < 1:
            db.close()
            return

        r = tables[0]
        tablename = r[0]
        whereclause = r[1]
        filepath = r[2]

        # 预处理
        if preprocess(db, tablename, data_dt) < 0:
            cur.close()
            db.close()
            return

        cur = db.cursor()
        selectsql = "SELECT * FROM " + tablename
        if not whereclause is None and len(whereclause) > 5:
            conditionsql = whereclause
            selectsql = selectsql + " WHERE " + conditionsql

        tmp = selectsql.upper().replace("\r", " ").replace("\n", " ") + " "
        selectsql = selectsql.replace(":DATA_DT", "%s")
        args = []
        pos = tmp.find(":DATA_DT")
        while pos > 0:
            args.append(data_dt)
            pos = tmp.find(":DATA_DT", pos + 1)

        # print("selectsql:" + selectsql)
        if filepath is None:
            filepath = "."
        else:
            filepath = filepath.strip()

        if not os.path.exists(filepath):
            os.mkdir(filepath)

        msg = f"{tablename} start dump"
        print(msg)
        # logger.info(msg)

        starttime = time.strftime("%Y%m%d%H%M%S", time.localtime(time.time()))
        filename = data_dt + tablename + ".gz"

        if len(args) > 0:
            logger.info("args:" + repr(args))
            logger.info("\n" + selectsql + "\n")
            cur.execute(selectsql, args)
        else:
            logger.info("\n" + selectsql + "\n")
            cur.execute(selectsql)
        col_properties = cur.description
        # colcnt = len(col_properties)

        col_line = ""
        for col_prop in col_properties:
            colname = col_prop[0]
            """colType = col_prop[1]
            typeName = "None"
            if colType in [252, 253, 254]:
                typeName = "CHAR"
            elif colType in [3,4,246]:
                typeName = "NUMBER"
            elif colType == 7:
                typeName = "TIMESTAMP"
            elif colType == 10:
                typeName = "DATE"
            elif colType == 11:
                typeName = "TIME"
            elif colType == 12:
                typeName = "DATETIME"
            print(typeName)"""
            col_line = col_line + colname + FIELD_SEPERATOR
        col_line = col_line.strip(FIELD_SEPERATOR)

        dats = cur.fetchall()
        rowcnt = cur.rowcount

        if rowcnt < 1:
            msg = f"{tablename} no data fetched"
            logger.info(msg)
            return

        filename = filepath.rstrip(os.path.sep) + os.path.sep + filename

        if os.path.isfile(filename):
            os.remove(filename)

        with open(filename, "wb") as f:
            fgz = gzip.GzipFile(mode="wb", fileobj=f)
            fgz.write((col_line + LINE_TERMINATOR).encode())
            i = 0
            for row in dats:
                col_index = 0
                txt = ""
                # print(row)
                for col_prop in col_properties:
                    coltype = col_prop[1]
                    data = row[col_index]
                    col_index = col_index + 1
                    # 字符型要处理回车换行符
                    if coltype in [252, 253, 254]:
                        if data is None:
                            data = ""
                        else:
                            data = data.replace("\r", " ").replace(LINE_TERMINATOR, " ")

                    # 数值型直接取数 转字符串
                    if coltype in [1, 2, 3, 4, 246]:
                        if data is None:
                            data = ""
                        else:
                            data = str(data)

                    if coltype == 7:
                        if data is None:
                            data = ""
                        else:
                            data = data.strftime("%Y%m%d%H%M%S")

                    if coltype == 12:
                        if data is None:
                            data = ""
                        else:
                            data = data.strftime("%Y%m%d%H%M%S")

                    if coltype == 10:
                        # 驱动有问题，已修改
                        if data is None:
                            data = ""
                        else:
                            data = data.strftime("%Y%m%d")

                    if coltype == 11:
                        if data is None:
                            data = ""
                        else:
                            # 驱动有问题，已修改
                            # 返回timedelta
                            h, r = divmod(data.seconds, 3600)
                            m, s = divmod(r, 60)
                            # GBase没有毫秒和微秒
                            # fs = data.microseconds / 1e6
                            data = "{0:0>2}{1:0>2}{2:0>2}".format(h, m, s)
                            # data = data.strftime("%H%M%S")

                    txt = txt + data + FIELD_SEPERATOR

                txt = txt[0:-1] + LINE_TERMINATOR
                fgz.write(txt.encode())

                i = i + 1
                if i >= 1000:
                    fgz.flush()
                    i = 0
            fgz.flush()
            fgz.close()

        ok_filename = filename + ".ok"
        msg = f"{tablename} {rowcnt} lines exported, ok filename: {ok_filename}"
        print(msg)
        logger.info(msg)
        filesize = os.path.getsize(filename)

        if os.path.isfile(ok_filename):
            os.remove(ok_filename)
        os.rename(filename, ok_filename)

        endtime = time.strftime("%Y%m%d%H%M%S", time.localtime(time.time()))
        sql = """INSERT INTO DATA_PUMP_LOG(TABLE_NAME,FILE_NAME,FILE_SIZE,ROW_COUNT,IMP_EXP_FLAG,START_TIME,END_TIME) 
                VALUES(%s,%s,%s,%s,%s,%s,%s)"""
        v = (tablename, filename, filesize, rowcnt, 'EXP', starttime, endtime)
        cur.execute(sql, v)
        db.commit()

        # 后处理
        if postprocess(db, tablename, data_dt) < 0:
            cur.close()
            db.close()
            return

        cur.close()
        db.close()
        msg = f"{tablename} dump over"
        print(msg)
        logger.info(msg)
        return
    except Exception as e2:
        print(e2)
        logger.error(e2)
        raise


def preprocess(db, tablename: str, data_dt: str):
    try:
        sql = """SELECT SQL_TXT FROM DATA_PUMP_SQL_CONF 
        WHERE TABLE_NAME = %s AND ENABLE_STATUS = '1' 
        AND IMP_EXP_FLAG = 'EXP' AND TRIGGER_TIME = 'BEFORE' ORDER BY EXEC_ORDER"""
        cur = db.cursor()
        cur.execute(sql, (tablename,))
        sqllist = cur.fetchall()
        for s in sqllist:
            if not s[0]:
                continue
            tmp = s[0].upper().replace("\r", " ").replace("\n", " ") + " "
            sql = s[0].replace(":DATA_DT", "%s")
            args = []
            pos = tmp.find(":DATA_DT")
            while pos > 0:
                args.append(data_dt)
                pos = tmp.find(":DATA_DT", pos + 1)
            logger.info("args:" + repr(args))
            logger.info("\n" + sql + "\n")
            if len(args) > 0:
                cur.execute(sql, args)
            else:
                cur.execute(sql)
    except Exception as err:
        print(err)
        logger.error(err)
        return -1
    return 1


def postprocess(db, tablename: str, data_dt: str):
    try:
        sql = """SELECT SQL_TXT FROM DATA_PUMP_SQL_CONF 
        WHERE TABLE_NAME = %s AND ENABLE_STATUS = '1' 
        AND IMP_EXP_FLAG = 'EXP' AND TRIGGER_TIME = 'AFTER' ORDER BY EXEC_ORDER"""
        cur = db.cursor()
        cur.execute(sql, (tablename,))
        sqllist = cur.fetchall()
        for s in sqllist:
            if not s[0]:
                continue
            tmp = s[0].upper().replace("\r", " ").replace("\n", " ") + " "
            sql = s[0].replace(":DATA_DT", "%s")
            args = []
            pos = tmp.find(":DATA_DT")
            while pos > 0:
                args.append(data_dt)
                pos = tmp.find(":DATA_DT", pos + 1)
            logger.info("args:" + repr(args))
            logger.info("\n" + sql + "\n")
            if len(args) > 0:
                cur.execute(sql, args)
            else:
                cur.execute(sql)
    except Exception as err:
        print(err)
        logger.error(err)
        return -1
    return 1


def get_last_day_for_last_month(pdate):
    if isinstance(pdate, datetime.date):
        adate = pdate
    if isinstance(pdate, str):
        if pdate.find("-") > 0 or pdate.find("/") > 0:
            adate = datetime.date.fromisoformat(pdate.replace("/", "-"))
        else:
            adate = datetime.datetime.strptime(pdate, "%Y%m%d").date()
    else:
        return ""

    next_month = adate - datetime.timedelta(days=adate.day)
    return next_month


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Missing data date parameters")
        logger.error("Missing data date Parameters")
        sys.exit(1)
    else:
        dt = sys.argv[1]
        try:
            print(datetime.datetime.strptime(dt, "%Y%m%d"))
        except Exception as err:
            print("input a correct date!")
            logger.error("date format error!")
            sys.exit(1)

    table = None
    if len(sys.argv) > 2:
        table = sys.argv[2].strip()

    try:
        prop = Properties(cwd + os.path.sep + url_file_name)
        dbconfig = prop.getpropeties()
    except Exception as err:
        print(err)
        logger.error(err)
        sys.exit(1)

    try:
        db = getdbconnect(dbconfig)
    except Exception as err:
        print(err)
        logger.error(err)
        sys.exit(1)
    print("connect to db successfully!")

    cur = db.cursor()

    if table:
        sql = """SELECT TABLE_NAME,WHERE_CLAUSE,FILE_PATH
            FROM DATA_PUMP_CONF  
            WHERE IMP_EXP_FLAG = 'EXP' AND ENABLE_STATUS = '1' AND TABLE_NAME = %s  ORDER BY ORDER_NO """
        cur.execute(sql, (table,))
    else:
        sql = """SELECT TABLE_NAME,WHERE_CLAUSE,FILE_PATH
            FROM DATA_PUMP_CONF  
            WHERE IMP_EXP_FLAG = 'EXP' AND ENABLE_STATUS = '1'  ORDER BY ORDER_NO """
        cur.execute(sql)
    tables = cur.fetchall()

    if cur.rowcount < 1:
        sys.exit(0)
    max_workers = 1
    if len(tables) < max_workers:
        max_workers = len(tables)
    starttime = datetime.datetime.now()
    msg = "data dump start"
    print(datetime.datetime.strftime(starttime, "%Y-%m-%d %H:%M:%S ") + msg)
    logger.info(msg)
    # dt = get_last_day_for_last_month(dt).strftime("%Y%m%d")
    with ProcessPoolExecutor(max_workers) as do:
        for tab in tables:
            tablename = tab[0]
            do.submit(dumptable, dbconfig, tablename, dt)

        do.shutdown(wait=True)

    endtime = datetime.datetime.now()
    msg = "All dump over"
    print(datetime.datetime.strftime(endtime, "%Y-%m-%d %H:%M:%S ") + "All dump over")
    logger.info(msg)

    cur.close()
    db.close()

    sys.exit(0)
