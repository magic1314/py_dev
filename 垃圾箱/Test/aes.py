from Crypto.Cipher import AES
import base64

class prpcrypt(object):
    def __init__(self,key='1234123412341112'):
        # 除了MODE_SIV模式key长度为：32, 48, or 64,
        # 其余key长度为16, 24 or 32#
        self.key = key.encode('utf-8')
        self.mode = AES.MODE_CBC

    # cryptographic functions
    def encrypt(self,text):
        cryptor = AES.new(self.key,self.mode,self.key)
        length = 16
        count = len(text)
        add = length - (count % length)
        text = text + ('\0'*add)
        self.ciphertext = cryptor.encrypt(text.encode('utf-8'))
        # 将二进制(ascii,基础编码)转换成base64编码(最常见的二进制编码方法)
        result = base64.b64encode(self.ciphertext)
        # print("测试：", result, self.ciphertext)
        print("测试：", type(result), type(self.ciphertext))
        # print("加密4:", result, type(result))
        return result


    def decrypt(self,text):
        cryptor = AES.new(self.key, self.mode, self.key)
        plain_text = cryptor.decrypt(base64.b64decode(text))
        # print(plain_text)
        # print(plain_text.rstrip('\0'))
        return plain_text.decode('utf-8').rstrip('\0')

a = prpcrypt()
print(len("hello world!"))
encrypt_text = a.encrypt("hello world!")
print(encrypt_text)

print("encrypt_text:", encrypt_text)

source_text = a.decrypt(encrypt_text)

print("source_text:", source_text)