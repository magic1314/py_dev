# -*- coding:UTF-8 -*-
import sqlparse

sql = """
/*
* @Author: Administrator
* @Date:   2021-03-21 21:25:23
* @Last Modified by:   Administrator
* @Last Modified time: 2021-03-22 15:30:56
-- 更新记录 --
20210408：刷新10天的数据 改为 刷新10天的数据
*/


-- 1.海外仓利润中心
-- etl_id=1610680020
 

 insert into SHWL_ODS.dwd_profit_center  
 (
 fid,fdate,pay_date,ods_product_id,account_id,platform_id,site_id,ods_site_id,division_id,fwarehouse_id,order_no,order_state,orderlineitemid,ebaytransactionid,parcel_id,qty,order_amt,order_post_fee,order_pp_fee,order_pack_fee,order_weight,order_price,order_cost,order_pay_date,order_ebay_fee,time_zone,pack_type,pack_class,post_way,theory_post_fee,ex_rate,currency,cur_ex_rate,sku_weight,sku_cost_rmb,sku_lose_rate,sku_amt,sku_amt_no_post,sku_post_fee,sku_at_post_fee,sku_pack_fee,sku_pp_fee,sku_ebay_fee,is_new,sku_th_post_fee,order_th_post_fee,itemid,listing,incomplete_code,purchase_cost,move_cost,current_profit,bad_rate,handle_fee_out,is_no_stock,multi_pack_fee,finan_cost,handle_fee_in,cal_date,cal_time,imp_date,update_time,etl_create_time -- FROM SHWL_ODS.fact_profit_center_info WHERE fid=55275931
)
select 
fid,
dt.date_int as fdate, -- 发货日期,
pay_date, -- 付款日期,
pr.ods_product_id,-- 数仓产品ID,
act.account_id, -- 账户,
tpm.platform_id, -- 平台,
st.site_id,
st.ods_site_id, -- 站点_简称,
dvm.division_id,-- 事业部ID,
wh.fwarehouseid, -- 目的仓仓库ID,
order_no,
order_state,
orderlineitemid,
ebaytransactionid,
parcel_id,
qty,
base.sku_amt*rt.ftormb_rate*base.qty,
base.order_post_fee*rt.ftormb_rate,
sku_pp_fee*qty*rt.ftormb_rate,
base.order_pack_fee*rt.ftormb_rate,
order_weight,
order_price*rt.ftormb_rate,
sku_cost_rmb*qty,
order_pay_date,
sku_ebay_fee*qty*rt.ftormb_rate,
time_zone,
pack_type,
pack_class,
post_way,
base.theory_post_fee*rt.ftormb_rate,
rt.ftormb_rate as ex_rate,
currency,
cur_ex_rate,
sku_weight,
sku_cost_rmb,
sku_lose_rate,
base.sku_amt*rt.ftormb_rate,
base.sku_amt_no_post*rt.ftormb_rate,
base.sku_post_fee*rt.ftormb_rate,
base.sku_at_post_fee*rt.ftormb_rate,
base.sku_pack_fee*rt.ftormb_rate,
base.sku_pp_fee*rt.ftormb_rate,
base.sku_ebay_fee*rt.ftormb_rate,
is_new,
base.sku_th_post_fee*rt.ftormb_rate,
base.order_th_post_fee*rt.ftormb_rate,
itemid,
listing,
incomplete_code,
base.purchase_cost*rt.ftormb_rate,
base.move_cost*rt.ftormb_rate,
base.current_profit*rt.ftormb_rate,
bad_rate,
handle_fee_out*rt.ftormb_rate,
is_no_stock,
multi_pack_fee*rt.ftormb_rate,
base.financial_cost*rt.ftormb_rate,
base.handle_fee_in*rt.ftormb_rate,
cal_date,
cal_time,
imp_date,
base.update_time, 
now() as etl_create_time
 from  SHWL_TEMP.tmp_bsd_profit_center base 
 join SHWL_ODS.dim_date dt 
		on base.fdate=dt.date
 left join SHWL_ODS.dim_account act  -- 关联取标准账户ID【account_id】
		on base.account_name=act.account_name 
 left join SHWL_ODS.dim_account_platform_mapping tpm  -- 关联取标准平台ID【platform_id】
		on act.account_id=tpm.account_id 
	left join SHWL_ODS.dim_site st     -- 关联取标准站点ID【site_id】
		on base.reg_country=st.site_name -- 后续修改的
	left join SHWL_ODS.dim_division_mapping_chain dvm  -- 关联取事业部标准ID【division_id】
		on (act.account_id=dvm.account_id
		and tpm.platform_id=dvm.platform_id
		and st.site_id=dvm.site_id and dt.date between start_date and end_date) 
	left join SHWL_ODS.dim_product pr 
		on base.sys_sku=pr.sys_sku
	left join SHWL_ODS.dim_warehouse wh 
		on base.fwh_old_code=wh.mapping_warehouse_id and mapping_use_range='220' 
	join SHWL_ODS.dim_exchange_rate rt 
		on  (base.fdate between rt.start_date and rt.end_date) and rt.fcurrency_short_name='GBP'
where 
			base.order_state not  in ('作废','订单取消-作废','订单-作废')
		  and cal_time<>'1900-01-01'
			and dt.date>=date_add(current_date(),interval -10 day) -- 日常数据，刷新近10天的数据
			;

"""
# 1.分割SQL
stmts = sqlparse.split(sql)
print(11111, stmts)

print("sql个数:", len(stmts))

for stmt in stmts:
    # 2.format格式化
    print(sqlparse.format(stmt, reindent=True, keyword_case="lower"))
    # 3.解析SQL
    stmt_parsed = sqlparse.parse(stmt)
    # print("解析结果:", stmt_parsed[0].tokens)
    result = stmt_parsed[0].tokens
    print("解析结果:", result)
    print(type(result), len(result))


    for i in range(len(result)):
        print(i, ":", result[i])
    # print("解析结果:", stmt_parsed, len(stmt_parsed))