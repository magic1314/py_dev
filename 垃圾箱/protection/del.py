# encoding: utf-8
import os

with open('photo.txt', 'r', encoding='utf-8') as file:
    file_paths = file.readlines()

for path in file_paths:
    path = path.strip()  # 去除行末尾的空格和换行符
    # print(path)
    if os.path.exists(path):
        os.remove(path)
        # print("已删除")
    else:
        print("跳过")