# encoding:utf-8
# 在Windows操作系统中，可以使用以下代码将文件夹彻底隐藏，让别人无法找到它的位置：
import win32api
import win32con
import win32file
import pywintypes
import sys

# 要隐藏的文件夹路径
folder_path = r'D:\python脚本\protection\测试'

# 获取文件夹句柄
handle = win32file.CreateFile(
    folder_path,
    win32file.GENERIC_READ | win32file.GENERIC_WRITE,
    win32file.FILE_SHARE_READ | win32file.FILE_SHARE_WRITE,
    None,
    win32file.OPEN_EXISTING,
    win32file.FILE_FLAG_BACKUP_SEMANTICS,
    None
)

# 设置文件夹属性为隐藏
win32api.SetFileAttributes(folder_path, win32con.FILE_ATTRIBUTE_HIDDEN)

print('隐藏文件')
# sys.exit(0)



# create_time = pywintypes.Time(win32api.GetSystemTime())
# last_access_time = pywintypes.Time(win32api.GetSystemTime())
# last_write_time = pywintypes.Time(win32api.GetSystemTime())


create_time = None
last_access_time = None
last_write_time = None



file_info = {
    win32file.FileBasicInfo: {
        "CreationTime": create_time,
        "LastAccessTime": last_access_time,
        "LastWriteTime": last_write_time,
    }
}

# 设置文件夹标记为“隐藏”
win32file.SetFileInformationByHandle(
    handle,   # 文件句柄
    win32file.FileBasicInfo,  # 设置文件的基本信息，如创建时间、修改时间、访问时间、文件属性等
    # win32file.PACK_LARGE_INTEGER(0),
    # win32file.FileBasicInfoFlags.FILE_ATTRIBUTE_HIDDEN
    file_info
)


# 关闭文件夹句柄
win32file.CloseHandle(handle)


print('完成')