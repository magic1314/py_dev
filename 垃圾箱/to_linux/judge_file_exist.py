# encoding:utf-8
"""
    -------------------------- 脚本说明 --------------------------
    @ author: magic
    @ create date: 2021-04-27
    @ purpose: 判断指定文件夹下面，特定文件是否存在
    @ 部署路径： /opt/module/kettle/etl_job/demo/T_60/python/check_file
    ***********************   更新记录   ***********************
    @2021-05-12: 改为从钉钉群告警
"""
import os
import sys
if sys.platform == 'linux':
    sys.path.append("/opt/module/kettle/etl_job/demo/T_60/python/share_function")
import public
import dingding


# 检查的文件夹
target_calalog = [r'/opt/module/kettle/data-integration/opt/module/kettle/etl_job/demo/T_60/error_file_60'
    ,r'/opt/module/kettle/data-integration/opt/module/kettle/etl_job/demo/T_252/error_file'
    ,r'/opt/module/kettle/data-integration/opt/module/kettle/etl_job/demo/T_252/error_file_252'
    ,r'D:\download\2021-05-12']

py_file_name = os.path.realpath(__file__)

# 文件的类型，只支持一种
file_type='.txt'

abs_file_name_list = list()
abs_file_name = ''


result_dic = dict()

# 轮循文件
for catalog_name in target_calalog:
    if os.path.isdir(catalog_name):
        public.write_log("正在访问目录:" + catalog_name)
        abs_file_name_list = list()
        for file_name in os.listdir(catalog_name):
            abs_file_name = catalog_name + os.path.sep + file_name
            if os.path.isfile(abs_file_name) and file_name.find(file_type) > 0:
                # print(file_name)
                abs_file_name_list.append(file_name)
        result_dic[catalog_name] = abs_file_name_list


# print('结果:', abs_file_name_list)
# len_abs_file_name_list =  len(abs_file_name_list)
#
# print("能匹配的文件个数为:%s" % len_abs_file_name_list)


message = str()
total_cnt = 0
for key in result_dic.keys():
    value = result_dic[key]
    cnt = len(value)
    if cnt:
        filename_list = '、'.join(value)
        message += "目录【{0}】里面有{1}个txt文件,文件名称为: {2}\n".format(key, cnt, filename_list)
        total_cnt += cnt
message = message.rstrip('\n')


# 如果目录下存在txt文件，则告警
if total_cnt:
    # 输出告警
    warn_content = """--------------- 异常文件检查 ---------------
【告警对象】etl_azkaban.bi.oigbuy.com服务器(etl linux系统)
【脚本名称】{0}
【异常问题】{1}
【解决方案】找到异常文件的etl_Id，查看具体同步任务""".format(py_file_name, message)

    dingding.dingtalk_warn_text(message=warn_content, webhook_id='T1', warn_object=None, at_all=False)
else:
    public.write_log("目录检查正常!")

# print(total_cnt, message)

# 如果能匹配到文件返回错误
# if len_abs_file_name_list > 0:
#     sys.exit(8)
    # dingding.dingtalk_warn_text(message="")
















