# encoding: utf-8
"""
    -------------------------- 脚本说明 --------------------------
    @ name: 异常中心的告警-利润表
    @ author: song saifeng
    @ create date: 2021-04-27
    @ usage: 直接调度
    @ purpose: 方便调用统一的方法
    @ used_script: warn_center.py
    ***********************   更新记录   ***********************
    @2021-04-28: 完善脚本的主体内容
    @2021-05-11: 调整dingding告警的调用
"""
import time
import datetime
import dingding
import public


# 设置检查的键
check_key = 'order_line_itemid,event_id,sku'

start_time = datetime.datetime.now()
sql = """
select table_name 
from information_schema.TABLES 
where TABLE_NAME like 'oms_profit_unit_fee_3_'
"""
table_name = public.run_sql(sql, dbFile='227_oms_center.txt', needResult=True, display=False)

# 获得配置信息
db = None
cur = None
try:
    dbconfig = public.config_file(dbFile='227_oms_center.txt')
    db = public.getdbconnect(dbconfig)
    cur = db.cursor()
except Exception as err:
    # cur = db.cur()
    public.write_log(err, tip='ERROR')


 # 循环
count = 0
for item in table_name:
    count += 1
    tbn = item[0]
    print("正在检索表: %s" % tbn, end='')
    sql = """
    select count(1) cnt
    from (
        select 1
        from oms_center.{0}
        group by {1}
        having count(1) > 1
    ) t
    """.format(tbn, check_key)
    cur.execute(sql)
    data = cur.fetchone()
    cnt = data[0]
    # print(tbn)
    if cnt > 0:

        chk_sql = """select * 
from oms_center.{1}
where ({0}) in
(
	select {0}
	from oms_center.{1}
	group by {0}
	having count(1) > 1
)
order by {0}
""".format(check_key, tbn)


        # 输出告警
        warn_content = """----- 异常中心告警 ----- 
【告警对象】库名：227库，域名：mysqlslave1.database.oigbuy.com，表名：【{0}】, 
【异常问题】表中存在重复的记录条数:{1} 
【检查逻辑】event_id,order_line_itemid,sku 4个字段值需要唯一
【检查脚本】{2}
""".format(tbn, cnt, chk_sql)

        public.write_log(warn_content, tip='WARN')
        # dingding.insert_dingding(warn_content, client_id=58, display=True)
        dingding.dingtalk_warn_text(message=warn_content, webhook_id='T1')
    else:
        print(",检查结果:无重复订单！")
    # 设置等待时间
    time.sleep(0.8)

end_time = datetime.datetime.now()

# 关闭数据库连接
cur.close()
# global db
db.close()

use_time = (end_time - start_time).total_seconds()

public.write_log("【统计】程序共花费%s秒!" % round(use_time,1))





