# -*- coding=utf-8 -*-
"""
    @ author: magic
    @ create date: 2021-02-01
    @ usage: python check_and_warn.py chk_id=1621493501
    @ purpose: 检查数据数据的完成情况
    @ used tables: 60库: etl_config_60, etl_record_60
    @ deposit path: /opt/module/kettle/etl_job/demo/T_60/python/check_and_warn/check_and_warn.py
    ***********************   更新记录   ***********************
    @2021-05-20: 完成脚本初版
"""
import os
import sys
if sys.platform == 'linux':
    sys.path.append("/opt/module/kettle/etl_job/demo/T_60/python/share_function")
import public
import dingding

# 脚本名称
py_file_name = os.path.realpath(__file__)


# 获得传入的参数
def get_parameter():
    argv = sys.argv
    chk_id = None
    if len(argv) <= 1:
        print("未传入参数，错误")
        sys.exit(2)

    # 目前只需要chk_Id这一个参数

    for i in argv:
        if '=' in i:
            # print(i)
            key = i.split('=')[0]
            value = i.split('=')[1]
            # print(key, value)
            if key == 'chk_id':
                #global chk_Id
                chk_id = value

    if not chk_id:
        print("chk_id参数不存在")
        sys.exit(3)

    return chk_id


def get_config_db(chk_id):
    sql = """
    select 
        chk_id,chk_jndi,chk_rule_name,chk_sql, robot_id from check_config2
    where is_in_use = 'Y'
    and chk_id = {}
    """.format(chk_id)
    result = public.run_sql(sql_txt=sql, dbFile='etl_config.txt', needResult=True,parameter_list=None, display=True)
    # print(result)
    return result



def execute_chk_sql(sql_result):
    # chk_result = None
    # chk_result_code = None
    for r in sql_result:
        chk_id,chk_jndi, chk_rule_name, chk_sql, robot_id = r
        dbFile = chk_jndi + '.txt'
        result = public.run_sql(sql_txt=chk_sql, dbFile=dbFile, needResult=True, parameter_list=None, display=True)
        print(result)

        if not result:
            public.write_log("记录为空!!", tip='ERROR')
        else:
            chk_result = result[0][-2]
            chk_result_code = result[0][-1]
            if chk_result_code != '1':
                public.write_log("正常！")
                continue

            warn_content = """----- 异常告警 ----- 
【检查名称】{0}
【告警脚本】{1}
【检查配置】select a.chk_sql, a.* from check_config2 a where chk_id = {2};
【检查库名】{3}
【异常内容】{4} """.format(chk_rule_name, py_file_name, chk_id, chk_jndi, chk_result)

            dingding.dingtalk_warn_text(message=warn_content, webhook_id='T1', warn_object=None, at_all=False)









def main():
    chk_id = get_parameter()

    config_records = get_config_db(chk_id)


    check_result = execute_chk_sql(config_records)






if __name__ == '__main__':
    main()












