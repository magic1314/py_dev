# encoding:utf-8
import time
import os
import sys

import win32gui
import win32con
import win32clipboard
import win32api

# 参考网页
# https://blog.csdn.net/woho778899/article/details/89249824?utm_medium=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromMachineLearnPai2-1.control&dist_request_id=&depth_1-utm_source=distribute.pc_relevant_t0.none-task-blog-BlogCommendFromMachineLearnPai2-1.control


cwd = os.path.split(os.path.realpath(__file__))[0]


class SendWeixinMsg():
    def __init__(self, window_title, wait_send_seconds=5):
        self.window_title = window_title
        self.wait_send_seconds = wait_send_seconds
        # self.msg=msg

    # 发现窗口
    def find_window(self):
        window_name = win32gui.FindWindow(None, self.window_title)
        if window_name == 0:
            print('未找到【{0}】窗口！'.format(self.window_title))
            sys.exit(88)
        else:
            return window_name


    # 等待
    def sleep(self):
        print("需要等待{0}秒".format(self.wait_send_seconds))
        time.sleep(self.wait_send_seconds)


    # 窗口的相关操作
    # 操作类型参考：https://blog.csdn.net/gotosola/article/details/7421438
    def windows_operation(self, window_name, type=None):
        if type == 'show':
            print("显示窗口!")
            # win32gui.ShowWindow(window_name, win32con.SW_SHOWMINIMIZED)
            win32gui.ShowWindow(window_name, win32con.SW_SHOWNORMAL)
            # win32gui.ShowWindow(window_name, win32con.SW_SHOW)
            win32gui.SetWindowPos(window_name, win32con.HWND_TOPMOST, 1000, 500, 500, 500,
                                  win32con.SWP_SHOWWINDOW)  # 第二个参数是置顶，前两个数字是位置，后两个数字是大小，最后一个是显示
            win32gui.SetForegroundWindow(window_name)  # 获取控制

        elif type == 'hide':
            print("隐藏窗口!")
            win32gui.ShowWindow(window_name, win32con.SW_HIDE)
        elif type == 'minimize':
            print('最小化窗口!')
            win32gui.ShowWindow(window_name, win32con.SW_MINIMIZE)
        else:
            print("窗口没有操作!")


    # 复制到粘贴板
    def copy_content(self, clipboard_info):#把要发送的消息复制到剪贴板
        win32clipboard.OpenClipboard()
        win32clipboard.EmptyClipboard()
        win32clipboard.SetClipboardData(win32con.CF_UNICODETEXT, clipboard_info)
        win32clipboard.CloseClipboard()


    # 粘贴(ctrl+v)
    def paste_key(self):
        win32api.keybd_event(17, 0, 0, 0)  # ctrl键位码是17
        win32api.keybd_event(86, 0, 0, 0)  # v键位码是86
        win32api.keybd_event(86, 0, win32con.KEYEVENTF_KEYUP, 0)  # 释放按键
        win32api.keybd_event(17, 0, win32con.KEYEVENTF_KEYUP, 0)

    # 发送(Alt+s)
    def enter_key(self):
        win32api.keybd_event(18, 0, 0, 0)  # Alt键位码
        win32api.keybd_event(83, 0, 0, 0)  # s键位码
        win32api.keybd_event(18, 0, win32con.KEYEVENTF_KEYUP, 0)  # 释放按键
        win32api.keybd_event(83, 0, win32con.KEYEVENTF_KEYUP, 0)

    # 发送内容
    def sendmsg(self, msg):#给好友发送消息

        # 找到名字为friendName的窗口(不是模糊匹配)
        weixinWin = self.find_window()

        if weixinWin == 0:
            print('未找到窗体【{0}】！'.format(self.window_title))
            return False

        # 显示窗口
        self.windows_operation(weixinWin, type='show')

        # 把要发送的消息复制到剪贴板
        self.copy_content(msg)

        # 粘贴到窗口上
        self.paste_key()

        # 等待N秒钟
        self.sleep()

        # 发送内容
        self.enter_key()

        # 将窗口最小化
        self.windows_operation(weixinWin, type='minimize')

        # 提示信息
        print("消息发送成功!")
        print("内容为:【{0}】".format(msg))


# 天气情况
def weather_info():
    now_date = time.strftime('%Y-%m-%d', time.localtime())
    file_weather_info = cwd + os.path.sep + now_date + os.path.sep + 'weather.txt'
    if os.path.isfile(file_weather_info):
        with open(file_weather_info, encoding='utf-8') as file:
            lines = file.readlines()
            all_content = ''.join(lines)
            # print('all_content:', all_content)

            return all_content
            # for line in lines:
            #     print(line)
    else:
        print("文件【{0}】不存在".format(file_weather_info))


if __name__ == '__main__':
    weixin = '文件传输助手'
    msg = weather_info()
    message = SendWeixinMsg(weixin, wait_send_seconds=12)
    message.sendmsg(msg)



    print('-------------------- over --------------------------')
    
    

