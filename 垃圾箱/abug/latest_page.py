import requests
from bs4 import BeautifulSoup
 
# 目标网页URL
url = 'https://wuqianmx.top/'

# 定义保存响应内容的文件名
filename = 'latest_page.html'

# 发送HTTP请求
response = requests.get(url)
 
# 确保网页请求成功
if response.status_code == 200:
    # 使用BeautifulSoup解析网页内容
    soup = BeautifulSoup(response.text, 'html.parser')

    formatted_html = soup.prettify()

    
    # 提取需要的信息，例如所有段落文本
    # paragraphs = soup.find_all('p')
    # for p in paragraphs:
    #     print(p.get_text())
    # print(response.text)

    # 都将响应内容保存到文件
    with open(filename, 'w', encoding='utf-8') as file:
        file.write(formatted_html)

else:
    print(f"请求失败，状态码: {response.status_code}")