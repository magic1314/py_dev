# -*- coding: UTF-8 -*-
############################################################################################################
# 名称：special_task.py
# 作者：magic
# 版本：v1.0
# 创建时间：2022-03-09
# 功能简述：执行特殊任务
# 配置表：etl_py_special_task_config
# 执行命令：python special_task.txt task_id=xxx(任务编号)
# 指令：
# 应用平台：window10、linux
# 注：谨慎操作；导入表数据之前，需要备份原有表的数据
############################################################################################################
# 更新记录
# 2022-03-09: 创建代码；使得展示更加直观
# 2022-03-10: 进一步优化输出的内容
#                        新增预处理、后处理、输出的显示类型(complex:显示一条的数据，reduced:只显示最近一条)
#                        新增
# 2022-07-27: 添加is_need_equal字段，默认是否等号，不默认只修改参数名称，否则会在参数前默认添加一个等于号
# 2022-08-13: is_need_equal字段bug修复
############################################################################################################
import sys
import datetime
import re
import time
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import public
# import dingding


# 分隔符
FIELD_SEPERATOR = "\001"

# 获得平台名称
def get_platform_name():
    # 判断windows和linux系统
    if sys.platform == 'win32':
        platform_name = 'windows'
    elif sys.platform == 'linux':
        platform_name = 'linux'
    else:
        platform_name = None
        public.write_log('当前【%s】系统不支持！！' % sys.platform)

    return platform_name



# 获得传入的参数
def get_parameter(least_num=1):
    len_para = len(sys.argv) - 1

    if len_para < least_num:
        public.write_log("传入的参数个数小于least_num，当前传入%s个" % len_para, tip='ERROR')
    public.write_log("传入参数：【{}】".format(sys.argv))


    para_dic = dict()
    # 目前只需要etl_id这一个参数
    key = None
    value = None
    for i in sys.argv:
        if '=' in i:
            # print(i)
            key = i.split('=')[0]
            value = i.split('=')[1]
            para_dic[key] = value
    if not para_dic:
        public.write_log("没有获得参数", tip='ERROR')
    return para_dic


def get_value(dic_name, key):
    value = None
    try:
        value = dic_name[key]
    except KeyError:
        pass
    return value


def find_parameter():
    paras = get_parameter()
    # print(paras)

    # 如果获取不到字段的变量，通过
    task_id = get_value(paras, 'task_id')


    if not task_id:
        public.write_log("task_id不能为空!", tip='ERROR')


    public.write_log("获取的参数值：task_id=【{}】".format(task_id))

    return task_id


def man_show(all_seconds):
    # print("秒数:", all_seconds)

    all_seconds = int(all_seconds)
    hours = all_seconds // 3600
    minus = all_seconds % 3600 // 60
    seconds = all_seconds % 60

    show_hour = ''
    show_minus = ''
    show_senconds = ''
    # print("%s小时%s分钟%s秒" % (hours, minus, seconds))
    if hours:
        show_hour = "{}小时".format(hours)
    if minus:
        show_minus = "{}分钟".format(minus)
    if seconds:
        show_senconds = "{}秒".format(seconds)
    else:
        show_senconds = "0秒"

    show_all_time = "%s%s%s" % (show_hour, show_minus, show_senconds)
    # print(show_all_time)
    return show_all_time


class Check():
    def __init__(self, task_id):
        # self.task_id = find_parameter()
        self.task_id = task_id
        self.df = None
        self.loop_type = None

        self.display = True
        # print(self.task_id)

    def set_display(self, display=True):
        self.display = display

    # 获得配置库的数据
    def get_conf_data(self):
        sql = """
select 
	a.task_id
	,a.jndi_name
	,a.table_name
	,a.loop_type
	,a.start_date
	,a.end_date
	,a.custom_loop_list
	,ifnull(b.etl_sql,a.etl_sql) etl_sql
	,a.before_sql
	,a.after_sql
	,a.output_mode
	,a.param_list
	,a.is_need_equal
from etl_py_special_task_config a
left join etl_config_60 b
	on a.etl_id = b.etl_id
where a.task_id = %s
and a.is_valid=1
""" % self.task_id

        config_data = public.run_sql(sql_txt=sql, dbFile='etl_config.txt', needResult=True, executemany=False,
                            parameter_list=None, display=True)

        if not config_data:
            public.write_log("【etl_py_special_task_config】表中未发现有task_id=【{}】的有效记录，请检查！".format(self.task_id), tip='ERROR')

        # 获得结果集的每个值
        (self.task_id,self.jndi_name,self.table_name,self.loop_type,self.start_date,self.end_date,self.custom_loop_list,self.etl_sql
         ,self.before_sql,self.after_sql,self.output_mode,self.param_list,self.is_need_equal) = config_data[0]


        # sys.exit(0)
        public.write_log("配置表中loop_type={}".format(self.loop_type))
        # print(loop_type)

    # 执行sql
    def execute_sql(self, sql, display=False):
        # 格式化sql语句，这里是去掉注释的内容; 为了防止sql备注中有分号，会导致程序报错
        # s = public.SQL(sql)
        # s.remove_comment()
        # sql = s.get_result()
        # 如果sql的内容不多，就输出内容
        if sql:
            public.run_sql(sql_txt=sql,dbFile=self.jndi_name,needResult=False,executemany=False,parameter_list=None,display=display)


    # 启动所有的情况
    def run_all(self):
        # 预处理
        self.execute_sql(self.before_sql, display=False)

        if self.loop_type in ('date'):
            # sys.exit(0)
            self.date_type(self.loop_type)
        elif self.loop_type == 'customize':
            public.write_log("暂时不支持自定义！", tip="ERROR")
            # sys.exit(1)
            self.customize(self.loop_type)
        else:
            public.write_log("关键字【%s】暂不支持！" % self.loop_type, tip='WARN')

        # 后处理
        self.execute_sql(self.after_sql, display=False)

    # 支持null，date，暂不支持range
    def date_type(self, loop_type):
        # if loop_type not in self.loop_type:
        #     public.write_log("没有找到【%s】关键字的数据，跳过" % loop_type, display=self.display)
        #     return None
        public.write_log("开始处理【%s】关键字的数据~" % loop_type, display=self.display)
        self.program_start_time = datetime.datetime.now()


        # start_date = df['start_date'].values[0].strftime('%Y-%m-%d')
        start_date = self.start_date
        end_date = self.end_date
        jndi_name = self.jndi_name
        etl_sql = self.etl_sql
        today = datetime.date.today()


        # print(888888888, today)
        # 结束日期不能大于当前日期
        if end_date > today:
            end_date = today

        if start_date > end_date:
            public.write_log("开始日期{0}，不能大于结束日期{1}".format(start_date, end_date), tip='ERROR')

        # 总的任务数
        all_task_cnt = (end_date - start_date).days + 1
        # # 剩余任务数
        remain_task_cnt = all_task_cnt
        # 当前任务完成数
        fininsh_task_cnt = 0

        public.write_log("任务总数:【%s】个，日期范围【%s~%s】" % (all_task_cnt, start_date, end_date))

        if self.table_name:
            public.write_log("结果表是：%s" % self.table_name)

        if self.param_list:
            all_param_list = self.param_list.replace(',',';').split(';')
        else:
            all_param_list = ['?',]
        # print(all_param_list)
        # print(888888, type(all_param_list), all_param_list)

        # 开始循环
        while start_date <= end_date:
            # 日期上面默认加单引号，sql中加或不加都行
            start_date_str = "= '" + start_date.strftime('%Y-%m-%d') + "'"
            start_date_str2 = " '" + start_date.strftime('%Y-%m-%d') + "'"
            show_start_date_str = start_date.strftime('%Y-%m-%d')
            etl_sql_ok = str()

            for p in range(len(all_param_list)):
                para_ori = all_param_list[p]
                para_temp = "'" + para_ori + "'"
                # print(4444444444, p)
                
                # 不默认加=号
                if not self.is_need_equal:
                    # 第一次出现时
                    if p == 0:
                        # public.write_log("6666666666666666 不加默认=号")
                        etl_sql_ok = etl_sql.replace(para_ori, start_date_str2).replace(para_temp, start_date_str2)
                        # print(etl_sql_ok)
                        # print(para_ori,start_date_str2,para_temp)
                    else:
                       etl_sql_ok = etl_sql_ok.replace(para_ori, start_date_str2).replace(para_temp, start_date_str2)

                # 给默认=号
                else:
                    if p == 0:
                        etl_sql_ok = etl_sql.replace(para_ori, start_date_str).replace(para_temp, start_date_str)
                        # 删除多余的=号
                        etl_sql_ok = re.sub('=\s*=', '=', etl_sql_ok)
                    else:
                        etl_sql_ok = etl_sql_ok.replace(para_ori, start_date_str).replace(para_temp, start_date_str)
                        # 删除多余的=号
                        etl_sql_ok = re.sub('=\s*=','=',etl_sql_ok)
                # print(5555555, para_temp, para, etl_sql)

            # print("格式化后的sql:", etl_sql_ok)
            rask_start_time = datetime.datetime.now()
            # public.run_sql(sql_txt=sql_txt,dbFile=jndi_name,needResult=False,executemany=False,parameter_list=None,display=False)
            self.execute_sql(etl_sql_ok, display=False)
            # time.sleep(2)
            rask_end_time = datetime.datetime.now()

            rask_used_time = (rask_end_time - rask_start_time).seconds

            # print(666666666, rask_end_time, rask_start_time, rask_used_time)
            all_used_time =  (rask_end_time - self.program_start_time).seconds

            show_rask_used_time = man_show(rask_used_time)
            show_all_used_time = man_show(all_used_time)



            fininsh_task_cnt += 1
            # 平均速度
            avg_speed = all_used_time / fininsh_task_cnt

            # 剩余任务数
            remain_task_cnt = all_task_cnt - fininsh_task_cnt

            # 预计完成时间
            predict_finish_used_time = avg_speed * remain_task_cnt
            finish_delta = datetime.timedelta(seconds=predict_finish_used_time)
            predict_finish_time = datetime.datetime.now() + finish_delta

            show_predict_finish_used_time = man_show(predict_finish_used_time)

            # 完成率
            fininsh_rate = round(fininsh_task_cnt / all_task_cnt * 100,2)

            # print("平均时间:【{}】,剩余任务数:【{}】,预计完成时间:【{}】".format(avg_speed, remain_task_cnt, predict_finish_used_time))
            # print(finish_delta,datetime.datetime.now() , predict_finish_time)

            # 同一天
            if predict_finish_time.strftime('%Y-%m-%d') == public.get_parameter('now_date2'):
                predict_finish_time_str = predict_finish_time.strftime('%H:%M:%S')
            else:
                predict_finish_time_str = predict_finish_time.strftime('%Y-%m-%d %H:%M:%S')


            content = "执行日期:【{0}】;本次耗时【{1}】,累计耗时【{2}】;完成进度【{3}%】,预计【{4}】后完成, 预计完成时间:【{5}】".format(show_start_date_str, show_rask_used_time,show_all_used_time, fininsh_rate,show_predict_finish_used_time,predict_finish_time_str)
            if self.output_mode == 'reduced':
                public.show_progress_bar(str_content=content, time_sleep=0)
            else:
                public.write_log(content)


            delta = datetime.timedelta(days=1)
            start_date += delta

        if self.output_mode == 'reduced':
            public.write_log("")
        public.write_log("日期遍历完成！")



    # 自定义的，一次只查询一个分组函数的值
    def customize(self, loop_type):
        df = self.df
        df_null = df[df['loop_type'] == loop_type]
        df_null = df_null.copy()
        df_customize = df_null.loc[:,['jndi_name', 'table_name', 'group_function', 'statistics_column', 'where_condition']]
        df_customize = df_customize.copy()

        # print(9999999999, df_customize, type(df_customize))


        # 用法参考：https://www.jb51.net/article/172623.htm
        for row in df_customize.itertuples():
            # print(row)
            jndi_name = row[1]
            table_name = row[2]
            group_function = row[3]
            statistics_column = row[4]
            where_condition = row[5]

            # 字段列表：loop_type,task_id, jndi_name, table_name, where_condition, group_function, statistics_column
            paras = (loop_type, self.task_id, jndi_name, table_name, where_condition, group_function, statistics_column)

            # insert_log_table(paras=paras, column_list=None)



    def _duplication(self):
        pass

if __name__ == '__main__':
    task_id = find_parameter()
    check = Check(task_id)
    check.get_conf_data()
    check.run_all()












