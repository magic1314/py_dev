# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：compare.py
# 作者：magic
# 版本：v1.0
# 创建时间：20210829
# 功能简述：比较记录数
# 配置表：etl_py_statistics_t2_config
# 执行命令：python compare.py check_num=xxx(检查编号)
# eg： check_num=full_new_stock_year_stock2
# 应用平台：window10、linux
# 注：谨慎操作；导入表数据之前，需要备份原有表的数据
#######################################################################################
# 更新记录
# 20210829: 完善脚本
########################################################################################
import sys
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import public
import dingding
from statistics_base import  insert_log_table

# 获得平台名称
def get_platform_name():
    # 判断windows和linux系统
    if sys.platform == 'win32':
        platform_name = 'windows'
    elif sys.platform == 'linux':
        platform_name = 'linux'
    else:
        platform_name = None
        public.write_log('当前【%s】系统不支持！！' % sys.platform)

    return platform_name



# 获得传入的参数
def get_parameter(least_num=1):
    len_para = len(sys.argv) - 1

    if len_para < least_num:
        public.write_log("传入的参数个数小于least_num，当前传入%s个" % len_para, tip='ERROR')
    public.write_log("传入参数：【{}】".format(sys.argv))


    para_dic = dict()
    # 目前只需要etl_id这一个参数
    key = None
    value = None
    for i in sys.argv:
        if '=' in i:
            # print(i)
            key = i.split('=')[0]
            value = i.split('=')[1]
            para_dic[key] = value
    if not para_dic:
        public.write_log("没有获得参数", tip='ERROR')
    return para_dic


def get_value(dic_name, key):
    value = None
    try:
        value = dic_name[key]
    except KeyError:
        pass
    # print(666, value)
    return value


def find_parameter():
    paras = get_parameter()
    # print(paras)

    # 如果获取不到字段的变量，通过
    check_num = get_value(paras, 'check_num')


    if not check_num:
        public.write_log("check_num不能为空!", tip='ERROR')


    public.write_log("获取的参数值：check_num=【{}】".format(check_num))

    return check_num




# 开始统计
def run(check_num):
    sql = """
select
    source_jndi_name,source_table_name,source_where_condition,source_group_function,source_statistics_column
    ,target_jndi_name,target_table_name,target_where_condition,target_group_function,target_statistics_column
from etl_py_statistics_t2_config
where check_num='%s' and is_valid=1
""" % check_num

    result = public.run_sql(sql_txt=sql,dbFile='etl_config.txt',needResult=True,executemany=False,parameter_list=None,display=False)

    if not result:
        public.write_log("etl_py_statistics_t2_config表未匹配到check_num=%s的数据，请检查！" % check_num, tip='ERROR')

    (source_jndi_name,source_table_name,source_where_condition,source_group_function,source_statistics_column
    ,target_jndi_name,target_table_name,target_where_condition,target_group_function,target_statistics_column) = result[0]

    source_sql_item = ('compare',check_num, source_jndi_name, source_table_name, source_where_condition, source_group_function,
     source_statistics_column)

    target_sql_item = ('compare',check_num, target_jndi_name, target_table_name, target_where_condition, target_group_function,
                       target_statistics_column)

    # 开始统计，并插入到log表中
    insert_log_table(paras=source_sql_item)
    insert_log_table(paras=target_sql_item)



def main():
    check_num = find_parameter()
    run(check_num)


if __name__ == '__main__':
    main()











