# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：magic.py
# 作者：magic
# 版本：v1.0
# 创建时间：20210829
# 功能简述：统计单表
# 指令：
# 应用平台：window10、linux
#######################################################################################
# 更新记录
# 2021--08-29: 完善脚本
# 2021--09-17: null和date添加where_condition条件;增加ifnull(xx,0)函数
# 2021--09-24：支持range类型的检查
# 2021--09-25：变量不能使用range关键字
# 2021--10-12：共享的公共函数脚本位置变动--
# 2022-03-14： 有重复任务执行的bug
########################################################################################
import sys
import pandas as pd
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import public
# import dingding
# from calc_index import  insert_log_table


# # 分隔符
# FIELD_SEPERATOR = "\001"
#
# # 获得平台名称
# def get_platform_name():
#     # 判断windows和linux系统
#     if sys.platform == 'win32':
#         platform_name = 'windows'
#     elif sys.platform == 'linux':
#         platform_name = 'linux'
#     else:
#         platform_name = None
#         public.write_log('当前【%s】系统不支持！！' % sys.platform, tip='ERROR')
#
#     return platform_name
#
#
#
# # 获得传入的参数
# def get_parameter(least_num=1):
#     len_para = len(sys.argv) - 1
#
#     if len_para < least_num:
#         public.write_log("传入的参数个数小于least_num，当前传入%s个" % len_para, tip='ERROR')
#     public.write_log("传入参数：【{}】".format(sys.argv))
#
#
#     para_dic = dict()
#     # 目前只需要etl_id这一个参数
#     key = None
#     value = None
#     for i in sys.argv:
#         if '=' in i:
#             # print(i)
#             key = i.split('=')[0]
#             value = i.split('=')[1]
#             para_dic[key] = value
#     if not para_dic:
#         public.write_log("没有获得参数", tip='ERROR')
#     return para_dic
#
#
# def get_value(dic_name, key):
#     value = None
#     try:
#         value = dic_name[key]
#     except KeyError:
#         pass
#     return value
#
#
# def find_parameter():
#     paras = get_parameter()
#     # print(paras)
#
#     # 如果获取不到字段的变量，通过
#     check_num = get_value(paras, 'check_num')
#     if not check_num:
#         public.write_log("check_num不能为空!", tip='ERROR')
#
#
#     public.write_log("获取的参数值：check_num=【{}】".format(check_num))
#
#     return check_num

class ShortCut():
    def __init__(self):
        # self.check_num = find_parameter()
        print('################## 通过表名，匹配到insert into语句 ##################')


    def _input(self):
        try_times = 3
        while try_times:
            result_falg = self.header_mod()
            # 如果错误记录下来
            if not result_falg:
                try_times -= 1


    def header_mod(self):
        input_str = input("请输入表名：")
        header_sql = """
select concat('truncate table ',table_schema,'.',table_name,char(59),char(13),'insert into ',table_schema, '.', table_name, '(', column_list, ') ') sql_txt
from (
	select table_schema, table_name, group_concat(lower(column_name) order by ordinal_position SEPARATOR ',' ) column_list 
	from information_schema.columns
	where table_name = '{}'   -- 修改这里的表名
	-- and table_schema = 'Test'
	and column_name not in ('etl_create_time','etl_update_time')
	group by table_schema, table_name	
) t
;
        """.format(input_str)
        header_data = public.run_sql(sql_txt=header_sql, dbFile='5.60_SHWL_ODS.txt', needResult=True, executemany=False,
                                     parameter_list=None,group_concat_max_len=10240, display=True)


        # print(header_data)
        if not header_data:
            public.write_log("没有找到表，请查看！", tip='WARN')
            return False

        # print(header_data)
        # print(len(header_data[0]))
        # print(header_data[0])
        output_str = header_data[0][0].replace('\r','\r\n')
        print("-----------------------------------------------------------"*3)
        print(output_str)
        print()
        print("-----------------------------------------------------------"*3)
        return True



if __name__ == '__main__':
    short_cut = ShortCut()
    short_cut._input()















