# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：singe.py
# 作者：magic
# 版本：v1.0
# 创建时间：20210829
# 功能简述：统计单表
# 配置表：etl_py_statistics_t1_config
# 执行命令：python singe.txt check_num=xxx(检查编号)
# 指令：
# 应用平台：window10、linux
#######################################################################################
# 更新记录
# 2021-08-29: 完善脚本
# 2021-09-17: null和date添加where_condition条件;增加ifnull(xx,0)函数
# 2021-09-24：支持range类型的检查
# 2021-09-25：变量不能使用range关键字
# 2021-10-12：共享的公共函数脚本位置变动
# 2022-03-14： 有重复任务执行的bug
# 2022-06-30: 处理null的方法中，插入多条时，新增异常处理
########################################################################################
import sys
import pandas as pd
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import public
# import dingding
from statistics_base import  insert_log_table


# 分隔符
FIELD_SEPERATOR = "\001"

# 获得平台名称
def get_platform_name():
    # 判断windows和linux系统
    if sys.platform == 'win32':
        platform_name = 'windows'
    elif sys.platform == 'linux':
        platform_name = 'linux'
    else:
        platform_name = None
        public.write_log('当前【%s】系统不支持！！' % sys.platform)

    return platform_name



# 获得传入的参数
def get_parameter(least_num=1):
    len_para = len(sys.argv) - 1

    if len_para < least_num:
        public.write_log("传入的参数个数小于least_num，当前传入%s个" % len_para, tip='ERROR')
    public.write_log("传入参数：【{}】".format(sys.argv))


    para_dic = dict()
    # 目前只需要etl_id这一个参数
    key = None
    value = None
    for i in sys.argv:
        if '=' in i:
            # print(i)
            key = i.split('=')[0]
            value = i.split('=')[1]
            para_dic[key] = value
    if not para_dic:
        public.write_log("没有获得参数", tip='ERROR')
    return para_dic


def get_value(dic_name, key):
    value = None
    try:
        value = dic_name[key]
    except KeyError:
        pass
    return value


def find_parameter():
    paras = get_parameter()
    # print(paras)

    # 如果获取不到字段的变量，通过
    check_num = get_value(paras, 'check_num')


    if not check_num:
        public.write_log("check_num不能为空!", tip='ERROR')


    public.write_log("获取的参数值：check_num=【{}】".format(check_num))

    return check_num




class Check():
    def __init__(self, check_num):
        # self.check_num = find_parameter()
        self.check_num = check_num
        self.df = None
        self.check_type_list = None

        self.display = True
        # print(self.check_num)

    def set_display(self, display=True):
        self.display = display

    # 获得配置库的数据
    def get_conf_data(self):
        sql = """
select jndi_name,table_name,statistics_column,group_function,check_type,where_condition,value_range,check_logic
from etl_py_statistics_t1_config
where check_num='%s' 
and is_valid=1
-- for testing
-- and table_name='odm_profit2_gross_abc'
order by id asc
""" % self.check_num

        config_data = public.run_sql(sql_txt=sql, dbFile='etl_config.txt', needResult=True, executemany=False,
                            parameter_list=None, display=True)

        if not config_data:
            public.write_log("【etl_py_statistics_t1_config】表中未发现有check_num=【{}】的有效记录，请检查！".format(self.check_num), tip='ERROR')

        # print(result)
        columns = ['jndi_name','table_name','statistics_column','group_function','check_type','where_condition','value_range', 'check_logic']
        # config_data不转成list，linux上会报错
        df = pd.DataFrame(list(config_data), columns=columns)
        self.df = df

        check_type_list = df['check_type'].unique()
        self.check_type_list = check_type_list
        public.write_log("配置表中check_type={}".format(check_type_list))
        # print(check_type)

    # 启动所有的情况
    def run_all(self):
        for t in self.check_type_list:
            if t in ('null', 'date'):
                self.null_date(t)
            elif t in ('customize', 'range'):
                self.customize(t)
            else:
                public.write_log("关键字【%s】暂不支持!" % t, tip='WARN')


    # 支持null，date，暂不支持range
    def null_date(self, check_type):
        if check_type not in self.check_type_list:
            public.write_log("没有找到【%s】关键字的数据，跳过" % check_type, display=self.display)
            return None
        public.write_log("开始处理【%s】关键字的数据~" % check_type, display=self.display)

        df = self.df
        df_null = df[df['check_type'] == check_type]
        df_null = df_null.copy()
        # 异常处理
        try:
            # 对所有字段名聚合,同时插入
            df_null['statistics_column'] = df_null.groupby(['jndi_name', 'table_name','where_condition'])['statistics_column'].transform(lambda x: FIELD_SEPERATOR.join(x))
        except Exception as err:
            public.write_log("check_type=【{}】出现异常，走异常处理！报错问题：{}".format(check_type,err))
        finally:
            # 过滤出这3个字段
            df_null_key = df_null.loc[:,['jndi_name', 'table_name', 'where_condition', 'statistics_column']]
            # 去重
            df_null_key = df_null_key.drop_duplicates()
            # print(df_null_key)
            datas = df_null_key.values

            # 获取值
            for line in datas:
                (jndi_name, table_name, where_condition, statistics_column) = line
                public.write_log("jndi_name=【{}】,table_name=【{}】,statistics_column=【{}】".format(jndi_name, table_name, statistics_column), display=self.display)

                statistics_column_list = statistics_column.split(FIELD_SEPERATOR)

                spliced_str = ','.join( ['ifnull(sum(isnull({0})),0) {0}'.format(x) for x in statistics_column_list])

                # public.write_log("spliced_str=【{}】".format(spliced_str), display=self.display)

                # 字段列表：check_type,check_num, jndi_name, table_name, where_condition, group_function, statistics_column
                paras = (check_type, self.check_num, jndi_name, table_name, where_condition, None, None)

                # 开始统计，并插入到log表中
                # statistics_column为【ods_product_idwarehouse_iddivision_id】
                # spliced_str为【sum(isnull(ods_product_id)) ods_product_id,sum(isnull(warehouse_id)) warehouse_id,sum(isnull(division_id)) division_id】

                column_list = (statistics_column, spliced_str)
                insert_log_table(paras=paras, column_list=column_list)


    # 自定义的，一次只查询一个分组函数的值
    def customize(self, check_type):
        df = self.df
        df_null = df[df['check_type'] == check_type]
        df_null = df_null.copy()
        df_customize = df_null.loc[:,['jndi_name', 'table_name', 'group_function', 'statistics_column', 'where_condition', 'value_range', 'check_logic']]
        df_customize = df_customize.copy()

        # print(9999999999, df_customize, type(df_customize))


        # 用法参考：https://www.jb51.net/article/172623.htm
        for row in df_customize.itertuples():
            # print(row)
            jndi_name = row[1]
            table_name = row[2]
            group_function = row[3]
            statistics_column = row[4]
            where_condition = row[5]
            range_value = row[6]
            check_logic = row[7]

            # 字段列表：check_type,check_num, jndi_name, table_name, where_condition, group_function, statistics_column
            paras = (check_type, self.check_num, jndi_name, table_name, where_condition, group_function, statistics_column)
            if check_type == 'range':
                range_list = (table_name, statistics_column, range_value, check_logic)
            else:
                range_list = None
            insert_log_table(paras=paras, column_list=None, range_list=range_list)



    def _duplication(self):
        pass

if __name__ == '__main__':
    check_num = find_parameter()
    check = Check(check_num)
    check.get_conf_data()
    check.run_all()














