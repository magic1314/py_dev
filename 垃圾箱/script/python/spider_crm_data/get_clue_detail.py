# -*- coding: UTF-8 -*-
import requests
import json, hashlib
import pymysql
import time

class XbbRequest:
    db = pymysql.connect(
        host="warehouse.bi.oigbuy.com",
        port=3306,
        user='guoxixiang',  # 在这里输入用户名
        password='Guoxixiang@123',  # 在这里输入密码
        charset='utf8mb4'
    )  # 连接数据库

    cursor = db.cursor()

    # 共享变量
    request_session = requests.session()

    def __init__(self, url: str, params: json, token: str):
        self.header = {
            'Content-Type': 'application/json;charset=UTF-8',
        }
        self.url = url
        self.params = params
        self.strparams = json.dumps(params, separators=(',', ':'))
        self.token = token

    def post(self):
        # 生成加密sign
        sign = self.__gen_sign_code(self.params, self.token)
        # sign值需要放在header中
        self.header["sign"] = sign
        # 发起post请求
        res = self.request_session.post(headers=self.header, data=self.strparams, url=self.url)
        return res

    def __gen_sign_code(self, request_parameters, xbb_access_token):
        """
        生成请求发送时的sign_code值
        """
        # json转化成字符串时加上ensure_ascii=False，不然会在有中文字符时导致sign检验不通过
        parameters = (json.dumps(request_parameters, separators=(',', ':'),
                                 ensure_ascii=False) + xbb_access_token).encode("utf-8")
        return hashlib.sha256(parameters).hexdigest()


if __name__ == "__main__":
    start_time = int(time.time() * 1000)

    host = 'http://proapi.xbongbong.com'
    # 可能会过期，后期考虑落表

    url = '/pro/v2/api/clue/detail'

    token = '0b234a1afa98da0862bb7b9ce585e410'

    get_dataid_sql = 'select id from SHWL_BSD.bsd_crm_api_clue_list'
    XbbRequest.cursor.execute(get_dataid_sql)
    dataid_list = XbbRequest.cursor.fetchall()
    
    print(f'应该加载{len(dataid_list)}条数据')
		
    truncate_sql = 'truncate table SHWL_BSD.bsd_crm_api_clue_detail'
    XbbRequest.cursor.execute(truncate_sql)

    # 记录数据条数
    count = 0

    for (dataid,) in dataid_list:
        request_json = {
            'corpid': 'ding97bd04a9ae353721',
            'userId': '051533301822782071',
            'dataId': dataid
        }
        xbbrequest = XbbRequest(host + url, request_json, token)
        res = xbbrequest.post().json()
        if res.get('result'):
            form_data = res.get('result')

            # 创建时间
            create_time = form_data['addTime']

            data_id = dataid
            formid = form_data['formId']

            # 更新时间
            update_time = form_data['updateTime']

            data = form_data['data']
            if_null_str = lambda arr, key: arr[key] if key in arr else ''
            if_null_num = lambda arr, key: arr[key] if key in arr else 0

            # 客户公司名称
            supplier_name = if_null_str(data, 'text_1')

            # 主营类目
            if if_null_str(data, 'array_2'):
                catagory_type = if_null_str(data['array_2'][0], 'text')
            else:
                catagory_type = ''

            # 供应商等级
            supplier_level = if_null_str(if_null_str(data, 'text_11'), 'text')

            # 预计年采购额
            predict_annual_purchase_amt = if_null_num(data, 'num_4')

            # 采购sku数量
            purchase_qty = if_null_num(data, 'num_5')

            # 联系人姓名
            contact_name = if_null_str(data, 'text_2')

            # 联系号码
            if if_null_str(data, 'subForm_1'):
                phone_number = if_null_str(data['subForm_1'][0],'text_2')
            else:
                phone_number = ''

            # 供应商地址
            if if_null_str(data, 'address_1'):
                address = if_null_str(data['address_1'], 'province') + if_null_str(data['address_1'],
                                                                                   'city') + if_null_str(
                    data['address_1'], 'address')
            else:
                address = ''

            # 账期天数
            account_periods_days = if_null_num(data, 'num_6')

            # 客户需求
            supplier_demands = if_null_str(data, 'text_14')

            # 线索状态
            clue_status = if_null_str(if_null_str(data, 'text_5'), 'text')

            # 线索阶段
            clue_stage = if_null_str(if_null_str(data, 'text_6'), 'text')

            # 谈判目标
            negotiating_goal = if_null_str(if_null_str(data, 'text_12'), 'text')

            # 拒绝原因（主要）
            refuse_reason_main = if_null_str(if_null_str(data, 'text_13'), 'text')

            # 拒绝原因（次要）
            refuse_reason_secondary = if_null_str(if_null_str(data, 'text_15'), 'text')

            sales_manager_list = []
            # 销售经理
            if data['ownerId']:
                for owner_data in data['ownerId']:
                    sales_manager_list.append(if_null_str(owner_data, 'name'))
                sales_manager = ('，').join(sales_manager_list)
            else:
                sales_manager = ''

            # 协同人
            if data['coUserId']:
                cooperate_name = data['coUserId'][0]['name']
            else:
                cooperate_name = ''

            # 创建人
            create_name = if_null_str(if_null_str(data, 'creatorId'), 'name')

            insert_sql = 'insert into SHWL_BSD.bsd_crm_api_clue_detail(id,supplier_name,catagory_type,supplier_level,predict_annual_purchase_amt,purchase_qty,contact_name,phone_number,address,account_periods_days,supplier_demands,clue_status,clue_stage,negotiating_goal,refuse_reason_main,refuse_reason_secondary,sales_manager,cooperate_name,create_name,create_time,update_time) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'
            XbbRequest.cursor.execute(insert_sql, (
            data_id, supplier_name, catagory_type, supplier_level, predict_annual_purchase_amt, purchase_qty,
            contact_name, phone_number, address, account_periods_days, supplier_demands, clue_status, clue_stage,
            negotiating_goal, refuse_reason_main, refuse_reason_secondary, sales_manager, cooperate_name, create_name,
            create_time, update_time))
            # print(supplier_name, '加载成功')
            count += 1

            XbbRequest.db.commit()
        else:
            continue

    XbbRequest.cursor.close()
    # XbbRequest.db.close()
    end_time = int(time.time() * 1000)
    print(f'线索详情表加载成功！！总共了{count}条数据，花费{(end_time - start_time) / 1000}秒')

