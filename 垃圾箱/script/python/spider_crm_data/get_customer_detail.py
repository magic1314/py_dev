# -*- coding: UTF-8 -*-
import requests
import json, hashlib
import pymysql
import time


class XbbRequest:
    db = pymysql.connect(
        host="warehouse.bi.oigbuy.com",
        port=3306,
        user='guoxixiang',  # 在这里输入用户名
        password='Guoxixiang@123',  # 在这里输入密码
        charset='utf8mb4'
    )  # 连接数据库

    cursor = db.cursor()

    # 共享变量
    request_session = requests.session()

    def __init__(self, url: str, params: json, token: str):
        self.header = {
            'Content-Type': 'application/json;charset=UTF-8',
        }
        self.url = url
        self.params = params
        self.strparams = json.dumps(params, separators=(',', ':'))
        self.token = token

    def post(self):
        # 生成加密sign
        sign = self.__gen_sign_code(self.params, self.token)
        # sign值需要放在header中
        self.header["sign"] = sign
        # 发起post请求
        res = self.request_session.post(headers=self.header, data=self.strparams, url=self.url)
        return res

    def __gen_sign_code(self, request_parameters, xbb_access_token):
        """
        生成请求发送时的sign_code值
        """
        # json转化成字符串时加上ensure_ascii=False，不然会在有中文字符时导致sign检验不通过
        parameters = (json.dumps(request_parameters, separators=(',', ':'),
                                 ensure_ascii=False) + xbb_access_token).encode("utf-8")
        return hashlib.sha256(parameters).hexdigest()


if __name__ == "__main__":
    start_time = int(time.time() * 1000)

    host = 'http://proapi.xbongbong.com'
    # 可能会过期，后期考虑落表

    url = '/pro/v2/api/customer/detail'

    token = '0b234a1afa98da0862bb7b9ce585e410'

    get_dataid_sql = 'select id from SHWL_BSD.bsd_crm_api_supplier_list'
    XbbRequest.cursor.execute(get_dataid_sql)
    dataid_list = XbbRequest.cursor.fetchall()

    truncate_sql = 'truncate table SHWL_BSD.bsd_crm_api_supplier_detail'
    XbbRequest.cursor.execute(truncate_sql)
    print(f'应该加载{len(dataid_list)}条数据')

    # 记录数据条数
    count = 0
    for (dataid,) in dataid_list:
        request_json = {'corpid': 'ding97bd04a9ae353721',
                        'userId': '051533301822782071',
                        'dataId': dataid
                        }
        xbbrequest = XbbRequest(host + url, request_json, token)
        res = xbbrequest.post().json()
        if res.get('result'):

            form_data = res.get('result')

            # 创建时间
            create_time = form_data['addTime']

            data_id = form_data['dataId']
            formid = form_data['formId']

            # 更新时间
            update_time = form_data['updateTime']

            data = form_data['data']
            if_null_str = lambda arr, key: arr[key] if key in arr else ''
            if_null_num = lambda arr, key: arr[key] if key in arr else 0
            # 创建人
            if if_null_str(data, 'ownerId'):
                create_name = data['ownerId'][0]['name']
            else:
                create_name = ''

            # 客户公司名称
            supplier_name = if_null_str(data, 'text_1')

            # 主营类目
            if if_null_str(data, 'array_2'):
                catagory_type = data['array_2'][0]['text']
            else:
                catagory_type = ''
            # data['array_2'][0]['text']

            # 供应商等级
            supplier_level = if_null_str(if_null_str(data, 'text_21'), 'text')

            # 预计年采购额
            predict_annual_purchase_amt = if_null_num(data, 'num_5')

            # 采购数量
            purchase_qty = if_null_num(data, 'num_6')

            # 联系人姓名
            contact_name = if_null_str(data, 'text_23')

            # 联系号码
            if if_null_str(data, 'subForm_1'):
                phone_number = data['subForm_1'][0]['text_2']
            else:
                phone_number = ''

            # 公司人数
            company_employees = if_null_str(data, 'text_25')

            # 公司年产值
            company_annual_production_amt = if_null_str(data, 'text_28')

            # 经营方式
            management_mode = if_null_str(if_null_str(data, 'text_24'), 'text')

            # 供应商地址
            address = if_null_str(if_null_str(data, 'address_1'), 'address')

            # 省
            province = if_null_str(if_null_str(data, 'address_1'), 'province')

            # 市
            city = if_null_str(if_null_str(data, 'address_1'), 'city')

            # 账期天数
            account_periods_days = if_null_num(data, 'num_7')

            # 交易方式
            trade_type = if_null_str(if_null_str(data, 'text_26'), 'text')

            # 销售时间
            sale_date = if_null_str(data, 'date_5')

            # 客户成交类型
            done_type = if_null_str(if_null_str(data, 'text_27'), 'text')

            # 成交账期天数
            done_account_periods_days = if_null_num(data, 'num_9')

            # 客户成交原因（主要）
            done_reason_main = if_null_str(if_null_str(data, 'text_29'), 'text')

            # 客户成交原因（次要）
            done_reason_secondary = if_null_str(if_null_str(data, 'text_30'), 'text')

            # 客户成交金额
            done_amt = if_null_num(data, 'num_8')

            # 成交时间

            done_date = if_null_str(data, 'date_1')


            # 销售经理
            sales_manager = if_null_str(if_null_str(data, 'text_16'), 'name')

            insert_sql = 'insert into SHWL_BSD.bsd_crm_api_supplier_detail(id,create_name,supplier_name,create_time,catagory_type,supplier_level,predict_annual_purchase_amt,purchase_qty,contact_name,phone_number,company_employees,company_annual_production_amt,management_mode,address,province,city,account_periods_days,trade_type,sale_date,done_type,done_account_periods_days,done_reason_main,done_reason_secondary,done_amt,done_date,sales_manager,update_time) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'
            XbbRequest.cursor.execute(insert_sql, (
            data_id, create_name, supplier_name, create_time, catagory_type, supplier_level,
            predict_annual_purchase_amt, purchase_qty, contact_name, phone_number, company_employees,
            company_annual_production_amt, management_mode, address, province, city, account_periods_days, trade_type,
            sale_date, done_type, done_account_periods_days, done_reason_main, done_reason_secondary, done_amt,
            done_date, sales_manager, update_time))
            count += 1

            XbbRequest.db.commit()
        else:
            continue

    XbbRequest.cursor.close()
   # XbbRequest.db.close()
    end_time = int(time.time() * 1000)
    print(f'客户详情表加载成功！！总共{count}条数据，花费{(end_time - start_time) / 1000}秒')

