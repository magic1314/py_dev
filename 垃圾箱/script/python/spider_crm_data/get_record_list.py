# -*- coding: UTF-8 -*-
import requests
import json, hashlib
import pymysql
import time


class XbbRequest:
    db = pymysql.connect(
        host="warehouse.bi.oigbuy.com",
        port=3306,
        user='guoxixiang',  # 在这里输入用户名
        password='Guoxixiang@123',  # 在这里输入密码
        charset='utf8mb4'
    )  # 连接数据库

    cursor = db.cursor()

    # 共享变量
    request_session = requests.session()

    def __init__(self, url: str, params: json, token: str):
        self.header = {
            'Content-Type': 'application/json;charset=UTF-8',
        }
        self.url = url
        self.params = params
        self.strparams = json.dumps(params, separators=(',', ':'))
        self.token = token

    def post(self):
        # 生成加密sign
        sign = self.__gen_sign_code(self.params, self.token)
        # sign值需要放在header中
        self.header["sign"] = sign
        # 发起post请求
        res = self.request_session.post(headers=self.header, data=self.strparams, url=self.url)
        return res

    def __gen_sign_code(self, request_parameters, xbb_access_token):
        """
        生成请求发送时的sign_code值
        """
        # json转化成字符串时加上ensure_ascii=False，不然会在有中文字符时导致sign检验不通过
        parameters = (json.dumps(request_parameters, separators=(',', ':'),
                                 ensure_ascii=False) + xbb_access_token).encode("utf-8")
        return hashlib.sha256(parameters).hexdigest()


if __name__ == "__main__":
    host = 'http://proapi.xbongbong.com'
    # 可能会过期，后期考虑落表
    token = '0b234a1afa98da0862bb7b9ce585e410'
    url = '/pro/v2/api/communicate/list'
    page = 1

    # 增量拉取，获取表内最大的updatetime
    get_max_updatetime = 'select max(update_time) from SHWL_BSD.bsd_crm_api_record_list'
    XbbRequest.cursor.execute(get_max_updatetime)
    max_updatetime = XbbRequest.cursor.fetchone()[0]




    request_json = {'corpid': 'ding97bd04a9ae353721',
                    'userId': '051533301822782071',
                    'page': page,
                    "pageSize": 100,
                    "conditions": [
                        {
                            "attr": "updateTime",
                            "symbol": "greaterequal",
                            "value": [
                                max_updatetime
                            ]
                        }
                    ]
                    }
    xbbrequest = XbbRequest(host + url, request_json, token)
    totalpage = xbbrequest.post().json().get('result')['totalPage']

    start_time = int(time.time()*1000)
    count = 0
    for i in range(0, totalpage):
        page = i + 1
        request_json = {
            "corpid": "ding97bd04a9ae353721",
            "userId": "051533301822782071",
            "page": page,
            "pageSize": 100,
            "conditions": [
                {
                    "attr": "updateTime",
                    "symbol": "greaterequal",
                    "value": [
                        max_updatetime
                    ]
                }
            ]
        }
        xbbrequest = XbbRequest(host + url, request_json, token)
        res = xbbrequest.post().json()
        data_list = res.get('result').get('list')
        for data in data_list:
            addtime = data['addTime']
            dataid = data['dataId']
            formid = data['formId']
            updatetime = data['updateTime']

            # 删除表内数据
            delete_sql = f'delete from SHWL_BSD.bsd_crm_api_record_list where id = {dataid}'
            XbbRequest.cursor.execute(delete_sql)

            insert_sql = 'insert into SHWL_BSD.bsd_crm_api_record_list(id,form_id,create_time,update_time) values(%s,%s,%s,%s)'
            XbbRequest.cursor.execute(insert_sql, (dataid, formid, addtime, updatetime))
            print(f'第{page}页:', dataid, '加载成功')
            count += 1
        XbbRequest.db.commit()
    XbbRequest.cursor.close()
   # XbbRequest.db.close()
    end_time = int(time.time()*1000)

    print(f'跟进记录列表加载完成！总共{count}条数据，花费{(end_time-start_time)/1000}秒')

