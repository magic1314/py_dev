# -*- coding: UTF-8 -*-
import requests
import json, hashlib
import pymysql


class XbbRequest:
    db = pymysql.connect(
        host="warehouse.bi.oigbuy.com",
        port=3306,
        user='guoxixiang',  # 在这里输入用户名
        password='Guoxixiang@123',  # 在这里输入密码
        charset='utf8mb4'
    )  # 连接数据库

    # 共享变量
    request_session = requests.session()

    def __init__(self, url: str, params: json, token: str):
        self.header = {
            'Content-Type': 'application/json;charset=UTF-8',
        }
        self.url = url
        self.params = params
        self.strparams = json.dumps(params, separators=(',', ':'))
        self.token = token

    def post(self):
        # 生成加密sign
        sign = self.__gen_sign_code(self.params, self.token)
        # sign值需要放在header中
        self.header["sign"] = sign
        # 发起post请求
        res = self.request_session.post(headers=self.header, data=self.strparams, url=self.url)
        return res

    def __gen_sign_code(self, request_parameters, xbb_access_token):
        """
        生成请求发送时的sign_code值
        """
        # json转化成字符串时加上ensure_ascii=False，不然会在有中文字符时导致sign检验不通过
        parameters = (json.dumps(request_parameters, separators=(',', ':'),
                                 ensure_ascii=False) + xbb_access_token).encode("utf-8")
        return hashlib.sha256(parameters).hexdigest()


if __name__ == "__main__":
    host = 'http://proapi.xbongbong.com'
    # 可能会过期，后期考虑落表
    token = '0b234a1afa98da0862bb7b9ce585e410'
    url = '/pro/v2/api/form/list'
    '''
    表单业务类型,客户：100，合同订单：201，退货退款：202，销售机会：301，联系人：401，跟进记录：501，回款计划：701，回款单：702，销项发票：901，
    供应商：1001，采购合同：1101，采购入库单：1404，其他入库单：1406，销售出库单：1504，其他出库单：1506，
    调拨单：1601，盘点单：1701，产品：2401；报价单：4700；线索：8000；市场活动：8100；仓库：1801；自定义表单：不传
    '''
    businesstype_list = [100, 201, 202, 301, 401, 501, 701, 702, 901, 1001, 1101, 1404, 1406, 1504, 1506, 1601, 1701, 2401,
                    4700, 8000, 8100, 1801]

    cursor = XbbRequest.db.cursor()
    sql = 'truncate table SHWL_BSD.bsd_crm_api_form_list'
    cursor.execute(sql)

    for businesstype in businesstype_list:
        request_json = {'corpid': 'ding97bd04a9ae353721',
                        'userId': '051533301822782071',
                        "saasMark": 1,
                        'businessType': businesstype
                        }
        xbbrequest = XbbRequest(host + url, request_json, token)
        res = xbbrequest.post().json()

        form_list = res.get('result').get('formList')

        for form_info in form_list:
            formid = form_info['formId']
            name = form_info['name']
            appid = form_info['appId']
            businesstype = form_info['businessType']
            menuid = form_info['menuId']
            isprocessform = form_info['isProcessForm']

            insert_sql = 'insert into SHWL_BSD.bsd_crm_api_form_list(form_id,name,app_id,business_type,menu_id,is_processform) values (%s,%s,%s,%s,%s,%s)'
            cursor.execute(insert_sql,(formid,name,appid,businesstype,menuid,isprocessform))


        XbbRequest.db.commit()

   # XbbRequest.db.close()
    print('formid加载完成！')


