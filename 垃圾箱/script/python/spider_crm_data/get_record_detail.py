# -*- coding: UTF-8 -*-
import requests
import json, hashlib
import pymysql
import time

class XbbRequest:
    db = pymysql.connect(
        host="warehouse.bi.oigbuy.com",
        port=3306,
        user='guoxixiang',  # 在这里输入用户名
        password='Guoxixiang@123',  # 在这里输入密码
        charset='utf8mb4'
    )  # 连接数据库

    cursor = db.cursor()

    # 共享变量
    request_session = requests.session()

    def __init__(self, url: str, params: json, token: str):
        self.header = {
            'Content-Type': 'application/json;charset=UTF-8',
        }
        self.url = url
        self.params = params
        self.strparams = json.dumps(params, separators=(',', ':'))
        self.token = token

    def post(self):
        # 生成加密sign
        sign = self.__gen_sign_code(self.params, self.token)
        # sign值需要放在header中
        self.header["sign"] = sign
        # 发起post请求
        res = self.request_session.post(headers=self.header, data=self.strparams, url=self.url)
        return res

    def __gen_sign_code(self, request_parameters, xbb_access_token):
        """
        生成请求发送时的sign_code值
        """
        # json转化成字符串时加上ensure_ascii=False，不然会在有中文字符时导致sign检验不通过
        parameters = (json.dumps(request_parameters, separators=(',', ':'),
                                 ensure_ascii=False) + xbb_access_token).encode("utf-8")
        return hashlib.sha256(parameters).hexdigest()



if __name__ == "__main__":


    host = 'http://proapi.xbongbong.com'
    # 可能会过期，后期考虑落表
    token = '0b234a1afa98da0862bb7b9ce585e410'
    url = '/pro/v2/api/communicate/detail'

    get_dataid_sql = 'select id from SHWL_BSD.bsd_crm_api_record_list'
    XbbRequest.cursor.execute(get_dataid_sql)
    dataid_list = XbbRequest.cursor.fetchall()
	   
    print(f'应加载{len(dataid_list)}条数据')
    truncate_sql = 'truncate table SHWL_BSD.bsd_crm_api_record_detail'
    XbbRequest.cursor.execute(truncate_sql)

    # 记录开始时间
    start_time = int(time.time() * 1000)

    # 记录数据条数
    count=0

    for (dataid,) in dataid_list:
        request_json = {'corpid': 'ding97bd04a9ae353721',
                        'userId': '051533301822782071',
                        'dataId': dataid
                        }
        xbbrequest = XbbRequest(host + url, request_json, token)
        res = xbbrequest.post().json()

        if res.get('result'):
            form_data = res.get('result')
            # print(form_data)
            # break
            # 创建时间
            create_time = form_data['addTime']
            data_id = form_data['dataId']
            formid = form_data['formId']
            # 更新时间
            update_time = form_data['updateTime']

            data = form_data['data']
            if_null_func = lambda arr, key: arr[key] if key in arr else ''
            # 创建人
            create_name = data['creatorId']['name']

            # 拜访时间
            visit_time = if_null_func(data,'date_1')

            # 拜访目标
            visit_goal = if_null_func(if_null_func(data, 'text_2'), 'text')

            # 客户公司名称
            customer_name = if_null_func(if_null_func(data, 'text_1'), 'name')
            customer_company_name = if_null_func(if_null_func(data, 'text_5'), 'name')

            # 商务类型
            business_type = if_null_func(if_null_func(data, 'text_5'), 'businessType')

            # 拜访方式
            visit_type = if_null_func(if_null_func(data, 'text_4'), 'text')

            # 跟进内容
            follow_content = if_null_func(data, 'text_6')

            # 拜访结果
            visit_result = if_null_func(if_null_func(data, 'text_8'), 'text')

            insert_sql = 'insert into SHWL_BSD.bsd_crm_api_record_detail(id,create_name,create_time,customer_name,customer_company_name,visit_time,business_type,visit_goal,visit_type,follow_content,visit_result,update_time,form_id) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'
            XbbRequest.cursor.execute(insert_sql, (
                data_id, create_name, create_time, customer_name,customer_company_name, visit_time, business_type, visit_goal, visit_type,
                follow_content, visit_result, update_time, formid))

            count += 1

            XbbRequest.db.commit()
        else:
            continue

    XbbRequest.cursor.close()
    # XbbRequest.db.close()
    end_time = int(time.time() * 1000)
    print(f'跟进详情表加载成功！总共{count}条数据，花费{(end_time - start_time) / 1000}秒')
