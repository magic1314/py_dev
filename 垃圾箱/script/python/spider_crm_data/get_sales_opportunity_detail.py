# -*- coding: UTF-8 -*-
import requests
import json, hashlib
import pymysql
import time

class XbbRequest:
    db = pymysql.connect(
        host="warehouse.bi.oigbuy.com",
        port=3306,
        user='guoxixiang',  # 在这里输入用户名
        password='Guoxixiang@123',  # 在这里输入密码
        charset='utf8mb4'
    )  # 连接数据库

    cursor = db.cursor()

    # 共享变量
    request_session = requests.session()

    def __init__(self, url: str, params: json, token: str):
        self.header = {
            'Content-Type': 'application/json;charset=UTF-8',
        }
        self.url = url
        self.params = params
        self.strparams = json.dumps(params, separators=(',', ':'))
        self.token = token

    def post(self):
        # 生成加密sign
        sign = self.__gen_sign_code(self.params, self.token)
        # sign值需要放在header中
        self.header["sign"] = sign
        # 发起post请求
        res = self.request_session.post(headers=self.header, data=self.strparams, url=self.url)
        return res

    def __gen_sign_code(self, request_parameters, xbb_access_token):
        """
        生成请求发送时的sign_code值
        """
        # json转化成字符串时加上ensure_ascii=False，不然会在有中文字符时导致sign检验不通过
        parameters = (json.dumps(request_parameters, separators=(',', ':'),
                                 ensure_ascii=False) + xbb_access_token).encode("utf-8")
        return hashlib.sha256(parameters).hexdigest()



if __name__ == "__main__":
    start_time = int(time.time() * 1000)

    host = 'http://proapi.xbongbong.com'
    # 可能会过期，后期考虑落表

    url = '/pro/v2/api/opportunity/detail'

    token = '0b234a1afa98da0862bb7b9ce585e410'

    get_dataid_sql = 'select id from SHWL_BSD.bsd_crm_api_sales_opportunity_list'
    XbbRequest.cursor.execute(get_dataid_sql)
    dataid_list = XbbRequest.cursor.fetchall()
    
    print(f'应该加载{len(dataid_list)}条数据')	

    truncate_sql = 'truncate table SHWL_BSD.bsd_crm_api_sales_opportunity_detail'
    XbbRequest.cursor.execute(truncate_sql)

    # 记录数据条数
    count = 0
    for (dataid,) in dataid_list:
        request_json = {
                        'corpid': 'ding97bd04a9ae353721',
                        'userId': '051533301822782071',
                        'dataId': dataid
                        }
        xbbrequest = XbbRequest(host + url, request_json, token)
        res = xbbrequest.post().json()
        if res.get('result'):
            form_data = res.get('result')
            # 创建时间
            create_time = form_data['addTime']
            data_id = dataid
            formid = form_data['formId']

            # 更新时间
            update_time = form_data['updateTime']

            data = form_data['data']
            if_null_str = lambda arr, key: arr[key] if key in arr else ''
            if_null_num = lambda arr, key: arr[key] if key in arr else 0


            # 销售机会编号
            sales_opportunity_no = if_null_str(data, 'serialNo')

            # 客户名称
            supplier_name = if_null_str(if_null_str(data, 'text_3'), 'name')

            # 销售阶段
            sales_stage = if_null_str(if_null_str(data, 'text_17'), 'text')

            # 销售经理
            if data['ownerId']:
                sales_manager = data['ownerId'][0]['name']
            else:
                sales_manager = ''

            # 协同人
            if data['coUserId']:
                cooperate_name = data['coUserId'][0]['name']
            else:
                cooperate_name = ''

            if data['array_1']:
                # 关联产品表的id
                product_id = if_null_str(data['array_1'][0], 'text_1')

                # 数量
                product_qty = if_null_num(data['array_1'][0], 'num_3')

                # 预估采购单价
                predict_purchase_unit_price = if_null_num(data['array_1'][0], 'num_1')

                # 预估采购价
                predict_purchase_price = if_null_num(data['array_1'][0], 'num_5')

                # 折扣
                discount = if_null_num(data['array_1'][0], 'num_4')

                # 单价
                unit_price = if_null_num(data['array_1'][0], 'num_6')

            else:
                product_id = ''
                product_qty = 0
                predict_purchase_unit_price = 0
                predict_purchase_price = 0
                discount = 0
                unit_price = 0

            # 预计金额
            predict_amt = if_null_num(data, 'num_1')

            # 预计结束日期
            if data['date_1']:
                done_date = data['date_1']
            else:
                done_date = ''

            # 创建人
            create_name = if_null_str(if_null_str(data, 'creatorId'), 'name')

            # 赢率
            win_rate = if_null_str(data, 'long_3')

            insert_sql = 'insert into SHWL_BSD.bsd_crm_api_sales_opportunity_detail(id,create_name,sales_opportunity_no,supplier_name,sales_stage,sales_manager,cooperate_name,product_id,product_qty,predict_purchase_unit_price,predict_purchase_price,discount,unit_price,predict_amt,done_date,win_rate) values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)'
            XbbRequest.cursor.execute(insert_sql, (data_id,create_name,sales_opportunity_no,supplier_name,sales_stage,sales_manager,cooperate_name,product_id,product_qty,predict_purchase_unit_price,predict_purchase_price,discount,unit_price,predict_amt,done_date,win_rate))
            print(sales_opportunity_no, '加载成功')
            count += 1

            XbbRequest.db.commit()
        else:
            continue

    XbbRequest.cursor.close()
    # XbbRequest.db.close()
    end_time = int(time.time() * 1000)
    print(f'销售机会详情表加载成功！！总共了{count}条数据，花费{(end_time - start_time) / 1000}秒')
