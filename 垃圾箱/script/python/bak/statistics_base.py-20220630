# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：count.py
# 作者：magic
# 版本：v1.0
# 创建时间：20210901
# 功能简述：统计表中字段某条件下的记录数
# 执行命令：python count.py + 下面的参数
"""
# 参数说明：
-- @@paras
@para1:check_num=xxx(检查编号，config表中的主键)
@para2: jndi_name=xxx(jndi的名称),默认为SHWL_ODS.txt
@para3: table_name=xxx(文件名称),必须
@para4: where_condition=xxx(where条件),默认None
@para5: group_function=xxx(分组函数),默认count)
@para6: statistics_column=xxx(统计的列),默认1

@para7: check_type=xxx(检查的类型)，必须
-- @@column_list
默认为None，不做处理
有值时，替代group_function和statistics_column
# eg: check_type=compare check_num=full_test_row jndi_name=5.60_Test.txt table_name=test_row where_condition=None group_function=count statistics_column=1
"""
# 应用平台：window10、linux
# 注：谨慎操作；导入表数据之前，需要备份原有表的数据
#######################################################################################
# 更新记录
# 20210901: 完善脚本
# 20210924：支持range类型的检查
# 20210925：变量不能使用range关键字
# 20211012：共享的公共函数脚本位置变动
########################################################################################
import time
import sys
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import public
import special_function

# 分隔符
FIELD_SEPERATOR = "\001"

# 获得平台名称
def get_platform_name():
    # 判断windows和linux系统
    if sys.platform == 'win32':
        platform_name = 'windows'
    elif sys.platform == 'linux':
        platform_name = 'linux'
    else:
        platform_name = None
        public.write_log('当前【%s】系统不支持！！' % sys.platform)

    return platform_name



# 获得传入的参数
def get_parameter(least_num=1):
    len_para = len(sys.argv) - 1
    if len_para < least_num:
        public.write_log("传入的参数个数小于least_num，当前传入%s个" % len_para, tip='ERROR')
    public.write_log("传入参数：【{}】".format(sys.argv))
    para_dic = dict()
    # 目前只需要etl_id这一个参数
    key = None
    value = None
    for i in sys.argv:
        if '=' in i:
            # print(i)
            key = i.split('=')[0]
            value = i.split('=')[1]
            para_dic[key] = value
    if not para_dic:
        public.write_log("没有获得参数", tip='ERROR')
    return para_dic


def get_value(dic_name, key):
    value = None
    try:
        value = dic_name[key]
    except KeyError:
        pass
    # print(666, key, value)
    return value


def find_parameter():
    paras = get_parameter()
    # print(paras)

    # 如果获取不到字段的变量，通过
    check_type = get_value(paras, 'check_type')
    check_num = get_value(paras, 'check_num')
    jndi_name = get_value(paras, 'jndi_name')
    table_name = get_value(paras, 'table_name')
    where_condition = get_value(paras, 'where_condition')
    group_function = get_value(paras, 'group_function')
    statistics_column = get_value(paras, 'statistics_column')


    public.write_log("传入参数：check_type=【{}】，check_num=【{}】，jndi_name=【{}】，table_name=【{}】，where_condition=【{}】，group_function=【{}】，statistics_column=【{}】".format(check_type,check_num,jndi_name, table_name, where_condition, group_function, statistics_column))

    return (check_type,check_num, jndi_name, table_name, where_condition, group_function, statistics_column)



# 可以获得记录数
def statistics(paras, column_list=None):
    (check_type,check_num, jndi_name, table_name, where_condition, group_function, statistics_column) = paras


    if not check_type:
        public.write_log("check_type不能为空!", tip='ERROR')

    if not check_num:
        public.write_log("check_num不能为空!", tip='ERROR')

    if not jndi_name:
        jndi_name = '5.60_Test.txt'

    if not table_name:
        public.write_log("table_name不能为空!", tip='ERROR')

    if not where_condition or where_condition=='None':
        where_condition = ''
    else:
        where_condition = where_condition.strip()
        if not where_condition.lower().startswith('where'):
            where_condition='where ' + where_condition

    if not group_function:
        group_function = 'count'

    if not statistics_column:
        statistics_column = 1


    # 如果column_list没有传入值，用group_function+statistics_column匹配的结果

    if not column_list:
        spliced_str = '{}({}) cnt'.format(group_function, statistics_column)
    else:
        spliced_str = column_list[1]

    sql = """
select {}
from {}
{}""".format(spliced_str, table_name, where_condition)

    # print(sql)
    start_time = time.time()
    result = public.run_sql(sql_txt=sql,dbFile=jndi_name,needResult=True,executemany=False,parameter_list=None,display=False)
    end_time = time.time()

    used_time = end_time - start_time

    result_value = -1
    if result:
        result_value = result[0]
    else:
        public.write_log("执行结果为空，请检查！", tip='WARN')
    # public.write_log("sql执行的结果为:【{}】".format(result_value))
    return (sql, result_value, used_time)



def insert_log_table(paras=None, column_list=None, range_list=None):
    # print(paras, len(paras), type(paras))
    # print(column_list)
    (check_type, check_num, jndi_name, table_name, where_condition, group_function, statistics_column) = paras
    # 得到记录数据和sql语句
    (sql_txt, result_value, used_time) = statistics(paras, column_list)
    today = public.get_parameter('now_date2')


    # 检查是否通过
    is_pass = 1
    if range_list:
        (table_name, statistics_column, range_value, check_logic) = range_list
        a_value = result_value[0]
        print("需要开始比较：", a_value, range_value)
        is_pass = special_function.operation(value=a_value, condition=range_value, table_name=table_name, statistics_column=statistics_column, warn_info=check_logic)



    # 不删除当天的记录
    # del_sql = """
    # delete from etl_py_statistics_log
    # where data_date='%s'
    # """ % today
    # public.run_sql(sql_txt=del_sql,dbFile='etl_config.txt',needResult=False,executemany=False,parameter_list=None,display=False)


    # column_list为空
    if not column_list:
        line = (today,check_type, check_num,  jndi_name, table_name, where_condition,group_function, statistics_column, result_value[0], used_time, sql_txt)
        sql = """
        insert into etl_py_statistics_log(data_date, check_type, check_num,jndi_name,table_name,where_condition,group_function,statistics_column,record_rows,used_time,etl_sql)
        values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        """
        public.run_sql(sql_txt=sql,dbFile='etl_config.txt',needResult=False,executemany=False,parameter_list=line,display=False)

        public.write_log("已插入1条数据到配置库etl_py_statistics_log表中！")
    # column_list不为空时
    else:
        statistics_columns = column_list[0].split(FIELD_SEPERATOR)

        # print("statistics_columns=", statistics_columns)
        # print("statistics_columns_len=", len(statistics_columns))

        # sys.exit(0)
        len_statistics_columns = len(statistics_columns)
        lines = list()
        for i in range(len_statistics_columns):
            line = (today, check_type, check_num, jndi_name, table_name, where_condition, group_function, statistics_columns[i],
                    result_value[i], used_time, sql_txt)
            lines.append(line)
        sql = """
        insert into etl_py_statistics_log(data_date, check_type, check_num,jndi_name,table_name,where_condition,group_function,statistics_column,record_rows,used_time,etl_sql)
        values(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
        """
        public.run_sql(sql_txt=sql,dbFile='etl_config.txt',needResult=False,executemany=True,parameter_list=lines,display=False)
        public.write_log("已插入%s条数据到配置库etl_py_statistics_log表中！" % len(lines))





def main():
    # 获取传入的参数
    paras = find_parameter()
    # 统计记录数，并插入到log表中
    insert_log_table(paras=paras, column_list='sum(1) sm')



if __name__ == '__main__':
    main()











