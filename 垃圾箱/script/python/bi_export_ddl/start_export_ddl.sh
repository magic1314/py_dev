#!/bin/bash

echo "--------------备份程序开始--------------"

cd /home/dev_admin/script/python/bi_export_ddl

# BSD层表备份
python /home/dev_admin/script/python/bi_export_ddl/mysql_get_ddl.py BSD
python /home/dev_admin/script/python/bi_export_ddl/mysql_beautiful_sql.py BSD

# ODS层ods开头的表,该部分的表无用
#python /home/dev_admin/script/python/bi_export_ddl/mysql_get_ddl.py ODS
#python /home/dev_admin/script/python/bi_export_ddl/mysql_beautiful_sql.py ODS


# ODS层dim开头维度表
python /home/dev_admin/script/python/bi_export_ddl/mysql_get_ddl.py DIM
python /home/dev_admin/script/python/bi_export_ddl/mysql_beautiful_sql.py DIM

# ODS层dwd开头的表
python /home/dev_admin/script/python/bi_export_ddl/mysql_get_ddl.py DWD
python /home/dev_admin/script/python/bi_export_ddl/mysql_beautiful_sql.py DWD


# ODS层dws开头的表
python /home/dev_admin/script/python/bi_export_ddl/mysql_get_ddl.py DWS
python /home/dev_admin/script/python/bi_export_ddl/mysql_beautiful_sql.py DWS


# ODS层etl开头的表
python /home/dev_admin/script/python/bi_export_ddl/mysql_get_ddl.py ETL
python /home/dev_admin/script/python/bi_export_ddl/mysql_beautiful_sql.py ETL


# KNOWLEDGE层的表
python /home/dev_admin/script/python/bi_export_ddl/mysql_get_ddl.py KLG
python /home/dev_admin/script/python/bi_export_ddl/mysql_beautiful_sql.py KLG


# 删除历史的文件
python /home/dev_admin/script/python/bi_export_ddl/delete_backup_file.py


# 2021-10-15:其他地方有调用该脚本,暂时不放在这里
# 统计【/home/dev_admin/script/python/bi_export_ddl/sql_formal】目录下今天文件的个数
# 做监控,check_id=1634109412
#python /home/dev_admin/script/python/obtain_file_num.py check_num=export_ddl



echo "--------------备份完成完成!--------------"
exit 0
