﻿#!/usr/bin/python
# -*- coding: UTF-8 -*-
import sys
import os
import time
import datetime
import public
#############################################
# 修改记录
# 2021-12-27: 正式ddl文件保留90天
############################################

# 删除n天之前的数据
del_file_days = 90
del_temp_file_days = 3


now_date = time.strftime('%Y%m%d', time.localtime())
#print("程序名称：" + sys.argv[0])

cwd = os.path.split(os.path.realpath(__file__))[0]

formal_ddl_path = cwd + os.path.sep + 'sql_formal' + os.path.sep
temp_ddl_path = cwd + os.path.sep + 'sql_temp' + os.path.sep
log_path = cwd + os.path.sep + 'log' + os.path.sep

# 删除正式的ddl文件
public.delete_his_file(formal_ddl_path, del_file_days)
# 删除临时的ddl文件
public.delete_his_file(temp_ddl_path, del_temp_file_days)
# 删除日志文件
public.delete_his_file(log_path, del_file_days)






