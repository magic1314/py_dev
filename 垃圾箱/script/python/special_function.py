# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：special_function.py
# 作者：magic
# 版本：v1.0
# 创建时间：20210924
# 功能简述：特殊的函数
# operation函数，检查表达式是否正确，不正确有dingding告警
#######################################################################################
# 更新记录
# 2021-09-24 magic: 完善脚本
# 2021-10-12 magic：共享的公共函数脚本位置变动
# 2022-08-23 magic: 运算符写错了
########################################################################################
import time
import sys
import os
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import public
import dingding


# 脚本名称
py_file_name = os.path.realpath(__file__)
# 机器人ID，10为个人群，58为正式群
robot_id=58

def operation(value=None, condition=None , warn_info=None, table_name=None, statistics_column=None):
    public.write_log("开始进行运算")
    if value == None or condition == None:
        public.write_log("value和condition都不能为空！ ", tip='ERROR')


    condition = condition.replace(' ','')
    operator = ''
    if condition.startswith('==') or condition.startswith('='):
        operator = '=='
    elif condition.startswith('<>') or condition.startswith('!='):
        operator = '!='
    elif condition.startswith('>='):
        operator = '>='
    elif condition.startswith('<='):
        operator = '<='
    elif condition.startswith('>'):
        operator = '>'
    elif condition.startswith('<'):
        operator = '<'
    else:
        public.write_log("字段depend_condition(依赖条件)格式为【运算符+数值】,【{0}】不符合！请检查~".format(condition), tip='ERROR')


    # 数值部分
    value_in_condition = condition.replace('=', '').replace('>','').replace('<','')


    expression = '{0} {1} {2}'.format(value,operator,value_in_condition)
    public.write_log("开始计算：" + expression)


    # if eval(expression)
    if eval(expression):
        public.write_log("检查通过！")
        return 1
    else:
        public.write_log("【{0}】检查不通过!".format(expression))
        warn_content = """---------- 异常预警通知 ---------- 
【异常概述】{0} 
【检查对象】{1}
【配置表   】etl_py_statistics_t1_config(检查类型为range)
【告警脚本】{2}
【异常内容】SQL统计值为：【{3}】，理论满足的条件【{4}】，条件不成立！
【核查方式】SQL统计逻辑请查看etl_py_statistics_t1_config(check_num='HW003')配置表，也可以看对应的etl_py_statistics_log表。""".format(warn_info.replace('\r',''), table_name,  py_file_name, value, condition)

        dingding.dingtalk_warn_text(message=warn_content, robot_id=robot_id, warn_object=None, at_all=False)
        return 0


