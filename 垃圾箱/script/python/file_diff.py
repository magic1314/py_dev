# encoding: utf-8
import time
import sys
import os
import glob
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import public



# file1 = r"D:\download\2021-08-23\source_oms_orders_lineitem_20210823_6750064_7000000.txt"
# file2 = r"D:\download\2021-08-23\target_oms_orders_lineitem_20210823_6750064_7000000.txt"



def compare_file(catalog, file1, file2, check_file):
    # check_file = r'D:\download\2021-08-23\diff_result_20210823.txt'

    os.chdir(catalog)
    # 删除检查的文件
    public.del_file(check_file)

    t1 = time.time()
    data1 = []
    for i in open(file1, "r", encoding="gb18030"):
        i = i.strip("\n")
        i = i.lower()
        data1.append(i)
    public.write_log("第一个file加入成功！")
    data2 = []
    for i in open(file2, "r", encoding="gb18030"):
        i = i.strip("\n")
        i = i.lower()
        data2.append(i)
    public.write_log("第二个file加入成功！")

    len1 = len(data1)
    len2 = len(data2)


    if len1 != len2:
        public.write_log("file1和file2的行数不一致，len1=%s, len2=%s" % (len1, len2), tip='ERROR')
    else:
        public.write_log("file1和file2的行数一致，共有%s行" % len1)

    diff_value = list()
    for i in range(len1):
        if i !=0 and i % 20000 == 0:
            public.write_log("已读取到{}行，进度为：{}%，发现不同的记录行数为：{}".format(i, round(round(i/len1,4)*100,2), len(diff_value)))

        if data1[i] != data2[i]:
            diff_value.append(i)
            public.write_str(data1[i] + "\n", file_name=check_file, write_type='a', encoding="gbk")
            if len(diff_value) >= 10000:
                public.write_log("不同的记录超过10000行，请查看check_file=【%s】文件，结束" % check_file, tip='ERROR')


    t2 = time.time()
    used_time=round(t2 - t1,3)
    print("#### 程序耗时：%ss" % used_time)
    if diff_value:
        public.write_log("file1和file2有【%s】条数据不一致，请检查！" % (len(diff_value)), tip='ERROR')
    else:
        os.renames(file1, file1 +'.old')
        os.renames(file2, file2 +'.old')
        public.write_log("file1和file2文件一致！(并在源文件后加.old后缀)")


def start_loop(catalogue):
    files = glob.glob(catalogue + os.path.sep + 'source_*.txt', recursive=True)
    if len(files) < 1:
        public.write_log("找不到导入数据的文件(source_开头的txt文件)：{}".format(catalogue), tip='ERROR')

    for f in files:
        # D:\\work\\37-大表更新检查\\source_oms_orders_20210824_6750064_7000000.txt
        print(f)
        pos = f.rfind(os.sep)
        if pos != -1:
            source_file_name = f[pos + 1:]
            save_catalog = f[:pos]
        else:
            source_file_name = f
            save_catalog = '.'

        no_prefix = source_file_name[len('source_'):]

        target_file_name = 'target_' + no_prefix
        fill_target_file_name = save_catalog + os.path.sep + source_file_name
        if not os.path.isfile(fill_target_file_name):
            public.write_log("目标文件【{}】不存在，请检查！".format(fill_target_file_name), tip='ERROR')

        check_file_name = 'check_' + no_prefix

        public.write_log("目录：{}， 源文件：{}，目标文件：{}，检查文件：{}".format(save_catalog, source_file_name, target_file_name, check_file_name))
        compare_file(save_catalog, source_file_name, target_file_name, check_file_name)


if __name__ == '__main__':
    len_para = len(sys.argv) - 1
    if len_para != 1:
        public.write_log("需要传入检查文件的目录" , tip='ERROR')

    check_calalogue = sys.argv[1]
    public.write_log("传入参数：【{}】".format(check_calalogue))



    # compare_file(file1, file2)
    start_loop(check_calalogue)



