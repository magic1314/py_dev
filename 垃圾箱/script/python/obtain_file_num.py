# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：obtain_file_num.py
# 作者：magic
# 版本：v1.1
# 创建时间：2021-10-13
# 功能简述：获得某目录下正则匹配的文件个数
# 数据表：配置表（etl_py_check_filenum_config），日志表（etl_py_check_filenum_log）
# 执行命令：python obtain_file_num.txt check_num=xxx(检查编号)
# 指令：
# 应用平台：window10、linux
#######################################################################################
# 更新记录
# 2021-10-13: 完善脚本
# 2021-10-13: 参数支持传入多个，比如check_num=export_ddl|error_file_60|error_file_252
# 2021-11-18: 目录添加${TODAY}参数，即当前的日期(YYYYMMDD); 增加all，对表中所有的check_num遍历
########################################################################################
import sys
import os
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import re
import public


# 分隔符
FIELD_SEPERATOR = "\001"



YYYYMMDD=public.get_parameter("YYYYMMDD")

# 获得平台名称
def get_platform_name():
    # 判断windows和linux系统
    if sys.platform == 'win32':
        platform_name = 'windows'
    elif sys.platform == 'linux':
        platform_name = 'linux'
    else:
        platform_name = None
        public.write_log('当前【%s】系统不支持！！' % sys.platform)

    return platform_name



# 获得传入的参数
def get_parameter(least_num=1):
    len_para = len(sys.argv) - 1

    if len_para < least_num:
        public.write_log("传入的参数个数小于least_num，当前传入%s个" % len_para, tip='ERROR')
    public.write_log("传入参数：【{}】".format(sys.argv))


    para_dic = dict()
    # 目前只需要etl_id这一个参数
    for i in sys.argv:
        if '=' in i:
            # print(i)
            key = i.split('=')[0]
            value = i.split('=')[1]
            para_dic[key] = value
    if not para_dic:
        public.write_log("没有获得参数", tip='ERROR')
    return para_dic


def get_value(dic_name, key):
    value = None
    try:
        value = dic_name[key]
    except KeyError:
        pass
    return value


def find_parameter():
    paras = get_parameter()
    # print(paras)

    # 如果获取不到字段的变量，通过
    check_num = get_value(paras, 'check_num')


    if not check_num:
        public.write_log("check_num不能为空!", tip='ERROR')


    # public.write_log("获取的参数值：check_num=【{}】".format(check_num))

    return check_num



def get_all_check_num():
    sql = """
    select group_concat(check_num order by id separator ',') check_num_list
    from etl_py_check_filenum_config
    where is_valid = 1
    """

    check_num_list = public.run_sql(sql_txt=sql, dbFile='etl_config.txt', needResult=True, executemany=False,
                                 parameter_list=None, display=False)

    if check_num_list:
        return check_num_list[0][0]
    else:
        public.write_log("【etl_py_check_filenum_config】表中未检索到有效的数据，请检查！", tip='ERROR')



class Check():
    def __init__(self, check_num):
        # self.check_num = find_parameter()
        self.check_num = check_num
        self.display = True
        # print(self.check_num)

    def set_display(self, display=True):
        self.display = display

    # 获得配置库的数据
    def get_conf_data(self):
        sql = """
select catalog_name,match_file_name,theoretical_qty 
from etl_py_check_filenum_config
where check_num='%s'
""" % self.check_num

        config_data = public.run_sql(sql_txt=sql, dbFile='etl_config.txt', needResult=True, executemany=False,
                            parameter_list=None, display=True)

        if not config_data:
            public.write_log("【etl_py_check_filenum_config】表中未发现有check_num=【{}】的有效记录，请检查！".format(self.check_num), tip='ERROR')

        (self.catalog_name, self.match_file_name, self.theoretical_qty) = config_data[0]

    def get_linux_file_num(self):
       # 获取绝对路径
       self.catalog_name = self.catalog_name.replace('${today}', YYYYMMDD)
       self.catalog_name = self.catalog_name.replace('${TODAY}', YYYYMMDD)
       self.catalog_name = os.path.abspath(self.catalog_name)

       # 匹配中年月日变量替换
       self.match_file_name = self.match_file_name.replace('${today}', YYYYMMDD)
       self.match_file_name = self.match_file_name.replace('${TODAY}', YYYYMMDD)

       cnt = 0
       for root, dirs, files in os.walk(self.catalog_name):  # 遍历统计
           # print("路径：", root)
           # print("目前有的文件：", files)
           # print("匹配文件名：", self.match_file_name)
           for each in files:
               # if each.endswith(self.match_file_name):
               if re.search(self.match_file_name, each):
               # if self.match_file_name in each:
                   cnt += 1  # 统计文件夹下文件个数
       self.actual_qty = cnt
       # print("实际个数:", self.actual_qty )  # 输出结果


    # 插入数据到日志表
    def insert_log(self):
        today = public.get_parameter('now_date2')
        line = (today, self.check_num, self.catalog_name, self.match_file_name, self.theoretical_qty, self.actual_qty)
        sql = """
        insert into etl_py_check_filenum_log(data_date,check_num,catalog_name,match_file_name,theoretical_qty,actual_qty)
        values(%s, %s, %s, %s, %s, %s)
        """
        public.run_sql(sql_txt=sql,dbFile='etl_config.txt',needResult=False,executemany=False,parameter_list=line,display=False)


def main():
    check_num = find_parameter()
    if check_num == 'all':
        check_num = get_all_check_num()
    # 对字符中逗号分割
    check_num_list = check_num.split(',')

    for item in check_num_list:
        check = Check(item)
        check.get_conf_data()
        check.get_linux_file_num()
        check.insert_log()


if __name__ == '__main__':
    main()














