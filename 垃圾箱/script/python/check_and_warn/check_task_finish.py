# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：check_task_finish.py
# 作者：magic
# 版本：v1.0
# 创建时间：2022-04-07
# 功能简述：检查任务完成情况
# 配置表：etl_py_flow_finish_check_config
# 执行命令：python check_task_finish.txt check_num=xxx(检查编号)
# 指令：
# 应用平台：window10、linux
#######################################################################################
# 更新记录
# 2022-04-07：创建该代码
########################################################################################
import sys
import pandas as pd
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import public
# import dingding
# from calc_index import  insert_log_table


# 分隔符
FIELD_SEPERATOR = "\001"

# 获得平台名称
def get_platform_name():
    # 判断windows和linux系统
    if sys.platform == 'win32':
        platform_name = 'windows'
    elif sys.platform == 'linux':
        platform_name = 'linux'
    else:
        platform_name = None
        public.write_log('当前【%s】系统不支持！！' % sys.platform, tip='ERROR')

    return platform_name



# 获得传入的参数
def get_parameter():
    public.write_log("传入参数：【{}】".format(sys.argv))

    # 没有参数时
    if len(sys.argv) == 1:
        return None


    para_dic = dict()
    # 目前只需要etl_id这一个参数
    for i in sys.argv:
        if '=' in i:
            # print(i)
            key = i.split('=')[0]
            value = i.split('=')[1]
            para_dic[key] = value
    if not para_dic:
        public.write_log("没有获得参数", tip='ERROR')
    return para_dic


def get_value(dic_name, key):
    if not dic_name:
        return None
    value = None
    try:
        value = dic_name[key]
    except KeyError:
        pass
    return value


def set_variable():
    paras = get_parameter()

    check_num = get_value(paras, 'check_num')


    if not check_num:
        public.write_log("check_num为空，默认为执行所有有效的check_num!", tip='WARN')
    else:
        public.write_log("获取的参数值：check_num=【{}】".format(check_num))
    return check_num


class Task():
    def __init__(self):
        # self.check_num = find_parameter()
        self.config_data = None
        self.azkaban_data = None
        self.check_num_list = None
        self.display = True
        self.check_num = None


    def set_check_num(self,check_num):
        self.check_num = check_num

    def set_display(self, display=True):
        self.display = display

    # 获得配置库的数据
    def get_conf_data(self):
        check_num_sql = ''
        if self.check_num:
            check_num_sql = "and check_num='%s'" % self.check_num

        now_time = public.get_parameter("now_time2")
        sql = """
select 
    check_num,flow_id,sync_direction,is_valid,check_start_time,check_end_time,check_interval_mins
from etl_config.etl_py_flow_finish_check_config
where is_valid = 1
and '{0}' between  check_start_time and check_end_time
{1}
""".format(now_time,check_num_sql)

        config_data = public.run_sql(sql_txt=sql, dbFile='etl_config.txt', needResult=True, executemany=False,
                            parameter_list=None, display=True)

        # if not config_data:
        #     public.write_log("【etl_py_flow_finish_check_config】表中无有效数据，请检查！", tip='ERROR')

        if not config_data:
            public.write_log("当前时间点，没有需要执行的任务!")
            sys.exit(0)

        # print('配置信息：', config_data)
        # sys.exit(0)

        # print(result)
        # columns = ['check_num','flow_id','sync_direction','is_valid','check_start_time','check_end_time','check_interval_mins']
        # # config_data不转成list，linux上会报错
        # df = pd.DataFrame(list(config_data), columns=columns)
        self.config_data = config_data

        # print("配置信息666666666", df)

        # check_num_list = df['check_num'].unique()
        # print(8888888888, check_num_list, type(check_num_list))
        # self.check_num_list = check_num_list
        # public.write_log("配置表中check_num={}".format(check_num_list))
        # print(check_num)

    # azkaban任务的完成时间
    def get_azkaban_data(self):
        now_time = public.get_parameter("now_time2")
        print(now_time, type(now_time))
        # sys.exit(0)
        sql = """
select
   a.flow_id
  ,date_format(from_unixtime(a.start_time/1000),'%H:%i:%s') start_time
  ,date_format(from_unixtime(a.end_time/1000),'%H:%i:%s') end_time
  ,b.check_start_time  check_start_time
  ,b.check_end_time check_end_time
  ,a.status
from azkaban.execution_flows a
inner join etl_config.etl_py_flow_finish_check_config b
on a.flow_id = b.flow_id
where from_unixtime(a.start_time/1000) >= current_date
and '{0}' between  b.check_start_time and b.check_end_time
and b.is_valid=1
order by a.start_time
        """.format(now_time)

        azkaban_data = public.run_sql(sql_txt=sql, dbFile='etl_config.txt', needResult=True, executemany=False,
                            parameter_list=None, display=True)

        self.azkaban_data = azkaban_data


    # 获得某张表的指标值
    def run_time_compare(self):
        config_data = self.config_data
        # 会卡检查时间
        azkaban_data = self.azkaban_data

        public.write_log("执行前梳理：配置信息【{}】个，azkaban任务记录数【{}】条".format(len(config_data), len(azkaban_data)))
        # 如果没有符合条件的任务，不会跑到这一步
        data_date = public.get_parameter('now_date2')
        for t in config_data:
            (check_num, flow_id, sync_direction, is_valid, check_start_time, check_end_time, check_interval_mins) = t
            finish_cnt = 0

            if azkaban_data:
                for az in azkaban_data:
                    (az_flow_id, start_time, end_time, check_start_time, check_end_time, status) = az
                    if flow_id == az_flow_id:
                        if status == 50:
                            finish_cnt += 1
                        sql = """insert into etl_config.etl_py_flow_finish_check_log(data_date,check_num,flow_id,check_start_time,check_end_time,start_time,end_time,check_interval_mins,status)
                        values(%s,%s,%s,%s,%s,%s,%s,%s,%s)
                        """
                        line = (data_date,check_num,flow_id,check_start_time,check_end_time,start_time,end_time,check_interval_mins,status)
                        public.run_sql(sql_txt=sql,dbFile='etl_config.txt',needResult=False,executemany=False,parameter_list=line,display=True)

            else:
                sql = """insert into etl_config.etl_py_flow_finish_check_log(data_date,check_num,flow_id,check_start_time,check_end_time,check_interval_mins,status)
                values(%s,%s,%s,%s,%s,%s,-1)
                """
                line = (data_date, check_num, flow_id, check_start_time, check_end_time,check_interval_mins)
                public.run_sql(sql_txt=sql, dbFile='etl_config.txt', needResult=False, executemany=False,
                               parameter_list=line, display=True)
            if finish_cnt:
                # 日志内容会冗余：由于以flow日志表作为主表，所以执行n遍，就会有n条相同的数据
                public.write_log("【{}】任务完成【{}】次".format(flow_id, finish_cnt))
            else:
                public.write_log("【{}】任务未完成！".format(flow_id, finish_cnt))


if __name__ == '__main__':
    # index_sum = Index()
    # index_sum.get_conf_data()
    # index_sum.exec_loop_table_index()
    # index_sum.run_all()
    check_num = set_variable()
    task = Task()
    task.set_check_num(check_num)
    task.get_azkaban_data()
    task.get_conf_data()
    task.run_time_compare()







