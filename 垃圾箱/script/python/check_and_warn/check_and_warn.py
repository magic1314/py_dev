# -*- coding=utf-8 -*-
"""
    @ author: magic
    @ create date: 2021-02-01
    @ usage: python check_and_warn.py chk_id=1621493501 [webhook_id='G1']
    @ purpose: 检查数据数据的完成情况
    @ used tables: 60库: etl_config_60, etl_record_60
    @ deposit path: /opt/module/kettle/etl_job/demo/T_60/python/check_and_warn/check_and_warn.py
    ***********************   更新记录   ***********************
    @2021-05-20: 完成脚本初版
    @2021-08-19: 增加了告警日志，输出到配置库etl_record2表中
    @2021-08-29: 增加告警的输出webhook_id
    @2021-09-10: 告警输入参数webhook_id改为robot_id
    @2021-09-29: 检查check_config2表时，遍历的数据用id字段来排序
    @2021-12-27: 增加at_person_code，艾特某人的功能
    @2022-03-30: chk_result_code字段识别数字和字符串的1
    @2022-04-12: 输出日志表中新增两个字段
"""
import os
import sys
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import public
import dingding

# 脚本名称
py_file_name = os.path.realpath(__file__)




# 获得传入的参数
def get_parameter():
    para_dic = dict()


    # 目前只需要etl_id这一个参数
    for i in sys.argv:
        if '=' in i:
            # print(i)
            key = i.split('=')[0]
            value = i.split('=')[1]

            para_dic[key] = value

    if not para_dic:
        public.write_log("没有获得参数", tip='ERROR')

    return para_dic



def get_config_db(chk_id):
    sql = """
    select 
        chk_id,chk_jndi,chk_rule_name,chk_sql, robot_id,at_person_code,chk_object	,chk_rule_id,chk_rule_comment
        from check_config2
    where is_in_use = 'Y'
    and chk_id = {}
    order by id
    """.format(chk_id)
    result = public.run_sql(sql_txt=sql, dbFile='etl_config.txt', needResult=True,parameter_list=None, display=True)
    # print(result)
    return result



def execute_chk_sql(sql_result):
    # chk_result = None
    # chk_result_code = None
    for r in sql_result:
        (chk_id,chk_jndi, chk_rule_name, chk_sql, robot_id, at_person_code,chk_object,chk_rule_id,chk_rule_comment)= r
        dbFile = chk_jndi + '.txt'
        result = public.run_sql(sql_txt=chk_sql, dbFile=dbFile, needResult=True, parameter_list=None, display=True)
        print(88888, result)



        if not result:
            chk_result = None
            chk_result_code = None

            # 插入到记录表(一行数据，其数据后面需要加逗号)
            script_comment = "该记录由python生成，script path:" + py_file_name
            insert_data = ((chk_id, chk_rule_name, chk_rule_comment, chk_sql, chk_result, chk_result_code,chk_object	,chk_rule_id,script_comment),)
            insert_record_sql = """
            insert into check_result2(chk_id, chk_rule_name, chk_rule_comment, chk_sql, chk_result, chk_result_code, chk_date, chk_time,chk_object	,chk_rule_id,script_comment)
            values(%s, %s, %s ,%s, %s, %s, current_date, current_time,%s,%s,%s)
            """
            public.run_sql(sql_txt=insert_record_sql, dbFile='etl_config_95.txt', needResult=True, executemany=True, parameter_list=insert_data, display=False)
            # 插入记录结束

            public.write_log("记录为空!!", tip='ERROR')
        else:
            chk_result = result[0][-2]
            chk_result_code = result[0][-1]

            # 插入到记录表(一行数据，其数据后面需要加逗号)
            script_comment = "该记录由python生成，script path:" + py_file_name
            insert_data = ((chk_id, chk_rule_name, chk_rule_comment, chk_sql, chk_result, chk_result_code,chk_object	,chk_rule_id,script_comment),)
            insert_record_sql = """
            insert into check_result2(chk_id, chk_rule_name, chk_rule_comment, chk_sql, chk_result, chk_result_code, chk_date, chk_time,chk_object	,chk_rule_id,script_comment)
            values(%s, %s, %s ,%s, %s, %s, current_date, current_time,%s,%s,%s)
            """
            public.run_sql(sql_txt=insert_record_sql, dbFile='etl_config_95.txt', needResult=True, executemany=True, parameter_list=insert_data, display=False)
            # 插入记录结束

            if str(chk_result_code) != '1':
                public.write_log("正常！")
                continue

            warn_content = """---------- 异常预警通知 ---------- 
【检查名称】{0}
【告警脚本】{1}
【检查配置】select a.chk_sql, a.* from check_config2 a where chk_id = {2};
【检查库名】{3}
【异常内容】{4} """.format(chk_rule_name, py_file_name, chk_id, chk_jndi, chk_result)
            dingding.dingtalk_warn_text(message=warn_content, robot_id=robot_id,  warn_object=at_person_code, at_all=False)


def main():
    public.write_log("传入参数：【{}】".format(sys.argv))

    result = get_parameter()
    chk_id = result['chk_id']

    config_records = get_config_db(chk_id)
    execute_chk_sql(config_records)

if __name__ == '__main__':
    main()












