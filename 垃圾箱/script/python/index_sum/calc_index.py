# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：count.py
# 作者：magic
# 版本：v1.0
# 创建时间：20210901
# 功能简述：统计表中字段某条件下的记录数
# 执行命令：python count.py + 下面的参数
"""
# 参数说明：
-- @@paras
@para1:check_num=xxx(检查编号，config表中的主键)
@para2: jndi_name=xxx(jndi的名称),默认为SHWL_ODS.txt
@para3: table_name=xxx(文件名称),必须
@para4: where_condition=xxx(where条件),默认None
@para5: group_function=xxx(分组函数),默认count)
@para6: column_name=xxx(统计的列),默认1

@para7: check_type=xxx(检查的类型)，必须
-- @@column_list
默认为None，不做处理
有值时，替代group_function和column_name
# eg: check_type=compare check_num=full_test_row jndi_name=5.60_Test.txt table_name=test_row where_condition=None group_function=count column_name=1
"""
# 应用平台：window10、linux
# 注：谨慎操作；导入表数据之前，需要备份原有表的数据
#######################################################################################
# 更新记录
# 20210901: 完善脚本
# 20210924：支持range类型的检查
# 20210925：变量不能使用range关键字
# 20211012：共享的公共函数脚本位置变动
########################################################################################
import time
import sys
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import public
# import special_function

# 分隔符
FIELD_SEPERATOR = "\001"

# 可以获得记录数
def statistics(paras, column_list=None):
    # (check_type,check_num, jndi_name, table_name, where_condition, group_function, column_name) = paras
    (jndi_name, table_name, where_condition, column_name)= paras

    if not jndi_name:
        jndi_name = '5.60_SHWL_ODS.txt'

    if not table_name:
        public.write_log("table_name不能为空!", tip='ERROR')

    if not column_list:
        public.write_log("column_list不能为空", tip='ERROR')

    if not where_condition or where_condition=='None':
        where_condition = ''
    else:
        where_condition = where_condition.strip()
        if not where_condition.lower().startswith('where'):
            where_condition='where ' + where_condition

    # 如果column_list没有传入值，用group_function+column_name匹配的结果

    spliced_str = column_list[1]

    sql = """select {}
from {}
{}""".format(spliced_str, table_name, where_condition)

    # print(sql)
    start_time = time.time()
    result = public.run_sql(sql_txt=sql,dbFile=jndi_name,needResult=True,executemany=False,parameter_list=None,display=False)
    end_time = time.time()

    if not result:
        public.write_log("执行结果失败，下面程序不再执行！", tip='WARN')
        return None

    used_time = end_time - start_time

    result_value = -1
    if result:
        result_value = result[0]
    else:
        public.write_log("执行结果为空，请检查！", tip='WARN')
    # public.write_log("sql执行的结果为:【{}】".format(result_value))
    # print('99999计算结果', result_value,type(result_value),len(result_value))
    # print(result_value[0],result_value[1])
    return (sql, result_value, used_time)



def insert_log_table(paras=None, column_list=None, range_list=None):
    # print(paras, len(paras), type(paras))
    # print(column_list)
    # print('999999999999999 insert_log_table')
    # sys.exit(0)
    # (check_type, check_num, jndi_name, table_name, where_condition, group_function, column_name) = paras
    (jndi_name, table_name, where_condition, column_name)= paras
    # print(paras)
    # 得到记录数据和sql语句
    (sql_txt, result_value, used_time) = statistics(paras, column_list)
    data_date = public.get_parameter('now_date2')

    # column_list为空
    if not column_list:
        line = (data_date, jndi_name, table_name, column_name, where_condition, sql_txt, result_value, used_time)
        # line = (today, jndi_name, table_name, where_condition, table_column, result_value[0], used_time, sql_txt)
        sql = """
        insert into etl_config.etl_py_index_sum_base_log(data_date,jndi_name,table_name,column_name,where_condition,sql_txt,result_value,used_time)
        values(%s, %s, %s, %s, %s, %s, %s, %s)
        """
        public.run_sql(sql_txt=sql,dbFile='etl_config.txt',needResult=False,executemany=False,parameter_list=line,display=False)

        public.write_log("已插入1条数据到配置库etl_py_index_sum_base_log表中(无字段列表)！")
    # column_list不为空时
    else:
        column_names = column_list[0].split(FIELD_SEPERATOR)

        # print("column_names=", column_names)
        # print("column_names_len=", len(column_names))

        # sys.exit(0)
        len_column_names = len(column_names)
        lines = list()
        for i in range(len_column_names):
            # line = (today, check_type, check_num, jndi_name, table_name, where_condition, group_function, column_names[i],
            #         result_value[i], used_time, sql_txt)
            line = (data_date, jndi_name, table_name, column_names[i], where_condition, sql_txt, result_value[i], used_time)
            # line = (data_date, jndi_name, table_name, where_condition, column_names[i], sql_txt, result_value, used_time)
            lines.append(line)
        sql = """
        insert into etl_config.etl_py_index_sum_base_log(data_date,jndi_name,table_name,column_name,where_condition,sql_txt,result_value,used_time)
        values(%s, %s, %s, %s, %s, %s, %s, %s)
        """
        # print(444444444444444, sql)
        # print(55555555555555555, lines)

        public.run_sql(sql_txt=sql,dbFile='etl_config.txt',needResult=False,executemany=True,parameter_list=lines,display=False)
        public.write_log("已插入%s条数据到配置库etl_py_index_sum_base_log表中！" % len(lines))




if __name__ == '__main__':
    pass











