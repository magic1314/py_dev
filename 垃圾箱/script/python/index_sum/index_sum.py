# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：index_sum.py
# 作者：magic
# 版本：v1.0
# 创建时间：2022-03-29
# 功能简述：指标求和统计
# 配置表：etl_py_index_sum_table_config、etl_py_index_sum_column_config、etl_py_index_sum_mapping_config
# 执行命令：python index_sum.txt check_num=xxx(检查编号)
# 指令：
# 应用平台：window10、linux
#######################################################################################
# 更新记录
# 2022-03-29: 完善脚本
########################################################################################
import sys
import pandas as pd
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import public
# import dingding
from calc_index import  insert_log_table


# 分隔符
FIELD_SEPERATOR = "\001"

# 获得平台名称
def get_platform_name():
    # 判断windows和linux系统
    if sys.platform == 'win32':
        platform_name = 'windows'
    elif sys.platform == 'linux':
        platform_name = 'linux'
    else:
        platform_name = None
        public.write_log('当前【%s】系统不支持！！' % sys.platform, tip='ERROR')

    return platform_name


class Index():
    def __init__(self):
        # self.check_num = find_parameter()
        self.df = None
        self.table_name_list = None
        self.display = True

    def set_display(self, display=True):
        self.display = display

    # 获得配置库的数据
    def get_conf_data(self):
        sql = """
select 
	t.jndi_name
	,t.table_name
	,t.where_condition
	,t.task_priority
	,t.parallel_num
	,c.column_name
from etl_config.etl_py_index_sum_column_config c
inner join etl_config.etl_py_index_sum_table_config t
	on c.table_id = t.table_id
	and t.is_valid = 1
	and c.is_valid = 1
        -- and t.table_name='dwd_profit2_gross_abc'
order by t.task_priority
"""

        config_data = public.run_sql(sql_txt=sql, dbFile='etl_config.txt', needResult=True, executemany=False,
                            parameter_list=None, display=True)

        if not config_data:
            public.write_log("【etl_py_index_sum_column_config、etl_py_index_sum_table_config】表中无有效数据，请检查！", tip='ERROR')

        # print(result)
        columns = ['jndi_name','table_name','where_condition','task_priority','parallel_num','column_name']
        # config_data不转成list，linux上会报错
        df = pd.DataFrame(list(config_data), columns=columns)
        self.df = df

        # print("配置信息666666666", df)

        table_name_list = df['table_name'].unique()
        self.table_name_list = table_name_list
        # public.write_log("配置表中table_name={}".format(table_name_list))
        # print(table_name)

    def exec_loop_table_index(self):
        table_name_list = self.table_name_list
        for t in table_name_list:
            # print(888888, t)
            self.get_table_index_number(t)

    # 获得某张表的指标值
    def get_table_index_number(self, table_name):
        df = self.df
        df_special = df[df['table_name'] == table_name]
        df_special = df_special.copy()
        # print(5555555, df_special)
        df_special['column_name'] = df_special.groupby(['jndi_name', 'table_name','where_condition'])['column_name'].transform(lambda x: FIELD_SEPERATOR.join(x))
        # 去重
        # df_special = df_special.drop_duplicates()
        df_special_key = df_special.loc[:,['jndi_name', 'table_name', 'where_condition', 'column_name']]
        df_special_key = df_special_key.drop_duplicates()

        # print(88888888, df_special_key)

        datas = df_special_key.values

        # print(6666666, datas)

        # 获取值
        for line in datas:
            (jndi_name, table_name, where_condition, column_name) = line
            public.write_log("jndi_name=【{}】,table_name=【{}】,column_name=【{}】".format(jndi_name, table_name, column_name), display=self.display)

            column_name_list = column_name.split(FIELD_SEPERATOR)

            # spliced_str = ','.join( ['ifnull(sum(isnull({0})),0) {0}'.format(x) for x in column_name_list])
            spliced_str = ','.join( ['ifnull(sum({0}),0) {0}'.format(x) for x in column_name_list])

            # public.write_log("77777 spliced_str=【{}】".format(spliced_str), display=self.display)

            # 字段列表：table_name,check_num, jndi_name, table_name, where_condition, group_function, column_name
            paras =(jndi_name, table_name, where_condition, column_name)

            # 开始统计，并插入到log表中
            # column_name为【ods_product_idwarehouse_iddivision_id】
            # spliced_str为【sum(isnull(ods_product_id)) ods_product_id,sum(isnull(warehouse_id)) warehouse_id,sum(isnull(division_id)) division_id】

            column_list = (column_name, spliced_str)
            insert_log_table(paras=paras, column_list=column_list)




    def _duplication(self):
        pass

if __name__ == '__main__':
    index_sum = Index()
    index_sum.get_conf_data()
    index_sum.exec_loop_table_index()
    # index_sum.run_all()














