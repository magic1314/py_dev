#!/usr/bin/python
# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：py_add_increase_data.py
# 作者：石大峰
# 版本：v1.0
# 创建时间：2021-06-28
# 作用：将普源数据少量数据导入到数仓
# windows：
# linux: /home/dev_admin/anaconda3/bin/python /home/dev_admin/script/python/py_add_increase_data.py etl_id=xxxx [nid=xxx]
#######################################################################################
# 更新记录
# 2021-06-28: 完善脚本
# 针对普源6张表做增量同步的任务，每次插入500条数据
# 2021-06-30: 优化脚本，目标表改为正式表；sqlserver字符集为cp936(即gbk)，只导入仅为gbk编码的数据
#                       sqlserver中nvarchar类型的数据，用utf8可以正常解析
# 2021-07-20: 明细表增量同步，需要根据的主表nid
# 2021-08-06: 脚本更新放在linux下，即该脚本
# 2021-10-12：共享的公共函数脚本位置变动
#######################################################################################
import time
import re
import os
import sys
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import public
import dingding
import pymssql


# 增量数据的上限(单位:万)
upper_limit_number = 40


# 导出文件的配置信息
FIELD_SEPERATOR = "\001"
LINE_TERMINATOR = "\n"


now_date = public.get_parameter('now_date')
py_file_name = os.path.realpath(__file__)

# 获得传入的参数
def get_parameter(para_name):
    argv = sys.argv
    if len(argv) <= 1:
        print("未传入参数，错误")
        sys.exit(2)

    # 目前只需要etl_id这一个参数
    # table_type = None
    for i in argv:
        if '=' in i:
            # print(i)
            key = i.split('=')[0]
            value = i.split('=')[1]
            # print(key, value)
            if key == para_name:
                table_type = value

                if not table_type:
                    print("etl_id参数不存在,或为0")
                    sys.exit(3)

                public.write_log('传入参数：etl_id=' + str(table_type))
                return table_type
    # 默认为空
    return None




# 获得etl_id
def deal_data(target_table_name, etl_sql, nid):
    sql = """
    select nid from SHWL_BSD.{0}_deal_nid
    where deal_flag = 1  -- 1:表示需要新增，-1表示需要删除
    and data_date >= current_date  -- 数据结存了
    """.format(target_table_name)

    result = public.run_sql(sql_txt=sql, dbFile='5.60_SHWL_BSD.txt',needResult=True, display=True)
    new_result = list()
    for i in result:
        for j in i:
            new_result.append(str(j))

    # 连接SQLServer, 字符集为utf8，会出现中文乱码（cp936）
    py_db = pymssql.connect(server='rdsbv1i462k5581y496fo.sqlserver.rds.aliyuncs.com', port='3433', user='bi',
                              password='oig123456', database='shopelf', charset='cp936')

    print("sqlserver数据库已连接！")
    # SQLServer的游标
    py_cur = py_db.cursor()
    # 删除备注信息
    etl_sql = re.sub('--.*', '', etl_sql)
    # 去掉换行符之类的
    etl_sql = etl_sql.replace('\r', ' ').replace('\n', ' ').replace('[', '').replace(']', '')
    # 多个空格转换成单个空格
    etl_sql = re.sub(' +', ' ', etl_sql)

    pos_select = etl_sql.find('select ') + len('select ')
    pos_from = etl_sql.find('from')

    # print(pos_select, pos_from)
    # print(etl_sql)

    column_list = etl_sql[pos_select: pos_from]

    minus_len = 0
    if 'substr' in etl_sql:
        minus_len = 2
    column_len = etl_sql.count(',') + 1 - minus_len
    s_list = ','.join(['%s'] * column_len)


    # 每500条etl_id插入一次
    every_num = 500

    total_num = len(new_result)
    # etl_id_list = ','.join(new_result)

    times = total_num // every_num

    remainder = total_num % every_num
    # 最后一次
    if remainder == 0:
        final_time = times
    else:
        final_time = times + 1

    if total_num == 0:
        public.write_log("无数据增加，正常退出")
        sys.exit(0)
    public.write_log("nid={}的数据有{}条，需要插入目标表【{}】！每次取nid {}条,共需要{}次插入".format(nid, total_num, target_table_name, every_num, final_time))


    if total_num >= upper_limit_number * 10000:
        public.write_log("本次同步数据量为{}条(已超过设定的阈值{}万条)，请确认后，再执行！".format(total_num, upper_limit_number), tip='ERROR')


    for i in range(final_time):
        # 如果到了最后一次
        if i == final_time:
            etl_id_list = new_result[(i + 1) * every_num:]
        else:
            etl_id_list = new_result[i*every_num:(i+1)*every_num]
        etl_id_list = ','.join(etl_id_list)

        # 一定要用三对双引号，并且数据库语句最后不要有分号";"
        sql = """{0} where {1} in ({2})""".format(etl_sql, nid, etl_id_list)
        # print("sqlserver上执行的sql: {0}".format(sql))
        # 执行数据库相应的语句
        data = None
        try:
            py_cur.execute(sql)
            data = py_cur.fetchall()
        except BaseException as error:
            public.write_log("执行sql出错:{0}, nid_list={1}".format(error, etl_id_list), tip='ERROR')

        if not data:
            public.write_log("数据内容为空，请检查!", tip='ERROR')
        # data = cur_sqls.fetchone()
        # print(data)

        # print(data[:10])
        # 去掉特殊的值
        column_list = column_list.replace('substring(memo,1, 800)','')
        sql = """
        insert into SHWL_BSD.{0}({1})
        values({2})
        """.format(target_table_name,column_list, s_list)
        public.run_sql(sql_txt=sql,dbFile='5.60_SHWL_BSD.txt',needResult=False,executemany=True,parameter_list=data,display=False)

        percentage = round((i+1)/final_time,2) * 100
        public.write_log("[{}]第{}次插入完成!,总进度为【{}%】".format(target_table_name,i+1, percentage))

        # 避免操作过快，加上sleep
        time.sleep(0.5)


    #  关闭游标
    py_cur.close()

    #  提交，有些数据库引擎无需此行
    # db_sqls.commit()

    # 关闭数据库
    py_db.close()

    public.write_log("已关闭游标和数据库！")




if __name__ == "__main__":

    public.write_log("----- 程序开始执行 -----")

    # 读取参数
    etl_id = get_parameter('etl_id')
    nid = get_parameter('nid')

    if not etl_id:
        public.write_log("etl_id不能为空")

    if not nid:
        public.write_log("nid是用默认值")
        nid = 'nid'


    # 读取配置
    # 在source_full_condition字段里面保存源库表的查询sql
    sql = """select target_table_name,source_full_condition from etl_config_60
    where etl_id = {} """.format(etl_id)
    result = public.run_sql(sql, dbFile='etl_config_95.txt', needResult=True, executemany=False,display=False)
    # print(result)

    if not result:
        public.write_log("sql结果不能为空,请检查！", tip='ERROR')


    target_table_name = result[0][0]
    etl_sql = result[0][1]


    # # 读取配置（取对应的etl_sql语句）
    # new_etl_id = ''
    # if etl_id == '1624847911':
    #     new_etl_id = '1617772852'
    # elif etl_id == '1624847912':
    #     new_etl_id = '1617772858'
    # elif etl_id == '1624847913':
    #     new_etl_id = '1617772882'
    # else:
    #     public.write_log("etl_id的取值不正确", tip='ERROR')
    #
    #
    # sql = """select etl_sql from etl_config_60
    # where etl_id = %s """ % new_etl_id
    # result = public.run_sql(sql, dbFile='etl_config.txt', needResult=True, executemany=False,display=False)
    #
    # if not result:
    #     public.write_log("sql结果不能为空,请检查！得不到etl_sql", tip='ERROR')
    #
    # etl_sql = result[0][0]


    # 获得新增的nid
    deal_data(target_table_name, etl_sql, nid)


    #
    # # print(555, result)
    # for i in result:
    #     (database_name, table_name) = i
    #     dump_table(database_name, table_name)


    public.write_log("----- 程序执行完成！-----")



