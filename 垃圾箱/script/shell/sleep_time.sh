#!/bin/bash

hourMinute=$1
log_file='/home/dev_admin/script/shell/1.log'

while true 
do
    now_time=`date +'%H%M'`
    if [[ now_time -gt $hourMinute ]]; then
        echo 'start running' >> $log_file
        break
    fi

    # value of minute
    echo "wait 5 mins" >> $log_file
    sleep 300

done
