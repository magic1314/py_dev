#!/bin/bash
# 脚本名称: backup_table.sh
# 作者：magic
# 创建时间: 2021-10-20
# 功能：
# 用法：sh backup_table.sh


#连接MySQL数据库
# Host=etl_azkaban.bi.oigbuy.com
# User=bi_select
# Password=BI_select@123
Host=warehouse.bi.oigbuy.com
User=shidafeng
Password=\@Shidafeng000

#echo "该脚本的位置：$0"



# 传入参数
if [ $#	-eq 1 ]; then
    echo "传入的参数为：$1"
    table_name=$1
else
    echo "当前传入了$#个参数，应该只有1个参数！"
    exit 1
fi

#if echo $etl_id | grep -q '[^0-9]'
#then
#        echo "参数table_name需要为数字，，请核查！！"
#        exit 1
#fi

# 年月日、时分秒
YYYYMMDD=`date '+%Y%m%d'`
HHMMSS=`date '+%H%M%S'`

#远程连接
table_cnt=`mysql -h$Host -u$User -p$Password <<EOF
select count(1) cnt from information_schema.tables 
where table_name = '${table_name}'
EOF`

table_cnt=`echo ${table_cnt} | awk '{print $2}'` 

echo "table_cnt=${table_cnt}"
if [ "$table_cnt" = "0" ]; then
  echo "找不到表${table_name}"
  exit 1
fi

if [ "$table_cnt" != "1" ]; then
  echo "存在${table_cnt}个相同的表名"
  exit 2
fi


#远程连接
full_table_name=`mysql -h$Host -u$User -p$Password <<EOF
select concat(table_schema,'.',table_name) full_table_name from information_schema.tables 
where table_name = '${table_name}'
EOF`

full_table_name=`echo ${full_table_name} | awk '{print $2}'`

if [ "$table_name" = "" ]; then
  echo "结果为null！"
  exit 3
fi


 
#远程连接
result=`mysql -h$Host -u$User -p$Password <<EOF
use SHWL_ODS;
alter table ${full_table_name} rename to ${full_table_name}_${YYYYMMDD};
create table ${full_table_name} like ${full_table_name}_${YYYYMMDD};
EOF`

error_code=$?
if [ $error_code -ne 0 ]; then
  echo "---------------------------- 执行失败！,错误代码为：$error_code--------------------------"
  #echo $result
  exit 1
else
  echo "--------------------------华丽的分割线------------------------------------"
  echo "表名已修改：${full_table_name} -> ${full_table_name}_${YYYYMMDD}"
  echo "    恭喜， 脚本执行成功！！"
  echo "--------------------------华丽的分割线------------------------------------"
  exit 0
fi

