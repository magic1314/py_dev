#!/bin/bash
# 脚本名称: table_compare.sh
# 作者：magic
# 创建时间: 2021-10-20
# 功能：执行kettle流程的命令
# 用法：sh table_compare.sh


#连接MySQL数据库
Host=etl_azkaban.bi.oigbuy.com
User=bi_select
Password=BI_select@123


echo "该脚本的位置：$0"



# 传入参数
if [ $#	-eq 1 ]; then
    echo "传入的参数为：$1"
    batch_no=$1
else
    echo "当前传入了$#个参数，应该只有1个参数！"
    exit 1
fi

#if echo $etl_id | grep -q '[^0-9]'
#then
#        echo "参数batch_no需要为数字，，请核查！！"
#        exit 1
#fi

 
#远程连接
result=`mysql -h$Host -u$User -p$Password <<EOF
select check_num from etl_config.etl_py_statistics_t2_config
where is_valid = 1
and batch_no='${batch_no}'
EOF`


if [ "$result" = "" ]; then
  echo "结果为空,程序终止！！"
  exit 1
fi

echo "before:result=【${result}】"



function run_script(){
    # 执行命令
    script_cmd="/home/dev_admin/anaconda3/bin/python /home/dev_admin/script/python/compare.py check_num=$1"
    echo "执行的命令：${script_cmd}"
    # 开始执行命令
    ${script_cmd}
    
    error_code=$?
    if [ $error_code -ne 0 ]; then
      echo "---------------------------- 执行失败！,错误代码为：$error_code--------------------------"
      exit 1
    else
      #echo "--------------------------华丽的分割线------------------------------------"
      #echo "    恭喜， 脚本执行成功！！"
      #echo "--------------------------华丽的分割线------------------------------------"
      echo ""
    fi
}





count=0
for check_num in $result
do
  if [ "$count" != "0" ]; then
    #echo "$count:$i"
    # 执行命令
    run_script $check_num
  fi
  count=`expr $count + 1`
done


