# encoding: utf-8
"""
    -------------------------- 脚本说明 --------------------------
    @ author: magic
    @ create date: 2021-04-27
    @ usage: windows10：钉钉告警的主要内容
    @ purpose: 方便调用统一的方法
    @ used_script: warn_center.py
    @ deposit path-etl_azkaban: /opt/module/kettle/etl_job/demo/T_60/python/share_function
         deposit path-etl_azkaban:D:\Script\share_function
    ***********************   更新记录   ***********************
    @2021-04-28: 完善脚本的主体内容
    @2021-05-06: 钉钉机器人post发送信息，第一种方式中，需要设置关键字；关键字必须包含在告警内容中
    @2021-05-11: 从配置表中设置一些参数
    @2021-09-10: 告警输入参数webhook_id改为robot_id
    @2021-12-21: 输出内容\r\n改为\n
    @2022-01-06: ${linefeed}变量写在sql中，表示一个换行符
"""
import sys
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import json
import requests
import public


# 钉钉告警
"""
-------- 输入参数 --------
# message: 告警的输出内容
# webhook_id:群机器人url对应的ID号；存放etl_dingding_webhook表中
# warn_object: 【@告警人的姓名】；存放在etl_dingding_at_person表中
# at_all: 【@所有人】 
2021-12-27: 增加艾特某人的功能
"""
def dingtalk_warn_text(message=None, robot_id=10, warn_object=None, at_all=False):

    # webhook list
    # dic_webhook = {
    #     # 群名称：【ETL告警】，1为【告警机器人】
    #     1: [r'https://oapi.dingtalk.com/robot/send?access_token=a8c0b762082d43cd112af90b149de14992e49bc8e478628b333c3b2649ed8f6b' , '告警'] # 关键字【告警】
    #     ,2:[r'https://oapi.dingtalk.com/robot/send?access_token=aa9ff94c843311667c1118f644215bf77d3add0a6d00b4fa0465a1dff57ffc62', '天气预报'] # 关键字【天气】
    # }

    dbFile = 'etl_config.txt'
    message = message.replace('\r\n','\n')
    message = message.replace('${linefeed}', '\n')
    # 判断为空的情况
    if not message:
        public.write_log("告警内容不能为空", tip='ERROR')


    sql = """
    select 
        webhook_url
        ,keyword
    from etl_dingding_webhook
    where robot_id = {0}
    """.format(robot_id)

    result = public.run_sql(sql, dbFile=dbFile, needResult=True)



    if not result:
        public.write_log("没有找到robot_id={}的记录".format(robot_id), tip='ERROR')

    webhook = result[0][0]
    keyword = result[0][1]
    # print(webhook)


    # 告警对象的手机号
    if warn_object:
        sql = """
select 
        phone_number
from etl_dingding_at_person
where person_code in ('{0}')
""".format("\',\'".join(warn_object.split(',')))
        result = public.run_sql(sql, dbFile=dbFile, needResult=True)
    else:
        result = None

    if not result:
        phone_number = None
    else:
        phone_number = list()
        for pn in result:
            phone_number.append(pn[0])

    send_message = "【{}】{}".format(keyword, message)
    headers = {'Content-Type': 'application/json; charset=utf-8'}
    data = {

            # 纯文字
            'msgtype': 'text'
            ,'text': {
                'title': keyword
                ,"content": send_message[:2000]
            }

            # 连接
            # "msgtype": "link",
            # "link": {
            #      "title": "时代的火车向前开"
            #      ,"text": "这个即将发布的新版本，告警"
            #      ,"picUrl": ""
            #      ,"messageUrl": "www.baidu.comI"
            # }

            # 图文形式
            #'msgtype': 'markdown'
            # ,"markdown": {
            #     "title": "杭州天气"
            #     ,"text":  "【告警】> ![screenshot](https://gw.alicdn.com/tfs/TB1ut3xxbsrBKNjSZFpXXcXhFXa-846-786.png)\n"
            #  }

            # @某人
            ,'at': {
                'atMobiles': ['18255624253'] 
                ,'isAtAll': False
            }
    }



    # 发送post
    post_data = json.dumps(data)
    public.write_log("post的数据内容：" + str(data)[:2000])
    response = requests.post(webhook, headers=headers, data=post_data)
    public.write_log("接受到的内容：" + str(response.text)[:2000])
    # return response.text




if __name__ == '__main__':
    warn_content = '测试数据111'
    dingtalk_warn_text(message=warn_content, robot_id=10, warn_object='P1', at_all=False)



