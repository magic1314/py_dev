import sys
import public
# 获得传入的参数
def get_parameter():
    argv = sys.argv
    if len(argv) <= 1:
        print("未传入参数，错误")
        sys.exit(2)

    # 目前只需要etl_id这一个参数
    table_type = None
    for i in argv:
        if '=' in i:
            # print(i)
            key = i.split('=')[0]
            value = i.split('=')[1]
            # print(key, value)
            if key == 'table_type':
                table_type = value

    if not table_type:
        print("table_type参数不存在")
        sys.exit(3)

    public.write_log('传入参数：table_type=' + str(table_type))
    return table_type