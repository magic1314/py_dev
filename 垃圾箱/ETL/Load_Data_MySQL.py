# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：Load_Data_MySQL.py
# 作者：石大峰
# 版本：v1.1
# 创建时间：20201229
# 功能简述：将数据文件导入到mysql库
# 执行命令：python Load_Data_MySQL.py  jndi=xxx(jndi的名称) file_name=xxx(文件的相对或绝对路径，支持*通配符)
# 指令：python Load_Data_MySQL.py jndi=etl_config file_name=20210817-etl_dingding_at_person.gz
# 应用平台：window10、linux
# 注：谨慎操作；导入表数据之前，需要备份原有表的数据
#######################################################################################
# 更新记录
# 20201229: 完善脚本
# 20210824: 修改为符合目前执行的环境
# 20211012：共享的公共函数脚本位置变动
# 20230814：修改为当前工作环境的内容
########################################################################################
import sys
import pymysql
import gzip
import time
import os
import glob
from concurrent.futures import ProcessPoolExecutor
from warnings import filterwarnings
filterwarnings("ignore",category=pymysql.Warning)
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import public
from mod import us


# 配置库
CONFIG_DB_JNDI = 'localhost_hrjx.txt'
# 每2000行一次提交
EVERY_LINES_COMMIT = 2000



# export_file_name = r'20210817-etl_dingding_at_person.gz'
now_date = public.get_parameter('now_date')
py_file_name = os.path.realpath(__file__)




# 判断windows和linux系统
if sys.platform == 'win32':
    platform_name = 'windows'
elif sys.platform == 'linux':
    platform_name = 'linux'
else:
    platform_name = None
    public.write_log('当前【%s】系统不支持！！' % sys.platform)



def loaddata(jndi:str, datafile:str, fiedlsep:str="\001"):
    try:
        public.write_log("{}文件开始导入".format(datafile))
        starttime: str = time.strftime("%Y%m%d%H%M%S", time.localtime(time.time()))
        if not os.path.isfile(datafile):
            print("File name not exists")
            public.write_log("File name not exists", tip='ERROR')
            return None

        # db = getdbconnect(dbprop)

        # sys.exit(1)
        # 数据文件的格式：20210817-etl_dingding_at_person.gz
        
        pos = datafile.rfind(os.sep)
        if pos != -1:
            tmp = datafile[pos + 1:]
            save_catalog = datafile[:pos]
        else:
            tmp = datafile
            save_catalog = '.'

        pos = tmp.find(".gz")
        if pos != -1:
            tmp = tmp[0:pos]
        
        data_dt = tmp[0:8]
        # print(data_dt)
        table_name = tmp[8+1:].strip()
        # print(tablename)
        public.write_log("已获取数据文件的信息：\n\t\t\t### file_name:【{}】 data_date:【{}】,tablename:【{}】".format(datafile, data_dt, table_name))



        # 清空之前表的数据
        truncate_sql = """truncate %s""" % table_name
        public.run_sql(sql_txt=truncate_sql, dbFile=jndi + '.txt', needResult=False, executemany=False, parameter_list=None,
                       display=False)
        # print(66666, '已清表空数据')



        sql = """select upper(column_name) column_name,upper(data_type) data_type  
        from information_schema.columns  
        where table_name = '%s' """ % table_name
        result = public.run_sql(sql_txt=sql,dbFile=jndi + '.txt',needResult=True,executemany=False,parameter_list=None,display=False)
        col_types = {}
        for data_row in result:
            col_types.setdefault(data_row[0],data_row[1])
        
        linenum = 0

        public.write_log("@@@ 开始导入{}表的数据".format(table_name))

        with open(datafile,"rb") as fp:
            g = gzip.GzipFile(fileobj=fp,mode="rb")
            # 第一行的数据
            # [b'id\x01person_name\x01phone_number\x01creator\x01update_time\x01remark\n']
            b = g.readlines(1)
            # print(122222, b, len(b))
            i = 0
            v=[]
            # 轮循所有的记录
            while len(b) == 1:
                linenum = linenum + 1
                s = b[0].decode().rstrip()
                # 具体的数据
                # idperson_namephone_numbercreatorupdate_timeremark
                # 1石大峰18255624253magic20210511095208
                # 2陶开15021015412magic20210511095530#
                # print(133333, s)
                dats = s.split(fiedlsep)
                # 第一行 字段头的处理
                if linenum == 1:
                    sql = "insert into " + table_name + "( " + ",".join(dats) + ") "
                    vals = "values("
                    for i, col in enumerate(dats):
                        # print("col: " + col)
                        # if col.strip() == 'UPDATE_TIME':
                        #     continue
                        col_type = col_types.get(col)
                        """if col_type == "TIME":
                            vals = vals + "TIME(%s)"
                        elif col_type == "TIMESTAMP":
                            vals = vals + "TO_DATE(%s,'YYYYMMDDHHMISS')"
                        elif col_type == "DATETIME":
                            vals = vals + "TO_DATE(%s,'YYYYMMDDHHMISS')"
                        elif col_type == "DATE":
                            vals = vals + "TO_DATE(%s,'YYYYMMDD')"
                        else:"""
                        vals = vals + "%s"

                        vals = vals + ","

                    vals = vals.rstrip(",") + ")"

                    sql = sql + vals
                else:
                    dats = [x if len(x) > 0 else None for x in dats]
                    v.append(tuple(dats))
                    # print(888888888, v)
                    i = i + 1
                    if i >= EVERY_LINES_COMMIT:
                        # print(v)
                        try:
                            # cur.executemany(sql,v)
                            # v是二维数组
                            public.run_sql(sql_txt=sql,dbFile=jndi + '.txt',needResult=False,executemany=True,parameter_list=v,display=False)
                        except Exception as e2:
                            public.write_log("文件{}导入失败(2)：{}".format(datafile, e2))
                            return False
                            # raise Exception(f"{datafile} load failed",sql)
                        # 重置计数
                        v = list()
                        i = 0
                b = g.readlines(1)
            if len(v) > 0:
                # public.run_sql(sql_txt=sql, dbFile=jndi + '.txt', needResult=False, executemany=True, parameter_list=v,
                #                display=False)
                try:
                    # cur.executemany(sql, v)
                    public.run_sql(sql_txt=sql, dbFile=jndi + '.txt', needResult=False, executemany=True,
                                   parameter_list=v, display=False)
                except Exception as e3:
                    # public.write_log(f"{datafile} load failed")
                    # public.write_log(sql,v)
                    # raise Exception(f"{datafile} load failed",sql)
                    public.write_log("文件{}导入失败(3)：{}".format(datafile, e3))
                    return False
                # db.commit()
            linenum = linenum - 1
            # msg = f"{table_name} {linenum}" + " rows imported"
            # print(msg)
            public.write_log("@@@ {}文件已导入全部的{}行记录！".format(table_name, linenum))
            # g.close()
            endtime = time.strftime("%Y%m%d%H%M%S", time.localtime(time.time()))
            # sql = """insert into data_pump_log(table_name,file_name,file_size,row_count,imp_exp_flag,start_time,end_time)
            #         values(%s,%s,%s,%s,%s,%s,%s)"""
            filesize = os.path.getsize(datafile)


            # 插入日志信息
            # public.write_log('开始插入日志')
            sql = """insert into data_dump_log(data_date,jndi_name,table_name,file_name,file_size,row_count,imp_exp_flag,platform_name,save_catalog,start_time,end_time)
                        values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
            v = (now_date, jndi, table_name, datafile, filesize, linenum, 'imp', platform_name, save_catalog, starttime, endtime)

            public.run_sql(sql_txt=sql, dbFile=CONFIG_DB_JNDI, needResult=False, executemany=False, parameter_list=v, display=True)

        # 已经完成导出的任务
        os.renames(datafile, datafile + '.old')
        public.write_log("@@@ {}文件导入完成！(并将已导入文件加.old后缀)".format(datafile))
        return True
    except Exception as e4:
        print(e4)

        return False



        # raise


if __name__ == "__main__":
    len_para = len(sys.argv) - 1

    if len_para < 2:
        public.write_log("传入的参数个数小于2，当前传入%s个" % len_para, tip='ERROR')

    public.write_log("传入参数：【{}】".format(sys.argv))

    result = us.get_parameter(sys.argv)
    jndi = result['jndi']
    file_name = result['file_name']

    if not file_name.endswith('.gz'):
        public.write_log("文件【{}】不是以.gz结尾，不继续执行程序！".format(file_name), tip='ERROR')

    ## 解决其他脚本，传入参数中有单引号的问题
    # filefilter = filefilter.replace("'","")
    files = glob.glob(file_name, recursive=False)
    if len(files) < 1:
        public.write_log("找不到导入数据的文件：{}".format(file_name), tip='ERROR')

    max_workers = 3
    if len(files) < max_workers:
        max_workers = len(files)

    public.write_log("开始加载数据导入程序！！")
    with ProcessPoolExecutor(max_workers) as do:
        for f in files:
            do.submit(loaddata,jndi, f)
        do.shutdown(wait=True)
    public.write_log("数据导入程序完成！！")
    sys.exit(0)
