#!/usr/bin/python
# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：Dump_Data_MySQL.py
# 作者：石大峰
# 版本：v1.0
# 创建时间：2021-06-08
# windows： python D:/Script/Py/Dump_Data_MySQL.py table_type=config
# linux: /home/dev_admin/anaconda3/bin/python /home/dev_admin/script/python/Dump_Data_MySQL.py table_type=config
# 输出文件名： xxxx.gz
# 是否支持查看README.md
#######################################################################################
# 更新记录
# 2021-06-08: 完善脚本
# 2021-06-09: 添加cursor.description“type_code”与数据库字段类型的对应关系
# 2021-08-24: 调整了data_dump_log表字段
# 2021-10-12：共享的公共函数脚本位置变动
# 2023-08-14: 修改为当前工作环境的内容
#######################################################################################
import gzip
import time
import os
import sys
if sys.platform == 'linux':
    sys.path.append("/home/dev_admin/script/share_function")
import public
from mod import us

# 每2000行一次读写
EVERY_LINES_DUMP = 2000


# 导出文件的配置信息
FIELD_SEPERATOR = "\001"
LINE_TERMINATOR = "\n"


now_date = public.get_parameter('now_date')
py_file_name = os.path.realpath(__file__)
CONFIG_DB_FILE = 'hrjx_develop'


def dump_table(database_name: str, table_name: str):
    if True:
    # try:
        # 读取配置信息
        sql = """select table_name,where_clause,save_catalog_windows,save_catalog_linux,jndi_name
                from data_dump_conf  
                where database_name = '{0}' and table_name = '{1}' and enable_status = 1 order by order_no """.format(database_name, table_name)

        result = public.run_sql(sql, dbFile=CONFIG_DB_FILE, needResult=True, executemany=False, display=False)
        r = result[0]
        tablename = r[0]
        whereclause = r[1]
        # 判断windows和linux系统
        if sys.platform == 'win32':
            platform_name = 'windows'
            save_catalog = r[2].replace(r'${TODAY}', now_date)
        elif sys.platform == 'linux':
            platform_name = 'linux'
            save_catalog = r[3].replace(r'${TODAY}', now_date)
        else:
            platform_name = None
            save_catalog = None
            public.write_log('当前【%s】系统不支持！！'  % sys.platform)

        jndi_name=r[4]

        public.write_log("开始导出【%s】表的数据！" % table_name)
        selectsql = "select * from " + database_name + '.' + tablename
        if not whereclause is None and len(whereclause) > 5:
            conditionsql = whereclause
            selectsql = selectsql + " where " + conditionsql

        selectsql = selectsql.lower().replace("\r", " ").replace("\n", " ") + " "

        # 库名不用小写
        selectsql = selectsql.replace(database_name.lower(), database_name)

        # 处理文件路径
        if not save_catalog:
            save_catalog = "."
        else:
            save_catalog = save_catalog.strip()

        parent_save_catalog = os.path.dirname(save_catalog)
        if not os.path.isdir(parent_save_catalog):
            public.write_log("data_dump_conf表中【%s】表的配置中，save_catalog字段对应的父级【%s】目录不存在，请检查！" % (table_name, parent_save_catalog), tip='ERROR')

        if not os.path.isdir(save_catalog):
            os.mkdir(save_catalog)

        starttime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
        filename = now_date + '-' + tablename + ".gz"

        # 获得配置信息
        db = None
        cur = None
        try:
            dbconfig = public.config_file(dbFile=jndi_name+'.txt')
            db = public.getdbconnect(dbconfig)
            cur = db.cursor()
        except Exception as err:
            public.write_log(err, tip='ERROR')

        # 执行sql
        cur.execute(selectsql)
        col_properties = cur.description
        # print(col_properties)


        # 关闭数据库连接、游标
        cur.close()
        db.close()

        col_line = ""
        for col_prop in col_properties:
            colname = col_prop[0]
            col_line = col_line + colname + FIELD_SEPERATOR
        col_line = col_line.strip(FIELD_SEPERATOR)

        dats = cur.fetchall()
        rowcnt = cur.rowcount

        if rowcnt < 1:
            public.write_log("%s表记录为空，跳过" % table_name, tip='WARN')
            return 0

        running_file = save_catalog.rstrip(os.path.sep) + os.path.sep + 'running_' + filename
        ok_file = save_catalog.rstrip(os.path.sep) + os.path.sep +  filename
        

        if os.path.isfile(running_file):
            os.remove(running_file)


        with open(running_file, "wb") as f:
            fgz = gzip.GzipFile(mode="wb", fileobj=f)
            fgz.write((col_line + LINE_TERMINATOR).encode())
            i = 0
            for row in dats:
                col_index = 0
                txt = ""
                # print(row)
                for col_prop in col_properties:
                    coltype = col_prop[1]
                    data = row[col_index]
                    col_index = col_index + 1
                    # 字符型要处理回车换行符
                    if coltype in [253, 254]:
                        if data is None:
                            data = ""
                        else:
                            # print(coltype, data)
                            data = data.replace("\r", " ").replace(LINE_TERMINATOR, " ")
                    
                    # Blob数据不读取，需要对字段序列化
                    # 空间字段（Spatial Field），可以用专门的模块处理
                    if coltype in [249, 250, 251, 252, 255]:
                        data = ""

                    # 数值型直接取数 转字符串
                    # 8是bignit , 5 float, 13 year
                    if coltype in [1, 2, 3, 4, 5, 8, 9, 246, 13]:
                        if data is None:
                            data = ""
                        else:
                            data = str(data)

                    # bit类型数据
                    if coltype in [16, ]:
                        if data is None:
                            data = ""
                        else:
                            # print(6666666, data, type(data), int(data),int.from_bytes(data, byteorder='big'))
                            data = str(int.from_bytes(data, byteorder='big'))


                    if coltype == 7:
                        if data is None:
                            data = ""
                        else:
                            data = data.strftime("%Y%m%d%H%M%S")

                    if coltype == 12:
                        if data is None:
                            data = ""
                        else:
                            data = data.strftime("%Y%m%d%H%M%S")

                    if coltype == 10:
                        # 驱动有问题，已修改
                        if data is None:
                            data = ""
                        else:
                            data = data.strftime("%Y%m%d")

                    if coltype == 11:
                        if data is None:
                            data = ""
                        else:
                            # 驱动有问题，已修改
                            # 返回timedelta
                            h, r = divmod(data.seconds, 3600)
                            m, s = divmod(r, 60)
                            # GBase没有毫秒和微秒
                            # fs = data.microseconds / 1e6
                            data = "{0:0>2}{1:0>2}{2:0>2}".format(h, m, s)
                            # data = data.strftime("%H%M%S")

                    # data不转成str，会出现问题
                    # print('>>>>> 类型为:', type(data), "coltype=", coltype, data)
                    if public.get_type(data) != 'str':
                        print('类型为:', type(data))
                        print("coltype=", coltype)
                        print(data)
                        sys.exit(0)

                    txt = txt + data + FIELD_SEPERATOR

                txt = txt[0:-1] + LINE_TERMINATOR
                fgz.write(txt.encode())

                i = i + 1
                if i >= EVERY_LINES_DUMP:
                    fgz.flush()
                    i = 0
            fgz.flush()
            fgz.close()

        filesize = os.path.getsize(running_file)

        if os.path.isfile(ok_file):
            os.remove(ok_file)
        os.rename(running_file, ok_file)
        print(running_file, ok_file)
        endtime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))

        # 插入日志信息
        # public.write_log('开始插入日志')
        sql = """insert into data_dump_log(data_date,jndi_name,table_name,file_name,file_size,row_count,platform_name,save_catalog,start_time,end_time,imp_exp_flag)
                values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
        v = (now_date, database_name, tablename, os.path.basename(ok_file), filesize, rowcnt,platform_name,os.path.abspath(save_catalog), starttime, endtime,'exp')
        public.run_sql(sql, dbFile=CONFIG_DB_FILE, needResult=False,executemany=False,parameter_list=v,display=False)


        public.write_log("【%s】表导出成功,导出文件名:【%s】" % (table_name, ok_file))
    # except Exception as error:
    #     public.write_log('出现错误:' + str(error), tip='ERROR')

if __name__ == "__main__":

    public.write_log("----- 程序开始执行 -----")

    # 读取参数
    table_type = us.get_parameter()

    # 读取配置
    sql = """select database_name,table_name
    from data_dump_conf  
    where enable_status = 1 and table_type='%s'
    order by order_no """ % table_type
    result = public.run_sql(sql, dbFile=CONFIG_DB_FILE, needResult=True, executemany=False,display=False)
    # print(result)

    # print(555, result)
    for i in result:
        (database_name, table_name) = i
        dump_table(database_name, table_name)


    public.write_log("----- 程序执行完成！-----")



