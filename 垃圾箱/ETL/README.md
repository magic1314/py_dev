Dump_Data_MySQL.py：
"""
sql执行后，结果中的描述信息(cur.description)的字段类型mapping关系
DECIMAL = 0
TINY = 1          支持
SHORT = 2         支持
LONG = 3          支持
FLOAT = 4         支持
DOUBLE = 5        支持
NULL = 6          待定(mysql没有该类型数据)
TIMESTAMP = 7     支持
LONGLONG = 8      支持
INT24 = 9         支持
DATE = 10         支持
TIME = 11         支持
DATETIME = 12     支持
YEAR = 13         支持
NEWDATE = 14      待定(mysql没有该类型数据)
VARCHAR = 15      支持(mysql VARCHAR转换成VAR_STRING)
BIT = 16          支持
JSON = 245        支持(mysql JSON转换成STRING)
NEWDECIMAL = 246  待定(mysql没有该类型数据)
ENUM = 247        支持(mysql ENUM转换成STRING)
SET = 248         支持(mysql SET转换成STRING)
TINY_BLOB = 249   不支持，数据不读取
MEDIUM_BLOB = 250 不支持，数据不读取
LONG_BLOB = 251   不支持，数据不读取
BLOB = 252        不支持，数据不读取
VAR_STRING = 253  支持
STRING = 254      支持
GEOMETRY = 255    不支持，数据不读取
CHAR = TINY       支持(mysql CHAR转换成STRING)
INTERVAL = ENUM   待定(mysql没有该类型数据)
"""


