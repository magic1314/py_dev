CREATE TABLE `data_dump_conf` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `order_no` int DEFAULT NULL COMMENT '排序序号',
  `table_type` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '表分类',
  `jndi_name` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'jndi名称',
  `database_name` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '数据库名称',
  `table_name` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '表名',
  `enable_status` int DEFAULT '1' COMMENT '有效标识',
  `where_clause` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '表过滤条件',
  `save_catalog_linux` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'linux系统保存路径',
  `save_catalog_windows` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'windows系统保存路径',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='数据导出配置表'
;


CREATE TABLE `data_dump_log` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `data_date` date DEFAULT NULL COMMENT '数据日期',
  `imp_exp_flag` varchar(3) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '导入导出标识',
  `jndi_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'jndi名称',
  `table_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '表名',
  `platform_name` varchar(10) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '系统类型',
  `save_catalog` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '系统存放路径',
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '文件名称',
  `row_count` int DEFAULT NULL COMMENT '行数',
  `file_size` decimal(26,2) DEFAULT NULL COMMENT '文件大小',
  `start_time` datetime DEFAULT NULL COMMENT '起始时间',
  `end_time` datetime DEFAULT NULL COMMENT '结束时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='数据导出日志'
;




truncate table data_dump_conf;
insert into data_dump_conf(order_no,table_type,jndi_name,database_name,table_name,enable_status,save_catalog_windows,remark)
select 
3 order_no
,'backup' table_type
,'hrjx_develop' jndi_name
,TABLE_SCHEMA database_name
,table_name
,1 enable_status
,'.\\data\\20230814_dev' save_catalog_windows
,'测试' remark
from information_schema.tables
where table_schema not in ('information_schema','sys','system')
and table_schema in ('hrjx','hr_jx')
and table_name not like '%202%'
and table_name not like '%copy_'
and table_name not like '%1'
and table_name not like '%2'
;