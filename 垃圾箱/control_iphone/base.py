import pyautogui
import pywinauto
from pywinauto import Application
import sys
import time

# 应用
class App():
    def __init__(self, app_name = '坚果云'):
        self.app_name = app_name
        self.window = None
        # 连接到名为“坚果云”的窗口
        try:
            print("开始寻找app_name")
            app = Application().connect(title=app_name)
            print(f"成功连接到{app_name}应用!")
            
            # top_level_windows = app.windows()
            # # 遍历顶级窗口并打印每个窗口的标题
            # for window in top_level_windows:
            #     print(333333, window.window_text())

            # 获取窗口
            main_window = app.window(title=app_name)
            
            # 打印窗口的位置和大小
            # rect = main_window.rectangle()
            # print(f"{app_name}窗口位置: {rect.left}, {rect.top}")
            # print(f"{app_name}窗口大小: {rect.width()}, {rect.height()}")
            
            # 进一步操作，例如点击按钮、输入文本等...
            # 例如，如果要点击一个名为"上传"的按钮，可以这样操作（假设这样的按钮存在）:
            # upload_button = main_window.child_window(title="上传", control_type="Button")
            # upload_button.click()

            self.window = main_window
            
        except Exception as e:
            print(f"未能连接到{app_name}应用: {e}")
            sys.exit(1)
    
    def get_size(self, display=True):
        rect = self.window.rectangle()
        x1 = rect.left
        y1 = rect.top
        # x2 = rect.left + rect.width() # rect.right
        # y2 = rect.top + rect.height() # rect.bottom
        w = rect.width()
        h = rect.height()
        x2 = rect.right
        y2 = rect.bottom
        if display:
            print(f"x1:{x1} y1:{y1} x2:{x2} y2:{y2}")
        # print(f"x22:{x22} y22:{y22} w:{w} h:{h}")
        return [x1, y1, x2, y2]

    # 激活窗口
    def pinned(self):
        self.window.set_focus()

    def get_wh(self):
        rect = self.window.rectangle()
        w = rect.width()
        h = rect.height()
        return [w, h]

    def click_ios(self):
        rect = self.window.rectangle()
        x1,y1,x2,y2 = rect.left,rect.top,rect.right,rect.bottom
        w,h = rect.width(),rect.height()
        if x1 < 0 or x2 < 0 or y1 < 0 or y2 < 0:
            print(f"未能获取到{self.app_name}窗口的位置")
            return None
        else:
            ios_pos_x = int(x1 + w * 0.8)
            ios_pos_y = int(y1 + h * 0.9)
            # 在当前位置执行鼠标左键单击
            pywinauto.mouse.move(coords=(ios_pos_x, ios_pos_y))
            time.sleep(3)
            pyautogui.click(ios_pos_x, ios_pos_y)
            print(f"单击完成：({ios_pos_x},{ios_pos_y})")



if __name__ == '__main__':
    window = App('Wormhole')
    window.get_size(display=True)
    window.pinned()
    window.click_ios()