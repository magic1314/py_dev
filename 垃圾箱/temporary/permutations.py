import itertools

# 定义变量
A = ('月', '季', '半年')
B = ('当期', '累计', '时点')
C = ('月', '季', '半年')
D = ('当期', '累计', '时点')

# 生成所有排列
permutations = list(itertools.product(A, B, C, D))

# 输出所有排列
for p in permutations:
    print('{}\t{}\t{}\t{}'.format(p[0], p[1], p[2], p[3]))
