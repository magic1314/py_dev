# -*- coding=utf-8 -*-
import os
import sys
import warnings
import pymysql
import time
from properties import Properties
from logger import Logger



warnings.filterwarnings("ignore")

# 格式化成2016-03-20 11:45:39形式
today = time.strftime("%Y-%m-%d", time.localtime())



#get current work directory
cwd = os.path.split(os.path.realpath(__file__))[0]

logfile = cwd + "/log/dependency-{0}.log".format(today)
log = Logger(__name__, logfile)
log.setlevel(log.INFO)
logger = log.getlogger()


# 打印正常日志
def log(info, display=True):
    info = str(info)
    if display:
        print(info)
    logger.info(info)


# 打印错误日志
def error(info):
    info = str(info)
    print(info)
    logger.error(info)
    sys.exit(8)


# 连接数据库
def getdbconnect(dbprop: dict):
    try:
        # db = connect()
        # db.connect(**dbconfig)
        # conn = pymysql.connect(host=host,user=user,password=password,database=db,charset="utf8mb4")
        db = pymysql.connect(
           host=dbprop.get("host"),
           user=dbprop.get("user"),
           password=dbprop.get("password"),
           database=dbprop.get("database"),
           charset=dbprop.get("charset"),
           autocommit=bool(dbprop.get("autocommit", True)),
           port=int(dbprop.get("port"))
        )

    except Exception as err:
        error(err)
    return db


# 执行sql语句
def run_sql(sqlTxt, dbFile='dburl_etlconfig.txt',needResult=False,parameters=None,display=False):
    try:
        cwd = os.path.split(os.path.realpath(__file__))[0]
        prop = Properties(cwd + '/' + dbFile)
        dbconfig = prop.getpropeties()
    except Exception as err:
        error(err)

    try:
        db = getdbconnect(dbconfig)
    except Exception as err:
        error(err)
    # print("connect to db successfully!")

    if display:
        # print("run_sql: ", sqlTxt)
        log("执行sql 【{0}】".format(sqlTxt))

    cur = db.cursor()
    # 有无sql执行的参数
    if parameters:
        cur.executemany(sqlTxt, parameters)
        print("已处理%d条数据" % len(parameters))
    else:
        cur.execute(sqlTxt)

    if needResult:
        sqlResult = cur.fetchall()

    cur.close()
    # 目前用完连接就断掉
    db.close()

    if needResult:
        return sqlResult


# 获得传入的参数
def get_parameter():
    argv = sys.argv
    if len(argv) <= 1:
        error("未传入参数，错误")
        sys.exit(2)

    # 目前只需要etl_id这一个参数

    for i in argv:
        if '=' in i:
            # print(i)
            key = i.split('=')[0]
            value = i.split('=')[1]
            # print(key, value)
            if key == 'etl_id':
                global etl_id
                etl_id = value

    if not etl_id:
        error("etl_id参数不存在")


# 获得配置信息
def get_config():
    global etl_id
    sql = """
    select 
	    dependencies,          -- 依赖的列表清单
	    ifnull(dependency_value,-1) dependency_value   -- 每天检查依赖的任务完成个数
    from etl_config_60
    where etl_id = {0}
        and is_job_using = 'Y'
    """.format(etl_id)

    result = run_sql(sql, needResult=True, display=True)

    if len(result) != 1:
        error("存在相同etl_id ")

    # 获得【依赖的列表清单】
    record = result[0][0].replace(' ','').rstrip(',')
    if not record:
        error("获取配置信息, 无依赖条件! ")

    # print(record)
    dependency_id_list = record.split(',')

    # 配置中依赖的个数
    dependency_value_list = len(dependency_id_list)

    # 获得【每天检查依赖的任务完成个数】
    dependency_value_need = result[0][1]

    if dependency_value_need == -1:
        dependency_value_need = dependency_value_list

    sql = """
    select 
        count(1) cnt 
    from etl_record_60
    where etl_id in ({0})
        and etl_end_time >= CURRENT_DATE
    """.format(record)

    result = run_sql(sql, needResult=True, display=True)
    dependency_value_finish = result[0][0]
    log("从日志表中得到完成的数量为: " + str(dependency_value_finish))

    # print(cnt, dependency_value)
    # print(dependency_value_finish, dependency_value_need)

    if dependency_value_finish == dependency_value_need:
        log("检查数，符合条件！完成任务数:{0}".format(dependency_value_finish) )
    else:
        log("配置中需要完成的个数:{0}, 实际完成的个数:{1}".format(dependency_value_need, dependency_value_finish))
        exit(9)


# 主函数
def main():

    get_parameter()

    log("etl_id的值:" + str(etl_id))

    get_config()




if __name__ == '__main__':
    main()











