# -*- coding=utf-8 -*-
import os
import sys
import warnings
import pymysql
import time
from properties import Properties
from logger import Logger
"""
    @ author: magic
    @ create date: 2021-02-01
    @ usage: python main.py etl_id=1611888540
    @ purpose: 由于azkaban各项目间，没有做直接的依赖关系。故通过该python脚本，读取运行日志的方法，实现依赖关系的建立
    @ used tables: 60库: etl_config_60, etl_record_60
    ***********************   更新记录   *********************** 
    @2021-02-08: 添加脚本的注释信息

"""

# 警告忽视
warnings.filterwarnings("ignore")

# 格式化成2016-03-20 11:45:39形式
today = time.strftime("%Y-%m-%d", time.localtime())

# 获得当前目录
cwd = os.path.split(os.path.realpath(__file__))[0]
print("cwd:", cwd)

# 日志文件
logfile = cwd + os.path.sep + "log"+ os.path.sep  + "dependency-{0}.log".format(today)
print(logfile)
log = Logger(__name__, logfile)
log.setlevel(log.INFO)
logger = log.getlogger()


# 打印正常日志
def log(info, display=True):
    info = str(info)
    if display:
        print(info)
    logger.info(info)


# 打印错误日志
def error(info):
    info = str(info)
    print(info)
    logger.error(info)
    sys.exit(8)


# 连接数据库
def getdbconnect(dbprop: dict):
    try:
        db = pymysql.connect(
           host=dbprop.get("host"),
           user=dbprop.get("user"),
           password=dbprop.get("password"),
           database=dbprop.get("database"),
           charset=dbprop.get("charset"),
           autocommit=bool(dbprop.get("autocommit", True)),
           port=int(dbprop.get("port"))
        )
    except Exception as err:
        error(err)
    return db


# 执行sql语句
def run_sql(sqlTxt, dbFile='dburl_etlconfig.txt',needResult=False,parameters=None,display=False):
    try:
        cwd = os.path.split(os.path.realpath(__file__))[0]
        prop = Properties(cwd + '/' + dbFile)
        dbconfig = prop.getpropeties()
    except Exception as err:
        error(err)
    try:
        db = getdbconnect(dbconfig)
    except Exception as err:
        error(err)
    # print("connect to db successfully!")

    if display:
        # print("run_sql: ", sqlTxt)
        log("执行sql 【{0}】".format(sqlTxt))

    cur = db.cursor()
    # 有无sql执行的参数
    if parameters:
        cur.executemany(sqlTxt, parameters)
        print("已处理%d条数据" % len(parameters))
    else:
        cur.execute(sqlTxt)

    if needResult:
        sqlResult = cur.fetchall()

    cur.close()
    # 目前用完连接就断掉
    db.close()

    if needResult:
        return sqlResult


# 获得传入的参数
def get_parameter():
    argv = sys.argv
    if len(argv) <= 1:
        error("未传入参数，错误")
        sys.exit(2)

    # 目前只需要etl_id这一个参数

    for i in argv:
        if '=' in i:
            # print(i)
            key = i.split('=')[0]
            value = i.split('=')[1]
            # print(key, value)
            if key == 'etl_id':
                global etl_id
                etl_id = value

    if not etl_id:
        error("etl_id参数不存在")


# 获得配置信息
def get_config():
    global etl_id
    # eg:表中两个字段 1600674543, 1600674544, 1600674547    =3
    sql = """
    select 
	    dependencies          -- 依赖的列表清单
	    ,depend_condition  -- 依赖的条件
    from etl_config_60
    where etl_id = {0}
        and is_job_using = 'Y'
    """.format(etl_id)

    result = run_sql(sql, needResult=True, display=True)

    if len(result) != 1:
        error("存在相同etl_id ")

    # 获得【依赖的列表清单】
    para_1 = result[0][0]
    record = para_1.replace(' ','').rstrip(',')
    if not record:
        error("获取配置信息, 无依赖条件! ")

    # eg. 1600674543  1600674544 1600674547
    dependency_id_list = record.split(',')

    # 配置中依赖的个数
    # eg:  3
    cnt_len_dependency_id_list = len(dependency_id_list)

    # 获得【每天检查依赖的任务完成个数，-1表示和dependencies中元素个数一直】
    # eg. 第二个字段 3
    # 支持【< 】【>】【 = 】【 == 】【>=】【 <= 】【!=】【<>】8种(==与=等价, !=与<>等价)
    para_2_original = result[0][1]
    para_2 = para_2_original
    # 默认值
    if para_2 == None or para_2.strip() == '':
        para_2 = '=-1'
    para_2 = para_2.replace(' ','')
    operator = ''
    if para_2.startswith('==') or para_2.startswith('='):
        operator = '=='
    elif para_2.startswith('<>') or para_2.startswith('!='):
        operator = '!='
    elif para_2.startswith('>='):
        operator = '>='
    elif para_2.startswith('<='):
        operator = '>='
    elif para_2.startswith('>'):
        operator = '>'
    elif para_2.startswith('<'):
        operator = '<'
    else:
        error("字段depend_condition(依赖条件)格式为【运算符+数值】,【{0}】不符合！请检查~".format(para_2))

    # 提示部分
    log("已获取表信息!! etl_id:【{0}】,dependencies:【{1}】,depend_condition:【{2}】".format(etl_id, para_1, para_2_original))

    log("")
    # 数值部分
    cnt_dependency_value_need = para_2.replace(operator, '')

    # 配置表中字段为null，表示实际完成数应该,等于配置依赖的etl_id个数
    # print('6666666666', cnt_dependency_value_need)
    if cnt_dependency_value_need == '=-1':
        cnt_dependency_value_need = '={0}'.format(cnt_len_dependency_id_list)


    sql = """
    select 
        count(1) cnt 
    from etl_record_60
    where etl_id in ({0})
        and etl_end_time >= CURRENT_DATE
    """.format(record)

    result = run_sql(sql, needResult=True, display=True)
    # 从日志表中找到实际完成的数量
    cnt_dependency_actual_finish = result[0][0]
    log("从日志表中得到完成的数量为: " + str(cnt_dependency_actual_finish))

    # print(cnt, dependency_value)
    # print(cnt_dependency_actual_finish, cnt_dependency_value_need)

    if cnt_dependency_actual_finish == cnt_dependency_value_need:
        log("etl_id={0}, 完成任务数:{0}。检查数 符合条件！".format(etl_id, cnt_dependency_actual_finish) )
    else:
        error("etl_id={0}, 配置中需要完成的个数:{1}, 实际完成的个数:{2}。当前检查不通过!".format(etl_id, cnt_dependency_value_need, cnt_dependency_actual_finish))



# 主函数
def main():

    get_parameter()

    log("etl_id的值:" + str(etl_id))
    get_config()




if __name__ == '__main__':
    main()

