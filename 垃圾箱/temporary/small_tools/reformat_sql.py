# encodint:utf-8
import sys
if sys.platform == 'linux':
    sys.path.append("/opt/module/kettle/etl_job/demo/T_60/python/share_function")
import public
import re


print('-------------------start---------------------')



class SQL:
    # 结果
    result = None

    # 初始化函数
    def __init__(self, sql):
        self.sql = sql

    # 重构sql的内容
    def remove_comment(self):
        # 将符号改为英文字母
        # 【/ 斜杠 Slash】:S，【* 星号 Asterisk】:A,【- 横杠 Horizontal bar】: H,【# 井号 Well number】:W
        # 注释
        comment = {
            'W': [0]  # 表示【#】
            ,'HH': [0, 0] # 表示【--】
            ,'SAAS': [0, 0, 0, 0]  # 表示【/**/】，0表示未找到，1表示已匹配
        }


        sql_txt = self.sql
        no_cmt_sql = list()

        h_pre_del = False
        delete_ending = False
        # left slash's delete flag
        ls_pre_del = False
        # start to delete content of slash
        delete_slash = False
        # delete end flag FOR /**/
        delete_pre_end_flag = False
        delete_end_flag = False
        # 判断/**/中，后面的*/是否存在，如果不存在该参数为True，会报出异常
        # SAAS_flag = 1
        is_error = False

        # 逐字遍历sql的内容
        for a_char in sql_txt:
            # 针对#的备注
            if a_char == '#':
                delete_ending = True
            # 针对--的备注
            elif a_char == '-':
                # 前面已经是H【-】了
                if h_pre_del:
                    # 删掉从这到/n的数据(前面的-也需要删掉)
                    delete_ending = True
                    if no_cmt_sql:
                        no_cmt_sql.pop()
                else:
                    # 首次出现
                    h_pre_del = True
            elif a_char == '/':
                ls_pre_del = True
            elif a_char == '*':
                if ls_pre_del:
                    # print(111111, a_char)
                    delete_slash = True
                    is_error = True
            else:
                ls_pre_del = False


            ############## 删除注释内容的地方 #############
            # 删除备注信息/**/
            if delete_slash:
                # 删掉从这到/*的数据(前面的/*也需要删掉)
                # print('需要删除: ', a_char)
                if no_cmt_sql:
                    no_cmt_sql.pop()
                no_cmt_sql.append('\n')

                # 没有找到【*/】
                if not delete_end_flag:
                    if a_char == '*':
                        # print(444, a_char)
                        delete_pre_end_flag = True
                    elif a_char == '/':
                        if delete_pre_end_flag:
                            # print(2222, a_char)
                            # 已经找到了
                            delete_end_flag = True
                            is_error = False
                    else:
                        delete_pre_end_flag = False
                else:
                    # print("已删除完毕")
                    delete_slash = False
                    delete_end_flag = False
            # 不是在/**/模式下
            else:
                if a_char == '\n':
                    # 【-- 和 #】需要重置参数
                    no_cmt_sql.append(a_char)
                    delete_ending = False
                    h_pre_del = False
                else:
                    # 说明这部分是不删除的内容
                    if not delete_ending:
                        no_cmt_sql.append(a_char)
        if is_error:
            public.write_log("/**/格式的备注信息中,后面的【*/】找不到，请检查！", tip='ERROR')


        sql_result = ''.join(no_cmt_sql)

        tmp_result = re.sub('(\r\n)+', '\r\n', sql_result)
        self.result = re.sub('(\n)+', '\n', tmp_result)



        # self.finish_times = finish_times
        # if SAAS_flag:
        #     public.write_log("备注类型为【/**/】,没有匹配到后面的*/, 请检查！", tip='ERROR')

    def get_result(self, display=False):
        public.write_log("格式化后的SQL：【{}】".format(r))
        return self.result




sql_text = """
/*

*/
select * from A
/*备注信息*/
over
-- \n




aaa


select * from B
"""
sql_ins = SQL(sql=sql_text)


sql_ins.remove_comment()

r = sql_ins.get_result()
print("格式化的内容：【{}】".format(r))















