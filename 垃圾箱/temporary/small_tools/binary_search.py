# encodint:utf-8
# 通过二分法的方式，找出两张表中id的不同
#
import sys
if sys.platform == 'linux':
    sys.path.append("/opt/module/kettle/etl_job/demo/T_60/python/share_function")
import public
import re
import math
import time


class Compare:
    def __init__(self, source_jndi, source_table_name, target_jndi, target_table_name,diff_value):

        self.source_jndi = source_jndi
        self.target_jndi = target_jndi

        self.source_table_name = source_table_name
        self.target_table_name = target_table_name

        self.diff_value = diff_value


        self.counter = 1

        #### 多次连接使用下面的方式  ####
        # 获得配置信息
        self.source_db = None
        self.source_cur = None
        try:
            dbconfig = public.config_file(dbFile=source_jndi)
            self.source_db = public.getdbconnect(dbconfig)
            self.source_cur = self.source_db.cursor()
        except Exception as err:
            public.write_log(err, tip='ERROR')

            self.target_db = None
        self.target_cur = None
        try:
            dbconfig = public.config_file(dbFile=target_jndi)
            self.target_db = public.getdbconnect(dbconfig)
            self.target_cur = self.target_db.cursor()
        except Exception as err:
            public.write_log(err, tip='ERROR')

        public.write_log("源库和目标库连接成功！")


    # 先获得目标表的信息
    def get_target_id(self):
        sql = """
        select min(id) min_id, max(id) max_id, count(1) cnt
        from {}""".format(self.target_table_name)

        # 获得游标
        self.target_cur.execute(sql)
        sql_result = self.target_cur.fetchall()
        # print(sql_result)

        (min_id, max_id, cnt) = (None, None, None)
        if sql_result:
            (min_id, max_id, cnt) = sql_result[0]
        else:
            self.close()
            public.write_log("get_target_id函数：获取结果为空！", tip='ERROR')

        return  (min_id, max_id, cnt)



    # 通过最大ID和最小ID获得范围内的记录数
    def get_cnt(self, jndi_name, table_name, left_value, right_value):
        sql = """
        select count(1) cnt from {}
        where id between {} and {}
        """.format(table_name, left_value, right_value)

        # 执行命令
        if jndi_name == self.source_jndi:
            # public.write_log("执行源表sql:{}".format(sql))
            self.source_cur.execute(sql)
            sql_result = self.source_cur.fetchall()
        elif jndi_name == self.target_jndi:
            # public.write_log("执行目标表sql:{}".format(sql))
            self.target_cur.execute(sql)
            sql_result = self.target_cur.fetchall()
        else:
            sql_result = None

        if sql_result:
            cnt = sql_result[0][0]
            # public.write_log("结果为:{}".format(cnt))
        else:
            cnt = None
            self.close()
            public.write_log("get_target_id函数：获取结果为空！", tip='ERROR')

        # print("cnt:", cnt)
        return cnt


    # #
    # def contrast(self):
    #
    #     diff_cnt_list = list()
    #     while True:
    #         (left_value, right_value, target_cnt) = self.get_target_id()
    #
    #         # 源表
    #         source_cnt = self.get_cnt(self.source_jndi, source_table_name, left_value, right_value)
    #         diff_cnt = target_cnt - source_cnt
    #
    #         # 1、target_cnt == source_cnt，检查另一侧
    #         # 2、target_cnt - source_cnt 等于上次计算的值，在当前区间进行查找
    #         # 3、另外的情况，报异常，结束程序
    #
    #         # 第一次进入
    #         if not len(diff_cnt_list):
    #             # 表示这个的值相同，不需要继续判断
    #             if diff_cnt == 0:
    #                 public.write_log("两个表的值相同，请检查是否记录数不同！", tip='ERROR')
    #             # 将这个值作为基准，保存起来；后面的值都会与前一个值比较
    #             else:
    #                 diff_cnt_list.append(diff_cnt)
    #         # 第2、3、4 ... n次判断
    #         else:
    #             # 序号换成另一侧的判断
    #             if diff_cnt == 0:


    def set_value(self):
        self.left_value = -1
        self.right_value = -1
        self.middle_value = -1
        self.parent_cnt = -1

    def contrast(self):
        # 首次
        # print("开始对比, left_value: ", self.left_value)
        # (left_value, right_value, parent_cnt) = (0, 0, 0)
        time.sleep(1)
        if self.left_value == -1:
             (self.left_value, self.right_value, self.parent_cnt) = self.get_target_id()
             # 递归调用
             self.contrast()
        else:
            self.middle_value = math.ceil((self.left_value + self.right_value) / 2)

            # 源表和目标表的条数
            # 由于ID是递增的，所有从由右向左遍历
            source_cnt = self.get_cnt(self.source_jndi, source_table_name, self.middle_value, self.right_value)
            target_cnt = self.get_cnt(self.target_jndi, target_table_name, self.middle_value, self.right_value)

            # print("source_cnt:%s, target_cnt:%s" % (source_cnt, target_cnt))
            public.write_log("当前检索ID范围:【%s~%s】, 目标表记录数:【%s】, 源表记录数:【%s】, 差值:【%s】" % (self.left_value, self.right_value, target_cnt, source_cnt, target_cnt - source_cnt))

            # 如果相等，则是另外一侧
            if source_cnt == target_cnt:
                public.write_log("开始进行二分法(另一侧);id 范围【{}, {}】".format(self.left_value, self.middle_value))

                # 递归调用
                self.left_value = self.left_value - 1
                self.right_value = self.middle_value

                self.contrast()


            # 如果差值为预期值，则继续
            elif target_cnt -  source_cnt == self.diff_value:
                public.write_log("开始进行二分法(当前侧);id 范围【{}, {}】".format(self.middle_value, self.right_value))
                # 递归调用
                self.left_value = self.middle_value
                self.contrast()
                # public.write_log("当前侧", tip='ERROR')
            else:
                self.close()
                public.write_log("""已初步确定ID不同的范围为[{}, {}]。
目前差值为{}，阈值为{}，相差：{}""".format(self.left_value, self.right_value, target_cnt-source_cnt, self.diff_value, abs(target_cnt-source_cnt - self.diff_value)), tip='WARN')
                sys.exit(0)




    # 预处理
    def pretreatment(self):
        pass


    # 关闭游标和数据库
    def close(self):
        self.source_cur.close()
        self.source_db.close()

        self.target_cur.close()
        self.target_db.close()

        public.write_log("已关闭连接！")



if __name__ == '__main__':

    # source_jndi = '227_oms_center.txt'
    # target_jndi = '5.60_SHWL_BSD.txt'
    # source_table_name = 'oms_orders_lineitem'
    # target_table_name = 'bsd_list_omscenter_oms_orders_lineitem'
    # diff_value = 29043
    source_jndi = '5.60_SHWL_BSD.txt'
    target_jndi = '5.60_SHWL_BSD.txt'
    source_table_name = 'test_20210709_source'
    target_table_name = 'test_20210709_target'
    diff_value = -1



    compare_instance = Compare(source_jndi, source_table_name, target_jndi, target_table_name,diff_value)

    compare_instance.set_value()
    compare_instance.contrast()

    compare_instance.close()















