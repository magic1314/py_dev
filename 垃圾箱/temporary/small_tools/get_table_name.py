# encoding:utf-8
import sys
import os
import re


# 获取依赖表
def get_sql_table_name(sqlResult):

    # print(666666666666, len(sqlResult))

    # 处理--的注释
    sql_content = re.sub(r'--.*\n', " ", sqlResult)


    # 处理sql
    # 忽略一种笛卡尔积的写法 select * from a,b;  这里便是b表检索不出来
    sql_content = re.sub(' +', ' ', sql_content.replace('\n', ' ').replace('\r', ' ').replace(';', ' ').replace('	', ' ').replace(')', ' ').lower()).replace('( select', '(select').replace('from ', 'from|').replace('join ', 'join|')

    # 处理/**/的注释
    # sql_content = re.sub(r'\/\*.*\*\/', "", sql_content)

    list_sql_file = sql_content.split(' ')

    list_depend_table = []
    for sqlStr in list_sql_file:
        # join|'改为只能在最前面
        # if ('join|' in sqlStr or 'from|' in sqlStr) and '|(select' not in sqlStr and 'convert.join' not in sqlStr:
        if (sqlStr.startswith('join|') or sqlStr.startswith('from|')) and '|(select' not in sqlStr and 'convert.join' not in sqlStr:
        # print(444444444, sqlStr)
        # if 'join|' in sqlStr or 'from|' in sqlStr:
        #     print(555555, sqlStr)
            depend_table_name = sqlStr[5:]
            # list_depend_table.append(depend_table_name)
            list_depend_table.append(depend_table_name)

    return list_depend_table


# 读取sql脚本的内容
def read_txt(file_name):
    if not os.path.isfile(file_name):
        print(file_name, '文件不存在')
        sys.exit(8)

    with open(file_name, 'r', encoding='utf-8') as file:
        all_sql_content = file.read()
        # print(all_sql_content)


    return all_sql_content



def removal_duplicate_and_sort(list_name):
    if list_name:
        result = list(set(list_name))
        result.sort()
        # print(66666666, result)
    else:
        print("空")
        sys.exit(6)
    return result


def change_to_str(list_name):
    return '\n'.join(list_name)





def filter_list(list_name):
    new_list = list()
    for full_table_name in list_name:

        # print(full_table_name)
        # 剔除252库中的临时表
        if full_table_name.startswith('#') :
           continue

        # 剔除子查询匹配不准确的情况
        # select * from (select * from a) t1
        if full_table_name.startswith('('):
           continue

        # 去掉[和]
        full_table_name = full_table_name.replace('[','').replace(']','')


        # 表名只有一个.；替换数仓的库名
        if full_table_name.count('.') == 1:
            (database_name, table_name) = full_table_name.split('.')
            if database_name == 'test':
                database_name = 'Test'
            elif 'shwl' in database_name:
                database_name = database_name.upper()
            else:
                pass
            full_table_name = database_name + '.' + table_name

        new_list.append(full_table_name)
    return new_list





if __name__ == '__main__':
    # file_name = r'D:\work\20-库存\t_odoostock.sql'
    while True:
        file_name = input("请输入sql脚本的位置: ")
        if file_name:
            break
        else:
            print("输入不正确!")

    result = read_txt(file_name)
    result = get_sql_table_name(result)
    result = filter_list(result)
    result = removal_duplicate_and_sort(result)

    print('-----------------------------------list of table--------------------------------------')
    print('----- 共%s张表-----' % str(len(result)))
    for i in result:
        print(i)

    #print(result)
