# encoding:utf-8
import yaml

# 读取login.yaml文件
file_name = input('请yaml文件的输入路径:')
with open(file_name, encoding='utf-8') as file:
    data = yaml.full_load(file)
    print(data)

# 返回数据类型为字典嵌套字典的格式(如下)
# {'login': {'id': 1, 'title': '正常登陆', 'url': 'http://www.baidu.com/user/login', 'method': 'POST',
#            'json': {'username': 'zhou', 'password': '123456'}, 'expected': {'status_code': 200, 'content': 'user_id'}}}

# 读取login1.yaml文件
# with open(r"login1.yaml", encoding='utf-8') as file:
#     data = yaml.full_load(file)
#     print(data)