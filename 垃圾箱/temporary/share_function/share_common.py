# -*- coding=utf-8 -*-
"""
  # 名称：自定义公共函数
  # 创建人：Magic
  # 创建日期：2021-02-10
  # 概述：该文件的目录需要加入到系统环境变量中,变量名为PYTHONPATH
  # 更新记录
    @2021-02-10 Magic：初稿完成

"""
import os
import sys
import warnings

import pymysql
from properties import Properties

warnings.filterwarnings("ignore")



#####################  基本信息  #####################

# 文件和目录的绝对路径，文件的相对路径
absolute_current_file_name = os.path.realpath(__file__)
(absolute_current_catalog, relative_current_file_name) = os.path.split(absolute_current_file_name)


# 举例子：【目录：D:\Script\share_function, 绝对文件：D:\Script\share_function\share_common.py, 相对文件share_common.py】
# print('目录：{0}, 绝对文件：{1}, 相对文件{2}'.format(absolute_current_catalog, absolute_current_file_name, relative_current_file_name))


"""
    # 函数名称： 用于测试
    # 传参说明：
       【必要】@test_content： 文本内容
    # 返回值及说明：无
    # 更新记录
        @2021-02-10 创建
"""
def echo_test(test_content):
    print("这个是测试内容", str(test_content))





"""
    # 函数名称：将文本信息加入到文件中
    # 传参说明：
       【必要】@write_content： 文本内容
       【可选】@deal_mode：处理的模式，可参考https://www.runoob.com/python3/python3-file-methods.html
                                                    常见的模式 a：只追加；w：只覆盖写入；r：只读； 
                                                                                ab wb rb：表示处理二进制文件
                                                                                添加+：表示可以同时读写某个文件
       
    # 返回值及说明：无
    # 更新记录
        @2021-02-10 创建
"""
def write_file(write_content, deal_mode='a'):



# 打印正常日志
def log(info, display=True):
    info = str(info)
    if display:
        print(info)


# 打印错误日志
def error(info):
    info = str(info)
    print(info)
    sys.exit(8)








"""
    # 函数名称:：读取字典中的配置信息(字典来源于dburl文件)
    # 传参说明：
       【必要】@[para1]：字典
    # 返回值及说明：
        @return1: mysql数据库连接
    # 更新记录
        @2021-02-10 创建
"""
def getdbconnect(dbprop: dict):
    try:
        # db = connect()
        # db.connect(**dbconfig)
        # conn = pymysql.connect(host=host,user=user,password=password,database=db,charset="utf8mb4")
        db = pymysql.connect(
           host=dbprop.get("host"),
           user=dbprop.get("user"),
           password=dbprop.get("password"),
           database=dbprop.get("database"),
           charset=dbprop.get("charset"),
           autocommit=bool(dbprop.get("autocommit", True)),
           port=int(dbprop.get("port"))
        )

    except Exception as err:
        print(err)
        # logger.error(err)
        sys.exit(1)
    return db



"""
    # 函数名称:：执行sql脚本
    # 传参说明：
       【必要】@[para1]：执行的sql语句
       【可选】@dbFile：数据库连接的文件，与本文件同级目录中
       【可选】@needResult：是否需要返回的结果,一般select语句需要
       【可选】@records：参数设置，一般用于insert插入的内容
                                                    输入内容必须是tuple
       【可选】@display：是否显示日志信息
    # 返回值及说明：
        @db: mysql数据库连接
    # 更新记录
        @2021-02-10 创建
"""
def run_sql(str_sql, dbFile='dburl_etlconfig.txt',needResult=False,records=None,display=False):
    try:
        prop = Properties(absolute_current_catalog + '/' + dbFile)
        dbconfig = prop.getpropeties()
    except Exception as err:
        print(err)
        # logger.error(err)
        sys.exit(1)

    try:
        db = getdbconnect(dbconfig)
    except Exception as err:
        print(err)
        sys.exit(2)
    #print("connect to db successfully!")

    if display:
        print("run_sql: ", str_sql)

    cur = db.cursor()
    # 有无sql执行的参数
    if records:
        cur.executemany(str_sql, records)
        print("已处理%d条数据" % len(records))
    else:
        cur.execute(str_sql)

    if needResult:
        sqlResult = cur.fetchall()

    cur.close()
    # 目前用完连接就断掉
    db.close()

    if needResult:
        return sqlResult







###########################   以下是不常用函数    ###########################
# 最好带schema
def delete_table(table_name):
    sql = """
    delete from %s
    """ % table_name
    run_sql(str_sql=sql)
    print("已清空%s表数据" % table_name)



# list表元素去重
def list_duplicate_removal(list_name):
    return list(set(list_name))






# 用于对本脚本的检测
if __name__ == '__main__':
    print("-----------------------------  separator -------------------------------------")
    # print(absolute_current_catalog)
    
    
    
    











