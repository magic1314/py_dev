# encoding:utf-8
# 执行命令: python .\DownloadMovie\getBT2.py 阿飞正传
from bs4 import BeautifulSoup
import requests
import os
import re
import sys
import urllib.parse

# bs4.element.ResultSet下面有多个bs4.element.Tag，相当于list和string的关系

# key_word = sys.argv[1]
def view_properties(content, is_full=False):
    print('------------------------------ 开始 ------------------------------------')
    print("类型: ", type(content))
    print("长度：", len(content))
    if is_full:
        print("内容：\n", content)
    print('------------------------------ 结束 ------------------------------------')


def BT(key_word):
    # url = r"https://cld83.buzz/search-' +  + r'-0-3-1.html"
    encoded_key_word = urllib.parse.quote(key_word, safe='')
    encoded_url = r"https://cld83.buzz/search-" + encoded_key_word + r"-0-3-1.html"

    # print(encoded_url)

    # sys.exit(0)

    # 发送 GET 请求
    # https://cld83.buzz/search-%E6%B3%B0%E5%9D%A6%E5%B0%BC%E5%85%8B%E5%8F%B7-0-3-1.html
    response = requests.get(encoded_url)
    href_score = list()
    # 输出网页内容
    # print(response.text)


    soup = BeautifulSoup(response.content, 'html.parser')
    # view_properties(soup, is_full=False)

    rank = 1
    s1 = soup.select('div.sbar') # 现在是个list

    for s2 in s1:
        # view_properties(s2, is_full=True)
        link = s2.find_all('a', text=re.compile('[磁力链接]'))
        # view_properties(link, is_full=True)
        if link:
            href = link[0]['href']
            # print("磁力链接", href)


        span = s2.find_all('span')
    # 类型:  <class 'bs4.element.ResultSet'>
    # 长度： 5
    # 内容：
    #  [<span><a href="magnet:?xt=urn:btih:4051F235E8D412109DC522BD4EEF1288513902DA" target="_blank">[磁力链接]</a></span>
    # , <span>添加时间:<b>2020-06-28</b></span>
    # , <span>大小:<b class="cpill yellow-pill">26.0 GB</b></span>
    # , <span>最近下载:<b>2023-04-25</b></span>
    # , <span>热度:<b>2429</b></span>]


        # view_properties(span, is_full=True)
        
        for i in span:
            # view_properties(i.get_text(), is_full=True)

            value = i.get_text().strip()
            # view_properties(value, is_full=True)

            if value:
                if '添加时间:' in value:
                    add_time = value.split(':')[-1]
                if '大小:' in value:
                    size = value.split(':')[-1]
                if '最近下载:' in value:
                    last_download_time = value.split(':')[-1] 
                if '热度:' in value:
                    heat = value.split(':')[-1]
        
        line = {
            'href': href,
            'add_time': add_time,
            'size': size,
            'last_download_time': last_download_time,
            'heat': heat,
        }

        # print("所有的关键信息输出结果：", line)
        # print(line['size'])

        if 'GB' in line['size']:
            size_decimal = line['size'].replace('GB','').strip()
            # view_properties(size_decimal)

            size_gb = 0
            try:
                size_gb = float(size_decimal)
            except ValueError:
                print('无法将字符串转换成小数')
        else:
            size_gb = 0
        
        # print(rank, size_gb)

        score1 = 100 - (rank-1) * 10
        if size_gb == 0:
            score2 = 0
        elif size_gb >= 0 and size_gb <= 3:
            score2 = 50
        elif size_gb > 3 and size_gb <= 4:
            score2 = 60
        elif size_gb > 4 and size_gb <= 5:
            score2 = 90
        elif size_gb > 5 and size_gb <= 10:
            score2 = 100
        elif size_gb > 10 and size_gb <= 15:
            score2 = 90
        elif size_gb > 15 and size_gb <= 21:
            score2 = 80
        elif size_gb > 21:
            score2 = 50
        else:
            score2 = 0


        score = score1 * 0.4 + score2 * 0.6
        # print("链接:{} 分数:{} 文件大小:{} 排名:{}".format(href, score,size_gb,rank))

        href_score.append((href, score,size_gb,heat))

        rank = rank + 1
        
    # print(href_score)


    # 找到最大数字及其对应的第一个值
    # print(href_score)
    # if href_score:
    max_item = max(href_score, key=lambda x: x[1])
    max_score, max_score_href, size_gb, heat = max_item[1], max_item[0], max_item[2], max_item[3]

    # print('----------', max_score, max_score_href)
    # print(score, score1, score2)
    # print("大小：", size_gb)

    # else:
    #     print("href_score为None")
    #     max_score = 0
    #     max_score_href = 'null'

    # print("结果：",max_score_href, max_score)

    # if not size_gb:
    #     size_gb = 0
    
    # if not heat:
    #     heat = 0


    # 将文本写入到txt文件中
    with open('result.txt', "a", encoding='utf-8') as f:
        f.write('{}\t{}\t{}\t{}\t{}\n'.format(key_word,max_score_href,max_score,size_gb,heat))
    

    # print("执行完成")


if __name__ == '__main__':
    BT('小鞋子')