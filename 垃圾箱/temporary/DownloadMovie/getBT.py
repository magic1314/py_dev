# encoding:utf-8
# 执行命令: python .\DownloadMovie\getBT.py
from bs4 import BeautifulSoup
import requests
import os
import re
import sys

# 发送 GET 请求
response = requests.get("https://cld83.buzz/search-%E6%B3%B0%E5%9D%A6%E5%B0%BC%E5%85%8B%E5%8F%B7-0-3-1.html")

# 输出网页内容
# print(response.text)

soup = BeautifulSoup(response.content, 'html.parser')


# 查找第一个<a>标签
link = soup.find('a')
print(5555, type(link))

# 查找所有<a>标签
links = soup.find_all('a')

# 查找class为'example'的所有<div>标签
divs = soup.find_all('div', {'class': 'example'})

# 获取<a>标签的href属性
href = link['href']
print(66666, type(href))


# 获取<a>标签的文本内容
text = link.get_text()

sys.exit(0)

# 查找所有class属性为"tbox"的div标签
# tbox_divs = soup.find_all('div', {'class': 'tbox'})
tbox_divs = soup.select('div.tbox')
# BeautifulSoup获取<div calss="tbox">...</div>的内容，剔除其中<div calss="tbox sort-box">...</div>的内容
# tbox_divs = soup.select('div.tbox:not(.sort-box)')



def deal_content(text):
    for line in text.splitlines():
        line = line.strip()
        # 空白行，过滤掉
        if not line:
            continue
        
        # print(line)



# 遍历所有找到的div标签并输出其文本内容
# if os.path.exists('output.txt'):
#     os.remove('output.txt')

i = 1
for tbox_div in tbox_divs:
    # print((tbox_div.get_text()))
    if i == 1:
        i = i + 1
        continue

    # with open('output.txt', 'a', encoding='utf-8') as f:
    #     f.write(tbox_div.get_text())

    # 链接
    link = tbox_div.find_all('a', text=re.compile('[磁力链接]'))
    if link:
        # print(link)
        # print(type(link))
        len_link = len(link)
        for j in link:
            print(j)
    
    # print(tbox_div.get_text())
    # title = tbox_div.find_all('div', {'class': 'title'})
    title = tbox_div.select('div.title')
    len_title = len(title)
    print("标题个数：", len_title)
    # title_text = title.get_text()
    # deal_content(title_text)
    # print(title)
    print(type(title)) # <class 'bs4.element.ResultSet'>
    for k in title:
        # print(1111, k,1111)
        # print(222222, k.get_text().strip(), 222222)
        pass

    # 这个是具体的下载内容清单
    # 通过<ul><li>内容1</li><li>内容2</li></ul>
    slist = tbox_div.select('div.slist')
    # print(5555, type(slist))
    print(66666, len(slist))

    for s1 in slist:
        # print(7777, type(s1), s1)  # <class 'bs4.element.Tag'>

        # 每一项的具体内容的list
        s2 = s1.select('li')
        print(8888, type(s2), len(s2), s2)  # 8888 <class 'list'> 2

        for s3 in s2:
            print('--------------------------------------------------------')
            print(9999, type(s3), len(s3), s3) # 9999 <class 'bs4.element.Tag'> 3
            print(111111, s3.get_text())





    # for slist_ul in slist.find_all('ul'):
    #     for slist_li in slist_ul.find_all('li'):
    #         print(333, slist_li, 333)
