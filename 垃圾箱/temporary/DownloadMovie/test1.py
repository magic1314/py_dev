# encoding:utf-8
from bs4 import BeautifulSoup

html_doc = """
<html>
<head>
<title>Example Page</title>
</head>
<body>
<div class='tboby'>
<ul>
  <li>Item 1.1</li>
  <li>Item 1.2</li>
  <li>Item 1.3</li>
</ul>
</div>
<ul>
  <li>Item 2.1</li>
  <li>Item 2.2</li>
</ul>
<ul>
  <li>Item 1.4</li>
  <li>Item 1.5</li>
</ul>
</body>
</html>
"""

soup = BeautifulSoup(html_doc, 'html.parser')

# 使用CSS选择器查找所有class属性为"list1"的ul标签，并遍历它们
for list_div in soup.select('div.tboby'):   #  <class 'bs4.element.Tag'>
    for list1_ul in list_div.select('ul'):
        print("List 1:")
        # 在每个list1_ul标签下，查找所有li标签并遍历它们
        for li in list1_ul.find_all('li'):
            print(li.get_text())

print(soup, type(soup))
print(list_div,type(list_div))
print(list1_ul,type(list1_ul))
# # 使用CSS选择器查找所有class属性为"list2"的ul标签，并遍历它们
# for list2_ul in soup.select('ul.list2'):
#     print("List 2:")
#     # 在每个list2_ul标签下，查找所有li标签并遍历它们
#     for li in list2_ul.find_all('li'):
#         print(li.get_text())