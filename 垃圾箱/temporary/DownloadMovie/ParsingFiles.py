# encoding:utf-8
# 执行命令：python .\DownloadMovie\ParsingFiles.py
import os
import getBT2

# print("ok")

create_catalog = r'D:\MOVIES\豆瓣TOP250'


# with open('result.txt', "w", encoding='utf-8') as f:
#     f.write('{}\t{}\t{}\t{}\t{}\n'.format('电影名称','磁力链接','评分','大小(GB)','热度'))


with open('movie.txt', "w", encoding='utf-8') as f:
    f.write('{}\t{}\t{}\n'.format('序号','电影名称','目录名称'))



i = 1
with open(r'DownloadMovie\电影清单.txt', encoding='utf-8') as file:
    for f in file.readlines():
        f = f.strip()
        # 空白行，过滤掉
        if not f:
            continue
        
        # 第一个字符不是数字，过滤掉
        if not f[0].isdigit():
            continue

        # 第二个字符为.，过滤掉
        if len(f) > 1 and f[1] == '.':
            continue
                
        # 第二个字符为.，过滤掉
        if len(f) >= 3 and f[0:4].isdigit() and f[0:4] >= '1900' and f[0:4] <= '2099':
            continue
        
        # 特殊处理
        if '4个臭皮匠顶个诸葛亮' in f or '10年的完美句点。' in f or '囚房的礼物' in f:
            continue
        
        # print(i, f)
        # if '7号' in f:
        #     print(f)

        seq_id = i
        movie_name = f[len(str(i)):]

        seq_num = '0' * (3 - len(str(i))) + str(i)
        
        # print(seq_num, movie_name)
        
        # if '哈利·波特与死亡圣器' in movie_name:
        #     i = i + 1
        #     print('----跳过下载-----')
        #     continue
        # getBT2.BT(movie_name)

        movie_catalog = seq_num + '_' + movie_name

        with open('movie.txt', "a", encoding='utf-8') as f:
            f.write('{}\t{}\t{}\n'.format(seq_num,movie_name,movie_catalog))

        # print(movie_catalog)

        movie_catalog_abs = create_catalog + os.path.sep + movie_catalog
        # print(movie_catalog_abs)

        if not os.path.isdir(movie_catalog_abs):
            os.mkdir(movie_catalog_abs)
            print(movie_catalog_abs, '已创建！')



        i = i + 1


print('Over!')