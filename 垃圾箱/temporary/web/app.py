from flask import Flask, request, render_template
import mysql.connector

app = Flask(__name__, static_folder='static', static_url_path='/scripts')

@app.route('/', methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        # 获取用户输入的 SQL 语句
        sql = request.form['sql']

        # 连接 MySQL 数据库
        conn = mysql.connector.connect(
            host='localhost',
            user='root',
            password='123456',
            database='django'
        )
        cursor = conn.cursor()

        # 执行 SQL 查询
        cursor.execute(sql)
        results = cursor.fetchall()

        # 关闭数据库连接
        cursor.close()
        conn.close()

        # 将查询结果返回给用户
        return render_template('result.html', results=results)
    else:
        return render_template('index2.html')

if __name__ == '__main__':
    app.run(debug=True)