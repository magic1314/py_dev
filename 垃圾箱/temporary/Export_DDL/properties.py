#!/usr/bin/python
# -*- coding: UTF-8 -*-

class Properties(object):
    def __init__(self, filename,encoding=None):
        self.__filename = filename
        if encoding is None:
            self.__encoding = "utf-8"
        else:
            self.__encoding = encoding
    def getpropeties(self):
        properties = {}
        #print(self.__filename)
        #print(self.__encoding)
        with open(file=self.__filename,mode='r',encoding=self.__encoding) as p_file:
            #print(p_file)
            for line in p_file:
                #print(line)
                tmp = line.strip()
                if not tmp.startswith("#"):
                    # print(tmp)
                    pos = tmp.find("=")
                    if pos > 0:
                        key = tmp[0:pos]
                        val = tmp[pos + 1:]
                        properties[key] = val

        return properties


