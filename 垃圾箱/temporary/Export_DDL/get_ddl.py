﻿#!/usr/bin/python
# -*- coding: UTF-8 -*-
import sys
import os
import time
from GBaseConnector import connect,GBaseError
from properties import Properties


now_date = time.strftime('%Y%m%d', time.localtime())
print("程序名称：" + sys.argv[0])


if len(sys.argv) < 2:
	print("无参数，退出")
	sys.exit(0)
else:
	ddl_type = sys.argv[1]
	FILENAME="./sql_temp/result_ddl_" + ddl_type + "_" + now_date + '.sql'
	print("参数: ", ddl_type)

# 删除文件的函数
def del_file(file_name):
	if(os.path.exists(file_name)):
		os.remove(file_name)

# 文件中写数据的函数
def write_txt(file_name, write_type, content, ending='\n'):
	with open(file_name, write_type) as f:
		f.write(str(content) + ending)


# 运行sql，获得ddl语句的函数
def run_sql(sql_string):
	# 排序处理
	sql_string = sql_string + " order by table_name asc"
	# 保存ddl语句
	drop_table_ddl_list = list()
	create_ddl_list = list()
	
	# 清理结果文件
	del_file(FILENAME)
	
	try:
		# 获得了一个字典(dbconfig)
		prop = Properties("dburl.txt")
		dbconfig = prop.getpropeties()
		print("[INFO] 已读取gbase配置文件")
	except Exception as err:
		print(err)
		sys.exit(-99)
	
	
	try:
		db = connect()
		#connection = db.connect(**dbconfig)
		connection = db.connect(host='22.188.9.139', port=5258, user='gb_a101_bat', password='gb_a101_bat', db='gb_a101_data', charset='utf8')
		print("[INFO] 已连接到gbase库")
	except Exception as err:
		print("[ERROR] 连接数据库失败" + str(err))
		sys.exit(-99)

	# 添加删除语句
	sql_string_drop_table_ddl = """ select 'DROP TABLE IF EXISTS '||UPPER(table_name)||';' from """ + sql_string
	print("[INFO] 执行SQL: " + sql_string_drop_table_ddl)
	cur = db.cursor()
	cur.execute(sql_string_drop_table_ddl)
	# 获得sql的结果
	list_set = cur.fetchall()
	for m in list_set:
		drop_table_ddl_list.append(m[0])
		
		
	#sql_string = """information_schema.tables where table_name like 'G_%' and substring(table_name,2,1)='_'"""
	sql_string_ddl = """ select 'show create table '||table_name||';' sql_str from """ + sql_string
	
	print("[INFO] 执行SQL: " + sql_string_ddl)
	cur = db.cursor()
	cur.execute(sql_string_ddl)
	# 获得sql的结果
	list_set = cur.fetchall()
	for i in list_set:
		#print(i[0])
		create_ddl_list.append(i[0])
	
	print("[INFO] 获取DDL语句")
	for j in range(len(create_ddl_list)):
		str_string_ddl = create_ddl_list[j]
		print((' ' + str(j))[-2:] + ") "+ str_string_ddl)
		cur = db.cursor()
		cur.execute(str_string_ddl)
		# show create table出来的内容，只会有一行数据
		create_ddl_result_list = cur.fetchall()
		
		
		# 先把drop table if exists 语句加入进去
		write_txt(FILENAME, 'a', drop_table_ddl_list[j], ending='\n')
		
		# 加入show出来的ddl语句
		for k in range(len(create_ddl_result_list)):
			write_txt(FILENAME, 'a', create_ddl_result_list[k][1], ending='\n;\n\n')
	
	print("[INFO] 已写入到文件: " + FILENAME)

	db.close()
	return

if __name__ == '__main__':
	sql_string = str()
	if( ddl_type == 'code' ):
		sql_string = """information_schema.tables where table_name like 'CODE_%' and substring(table_name,5,1)='_' and table_name not like '%2020%'"""
	else:
		print("参数错误！")
		sys.exit(-99)
	# 开始执行对应的sql语句
	run_sql(sql_string)




























