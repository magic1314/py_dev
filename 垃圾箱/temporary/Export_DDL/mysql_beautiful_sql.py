﻿#!/usr/bin/python
# -*- coding: UTF-8 -*-
import sys
import os
import time
import re
"""
# 传入参数：BSD
# 
"""


# 20200707

now_date = time.strftime('%Y%m%d', time.localtime())
print("程序名称：" + sys.argv[0])


if len(sys.argv) == 1:
	print("无参数，错误")
	sys.exit(2)



stage_name = sys.argv[1]
cwd = os.path.split(os.path.realpath(__file__))[0]
sql_temp_dir = cwd + os.path.sep + 'sql_temp'
sql_formal_dir = cwd + os.path.sep + 'sql_formal'
error_log_file = cwd + os.path.sep + 'log' + os.path.sep + os.path.basename(sys.argv[0]).split('.')[0] + '-' + now_date + '-error.log.'
len_base_column = 45


# 文件中写数据的函数
def write_txt(file_name, write_type, content, ending='\n'):
	with open(file_name, write_type) as f:
		f.write(str(content) + ending)

# 打印正常日志
def log(info, display=True):
	info = str(info)
	if display:
		print(info)
	write_txt(error_log_file, 'w', info, ending='\n')


# 打印错误日志
def error(info):
	info = str(info)
	print(info)
	logger.error(info)
	sys.exit(8)


# 删除文件
def del_file(file_name):
	if os.path.isfile(file_name):
		os.remove(file_name)



# 获得文件列表
def execute_sql():
	for home, dirs, files in os.walk(sql_temp_dir):
		for filename in files:
			sql_temp_fullname = os.path.join(sql_temp_dir, filename)
			# 只处理当前日期的文件
			if now_date not in filename or 'result_ddl_' + stage_name not in filename:
				continue

			log("待处理的文件: {0}".format(sql_temp_fullname))
			sql_formal_fullname = os.path.join(sql_formal_dir, filename)
			# 将temp文件中的所有内容，读到内存中解析
			with open(sql_temp_fullname) as file:
				file_content = file.readlines()
			
			# 处理源sql数据，重新加载到新文件中
			del_file(sql_formal_fullname)
			print("正在处理文件: ", filename)
			for line in file_content:
				with open(sql_formal_fullname, 'a') as new_file:
					# print("旧：", line)
					# 剔除没有用的部分
					# line = line.replace('`', '')
					# line = line.replace(' DEFAULT NULL','').replace(""" ENGINE=InnoDB DEFAULT '""", '')
					line = line.replace(' DEFAULT NULL','')
					# line = line.replace("""ENGINE=EXPRESS REPLICATED  DEFAULT CHARSET=utf8 TABLESPACE='sys_tablespace'""", 'CHARSET=utf8').strip(" ")
					# EDP特殊处理(规范化表名)
					# if 'edp' in filename and 'TEST_SDF_EDP_DDL'.lower() in line:
					# 	line = line.replace('TEST_SDF_EDP_DDL_'.lower(), '')
					
					# 统一转换成大写、规范int(11)
					line = line.replace('int(11)', 'int')

					# 转成小写
					if line.startswith('CREATE TABLE'):
						line = line.replace('CREATE TABLE', 'create table')
						line = line.replace('`', '')
					
					# 留空白格处理
					# 不处理的部分
					if line.startswith(')') or line.startswith('create table') or line.startswith('drop table') or not line:
						pass
					else:
						#print("需要处理", line)
						line = re.sub(' +', ' ', line).lstrip()
						lines_list = line.split(' ')
						# print(6666, lines_list, len(lines_list))
						lines_list_count = len(lines_list)
						if lines_list_count >=2 :
							column_name = lines_list[0]
							# print("7777: ", column_name)
							column_data_type = lines_list[1]
							# 采用gbk编码，是由于gbk占用两个字节,符合当前格式的要求
							len_column_name = len(column_name.encode('gbk'))
							if len_base_column <= len_column_name:
								print("错误，长度不够; 需要调整len_base_column的值!")
								print("column_name:", column_name, ", 长度为:", len_column_name)
								sys.exit(-98)
							
							many_space = ''
							if lines_list_count >= 3:
								# 如果PRIMARY                                      KEY              (`ID`)， KEY后面应该和(连接起来,不需要空白格
								if lines_list[2].startswith('('):
									pass
								# `FID`                                        int              NOT NULL， 后面留一个空格
								elif lines_list[2]  == 'NOT' and lines_list[3] == 'NULL':
									# print(lines_list)
									lines_list[2] = 'not'
									lines_list[3] = 'null'
									many_space = ' '
								else:
									many_space = ' '*(17-len(column_data_type))
							elif lines_list_count == 2:
								many_space = ''
							else:
								print("错误！ 不可能事件")
								sys.exit(-99)
							#print("start_str: ", start_str)
							
							start_str = ' '*2 + column_name + ' '*(len_base_column-len_column_name) + column_data_type + many_space
							

							line = start_str + ' '.join(lines_list[2:])
					# print(line)

					if line.startswith(';'):
						line += '\n'
					# print("新:", line)
					new_file.write(line)
			

			
if __name__ == '__main__':
	execute_sql()
	print("格式化sql成功！")



























