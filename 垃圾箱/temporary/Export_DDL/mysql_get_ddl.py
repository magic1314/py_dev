﻿#!/usr/bin/python
# -*- coding: UTF-8 -*-
import sys
import os
import time
import pymysql
from properties import Properties

now_date = time.strftime('%Y%m%d', time.localtime())
print("程序名称：" + sys.argv[0])

cwd = os.path.split(os.path.realpath(__file__))[0]
error_log_file = cwd + os.path.sep + 'log' + os.path.sep + os.path.basename(sys.argv[0]).split('.')[0] + '-' + now_date + '-error.log.'

if len(sys.argv) < 2:
	print("无参数，退出")
	sys.exit(0)
else:
	ddl_type = sys.argv[1]
	FILENAME = cwd + os.path.sep + "sql_temp" + os.path.sep + "result_ddl_" + ddl_type + "_" + now_date + '.sql'
	print("传入参数: {0}, FILENAME={1}".format(ddl_type, FILENAME))


# 删除文件的函数
def del_file(file_name):
	if (os.path.exists(file_name)):
		os.remove(file_name)


# 文件中写数据的函数
def write_txt(file_name, write_type, content, ending='\n'):
	with open(file_name, write_type) as f:
		f.write(str(content) + ending)


# 打印正常日志
def log(info, display=True):
	info = str(info)
	if display:
		print(info)
	write_txt(error_log_file, 'w', info, ending='\n')


# 打印错误日志
def error(info):
	info = str(info)
	print(info)
	logger.error(info)
	sys.exit(8)


# 连接数据库
def getdbconnect(dbprop: dict):
	try:
		# db = connect()
		# db.connect(**dbconfig)
		# conn = pymysql.connect(host=host,user=user,password=password,database=db,charset="utf8mb4")
		db = pymysql.connect(
			host=dbprop.get("host"),
			user=dbprop.get("user"),
			password=dbprop.get("password"),
			database=dbprop.get("database"),
			charset=dbprop.get("charset"),
			autocommit=bool(dbprop.get("autocommit", True)),
			port=int(dbprop.get("port"))
		)

	except Exception as err:
		error(err)
	return db


# 运行sql，获得ddl语句的函数
def execute_sql(sql_string):
	# 保存ddl语句
	drop_table_ddl_list = list()
	create_ddl_list = list()

	# 清理结果文件
	del_file(FILENAME)

	# 获取配置信息内容
	try:
		prop = Properties(cwd + os.path.sep + 'dburl_BSD.txt')
		dbconfig = prop.getpropeties()
	except Exception as err:
		error(err)

	try:
		db = getdbconnect(dbconfig)
	except Exception as err:
		error(err)
	log("connect to db successfully!")

	# 添加删除语句
	sql_string_drop_table_ddl = """ select concat('drop table if exists ',table_name,';') delete_sql from """ + sql_string
	print("[INFO] 执行SQL: " + sql_string_drop_table_ddl)
	cur = db.cursor()
	cur.execute(sql_string_drop_table_ddl)
	# 获得sql的结果
	list_set = cur.fetchall()
	for m in list_set:
		drop_table_ddl_list.append(m[0])
	# print(5555555555, drop_table_ddl_list)

	# sql_string = """information_schema.tables where table_name like 'G_%' and substring(table_name,2,1)='_'"""
	# 获取所有字段
	sql_string_ddl = """ select concat('show create table ',table_schema,'.', table_name, ';') create_sql from """ + sql_string
	# print(sql_string_ddl)

	print("[INFO] 执行SQL: " + sql_string_ddl)
	cur = db.cursor()
	cur.execute(sql_string_ddl)
	# 获得sql的结果
	list_set = cur.fetchall()
	cur.close()
	for i in list_set:
		# print(i[0])
		create_ddl_list.append(i[0])

	log("[INFO] 获取DDL语句, 共%d张表" % len(create_ddl_list) )
	# print(create_ddl_list)
	# sys.exit(0)

	for j in range(len(create_ddl_list)):
		str_string_ddl = create_ddl_list[j]
		# print((' ' + str(j))[-2:] + ") " + str_string_ddl)
		cur = db.cursor()
		cur.execute(str_string_ddl)
		# show create table出来的内容，只会有一行数据
		create_ddl_result_list = cur.fetchall()
		cur.close()

		# # 先把drop table if exists 语句加入进去
		write_txt(FILENAME, 'a', drop_table_ddl_list[j], ending='\n')
		# # 加入show出来的ddl语句
		for k in range(len(create_ddl_result_list)):
			write_txt(FILENAME, 'a', create_ddl_result_list[k][1], ending='\n;\n\n')

	print("[INFO] 已写入到文件: " + FILENAME)

	db.close()
	return


if __name__ == '__main__':
	sql_string = str()
	if (ddl_type == 'BSD'):
		sql_string = """
		information_schema.tables
		where table_schema = 'SHWL_BSD'
		and table_name not like '%202%'
		and table_type = 'BASE TABLE'
		order by table_name
	"""
	else:
		print("输入参数错误！")
		sys.exit(88)
	# 开始执行对应的sql语句
	execute_sql(sql_string)