CREATE TABLE `data_dump_conf` (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_no` int DEFAULT NULL,
  `database_name` varchar(20) DEFAULT NULL,
  `table_name` varchar(50) DEFAULT NULL,
  `where_clause` varchar(255) DEFAULT NULL,
  `save_catalog_windows` varchar(100) DEFAULT NULL,
  `save_catalog_linux` varchar(100) DEFAULT NULL,
  `jndi_name` varchar(50) DEFAULT NULL,
  `imp_exp_flag` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `enable_status` varchar(1) DEFAULT NULL,
  `table_type` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
;
INSERT INTO `django`.`data_dump_conf` (`id`, `order_no`, `database_name`, `table_name`, `where_clause`, `save_catalog_windows`, `save_catalog_linux`, `jndi_name`, `imp_exp_flag`, `enable_status`, `table_type`) VALUES (1, NULL, 'etl_config', 'test_001', '', 'D:\\work\\测试', NULL, 'etl_config', 'exp', '1', 'config');


CREATE TABLE `data_dump_log` (
  `id` int NOT NULL AUTO_INCREMENT,
  `data_date` date DEFAULT NULL,
  `database_name` varchar(20) DEFAULT NULL,
  `table_name` varchar(50) DEFAULT NULL,
  `export_file_name` varchar(100) DEFAULT NULL,
  `file_size` varchar(20) DEFAULT NULL,
  `row_count` int DEFAULT NULL,
  `imp_exp_flag` varchar(5) DEFAULT NULL,
  `platform_name` varchar(20) DEFAULT NULL,
  `save_catalog` varchar(100) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='文件导出日志表'
;

CREATE TABLE `test_001` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
;

