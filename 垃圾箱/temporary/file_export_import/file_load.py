# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：table_load.py
# 作者：石大峰
# 版本：v1.2
# 创建时间：20230321
# 功能简述：将数据文件导入到mysql库
# 执行命令：python file_export_import\file_load.py  target_jndi=etl_config file_name=D:\work\测试\20230322_01_sys_user_20230322_01.dat (文件的相对或绝对路径，支持*通配符)
# 指令：python table_load.py xx
# 应用平台：window10、linux
# 注：谨慎操作；导入表数据之前，需要备份原有表的数据
# target_jndi参数为目标库的jndi
#######################################################################################
# 更新记录
# 2023-03-21: 从之前的代码中迁移
# 2023-03-22: 将代码测试运行，没有问题
# 2023-03-23: 这里只用于导入文件
#######################################################################################
import sys
import pymysql
import time
import os
import glob
from concurrent.futures import ProcessPoolExecutor
from warnings import filterwarnings
filterwarnings("ignore",category=pymysql.Warning)
# if sys.platform == 'linux':
#     sys.path.append("/home/dev_admin/script/share_function")
import common


# export_file_name = r'20210817-etl_dingding_at_person.gz'
now_date = common.get_parameter('now_date')
py_file_name = os.path.realpath(__file__)


FIELD_SEPERATOR = "!|"

# 判断windows和linux系统
if sys.platform == 'win32':
    platform_name = 'windows'
elif sys.platform == 'linux':
    platform_name = 'linux'
else:
    platform_name = None
    common.write_log('当前【%s】系统不支持！！' % sys.platform)

# 获得传入的参数
def get_parameter(para_list):
    para_dic = dict()


    # 目前只需要etl_id这一个参数
    key = None
    value = None
    for i in sys.argv:
        if '=' in i:
            key = i.split('=')[0]
            value = i.split('=')[1]

            para_dic[key] = value

    if not para_dic:
        common.write_log("没有获得参数", tip='ERROR')

    return para_dic



def loaddata(jndi:str, datafile:str, fiedlsep:str=FIELD_SEPERATOR):
    try:
        common.write_log("{}文件开始导入".format(datafile))
        starttime: str = time.strftime("%Y%m%d%H%M%S", time.localtime(time.time()))
        if not os.path.isfile(datafile):
            print("File name not exists")
            common.write_log("File name not exists", tip='ERROR')
            return None

        # db = getdbconnect(dbprop)

        # sys.exit(1)
        # 数据文件的格式：20210817-etl_dingding_at_person.gz


        
        pos = datafile.rfind(os.sep)
        if pos != -1:
            tmp = datafile[pos + 1:]
            save_catalog = datafile[:pos]
        else:
            tmp = datafile
            save_catalog = '.'



        pos = tmp.find(".dat")
        if pos != -1:
            tmp = tmp[0:pos]
        
        data_dt = tmp[0:8]
        # print(data_dt)
        table_name = tmp[8+1+3:-3]



        # print(tablename)
        common.write_log("已获取数据文件的信息：\n\t\t\t### file_name:【{}】 data_date:【{}】,tablename:【{}】".format(datafile, data_dt, table_name))


        selectsql = """select count(1) cnt from {}
        """.format(table_name)
        result = common.run_sql(sql_txt=selectsql, dbFile=jndi + '.txt', needResult=True, executemany=False, parameter_list=None,
                       display=False)

        # print(result,type(result))

        if not result:
            common.write_log("没有记录(正常不出该结果)！", tip="ERROR", write_flag=True, display=True)
        cnt = result[0][0]
        if cnt > 0:
            common.write_log("{}表存在{}条记录，默认更新方式为全删全插！".format(table_name, cnt), tip='WARN', write_flag=True, display=True)
            # 清空之前表的数据
            truncate_sql = """truncate %s""" % table_name
            common.run_sql(sql_txt=truncate_sql, dbFile=jndi + '.txt', needResult=False, executemany=False, parameter_list=None,
                        display=False)
            # print(66666, '已清表空数据')

        # sys.exit(99)


        sql = """select column_name ,upper(data_type) data_type  
        from information_schema.columns  
        where table_name = '%s' """ % table_name
        result = common.run_sql(sql_txt=sql,dbFile=jndi + '.txt',needResult=True,executemany=False,parameter_list=None,display=True)
        col_types = {}
        for data_row in result:
            col_types.setdefault(data_row[0],data_row[1])
        # print(999999999, type(col_types), col_types)
        # sys.exit(88)
        
        linenum = 0

        common.write_log("@@@ 开始导入{}表的数据".format(table_name), tip='INFO', write_flag=True, display=True)

        # sys.exit(55)

        with open(datafile,"rb") as file:
            # g = gzip.GzipFile(fileobj=fp,mode="rb")
            # 第一行的数据
            # [b'id\x01person_name\x01phone_number\x01creator\x01update_time\x01remark\n']
            b = file.readlines(1)
            # print(b, type(b), len(b))
            # sys.exit(77)
            i = 0
            v=[]
            # 轮循第一行的字段
            while len(b) == 1:
                linenum = linenum + 1
                s = b[0].decode().rstrip()
                # 具体的数据
                # idperson_namephone_numbercreatorupdate_timeremark
                # 1石大峰18255624253magic20210511095208
                # 2陶开15021015412magic20210511095530#
                # print(133333, s)
                dats = s.split(fiedlsep)
                # print(777777777, dats)
                # 第一行 字段头的处理
                if linenum == 1:
                    sql = "insert into " + table_name + "( " + ",".join(dats) + ") "
                    vals = "values("
                    for i, col in enumerate(dats):
                        # print("col: " + col)
                        # if col.strip() == 'UPDATE_TIME':
                        #     continue
                        col_type = col_types.get(col)
                        """if col_type == "TIME":
                            vals = vals + "TIME(%s)"
                        elif col_type == "TIMESTAMP":
                            vals = vals + "TO_DATE(%s,'YYYYMMDDHHMISS')"
                        elif col_type == "DATETIME":
                            vals = vals + "TO_DATE(%s,'YYYYMMDDHHMISS')"
                        elif col_type == "DATE":
                            vals = vals + "TO_DATE(%s,'YYYYMMDD')"
                        else:"""
                        # if col_type == 'BIT':


                        vals = vals + "%s"

                        vals = vals + ","
                        # print(5555555, vals)

                    vals = vals.rstrip(",") + ")"

                    sql = sql + vals
                else:
                    # 插入的数据部分
                    # print(6666666, dats)
                    # print(5555555, col_types)
                    # dats = [x if len(x) > 0 else None for x in dats]
                    # print(777777, dats)

                    
                    # 总字段个数
                    col_name_list = enumerate(list(col_types.values()))
                    # print(8888888, len(col_name_list), len(dats))
                    tmp = list()
                    for x,column_name in col_name_list:
                        # print(566666555, len_col, x)
                        # 分别得到字段名和字段的值
                        # print(col_types)
                        column_value = dats[x]
                        # print(66666, column_name, column_value)

                        if not column_value:
                            column_value = None
                            # 如果是二进制类型，置为数值类型的数据
                        if column_name == 'BIT' and column_name:
                            column_value = int(column_value)
                        tmp.append(column_value)
                    
                    dats = tmp
                    # print(dats)

                    v.append(tuple(dats))
                    i = i + 1
                    if i >= 2000:
                        try:
                            # cur.executemany(sql,v)
                            # v是二维数组
                            common.run_sql(sql_txt=sql,dbFile=jndi + '.txt',needResult=False,executemany=True,parameter_list=v,display=False)
                        except Exception as e2:
                            common.write_log("文件{}导入失败(2)：{}".format(datafile, e2))
                            return False
                            # raise Exception(f"{datafile} load failed",sql)
                        # 重置计数
                        v = list()
                        i = 0
                b = file.readlines(1)
            if len(v) > 0:
                # common.run_sql(sql_txt=sql, dbFile=jndi + '.txt', needResult=False, executemany=True, parameter_list=v,
                #                display=False)
                try:
                    # cur.executemany(sql, v)
                    # print(8888, v[2])
                    common.run_sql(sql_txt=sql, dbFile=jndi + '.txt', needResult=False, executemany=True, parameter_list=v, display=True)
                except Exception as e3:
                    # common.write_log(f"{datafile} load failed")
                    # common.write_log(sql,v)
                    # raise Exception(f"{datafile} load failed",sql)
                    common.write_log("文件{}导入失败(3)：{}".format(datafile, e3), tip='WARN', write_flag=True, display=True)
                    return False
                # db.commit()
            linenum = linenum - 1
            # msg = f"{table_name} {linenum}" + " rows imported"
            # print(msg)
            common.write_log("@@@ {}文件已导入全部的{}行记录！".format(table_name, linenum), tip='INFO', write_flag=True, display=False)
            # file.close()
            endtime = time.strftime("%Y%m%d%H%M%S", time.localtime(time.time()))
            filesize = os.path.getsize(datafile)


            # 插入日志信息
            # common.write_log('开始插入日志')
            sql = """insert into data_dump_log(data_date,jndi_name,table_name,file_name,file_size,row_count,imp_exp_flag,platform_name,save_catalog,start_time,end_time)
                        values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
            v = (now_date, jndi, table_name, datafile, filesize, linenum, 'imp', platform_name, save_catalog, starttime, endtime)

            common.run_sql(sql_txt=sql, dbFile='etl_config.txt', needResult=False, executemany=False, parameter_list=v, display=False)

        # 已经完成导出的任务
        os.renames(datafile, datafile + '.old')
        common.write_log("@@@ {}文件导入完成！表名为【{}】 (并将已导入文件加.old后缀)".format(datafile, table_name), tip='INFO', write_flag=True, display=True)
        return True
    except Exception as e4:
    # except ImportError as e4:
        common.write_log('出现错误:' + str(e4), tip='ERROR')
        # warn_content = """----- ETL依赖关系告警 ----- 
        # 【告警对象】 etl_azkaban.bi.oigbuy.com-后台linux系统
        # 【脚本位置】{0}
        # 【异常问题】表数据导入时，出现异常
        # 【脚本返回异常信息】:{1}
        # 【解决方案】检查python脚本，查看日志
        # ;""".format(py_file_name, e4)

        # dingding.dingtalk_warn_text(message=warn_content, webhook_id='T1', warn_object=None, at_all=False)

        # return False



        # raise


if __name__ == "__main__":
    len_para = len(sys.argv) - 1

    if len_para < 2:
        common.write_log("传入的参数个数小于2，当前传入%s个" % len_para, tip='ERROR')

    common.write_log("传入参数：【{}】".format(sys.argv))

    result = get_parameter(sys.argv)
    target_jndi = result['target_jndi']
    file_name = result['file_name']

    if not file_name.endswith('.dat'):
        common.write_log("文件【{}】不是以.dat结尾，不继续执行程序！".format(file_name), tip='ERROR')

    ## 解决其他脚本，传入参数中有单引号的问题
    # filefilter = filefilter.replace("'","")
    files = glob.glob(file_name, recursive=False)
    if len(files) < 1:
        common.write_log("找不到导入数据的文件：{}".format(file_name), tip='ERROR')

    max_workers = 3
    if len(files) < max_workers:
        max_workers = len(files)

    common.write_log("开始加载数据导入程序！！")
    with ProcessPoolExecutor(max_workers) as do:
        for f in files:
            do.submit(loaddata,target_jndi, f)
        do.shutdown(wait=True)
    common.write_log("数据导入程序完成！！")
    sys.exit(0)
