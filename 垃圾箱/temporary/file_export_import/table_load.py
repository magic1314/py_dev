# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：file_load.py
# 作者：石大峰
# 版本：v1.1
# 创建时间：20201229
# 功能简述：将数据文件导入到mysql库
# 执行命令：python file_export_import\table_load.py  table_type=test
# 应用平台：window10、linux
# 注：谨慎操作；导入表数据之前，需要备份原有表的数据
# target_jndi参数为目标库的jndi
#######################################################################################
# 更新记录
# 2023-03-21: 从之前的代码中迁移
# 2023-03-22: 将代码测试运行，没有问题
# 2023-03-23: 从file_load.py脚本衍生而来
# 2023-03-31: 支持BIT类型(布尔类型)的数据导入
#######################################################################################
import sys
import pymysql
import time
import os
import re
from warnings import filterwarnings
filterwarnings("ignore",category=pymysql.Warning)
# if sys.platform == 'linux':
#     sys.path.append("/home/dev_admin/script/share_function")
import common


# export_file_name = r'20210817-etl_dingding_at_person.gz'
now_date = common.get_parameter('now_date')
py_file_name = os.path.realpath(__file__)


FIELD_SEPERATOR = "!|"

# 判断windows和linux系统
if sys.platform == 'win32':
    platform_name = 'windows'
elif sys.platform == 'linux':
    platform_name = 'linux'
else:
    platform_name = None
    common.write_log('当前【%s】系统不支持！！' % sys.platform)


# 20230322_01_sys_dept_01.dat
def match_file(path, table_name):
    if not os.path.isdir(path):
        common.write_log("找不到目录：{}".format(path), tip='WARN')
        return 0
    listdir = os.listdir(path)
    list_file = list()
    for x in listdir:
        match = re.match(r"^\d{8}_\d{2}_" + table_name + "_\d{2}.dat$", x)
        try:
            match.group()
            # common.write_log("匹配成功,文件为：{}".format(x), tip='INFO')
            list_file.append(x)
        except:
            # print('匹配失败')
            pass
        
    if len(list_file) == 1:
        common.write_log("匹配成功,文件为【{}】".format(list_file[0]), tip='INFO')
        return list_file[0]
    else:
        common.write_log("匹配文件的个数，目前必须是1，否则均不装载，当前文件有【{}】个，文件列表【{}】".format(len(list_file), list_file), tip='WARN')
        return None



def load_table(database_name: str, table_name: str, display = True):
    # 读取配置信息
    sql = """select database_name,table_name,save_catalog_windows,save_catalog_linux,jndi_name
            from data_dump_conf  
            where database_name = '{0}' and table_name = '{1}' and imp_exp_flag = 'imp' and enable_status = '1' order by order_no """.format(database_name, table_name)

    result = common.run_sql(sql, dbFile='etl_config.txt', needResult=True, executemany=False, display=display)

    if not result:
        common.write_log("无记录，异常！", tip='ERROR')

    r = result[0]

    database = r[0]
    table_name = r[1]
    # 判断windows和linux系统
    if sys.platform == 'win32':
        platform_name = 'windows'
        save_catalog = r[2].replace(r'${TODAY}', now_date)
    elif sys.platform == 'linux':
        platform_name = 'linux'
        save_catalog = r[3].replace(r'${TODAY}', now_date)
    else:
        platform_name = None
        save_catalog = None
        common.write_log('当前【%s】系统不支持！！'  % sys.platform)

    jndi=r[4]
    starttime: str = time.strftime("%Y%m%d%H%M%S", time.localtime(time.time()))
    common.write_log("开始导出【%s】表的数据！" % table_name, tip='INFO', write_flag=True, display=display)

    # 匹配本地文件
    # 文件名实例：20230323_01_sys_dept_01.dat
    simple_file_name = match_file(save_catalog, table_name)
    if not simple_file_name:
        return None

    datafile = save_catalog + os.path.sep + simple_file_name
    line_cnt = common.get_file_line_cnt(datafile)
    if line_cnt <= 1 or line_cnt == None:
        return None


    pos = datafile.rfind(os.sep)
    if pos != -1:
        tmp = datafile[pos + 1:]
        save_catalog = datafile[:pos]
    else:
        tmp = datafile
        save_catalog = '.'

    pos = tmp.find(".dat")
    if pos != -1:
        tmp = tmp[0:pos]
    
    data_dt = tmp[0:8]
    # print(data_dt)
    table_name = tmp[8+1+3:-3]


    # print(tablename)
    common.write_log("已获取数据文件的信息：\n\t\t\t### file_name:【{}】 data_date:【{}】,tablename:【{}】".format(datafile, data_dt, table_name))

    # return 0

    selectsql = """select count(1) cnt from {}
    """.format(table_name)
    result = common.run_sql(sql_txt=selectsql, dbFile=jndi + '.txt', needResult=True, executemany=False, parameter_list=None,
                    display=False)

    # print(result,type(result))

    if not result:
        common.write_log("没有记录(正常不出该结果)！", tip="ERROR", write_flag=True, display=display)
    cnt = result[0][0]
    if cnt > 0:
        common.write_log("{}表存在{}条记录，默认更新方式为全删全插！".format(table_name, cnt), tip='WARN', write_flag=True, display=display)
        # 清空之前表的数据
        truncate_sql = """truncate %s""" % table_name
        common.run_sql(sql_txt=truncate_sql, dbFile=jndi + '.txt', needResult=False, executemany=False, parameter_list=None,
                    display=display)
        # print(66666, '已清表空数据')

    # sys.exit(99)


    sql = """select column_name ,upper(data_type) data_type  
    from information_schema.columns  
    where table_name = '%s' """ % table_name
    result = common.run_sql(sql_txt=sql,dbFile=jndi + '.txt',needResult=True,executemany=False,parameter_list=None,display=display)
    col_types = {}
    for data_row in result:
        col_types.setdefault(data_row[0],data_row[1])
    # print(999999999, type(col_types), col_types)
    # sys.exit(88)
    
    linenum = 0

    common.write_log("@@@ 开始导入{}表的数据".format(table_name), tip='INFO', write_flag=True, display=display)

    # sys.exit(55)

    with open(datafile,"rb") as file:
        # g = gzip.GzipFile(fileobj=fp,mode="rb")
        # 第一行的数据
        # [b'id\x01person_name\x01phone_number\x01creator\x01update_time\x01remark\n']
        b = file.readlines(1)
        # print(b, type(b), len(b))
        # sys.exit(77)
        i = 0
        v=[]
        # 轮循第一行的字段
        while len(b) == 1:
            linenum = linenum + 1
            s = b[0].decode().rstrip()
            # 具体的数据
            # idperson_namephone_numbercreatorupdate_timeremark
            # 1石大峰18255624253magic20210511095208
            # 2陶开15021015412magic20210511095530#
            # print(133333, s)
            dats = s.split(FIELD_SEPERATOR)
            # print(777777777, dats)
            # 第一行 字段头的处理
            if linenum == 1:
                sql = "insert into " + table_name + "( " + ",".join(dats) + ") "
                vals = "values("
                for i, col in enumerate(dats):
                    # print("col: " + col)
                    # if col.strip() == 'UPDATE_TIME':
                    #     continue
                    col_type = col_types.get(col)
                    """if col_type == "TIME":
                        vals = vals + "TIME(%s)"
                    elif col_type == "TIMESTAMP":
                        vals = vals + "TO_DATE(%s,'YYYYMMDDHHMISS')"
                    elif col_type == "DATETIME":
                        vals = vals + "TO_DATE(%s,'YYYYMMDDHHMISS')"
                    elif col_type == "DATE":
                        vals = vals + "TO_DATE(%s,'YYYYMMDD')"
                    else:"""
                    # if col_type == 'BIT':


                    vals = vals + "%s"

                    vals = vals + ","
                    # print(5555555, vals)

                vals = vals.rstrip(",") + ")"

                sql = sql + vals
            else:
                # 插入的数据部分
                # print(6666666, dats)
                # print(5555555, col_types)
                # dats = [x if len(x) > 0 else None for x in dats]
                # print(777777, dats)

                # 总字段个数
                col_name_list = enumerate(list(col_types.values()))
                # print(8888888, len(col_name_list), len(dats))
                tmp = list()
                for x,column_name in col_name_list:
                    # print(566666555, len_col, x)
                    # 分别得到字段名和字段的值
                    # print(col_types)
                    column_value = dats[x]
                    # print(66666, column_name, column_value)

                    if not column_value:
                        column_value = None
                        # 如果是二进制类型，置为数值类型的数据
                    if column_name == 'BIT' and column_name:
                        column_value = int(column_value)
                    tmp.append(column_value)
                
                dats = tmp
                # print(dats)

                v.append(tuple(dats))
                i = i + 1
                if i >= 2000:
                    try:
                        # cur.executemany(sql,v)
                        # v是二维数组
                        common.run_sql(sql_txt=sql,dbFile=jndi + '.txt',needResult=False,executemany=True,parameter_list=v,display=display)
                    except Exception as e2:
                        common.write_log("文件{}导入失败(2)：{}".format(datafile, e2))
                        return False
                        # raise Exception(f"{datafile} load failed",sql)
                    # 重置计数
                    v = list()
                    i = 0
            b = file.readlines(1)
        if len(v) > 0:
            # common.run_sql(sql_txt=sql, dbFile=jndi + '.txt', needResult=False, executemany=True, parameter_list=v,
            #                display=False)
            try:
                # cur.executemany(sql, v)
                # print(8888, v[2])
                common.run_sql(sql_txt=sql, dbFile=jndi + '.txt', needResult=False, executemany=True, parameter_list=v, display=True)
            except Exception as e3:
                # common.write_log(f"{datafile} load failed")
                # common.write_log(sql,v)
                # raise Exception(f"{datafile} load failed",sql)
                common.write_log("文件{}导入失败(3)：{}".format(datafile, e3), tip='WARN', write_flag=True, display=True)
                return False
            # db.commit()
        linenum = linenum - 1
        # msg = f"{table_name} {linenum}" + " rows imported"
        # print(msg)
        common.write_log("@@@ {}文件已导入全部的{}行记录！".format(table_name, linenum), tip='INFO', write_flag=True, display=display)
        # file.close()
        endtime = time.strftime("%Y%m%d%H%M%S", time.localtime(time.time()))
        filesize = os.path.getsize(datafile)


        # 插入日志信息
        # common.write_log('开始插入日志')
        sql = """insert into data_dump_log(data_date,jndi_name,table_name,file_name,file_size,row_count,imp_exp_flag,platform_name,save_catalog,start_time,end_time)
                    values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
        v = (now_date, jndi, table_name, datafile, filesize, linenum, 'imp', platform_name, save_catalog, starttime, endtime)

        common.run_sql(sql_txt=sql, dbFile='etl_config.txt', needResult=False, executemany=False, parameter_list=v, display=False)

    # 已经完成导出的任务
    os.renames(datafile, datafile + '.old')
    common.write_log("@@@ {}文件导入完成！表名为【{}】 (并将已导入文件加.old后缀)".format(datafile, table_name), tip='INFO', write_flag=True, display=display)
    return True




# 异常
def try_exception(database_name, table_name,te=True,display=True):
    if te:
        try:
            load_table(database_name, table_name, display=display)
        except Exception as e4:
            # except ImportError as e4:
            common.write_log('出现错误:' + str(e4), tip='ERROR')
    else:
        load_table(database_name, table_name, display=display)


def start_loop_import(table_type):
    # 读取配置
    sql = """select database_name,table_name
    from data_dump_conf  
    where imp_exp_flag = 'imp' and enable_status = '1' and table_type='%s'
    order by order_no """ % table_type
    result = common.run_sql(sql, dbFile='etl_config.txt', needResult=True, executemany=False,display=True)
    # print(result)

    # print(555, result)
    for i in result:
        (database_name, table_name) = i
        try_exception(database_name, table_name,te=True,display=True)




if __name__ == "__main__":
    len_para = len(sys.argv) - 1

    if len_para < 1:
        common.write_log("传入的参数个数小于1，当前传入%s个" % len_para, tip='ERROR')

    common.write_log("传入参数：【{}】".format(sys.argv[1]))

    table_type = common.get_input_param(sys.argv).get('table_type')
    common.write_log("已读取配置！%s" % table_type, tip='INFO', write_flag=True, display=True)

    start_loop_import(table_type)

    common.write_log("数据导入程序完成！！")
    sys.exit(0)
