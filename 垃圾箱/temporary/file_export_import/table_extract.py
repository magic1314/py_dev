# -*- coding: UTF-8 -*-
#######################################################################################
# 名称：table_extract.py
# 作者：石大峰
# 版本：v1.0
# 创建时间：2021-06-08
# windows： python file_export_import/table_extract.py table_type=config
# linux: /home/dev_admin/anaconda3/bin/python /home/dev_admin/script/python/table_extract.py table_type=config
#######################################################################################
# 更新记录
# 2023-03-21: 从之前的代码中迁移
# 2023-03-22: 将代码测试运行，没有问题
#######################################################################################
import time
import os
import sys
# if sys.platform == 'linux':
#     sys.path.append("/home/dev_admin/script/share_function")
import common
#import dingding


# 导出文件的配置信息
# FIELD_SEPERATOR = "\001"
FIELD_SEPERATOR = "!|"
LINE_TERMINATOR = "\n"


now_date = common.get_parameter('YYYYMMDD')
py_file_name = os.path.realpath(__file__)

def dump_table(database_name: str, table_name: str):
    try:
        # 读取配置信息
        sql = """select table_name,where_clause,save_catalog_windows,save_catalog_linux,jndi_name
                from data_dump_conf  
                where database_name = '{0}' and table_name = '{1}' and imp_exp_flag = 'exp' and enable_status = '1' order by order_no """.format(database_name, table_name)

        result = common.run_sql(sql, dbFile='etl_config.txt', needResult=True, executemany=False, display=True)
        r = result[0]
        tablename = r[0]
        whereclause = r[1]
        # 判断windows和linux系统
        if sys.platform == 'win32':
            platform_name = 'windows'
            save_catalog = r[2].replace(r'${TODAY}', now_date)
        elif sys.platform == 'linux':
            platform_name = 'linux'
            save_catalog = r[3].replace(r'${TODAY}', now_date)
        else:
            platform_name = None
            save_catalog = None
            common.write_log('当前【%s】系统不支持！！'  % sys.platform)

        jndi_name=r[4]

        common.write_log("开始导出【%s】表的数据！" % table_name, tip='INFO', write_flag=True, display=True)
        selectsql = "select * from " + database_name + '.' + tablename
        if not whereclause is None and len(whereclause) > 5:
            conditionsql = whereclause
            selectsql = selectsql + " where " + conditionsql

        selectsql = selectsql.lower().replace("\r", " ").replace("\n", " ") + " "

        # 库名不用小写
        # selectsql = selectsql.replace(database_name.lower(), database_name)
        common.write_log("sql语句：%s" % selectsql, tip='INFO', write_flag=True, display=True)

        # 处理文件路径
        if not save_catalog:
            save_catalog = "."
        else:
            save_catalog = save_catalog.strip()

        parent_save_catalog = os.path.dirname(save_catalog)
        if not os.path.isdir(parent_save_catalog):
            common.write_log("data_dump_conf表中【%s】表的配置中，save_catalog字段对应的父级【%s】目录不存在，请检查！" % (table_name, parent_save_catalog), tip='ERROR')

        if not os.path.isdir(save_catalog):
            os.mkdir(save_catalog)

        starttime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))
        # 导出文件的命名规则
        filename = now_date + '_01_' + tablename + "_01.dat"
        # print(66666666)

        # 获得配置信息
        db = None
        cur = None
        try:
            dbconfig = common.config_file(dbFile=jndi_name+'.txt')
            db = common.getdbconnect(dbconfig)
            cur = db.cursor()
            # print('连接数据库成功！')
        except Exception as err:
            common.write_log(err, tip='ERROR')

        # 执行sql
        # print(66666, selectsql)
        cur.execute(selectsql)
        col_properties = cur.description
        # print(col_properties)


        # 关闭数据库连接、游标
        cur.close()
        db.close()

        col_line = ""
        for col_prop in col_properties:
            colname = col_prop[0]
            col_line = col_line + colname + FIELD_SEPERATOR
        col_line = col_line.strip(FIELD_SEPERATOR)

        dats = cur.fetchall()
        rowcnt = cur.rowcount

        if rowcnt < 1:
            common.write_log("%s表记录为空，跳过" % table_name, tip='WARN')
            return 0

        running_file = save_catalog.rstrip(os.path.sep) + os.path.sep + 'running_' + filename
        # print(44444, running_file)

        # sys.exit(44)
        ok_file = save_catalog.rstrip(os.path.sep) + os.path.sep +  filename

        if os.path.isfile(running_file):
            os.remove(running_file)

        """
        # sql执行后，结果中的描述信息(cur.description)的字段类型mapping关系
DECIMAL = 0
TINY = 1
SHORT = 2
LONG = 3
FLOAT = 4
DOUBLE = 5
NULL = 6
TIMESTAMP = 7
LONGLONG = 8
INT24 = 9
DATE = 10
TIME = 11
DATETIME = 12
YEAR = 13
NEWDATE = 14
VARCHAR = 15
BIT = 16
JSON = 245
NEWDECIMAL = 246
ENUM = 247
SET = 248
TINY_BLOB = 249
MEDIUM_BLOB = 250
LONG_BLOB = 251
BLOB = 252
VAR_STRING = 253
STRING = 254
GEOMETRY = 255

CHAR = TINY
INTERVAL = ENUM
        
        """

        # print("开始导出文件内容")
        with open(running_file, "wb") as file:
            # fgz = gzip.GzipFile(mode="wb", fileobj=f)
            file.write((col_line + LINE_TERMINATOR).encode())
            i = 0
            for row in dats:
                col_index = 0
                txt = ""
                for col_prop in col_properties:
                    coltype = col_prop[1]
                    data = row[col_index]
                    col_index = col_index + 1
                    # 字符型要处理回车换行符
                    if coltype in [252, 253, 254]:
                        if data is None:
                            data = ""
                        else:
                            data = data.replace("\r", " ").replace(LINE_TERMINATOR, " ")

                    # 数值型直接取数 转字符串
                    # 8是bignit , 5 float
                    if coltype in [1, 2, 3, 4, 5, 8, 246]:
                        if data is None:
                            data = ""
                        else:
                            data = str(data)


                    if coltype == 16:
                        if data is None:
                            data = ""
                        else:
                            # from_bytes用法参考：http://www.manongjc.com/detail/31-fxmsamalsvsmdvc.html
                            data = str(int.from_bytes(data, byteorder='big', signed=False))
                            # print(data)

                    if coltype == 7:
                        if data is None:
                            data = ""
                        else:
                            data = data.strftime("%Y%m%d%H%M%S")

                    if coltype == 12:
                        if data is None:
                            data = ""
                        else:
                            data = data.strftime("%Y%m%d%H%M%S")

                    if coltype == 10:
                        # 驱动有问题，已修改
                        if data is None:
                            data = ""
                        else:
                            data = data.strftime("%Y%m%d")

                    if coltype == 11:
                        if data is None:
                            data = ""
                        else:
                            # 驱动有问题，已修改
                            # 返回timedelta
                            h, r = divmod(data.seconds, 3600)
                            m, s = divmod(r, 60)
                            # GBase没有毫秒和微秒
                            # fs = data.microseconds / 1e6
                            data = "{0:0>2}{1:0>2}{2:0>2}".format(h, m, s)
                            # data = data.strftime("%H%M%S")
                    # print('6666', txt)
                    # print(7777, data)
                    # time.sleep(2)
                    # data不转成str，会出现问题
                    if common.get_type(data) != 'str':
                        print('类型为:', type(data))
                        print("coltype=", coltype)
                        print(data)
                        sys.exit(0)

                    txt = txt + data + FIELD_SEPERATOR

                txt = txt[0:-len(FIELD_SEPERATOR)] + LINE_TERMINATOR
                # print(txt)
                file.write(txt.encode())

                # i = i + 1
                # if i >= 2000:
                #     file.flush()
                #     i = 0
            # file.flush()
            # file.close()

        filesize = os.path.getsize(running_file)

        if os.path.isfile(ok_file):
            os.remove(ok_file)
        os.rename(running_file, ok_file)
        endtime = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(time.time()))

        # 插入日志信息
        # common.write_log('开始插入日志')
        # sql = """insert into data_dump_log(data_date,jndi_name,table_name,file_name,file_size,row_count,imp_exp_flag,platform_name,save_catalog,start_time,end_time)
        #         values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
        # v = (now_date, database_name, tablename, ok_file, filesize, rowcnt, 'exp',platform_name,save_catalog, starttime, endtime)
        sql = """insert into data_dump_log(data_date,jndi_name,database_name,table_name,file_name,file_size,row_count,imp_exp_flag,platform_name,save_catalog,start_time,end_time)
                values(%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"""
        v = (now_date, jndi_name,database_name, tablename, ok_file, filesize, rowcnt, 'exp',platform_name,save_catalog, starttime, endtime)
        common.run_sql(sql, dbFile='etl_config.txt', needResult=False,executemany=False,parameter_list=v,display=False)


        common.write_log("【%s】表导出成功,导出文件名:【%s】" % (table_name, ok_file))
    except Exception as error:
        common.write_log('出现错误:' + str(error), tip='ERROR')


def start_loop_export(table_type):
    # 读取配置
    sql = """select database_name,table_name
    from data_dump_conf  
    where imp_exp_flag = 'exp' and enable_status = '1' and table_type='%s'
    order by order_no """ % table_type
    result = common.run_sql(sql, dbFile='etl_config.txt', needResult=True, executemany=False,display=True)
    # print(result)

    # print(555, result)
    for i in result:
        (database_name, table_name) = i
        dump_table(database_name, table_name)


if __name__ == "__main__":

    common.write_log("----- 程序开始执行 -----")

    # 读取参数
    table_type = common.get_input_param(sys.argv)['table_type']
    common.write_log("已读取配置！%s" % table_type, tip='INFO', write_flag=True, display=True)
    # sys.exit(10)

    start_loop_export(table_type)

    common.write_log("----- 程序执行完成！-----")



