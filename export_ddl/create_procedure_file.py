import public
import os

import time
import re
import shutil
from datetime import datetime


# 获得文件size
def get_file_size(file_path, units=['bytes', 'KB', 'MB', 'GB', 'TB']):
    size = os.path.getsize(file_path)
    # for unit in units:
    #     if size < 1024.0 or unit == units[-1]:
    #         break
    #     size /= 1024.0
    # return f"{size:.2f} {unit}"
    return size


# 获得文件的创建和更新时间
def get_file_times(file_path):
    stat_info = os.stat(file_path)
    
    # 获取最后修改时间
    mtime = stat_info.st_mtime
    modified_time = datetime.fromtimestamp(mtime).strftime('%Y-%m-%d %H:%M:%S')
    
    # 获取创建时间
    if hasattr(os, 'statvfs'):
        # Unix-like系统可能需要使用其他方法获取创建时间
        ctime = stat_info.st_ctime
    else:
        # Windows系统
        ctime = stat_info.st_ctime
    created_time = datetime.fromtimestamp(ctime).strftime('%Y-%m-%d %H:%M:%S')
    
    return created_time, modified_time
    
# public.write_log("测试")

# procedure_name = 'PRO_LOAD_HR_EMP'


# 返回日志最新的日期
def get_max_date(file_path):
    try:
        match_list = list()
        i = 0
        pattern = re.compile('202\d[/-][01]?\d[/-][0123]?\d')
        with open(file_path, mode='r', encoding='utf-8') as f:
            lines = f.readlines()
            for line in lines:
                # print(line)
                i += 1
                matches = re.findall(pattern, line)
                if matches:
                    # print(matches)
                    match_list += matches
        
        # print(match_list)
        if match_list:
            return (max(match_list), i)
        return (None, None)
    except Exception as e:
        print("e:", e)
        return (None, None)







def except_deal(message):
    def decorator(func):
        def wrapper(*args, **kwargs):
            try:
                result = func(*args, **kwargs)
            except Exception as e:
                public.write_log(f"出现异常：{e}")
                print(f"函数：{func.__name__}  出现异常：{e}")
                # raise e
            return result
        return wrapper
    return decorator



# @except_deal
def create_procedure(procedure_name=None, dbFile='hrjx_develop.txt'):
    procedure_sql = f"show create PROCEDURE {procedure_name}"

    # 连接数据库
    result = public.run_sql(sql_txt=procedure_sql,dbFile=dbFile,mul_sql=False,needResult=True,executemany=False,parameter_list=None,group_concat_max_len=0,display=False)

    YYYYMMDD = public.get_parameter('YYYYMMDD')

    tmp_file_name = 'tmp.sql'
    dev_procedure_file_name = os.path.join('dev', YYYYMMDD + '_' + procedure_name + '.sql')
    local_procedure_file_name = os.path.join('local', YYYYMMDD + '_' + procedure_name + '.sql')

    if dbFile == 'hrjx_develop.txt':
        procedure_file_name = dev_procedure_file_name
    else:
        procedure_file_name = local_procedure_file_name

    with open(tmp_file_name, 'w', encoding='utf-8') as file:
        # file.write(result[0][2].replace('$!','\n').replace('$de$',';\n'))
        file.write(result[0][2].replace('\r',''))

    # print(888888888, procedure_file_name)
    with open(tmp_file_name, 'r', encoding='utf-8') as file1, open(procedure_file_name, 'w', encoding='utf-8') as file2:
        lines = file1.readlines()
        file2.write(f'DROP PROCEDURE IF EXISTS {procedure_name};\n')
        for line in lines:
            file2.write(line.replace('DEFINER=`HR_JX`@`%` ','').replace('DEFINER=`root`@`localhost` ',''))
        file2.write(f'\n;')

    abs_path = os.path.abspath(procedure_file_name)
    file_size = get_file_size(abs_path)
    max_date,lines = get_max_date(abs_path)
    public.write_log(
    """-------------------------\n存放位置：{0}
    存储过程名称：{1}
    最新更新日期: {3}
    行数：        {4}
    文件大小：    {2}
    """.format(abs_path, procedure_name, file_size, max_date, lines))


if __name__ == '__main__':
    while True:
        input_str = input("请输入存储过程名：")
        # print(input_str, type(input_str))
        try:
            create_procedure(procedure_name=input_str, dbFile='hrjx_develop.txt')
        except Exception as e:
            print(e)
        
        try:
            create_procedure(procedure_name=input_str, dbFile='localhost_hrjx.txt')
        except Exception as e:
            print(e)


