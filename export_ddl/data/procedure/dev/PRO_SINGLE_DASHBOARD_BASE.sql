drop procedure if exists PRO_SINGLE_DASHBOARD_BASE;
CREATE PROCEDURE `PRO_SINGLE_DASHBOARD_BASE`(IN BIZ_DATE DATE)
label:BEGIN
/************************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_SINGLE_DASHBOARD_BASE
         功能简述：   看板基础数据
         参数：       BIZ_DATE  数据日期(YYYY-MM-DD)
         注意事项：
                      生效时间：数据是T+1生效且任务只在每日调度中执行
                      数据范围：按照【部门+用户】取最新的记录，以及上一期的记录(用于计算环比)
         数据源：
                    1. t_assess_member_score        员工考核结果表
                    2. t_assess_member_score_dtl    员工考核结果表明细
         目标表：
                    1. t_dashboard_overview_group    看板A-2:团队经营绩效概览
         修改记录;
         --------------------------------------------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2023/08/30                  创建
         MG         2023/09/28                  提交V18版本
         MG         2023/10/11                  提交V19版本
         MG         2023/12/04                  更新模拟试算的取值
         MG         2023/12/05                  考核模式逻辑存在问题
         MG         2023/12/08                  排行榜剔除班子成员
         MG         2023/12/12                  模拟试算目标值取考核方案中年度考核目标值
         MG         2023/12/14                  财富条线占比分母调整
         MG         2024/05/06                  个人象限数据列定
************************************************************************************************************************************************/
-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);

-- 定义变量来存储字段值
DECLARE output_dept_id bigint;
  
-- 游标声明
DECLARE cur CURSOR FOR 
SELECT dept_id FROM sys_dept
where org_type in (4,6)   -- 组织类型代码(1总公司 4总公司下挂各总部  6分公司  2营业部)
and status = 0
; 
  
-- 异常处理器
DECLARE CONTINUE HANDLER FOR NOT FOUND
  SET @finished = TRUE;

set @finished = NULL;

-- 设置变量
set V_EVENT_NAME = '看板概览';
select now() into V_START_TIME;



set V_RUN_COMMAND = concat('call PRO_SINGLE_DASHBOARD_BASE('
                            ,BIZ_DATE
                            ,')'
                         );



DROP TEMPORARY TABLE IF EXISTS tmp_dashboard_valid_user;
CREATE TEMPORARY TABLE tmp_dashboard_valid_user
select 
    u.dept_id  -- 人事部门
    ,u.user_id
    ,u.user_name
    ,u.account_flag
    ,r.role_id
    ,r.role_name
    ,e.Managercode
from sys_user u
left join sys_user_role sur 
    on u.user_id=sur.user_id
left join sys_role r 
    on sur.role_id = r.role_id
left join t_employee e 
    on u.user_id = e.sys_user_id
left join sys_dict_data d 
    on d.dict_type = 'emp_manager_status'
    and e.Managercode = d.dict_value
where u.del_flag='0'
and u.account_status=0
and (
    u.account_flag = 1 or 
    (
        u.account_flag = 2 
        and r.role_id not in (select role_id from sys_role where role_name like '%专员%' or role_name like '%管理员%')
    )
)
and (d.remark = '非管理人员' or d.remark is null)
;

DROP TEMPORARY TABLE IF EXISTS tmp_assess_member_score;
CREATE TEMPORARY TABLE tmp_assess_member_score
select 
     a.id
    ,a.par_year
    ,concat(a.par_year,mqh.assess_month_2str) year_months
    ,a.assess_pd
    ,a.assess_pd_value
    ,a.dept_id
    -- 这里是根据用户来排序，一个考核部门可以有同一个人的不同用户（中心和卫星营业部转换），先用【部门+用户】指定一个人
    -- 同一个用户，也可以在不同部门考核
    ,row_number() over(partition by a.dept_id,a.member_id order by concat(a.par_year,mqh.assess_month_2str) desc) rn_user_desc
    ,a.dept_appraisal_id
    ,a.magr_dept_id
    ,a.group_id
    ,a.member_id
    ,a.assess_score
    ,a.adjust_score
    ,a.final_assess_score
from t_assess_member_score a
inner join t_dict_assess_pd_mqh mqh 
    on a.assess_pd_value = mqh.assess_pd_value
inner join tmp_dashboard_valid_user t1
    on a.dept_id = t1.dept_id
    and a.member_id = t1.user_id
;

DROP TEMPORARY TABLE IF EXISTS tmp_assess_member_score_dtl;
CREATE TEMPORARY TABLE tmp_assess_member_score_dtl
select
     a.id
    ,a.par_year
    ,concat(a.par_year,mqh.assess_month_2str) year_months
    ,a.assess_pd
    ,a.assess_pd_value
    ,a.dept_id
    ,a.dept_appraisal_id
    ,a.magr_dept_id
    ,a.group_id
    ,a.member_id
    ,a.index_code
    ,a.index_appraisal_right
    ,a.business_type
    ,a.current_tgt
    ,a.complete_info
    ,a.index_score
    ,a.assess_score
    ,a.assess_member_socre_id
from t_assess_member_score_dtl a
inner join t_dict_assess_pd_mqh mqh 
    on a.assess_pd_value = mqh.assess_pd_value
inner join tmp_dashboard_valid_user t1
    on a.dept_id = t1.dept_id
    and a.member_id = t1.user_id
;



-- 取员工最新的考核方案
DROP TEMPORARY TABLE IF EXISTS tmp_assess_dtl;
CREATE TEMPORARY TABLE tmp_assess_dtl
select assess_dtl_id,par_year,dept_id,user_id
from (
    select 
        id assess_dtl_id,par_year,dept_id,user_id
        ,row_number() over(partition by par_year,dept_id,user_id order by status asc,effect_time desc) rn 
    from t_assess_dtl 
    where status in (4,5)
    and deleted = 0
    and effect_time != Logoff_time
) x 
where x.rn = 1
;

alter table tmp_assess_dtl add primary key(assess_dtl_id);



/*
select 
    t.par_year,t.dept_id,t.user_id,a.idx_code index_code,cast(a.annual_tgt as decimal(26,6)) annual_tgt,a.annual_tgt source_annual_tgt
from t_assess_dtl_idx a
inner join tmp_assess_dtl t 
on a.assess_dtl_id = t.assess_dtl_id
left join t_index_pool ip 
on a.idx_code = ip.index_code
where ip.business_type != 6
and cast(a.annual_tgt as decimal(26,6)) = 0
and a.annual_tgt != '0'
;

*/


-- 取定量指标的年度考核目标
DROP TEMPORARY TABLE IF EXISTS tmp_assess_dtl_idx;
CREATE TEMPORARY TABLE tmp_assess_dtl_idx
select 
    t.par_year,t.dept_id,t.user_id,a.idx_code index_code,a.annual_tgt,a.assess_model
from t_assess_dtl_idx a
inner join tmp_assess_dtl t 
on a.assess_dtl_id = t.assess_dtl_id
left join t_index_pool ip 
on a.idx_code = ip.index_code
where ip.business_type != 6
;

-- 对上面的结果汇总，聚合掉人员维度
DROP TEMPORARY TABLE IF EXISTS tmp_assess_dtl_idx2;
CREATE TEMPORARY TABLE tmp_assess_dtl_idx2
select 
    par_year,dept_id,index_code,sum(annual_tgt) current_tgt
from tmp_assess_dtl_idx
where assess_model = 1
group by par_year,dept_id,index_code
;



-- 模拟试算的取值
DROP TEMPORARY TABLE IF EXISTS tmp_emp_perf_cmplt_val_new;
CREATE TEMPORARY TABLE tmp_emp_perf_cmplt_val_new
select 
     x.assess_year
    ,x.year_months
    ,x.dept_id
    ,x.user_id
    ,x.index_code
    ,x.current_tgt
    ,x.index_value
    ,x.index_source
    ,x.create_time
    ,x.update_time
    ,x.assess_model
from (
    select 
         a.assess_year
        ,a.year_months
        ,a.dept_id
        ,a.user_id
        ,a.index_code
        ,row_number() over(partition by a.dept_id,a.user_id,a.index_code order by a.year_months desc) rn_user_desc
        ,di.annual_tgt current_tgt
        ,a.index_value
        ,a.index_source
        ,a.create_time
        ,a.update_time
        ,di.assess_model
    from t_emp_perf_cmplt_val a
    left join tmp_assess_dtl_idx di 
        on a.assess_year = di.par_year
        and a.dept_id = di.dept_id
        and a.user_id = di.user_id
        and a.index_code = di.index_code
    where a.default_zero_flag = 0
    and di.assess_model in (2,3)
) x
where x.rn_user_desc = 1
;


DROP TEMPORARY TABLE IF EXISTS tmp_emp_perf_cmplt_val_accu1;
CREATE TEMPORARY TABLE tmp_emp_perf_cmplt_val_accu1
select 
    x1.assess_year
    ,x1.dept_id
    ,x1.index_code
    ,x2.current_tgt
    ,x1.index_value
    ,x1.val_create_time
from 
    (
    select 
         a.assess_year
        ,a.dept_id
        ,a.index_code
        ,sum(a.index_value) index_value
        ,min(a.create_time) val_create_time
    from t_emp_perf_cmplt_val a
    left join tmp_assess_dtl_idx di 
        on a.assess_year = di.par_year
        and a.dept_id = di.dept_id
        and a.user_id = di.user_id
        and a.index_code = di.index_code
    where a.default_zero_flag = 0
    and di.assess_model = 1
    group by a.assess_year,a.dept_id,a.index_code
    ) x1 
left join tmp_assess_dtl_idx2 x2
    on x1.assess_year = x2.par_year
    and x1.dept_id = x2.dept_id
    and x1.index_code = x2.index_code
;





DROP TEMPORARY TABLE IF EXISTS tmp_emp_perf_cmplt_val_accu2;
CREATE TEMPORARY TABLE tmp_emp_perf_cmplt_val_accu2
select 
     max(a.assess_year) assess_year
    ,a.dept_id
    ,a.index_code
    ,sum(a.current_tgt) current_tgt
    ,sum(a.index_value) index_value
    ,min(a.create_time) val_create_time
from tmp_emp_perf_cmplt_val_new a
group by a.dept_id,a.index_code
;




update t_dashboard_perf_manage a 
inner join t_dashboard_perf_manage_powerless_idx b 
    on a.id = b.dashboard_perf_manage_id 
left join tmp_emp_perf_cmplt_val_accu1 t1
    on a.dept_id = t1.dept_id 
    and a.index_code = t1.index_code
left join tmp_emp_perf_cmplt_val_accu2 t2
    on a.dept_id = t2.dept_id 
    and a.index_code = t2.index_code
set  b.simulate_target_tgt  = ifnull(t1.current_tgt,t2.current_tgt)
    ,b.simulate_finish_tgt  = ifnull(t1.index_value,t2.index_value)
    ,b.simulate_update_time = ifnull(t1.val_create_time,t2.val_create_time)
;


update t_dashboard_perf_manage a 
inner join t_dashboard_perf_manage_power_idx b 
    on a.id = b.dashboard_perf_manage_id 
left join tmp_emp_perf_cmplt_val_accu1 t1
    on a.dept_id = t1.dept_id 
    and a.index_code = t1.index_code
left join tmp_emp_perf_cmplt_val_accu2 t2
    on a.dept_id = t2.dept_id 
    and a.index_code = t2.index_code
set  b.simulate_target_tgt  = ifnull(t1.current_tgt,t2.current_tgt)
    ,b.simulate_finish_tgt  = ifnull(t1.index_value,t2.index_value)
    ,b.simulate_update_time = ifnull(t1.val_create_time,t2.val_create_time)
;



/***** 看板F:绩效荣誉榜 *****/

/*
绩效荣誉榜：
        A 绝对分数排名前30%
        B 得分较上期增幅超10%

重点关注榜：
        C 最新一次考核绝对分数排名末10%且绝对分数低于75分
        D 得分较上期跌幅超10%

承担定量指标人员：
*/


-- 最新各部门的人数
drop TEMPORARY table if exists tmp_dept_people_cnt;
create TEMPORARY table tmp_dept_people_cnt
select a.dept_id,count(1) dept_people_cnt 
from tmp_assess_member_score a
where a.rn_user_desc = 1
group by a.dept_id
;

alter table tmp_dept_people_cnt add index idx_dept_id(dept_id);


-- 最新各团队(考核组)的人数
-- 团队必然只对应一个部门
drop TEMPORARY table if exists tmp_group_people_cnt;
create TEMPORARY table tmp_group_people_cnt
select a.group_id,count(1) dept_people_cnt 
from tmp_assess_member_score a
where a.rn_user_desc = 1
group by a.group_id
;

alter table tmp_group_people_cnt add index idx_group_id(group_id);






drop TEMPORARY table if exists tmp_dept_contain_qt_flag;
create TEMPORARY table tmp_dept_contain_qt_flag
select 
    a.dept_id
    ,a.member_id
    ,min(case when a.business_type != 6 then 1 else 2 end) contain_qt_flag  -- 承担定量人员标识(1:是,2:否)
from tmp_assess_member_score_dtl a
inner join tmp_assess_member_score b 
on a.assess_member_socre_id = b.id
where b.rn_user_desc = 1
group by a.dept_id,a.member_id
;


alter table tmp_dept_contain_qt_flag add index lh_2(dept_id,member_id);




/*
下面是黑榜基础数据的生成
按分数高低和涨跌幅比例，对数据排序，并标记记录是否符合条件


*/
-- 红黑榜基础排序
drop TEMPORARY table if exists tmp_dashboard_red_black_list_rank;
create TEMPORARY table tmp_dashboard_red_black_list_rank
select 
     a.par_year
    ,a.year_months
    ,a.assess_pd
    ,a.assess_pd_value
    ,a.dept_id
    ,row_number() over(partition by a.rn_user_desc,a.dept_id order by a.final_assess_score desc) rn_dept_desc
    ,row_number() over(partition by a.rn_user_desc,a.dept_id order by a.final_assess_score  asc) rn_dept_asc
    -- 这里是根据用户来排序，一个考核部门可以有同一个人的不同用户（中心和卫星营业部转换），先用【部门+用户】指定一个人
    -- 同一个用户，也可以在不同部门考核
    ,a.rn_user_desc
    ,d.dept_people_cnt
    ,e.contain_qt_flag
    ,a.group_id
    ,a.member_id
    ,a.final_assess_score
from tmp_assess_member_score a
left join tmp_dept_people_cnt d   -- 部门最新打分的人数
    on a.dept_id = d.dept_id
left join tmp_dept_contain_qt_flag e
    on a.dept_id = e.dept_id
    and a.member_id = e.member_id
-- where    -- 这里可以只保留rn_user_desc in (1,2)的数据
where a.rn_user_desc in (1,2)
;




-- 红榜
truncate table t_dashboard_red_list_rank;
insert into t_dashboard_red_list_rank(par_year,year_months,assess_pd,assess_pd_value,dept_id,dept_people_cnt,rn,rn_user_desc,current_ratio
    ,rank_status,contain_qt_flag,group_id,member_id,final_assess_score)
select
     a.par_year
    ,a.year_months
    ,a.assess_pd
    ,a.assess_pd_value
    ,a.dept_id
    ,a.dept_people_cnt
    ,a.rn_dept_desc as rn
    ,a.rn_user_desc
    ,a.rn_dept_desc/dept_people_cnt*100 current_ratio
    ,if(a.rn_dept_desc/a.dept_people_cnt*100<=ifnull(t1.red_list_rank_percentage,t2.red_list_rank_percentage),0,1) rank_status
    ,contain_qt_flag
    ,group_id
    ,member_id
    ,final_assess_score
from tmp_dashboard_red_black_list_rank a
left join t_dashboard_red_black_ranklist t1
    on a.dept_id = t1.dept_id
    and t1.status = 0
    and t1.deleted = 0
left join t_dashboard_red_black_ranklist t2
    on t2.dept_id = 1000  -- 有且仅有一条
;


update t_dashboard_red_list_rank a
left join t_dashboard_red_list_rank b -- 上一期的取值
    on a.dept_id = b.dept_id
    and a.member_id = b.member_id
    and a.rn_user_desc = b.rn_user_desc - 1
left join t_dashboard_red_black_ranklist t1
    on a.dept_id = t1.dept_id
    and t1.status = 0
    and t1.deleted = 0
left join t_dashboard_red_black_ranklist t2
    on t2.dept_id = 1000  -- 有且仅有一条
set a.last_time_final_assess_score = b.final_assess_score
    ,a.change_percentage = (a.final_assess_score - b.final_assess_score)/b.final_assess_score*100
    ,a.change_status = case when (a.final_assess_score - b.final_assess_score)/b.final_assess_score*100 > ifnull(t1.red_list_change_percentage,t2.red_list_change_percentage) then 0 else 1 end
;


-- 黑榜
truncate table t_dashboard_black_list_rank;
insert into t_dashboard_black_list_rank(par_year,year_months,assess_pd,assess_pd_value,dept_id,dept_people_cnt,rn,rn_user_desc,current_ratio
    ,rank_status,contain_qt_flag,group_id,member_id,final_assess_score)
select 
     x.par_year
    ,x.year_months
    ,x.assess_pd
    ,x.assess_pd_value
    ,x.dept_id
    ,x.dept_people_cnt
    ,row_number() over(partition by x.dept_id order by x.rank_status asc,rn asc) rn  -- 满足条件，排在前面
    ,x.rn_user_desc
    ,x.current_ratio
    ,x.rank_status
    ,x.contain_qt_flag
    ,x.group_id
    ,x.member_id
    ,x.final_assess_score
from (
    select
         a.par_year
        ,a.year_months
        ,a.assess_pd
        ,a.assess_pd_value
        ,a.dept_id
        ,a.dept_people_cnt
        ,a.rn_dept_asc rn
        ,a.rn_user_desc
        ,a.rn_dept_asc/a.dept_people_cnt*100 current_ratio
        ,case when ifnull(t1.black_list_rank_condition,t2.black_list_rank_condition) = 1 -- 逻辑与
                   and a.rn_dept_asc/a.dept_people_cnt*100<=ifnull(t1.black_list_rank_percentage,t2.black_list_rank_percentage) 
                   and a.final_assess_score<ifnull(t1.black_list_rank_lowest_score,t2.black_list_rank_lowest_score)
              then 0
              when ifnull(t1.black_list_rank_condition,t2.black_list_rank_condition) = 2 -- 逻辑或
                   and (
                        a.rn_dept_asc/a.dept_people_cnt*100<=ifnull(t1.black_list_rank_percentage,t2.black_list_rank_percentage) 
                        or a.final_assess_score<ifnull(t1.black_list_rank_lowest_score,t2.black_list_rank_lowest_score)
                    )
              then 0
              else 1 end rank_status
        ,a.contain_qt_flag
        ,a.group_id
        ,a.member_id
        ,a.final_assess_score
    from tmp_dashboard_red_black_list_rank a
    left join t_dashboard_red_black_ranklist t1
        on a.dept_id = t1.dept_id
        and t1.status = 0
        and t1.deleted = 0
    left join t_dashboard_red_black_ranklist t2
        on t2.dept_id = 1000  -- 有且仅有一条
) x
;



update t_dashboard_black_list_rank a
left join t_dashboard_black_list_rank b -- 上一期的取值
    on a.dept_id = b.dept_id
    and a.member_id = b.member_id
    and a.rn_user_desc = b.rn_user_desc - 1
left join t_dashboard_red_black_ranklist t1
    on a.dept_id = t1.dept_id
    and t1.status = 0
    and t1.deleted = 0
left join t_dashboard_red_black_ranklist t2
    on t2.dept_id = 1000  -- 有且仅有一条
set a.last_time_final_assess_score = b.final_assess_score
    ,a.change_percentage = (a.final_assess_score - b.final_assess_score)/b.final_assess_score*100*(-1) -- 由于这里是跌幅，需要取相反数
    ,a.change_status = case when (a.final_assess_score - b.final_assess_score)/b.final_assess_score*100*(-1) > ifnull(t1.red_list_change_percentage,t2.red_list_change_percentage) then 0 else 1 end
;



-- 删除不是最新记录的数据
delete from t_dashboard_red_list_rank where rn_user_desc = 2;
delete from t_dashboard_black_list_rank where rn_user_desc = 2;




/* 
下面是针对【承担定量人员】，对相应的四个字段进行清洗
注：只标记状态是【符合】的数据 
相关字段：
    contain_qt_rn             
    net_qlt_rn                
    change_contain_qt_rn      
    change_net_qlt_rn         
    group_contain_qt_rn       
    group_net_qlt_rn          
    group_change_contain_qt_rn
    group_change_net_qlt_rn   


*/

/* R部分 */
-- 筛选条件重新排序
drop TEMPORARY table if exists tmp_dashboard_rn_1;
create TEMPORARY table tmp_dashboard_rn_1
select
    a.id
    ,row_number() over(partition by a.dept_id order by a.rn asc) contain_qt_rn
    ,row_number() over(partition by a.dept_id,a.group_id order by a.rn asc) group_contain_qt_rn
from t_dashboard_red_list_rank a
where a.rank_status = 0 and a.contain_qt_flag = 1
;

drop TEMPORARY table if exists tmp_dashboard_rn_2;
create TEMPORARY table tmp_dashboard_rn_2
select
    a.id
    ,row_number() over(partition by a.dept_id order by a.rn asc) net_qlt_rn
    ,row_number() over(partition by a.dept_id,a.group_id order by a.rn asc) group_net_qlt_rn
from t_dashboard_red_list_rank a
where a.rank_status = 0 and a.contain_qt_flag = 2
;

drop TEMPORARY table if exists tmp_dashboard_rn_3;
create TEMPORARY table tmp_dashboard_rn_3
select
    a.id
    ,row_number() over(partition by a.dept_id order by a.change_percentage desc) change_contain_qt_rn
    ,row_number() over(partition by a.dept_id,a.group_id order by a.change_percentage desc) group_change_contain_qt_rn
from t_dashboard_red_list_rank a
where a.change_status = 0 and a.contain_qt_flag = 1
;

drop TEMPORARY table if exists tmp_dashboard_rn_4;
create TEMPORARY table tmp_dashboard_rn_4
select
    a.id
    ,row_number() over(partition by a.dept_id order by a.change_percentage desc) change_net_qlt_rn
    ,row_number() over(partition by a.dept_id,a.group_id order by a.change_percentage desc) group_change_net_qlt_rn
from t_dashboard_red_list_rank a
where a.change_status = 0 and a.contain_qt_flag = 2
;

alter table tmp_dashboard_rn_1 add primary key (id);
alter table tmp_dashboard_rn_2 add primary key (id);
alter table tmp_dashboard_rn_3 add primary key (id);
alter table tmp_dashboard_rn_4 add primary key (id);

-- 更新排序字段 
update t_dashboard_red_list_rank a
left join tmp_dashboard_rn_1 b1
on a.id = b1.id 
left join tmp_dashboard_rn_2 b2
on a.id = b2.id 
left join tmp_dashboard_rn_3 b3
on a.id = b3.id 
left join tmp_dashboard_rn_4 b4
on a.id = b4.id 
set a.contain_qt_rn        = b1.contain_qt_rn
   ,a.net_qlt_rn           = b2.net_qlt_rn
   ,a.change_contain_qt_rn = b3.change_contain_qt_rn
   ,a.change_net_qlt_rn    = b4.change_net_qlt_rn
   ,a.group_contain_qt_rn        = b1.group_contain_qt_rn
   ,a.group_net_qlt_rn           = b2.group_net_qlt_rn
   ,a.group_change_contain_qt_rn = b3.group_change_contain_qt_rn
   ,a.group_change_net_qlt_rn    = b4.group_change_net_qlt_rn
;


/* B部分 */
-- 筛选条件重新排序(目标表不同，其他都一样)
drop TEMPORARY table if exists tmp_dashboard_rn_1;
create TEMPORARY table tmp_dashboard_rn_1
select
    a.id
    ,row_number() over(partition by a.dept_id order by a.rn asc) contain_qt_rn
    ,row_number() over(partition by a.dept_id,a.group_id order by a.rn asc) group_contain_qt_rn
from t_dashboard_black_list_rank a
where a.rank_status = 0 and a.contain_qt_flag = 1
;

drop TEMPORARY table if exists tmp_dashboard_rn_2;
create TEMPORARY table tmp_dashboard_rn_2
select
    a.id
    ,row_number() over(partition by a.dept_id order by a.rn asc) net_qlt_rn
    ,row_number() over(partition by a.dept_id,a.group_id order by a.rn asc) group_net_qlt_rn
from t_dashboard_black_list_rank a
where a.rank_status = 0 and a.contain_qt_flag = 2
;

drop TEMPORARY table if exists tmp_dashboard_rn_3;
create TEMPORARY table tmp_dashboard_rn_3
select
    a.id
    ,row_number() over(partition by a.dept_id order by a.change_percentage desc) change_contain_qt_rn
    ,row_number() over(partition by a.dept_id,a.group_id order by a.change_percentage desc) group_change_contain_qt_rn
from t_dashboard_black_list_rank a
where a.change_status = 0 and a.contain_qt_flag = 1
;

drop TEMPORARY table if exists tmp_dashboard_rn_4;
create TEMPORARY table tmp_dashboard_rn_4
select
    a.id
    ,row_number() over(partition by a.dept_id order by a.change_percentage desc) change_net_qlt_rn
    ,row_number() over(partition by a.dept_id,a.group_id order by a.change_percentage desc) group_change_net_qlt_rn
from t_dashboard_black_list_rank a
where a.change_status = 0 and a.contain_qt_flag = 2
;

alter table tmp_dashboard_rn_1 add primary key (id);
alter table tmp_dashboard_rn_2 add primary key (id);
alter table tmp_dashboard_rn_3 add primary key (id);
alter table tmp_dashboard_rn_4 add primary key (id);

-- 更新排序字段(排序需要倒序)
-- 比如部门有18个人
-- 倒数第一 正序排名1，倒序排名为18
-- 倒数第二，正序排名2，倒序排名为17
-- 倒数第n，正序排名n，倒序排名为18-n+1
-- ,(a.dept_people_cnt - a.rn_dept_asc + 1) as rn 
update t_dashboard_black_list_rank a
left join tmp_dashboard_rn_1 b1
on a.id = b1.id 
left join tmp_dashboard_rn_2 b2
on a.id = b2.id 
left join tmp_dashboard_rn_3 b3
on a.id = b3.id 
left join tmp_dashboard_rn_4 b4
on a.id = b4.id 
left join tmp_group_people_cnt tt
on a.group_id = tt.group_id
set a.contain_qt_rn              =  a.dept_people_cnt - b1.contain_qt_rn              + 1
   ,a.net_qlt_rn                 =  a.dept_people_cnt - b2.net_qlt_rn                 + 1
   ,a.change_contain_qt_rn       =  a.dept_people_cnt - b3.change_contain_qt_rn       + 1
   ,a.change_net_qlt_rn          =  a.dept_people_cnt - b4.change_net_qlt_rn          + 1
   ,a.group_contain_qt_rn        = tt.dept_people_cnt - b1.group_contain_qt_rn        + 1
   ,a.group_net_qlt_rn           = tt.dept_people_cnt - b2.group_net_qlt_rn           + 1
   ,a.group_change_contain_qt_rn = tt.dept_people_cnt - b3.group_change_contain_qt_rn + 1
   ,a.group_change_net_qlt_rn    = tt.dept_people_cnt - b4.group_change_net_qlt_rn    + 1
;



drop TEMPORARY table if exists tmp_dashboard_all_dept;
create TEMPORARY table tmp_dashboard_all_dept
select dept_id
from (
    select distinct dept_id from t_dashboard_red_list_rank
    union 
    select distinct dept_id from t_dashboard_black_list_rank
) a
;

alter table tmp_dashboard_all_dept add primary key(dept_id);



truncate table t_dashboard_red_black_ranklist_his;
insert into t_dashboard_red_black_ranklist_his(dept_id,apply_dept_id,red_list_rank_percentage,red_list_change_percentage
    ,black_list_rank_percentage,black_list_rank_condition,black_list_rank_lowest_score,black_list_change_percentage
    ,red_list_rank_info,red_list_change_info,black_list_rank_info,black_list_change_info)
select 
     x.dept_id
     ,x.apply_dept_id
    ,x.red_list_rank_percentage
    ,x.red_list_change_percentage
    ,x.black_list_rank_percentage
    ,x.black_list_rank_condition            -- 条件(1且，2或)
    ,x.black_list_rank_lowest_score
    ,x.black_list_change_percentage
    ,concat('绝对分数排名前',x.red_list_rank_percentage,'%')       red_list_rank_info      -- *绝对分数排名前30%
    ,concat('得分较上期增幅超',x.red_list_change_percentage,'%')   red_list_change_info    -- *得分较上期增幅超10%
    ,concat('最新一次考核绝对分数排名末',x.black_list_rank_percentage,'%'
            ,(case x.black_list_rank_condition when 1 then '且' when 2 then '或' end)
            ,'绝对分数低于',REGEXP_REPLACE(x.black_list_rank_lowest_score   -- 小数后面不要0
            ,'(\\.0+|0+)$', '') ,'分')                              black_list_rank_info    -- *最新一次考核绝对分数排名末10%且绝对分数低于75分
    ,concat('得分较上期跌幅超',x.black_list_change_percentage,'%') black_list_change_info  -- *得分较上期跌幅超10%
from (
    select 
        a.dept_id
        ,ifnull(t1.dept_id,t2.dept_id) apply_dept_id
        ,ifnull(t1.red_list_rank_percentage    ,t2.red_list_rank_percentage    ) red_list_rank_percentage    
        ,ifnull(t1.red_list_change_percentage  ,t2.red_list_change_percentage  ) red_list_change_percentage  
        ,ifnull(t1.black_list_rank_percentage  ,t2.black_list_rank_percentage  ) black_list_rank_percentage  
        ,ifnull(t1.black_list_rank_condition   ,t2.black_list_rank_condition   ) black_list_rank_condition   
        ,ifnull(t1.black_list_rank_lowest_score,t2.black_list_rank_lowest_score) black_list_rank_lowest_score
        ,ifnull(t1.black_list_change_percentage,t2.black_list_change_percentage) black_list_change_percentage
    from tmp_dashboard_all_dept a
    left join t_dashboard_red_black_ranklist t1
        on a.dept_id = t1.dept_id
        and t1.status = 0
        and t1.deleted = 0
    left join t_dashboard_red_black_ranklist t2
        on t2.dept_id = 1000  -- 有且仅有一条
) x
;



/*
# 公司绩效地图：
t_dashboard_company_map         看板-公司地图主表（部门是归属的分公司+总部） 总部的数据由【部门绩效管理】-【看板指标展示维护】生成
t_dashboard_company_map_idx     看板-公司地图指标表（部门是归属的分公司）
t_dashboard_company_map_idx_dtl 看板-公司地图指标明细表（部门是考核部门）
*/


-- 结果表
truncate table t_dashboard_dept_level;
  
-- 打开游标
OPEN cur;

-- 循环取值
FETCH cur INTO output_dept_id;
WHILE (@finished is NULL) DO
  -- 将字段值插入临时表
  -- select output_dept_id;
  
  -- 插入数据到结果表中
  insert into t_dashboard_dept_level(first_parent_id,dept_id, parent_id)
  WITH RECURSIVE subtree AS (
    select 
        output_dept_id first_parent_id,a.dept_id,a.parent_id 
    from sys_dept a
    where a.dept_id = output_dept_id
    UNION ALL
    SELECT 
            output_dept_id first_parent_id,t.dept_id, t.parent_id
    FROM sys_dept t
    INNER JOIN subtree s 
    ON t.parent_id = s.dept_id
  )
  SELECT first_parent_id,dept_id, parent_id
  FROM subtree
  ;
  
  
-- 继续获取下一个字段值
FETCH cur INTO output_dept_id;
END WHILE;

-- 关闭游标
CLOSE cur;


/*
-- 暂时用不上
drop TEMPORARY table if exists tmp_dashboard_dept_level;
create TEMPORARY table tmp_dashboard_dept_level
select dept_id from t_dashboard_dept_level
group by dept_id 
having count(1) > 1
;

update t_dashboard_dept_level
set status = 1 -- 该字段暂时没有用
    ,remark = '西部证券下属的分公司中已包含'
where dept_id in (
select dept_id from tmp_dashboard_dept_level
)
and first_parent_id = 99800 -- 申万宏源西部证券有限公司
;
*/

update t_dashboard_dept_level a
inner join sys_dept d 
on a.first_parent_id = d.dept_id 
set a.first_org_type = d.org_type
;

/*
select dept_id,count(1) 
from t_dashboard_dept_level
where status = 0
group by dept_id 
having count(1) > 1
*/



-- 当前
drop TEMPORARY table if exists tmp_dashboard_current_dept_cnt;
create TEMPORARY table tmp_dashboard_current_dept_cnt
select dept_id,count(1) current_dept_cnt from sys_user 
where account_status = 0
group by dept_id
;

alter table tmp_dashboard_current_dept_cnt add primary key(dept_id);


-- 去年12月,也就是年初
drop TEMPORARY table if exists tmp_dashboard_beginnig_year_dept_cnt;
create TEMPORARY table tmp_dashboard_beginnig_year_dept_cnt
select dept_id,count(1) beginnig_year_dept_cnt from t_employee_his
where year_months = DATE_FORMAT(DATE_SUB(current_date, INTERVAL 1 YEAR), '%Y12')
and account_status = 0
group by dept_id
;

alter table tmp_dashboard_beginnig_year_dept_cnt add primary key(dept_id);


-- 按照第一部门(即分公司)汇总
truncate table t_dashboard_company_map;
insert into t_dashboard_company_map(dept_id,current_dept_cnt,beginnig_year_dept_cnt)
select 
    a.first_parent_id dept_id
    ,ifnull(sum(t1.current_dept_cnt      ),0) current_dept_cnt
    ,ifnull(sum(t2.beginnig_year_dept_cnt),0) beginnig_year_dept_cnt
from t_dashboard_dept_level a 
left join tmp_dashboard_current_dept_cnt t1 
on a.dept_id = t1.dept_id 
left join tmp_dashboard_beginnig_year_dept_cnt t2
on a.dept_id = t2.dept_id 
group by a.first_parent_id
;

update t_dashboard_company_map a
inner join sys_dept d 
on a.dept_id = d.dept_id 
set a.org_type = d.org_type
;




drop TEMPORARY table if exists tmp_dashboard_branch_total_cnt;
create TEMPORARY table tmp_dashboard_branch_total_cnt
select 
    sum(current_dept_cnt)        current_branch_total_cnt
    ,sum(case when dept_id in (11600,11800) or org_type = 6 then current_dept_cnt else 0 end ) current_wealth_total_cnt  -- 财富条线总人数
    ,sum(beginnig_year_dept_cnt) beginnig_year_branch_total_cnt
from t_dashboard_company_map
where dept_id not in (99100,99200,99400) -- 甘肃分公司、陕西分公司和宁夏分公司 上面有西部证券分公司,不统计到总人数中
;


-- 计算总人数和占比
update t_dashboard_company_map a 
,tmp_dashboard_branch_total_cnt b 
set a.current_branch_total_cnt = b.current_branch_total_cnt
    ,a.beginnig_year_branch_total_cnt = b.beginnig_year_branch_total_cnt
    ,a.people_percentage =  a.current_dept_cnt / case when (dept_id in (11600,11800) or org_type = 6) then b.current_wealth_total_cnt
                                                 else b.current_branch_total_cnt end * 100
;



truncate table t_dashboard_company_map_idx_dtl;
insert into t_dashboard_company_map_idx_dtl(year_months,assess_year,assess_pd,assess_pd_value,dept_id,source_dept_id
    ,user_id,index_code,current_tgt,index_value,index_source,rn_user_desc)
select 
     a.year_months
    ,a.assess_year
    ,a.assess_pd
    ,a.assess_pd_value
    ,t1.first_parent_id dept_id
    ,a.dept_id source_dept_id
    ,a.user_id
    ,a.index_code
    ,a.current_tgt
    ,a.index_value
    ,a.index_source
    -- 取最新的一条记录
    ,row_number() over(partition by t1.first_parent_id,a.user_id,a.index_code order by a.year_months desc) rn_user_desc
from t_emp_perf_cmplt_val a 
inner join t_dashboard_dept_level t1 
on a.dept_id = t1.dept_id
and t1.first_org_type = 6 -- 只统计所有分公司，总部由前台配置提供
where a.default_zero_flag = 0  -- 源系统没有值，默认为0的不统计
;

/*

select dept_id,user_id,index_code,assess_year,assess_pd_value,count(1) from t_emp_perf_cmplt_val
group by dept_id,user_id,index_code,assess_year,assess_pd_value
having count(1) > 1;

select * from t_emp_perf_cmplt_val
where dept_id = 22035
and user_id = 9174
and index_code = '22035_001_02_000'
*/


truncate table t_dashboard_company_map_idx;
insert into t_dashboard_company_map_idx(dept_id,index_code,sum_current_tgt,sum_index_value,avg_current_tgt,avg_index_value,finish_percentage)
select 
    a.dept_id
    ,a.index_code
    ,ifnull(sum(a.current_tgt),0)                        sum_current_tgt
    ,ifnull(sum(a.index_value),0)                        sum_index_value
    ,ifnull(sum(a.current_tgt)/b.current_dept_cnt,0)     avg_current_tgt
    ,ifnull(sum(a.index_value)/b.current_dept_cnt,0)     avg_index_value
    ,ifnull(sum(a.current_tgt)/sum(a.index_value)*100,0) finish_percentage
from t_dashboard_company_map_idx_dtl a
left join t_dashboard_company_map b 
on a.dept_id = b.dept_id
where rn_user_desc = 1 -- 取最新的一条记录
group by a.dept_id,a.index_code
;


-- 获得记录数(不需要记录)
select 0 into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;
