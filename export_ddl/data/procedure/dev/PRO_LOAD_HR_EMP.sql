drop procedure if exists PRO_LOAD_HR_EMP;
CREATE PROCEDURE `PRO_LOAD_HR_EMP`(IN BIZ_DATE DATE)
BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_LOAD_HR_EMP
         功能简述：   人员信息表转换
         参数：       BIZ_DATE  上个工作日
         注意事项：
         sys_user:
              status: 对应前台允许登录(0:允许登录，1：不允许登录)
              account_status: 对应前台员工状态
                     0: 正常(实体/虚拟)
                     1: 离职(实体)
                     2: 注销(实体/虚拟)
                     3: 组织调整(实体)
                     4: 人员类别变更临时状态
         t_employee:
              status: 员工状态，前台不显示，用于更新sys_user表
              current_flag: 记录状态
                          -2:历史状态_人员类别变更(emp和user表均更新，可恢复原账号)
                          -1:历史状态_离职(单独逻辑，离职后再入职，不可恢复原账号)
                           0:历史状态_组织调整(单独更新逻辑)
                           1:当前最新状态(emp和user表均更新)
                           2:中间状态_组织调整变更中(临时状态)
         状态对应关系：
              t_employee     t_employee sys_user        sys_user
              current_flag   status     account_status  status
              -2             2          2(类别变更)     1
              -1             1          1(离职)         1
              0              1          1(组织调整)     0/1
              1              0          0(正常)         0/1
         账户状态转换：
                正常状态 -> 离职/组织调整/人员类别无效
                离职/组织调整，不能转其他状态，会重新生成新用户
                人员类别无效 -> 在职/离职
         数据源：
                 1、 int_d_new_hrs_emp          人员信息表
                 2、 sys_dept                   部门表(依赖部门表)
                 3、 t_appraisal_group_member   考核组成员表
                 4、 t_kpm_define               kpm定义表
         目标表：
                 1、 int_d_new_hrs_emp_chain    人员信息拉链表
                 2、 t_employee                 员工表
                 3、 sys_role                   角色表
                 4、 sys_user                   用户表
                 5、 sys_user_role              用户角色关系表
                 6、 t_appraisal_member/t_appraisal_dept/t_appraisal_dept_magr 考核权表
                 7、 t_assess_dtl               考核方案明细
                 8、 t_kpm_task                 kpm任务表
                 9、 t_record_last_password     记录最后一次密码
                 
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         CZB         2023/02/14                 创建
         MG          2023/03/01                 t_employee去掉sys_user_id自增,用sys_user中user_id，否则id会有冲突
                                                sys_user新增依赖于t_employee,用sys_user_id、user_id关联
                                                后续sys_user更新依赖于员工号更新，一个员工号在职状态只能有一条记录
         MG          2023/03/02                 job_rank更新，已离职人员账号状态刷成离职,emp表数据过滤掉2022年12月31号前离职的员
         MG          2023/03/08-09              员工切换部门后，保留上次密码bug修复
         MG          2023/03/10                 修复员工是否允许登录和员工状态不正确的问题
         MG          2023/03/28                 新增考核权管理部门表
         MG          2023/03/29                 卫星营业部 考核部门和管理部门需要映射到中心营业部上面
         MG          2023/05/08                 离职人员注销虚拟账户 子查询中状态应该用account_status
                                                用户表新增入职日期和离职日期字段
                                                离职人员 考核权、考核方案注销
                                                2023/03/02 bug修复
         MG          2023/05/10                 考核权开始日期调整，考核部门和考核管理部门手动修改后不插入
                                                考核权生效日期，用员工入职日期的当月第一天，2023年之前入职记为2023-01-01
         MG          2023/05/12                 切换部门和离职del_flag=0，job暂时不更新
         MG          2023/05/15                 有重复工号时，将当前的职务置为历史职务(临时，等版本升级后剔除)
         MG          2023/05/16                 员工表、用户表新增/修改remark字段；员工表不能剔除在职状态的记录；离退人员添加删除标识，不加入到考核权中
         MG          2023/05/18                 员工表变更部门后，自增id 需要考虑用户表的user_id新增，因为有虚拟用户的插入
         MG          2023/05/19                 注销离职人员考核方案信息 逻辑调整
         MG          2023/06/01                 离退人员前台显示；补充遗漏的row_number；去掉job更新的逻辑
         MG          2023/06/08                 用户表新增账户的状态3(组织调整)；离退人员考核权注销时间改为1号
         MG          2023/07/05                 考核权相关表，均不对虚拟账户初始化数据
         MG          2023/07/19                 允许登录状态：有工号新增的用户不改变(即切换部门的人员)，完全新的用户默认为0
                                                新增kpm模块功能
         MG          2023/07/20                 入职日期用Join_Begin_Dt字段，而不是join_dt
         MG          2023/07/21                 上次密码更新逻辑调整
         MG          2023/08/04                 考核权生效日期调整：15号之前(包含15号)，考核权在当月，超过15号记在下个月
         MG          2023/08/07                 离职再入职人员，当新员工逻辑处理：新增默认的考核权；退休人员表清理
         MG          2023/09/26                 新入职人员kpm提醒，离职用户不再反复更新备注字段
         MG          2023/09/28                 二级部门名称中【-】及前面的内容删除,注：暂时注释掉新入职人员kpm提醒的代码
         MG          2023/10/18                 员工表新增9个字段
         MG          2023/10/25                 员工离职后，员工表当前状态改为-1，适用离职再入职的场景
                                                员工拉链表起始和结束时间逻辑调整
                                                年龄、工龄随int表更新
         MG          2023/10/26                 拉链表去掉部门的限制，新入职kpm提醒开启
         MG          2023/11/01                 人员离职后，会多插记录到员工表的bug修复；离职后更新用户表账户状态
         MG          2023/12/11                 离职后再入职，考核权插入存在问题
         MG          2023/12/21                 新转入人员也加入到kpm
         MG          2023/12/22                 处理【其他】部门，编码：11002；剔除人员类型为：0106外派人员、0402博士后、0302退休返聘人员
         MG          2023/12/25                 人员类别恢复到之前的状态
         MG          2024/02/28                 到现岗时间<='1900-01-01'默认成null值
         MG          2024/03/14                 重新启动该存储过程；人员编码只保留【0101】类别
         MG          2024/03/19                 格式调整，人员类别变更处理
         MG          2024/03/20                 测试数据，并修改代码
         MG          2024/03/25                 注销用户注销时间逻辑调整,人员类别白名单
         MG          2024/03/26                 修改考核权中员工工号和姓名
         MG          2024/03/27                 离退人员从次月起应从考核组中移出
         MG          2024/03/29                 注销用户，员工表状态置为离职
         MG          2024/04/03                 离退人员KPM:充当直接汇报上级、考核人、调整项考核人、被考核人任何一种情况，都会提醒
                                                提醒角色新增：公司绩效管理员；白名单人员，员工状态为正常
                                                无考核关系人员也会提醒
         MG          2024/04/08                 考核权无效后，人员会自动剔除考核组
         MG          2024/04/24                 用户状态逻辑调整，登录状态暂时不修改
         MG          2024/04/25                 【允许登录状态】逻辑单独处理
         MG          2024/06/03                 修改用户默认密码
***************************************************************************************************************************************/

DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);

-- 设置变量
set V_EVENT_NAME = '人员信息表转换';
set V_RUN_COMMAND = concat('call PRO_LOAD_HR_EMP(',BIZ_DATE,')');
select now() into V_START_TIME;
/******************* segment1：员工拉链表 ***********************/
-- 情况1：部分字段内容发生变化
-- 1、 针对变更的数据(这里只考虑部门),做更新处理:更新结束日期、数据更新时间  
UPDATE int_d_new_hrs_emp_chain chain
LEFT JOIN int_d_new_hrs_emp emp 
ON chain.Pk_Psndoc = emp.Pk_Psndoc -- Pk_Psndoc一致，说明是同一条记录  
SET chain.end_date = date_sub(current_date, INTERVAL 1 DAY),
 chain.current_flag = 2 -- 表示需要后面更新  
WHERE chain.current_flag = 1
and ( 
    chain.Emp_Id           <> emp.Emp_Id       
    or chain.Emp_Name      <> emp.Emp_Name     
    or chain.Cert_Id       <> emp.Cert_Id      
    or chain.Sex           <> emp.Sex          
    or chain.Ethnic_Cd     <> emp.Ethnic_Cd    
    or chain.Native_Place  <> emp.Native_Place 
    or chain.Birth_Dt      <> emp.Birth_Dt     
    or chain.Census_Addr   <> emp.Census_Addr  
    or chain.Family_Addr   <> emp.Family_Addr  
    or chain.MOBILE        <> emp.MOBILE       
    or chain.Email         <> emp.Email        
    or chain.EDU           <> emp.EDU          
    or chain.Grad_School   <> emp.Grad_School  
    or chain.Grad_Dt       <> emp.Grad_Dt      
    or chain.Work_dt       <> emp.Work_dt      
    or chain.Poli_Status   <> emp.Poli_Status  
    or chain.Join_Dt       <> emp.Join_Dt      
    or chain.Retire_Dt     <> emp.Retire_Dt    
    or chain.Org_Code      <> emp.Org_Code     
    or chain.Join_Begin_Dt <> emp.Join_Begin_Dt
    or chain.Join_End_Dt   <> emp.Join_End_Dt  
    or chain.Is_Cadre      <> emp.Is_Cadre     
    or chain.Job_Type      <> emp.Job_Type     
    or chain.Job_Name      <> emp.Job_Name     
    or chain.Job_Rank      <> emp.Job_Rank     
    or chain.Emp_Status    <> emp.Emp_Status   
    or chain.Post          <> emp.Post         
    or chain.Org_Sec_Code  <> emp.Org_Sec_Code 
    or chain.Org_Sec_Name  <> emp.Org_Sec_Name 
)
;
-- 2、针对变更的数据,做新增数据处理  
INSERT INTO int_d_new_hrs_emp_chain (
    Pk_Psndoc,Emp_Id,Emp_Name,Cert_Id,Sex,Ethnic_Cd,Native_Place,Birth_Dt,Census_Addr,Family_Addr,MOBILE,Email,EDU,Grad_School,Grad_Dt
    ,Work_dt,Poli_Status,Join_Dt,Retire_Dt,Org_Code,Join_Begin_Dt,Join_End_Dt,Is_Cadre,Job_Type,Job_Name,Job_Rank,Emp_Status,Post,Org_Sec_Code
    ,Org_Sec_Name,start_date,end_date,current_flag
) SELECT
     emp.Pk_Psndoc,emp.Emp_Id,emp.Emp_Name,emp.Cert_Id,emp.Sex,emp.Ethnic_Cd,emp.Native_Place,emp.Birth_Dt,emp.Census_Addr,emp.Family_Addr,emp.MOBILE
    ,emp.Email,emp.EDU,emp.Grad_School,emp.Grad_Dt,emp.Work_dt,emp.Poli_Status,emp.Join_Dt,emp.Retire_Dt,emp.Org_Code,emp.Join_Begin_Dt,emp.Join_End_Dt
    ,emp.Is_Cadre,emp.Job_Type,emp.Job_Name,emp.Job_Rank,emp.Emp_Status,emp.Post,emp.Org_Sec_Code,emp.Org_Sec_Name
    ,current_date start_date
    ,'0001-01-01' end_date
    ,1 current_flag
FROM int_d_new_hrs_emp emp
INNER JOIN int_d_new_hrs_emp_chain chain 
ON chain.Pk_Psndoc = emp.Pk_Psndoc -- 供应商名称一致，说明是同一条记录  
where chain.current_flag = 2
;
-- 3、已更新完成,状态转为无效  
UPDATE int_d_new_hrs_emp_chain
SET current_flag = 0
WHERE current_flag = 2
;
-- 情况2：新增
INSERT INTO int_d_new_hrs_emp_chain (
     Pk_Psndoc,Emp_Id,Emp_Name,Cert_Id,Sex,Ethnic_Cd,Native_Place,Birth_Dt,Census_Addr,Family_Addr,MOBILE,Email,EDU,Grad_School,Grad_Dt,Work_dt
    ,Poli_Status,Join_Dt,Retire_Dt,Org_Code,Join_Begin_Dt,Join_End_Dt,Is_Cadre,Job_Type,Job_Name,Job_Rank,Emp_Status,Post,Org_Sec_Code,Org_Sec_Name
    ,start_date,end_date,current_flag
) SELECT
     emp.Pk_Psndoc,emp.Emp_Id,emp.Emp_Name,emp.Cert_Id,emp.Sex,emp.Ethnic_Cd,emp.Native_Place,emp.Birth_Dt,emp.Census_Addr,emp.Family_Addr,emp.MOBILE
    ,emp.Email,emp.EDU,emp.Grad_School,emp.Grad_Dt,emp.Work_dt,emp.Poli_Status,emp.Join_Dt,emp.Retire_Dt,emp.Org_Code,emp.Join_Begin_Dt,emp.Join_End_Dt
    ,emp.Is_Cadre,emp.Job_Type,emp.Job_Name,emp.Job_Rank,emp.Emp_Status,emp.Post,emp.Org_Sec_Code,emp.Org_Sec_Name
    ,current_date start_date
    ,'0001-01-01' end_date
    ,1 current_flag
FROM int_d_new_hrs_emp emp
LEFT JOIN int_d_new_hrs_emp_chain chain 
ON chain.Pk_Psndoc = emp.Pk_Psndoc
and chain.current_flag = 1  -- 当前状态下 
where chain.Pk_Psndoc is null
;

-- 情况3：删除
UPDATE int_d_new_hrs_emp_chain chain
LEFT JOIN int_d_new_hrs_emp emp 
ON chain.Pk_Psndoc = emp.Pk_Psndoc
SET chain.end_date = current_date,
 chain.current_flag = -1
WHERE chain.current_flag = 1
AND emp.Pk_Psndoc IS NULL
;

/*********************** segment2：员工表 ******************************/
/********************* 部门变更，历史数据保留 *********************/
-- 1. 员工新增的数据
insert into t_employee
(psndoc_id,-- 源系统人员id
sys_user_id, -- 系统用户id自增长
dept_id,-- 部门id
name,-- 姓名
code,-- 人员工号
status,-- 员工状态（0正常 1异常 ）
begindate,-- 入职日期
enddate,-- 离职日期
pk_org,-- 所属组织id
pk_group,-- 所属集团id
pk_hrorg,-- 人力组织id
iscadre,-- 是否干部
pk_job,-- 职务
sex,-- 性别
edu,-- 学历
birthdate,-- 出生日期
age,-- 年龄
workage,-- 工龄
marital,-- 婚姻状况
polit,-- 政治面貌
nationality,-- 民族
nativeplace,-- 籍贯
health,-- 健康状况
user_post_id,-- 人员岗位id
indutydate,-- 任职开始日期
enddutydate,-- 任职结束日期
email,-- 邮箱
ident_id,-- 证件号码
idtype,-- 证件类型
mobile,-- 手机
characterrpr,-- 户口性质
censusaddr,-- 户籍地址
create_time,-- 创建时间
Job_Rank, -- 职等
Post,  -- 岗位
Org_Sec_Code, -- 所属二级部门编码
Org_Sec_Name, -- 所属二级部门名称
remark,  -- 备注
Emp_Type_Code,          -- 人员类别编码
Emp_Type_Name,          -- 人员类别名称
Managercode,            -- 管理人员属性编码
Job_Appt_Dt,            -- 本岗位任职日期
Job_Rank_Appt_Dt,       -- 现职级任职开始日期
Enter_Type              -- 进入来源
)
select
    a.Pk_Psndoc psndoc_id,-- 源系统人员id
    (select max(greatest(m.max_user_id,n.max_user_id)) from (select max(user_id) max_user_id from sys_user) m,(select max(sys_user_id) max_user_id from t_employee) n)+row_number() over() sys_user_id,  -- 从sys_user获取最大的id
    (case a.Org_Code when 'ZQ0002' THEN 1000
    ELSE replace(a.Org_Code,'ZQ','9') end) dept_id,-- 部门id
    a.Emp_Name name,-- 姓名
    a.Emp_Id code,-- 人员工号
    (case a.Emp_Status when '4' then 1 else 0 end) status,-- 员工状态（0正常 1异常 ）
    a.Join_Dt begindate,-- 入职日期
    (case  when a.Emp_Status != '4' then null 
    when a.Retire_Dt = '0001-01-01' and a.Emp_Status = '4' then a.Join_End_Dt 
    else a.Retire_Dt end) enddate,-- 离职日期
    null pk_org,-- 所属组织id
    null pk_group,-- 所属集团id
    null pk_hrorg,-- 人力组织id
    a.Is_Cadre iscadre,-- 是否干部
    a.Job_Name pk_job,-- 职务
    a.Sex sex,-- 性别
    a.EDU edu,-- 学历
    a.Birth_Dt birthdate,-- 出生日期
    a.AGE age,-- 年龄
    a.Work_Age workage,-- 工龄
    null marital,-- 婚姻状况
    null polit,-- 政治面貌
    a.Ethnic_Cd nationality,-- 民族
    a.Native_Place nativeplace,-- 籍贯
    null  health,-- 健康状况
    null user_post_id,-- 人员岗位id
    a.Join_Begin_Dt indutydate,-- 任职开始日期
    a.Join_End_Dt enddutydate,-- 任职结束日期
    a.Email email,-- 邮箱
    a.Cert_Id ident_id,-- 证件号码
    null idtype,-- 证件类型
    a.MOBILE mobile,-- 手机
    null characterrpr,-- 户口性质
    a.Census_Addr censusaddr,-- 户籍地址
    sysdate() create_time,-- 创建时间
    a.Job_Rank Job_Rank, -- 职等
    a.Post Post,  -- 岗位
    a.Org_Sec_Code Org_Sec_Code, -- 所属二级部门编码
    -- a.Org_Sec_Name Org_Sec_Name, -- 所属二级部门名称
    if(a.Org_Sec_Name like '%-%',SUBSTRING_INDEX(a.Org_Sec_Name, '-', -1),a.Org_Sec_Name) Org_Sec_Name, -- 所属二级部门名称
    concat('系统初始化员工信息 ',sysdate()) remark, -- 备注
    a.Emp_Type_Code,          -- 人员类别编码
    a.Emp_Type_Name,          -- 人员类别名称
    a.Managercode,            -- 管理人员属性编码
    if(a.Job_Appt_Dt>='1900-01-01',a.Job_Appt_Dt,null) Job_Appt_Dt,            -- 本岗位任职日期
    a.Job_Rank_Appt_Dt,       -- 现职级任职开始日期
    a.Enter_Type              -- 进入来源
from int_d_new_hrs_emp a
left join t_employee b
on a.Pk_Psndoc = b.psndoc_id  -- 源系统人员id
  and (b.current_flag = 1   -- 在职不录入
       or (b.current_flag = -1 and a.Emp_Status = '4')  -- 离职后不在录入，离职再入职会统计
       or b.current_flag = -2  -- 人员类别变更不统计
      )
where b.psndoc_id is null -- 增加过滤已存在的 营业部编码，工号，证件号码 相同的账号
    AND ((
        a.Retire_Dt > '2022-12-31' 
        OR ( a.Retire_Dt = '0001-01-01' AND a.Join_End_Dt > '2022-12-31' ))
        OR a.Emp_Status != '4'
        )
    and not (a.Org_Code = '11002' and a.Org_Sec_Code != '110020001')  -- 其他部门 其他-博士后科研工作站
    -- and a.Emp_Type_Code in ('0101','0301','0410','0303','0404','0405','01','0401','04') -- 没有空值
    and a.Emp_Type_Code = '0101'
group by 
a.Pk_Psndoc
,(case a.Org_Code when 'ZQ0002' THEN 1000 ELSE replace(a.Org_Code,'ZQ','9') end)
,a.Emp_Name
,a.Emp_Id
,(case a.Emp_Status when '4' then 1 else 0 end)
,a.Join_Dt
,a.Retire_Dt
,a.Is_Cadre
,a.Job_Name
,a.Sex
,a.EDU
,a.Birth_Dt
,a.AGE -- 年龄
,a.Work_Age -- 工龄
,a.Ethnic_Cd
,a.Native_Place
,a.Join_Begin_Dt
,a.Join_End_Dt
,a.Email
,a.Cert_Id
,a.MOBILE
,a.Census_Addr
,a.Job_Rank
,a.Post
,a.Org_Sec_Code
,if(a.Org_Sec_Name like '%-%',SUBSTRING_INDEX(a.Org_Sec_Name, '-', -1),a.Org_Sec_Name)
,a.Emp_Type_Code          -- 人员类别编码
,a.Emp_Type_Name          -- 人员类别名称
,a.Managercode            -- 管理人员属性编码
,if(a.Job_Appt_Dt>='1900-01-01',a.Job_Appt_Dt,null)            -- 本岗位任职日期
,a.Job_Rank_Appt_Dt       -- 现职级任职开始日期
,a.Enter_Type              -- 进入来源
;

-- 2. 结果表中标记变更部门
update t_employee b
inner join int_d_new_hrs_emp a
on b.psndoc_id = a.Pk_Psndoc  -- 源系统人员id
  and b.dept_id <> (case a.Org_Code when 'ZQ0002' THEN 1000 ELSE replace(a.Org_Code,'ZQ','9') end)  -- 部门变更
  and b.current_flag = 1  -- 与最新数据对比
set b.status = 1  -- 员工状态，停用
    ,b.enddutydate = date_sub(a.Join_Begin_Dt, interval 1 day)   -- 任职结束日期，取当前任职结束日期的前一天
    ,b.current_flag = 2   -- 标记该数据需要更新
    ,b.remark = concat(ifnull(b.remark,''), '部门变更',sysdate()) -- 备注
;
-- 3. 将变更部门的数据插入结果表
insert into t_employee
(psndoc_id,-- 源系统人员id
sys_user_id, -- 系统用户id自增长
dept_id,-- 部门id
name,-- 姓名
code,-- 人员工号
status,-- 员工状态（0正常 1异常 ）
begindate,-- 入职日期
enddate,-- 离职日期
pk_org,-- 所属组织id
pk_group,-- 所属集团id
pk_hrorg,-- 人力组织id
iscadre,-- 是否干部
pk_job,-- 职务
sex,-- 性别
edu,-- 学历
birthdate,-- 出生日期
age,-- 年龄
workage,-- 工龄
marital,-- 婚姻状况
polit,-- 政治面貌
nationality,-- 民族
nativeplace,-- 籍贯
health,-- 健康状况
user_post_id,-- 人员岗位id
indutydate,-- 任职开始日期
enddutydate,-- 任职结束日期
email,-- 邮箱
ident_id,-- 证件号码
idtype,-- 证件类型
mobile,-- 手机
characterrpr,-- 户口性质
censusaddr,-- 户籍地址
create_time,-- 创建时间
Job_Rank, -- 职等
Post,  -- 岗位
Org_Sec_Code, -- 所属二级部门编码
Org_Sec_Name, -- 所属二级部门名称
current_flag,  -- 当前标识
remark,
Emp_Type_Code,          -- 人员类别编码
Managercode,            -- 管理人员属性编码
Job_Appt_Dt,            -- 本岗位任职日期
Job_Rank_Appt_Dt,       -- 现职级任职开始日期
Enter_Type              -- 进入来源
)
select
    a.Pk_Psndoc psndoc_id,-- 源系统人员id
    (select max(greatest(m.max_user_id,n.max_user_id)) from (select max(user_id) max_user_id from sys_user) m,(select max(sys_user_id) max_user_id from t_employee) n)+row_number() over() sys_user_id,  -- 从sys_user获取最大的id
    (case a.Org_Code when 'ZQ0002' THEN 1000
    ELSE replace(a.Org_Code,'ZQ','9') end) dept_id,-- 部门id
    a.Emp_Name name,-- 姓名
    a.Emp_Id code,-- 人员工号
    (case a.Emp_Status when '4' then 1 else 0 end) status,-- 员工状态（0正常 1异常 ） 员工当前的状态
    a.Join_Dt begindate,-- 入职日期
    -- a.Retire_Dt enddate,-- 离职日期
    (case  when a.Emp_Status != '4' then null 
    when a.Retire_Dt = '0001-01-01' and a.Emp_Status = '4' then a.Join_End_Dt 
    else a.Retire_Dt end) enddate,-- 离职日期
    null pk_org,-- 所属组织id
    null pk_group,-- 所属集团id
    null pk_hrorg,-- 人力组织id
    a.Is_Cadre iscadre,-- 是否干部
    a.Job_Name pk_job,-- 职务
    a.Sex sex,-- 性别
    a.EDU edu,-- 学历
    a.Birth_Dt birthdate,-- 出生日期
    a.AGE age,-- 年龄
    a.Work_Age workage,-- 工龄
    null marital,-- 婚姻状况
    null polit,-- 政治面貌
    a.Ethnic_Cd nationality,-- 民族
    a.Native_Place nativeplace,-- 籍贯
    null  health,-- 健康状况
    null user_post_id,-- 人员岗位id
    a.Join_Begin_Dt indutydate,-- 任职开始日期
    a.Join_End_Dt enddutydate,-- 任职结束日期
    a.Email email,-- 邮箱
    a.Cert_Id ident_id,-- 证件号码
    null idtype,-- 证件类型
    a.MOBILE mobile,-- 手机
    null characterrpr,-- 户口性质
    a.Census_Addr censusaddr,-- 户籍地址
    sysdate() create_time,-- 创建时间
    a.Job_Rank Job_Rank, -- 职等
    a.Post Post,  -- 岗位
    a.Org_Sec_Code Org_Sec_Code, -- 所属二级部门编码
    -- a.Org_Sec_Name Org_Sec_Name, -- 所属二级部门名称
    if(a.Org_Sec_Name like '%-%',SUBSTRING_INDEX(a.Org_Sec_Name, '-', -1),a.Org_Sec_Name) Org_Sec_Name, -- 所属二级部门名称
    1 current_flag, -- 当前标识
    concat('变更部门后的员工信息 ',sysdate()) remark, -- 备注
    a.Emp_Type_Code,          -- 人员类别编码
    a.Managercode,            -- 管理人员属性编码
    if(a.Job_Appt_Dt>='1900-01-01',a.Job_Appt_Dt,null) Job_Appt_Dt,            -- 本岗位任职日期
    a.Job_Rank_Appt_Dt,       -- 现职级任职开始日期
    a.Enter_Type              -- 进入来源
from int_d_new_hrs_emp a  -- 当前的
inner join t_employee b  -- 原始的
  on a.Pk_Psndoc = b.psndoc_id  -- 源系统人员id
  and b.current_flag = 2  -- 新增的
group by 
a.Pk_Psndoc
,(case a.Org_Code when 'ZQ0002' THEN 1000 ELSE replace(a.Org_Code,'ZQ','9') end)
,a.Emp_Name
,a.Emp_Id
,(case a.Emp_Status when '4' then 1 else 0 end)
,a.Join_Dt
,a.Retire_Dt
,a.Is_Cadre
,a.Job_Name
,a.Sex
,a.EDU
,a.Birth_Dt
,a.AGE -- 年龄
,a.Work_Age -- 工龄
,a.Ethnic_Cd
,a.Native_Place
,a.Join_Begin_Dt
,a.Join_End_Dt
,a.Email
,a.Cert_Id
,a.MOBILE
,a.Census_Addr
,a.Job_Rank
,a.Post
,a.Org_Sec_Code
,if(a.Org_Sec_Name like '%-%',SUBSTRING_INDEX(a.Org_Sec_Name, '-', -1),a.Org_Sec_Name)
,a.Emp_Type_Code          -- 人员类别编码
,a.Emp_Type_Name          -- 人员类别名称
,a.Managercode            -- 管理人员属性编码
,if(a.Job_Appt_Dt>='1900-01-01',a.Job_Appt_Dt,null)            -- 本岗位任职日期
,a.Job_Rank_Appt_Dt       -- 现职级任职开始日期
,a.Enter_Type             -- 进入来源
;
-- 4. 状态修改为0
update t_employee
set current_flag = 0
where current_flag = 2
;
-- 更新t_employee表
update t_employee a 
inner join int_d_new_hrs_emp b 
   on a.psndoc_id = b.Pk_Psndoc   -- 源系统人员id
   and a.current_flag in (-2, 1) -- 更新最新状态和人员类别变更
left join t_employee_white_list wl -- 如果加入到白名单中，将不会把状态改为-2
   on a.code = wl.user_name
set 
    a.name = b.Emp_Name -- 姓名
   ,a.code = b.Emp_Id -- 人员工号
   ,a.status = (case when wl.id is not null then 0  -- 白名单状态为0
                when b.Emp_Status = '4' or ifnull(b.Emp_Type_Code,'') != '0101' then 1
                else 0 end
               ) -- 员工状态（0正常 1异常 ） 员工当前的状态
   ,a.current_flag = (case when b.Emp_Status ='4' then -1 
                           when ifnull(b.Emp_Type_Code,'') != '0101' and wl.id is null then -2
                           else 1 end)
   ,a.begindate = b.Join_Dt -- 入职日期
   ,a.enddate = (case when b.Emp_Status != '4' then null 
                when b.Retire_Dt = '0001-01-01' and b.Emp_Status = '4' then b.Join_End_Dt 
                else b.Retire_Dt end) -- 离职日期
   ,a.iscadre = b.Is_Cadre -- 是否干部
   ,a.pk_job = b.Job_Name -- 职务
   ,a.sex = b.Sex -- 性别
   ,a.edu = b.EDU -- 学历
   ,a.birthdate = b.Birth_Dt -- 出生日期
   ,a.age = b.AGE-- 年龄
   ,a.workage = b.Work_Age -- 工龄
   ,a.nationality = b.Ethnic_Cd -- 民族
   ,a.nativeplace = b.Native_Place -- 籍贯
   ,a.indutydate = b.Join_Begin_Dt -- 任职开始日期
   ,a.enddutydate = b.Join_End_Dt -- 任职结束日期
   ,a.email = b.Email -- 邮箱
   ,a.ident_id = b.Cert_Id -- 证件号码
   ,a.mobile = b.MOBILE -- 手机
   ,a.censusaddr = b.Census_Addr -- 户籍地址
   ,a.Job_Rank = b.Job_Rank -- 职等
   ,a.Post = b.Post  -- 岗位
   ,a.Org_Sec_Code = b.Org_Sec_Code -- 所属二级部门编码
   ,a.Org_Sec_Name= if(b.Org_Sec_Name like '%-%',SUBSTRING_INDEX(b.Org_Sec_Name, '-', -1),b.Org_Sec_Name)  -- 所属二级部门名称
   ,a.Emp_Type_Code       =    b.Emp_Type_Code      -- 人员类别编码
   ,a.Emp_Type_Name       =    b.Emp_Type_Name      -- 人员类别名称
   ,a.Managercode         =    b.Managercode        -- 管理人员属性编码
   ,a.Job_Appt_Dt         =    if(b.Job_Appt_Dt>='1900-01-01',b.Job_Appt_Dt,null)        -- 本岗位任职日期
   ,a.Job_Rank_Appt_Dt    =    b.Job_Rank_Appt_Dt   -- 现职级任职开始日期
   ,a.Enter_Type          =    b.Enter_Type         -- 进入来源
;


/******************* segment3：用户表 ***********************/
-- 新增员工，初始化到用户表
insert into sys_user
    (user_id, -- 用户id
     dept_id, -- 部门id
     user_name, -- 用户账号
     nick_name, -- 用户昵称
     user_type, -- 用户类型（00系统用户）
     email, -- 用户邮箱
     phonenumber, -- 手机号码
     sex, -- 用户性别（0男 1女 2未知）
     avatar, -- 头像地址
     password, -- 密码
     status, -- 帐号状态（0正常 1停用）
     del_flag, -- 删除标志（0代表存在 2代表删除）
     login_ip, -- 最后登录ip
     login_date, -- 最后登录时间
     create_by, -- 创建者
     create_time, -- 创建时间
     update_by, -- 更新者
     update_time, -- 更新时间
     remark, -- 备注
     account_status,-- 状态(0正常 1离职 2废弃)
     account_flag,-- 工号标识(1实体 | 2虚拟)
     belonging_user_name,-- 所属用户账号
     last_chgpwdtime,-- 上次修改密码时间
     post, -- 岗位
     org_sec_code, -- 所属二级部门编码
     org_sec_name, -- 所属二级部门名称
     begindate, -- 入职日期
     enddate -- 离职日期
     )
    select a.sys_user_id user_id, -- 系统用户id
           a.dept_id dept_id, -- 部门id
           a.code user_name, -- 人员工号
           a.name nick_name, -- 姓名
           '00' user_type, -- 用户类型（00系统用户）
           null email, -- 用户邮箱
           a.mobile phonenumber, -- 手机号码
           a.sex sex, -- 用户性别（0男 1女 2未知）
           null avatar, -- 头像地址
           '$2a$10$BGw3osZD8hp8JiumC1CdB.JeNlV98SIATHLf/hUjha.muffsYWkNK' password, -- 密码，默认密码：Rljx_2023
           if(c.user_id is null,'0','1') status, -- 帐号状态（0正常 1停用） 是否允许登录
           '0' del_flag, -- 删除标志（0代表存在 2代表删除）
           null login_ip, -- 最后登录ip
           null login_date, -- 最后登录时间
           1 create_by, -- 创建者
           sysdate() create_time, -- 创建时间
           null update_by, -- 更新者
           null update_time, -- 更新时间
           concat('系统新增用户',sysdate()) remark, -- 备注
           if(a.status='0','0','1') account_status, -- 状态(0正常 1离职(离退) 2废弃 3组织调整) 员工状态
           1 account_flag,-- 工号标识(1实体 | 2虚拟)  虚拟用户是系统新增的，初始化不处理
           null belonging_user_name,-- 所属用户账号
           null last_chgpwdtime,-- 上次修改密码时间
           a.post, -- 岗位
           a.org_sec_code, -- 所属二级部门编码
           a.org_sec_name, -- 所属二级部门名称
           a.indutydate begindate, -- 入职日期(Join_Begin_Dt)
           a.enddate -- 离职日期
      from t_employee a
      left join sys_user b
        on a.sys_user_id = b.user_id -- 增加过滤已存在的营业部编码，工号，证件号码相同的账号
      left join sys_user c   -- 相同工号允许登录至多1条，前端有限制(如果该工号不是第一次新增，允许登录默认为1)
        on a.code = c.user_name 
        and c.status = '0'
     where b.user_id is null
     and a.current_flag = 1 -- 只取当前状态的数据
;
-- 初始化用户角色信息
insert into sys_user_role(user_id,role_id)
select sys_user_id,2003 role_id
from t_employee a
where not exists(select 1 from sys_user_role b where a.sys_user_id = b.user_id)
;
-- 员工切换部门，原纪录置为状态3
update sys_user a
inner join t_employee b 
    on a.user_name = b.code -- 人员工号
    and b.current_flag = 1 -- 当前状态
    and a.dept_id <> b.dept_id -- 部门id
set a.account_status = 3, -- 状态(0正常 1离职 2废弃 3组织调整)
    a.update_by = 1, 
    a.update_time = sysdate(), 
    a.remark = concat('员工切换部门：',a.dept_id,'->',b.dept_id,'，切换时间：',sysdate())
where a.account_status = 0 -- 状态(0正常 1离职 2废弃)
;

-- 更新sys_user表
-- 情况1：用户状态正常，员工表状态发生了变化
update sys_user b 
inner join t_employee a
on a.sys_user_id = b.user_id
    and a.current_flag in (-2, -1, 1) -- 只取当前状态的数据,禁用用户不再更新；离职用户不再更新
    and b.account_status = 0 -- 状态(0正常 1离职 2废弃)
set
    b.dept_id = a.dept_id-- 部门id
    ,b.user_name = a.code-- 人员工号
    ,b.nick_name = a.name-- 姓名
    ,b.phonenumber = a.mobile-- 手机号码
    ,b.sex = a.sex-- 用户性别（0男 1女 2未知）
    ,b.account_status = case when a.current_flag = -2 then 2 -- 人员类别变更->注销
                            when a.current_flag = -1 then 1 -- 人员离职->离职
                            else 0
                       end   -- 帐号状态
    ,b.Post = a.Post  -- 岗位
    ,b.Org_Sec_Code = a.Org_Sec_Code -- 所属二级部门编码
    ,b.Org_Sec_Name = a.Org_Sec_Name -- 所属二级部门名称
    ,b.begindate = a.indutydate -- 入职日期(Join_Begin_Dt)
    ,b.enddate   = a.enddate -- 离职日期
    ,b.remark = case when a.current_flag = -1 then concat('离退人员，操作时间:',sysdate())
                     when a.current_flag = -2 then concat('注销人员(人员类别变更为无效)，操作时间：',sysdate())
                else b.remark end    -- 备注
;

-- 情况2：用户状态为注销，员工表状态发生了变化
update sys_user b 
inner join t_employee a
on a.sys_user_id = b.user_id
    and a.current_flag in (-2, -1, 1)
    and b.account_status = 2 -- 状态(0正常 1离职 2废弃)
set
    b.dept_id = a.dept_id-- 部门id
    ,b.user_name = a.code-- 人员工号
    ,b.nick_name = a.name-- 姓名
    ,b.phonenumber = a.mobile-- 手机号码
    ,b.sex = a.sex-- 用户性别（0男 1女 2未知）
    ,b.account_status = case when a.current_flag = -1 then 1 -- 人员离职->离职
                            when a.current_flag = 1 then 4 -- 当前状态->正常 临时状态
                            else 2
                       end   -- 帐号状态
    ,b.Post = a.Post  -- 岗位
    ,b.Org_Sec_Code = a.Org_Sec_Code -- 所属二级部门编码
    ,b.Org_Sec_Name = a.Org_Sec_Name -- 所属二级部门名称
    ,b.begindate = a.indutydate -- 入职日期(Join_Begin_Dt)
    ,b.enddate   = a.enddate -- 离职日期
    ,b.remark = case when a.current_flag = -1 then concat('离退人员，操作时间:',sysdate())
                     when a.current_flag = 1 then concat('注销人员恢复，操作时间：',sysdate())
                else b.remark end    -- 备注
;


-- 修改用户表登录状态
drop TEMPORARY TABLE if exists `tmp_no_transferred_user`;
CREATE TEMPORARY TABLE `tmp_no_transferred_user` (
  `new_user_id` bigint DEFAULT '0' COMMENT '用户ID',
  `old_user_id` bigint DEFAULT NULL COMMENT '用户id',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '用户账号',
  `nick_name` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci COMMENT '用户昵称',
  `old_dept_id` bigint DEFAULT NULL COMMENT '部门id',
  `new_dept_id` bigint DEFAULT NULL COMMENT '部门ID',
  `status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '帐号状态（0正常 可以登录 1停用 不可登录）',
  KEY idx_new_user_id(new_user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '未转入部门的人员'
;

insert into tmp_no_transferred_user(new_user_id,old_user_id,user_name,nick_name,old_dept_id,new_dept_id,status)
SELECT
    t1.user_id new_user_id,
    t.user_id old_user_id,
    t1.user_name,
    t1.nick_name,
    t.dept_id old_dept_id,
    t1.dept_id new_dept_id,
    t1.status
FROM
(
    SELECT
        a.user_id,
        a.user_code,
        a.user_name,
        a.dept_id
    FROM
        t_appraisal_member a 
    WHERE
        a.user_status = 0 
        and a.deleted = 0
        AND EXISTS ( SELECT 1 FROM sys_user b WHERE b.del_flag = '0' and a.user_code = b.user_name AND B.account_status = 3 )
        AND EXISTS ( SELECT 1 FROM sys_user c WHERE c.del_flag = '0' and a.user_code = c.user_name and a.user_id != c.user_id and c.account_status = 0 )
) t
LEFT JOIN sys_user t1 
ON t.user_code = t1.user_name 
    AND t1.account_status = '0'
    and t1.del_flag = '0'
where t1.status = '1'
;

/**************************************************************************************
                         单独修改允许登录状态
如果人员未转入，登录状态不会更新，否则其他情况下，会根据emp表状态更新
这里其他情况：目前只有人员类别从【其他->0101】这种
**************************************************************************************/
update sys_user a 
inner join t_employee b
on a.user_id = b.sys_user_id
   and a.account_status = '0'
   and a.status = '1'
   and b.current_flag = 1
   and b.status = '0'
left join tmp_no_transferred_user tn  -- 未转入部门的人员
   on a.user_id = tn.new_user_id
set a.status = case when tn.new_user_id is null then '0' else a.status end
;

-- 【离职】、【类别变更】：会把登录改成【禁止登录】
update sys_user a 
inner join t_employee b
on a.user_id = b.sys_user_id
   and a.account_status in (1,2)
   and a.status = '0'
   and b.current_flag in (-2,-1)
   and b.status = '1'
set a.status = '1'
;




-- 离职人员注销其虚拟账户
update sys_user a -- 虚拟账户
inner join sys_user b -- 虚拟账户对应的主账户
    on a.belonging_user_name = b.user_name
    and a.account_status = 0
    and a.account_flag = 2 -- 账户类型(1:实体账户,2:虚拟账户)
    and b.account_flag = 1 -- 账户类型(1:实体账户,2:虚拟账户)
    and b.account_status = 1 -- 账户状态(0正常 1离职 2废弃)
set a.begindate = b.begindate,
    a.enddate = b.enddate,
    a.account_status = 2, -- 状态(0正常 1离职 2废弃)
    a.update_by = 1, 
    a.update_time = sysdate(), 
    a.remark = concat('员工离职注销虚拟账户，注销时间：',sysdate())
;

-- 注销人员，注销虚拟账户
update sys_user a -- 虚拟账户
inner join sys_user b -- 虚拟账户对应的主账户
    on a.belonging_user_name = b.user_name
    and a.account_flag = 2 -- 账户类型(1:实体账户,2:虚拟账户)
    and b.account_flag = 1 -- 账户类型(1:实体账户,2:虚拟账户)
    and b.account_status = 2 -- 账户状态(0正常 1离职 2废弃)
set a.begindate = b.begindate,
    a.enddate = b.enddate,
    a.account_status = 2, -- 状态(0正常 1离职 2废弃)
    a.update_by = 1,
    a.update_time = sysdate(), 
    a.remark = concat('注销人员，注销虚拟账户，注销时间：',sysdate())
;



-- 记录用户最后一次更新的密码(只记录首次切换部门的密码)
insert into t_record_last_password(user_id,user_name,last_password,last_chgpwdtime)
select 
    a.user_id,a.user_name,a.last_password,a.last_chgpwdtime 
from (
    select 
        row_number() over(PARTITION by user_name order by create_time,user_id desc) nr
        ,user_id
        ,user_name
        ,password last_password
        ,last_chgpwdtime
    from sys_user a
    where account_status = 3 -- 状态(0正常 1离职 2废弃 3组织调整)
    and last_chgpwdtime is not null -- 上次修改密码时间不能为空
) a 
left join t_record_last_password b 
    on a.user_id = b.user_id 
where b.user_id is null
    and a.nr = 1  -- 最近一次
;
-- 上次密码更新到用户表中
update sys_user a
inner join t_record_last_password b
on a.user_name = b.user_name 
    and a.account_status = 0 -- 状态(0正常 1离职 2废弃)
set a.password = b.last_password
    ,a.last_chgpwdtime = b.last_chgpwdtime
where a.last_chgpwdtime is null -- 已经记录过，不再更新(如果一直没有登录，则会更新)
;

/******************* segment4：考核权、考核方案、考核组 ***********************/
-- 情况1:正常新增人员_考核权信息
insert into t_appraisal_member
(dept_id,-- 部门id
dept_name,-- 部门名称
user_name,-- 姓名
user_code,-- 工号
user_id,-- 用户id
job,-- 职务
user_status,-- 记录状态: 0-正常 1-审批中 2-已注销
register_date,-- 生效年月
cancel_date,-- 注销年月
create_time,-- 创建时间
creator,-- 创建人
update_time,-- 修改时间
updater,-- 修改人
remark1,-- 备注字段1
remark2,-- 备注字段2
deleted -- 删除标识(0是正常，1是删除)
)
SELECT
a.dept_id dept_id,-- 部门id
dp.dept_name dept_name,-- 部门名称
a.nick_name user_name,-- 姓名
a.user_name user_code,-- 工号
a.user_id user_id,-- 用户id
null job,-- 职务
0 user_status,-- 记录状态: 0-正常 1-审批中 2-已注销
-- (case when begindate is null or begindate < '2023-01-01' then '2023-01-01' else concat(left(begindate,8),'01') end) register_date,-- 生效年月
(case when 
         a.begindate is null or a.begindate < '2023-01-01' then '2023-01-01' 
         else concat( left(if( day(a.begindate) > 15, date_add(a.begindate, interval 1 month), a.begindate ),8),'01')
         end) register_date,-- 生效年月
null cancel_date,-- 注销年月
sysdate() create_time,-- 创建时间
1 creator,-- 创建人
null update_time,-- 修改时间
null updater,-- 修改人
'考核权初始化新增' remark1,-- 备注字段1
null remark2,-- 备注字段2
0 deleted -- 删除标识(0是正常，1是删除)
from sys_user a
left join sys_dept dp   -- dept_id为主键
on a.dept_id = dp.dept_id
left join t_appraisal_member b -- 有重复，但是取新增部分，不会导致结果冗余
on a.user_id = b.user_id
left join t_appraisal_member c 
on a.user_name = c.user_code -- 工号
where b.user_id is null
and c.user_code is null -- 相同工号的不新增，前台会调出调入
and a.user_id!=1   -- 管理员不需要考核
and a.account_flag <> 2   -- 2为虚拟账户，用于单员工使用不同角色，建立的多用户；只需考核实体账户即可
and a.del_flag != 2  -- 离职人员不加入到考核权中
;

-- 情况2:离职再入职人员_考核权信息
insert into t_appraisal_member
(dept_id,-- 部门id
dept_name,-- 部门名称
user_name,-- 姓名
user_code,-- 工号
user_id,-- 用户id
job,-- 职务
user_status,-- 记录状态: 0-正常 1-审批中 2-已注销
register_date,-- 生效年月
cancel_date,-- 注销年月
create_time,-- 创建时间
creator,-- 创建人
update_time,-- 修改时间
updater,-- 修改人
remark1,-- 备注字段1
remark2,-- 备注字段2
deleted -- 删除标识(0是正常，1是删除)
)
SELECT
a.dept_id,-- 部门id
dp.dept_name dept_name,-- 部门名称
a.nick_name user_name,-- 姓名
a.user_name user_code,-- 工号
a.user_id user_id,-- 用户id
null job,-- 职务
0 user_status,-- 记录状态: 0-正常 1-审批中 2-已注销
(case when 
         a.begindate is null or a.begindate < '2023-01-01' then '2023-01-01' 
         else concat( left(if( day(a.begindate) > 15, date_add(a.begindate, interval 1 month), a.begindate ),8),'01')
         end) register_date,-- 生效年月
null cancel_date,-- 注销年月
now() create_time,-- 创建时间
1 creator,-- 创建人
now() update_time,-- 修改时间
1 updater,-- 修改人
'离职再入职人员考核权初始化' remark1,-- 备注字段1
null remark2,-- 备注字段2
0 deleted -- 删除标识(0是正常，1是删除)
from (
    select row_number() over(partition by u.user_name order by u.create_time desc,u.user_id desc) rn
    ,u.dept_id,u.nick_name,u.user_name,u.user_id,u.begindate,u.account_status
    from sys_user u
    where exists (select 1 from sys_user where u.user_name = user_name and del_flag = '0' and account_status = 0)
    and exists (select 1 from sys_user where u.user_name = user_name and del_flag = '0' and account_status = 1)
    and u.del_flag = 0
) a
left join (
    select row_number() over(partition by u.user_name order by u.create_time desc,u.user_id desc) rn
    ,u.dept_id,u.nick_name,u.user_name,u.user_id,u.begindate,u.account_status
    from sys_user u
    where exists (select 1 from sys_user where u.user_name = user_name and del_flag = '0' and account_status = 0)
    and exists (select 1 from sys_user where u.user_name = user_name and del_flag = '0' and account_status = 1)
    and u.del_flag = 0
) b
on a.user_name = b.user_name
left join sys_dept dp   -- dept_id为主键
on a.dept_id = dp.dept_id
where (
        (a.rn=1 and a.account_status = 0) -- 第一条记录为正常
        and (b.rn=2 and b.account_status = 1) -- 前一条为离职
      )
and not exists (select 1 from t_appraisal_member where a.user_id = user_id)
;
-- 情况3: 人员类别类型恢复，考核权新增
insert into t_appraisal_member
(dept_id,-- 部门id
dept_name,-- 部门名称
user_name,-- 姓名
user_code,-- 工号
user_id,-- 用户id
job,-- 职务
user_status,-- 记录状态: 0-正常 1-审批中 2-已注销
register_date,-- 生效年月
cancel_date,-- 注销年月
create_time,-- 创建时间
creator,-- 创建人
update_time,-- 修改时间
updater,-- 修改人
remark1,-- 备注字段1
remark2,-- 备注字段2
deleted -- 删除标识(0是正常，1是删除)
)
SELECT
distinct a.dept_id dept_id,-- 部门id
dp.dept_name dept_name,-- 部门名称
a.nick_name user_name,-- 姓名
a.user_name user_code,-- 工号
a.user_id user_id,-- 用户id
null job,-- 职务
4 user_status,-- 记录状态: 0-正常 1-审批中 2-已注销 4-临时状态
(case when a.begindate is null or a.begindate < '2023-01-01' then '2023-01-01' 
      else concat( left(if( day(a.begindate) > 15, date_add(a.begindate, interval 1 month), a.begindate ),8),'01')
      end) register_date,-- 生效年月
null cancel_date,-- 注销年月
sysdate() create_time,-- 创建时间
1 creator,-- 创建人
now() update_time,-- 修改时间
1 updater,-- 修改人
'人员类别恢复人员考核权初始化新增' remark1,-- 备注字段1
null remark2,-- 备注字段2
0 deleted -- 删除标识(0是正常，1是删除)
from sys_user a
left join sys_dept dp   -- dept_id为主键
on a.dept_id = dp.dept_id
left join t_appraisal_member b -- 可能有重复
    on a.user_id = b.user_id
    and b.user_status = 0
-- left join t_appraisal_member c -- 可能有重复
--     on a.user_id = c.user_id
--     and c.user_status = 2
where b.user_id is null
and a.user_id!=1   -- 管理员不需要考核
and a.account_flag <> 2   -- 2为虚拟账户，用于单员工使用不同角色，建立的多用户；只需考核实体账户即可
and a.del_flag != 2  -- 离职人员不加入到考核权中
and a.account_status = '4'
;
-- 更新生效日期，尽量让考核权时间不重叠
update t_appraisal_member a
inner join t_appraisal_member b 
on a.user_id = b.user_id
and a.user_status = 4
and b.user_status = 2
and b.cancel_date is not null
set a.register_date = b.cancel_date
    ,a.user_status = 0
;

-- 更新考核权员工工号和姓名
update t_appraisal_member a 
inner join sys_user u 
on a.user_id = u.user_id 
set a.user_code = u.user_name 
    ,a.user_name = u.nick_name
;

-- 用户表状态，恢复正常
update sys_user 
set account_status = '0'
where account_status = '4'
;

-- 如果是卫星营业部，考核部门和管理部门需要映射到中心营业部上面
-- 考核部门
insert into t_appraisal_dept
(dept_id,-- 部门id
appraisal_id,-- 考核人员表id
appraisal_right,-- 考核权重
note,-- 备注信息
create_time,-- 创建时间
creator,-- 创建人
update_time,-- 修改时间
updater,-- 修改人
remark1,-- 备注字段1
remark2,-- 备注字段2
deleted -- 删除标识(0是正常，1是删除)
)
select
    ifnull(d.parent_id,a.dept_id) dept_id,-- 部门id
    a.ID appraisal_id,-- 考核人员表id
    100 appraisal_right,-- 考核权重
    null note,-- 备注信息
    sysdate() create_time,-- 创建时间
    1 creator,-- 创建人
    null update_time,-- 修改时间
    null updater,-- 修改人
    '初始化考核权部门' remark1,-- 备注字段1
    null remark2,-- 备注字段2
    0 deleted -- 删除标识(0是正常，1是删除)
from t_appraisal_member a 
left join t_appraisal_dept b 
    on a.ID = b.appraisal_id -- 考核人员表id
left join sys_dept d 
    on a.dept_id = d.dept_id 
    and d.Fic_Org = '1'  -- 卫星营业部
left join sys_user c 
    on a.user_id = c.user_id
where b.appraisal_id is null
and a.creator = 1
and c.account_flag = 1 -- 实体账户
;
-- 考核管理部门
insert into t_appraisal_dept_magr(dept_id,appraisal_id,note,create_time,creator,update_time,updater,remark1,remark2,deleted)
select
    ifnull(d.parent_id,a.dept_id) dept_id,-- 管理部门id
    a.ID appraisal_id,-- 考核人员表id
    null note,-- 备注信息
    sysdate() create_time,-- 创建时间
    1 creator,-- 创建人
    null update_time,-- 修改时间
    null updater,-- 修改人
    '初始化考核权管理部门' remark1,-- 备注字段1
    null remark2,-- 备注字段2
    0 deleted -- 删除标识(0是正常，1是删除)
from t_appraisal_member a 
left join t_appraisal_dept_magr b 
    on a.ID = b.appraisal_id -- 考核人员表id
left join sys_dept d 
    on a.dept_id = d.dept_id 
    and d.Fic_Org = '1'  -- 卫星营业部
left join sys_user c 
    on a.user_id = c.user_id
where b.appraisal_id is null
and a.creator = 1
and c.account_flag = 1 -- 实体账户
;

-- 注销人员和离职人员的考核权 注销
update t_appraisal_member t 
inner join sys_user u 
    on t.user_id = u.user_id  
    and u.account_status in ('1', '2') -- -- 账户状态(0正常 1离职 2废弃)
set t.user_status = 2, -- 记录状态: 0-正常 1-审批中 2-已注销
    t.updater = 1, 
    t.update_time = sysdate(), 
    t.remark1 = case when u.account_status = '1' then concat('员工离职注销考核权信息，注销时间：',sysdate())
                     when u.account_status = '2' then concat('已注销员工 注销考核权信息，注销时间：',sysdate())
                     end,
    -- t.cancel_date = ifnull(u.enddate,last_day(date_sub(now(), interval 1 month)))  -- 取员工离职日期，如果没有则默认为上月最后一天
    -- t.cancel_date = ifnull(date_add(last_day(u.enddate),interval 1 day),date_format(CURRENT_DATE,'%Y-%m-01'))  -- 默认取员工离职日期下个月1号，如果没有则默认为本月1号
    t.cancel_date = date_add(last_day(ifnull(u.enddate,current_date)),interval 1 day)  -- 默认取员工离职日期下个月1号，如果没有则默认为次月1号
    -- t.cancel_date = (case 
    --                  when u.account_status = '2' and day(current_date) >  15 then date_add(last_day(current_date), interval 1 day) -- 注销用户，过半注销日期为下月初
    --                  when u.account_status = '2' and day(current_date) <= 15 then concat(left(current_date,8),'01') -- 注销用户，未过半注销日期为当月初
    --                  when u.account_status = '1' and (u.enddate is null or u.enddate < '2023-01-01') then '2023-01-01' 
    --                  else concat( left(if( day(u.enddate) > 15, date_add(u.enddate, interval 1 month), u.enddate ),8),'01') -- 过半取下月初，未过半取当月初
    --                  end)
where t.user_status = 0  -- 记录状态: 0-正常 1-审批中 2-已注销(已经更新的记录不再刷新)
;

-- 离职人员的考核方案 注销 
-- 只注销当年的考核方案
update t_assess_dtl t 
inner join sys_user u 
    on t.user_id = u.user_id  
    and u.account_status = '1' -- -- 账户状态(0正常 1离职 2废弃)
    and t.status = 4 -- 员工考核明细状态(1:未审批|2:审批中|3:审批失败|4:正常|5:注销)
set t.Logoff_time = case when t.par_year = cast(year(now()) as char) collate utf8mb4_general_ci then ifnull(last_day(u.enddate),last_day(date_sub(now(), interval 1 month))) -- 取员工离职日期当月最后一天，如果没有则默认为上月最后一天
                         else t.effect_time end, -- 以后年份的考核方案，取生效时间
    t.status = 5, -- 员工考核明细状态(1:未审批|2:审批中|3:审批失败|4:正常|5:注销)
    t.updater = 1,
    t.update_time = now(),
    t.remark = case when u.account_status = '1' then concat('员工离职注销考核方案信息，注销时间：',sysdate())
                     -- when u.account_status = '2' then concat('已注销员工 注销考核方案信息，注销时间：',sysdate())
                     end
where t.par_year >= cast(year(now()) as char) collate utf8mb4_general_ci
;

-- 离职人员考核组成员表状态改为无效
/* 
当月离退人员从次月起应从考核组中移出
如张三20230316离职，则4月1日凌晨跑批任务中，应将张三从考核组总移出:
*/
update t_appraisal_group_member a
inner join sys_user u 
    on a.member_id = u.user_id  
    and a.member_status = 1 -- 目前考核组中正常
    and u.account_status = '1' -- 账户状态(0正常 1离职 2废弃)
    and u.account_flag = 1 -- 只更新实体用户
    and a.deleted = 0
set a.member_status = 2 -- 状态改为无效
    ,a.logoff_time = now() -- 注销时间改为当前时间
    ,a.remark1 = concat('离职人员考核组成员表状态改为无效, 操作时间:',now())
where u.enddate <= last_day(date_sub(current_date, interval 1 month))
;

/*
目的：用户在考核部门的考核权失效后，会将该人员移出考核组
*/
drop TEMPORARY table if exists tmp_valid_appraisal_member;
CREATE TEMPORARY TABLE `tmp_valid_appraisal_member` (
  `dept_id` bigint DEFAULT NULL COMMENT '部门id',
  `member_id` bigint DEFAULT NULL COMMENT '用户id',
  KEY `index_2` (`dept_id`,`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '临时表-有效考核权人员'
;
insert into tmp_valid_appraisal_member(dept_id,member_id)
select distinct dep.dept_id,m.user_id member_id
from t_appraisal_member m
inner join t_appraisal_dept dep 
on m.id=dep.appraisal_id 
and dep.deleted=0
and m.deleted = 0
where (dep.appraisal_right is not null and dep.appraisal_right !=0) 
and ifnull(m.cancel_date,'2099-12-31') > CURRENT_DATE
and (m.cancel_date IS NULL OR m.cancel_date > concat(year(current_date),'-01-01') OR m.register_date >= concat(year(current_date),'-01-01'))
AND m.register_date < ifnull(m.cancel_date,'2099-12-31')
;

drop TEMPORARY table if exists tmp_invalid_appraisal_member;
CREATE TEMPORARY TABLE `tmp_invalid_appraisal_member` (
  `id` bigint NOT NULL DEFAULT '0' COMMENT 't_appraisal_group_member表的id',
  `dept_id` bigint DEFAULT NULL COMMENT '部门id',
  `member_id` bigint DEFAULT NULL COMMENT '考核组成员ID',
  `member_number` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '成员工号',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '临时表-无效考核权人员'
;
insert into tmp_invalid_appraisal_member(id,dept_id,member_id,member_number)
select a.id,a.dept_id,a.member_id,a.member_number 
from t_appraisal_group_member a
left join tmp_valid_appraisal_member b
on a.dept_id = b.dept_id 
and a.member_id = b.member_id 
where a.member_status = 1 
and a.deleted = 0
and b.dept_id is null
;

update t_appraisal_group_member a
inner join tmp_invalid_appraisal_member b
on a.dept_id = b.dept_id
and a.member_id = b.member_id
and a.id = b.id
set a.member_status = 2
    ,a.logoff_time = now() -- 注销时间改为当前时间
    ,a.remark1 = concat('该用户考核权无效，后台脚本将该用户考核组状态置为无效！操作时间：',now())
;


/*********************** segment5：kpm模块(通知人员处理问题) ******************************/
drop TEMPORARY table if exists tmp_dept_specialist;
CREATE TEMPORARY TABLE `tmp_dept_specialist` (
  `dept_id` bigint DEFAULT NULL COMMENT '部门ID',
  `specialist_user_id` bigint NOT NULL DEFAULT '0' COMMENT '用户ID',
  `specialist_user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
  `specialist_nick_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  key idx_dept_id(dept_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '公司绩效专员_临时表'
;
insert into tmp_dept_specialist(dept_id,specialist_user_id,specialist_user_name,specialist_nick_name,role_name)
select 
    a.dept_id   -- 部门
    ,a.user_id   specialist_user_id -- 绩效专员/管理员的用户id
    ,a.user_name specialist_user_name
    ,a.nick_name specialist_nick_name
    ,r.role_name
from sys_user a -- 考核部门下所有的人员
inner join sys_user_role ur
    on a.user_id = ur.user_id 
inner join sys_role r
    on ur.role_id = r.role_id
    and r.role_id in (2000,2001,2009,2010,2016
                     ,2013,2014,2015)  -- 绩效专员角色和管理员
where a.account_status = 0
;
/*********** 离退人员事项 ***********/
-- 用临时表报错离退人员提醒各项目
drop TEMPORARY table if exists tmp_appraisal_group_role;
CREATE TEMPORARY TABLE `tmp_appraisal_group_role` (
  `dept_id` bigint COMMENT '部门id',
  `user_id` bigint  COMMENT '用户ID',
  `user_name` varchar(30) COMMENT '用户账号',
  `nick_name` varchar(300) COMMENT '用户昵称',
  `group_role_name` varchar(20)  DEFAULT '考核组中角色名称',
  KEY `idx_dept_id` (`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '临时表-考核组中扮演角色表'
;
insert into tmp_appraisal_group_role(dept_id,user_id,user_name,nick_name,group_role_name)
select 
    distinct t1.dept_id
    ,a.user_id 
    ,a.user_name 
    ,a.nick_name
    ,'被考核人' group_role_name
from sys_user a 
inner join t_kpm_define b 
    on b.kpm_code = 'DepartingEmployeeTask'
inner join t_appraisal_group_member t1 -- 被考核人
    on a.user_id = t1.member_id 
    and t1.member_status = 1
    and t1.deleted = 0
where a.del_flag = '0'
    and a.account_status = '1'  -- 离退人员
    and not exists(select 1 from t_kpm_task k where a.user_id = k.relation_id1 and k.kpm_code = b.kpm_code)
;

insert into tmp_appraisal_group_role(dept_id,user_id,user_name,nick_name,group_role_name)
select 
    distinct t1.dept_id
    ,a.user_id 
    ,a.user_name 
    ,a.nick_name
    ,'直接汇报上级' group_role_name
from sys_user a 
inner join t_kpm_define b 
    on b.kpm_code = 'DepartingEmployeeTask'
inner join t_appraisal_group t1 -- 直接汇报上级
    on t1.deleted = 0
    and t1.leader_id = a.user_id
where a.del_flag = '0'
    and a.account_status = '1'  -- 离退人员
    and not exists(select 1 from t_kpm_task k where a.user_id = k.relation_id1 and k.kpm_code = b.kpm_code)
;

insert into tmp_appraisal_group_role(dept_id,user_id,user_name,nick_name,group_role_name)
select 
    distinct t1.dept_id
    ,a.user_id 
    ,a.user_name 
    ,a.nick_name
    ,'调整人' group_role_name
from sys_user a 
inner join t_kpm_define b 
    on b.kpm_code = 'DepartingEmployeeTask'
inner join t_appraisal_group t1 -- 调整项考核人
    on t1.deleted = 0
    and t1.adjust_id = a.user_id
where a.del_flag = '0'
    and a.account_status = '1'  -- 离退人员
    and not exists(select 1 from t_kpm_task k where a.user_id = k.relation_id1 and k.kpm_code = b.kpm_code)
;

insert into tmp_appraisal_group_role(dept_id,user_id,user_name,nick_name,group_role_name)
select 
    distinct t1.dept_id
    ,a.user_id 
    ,a.user_name 
    ,a.nick_name
    ,'考核人' group_role_name
from sys_user a 
inner join t_kpm_define b 
    on b.kpm_code = 'DepartingEmployeeTask'
inner join t_appraisal_group_reviewer t1 -- 考核人
    on t1.deleted = 0
    and t1.appraisal_id = a.user_id
where a.del_flag = '0'
    and a.account_status = '1'  -- 离退人员
    and not exists(select 1 from t_kpm_task k where a.user_id = k.relation_id1 and k.kpm_code = b.kpm_code)
;

-- 将数据聚合，不会重复提醒
drop TEMPORARY table if exists tmp_appraisal_group_role_sum;
create TEMPORARY table tmp_appraisal_group_role_sum
select dept_id,user_id,user_name,nick_name,group_concat(group_role_name) group_role_name
from tmp_appraisal_group_role
group by dept_id,user_id,user_name,nick_name
;

-- 离退人员-在考核组中
insert into t_kpm_task(kpm_code,content,url,dept_id,assignee_user_id,status,relation_id1,relation_id2,creator,create_time,updater,update_time,remark)
select 
     b.kpm_code                                                   as    kpm_code       -- kpm事件编码
    ,replace(replace(b.template_content,'${user_name}',a.user_name),'${nick_name}',a.nick_name)       as    content        -- kpm正文
    ,b.template_url                                               as    url            -- 跳转链接
    ,ds.dept_id                                                   as    dept_id        -- 部门id(所在hr部门)
    ,ds.specialist_user_id                                        as    assignee_user_id     -- 告警用户id(提醒人，一般是绩效专员)
    ,1                                                            as    status         -- 状态（1 待处理，2已处理，3 已忽略）
    ,a.user_id                                                    as    relation_id1   -- 关联id1
    ,null                                                         as    relation_id2   -- 关联id2
    ,1                                                            as    creator        -- 创建人
    ,now()                                                        as    create_time    -- 创建时间
    ,1                                                            as    updater        -- 修改人
    ,now()                                                        as    update_time    -- 修改时间
    ,concat('人员离退，该人员在考核关系中充当：【',a.group_role_name,'】，已提醒',ds.role_name,'!')        as    remark    -- 备注
from tmp_appraisal_group_role_sum a 
inner join t_kpm_define b 
    on b.kpm_code = 'DepartingEmployeeTask'
inner join tmp_dept_specialist ds
    on a.dept_id = ds.dept_id
;

-- 离退人员-不在考核组中
insert into t_kpm_task(kpm_code,content,url,dept_id,assignee_user_id,status,relation_id1,relation_id2,creator,create_time,updater,update_time,remark)
select 
     b.kpm_code                                                   as    kpm_code       -- kpm事件编码
    ,concat(a.nick_name,'(',a.user_name,')员工已离职!')       as    content        -- kpm正文
    ,b.template_url                                               as    url            -- 跳转链接
    ,ds.dept_id                                                   as    dept_id        -- 部门id(所在hr部门)
    ,ds.specialist_user_id                                        as    assignee_user_id     -- 告警用户id(提醒人，一般是绩效专员)
    ,1                                                            as    status         -- 状态（1 待处理，2已处理，3 已忽略）
    ,a.user_id                                                    as    relation_id1   -- 关联id1
    ,null                                                         as    relation_id2   -- 关联id2
    ,1                                                            as    creator        -- 创建人
    ,now()                                                        as    create_time    -- 创建时间
    ,1                                                            as    updater        -- 修改人
    ,now()                                                        as    update_time    -- 修改时间
    ,concat('人员离退，不在考核组中，已提醒',ds.role_name,'!')    as    remark         -- 备注
from sys_user a 
inner join t_kpm_define b 
    on b.kpm_code = 'DepartingEmployeeTask'
inner join tmp_dept_specialist ds
    on a.dept_id = ds.dept_id
where a.del_flag = '0'
    and a.account_status = '1'  -- 离退人员
    and a.enddate >= '2024-04-01'
    and not exists(select 1 from t_kpm_task k where a.user_id = k.relation_id1 and k.kpm_code = b.kpm_code)
;

/*********** 组织调整人员转出事项 ***********/
-- 组织调整，且考核权未转入新部门的用户
insert into t_kpm_task(kpm_code,content,url,dept_id,assignee_user_id,status,relation_id1,relation_id2,creator,create_time,updater,update_time,remark)
select
     y.kpm_code                                                   as    kpm_code       -- kpm事件编码
    ,replace(replace(y.template_content,'${user_name}',x.user_name),'${nick_name}',x.nick_name)       as    content        -- kpm正文
    ,y.template_url                                               as    url            -- 跳转链接
    ,x.old_dept_id                                                as    dept_id        -- 部门id(所在hr部门)
    ,ds.specialist_user_id                                        as    assignee_user_id     -- 告警用户id(提醒人，一般是绩效专员)
    ,1                                                            as    status         -- 状态（1 待处理，2已处理，3 已忽略）
    ,x.old_user_id                                                as    relation_id1   -- 关联id1
    ,null                                                         as    relation_id2   -- 关联id2
    ,1                                                            as    creator        -- 创建人
    ,now()                                                        as    create_time    -- 创建时间
    ,1                                                            as    updater        -- 修改人
    ,now()                                                        as    update_time    -- 修改时间
    ,concat('人员转部门【'
            ,x.old_dept_name
            ,'('
            ,x.old_dept_id
            ,')->'
            ,x.new_dept_name
            ,'(',x.new_dept_id,')】,考核权未转入当前部门；已提醒',ds.role_name,'!')    as    remark    -- 备注
FROM
    (
        SELECT
            t1.user_id new_user_id,
            t.user_id old_user_id,
            t1.user_name,
            t1.nick_name,
            t.dept_id old_dept_id,
            ( SELECT dept_name FROM sys_dept WHERE dept_id = t.dept_id ) old_dept_name,
            t1.dept_id new_dept_id,
            ( SELECT dept_name FROM sys_dept WHERE dept_id = t1.dept_id ) new_dept_name
        FROM
        (
            SELECT
                a.user_id,
                a.user_code,
                a.user_name,
                a.dept_id,
                a.register_date,
                a.id 
            FROM
                t_appraisal_member a 
            WHERE
                a.user_status = 0 
                and a.deleted = 0
                AND EXISTS ( SELECT 1 FROM sys_user b WHERE b.del_flag = '0' and a.user_code = b.user_name AND B.account_status = 3 ) -- 属于组织调整
                AND EXISTS ( SELECT 1 FROM sys_user c WHERE c.del_flag = '0' and a.user_code = c.user_name and a.user_id != c.user_id and c.account_status = 0 ) -- 当前用户考核权还没有转入
        ) t
        LEFT JOIN sys_user t1 
            ON t.user_code = t1.user_name 
            AND t1.account_status = '0'
            and t1.del_flag = '0'
    ) x
inner join t_kpm_define y 
    on y.kpm_code = 'TransferBranchTask'
inner join tmp_dept_specialist ds
    on x.old_dept_id = ds.dept_id
WHERE not exists(select 1 from t_kpm_task k where x.old_user_id = k.relation_id1 and k.kpm_code = y.kpm_code)
;
/*********** 新进/转入人员事件事项 ***********/
-- 考核权转入由系统生成：人员点击【考核关系管理】-【考核权查询/维护】-【调离注销】后生成kpm
insert into t_kpm_task(kpm_code,content,url,dept_id,assignee_user_id,status,relation_id1,relation_id2,creator,create_time,updater,update_time,remark)
select 
     b.kpm_code                                                   as    kpm_code       -- kpm事件编码
    ,replace(replace(replace(b.template_content,'${user_name}',a.user_name),'${nick_name}',a.nick_name),'${notify_item}','新入职')       as    content        -- kpm正文
    ,b.template_url                                               as    url            -- 跳转链接
    ,ds.dept_id                                                   as    dept_id        -- 部门id(所在hr部门)
    ,ds.specialist_user_id                                        as    assignee_user_id     -- 告警用户id(提醒人，一般是绩效专员)
    ,1                                                            as    status         -- 状态（1 待处理，2已处理，3 已忽略）
    ,a.user_id                                                    as    relation_id1   -- 关联id1
    ,null                                                         as    relation_id2   -- 关联id2
    ,1                                                            as    creator        -- 创建人
    ,now()                                                        as    create_time    -- 创建时间
    ,1                                                            as    updater        -- 修改人
    ,now()                                                        as    update_time    -- 修改时间
    ,concat('该人员于【',DATE_FORMAT(a.begindate,'%Y%m%d'),'】新入职，已提醒',ds.role_name,'!')    as    remark    -- 备注
from sys_user a 
inner join t_kpm_define b 
    on b.kpm_code = 'TransferAssessPowerTask'
left join sys_dept d 
    on a.dept_id = d.dept_id 
    and d.Fic_Org = '1'  -- 如果是卫星营业部，取中心营业部
inner join tmp_dept_specialist ds
    on ifnull(d.parent_id,a.dept_id) = ds.dept_id
where a.account_flag = '1'
    and a.account_status = '0'  -- 正常人员
    and a.create_time >= DATE_SUB(CURRENT_DATE,interval 2 week) -- 最近2周
    and not exists (select 1 from sys_user u where a.user_name = u.user_name and u.account_status = '3')  -- 没有发生组织调整
    and not exists(select 1 from t_kpm_task k where a.user_id = k.relation_id1 and k.kpm_code = b.kpm_code)
;

insert into t_kpm_task(kpm_code,content,url,dept_id,assignee_user_id,status,relation_id1,relation_id2,creator,create_time,updater,update_time,remark)
select 
     b.kpm_code                                                   as    kpm_code       -- kpm事件编码
    ,replace(replace(replace(b.template_content,'${user_name}',a.user_name),'${nick_name}',a.nick_name),'${notify_item}','新转入')       as    content        -- kpm正文
    ,b.template_url                                               as    url            -- 跳转链接
    ,ds.dept_id                                                   as    dept_id        -- 部门id(所在hr部门)
    ,ds.specialist_user_id                                        as    assignee_user_id     -- 告警用户id(提醒人，一般是绩效专员)
    ,1                                                            as    status         -- 状态（1 待处理，2已处理，3 已忽略）
    ,a.user_id                                                    as    relation_id1   -- 关联id1
    ,null                                                         as    relation_id2   -- 关联id2
    ,1                                                            as    creator        -- 创建人
    ,now()                                                        as    create_time    -- 创建时间
    ,1                                                            as    updater        -- 修改人
    ,now()                                                        as    update_time    -- 修改时间
    ,concat('该人员于【',DATE_FORMAT(a.begindate,'%Y%m%d'),'】新转入，已提醒',ds.role_name,'!')    as    remark    -- 备注
from sys_user a 
inner join t_kpm_define b 
    on b.kpm_code = 'TransferAssessPowerTask'
left join sys_dept d 
    on a.dept_id = d.dept_id 
    and d.Fic_Org = '1'  -- 如果是卫星营业部，取中心营业部
inner join tmp_dept_specialist ds
    on ifnull(d.parent_id,a.dept_id) = ds.dept_id
where a.account_flag = '1'
    and a.account_status = '0'  -- 正常人员
    and a.create_time >= DATE_SUB(CURRENT_DATE,interval 2 week) -- 最近2周
    and exists (select 1 from sys_user u where a.user_name = u.user_name and u.account_status = '3')  -- 发生组织调整
    and not exists(select 1 from t_kpm_task k where a.user_id = k.relation_id1 and k.kpm_code = b.kpm_code)
    and a.status = 0  -- 允许登录，也就是已转入人员
;


-- 获得记录数
select count(1) cnt into V_TABLE_CNT from sys_user;
-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,BIZ_DATE,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;
