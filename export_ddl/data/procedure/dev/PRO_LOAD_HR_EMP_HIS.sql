drop procedure if exists PRO_LOAD_HR_EMP_HIS;
CREATE PROCEDURE `PRO_LOAD_HR_EMP_HIS`(IN BIZ_DATE DATE)
label:BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_LOAD_HR_EMP_HIS
         功能简述：   人员历史信息表
         参数：       BIZ_DATE  上个工作日
         注意事项：
                 1. 源数据来自HR系统，不是绩效系统结存
                 2. 增量插入数据到目标表，不会主动删除目标表数据
         数据源：
                 1、 int_d_new_hrs_career_info  员工履历信息表
                 2、 int_d_new_hrs_edu_info     员工学历信息表
                 3、 int_d_new_hrs_job_info     员工任职信息表
                 4、 int_d_new_hrs_kpi_info     员工考核信息
         结果表：
                 t_employee_career_info
                 t_employee_edu_info
                 t_employee_job_info
                 t_employee_kpi_info
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2023/10/20                  创建
         MG         2023/10/25                  t_employee_job_info 表 job_dept改为dept_id
                                                4张表添加更新逻辑
         MG         2023/12/01                  员工考核信息新增原考核等级字段、调整考核等级字段
         MG         2023/12/25                  新增【任职部门编码】字段；新增步骤日志
         MG         2024/04/25                  员工任职信息表新增【部门id】字段
******************************************************************************************************************************************/

-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);
DECLARE V_BATCH_NO varchar(200);
DECLARE V_TOTAL_STEP_NUM int;

-- 设置变量
set V_EVENT_NAME = '人员历史信息表';
set V_TOTAL_STEP_NUM = 2;  -- 设置总步骤数
set @remark = ''; -- 步骤日志中的备注，可以每步都定义
select now() into V_START_TIME;


-- 解决字符集问题：Illegal mix of collations (utf8mb4_0900_ai_ci,IMPLICIT) and (utf8mb4_general_ci,IMPLICIT) for operation
set collation_connection = utf8mb4_general_ci;


set @data_date = date_format(BIZ_DATE,'%Y%m%d');

set V_RUN_COMMAND = concat('call PRO_LOAD_HR_EMP_HIS('
                            ,@data_date
                            ,')'
                         );

set V_BATCH_NO = concat(   UNIX_TIMESTAMP(),'-'
                        ,replace(V_RUN_COMMAND,'call ',''),'-'
                        ,V_TOTAL_STEP_NUM
                     );


-- 写步骤日志
set @step_info = '1.参数定义&基础检查:正常';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);





/*
-- 不能有重复记录
select psndoc_id,count(1) from t_employee
where current_flag = 1
group by psndoc_id 
having count(1) > 1
;

*/


-- 写步骤日志
set @step_info = '2.人员历史信息表:员工履历信息表';
set @remark = '员工履历信息表';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);
set @remark = '';


-- 员工履历信息表
insert into t_employee_career_info(Pk_Psndoc,user_name,ident_id,work_duty,work_dept,work_unit
    ,work_begin_dt,work_end_dt,status,remark)
select 
     a.Pk_Psndoc        Pk_Psndoc        -- 主键
    ,a.Emp_Id           user_name        -- 工号
    ,a.Cert_Id          ident_id         -- 身份证号
    ,a.Work_Duty        work_duty        -- 职务
    ,a.Work_Dept        work_dept        -- 部门
    ,a.Work_Unit        work_unit        -- 工作单位
    ,a.Work_Begin_Dt    work_begin_dt    -- 工作开始日期
    ,a.Work_End_Dt      work_end_dt      -- 工作结束日期
    ,0                  status           -- 状态(0:正常，1：无效)
    ,'数据初始化'       remark           -- 备注
from int_d_new_hrs_his_career_info a
left join t_employee_career_info b
    on a.Pk_Psndoc = b.Pk_Psndoc
    and a.Work_Begin_Dt = b.Work_Begin_Dt
where b.Pk_Psndoc is null
;




update t_employee_career_info a
inner join int_d_new_hrs_his_career_info b
    on a.Pk_Psndoc = b.Pk_Psndoc
    and a.Work_Begin_Dt = b.Work_Begin_Dt
set  a.user_name    = b.Emp_Id       
    ,a.ident_id     = b.Cert_Id      
    ,a.work_duty    = b.Work_Duty    
    ,a.work_dept    = b.Work_Dept    
    ,a.work_unit    = b.Work_Unit    
    ,a.work_end_dt  = b.Work_End_Dt  
;



-- 写步骤日志
set @step_info = '3.人员历史信息表:员工学历信息表';
set @remark = '员工学历信息表';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);
set @remark = '';


-- 员工学历信息表
insert into t_employee_edu_info(Pk_Psndoc,user_name,ident_id,school,major,study_type
    ,edu,entr_dt,grad_dt,status,remark)
select 
     a.Pk_Psndoc        Pk_Psndoc        -- 主键
    ,a.Emp_Id           user_name        -- 工号
    ,a.Cert_Id          ident_id         -- 身份证号
    ,a.School           school           -- 学校
    ,a.Major            major            -- 专业
    ,a.Study_Type       study_type       -- 学习方式
    ,a.Edu              edu              -- 学历
    ,a.Entr_Dt          entr_dt          -- 入学日期
    ,a.Grad_Dt          grad_dt          -- 毕业日期
    ,0                  status           -- 状态(0:正常，1：无效)
    ,'数据初始化'       remark           -- 备注
from int_d_new_hrs_his_edu_info a
left join t_employee_edu_info b 
    on a.Pk_Psndoc = b.Pk_Psndoc
    and a.entr_dt = b.entr_dt
where b.Pk_Psndoc is null
;





update t_employee_edu_info a
inner join int_d_new_hrs_his_edu_info b 
    on a.Pk_Psndoc = b.Pk_Psndoc
    and a.entr_dt = b.entr_dt
set  a.user_name   = b.Emp_Id    
    ,a.ident_id    = b.Cert_Id   
    ,a.school      = b.School    
    ,a.major       = b.Major     
    ,a.study_type  = b.Study_Type
    ,a.edu         = b.Edu       
    ,a.grad_dt     = b.Grad_Dt   
;



-- 写步骤日志
set @step_info = '4.人员历史信息表:员工任职信息表';
set @remark = '员工任职信息表';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);
set @remark = '';


-- 员工任职信息表
insert into t_employee_job_info(Pk_Psndoc,dept_id,user_name,ident_id,main_org_flag,job_begin_dt,job_end_dt
    ,job_dept_code,job_dept,job_org,status,remark)
select
     a.Pk_Psndoc        Pk_Psndoc        -- 主键
    ,replace(a.Job_Dept_Code,'ZQ','9')  dept_id -- 部门id
    ,a.Emp_Id           user_name        -- 工号
    ,a.Cert_Id          ident_id         -- 身份证号
    ,a.Main_Org_Flag    main_org_flag    -- 主部门标识
    ,a.Job_Begin_Dt     job_begin_dt     -- 任职开始日期
    ,a.Job_End_Dt       job_end_dt       -- 任职结束日期
    ,a.Job_Dept_Code    Job_Dept_Code    -- 任职部门编码
    ,a.Job_Dept         job_dept         -- 任职部门
    ,a.Job_Org          job_org          -- 任职组织
    ,0                  status           -- 状态(0:正常，1：无效)
    ,'数据初始化'       remark           -- 备注
from int_d_new_hrs_his_job_info a
left join t_employee_job_info b
    on a.Pk_Psndoc = b.Pk_Psndoc
    and a.job_begin_dt = b.job_begin_dt
where b.Pk_Psndoc is null
;

update t_employee_job_info a
inner join int_d_new_hrs_his_job_info b
    on a.Pk_Psndoc = b.Pk_Psndoc
    and a.job_begin_dt = b.job_begin_dt
set  a.dept_id        = replace(b.Job_Dept_Code,'ZQ','9')
    ,a.user_name      = b.Emp_Id
    ,a.ident_id       = b.Cert_Id
    ,a.main_org_flag  = b.Main_Org_Flag
    ,a.job_end_dt     = b.Job_End_Dt
    ,a.Job_Dept_Code  = b.Job_Dept_Code
    ,a.job_dept       = b.job_dept
    ,a.job_org        = b.Job_Org
;

drop temporary table if exists tmp_employee_job_info;
CREATE TEMPORARY TABLE `tmp_employee_job_info` (
  `dept_id` bigint DEFAULT NULL COMMENT '部门id',
  `user_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '人员工号',
  `join_group_date` date DEFAULT NULL COMMENT '任职开始日期',
  KEY `idx_2` (`dept_id`,`user_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '临时表-员工任职信息表'
;

insert into tmp_employee_job_info(dept_id,user_name,join_group_date)
select 
    dept_id
    ,user_name
    ,max(Job_Begin_Dt) join_group_date
from t_employee_job_info
group by dept_id,user_name 
;


-- 更新员工表
update t_employee a 
inner join tmp_employee_job_info b 
on a.dept_id = b.dept_id 
and a.code = b.user_name
set a.join_group_date = b.join_group_date
where a.status = '1'
;





-- select Job_Dept_Code,replace(Job_Dept_Code,'ZQ','9') from int_d_new_hrs_his_job_info;


-- 写步骤日志
set @step_info = '5.人员历史信息表:员工考核信息';
set @remark = '员工考核信息';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);
set @remark = '';


-- 员工考核信息
insert into t_employee_kpi_info(Pk_Psndoc,user_name,ident_id,kpi_year,kpi_level_code,source_kpi_level_code,kpi_level_name
    ,status,remark)
select
     a.Pk_Psndoc        Pk_Psndoc               -- 主键
    ,a.Emp_Id           user_name               -- 工号
    ,a.Cert_Id          ident_id                -- 身份证号
    ,a.Kpi_Year         kpi_year                -- 绩效年度
    ,d.assess_level     kpi_level_code          -- 绩效等级编码
    ,a.kpi_level_code   source_kpi_level_code   -- 原绩效等级编码
    ,a.Kpi_Level_Name   kpi_level_name          -- 绩效等级名称
    ,0                  status                  -- 状态(0:正常，1：无效)
    ,'数据初始化'       remark                  -- 备注
from int_d_new_hrs_his_kpi_info a
left join t_employee_kpi_info b 
    on a.Pk_Psndoc = b.Pk_Psndoc
    and a.kpi_year = b.kpi_year
left join t_annual_assess_dict_assess_level d 
    on d.assess_flag = 2
    and a.kpi_level_code = d.kpi_level_code
where b.Pk_Psndoc is null
and a.kpi_level_code != 'kpdj-100'
;


update t_employee_kpi_info a
inner join int_d_new_hrs_his_kpi_info b 
    on a.Pk_Psndoc = b.Pk_Psndoc
    and a.kpi_year = b.kpi_year
left join t_annual_assess_dict_assess_level d 
    on d.assess_flag = 2
    and a.kpi_level_code     = d.kpi_level_code
set  a.user_name             = b.Emp_Id
    ,a.ident_id              = b.Cert_Id
    ,a.kpi_level_code        = d.assess_level
    ,a.source_kpi_level_code = b.kpi_level_code
    ,a.kpi_level_name        = b.Kpi_Level_Name
;





-- 不统计记录数
select 0 into V_TABLE_CNT;


-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;
