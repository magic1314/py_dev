DROP PROCEDURE IF EXISTS PRO_YJGL_DAILY_LOOP;
CREATE PROCEDURE `PRO_YJGL_DAILY_LOOP`(IN IN_MONTHS INT,IN IN_DEPT_ID INT)
label:BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_YJGL_DAILY_LOOP
         功能简述：   业绩管理-完成值计算轮询&临时任务
         参数：       IN_MONTHS       轮询的月数
                      IN_DEPT_ID      部门编码(指定部门，0：表示全部部门)
         注意事项：
                 1. 第一个月为上月，不会轮询当月
                 2. 月数范围：整数[1,6]
                 3. 不计算202305及之前周期的数据
         数据源：
                 1、 PRO_YJGL_ASSESS_INFO_HIS             存储过程-考核方案列定
                 2、 PRO_YJGL_COMPLETED_VALUE             存储过程-计算指标完成值
         结果表：
                 t_assess_info_his                        考核方案列定
                 t_emp_perf_cmplt_val                     指标完成值表(完成值导入 不主动跑)
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2023/09/12                  创建
         MG         2023/09/25                  添加传入参数：部门编码
         MG         2023/10/13                  日志中加入部门编码，删除【新进人员考核方案修改】临时任务
         MG         2023/11/06                  修改日志表中存储过程中文名
                                                新增【有考核方案未考核的虚拟账户】临时任务-暂时不运行
******************************************************************************************************************************************/

-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);
DECLARE V_BATCH_NO varchar(200);
DECLARE V_TOTAL_STEP_NUM int;
DECLARE loop_i INT DEFAULT 1;


-- 设置变量
set V_EVENT_NAME = '每日轮询和临时任务';
set V_TOTAL_STEP_NUM = 5;  -- 设置总步骤数
set @remark = ''; -- 步骤日志中的备注，可以每步都定义
select now() into V_START_TIME;


-- 解决字符集问题：Illegal mix of collations (utf8mb4_0900_ai_ci,IMPLICIT) and (utf8mb4_general_ci,IMPLICIT) for operation
-- set collation_connection = utf8mb4_general_ci;

set @months = IN_MONTHS; -- 格式: 6 (默认为6，最大为6)
set @dept_id = IN_DEPT_ID; -- 格式：25612


set V_RUN_COMMAND = concat('call PRO_YJGL_DAILY_LOOP('
                            ,IN_MONTHS,','
                            ,IN_DEPT_ID
                            ,')'
                         );

set V_BATCH_NO = concat(   UNIX_TIMESTAMP(),'-'
                        ,replace(V_RUN_COMMAND,'call ',''),'-'
                        ,V_TOTAL_STEP_NUM
                     );

/************ 业绩管理-完成值计算轮询 ************/
drop TEMPORARY table if exists tmp_pro_loop_months_1;
CREATE TEMPORARY TABLE `tmp_pro_loop_months_1` (
  `seq` bigint NOT NULL DEFAULT '0',
  `months` varchar(6) COLLATE utf8mb4_general_ci DEFAULT NULL,
  status tinyint default '0' comment '状态(0:正常，1：无效)',
  `cmd_1` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL comment '命令1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '临时表-用于轮询月份'
;


drop TEMPORARY table if exists tmp_pro_loop_months_2;
CREATE TEMPORARY TABLE `tmp_pro_loop_months_2` (
  `seq` bigint NOT NULL DEFAULT '0',
  `months` varchar(6) COLLATE utf8mb4_general_ci DEFAULT NULL,
  status tinyint default '0' comment '状态(0:正常，1：无效)',
  `cmd_2` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL comment '命令2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '临时表-用于轮询月份'
;


insert into tmp_pro_loop_months_1(seq,months)
select seq,months
from (
    SELECT 1 seq,DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 1 MONTH), '%Y%m') months
    UNION ALL
    SELECT 2 seq,DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH), '%Y%m') months
    UNION ALL
    SELECT 3 seq,DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 3 MONTH), '%Y%m') months
    UNION ALL
    SELECT 4 seq,DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 4 MONTH), '%Y%m') months
    UNION ALL
    SELECT 5 seq,DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 5 MONTH), '%Y%m') months
    UNION ALL
    SELECT 6 seq,DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 6 MONTH), '%Y%m') months
) x
where seq <= @months
;


delete from tmp_pro_loop_months_1 where months <= '202305';


insert into tmp_pro_loop_months_2(seq,months)
select seq,months
from (
    SELECT 1 seq,DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 1 MONTH), '%Y%m') months
    UNION ALL
    SELECT 2 seq,DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 2 MONTH), '%Y%m') months
    UNION ALL
    SELECT 3 seq,DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 3 MONTH), '%Y%m') months
    UNION ALL
    SELECT 4 seq,DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 4 MONTH), '%Y%m') months
    UNION ALL
    SELECT 5 seq,DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 5 MONTH), '%Y%m') months
    UNION ALL
    SELECT 6 seq,DATE_FORMAT(DATE_SUB(CURRENT_DATE, INTERVAL 6 MONTH), '%Y%m') months
) x
where seq <= @months
;


delete from tmp_pro_loop_months_2 where months <= '202305';

-- 循环遍历固定次数
SET loop_i = 1;
WHILE loop_i <= @months DO
    select count(1) into @cnt from tmp_pro_loop_months_1 where seq = loop_i and status = 0;
    IF @cnt = 0 THEN 
        SELECT '@year_months is NULL' INTO @info;
    ELSE
        -- 获取字段值
        select months into @year_months from tmp_pro_loop_months_1 where seq = loop_i and status = 0;
        call PRO_YJGL_ASSESS_INFO_HIS(@year_months,@dept_id,0);
    END IF;

    SET loop_i = loop_i + 1;
END WHILE
;

-- 循环遍历固定次数
SET loop_i = 1;
WHILE loop_i <= @months DO
    select count(1) into @cnt from tmp_pro_loop_months_2 where seq = loop_i and status = 0;
    IF @cnt = 0 THEN 
        SELECT '@year_months is NULL' INTO @info;
    ELSE
        -- 获取字段值
        select months into @year_months from tmp_pro_loop_months_2 where seq = loop_i and status = 0;
        call PRO_YJGL_COMPLETED_VALUE(@year_months,@dept_id,0);
    END IF;
    SET loop_i = loop_i + 1;
END WHILE
;


/************ 临时任务区域 ************/




set @run_flag = 1;  -- 0运行，1不运行

if @run_flag = 0 then 
    -- 有考核方案未考核的虚拟账户
    drop TEMPORARY table if exists tmp_xn_user;
    CREATE TEMPORARY TABLE `tmp_xn_user` (
      `user_id` bigint NOT NULL DEFAULT '0' COMMENT '用户ID',
      `dept_id` bigint DEFAULT NULL COMMENT '部门ID',
      `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
      `nick_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
      `account_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '状态(0正常 1离退 2废弃 3组织调整)',
      `belonging_user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '所属用户账号'
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '有效的虚拟账户'
    ;


    drop TEMPORARY table if exists tmp_xn_user_assess;
    CREATE TEMPORARY TABLE `tmp_xn_user_assess` (
      `par_year` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '考核年份',
      `year_months` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '年月',
      `assess_pd_value` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '考核周期值(M1-M12,Q1..)',
      `dept_id` bigint DEFAULT NULL COMMENT '部门编码',
      `user_id` bigint DEFAULT NULL COMMENT '人员ID'
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '虚拟用户的考核方案'
    ;



    -- 虚拟用户的考核打分情况
    drop TEMPORARY table if exists tmp_xn_user_score;
    CREATE TEMPORARY TABLE `tmp_xn_user_score` (
      `par_year` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '考核年份',
      `year_months` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '年月',
      `assess_pd_value` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '考核周期值',
      `dept_id` bigint DEFAULT NULL COMMENT '部门编码',
      `user_id` bigint DEFAULT NULL COMMENT '被考核人id'
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '虚拟用户的考核打分情况'
    ;



    drop TEMPORARY table if exists tmp_xn_user_list;
    CREATE TEMPORARY TABLE `tmp_xn_user_list` (
      `user_id` bigint DEFAULT NULL COMMENT '人员ID'
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '存在有考核方案未考核的虚拟账户名单'
    ;


    -- 有效的虚拟账户
    insert into tmp_xn_user(user_id,dept_id,user_name,nick_name,account_status,belonging_user_name)
    select user_id,dept_id,user_name,nick_name,account_status,belonging_user_name 
    from sys_user 
    where account_flag = 2 
    and account_status = 0
    ;





    -- 虚拟用户的考核方案
    insert into tmp_xn_user_assess(par_year,year_months,assess_pd_value,dept_id,user_id)
    select distinct a.par_year,a.year_months,a.assess_pd_value,a.dept_id,a.user_id 
    from t_assess_info_his a
    where user_id in (select user_id from tmp_xn_user)
    ;




    -- 虚拟用户的考核打分情况
    insert into tmp_xn_user_score(par_year,year_months,assess_pd_value,dept_id,user_id)
    select distinct par_year,year_months,assess_pd_value,dept_id,member_id user_id 
    from t_assess_score_dtl
    where member_id in (select user_id from tmp_xn_user)
    ;


    -- 存在有考核方案未考核的虚拟账户明细
    truncate table t_xn_user_assess_dtl;
    insert into t_xn_user_assess_dtl(par_year,year_months,assess_pd_value,dept_id,user_id)
    select 
        a.par_year,a.year_months,a.assess_pd_value,a.dept_id,a.user_id 
        -- distinct a.user_id
    from tmp_xn_user_assess a 
    left join tmp_xn_user_score b 
    on a.par_year = b.par_year 
    and a.assess_pd_value = b.assess_pd_value
    and a.dept_id = b.dept_id
    and a.user_id = b.user_id
    where b.par_year is null
    ;

    -- 存在有考核方案未考核的虚拟账户名单
    insert into tmp_xn_user_list(user_id)
    select distinct user_id 
    from t_xn_user_assess_dtl
    ;



    truncate table t_xn_user;
    insert into t_xn_user(
    user_id
    ,dept_id
    ,user_name
    ,nick_name
    ,account_status
    ,belonging_user_name
    ,assess_status
    ,remark
    )
    select 
         a.user_id
        ,a.dept_id
        ,a.user_name
        ,a.nick_name
        ,a.account_status
        ,a.belonging_user_name
        ,(case when b.user_id is not null then 0 else 1 end ) assess_status
        ,'数据每日更新' remark
    from tmp_xn_user a
    left join tmp_xn_user_dtl b 
    on a.user_id = b.user_id
    ;
end if
;

-- 获得记录数
select 0 cnt into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;