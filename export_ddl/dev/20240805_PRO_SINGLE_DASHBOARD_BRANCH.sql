DROP PROCEDURE IF EXISTS PRO_SINGLE_DASHBOARD_BRANCH;
CREATE PROCEDURE `PRO_SINGLE_DASHBOARD_BRANCH`(IN IN_CHART_TYPE INT)
label:BEGIN
/************************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_SINGLE_DASHBOARD_BRANCH
         功能简述：   看板-分公司
         参数：       IN_CHART_TYPE  图表类型(0: 全部，1:【看板1-3】，2:【看板1-4】)
         注意事项：
                      生效时间：数据是T+1生效且任务只在每日调度中执行
                      依赖于过程过程：【PRO_SINGLE_DASHBOARD_BASE】
         数据源：
                    1.  sys_dept
                    2.  t_assess_dtl
                    3.  t_assess_dtl_idx
                    4.  t_employee
                    5.  t_index_pool
                    6.  t_assess_info_his                    考核方案列定
                    7.  t_dept_annual_assess                 部门考核方案总览
                    8.  t_dict_assess_pd_split_month         考核周期拆分成月份
                    9.  t_dict_job_rank_mapping              职级映射表
                    10. t_emp_perf_cmplt_val                 指标完成值表
                    11. t_dashboard_dept_level               部门层级表
         目标表：
                    1.  t_dashboard_branch_assess_last_year  数据生成逻辑放在BASE存储过程中
                    2.  t_dashboard_branch_cmplt_all_dept
                    3.  t_dashboard_branch_cmplt             看板1-3目标表
                    4.  t_dashboard_branch_cmplt_idx         看板1-3目标明细
                    5.  t_dashboard_branch_cmplt_idx_split
                    6.  t_dashboard_branch_cmplt_imp         看板1-3导入表
                    7.  t_dashboard_branch_dept_user_cnt
                    8.  t_dashboard_branch_income_cmplt      看板1-4目标表
                    9.  t_dashboard_branch_income_cmplt_idx  看板1-4目标表明细
                    10. t_dashboard_branch_income_cmplt_dtl  看板1-4人数明细表
                    11. t_dashboard_branch_last_annual_tgt
                    12. t_dashboard_branch_last_complete_info
         修改记录:
         --------------------------------------------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2024/05/22                  创建
         MG         2024/05/23                  新增指标名称
         MG         2024/05/29                  修改日志内容，单位改为(万元)
         MG         2024/05/30                  调整人数相关字段
         MG         2024/06/04                  看板1-3：人数默认为null
         MG         2024/06/06                  看板1-3：导入表新增 展示标识字段
         MG         2024/06/07                  达标人数占比为百分比
         MG         2024/06/18                  看板1-3:指标名称带单位
         MG         2024/06/18                  看板1-4:年龄段/职等全部需要展示，没有人展示0
         MG         2024/07/09                  看板1-4新增人数明细表
         MG         2024/07/10                  部门最新年份表的数据生成逻辑放在BASE存储过程中
************************************************************************************************************************************************/
-- 变量定义
DECLARE V_TABLE_CNT int;DECLARE V_START_TIME datetime;DECLARE V_EVENT_NAME varchar(200);DECLARE V_RUN_COMMAND varchar(1000);DECLARE V_BATCH_NO varchar(200);DECLARE V_TOTAL_STEP_NUM int;-- 设置变量
set V_EVENT_NAME = '分公司看板';select now() into V_START_TIME;if @chart_type = 0 then 
    set V_TOTAL_STEP_NUM = 5;-- 设置总步骤数
else 
    set V_TOTAL_STEP_NUM = 4;-- 设置总步骤数
end if
;set @remark = '';-- 步骤日志中的备注，可以每步都定义
set V_RUN_COMMAND = concat('call PRO_SINGLE_DASHBOARD_BRANCH('
                            ,IN_CHART_TYPE
                            ,')'
                         );set V_BATCH_NO = concat(   UNIX_TIMESTAMP(),'-'
                        ,replace(V_RUN_COMMAND,'call ',''),'-'
                        ,V_TOTAL_STEP_NUM
                     );-- 写步骤日志
set @step_info = concat('1.参数定义&基础检查:正常');call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);set @chart_type = IN_CHART_TYPE;if @chart_type > 10 then
    select concat('不支持图表类型：',@chart_type);LEAVE label;-- 退出存储过程 
end if;-- 写步骤日志
set @step_info = concat('2.基础公共数据生成,chart_type=',@chart_type);set @remark = '';call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);set @step_id = 3;/************************************************************************
    “前台员工业绩收入”和“前台员工净创收” 指标的基础数据
    数据来源于【考核方案列定表】和【指标完成值表】
************************************************************************/
truncate table t_dashboard_branch_cmplt_idx;insert into t_dashboard_branch_cmplt_idx(par_year,assess_pd,assess_pd_value,assess_month,dept_id,user_id
    ,index_code,assess_model,current_tgt,complete_info)
select 
     a.par_year                                             -- 年份
    ,a.assess_pd                                            -- 考核周期
    ,a.assess_pd_value                                      -- 考核周期值
    ,cast(right(a.year_months,2) as decimal)  assess_month  -- 月份_数值
    ,a.dept_id                                              -- 部门编码
    ,a.user_id                                              -- 被考核人id
    ,a.index_code                                           -- 指标代码
    ,a.assess_model                                         -- 考核模式
    ,round(a.current_tgt/10000,2) current_tgt               -- 考核目标
    ,round(b.index_value/10000,2) complete_info             -- 完成情况
from t_assess_info_his a 
left join t_emp_perf_cmplt_val b 
on a.year_months = b.year_months
    and a.dept_id = b.dept_id
    and a.user_id = b.user_id
    and a.index_code = b.index_code
    and a.assess_pd = b.assess_pd
where a.business_type != 6 
and a.index_code in ('GJJX_YJSR_11000_001_01_000','GJJX_JCS_11000_001_01_000')
;if (@chart_type = 0 or @chart_type = 1) then
    -- 写步骤日志
    set @step_info = concat(@step_id,'.【看板1-3】数据生成');set @step_id = @step_id + 1;set @remark = '';call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);/************************************************************************
        “前台员工业绩收入”和“前台员工净创收” 指标的基础数据拆分
    ************************************************************************/
    truncate table t_dashboard_branch_cmplt_idx_split;insert into t_dashboard_branch_cmplt_idx_split(par_year,year_months,assess_pd,assess_pd_value,dept_id,user_id
        ,index_code,assess_model,split_month,current_tgt,complete_info)
    select 
         a.par_year
        ,concat(a.par_year,td.split_month_2str) year_months
        ,a.assess_pd
        ,a.assess_pd_value
        ,a.dept_id
        ,a.user_id
        ,a.index_code
        ,a.assess_model
        ,td.split_month
        ,case when a.assess_pd = 1 then a.current_tgt   else (a.current_tgt   * td.split_month / td.current_month ) end current_tgt
        ,case when a.assess_pd = 1 then a.complete_info else (a.complete_info * td.split_month / td.current_month ) end complete_info
    from t_dashboard_branch_cmplt_idx a
    left join t_dict_assess_pd_split_month td
    on a.assess_pd_value = td.assess_pd_value
    ;/************************************************************************
        “前台员工业绩收入”和“前台员工净创收” 指标的数据聚合
    ************************************************************************/
    truncate table t_dashboard_branch_cmplt_all_dept;insert into t_dashboard_branch_cmplt_all_dept(par_year,year_months,dept_id,index_code
        ,assess_model,current_tgt,complete_info)
    select 
         a.par_year                                      par_year           -- 年份
        ,a.year_months                                   year_months        -- 年月
        ,a.dept_id                                       dept_id            -- 部门编码
        ,a.index_code                                    index_code         -- 指标代码
        ,a.assess_model                                  assess_model       -- 考核模式
        ,sum(a.current_tgt  )                            current_tgt        -- 考核总目标
        ,sum(a.complete_info)                            complete_info      -- 完成情况
    from t_dashboard_branch_cmplt_idx_split a
    group by a.par_year,a.year_months,a.dept_id,a.index_code,a.assess_model
    ;/************************************************************************
        看板1-3辖内绩效完成情况总览表
        11:左图指标-全辖 12:左图-仅本部 21:右图-全辖 22:右图-仅本部
    ************************************************************************/
    truncate table t_dashboard_branch_cmplt;-- 21:右图-全辖,聚合数据
    insert into t_dashboard_branch_cmplt(par_year,year_months,dept_id,chart_xy,assess_month,last_assess_month,index_code,assess_model
        ,current_tgt,complete_info)
    select 
         a.par_year
        ,a.year_months
        ,b.first_parent_id dept_id   -- 合并到分公司上
        ,21 chart_xy
        ,cast(right(a.year_months,2) as decimal)     assess_month       -- 月份_数值
        ,cast(right(a.year_months,2) as decimal)-1   last_assess_month  -- 上月份_数值
        ,a.index_code
        ,a.assess_model
        ,sum(a.current_tgt  ) current_tgt
        ,sum(a.complete_info) complete_info
    from t_dashboard_branch_cmplt_all_dept a
    inner join t_dashboard_dept_level b
    on a.dept_id = b.dept_id
    where b.first_org_type = 6
    group by a.par_year,a.year_months,b.first_parent_id,a.index_code,a.assess_model
    ;-- 22:右图-仅本部
    insert into t_dashboard_branch_cmplt(par_year,year_months,dept_id,chart_xy,assess_month,last_assess_month,index_code,assess_model
        ,current_tgt,complete_info)
    select 
         a.par_year
        ,a.year_months
        ,a.dept_id
        ,22 chart_xy
        ,cast(right(a.year_months,2) as decimal)     assess_month       -- 月份_数值
        ,cast(right(a.year_months,2) as decimal)-1   last_assess_month  -- 上月份_数值
        ,a.index_code
        ,a.assess_model
        ,a.current_tgt
        ,a.complete_info
    from t_dashboard_branch_cmplt_all_dept a
    inner join sys_dept d
    on a.dept_id = d.dept_id
    where d.org_type = 6
    ;-- 部门人数
    drop TEMPORARY table if exists tmp_dashboard_branch_user_cnt;CREATE TEMPORARY TABLE `tmp_dashboard_branch_user_cnt` (
      `year_months` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '年月',
      `dept_id` bigint DEFAULT NULL COMMENT '部门编码',
      data_type int comment '数据类型(1:全辖，2:仅本部)',
      `user_cnt` bigint comment '人数',
      KEY `idx_2` (`year_months`,`dept_id`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '临时表-看板人数表(全辖/仅本部)'
    ;insert into tmp_dashboard_branch_user_cnt(year_months,dept_id,data_type,user_cnt)
    select 
         a.year_months
        ,b.first_parent_id dept_id
        ,1 data_type
        ,count(distinct a.user_id) user_cnt
    from t_dashboard_branch_cmplt_idx_split a
    inner join t_dashboard_dept_level b 
    on a.dept_id = b.dept_id
    and b.first_org_type = 6
    group by a.year_months,b.first_parent_id
    ;insert into tmp_dashboard_branch_user_cnt(year_months,dept_id,data_type,user_cnt)
    select 
         a.year_months
        ,a.dept_id
        ,2 data_type
        ,count(distinct a.user_id) user_cnt
    from t_dashboard_branch_cmplt_idx_split a
    inner join sys_dept d 
    on a.dept_id = d.dept_id 
    and d.org_type = 6
    group by a.year_months,a.dept_id
    ;-- 业务净收入（分公司经营管理考核）
    /*
    1、业务净收入（分公司经营管理考核）
    总体目标值-后台录入
    总体完成值-后台录入
    人均=总体/ 
    人数=承担了以下两个指标其中任一的全部人员
    */

    -- 11:左图-全辖
    insert into t_dashboard_branch_cmplt(par_year,year_months,dept_id,chart_xy,assess_month,last_assess_month,index_code,assess_model
        ,current_tgt,complete_info)
    select 
         a.par_year
        ,concat(a.par_year,if(a.assess_month<=9,'0',''),a.assess_month) year_months
        ,a.dept_id
        ,11 chart_xy
        ,a.assess_month      assess_month       -- 月份_数值
        ,a.assess_month-1    last_assess_month  -- 上月份_数值
        ,a.index_code         -- 这里指标代码用指标名称
        ,2 assess_model
        ,a.current_tgt
        ,a.complete_info
    from t_dashboard_branch_cmplt_imp a
    where a.dept_class = 0 -- 0:分公司全辖
    and a.show_flag = 0
    ;-- 12:左图-仅本部
    insert into t_dashboard_branch_cmplt(par_year,year_months,dept_id,chart_xy,assess_month,last_assess_month,index_code,assess_model
        ,current_tgt,complete_info)
    select 
         a.par_year
        ,concat(a.par_year,if(a.assess_month<=9,'0',''),a.assess_month) year_months
        ,a.dept_id
        ,12 chart_xy
        ,a.assess_month      assess_month       -- 月份_数值
        ,a.assess_month-1    last_assess_month  -- 上月份_数值
        ,a.index_code
        ,2 assess_model
        ,a.current_tgt
        ,a.complete_info
    from t_dashboard_branch_cmplt_imp a
    where a.dept_class = 1 -- 1:分公司本部
    and a.show_flag = 0
    ;-- 全辖
    update t_dashboard_branch_cmplt a
    inner join tmp_dashboard_branch_user_cnt b 
        on a.year_months = b.year_months
        and a.dept_id = b.dept_id
        and b.data_type = 1
    set a.user_cnt = b.user_cnt
    where a.chart_xy in (11,21)
    ;-- 仅本部
    update t_dashboard_branch_cmplt a
    inner join tmp_dashboard_branch_user_cnt b 
        on a.year_months = b.year_months
        and a.dept_id = b.dept_id
        and b.data_type = 2
    set a.user_cnt = b.user_cnt
    where a.chart_xy in (12,22)
    ;/************************* 更新增长率 ******************************/
    -- 更新指标名称
    update t_dashboard_branch_cmplt a 
    left join t_index_pool ip 
        on a.index_code = ip.index_code 
        and ip.deleted = 0
    left join t_dashboard_manual_add_index b 
        on a.index_code = b.index_code
        and b.deleted = 0
    left join sys_dict_data dd 
        on dd.dict_type = 'index_money_unit'
        and ifnull(ip.index_unit,b.index_unit) = dd.dict_value
    set a.index_name = concat(
                                ifnull(ip.index_name, b.index_name)
                                ,'('
                                ,dd.dict_label
                                ,')'
                             )
    ;-- 差值计算
    update t_dashboard_branch_cmplt a
    left join t_dashboard_branch_cmplt b 
        on a.par_year = b.par_year
        and a.last_assess_month = b.assess_month
        and a.dept_id = b.dept_id
        and a.index_code = b.index_code
        and a.chart_xy = b.chart_xy
    left join t_dashboard_branch_cmplt c
        on b.par_year = c.par_year
        and b.last_assess_month = c.assess_month
        and b.dept_id = c.dept_id
        and b.index_code = c.index_code
        and b.chart_xy = c.chart_xy
    set  a.current_tgt_avg   = a.current_tgt / a.user_cnt
        ,a.complete_info_avg = a.complete_info / a.user_cnt
        ,a.complete_info_t0  = a.complete_info
        ,a.complete_info_t1  = if(a.assess_month <= 1,0, b.complete_info)
        ,a.complete_info_t2  = if(a.assess_month <= 2,0, c.complete_info)
    ;-- delta增量
    update t_dashboard_branch_cmplt a 
    set a.complete_info_t0_delta = (a.complete_info_t0 - a.complete_info_t1)
        ,a.complete_info_t1_delta = (a.complete_info_t1 - a.complete_info_t2)
    ;/*
    假设当前月为T月，供数周期为月度（其他供数周期同理）。供数模式为：
    1、当期，增长率=（（T月人均完成业绩值-T-1月人均完成业绩值）/T-1月人均完成业绩值）*100%；
    2、累计，增长率=（（（T月人均完成业绩值-T-1月周期人均完成业绩值）-（T-1月人均完成业绩值-T-2月人均完成业绩值））/（T-1月人均完成业绩值-T-2月人均完成业绩值））*100%
    3、时点，增长率= 同当期
    */
    update t_dashboard_branch_cmplt a
    set a.complete_info_increase_rate = case 
            when a.assess_month = 1 then 0
            when a.assess_model = 1 then (a.complete_info_t0_delta / a.complete_info_t1) * 100
            when a.assess_model = 2 then (a.complete_info_t0_delta - a.complete_info_t1_delta) / a.complete_info_t1_delta * 100
            when a.assess_model = 3 then (a.complete_info_t0_delta / a.complete_info_t1) * 100
            end
    ;end if
;if (@chart_type = 0 or @chart_type = 2) then 
    -- 写步骤日志
    set @step_info = concat(@step_id,'.【看板1-4】数据生成');set @step_id = @step_id + 1;set @remark = '';call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);-- 每个员工最新的考核方案-年度目标值
    truncate table t_dashboard_branch_last_annual_tgt;insert into t_dashboard_branch_last_annual_tgt(par_year,dept_id,user_id,annual_tgt)
    select x.par_year,x.dept_id,x.user_id,x.annual_tgt
    from (
        select 
             row_number() over(partition by d.par_year,d.dept_id,d.user_id order by d.Logoff_time desc) rn
            ,d.par_year
            ,d.dept_id
            ,d.user_id
            ,d.effect_time
            ,d.Logoff_time
            ,round(di.annual_tgt/10000,2) annual_tgt
        from t_assess_dtl d 
        left join t_assess_dtl_idx di 
            on d.id = di.assess_dtl_id
        inner join t_index_pool ip 
            on di.idx_code = ip.index_code
            and ip.business_type != 6
        where d.status in (4,5)   -- 已完成和注销
        and di.idx_code = 'GJJX_YJSR_11000_001_01_000'
    ) x
    where x.rn = 1
    ;-- 部门人数
    truncate table t_dashboard_branch_dept_user_cnt;insert into t_dashboard_branch_dept_user_cnt(par_year,dept_id,user_cnt)
    select par_year,dept_id,count(1) user_cnt
    from t_dashboard_branch_last_annual_tgt
    group by par_year,dept_id
    ;-- 每个员工最新周期的完成值
    truncate table t_dashboard_branch_last_complete_info;insert into t_dashboard_branch_last_complete_info(par_year,assess_pd,assess_pd_value,assess_month,dept_id,user_id,index_code,assess_model,complete_info)
    select 
        x.par_year,x.assess_pd,x.assess_pd_value,x.assess_month,x.dept_id,x.user_id
        ,x.index_code,x.assess_model,x.complete_info
    from (
        select 
             row_number() over(partition by a.par_year,a.dept_id,a.user_id order by a.assess_month desc) rn
            ,a.par_year
            ,a.assess_pd
            ,a.assess_pd_value
            ,a.assess_month
            ,a.dept_id
            ,a.user_id
            ,a.index_code
            ,a.assess_model
            ,a.complete_info
        from t_dashboard_branch_cmplt_idx a
        where a.index_code = 'GJJX_YJSR_11000_001_01_000'
    ) x
    where x.rn = 1
    ;-- 匹配的明细数据
    truncate table t_dashboard_branch_income_cmplt_idx;insert into t_dashboard_branch_income_cmplt_idx(par_year,dept_id,user_id,annual_tgt,complete_info)
    select 
        a.par_year
        ,a.dept_id 
        ,a.user_id
        ,a.annual_tgt
        ,b.complete_info
    from t_dashboard_branch_last_annual_tgt a 
    left join t_dashboard_branch_last_complete_info b 
        on a.par_year = b.par_year
        and a.dept_id = b.dept_id
        and a.user_id = b.user_id
    ;update t_dashboard_branch_income_cmplt_idx a 
    inner join t_employee e 
        on a.user_id = e.sys_user_id 
    left join t_dict_job_rank_mapping dm
        on e.job_rank = dm.job_rank
    left join t_dict_siling_class sc
        on e.workage between sc.siling_start and sc.siling_end
    set a.siling = e.workage -- 司龄
        -- 0~1，2~5，6~10，11~15，16~20，>20
        ,a.siling_class = sc.siling_class
        ,a.job_rank_int = dm.job_rank_int -- 职等_数值
    ;-- 司龄段-全辖  chart-11
    truncate table t_dashboard_branch_income_cmplt;insert into t_dashboard_branch_income_cmplt(par_year,dept_id,chart_xy,siling_class,job_rank_int,user_cnt
                ,achieve_user_cnt,achieve_percentage,annual_tgt,complete_info,annual_tgt_avg,complete_info_avg)
    select 
        a.par_year
        ,b.first_parent_id dept_id
        ,11 chart_xy
        ,a.siling_class
        ,null job_rank_int
        ,count(1) user_cnt -- 参评人数
        ,sum(if(a.complete_info>=a.annual_tgt,1,0)) achieve_user_cnt -- 达标人数
        ,sum(if(a.complete_info>=a.annual_tgt,1,0))/count(1) * 100 achieve_percentage -- 达标人数占比
        ,sum(a.annual_tgt) annual_tgt -- 年度目标值
        ,sum(a.complete_info) complete_info -- 完成值
        ,sum(a.annual_tgt)/count(1) annual_tgt_avg -- 人均业绩目标
        ,sum(a.complete_info)/count(1) complete_info_avg -- 人均业绩完成
    from t_dashboard_branch_income_cmplt_idx a
    inner join t_dashboard_dept_level b
    on a.dept_id = b.dept_id
    where b.first_org_type = 6
    group by a.par_year,b.first_parent_id,a.siling_class
    ;-- 司龄段-仅本部 chart-12
    insert into t_dashboard_branch_income_cmplt(par_year,dept_id,chart_xy,siling_class,job_rank_int,user_cnt
                ,achieve_user_cnt,achieve_percentage,annual_tgt,complete_info,annual_tgt_avg,complete_info_avg)
    select 
        a.par_year
        ,a.dept_id
        ,12 chart_xy
        ,a.siling_class
        ,null job_rank_int
        ,count(1) user_cnt -- 参评人数
        ,sum(if(a.complete_info>=a.annual_tgt,1,0)) achieve_user_cnt -- 达标人数
        ,sum(if(a.complete_info>=a.annual_tgt,1,0))/count(1) * 100 achieve_percentage -- 达标人数占比
        ,sum(a.annual_tgt) annual_tgt -- 年度目标值
        ,sum(a.complete_info) complete_info -- 完成值
        ,sum(a.annual_tgt)/count(1) annual_tgt_avg -- 人均业绩目标
        ,sum(a.complete_info)/count(1) complete_info_avg -- 人均业绩完成
    from t_dashboard_branch_income_cmplt_idx a
    inner join sys_dept d
        on a.dept_id = d.dept_id
        where d.org_type = 6
    group by a.par_year,a.dept_id,a.siling_class
    ;-- 特殊处理：年龄段全部需要展示，没有人展示0；只考虑最新年份
    -- 司龄段-全辖  chart-11 特殊部分
    insert into t_dashboard_branch_income_cmplt(par_year,dept_id,chart_xy,siling_class,job_rank_int,user_cnt
                ,achieve_user_cnt,achieve_percentage,annual_tgt,complete_info,annual_tgt_avg,complete_info_avg)
    select 
        y.last_par_year par_year
        ,y.dept_id
        ,11 chart_xy
        ,a.siling_class
        ,null job_rank_int
        ,0 user_cnt -- 参评人数
        ,0 achieve_user_cnt -- 达标人数
        ,0 achieve_percentage -- 达标人数占比
        ,0 annual_tgt -- 年度目标值
        ,0 complete_info -- 完成值
        ,0 annual_tgt_avg -- 人均业绩目标
        ,0 complete_info_avg -- 人均业绩完成
    from t_dict_siling_class a
    inner join t_dashboard_branch_assess_last_year y
        on 1=1
    left join t_dashboard_branch_income_cmplt b
        on a.siling_class = b.siling_class
        and b.chart_xy = 11
    where exists (select 1 from t_dashboard_branch_income_cmplt b where b.dept_id = y.dept_id and b.par_year = y.last_par_year)
    and exists (select 1 from sys_dept d where d.org_type = '6' and y.dept_id = d.dept_id)
    and b.siling_class is null
    ;-- 司龄段-仅本部  chart-12 特殊部分
    insert into t_dashboard_branch_income_cmplt(par_year,dept_id,chart_xy,siling_class,job_rank_int,user_cnt
                ,achieve_user_cnt,achieve_percentage,annual_tgt,complete_info,annual_tgt_avg,complete_info_avg)
    select 
        y.last_par_year par_year
        ,y.dept_id
        ,12 chart_xy
        ,a.siling_class
        ,null job_rank_int
        ,0 user_cnt -- 参评人数
        ,0 achieve_user_cnt -- 达标人数
        ,0 achieve_percentage -- 达标人数占比
        ,0 annual_tgt -- 年度目标值
        ,0 complete_info -- 完成值
        ,0 annual_tgt_avg -- 人均业绩目标
        ,0 complete_info_avg -- 人均业绩完成
    from t_dict_siling_class a
    inner join t_dashboard_branch_assess_last_year y
        on 1=1
    left join t_dashboard_branch_income_cmplt b
        on a.siling_class = b.siling_class
        and b.chart_xy = 12
    where exists (select 1 from t_dashboard_branch_income_cmplt b where b.dept_id = y.dept_id and b.par_year = y.last_par_year)
    and exists (select 1 from sys_dept d where d.org_type = '6' and y.dept_id = d.dept_id)
    and b.siling_class is null
    ;-- 职等-全辖  chart-21
    insert into t_dashboard_branch_income_cmplt(par_year,dept_id,chart_xy,siling_class,job_rank_int,user_cnt
                ,achieve_user_cnt,achieve_percentage,annual_tgt,complete_info,annual_tgt_avg,complete_info_avg)
    select 
        a.par_year
        ,b.first_parent_id dept_id
        ,21 chart_xy
        ,null siling_class
        ,a.job_rank_int
        ,count(1) user_cnt -- 参评人数
        ,sum(if(a.complete_info>=a.annual_tgt,1,0)) achieve_user_cnt -- 达标人数
        ,sum(if(a.complete_info>=a.annual_tgt,1,0))/count(1) * 100 achieve_percentage -- 达标人数占比
        ,sum(a.annual_tgt) annual_tgt -- 年度目标值
        ,sum(a.complete_info) complete_info -- 完成值
        ,sum(a.annual_tgt)/count(1) annual_tgt_avg -- 人均业绩目标
        ,sum(a.complete_info)/count(1) complete_info_avg -- 人均业绩完成
    from t_dashboard_branch_income_cmplt_idx a
    inner join t_dashboard_dept_level b
    on a.dept_id = b.dept_id
    where b.first_org_type = 6
    group by a.par_year,b.first_parent_id,a.job_rank_int
    ;-- 职等-仅本部 chart-22
    insert into t_dashboard_branch_income_cmplt(par_year,dept_id,chart_xy,siling_class,job_rank_int,user_cnt
                ,achieve_user_cnt,achieve_percentage,annual_tgt,complete_info,annual_tgt_avg,complete_info_avg)
    select 
        a.par_year
        ,a.dept_id
        ,22 chart_xy
        ,null siling_class
        ,a.job_rank_int
        ,count(1) user_cnt -- 参评人数
        ,sum(if(a.complete_info>=a.annual_tgt,1,0)) achieve_user_cnt -- 达标人数
        ,sum(if(a.complete_info>=a.annual_tgt,1,0))/count(1) * 100 achieve_percentage -- 达标人数占比
        ,sum(a.annual_tgt) annual_tgt -- 年度目标值
        ,sum(a.complete_info) complete_info -- 完成值
        ,sum(a.annual_tgt)/count(1) annual_tgt_avg -- 人均业绩目标
        ,sum(a.complete_info)/count(1) complete_info_avg -- 人均业绩完成
    from t_dashboard_branch_income_cmplt_idx a
    inner join sys_dept d
        on a.dept_id = d.dept_id
        where d.org_type = 6
    group by a.par_year,a.dept_id,a.job_rank_int
    ;-- 职等-全辖  chart-21 特殊处理：职等全部需要展示，没有人展示0；只考虑最新年份
    insert into t_dashboard_branch_income_cmplt(par_year,dept_id,chart_xy,siling_class,job_rank_int,user_cnt
                ,achieve_user_cnt,achieve_percentage,annual_tgt,complete_info,annual_tgt_avg,complete_info_avg)
    select 
        y.last_par_year par_year
        ,y.dept_id
        ,21 chart_xy
        ,null siling_class
        ,a.job_rank_int
        ,0 user_cnt -- 参评人数
        ,0 achieve_user_cnt -- 达标人数
        ,0 achieve_percentage -- 达标人数占比
        ,0 annual_tgt -- 年度目标值
        ,0 complete_info -- 完成值
        ,0 annual_tgt_avg -- 人均业绩目标
        ,0 complete_info_avg -- 人均业绩完成
    from t_dict_job_rank a
    inner join t_dashboard_branch_assess_last_year y
        on 1=1
    left join t_dashboard_branch_income_cmplt b
        on a.job_rank_int = b.job_rank_int
        and b.chart_xy = 21
    where exists (select 1 from t_dashboard_branch_income_cmplt b where b.dept_id = y.dept_id and b.par_year = y.last_par_year)
    and exists (select 1 from sys_dept d where d.org_type = '6' and y.dept_id = d.dept_id)
    and b.job_rank_int is null
    and a.job_rank_int <= 24
    ;-- 职等-仅本部  chart-22 特殊处理：职等全部需要展示，没有人展示0；只考虑最新年份
    insert into t_dashboard_branch_income_cmplt(par_year,dept_id,chart_xy,siling_class,job_rank_int,user_cnt
                ,achieve_user_cnt,achieve_percentage,annual_tgt,complete_info,annual_tgt_avg,complete_info_avg)
    select 
        y.last_par_year par_year
        ,y.dept_id
        ,22 chart_xy
        ,null siling_class
        ,a.job_rank_int
        ,0 user_cnt -- 参评人数
        ,0 achieve_user_cnt -- 达标人数
        ,0 achieve_percentage -- 达标人数占比
        ,0 annual_tgt -- 年度目标值
        ,0 complete_info -- 完成值
        ,0 annual_tgt_avg -- 人均业绩目标
        ,0 complete_info_avg -- 人均业绩完成
    from t_dict_job_rank a
    inner join t_dashboard_branch_assess_last_year y
        on 1=1
    left join t_dashboard_branch_income_cmplt b
        on a.job_rank_int = b.job_rank_int
        and b.chart_xy = 22
    where exists (select 1 from t_dashboard_branch_income_cmplt b where b.dept_id = y.dept_id and b.par_year = y.last_par_year)
    and exists (select 1 from sys_dept d where d.org_type = '6' and y.dept_id = d.dept_id)
    and b.job_rank_int is null
    and a.job_rank_int <= 24
    ;-- 人数明细表
    truncate table t_dashboard_branch_income_cmplt_dtl;-- 司龄段-全辖  chart-11
    insert into t_dashboard_branch_income_cmplt_dtl(flag,par_year,dept_id,chart_xy
        ,siling_class,job_rank_int,user_id)
    select 
         0                                       flag                                    -- 标志(0:参评人员，1:达标人数)
        ,a.par_year                              par_year                                -- 年份
        ,b.first_parent_id                       dept_id                                 -- 一级父节点/分公司
        ,11                                      chart_xy                                -- 图表(11:司龄-全辖 12:司龄-仅本部 21:职等-全辖 22:右图-仅本部)
        ,a.siling_class                          siling_class                            -- 司龄段
        ,null                                    job_rank_int                            -- 职等_数值
        ,a.user_id                               user_id                                 -- 用户id
    from t_dashboard_branch_income_cmplt_idx a
    inner join t_dashboard_dept_level b
    on a.dept_id = b.dept_id
    where b.first_org_type = 6
    ;insert into t_dashboard_branch_income_cmplt_dtl(flag,par_year,dept_id,chart_xy
        ,siling_class,job_rank_int,user_id)
    select 
         1                                       flag                                    -- 标志(0:参评人员，1:达标人数)
        ,a.par_year                              par_year                                -- 年份
        ,b.first_parent_id                       dept_id                                 -- 一级父节点/分公司
        ,11                                      chart_xy                                -- 图表(11:司龄-全辖 12:司龄-仅本部 21:职等-全辖 22:右图-仅本部)
        ,a.siling_class                          siling_class                            -- 司龄段
        ,null                                    job_rank_int                            -- 职等_数值
        ,a.user_id                               user_id                                 -- 用户id
    from t_dashboard_branch_income_cmplt_idx a
    inner join t_dashboard_dept_level b
    on a.dept_id = b.dept_id
    where b.first_org_type = 6
    and a.complete_info>=a.annual_tgt
    ;-- 司龄段-仅本部  chart-12
    insert into t_dashboard_branch_income_cmplt_dtl(flag,par_year,dept_id,chart_xy
        ,siling_class,job_rank_int,user_id)
    select 
         0                                       flag                                    -- 标志(0:参评人员，1:达标人数)
        ,a.par_year                              par_year                                -- 年份
        ,a.dept_id                               dept_id                                 -- 一级父节点/分公司
        ,12                                      chart_xy                                -- 图表(11:司龄-全辖 12:司龄-仅本部 21:职等-全辖 22:右图-仅本部)
        ,a.siling_class                          siling_class                            -- 司龄段
        ,null                                    job_rank_int                            -- 职等_数值
        ,a.user_id                               user_id                                 -- 用户id
    from t_dashboard_branch_income_cmplt_idx a
    inner join sys_dept d
    on a.dept_id = d.dept_id
    where d.org_type = 6
    ;insert into t_dashboard_branch_income_cmplt_dtl(flag,par_year,dept_id,chart_xy
        ,siling_class,job_rank_int,user_id)
    select 
         1                                       flag                                    -- 标志(0:参评人员，1:达标人数)
        ,a.par_year                              par_year                                -- 年份
        ,a.dept_id                               dept_id                                 -- 一级父节点/分公司
        ,12                                      chart_xy                                -- 图表(11:司龄-全辖 12:司龄-仅本部 21:职等-全辖 22:右图-仅本部)
        ,a.siling_class                          siling_class                            -- 司龄段
        ,null                                    job_rank_int                            -- 职等_数值
        ,a.user_id                               user_id                                 -- 用户id
    from t_dashboard_branch_income_cmplt_idx a
    inner join sys_dept d
    on a.dept_id = d.dept_id
    where d.org_type = 6
    and a.complete_info>=a.annual_tgt
    ;-- 职等-全辖  chart-21
    insert into t_dashboard_branch_income_cmplt_dtl(flag,par_year,dept_id,chart_xy
        ,siling_class,job_rank_int,user_id)
    select 
         0                                       flag                                    -- 标志(0:参评人员，1:达标人数)
        ,a.par_year                              par_year                                -- 年份
        ,b.first_parent_id                       dept_id                                 -- 一级父节点/分公司
        ,21                                      chart_xy                                -- 图表(11:司龄-全辖 12:司龄-仅本部 21:职等-全辖 22:右图-仅本部)
        ,null                                    siling_class                            -- 司龄段
        ,a.job_rank_int                          job_rank_int                            -- 职等_数值
        ,a.user_id                               user_id                                 -- 用户id
    from t_dashboard_branch_income_cmplt_idx a
    inner join t_dashboard_dept_level b
    on a.dept_id = b.dept_id
    where b.first_org_type = 6
    ;insert into t_dashboard_branch_income_cmplt_dtl(flag,par_year,dept_id,chart_xy
        ,siling_class,job_rank_int,user_id)
    select 
         1                                       flag                                    -- 标志(0:参评人员，1:达标人数)
        ,a.par_year                              par_year                                -- 年份
        ,b.first_parent_id                       dept_id                                 -- 一级父节点/分公司
        ,21                                      chart_xy                                -- 图表(11:司龄-全辖 12:司龄-仅本部 21:职等-全辖 22:右图-仅本部)
        ,null                                    siling_class                            -- 司龄段
        ,a.job_rank_int                          job_rank_int                            -- 职等_数值
        ,a.user_id                               user_id                                 -- 用户id
    from t_dashboard_branch_income_cmplt_idx a
    inner join t_dashboard_dept_level b
    on a.dept_id = b.dept_id
    where b.first_org_type = 6
    and a.complete_info>=a.annual_tgt
    ;-- 职等-仅本部  chart-22
    insert into t_dashboard_branch_income_cmplt_dtl(flag,par_year,dept_id,chart_xy
        ,siling_class,job_rank_int,user_id)
    select 
         0                                       flag                                    -- 标志(0:参评人员，1:达标人数)
        ,a.par_year                              par_year                                -- 年份
        ,a.dept_id                               dept_id                                 -- 一级父节点/分公司
        ,22                                      chart_xy                                -- 图表(11:司龄-全辖 12:司龄-仅本部 21:职等-全辖 22:右图-仅本部)
        ,null                                          siling_class                            -- 司龄段
        ,a.job_rank_int                          job_rank_int                            -- 职等_数值
        ,a.user_id                               user_id                                 -- 用户id
    from t_dashboard_branch_income_cmplt_idx a
    inner join sys_dept d
    on a.dept_id = d.dept_id
    where d.org_type = 6
    ;insert into t_dashboard_branch_income_cmplt_dtl(flag,par_year,dept_id,chart_xy
        ,siling_class,job_rank_int,user_id)
    select 
         1                                       flag                                    -- 标志(0:参评人员，1:达标人数)
        ,a.par_year                              par_year                                -- 年份
        ,a.dept_id                               dept_id                                 -- 一级父节点/分公司
        ,22                                      chart_xy                                -- 图表(11:司龄-全辖 12:司龄-仅本部 21:职等-全辖 22:右图-仅本部)
        ,null                                          siling_class                            -- 司龄段
        ,a.job_rank_int                          job_rank_int                            -- 职等_数值
        ,a.user_id                               user_id                                 -- 用户id
    from t_dashboard_branch_income_cmplt_idx a
    inner join sys_dept d
    on a.dept_id = d.dept_id
    where d.org_type = 6
    and a.complete_info>=a.annual_tgt
    ;-- 更新关联id
    update t_dashboard_branch_income_cmplt_dtl a 
    inner join t_dashboard_branch_income_cmplt b 
    on a.par_year = b.par_year
    and a.dept_id = b.dept_id
    and a.chart_xy = b.chart_xy
    and ifnull(a.siling_class,'') = ifnull(b.siling_class,'')
    and ifnull(a.job_rank_int,'') = ifnull(b.job_rank_int,'')
    set a.dashboard_branch_income_cmplt_id = b.id
    ;end if
;-- 写步骤日志
set @step_info = concat(@step_id,'.看板数据生成完成');set @remark = '';call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);-- 获得记录数(不需要记录)
select 0 into V_TABLE_CNT;-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;