DROP PROCEDURE IF EXISTS PRO_SINGLE_ANNUAL_ASSESS_RESULT;
CREATE PROCEDURE `PRO_SINGLE_ANNUAL_ASSESS_RESULT`(IN BIZ_DATE DATE)
label:BEGIN
/************************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_SINGLE_ANNUAL_ASSESS_RESULT
         功能简述：   年度考核结果
         参数：       BIZ_DATE  数据日期(YYYY-MM-DD)
         注意事项：   通过其他存储过程来调用该存储过程
            部门支持查询时点人事关系部门（是否支持查询已撤销部门？）
            增加查询条件“是否包含历史人员”，
            默认否，若选是，则查询该部门下历史离退人员考核数据（仅支持查询离退人员在该部门离退以前的记录，ps存在离退再入职的情况）
            注：无虚拟账户，注销/废弃用户不需要
         数据源：
                    1. sys_user
                    2. t_appraisal_member
         目标表：
                    1. t_employee_kpi_forward
                    2. t_employee_kpi_forward_idx
         修改记录:
         --------------------------------------------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2024/05/07                  创建,新增【二级部门】字段
         MG         2024/06/03                  全日制营销人员kpi_forward数据逻辑补充
************************************************************************************************************************************************/
-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);


-- 设置变量
set V_EVENT_NAME = '年度考核结果';
select now() into V_START_TIME;

set V_RUN_COMMAND = concat('call PRO_SINGLE_ANNUAL_ASSESS_RESULT('
                            ,BIZ_DATE
                            ,')'
                         );

/*

部门支持查询时点人事关系部门（是否支持查询已撤销部门？）
增加查询条件“是否包含历史人员”，
默认否，若选是，则查询该部门下历史离退人员考核数据（仅支持查询离退人员在该部门离退以前的记录，ps存在离退再入职的情况）

# 情况1：完全正常的人员
没有限制

# 情况2：仅1条离职信息的人员
【是否包含历史人员】选择【是】：展示数据
【是否包含历史人员】选择【否】：不展示


最大变更年份 + 人事部门 + 工号 + user_id + 类型(正常/离职/已组织调整) + 最大时间(离职时间+组织调整时间) 


通过部门工号，找到sys_user表中的user_id，如果是离职可以获得离职时间，如果是已组织调整，可以关联考核权，得到组织调整时间。

部门+工号可以关联多条数据


# 情况3：1条离职，1条正常的数据
A部门 2021 离职 离职时间
A部门 2022 离职 离职时间
B部门 2022 离职 离职时间
A部门 2023 离职 组织调整
C部门 2023 正常 无

选A部门，年份不限制，包含历史人员，结果：2021年之前的所有kpi数据
选B部门，年份不限制，包含历史人员，结果：2022年之前的所有kpi数据

最大变更年份 


注：无虚拟账户，注销/废弃用户不需要

*/

-- 查账号正常或者组织调整但可登录的人
truncate table t_employee_kpi_forward_idx;
insert into t_employee_kpi_forward_idx(dept_id,user_id,user_name,nick_name,Org_Sec_Name,status,change_date)
select 
     dept_id                  dept_id             -- 人事部门
    ,user_id                  user_id             -- 用户ID
    ,user_name                user_name           -- 人员工号
    ,nick_name                nick_name           -- 用户昵称
    ,Org_Sec_Name             Org_Sec_Name        -- 二级部门
    ,0                        status              -- 状态(0:正常，1：离职，3: 已组织调整)
    ,null                     change_date         -- 变更时间(离职时间和组织调整时间)，如果是正常状态，时间为null
from sys_user 
where del_flag = '0' 
    and account_flag = 1
    and account_status in ('0','3') 
    and status = '0'
    and user_id != 1
;


-- 查组织调整且不可登录的人(已组织调整)
insert into t_employee_kpi_forward_idx(dept_id,user_id,user_name,nick_name,Org_Sec_Name,status,change_date)
select 
     a.dept_id                dept_id             -- 人事部门
    ,a.user_id                user_id             -- 用户ID
    ,a.user_name              user_name           -- 人员工号
    ,a.nick_name              nick_name           -- 用户昵称
    ,a.Org_Sec_Name           Org_Sec_Name        -- 二级部门
    ,3                        status              -- 状态(0:正常，1：离职，3: 已组织调整)
    ,m.cancel_date            change_date         -- 变更时间(离职时间和组织调整时间)，如果是正常状态，时间为null
from sys_user a
left join t_appraisal_member m
    on m.deleted = 0
    and a.user_id = m.user_id
where a.del_flag = '0' 
    and a.account_flag = 1 
    and a.account_status = '3'
    and a.status = '1'
;

-- 查离职的人
insert into t_employee_kpi_forward_idx(dept_id,user_id,user_name,nick_name,Org_Sec_Name,status,change_date)
select 
     dept_id                  dept_id             -- 人事部门
    ,user_id                  user_id             -- 用户ID
    ,user_name                user_name           -- 人员工号
    ,nick_name                nick_name           -- 用户昵称
    ,Org_Sec_Name             Org_Sec_Name        -- 二级部门
    ,1                        status              -- 状态(0:正常，1：离职，3: 已组织调整)
    ,enddate                  change_date         -- 变更时间(离职时间和组织调整时间)，如果是正常状态，时间为null
from sys_user 
where del_flag = '0' 
    and account_status = '1'
;

-- 全日制营销人员
insert into t_employee_kpi_forward_idx(dept_id,user_id,user_name,nick_name,Org_Sec_Name,status,change_date)
select 
    dept_id
    ,user_id
    ,user_name
    ,nick_name
    ,Org_Sec_Name
    ,status
    ,change_date
from (
    select 
         row_number() over(partition by a.user_name order by a.par_year desc) rn
        ,replace(b.Org_Code,'ZQ','9') dept_id
        ,a.user_id                   user_id             -- 用户ID
        ,a.user_name                 user_name           -- 人员工号
        ,a.nick_name                 nick_name           -- 用户昵称
        ,a.Org_Sec_Name              Org_Sec_Name        -- 二级部门
        ,0                           status              -- 状态(0:正常，1：离职，3: 已组织调整)
        ,null                        change_date         -- 变更时间(离职时间和组织调整时间)，如果是正常状态，时间为null
    from t_annual_assess_user a
    inner join int_d_new_hrs_emp b
    on a.user_name = b.emp_id
    where user_id < 0
) x
;



-- 汇总
truncate table t_employee_kpi_forward;
insert into t_employee_kpi_forward(max_change_year,dept_id,user_name,nick_name,Org_Sec_Name,status,remark)
select 
     (case when x.status = 0 then '9999'
           when x.status = 1 then year(x.change_date)
           when x.status = 3 then year(x.change_date)-1
      end)                                  max_change_year        -- 最大变更年份
    ,x.dept_id                              dept_id                -- 人事部门
    ,x.user_name                            user_name              -- 人员工号
    ,x.nick_name                            nick_name              -- 用户昵称
    ,x.Org_Sec_Name                         Org_Sec_Name           -- 二级部门
    ,if(x.change_date is null,0,1)          status                 -- 状态(0:正常，1：离职/已组织调整)
    ,'初始化数据'                           remark                 -- 备注
from (
    select 
         row_number() over(partition by a.dept_id,a.user_name order by a.user_id desc) rn
        ,a.dept_id                                    dept_id                -- 人事部门
        ,a.user_name                                  user_name              -- 人员工号
        ,a.nick_name                                  nick_name              -- 用户昵称
        ,a.Org_Sec_Name                               Org_Sec_Name           -- 二级部门
        ,a.change_date                                change_date            -- 变更时间
        ,a.status                                     status                 -- 状态
    from t_employee_kpi_forward_idx a
) x 
where x.rn = 1
;


/*

drop TEMPORARY table tmp_1; 
create TEMPORARY table tmp_1
select 
    a.dept_id,a.user_name
    ,if(year(max(a.change_date))-1 is null,0,1) status
from t_employee_kpi_forward_idx a
group by a.dept_id,a.user_name
;

select a.*,b.status from t_employee_kpi_forward a
left join tmp_1 b 
on a.dept_id = b.dept_id
and a.user_name = b.user_name
where a.status != b.status
;

*/


-- 获得记录数(不需要记录)
select 0 into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;