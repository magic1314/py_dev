DROP PROCEDURE IF EXISTS PRO_MANUAL_INDEPENDENT_DEPT_无效;
CREATE PROCEDURE `PRO_MANUAL_INDEPENDENT_DEPT_无效`(IN INDEP_DEPT_ID INT, IN ORIGIN_CENTER_DPET_ID INT, EFFECT_DATE DATE, BACKUP_FLAG INT)
label:BEGIN
/************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_MANUAL_INDEPENDENT_DEPT
         功能简述：   独立卫星营业部
         参数：       INDEP_DEPT_ID              需要独立的卫星营业部
                      ORIGIN_CENTER_DPET_ID      原先的中心营业部
                      EFFECT_DATE                生效日期
                      BACKUP_FLAG                备份标识(1:备份,0:不备份)
         返回：       无
         算法：
         注意事项：
                 以下两个营业部原先为卫星营业部，现在转独立营业部了，涉及部门信息，用户信息、考核权、考核组、考核方案的数据需要进行调整，大峰汇总一个脚本，后续有部门独立了，替换相关部门编码即可
         数据源：
         目标表：
         修改记录;
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2023/05/15                  创建
***************************************************************************/

/*
原卫星营业部： @dept_id_center
原中心营业部： @dept_id_satellite
现营业部：26713


生效日2023年4月1日


人员调动，考核权生效时间会有影响

*/



-- 设置变量
set @indep_dept_id = INDEP_DEPT_ID; -- 格式: 12800
set @origin_center_dpet_id = ORIGIN_CENTER_DPET_ID;   -- 格式：12801
set @effect_date = EFFECT_DATE; -- 2023-05-15
set @par_year = left(EFFECT_DATE,4);   -- 格式：2023

set @today =  DATE_FORMAT(current_date, '%Y%m%d');


-- select @EFFECT_DATE,@par_year,day(@effect_date);

/*************** 做一些必要的检查 start **********************/

-- 1.生效日期检查
if day(@effect_date) != 1 then
    select '生效日期必须是1号' info;
    LEAVE label; -- 退出存储过程 
end if;



-- 2.检查营业部
select count(1) into @dept_cnt from sys_dept
where dept_id in (@indep_dept_id,@origin_center_dpet_id)
and fic_org = 0  -- 非卫星营业部
;


if @dept_cnt != 2 then 
    select '传入的部门id存在问题' info;
    LEAVE label; -- 退出存储过程
end if;



-- 3.检查原考核组中是否有卫星营业部的人员
select count(1) into @indep_dept_user_cnt from t_appraisal_member
where user_id in (
    select user_id from sys_user
    where dept_id = @indep_dept_id
) and dept_id = @origin_center_dpet_id
;


if @indep_dept_user_cnt = 0 then 
    select '原考核组需要有卫星营业部人员' info;
    LEAVE label; -- 退出存储过程
end if;



-- 4.卫星营业部人员在考核组，不能有【直接汇报上级】和【调整人】
select count(1) into @leader_cnt from t_appraisal_group
where dept_id = @origin_center_dpet_id
and (
        leader_id in (
            select user_id from sys_user
            where dept_id = @indep_dept_id
        )
        or
        adjust_id in (
            select user_id from sys_user
            where dept_id = @indep_dept_id
        )
    )
and deleted = 0
;

if @indep_dept_user_cnt > 0 then 
    select '原考核组不能有直接汇报上级和调整人' info;
    LEAVE label; -- 退出存储过程
end if;



-- 5.卫星营业部人员在考核组，不能有【打分人】
select count(1) into @reviewer_cnt from t_appraisal_group_reviewer a
where dept_id = @origin_center_dpet_id
and appraisal_id in (
    select id from t_appraisal_member
    where user_id in (
        select user_id from sys_user
        where dept_id = @indep_dept_id
    )
)
;


if @reviewer_cnt > 0 then 
    select '原考核组不能有打分人' info;
    LEAVE label; -- 退出存储过程
end if;

-- 6.考核方案没有进行中的记录
select count(1) assess_cnt from t_dept_annual_assess 
where par_year = @par_year
and dept_id = @origin_center_dpet_id
and status != 3
;


if @assess_cnt = 0 then 
    select '考核方案没有进行中的记录' info;
    LEAVE label; -- 退出存储过程
end if;



/*************** 做一些必要的检查 end **********************/


-- 备份表
if BACKUP_FLAG = 1 then 
    SET @sql = CONCAT('create table backup_',@today,'_t_appraisal_member as select * from t_appraisal_member;');
    PREPARE stmt FROM @sql;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;


    SET @sql = CONCAT('create table backup_',@today,'_t_appraisal_group_member as select * from t_appraisal_group_member;');
    PREPARE stmt FROM @sql;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

    SET @sql = CONCAT('create table backup_',@today,'_t_assess_dtl as select * from t_assess_dtl;');
    PREPARE stmt FROM @sql;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;

end if;



-- 1.注销原卫星营业部考核权
update t_appraisal_member
set user_status = 2
    ,cancel_date = date_sub(@effect_date, interval 1 day)
    ,remark1 = '注销原卫星营业部考核权'
where user_id in (
    select user_id from sys_user
    where dept_id = @indep_dept_id
) and dept_id = @origin_center_dpet_id
;

-- 2.考核组成员注销
update t_appraisal_group_member
set logoff_time = date_sub(@effect_date, interval 1 day)
    ,member_status = 2
where member_id in (
    select user_id from sys_user
    where dept_id = @indep_dept_id
) and dept_id = @origin_center_dpet_id
;



-- 3.考核方案注销
update t_assess_dtl
set status = 5
    ,Logoff_time = date_sub(@effect_date, interval 1 day)
where user_id in (
    select user_id from sys_user
    where dept_id = @indep_dept_id
) and dept_id = @origin_center_dpet_id
and par_year = @par_year
;


END
;