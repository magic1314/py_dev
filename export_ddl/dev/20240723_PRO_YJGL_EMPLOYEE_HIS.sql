DROP PROCEDURE IF EXISTS PRO_YJGL_EMPLOYEE_HIS;
CREATE PROCEDURE `PRO_YJGL_EMPLOYEE_HIS`(IN IN_YEAR_MONTH VARCHAR(6))
BEGIN
/************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_YJGL_EMPLOYEE_HIS
         功能简述：   员工信息归档表
         参数：       IN_YEAR_MONTH    考核年月
         返回：       无
         算法：
         注意事项：
                 1. 传入日期的上个月，是考核的月份
                 eg.  今日为：202303
                      考核月份是：202303，也就是结果表中year_months的取值
            PRO_YJGL_EMPLOYEE_HIS     2. 该脚本是每月1号，02:00开始执行,依赖于01:00执行的【PRO_LOAD_HR_EMP】存储过程
         数据源：
                 1、 sys_user                 系统用户表
                 2、 t_employee               员工表
         修改记录;
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2023/03/30                  创建
         MG         2023/04/18                  参数传入年月
         MG         2023/09/21                  添加账户状态字段，过滤条件加入删除标识
         MG         2023/10/30                  是否管理员字段更新
***************************************************************************/

-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);

-- 设置变量
set V_EVENT_NAME = '员工信息';
set V_RUN_COMMAND = concat('call PRO_YJGL_EMPLOYEE_HIS('
                            ,IN_YEAR_MONTH
                            ,')'
                         );



select now() into V_START_TIME;

set @assess_year_month = IN_YEAR_MONTH; -- 格式: 202302

/*
-- 数据核对
select distinct b.* from sys_user b 
left join t_employee a
on a.sys_user_id = b.user_id 
where a.sys_user_id is null
and b.account_flag = 1
and b.account_status != 1
and b.user_id != 1
*/

delete from t_employee_his where year_months = @assess_year_month;
insert into t_employee_his(year_months,user_id,emp_code,emp_name,account_status,account_flag,ident_id,dept_id,Org_Sec_Code,Org_Sec_Name,job_rank
    ,pk_job,role_id,begindate,enddate,indutydate,enddutydate
    ,Emp_Type_Code,Emp_Type_Name,Managercode,Job_Appt_Dt,Job_Rank_Appt_Dt,Enter_Type)
select 
     @assess_year_month year_months
    ,b.user_id -- 员工ID
    ,b.user_name emp_code
    ,b.nick_name  emp_name  -- 姓名
    ,b.account_status
    ,b.account_flag
    ,a.ident_id   -- 证件号码
    ,b.dept_id
    ,a.Org_Sec_Code
    ,a.Org_Sec_Name
    ,a.job_rank
    ,a.pk_job
    ,c.role_id
    ,a.begindate
    ,a.enddate
    ,a.indutydate
    ,a.enddutydate
    ,a.Emp_Type_Code
    ,a.Emp_Type_Name
    ,a.Managercode
    ,a.Job_Appt_Dt
    ,a.Job_Rank_Appt_Dt
    ,a.Enter_Type
from sys_user b 
left join t_employee a
on a.sys_user_id = b.user_id 
left join sys_user_role c 
on b.user_id = c.user_id
where b.del_flag = '0'
;


update t_employee_his a
inner join sys_dict_data d 
    on d.dict_type = 'emp_manager_status'
    and a.Managercode = d.dict_value
set a.Manager_status = case when d.remark = '管理人员' then 0 when d.remark = '非管理人员' then 1 end
    ,a.imp_manager_status = if(d.remark = '管理人员',0,1)
where a.year_months = @assess_year_month
;




-- 获得记录数
select count(1) cnt into V_TABLE_CNT from t_employee_his where year_months = @assess_year_month;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;