DROP PROCEDURE IF EXISTS PRO_SINGLE_ARCHIVES;
CREATE PROCEDURE `PRO_SINGLE_ARCHIVES`(IN BIZ_DATE DATE)
label:BEGIN
/************************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_SINGLE_ARCHIVES
         功能简述：   全员档案
         参数：       BIZ_DATE  数据日期(YYYY-MM-DD)
         注意事项：   通过其他存储过程来调用该存储过程
         数据源：
                    1. sys_dept
                    2. sys_user
                    3. t_appraisal_group
                    4. t_appraisal_group_member
                    5. t_assess_member_score
                    6. t_dict_assess_pd_mqh
                    7. t_dict_job_rank
                    8. t_dict_job_rank_mapping
                    9. t_employee
         目标表：
                    1. t_dashboard_user_assess_avg_dept
                    2. t_dashboard_user_assess_avg_group
                    3. t_dashboard_user_assess_sum_dept
                    4. t_dashboard_user_assess_sum_group
                    5. t_dashboard_user_assess_lastest         最终结果表
         修改记录:
         --------------------------------------------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2024/05/06                  创建-个人象限部分
         MG         2024/07/02                  部门均值和团队均值逻辑调整
************************************************************************************************************************************************/
-- 变量定义
DECLARE V_TABLE_CNT int;DECLARE V_START_TIME datetime;DECLARE V_EVENT_NAME varchar(200);DECLARE V_RUN_COMMAND varchar(1000);-- 设置变量
set V_EVENT_NAME = '全员档案';select now() into V_START_TIME;set V_RUN_COMMAND = concat('call PRO_SINGLE_ARCHIVES('
                            ,BIZ_DATE
                            ,')'
                         );/***************** 模块：个人象限数据列定 *****************/
-- 所有用户最新一条考核记录
drop TEMPORARY table if exists tmp_dashboard_user_assess_lastest;CREATE TEMPORARY TABLE `tmp_dashboard_user_assess_lastest` (
  `id` bigint NOT NULL DEFAULT '0' COMMENT '主键',
  `par_year` char(4) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '考核年份',
  `assess_pd_value` varchar(3) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '考核周期值',
  `dept_id` bigint DEFAULT NULL COMMENT '考核权部门编码',
  `user_id` bigint DEFAULT NULL COMMENT '被考核人员ID',
  `user_name` varchar(30) COLLATE utf8mb4_general_ci COMMENT '用户账号',
  `nick_name` varchar(100) COLLATE utf8mb4_general_ci COMMENT '用户昵称',
  `final_assess_score` decimal(12,4) DEFAULT NULL COMMENT '考核得分=本期绩效得分+调整项得分',
  `group_id` bigint DEFAULT NULL COMMENT '考核组ID',
  key idx_user_id(user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '临时表-所有用户最新一条考核记录'
;insert into tmp_dashboard_user_assess_lastest(id,par_year,assess_pd_value,dept_id,user_id
   ,user_name,nick_name,final_assess_score,group_id)
select
   x.id
   ,x.par_year,x.assess_pd_value,x.dept_id
   ,x.member_id user_id -- 实体或虚拟账户
    ,x.user_name,x.nick_name,x.final_assess_score,x.group_id
from
(
    select 
        row_number() over(partition by a.member_id order by a.par_year desc,b.assess_month desc) rn
        ,a.id
        ,a.par_year
        ,a.assess_pd_value
        ,a.dept_id
        ,a.member_id
        ,u.user_name
        ,u.nick_name
        ,a.final_assess_score
        ,a.group_id
    from t_assess_member_score a
    left join sys_user u 
        on a.member_id=u.user_id
    left join t_dict_assess_pd_mqh b
        on a.assess_pd_value = b.assess_pd_value
) x
where x.rn = 1
;-- 当前有效的考核组成员
drop TEMPORARY table if exists tmp_dashboard_valid_user;CREATE TEMPORARY TABLE `tmp_dashboard_valid_user` (
  `user_id` bigint NOT NULL DEFAULT '0' COMMENT '用户ID',
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
  `nick_name` varchar(300) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
  `age` int DEFAULT NULL COMMENT '年龄',
  `siling` int DEFAULT NULL COMMENT '工龄',
  `job_rank` int DEFAULT NULL COMMENT '数字职等(标准的职等 1,2,3...)',
  `job_class` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '职级(MD,ED,D)',
  key idx_user_id(user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci
;insert into tmp_dashboard_valid_user(user_id,user_name,nick_name,age,siling,job_rank,job_class)
select
     a.user_id      -- 用户id
    ,a.user_name    -- 工号
    ,a.nick_name    -- 姓名
    ,b.age
    ,b.workage      siling
    ,m.job_rank_int job_rank
    ,n.job_class
from sys_user a
inner join t_employee b
     on a.user_id = b.sys_user_id
     and (b.Managercode in ('013','014','020','022','023') or b.Managercode IS NULL)
left join sys_dept d 
    on a.dept_id = d.dept_id
left join t_dict_job_rank_mapping m 
    on b.job_rank = m.job_rank 
    and m.`status` = 0
left join t_dict_job_rank n 
    on m.job_rank_int = n.job_rank_int
inner join t_appraisal_group_member gm 
    on if(d.Fic_Org='0',d.dept_id,d.parent_id) = gm.dept_id
    and a.user_id = gm.member_id
    and gm.member_status = 1
    and gm.deleted = 0
where a.account_status = 0 
and a.account_flag = '1'
;-- 所有用户最新一条考核记录
truncate table t_dashboard_user_assess_lastest;insert into t_dashboard_user_assess_lastest(id,par_year,assess_pd_value,dept_id,user_id
     ,user_name,nick_name,group_id,group_name,age,siling,job_rank,job_class,final_assess_score)
select
     t1.id                           id                 -- t_assess_member_score表主键
    ,t1.par_year                     par_year           -- 年份
    ,t1.assess_pd_value              assess_pd_value    -- 考核周期
    ,t1.dept_id                      dept_id            -- 部门id
    ,t1.user_id                      user_id            -- 用户id
    ,t1.user_name                    user_name          -- 工号
    ,t1.nick_name                    nick_name          -- 姓名
    ,t1.group_id                     group_id           -- 考核组id
    ,g.group_name                    group_name         -- 考核组
    ,ifnull(t2.age     ,0)           age                -- 年龄
    ,ifnull(t2.siling  ,0)           siling             -- 司龄
    ,ifnull(t2.job_rank,0)           job_rank           -- 职等
    ,t2.job_class                    job_class          -- 职级
    ,ifnull(t1.final_assess_score,0) final_assess_score -- 得分
from tmp_dashboard_user_assess_lastest t1 
inner join tmp_dashboard_valid_user t2 
on t1.user_id = t2.user_id
left join t_appraisal_group g 
    on t1.group_id = g.id
    and g.deleted = 0
;-- 按部门计算汇总值
truncate table t_dashboard_user_assess_sum_dept;insert into t_dashboard_user_assess_sum_dept(dept_id,age_dept_sum,siling_dept_sum,job_rank_dept_sum,final_assess_score_dept_sum,people_cnt_dept)
select 
     dept_id                 dept_id
    ,sum(age     )           age_dept_sum
    ,sum(siling  )           siling_dept_sum
    ,sum(job_rank)           job_rank_dept_sum
    ,sum(final_assess_score) final_assess_score_dept_sum
    ,count(distinct user_id) people_cnt_dept
from t_dashboard_user_assess_lastest
group by dept_id
;-- 按团队计算汇总值
truncate table t_dashboard_user_assess_sum_group;insert into t_dashboard_user_assess_sum_group(dept_id,group_id,age_group_sum,siling_group_sum,job_rank_group_sum,final_assess_score_group_sum,people_cnt_group)
select 
     dept_id                 dept_id
    ,group_id                group_id
    ,sum(age     )           age_group_sum
    ,sum(siling  )           siling_group_sum
    ,sum(job_rank)           job_rank_group_sum
    ,sum(final_assess_score) final_assess_score_group_sum
    ,count(distinct user_id) people_cnt_group
from t_dashboard_user_assess_lastest
group by dept_id,group_id
;-- 按部门计算均值
truncate table t_dashboard_user_assess_avg_dept;insert into t_dashboard_user_assess_avg_dept(dept_id,age_dept_avg,siling_dept_avg,job_rank_dept_avg,final_assess_score_dept_avg)
select 
     s.dept_id                                                     dept_id
    ,ifnull(s.age_dept_sum                / s.people_cnt_dept, 0)  age_dept_avg
    ,ifnull(s.siling_dept_sum             / s.people_cnt_dept, 0)  siling_dept_avg
    ,ifnull(s.job_rank_dept_sum           / s.people_cnt_dept, 0)  job_rank_dept_avg
    ,ifnull(s.final_assess_score_dept_sum / s.people_cnt_dept, 0)  final_assess_score_dept_avg
from t_dashboard_user_assess_sum_dept s
group by s.dept_id
;-- 按团队计算均值
truncate table t_dashboard_user_assess_avg_group;insert into t_dashboard_user_assess_avg_group(dept_id,group_id,age_group_avg,siling_group_avg,job_rank_group_avg,final_assess_score_group_avg)
select 
     s.dept_id                                                      dept_id
    ,s.group_id                                                     group_id
    ,ifnull(s.age_group_sum                / s.people_cnt_group, 0) age_group_avg
    ,ifnull(s.siling_group_sum             / s.people_cnt_group, 0) siling_group_avg
    ,ifnull(s.job_rank_group_sum           / s.people_cnt_group, 0) job_rank_group_avg
    ,ifnull(s.final_assess_score_group_sum / s.people_cnt_group, 0) final_assess_score_group_avg
from t_dashboard_user_assess_sum_group s
group by s.dept_id,s.group_id
;-- 更新部门相关汇总值和均值
update t_dashboard_user_assess_lastest a
inner join t_dashboard_user_assess_sum_dept b
    on a.dept_id = b.dept_id 
inner join t_dashboard_user_assess_avg_dept c 
    on a.dept_id = c.dept_id 
set  a.age_dept_sum                = b.age_dept_sum     
    ,a.age_dept_avg                = c.age_dept_avg     
    ,a.siling_dept_sum             = b.siling_dept_sum  
    ,a.siling_dept_avg             = c.siling_dept_avg  
    ,a.job_rank_dept_sum           = b.job_rank_dept_sum
    ,a.job_rank_dept_avg           = c.job_rank_dept_avg
    ,a.final_assess_score_dept_sum = b.final_assess_score_dept_sum
    ,a.final_assess_score_dept_avg = c.final_assess_score_dept_avg
;-- 更新团队相关汇总值和均值
update t_dashboard_user_assess_lastest a
inner join t_dashboard_user_assess_sum_group b
    on a.dept_id = b.dept_id 
    and a.group_id = b.group_id
inner join t_dashboard_user_assess_avg_group c 
    on a.dept_id = c.dept_id 
    and a.group_id = c.group_id
set  a.age_group_sum                = b.age_group_sum     
    ,a.age_group_avg                = c.age_group_avg     
    ,a.siling_group_sum             = b.siling_group_sum  
    ,a.siling_group_avg             = c.siling_group_avg  
    ,a.job_rank_group_sum           = b.job_rank_group_sum
    ,a.job_rank_group_avg           = c.job_rank_group_avg
    ,a.final_assess_score_group_sum = b.final_assess_score_group_sum
    ,a.final_assess_score_group_avg = c.final_assess_score_group_avg
;/*
6、这个人的排名问题分为部门内同职级、同年龄段、同司龄段排名和团队内同职级、同年龄段、同司龄段排名
（1）职级就按照A VP SVP D ED MD进行划分
（2）年龄就按照（部门绩效看板中”年龄结构“划分标准）35岁以下、35-40、40-45、45-50、50-55、55以上
（3）司龄就按照（0-5,6-10,11-15,16-20，21-25,26+）划分

*/
update t_dashboard_user_assess_lastest a
set a.age_class = case when a.age < 35 then '35岁以下'
                       when a.age between 35 and 39 then '35-39'
                       when a.age between 40 and 44 then '40-44'
                       when a.age between 45 and 49 then '45-50'
                       when a.age between 50 and 54 then '51-54'
                       when a.age >= 55 then '55及以上'
                  end
    ,a.siling_class = case when a.siling between 0 and 5 then '0-5'
                           when a.siling between 6 and 10 then '6-10'
                           when a.siling between 11 and 15 then '11-15'
                           when a.siling between 16 and 20 then '16-20'
                           when a.siling between 21 and 25 then '21-26'
                           when a.siling >= 26 then '26+'
                      end
;-- 部门/团队 同职级、同年龄段、同司龄段排名
drop temporary table if exists tmp_dashboard_user_rank;CREATE TEMPORARY TABLE `tmp_dashboard_user_rank` (
  `par_year` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '考核年份',
  `dept_id` bigint DEFAULT NULL COMMENT '考核权部门编码',
  `group_id` bigint DEFAULT NULL COMMENT '考核组ID',
  `user_id` bigint DEFAULT NULL COMMENT '被考核人员ID',
  `final_assess_score` decimal(26,4) DEFAULT NULL COMMENT '个人最终得分',
  `job_class` varchar(5) COMMENT '职级(职等段)',
  `rank_dept_job_class` int comment '部门同职级排名',
  `rank_group_job_class` int comment '团队同职级排名' ,
  `siling_class` varchar(10) COMMENT '司龄段',
  `rank_dept_siling_class` int comment '部门同司龄段排名',
  `rank_group_siling_class` int comment '团队同司龄段排名',
  `age_class` varchar(10) COMMENT '年龄段',
  `rank_dept_age_class` int comment '部门同年龄段排名',
  `rank_group_age_class` int comment '团队同年龄段排名',
  key idx_dept_id(dept_id),
  key idx_group_id(group_id),
  key idx_user_id(user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci
;insert into tmp_dashboard_user_rank(par_year,dept_id,group_id,user_id,final_assess_score,job_class,rank_dept_job_class,rank_group_job_class
    ,siling_class,rank_dept_siling_class,rank_group_siling_class,age_class,rank_dept_age_class,rank_group_age_class)
select 
    a.par_year
    ,a.dept_id
    ,a.group_id
    ,a.user_id
    ,a.final_assess_score
    ,a.job_class
    ,row_number() over(partition by a.dept_id,a.job_class               order by a.final_assess_score desc) rank_dept_job_class
    ,row_number() over(partition by a.dept_id,a.group_id,a.job_class    order by a.final_assess_score desc) rank_group_job_class
    ,a.siling_class
    ,row_number() over(partition by a.dept_id,a.siling_class            order by a.final_assess_score desc) rank_dept_siling_class
    ,row_number() over(partition by a.dept_id,a.group_id,a.siling_class order by a.final_assess_score desc) rank_group_siling_class
    ,a.age_class
    ,row_number() over(partition by a.dept_id,a.age_class               order by a.final_assess_score desc) rank_dept_age_class
    ,row_number() over(partition by a.dept_id,a.group_id,a.age_class    order by a.final_assess_score desc) rank_group_age_class
from t_dashboard_user_assess_lastest a 
;update t_dashboard_user_assess_lastest a
inner join tmp_dashboard_user_rank b 
on a.dept_id = b.dept_id
   and a.group_id = b.group_id
   and a.user_id = b.user_id
set a.rank_dept_job_class      =  b.rank_dept_job_class    
   ,a.rank_group_job_class     =  b.rank_group_job_class   
   ,a.rank_dept_siling_class   =  b.rank_dept_siling_class 
   ,a.rank_group_siling_class  =  b.rank_group_siling_class
   ,a.rank_dept_age_class      =  b.rank_dept_age_class    
   ,a.rank_group_age_class     =  b.rank_group_age_class   
;/****************** 计算排名的分母(denominator) ******************/
-- 部门粒度
drop temporary table if exists tmp_dashboard_dept_denominator_job;create temporary table tmp_dashboard_dept_denominator_job
select 
    dept_id
    ,job_class
    ,count(1) denominator
from tmp_dashboard_user_rank
group by dept_id,job_class
;drop temporary table if exists tmp_dashboard_dept_denominator_siling;create temporary table tmp_dashboard_dept_denominator_siling
select 
    dept_id
    ,siling_class
    ,count(1) denominator
from tmp_dashboard_user_rank
group by dept_id,siling_class
;drop temporary table if exists tmp_dashboard_dept_denominator_age;create temporary table tmp_dashboard_dept_denominator_age
select 
    dept_id
    ,age_class
    ,count(1) denominator
from tmp_dashboard_user_rank
group by dept_id,age_class
;-- 团队粒度
drop temporary table if exists tmp_dashboard_group_denominator_job;create temporary table tmp_dashboard_group_denominator_job
select 
    dept_id
    ,group_id
    ,job_class
    ,count(1) denominator
from tmp_dashboard_user_rank
group by dept_id,group_id,job_class
;drop temporary table if exists tmp_dashboard_group_denominator_siling;create temporary table tmp_dashboard_group_denominator_siling
select 
    dept_id
    ,group_id
    ,siling_class
    ,count(1) denominator
from tmp_dashboard_user_rank
group by dept_id,group_id,siling_class
;drop temporary table if exists tmp_dashboard_group_denominator_age;create temporary table tmp_dashboard_group_denominator_age
select 
    dept_id
    ,group_id
    ,age_class
    ,count(1) denominator
from tmp_dashboard_user_rank
group by dept_id,group_id,age_class
;alter table tmp_dashboard_dept_denominator_job      add index idx_2(dept_id,job_class);alter table tmp_dashboard_dept_denominator_siling   add index idx_2(dept_id,siling_class);alter table tmp_dashboard_dept_denominator_age      add index idx_2(dept_id,age_class);alter table tmp_dashboard_group_denominator_job     add index idx_3(dept_id,group_id,job_class);alter table tmp_dashboard_group_denominator_siling  add index idx_3(dept_id,group_id,siling_class);alter table tmp_dashboard_group_denominator_age     add index idx_3(dept_id,group_id,age_class);update t_dashboard_user_assess_lastest a
left join tmp_dashboard_dept_denominator_job t11
    on a.dept_id = t11.dept_id
    and a.job_class = t11.job_class
left join tmp_dashboard_dept_denominator_siling t12
    on a.dept_id = t12.dept_id
    and a.siling_class = t12.siling_class
left join tmp_dashboard_dept_denominator_age t13
    on a.dept_id = t13.dept_id
    and a.age_class = t13.age_class
left join tmp_dashboard_group_denominator_job t21
    on a.dept_id = t21.dept_id
    and a.group_id = t21.group_id
    and a.job_class = t21.job_class
left join tmp_dashboard_group_denominator_siling t22
    on a.dept_id = t22.dept_id
    and a.group_id = t22.group_id
    and a.siling_class = t22.siling_class
left join tmp_dashboard_group_denominator_age t23
    on a.dept_id = t23.dept_id
    and a.group_id = t23.group_id
    and a.age_class = t23.age_class
set 
     a.rank_dept_job_class_denominator     = t11.denominator
    ,a.rank_dept_siling_class_denominator  = t12.denominator
    ,a.rank_dept_age_class_denominator     = t13.denominator
    ,a.rank_group_job_class_denominator    = t21.denominator
    ,a.rank_group_siling_class_denominator = t22.denominator
    ,a.rank_group_age_class_denominator    = t23.denominator
;update t_dashboard_user_assess_lastest a 
left join t_dict_job_rank d
on round(a.job_rank_dept_avg) = d.job_rank_int
left join t_dict_job_rank g
on round(a.job_rank_group_avg) = g.job_rank_int
set  a.job_class_dept_avg  = d.job_class
    ,a.job_class_group_avg = g.job_class
;-- 获得记录数(不需要记录)
select 0 into V_TABLE_CNT;-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;
;