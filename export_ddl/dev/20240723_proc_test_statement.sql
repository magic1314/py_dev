DROP PROCEDURE IF EXISTS proc_test_statement;
CREATE PROCEDURE `proc_test_statement`(
    IN num INT(11),
    OUT result VARCHAR(255)
 )
BEGIN 
    IF(num >= 80) THEN
        SET result='优秀';
    ELSEIF num >= 70 THEN
        SET result='良好';
    ELSEIF (num >= 60 ) THEN
        SET result='及格';
    ELSE
        SET result='不及格';
    END IF;
END
;