DROP PROCEDURE IF EXISTS PRO_TABLE_INFO_RECORD;
CREATE PROCEDURE `PRO_TABLE_INFO_RECORD`(IN IN_DATE DATE)
label:BEGIN
/************************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_TABLE_INFO_RECORD
         功能简述：   表信息记录
         参数：       IN_DATE    日期
         返回：       无
         数据源：
         目标表：
         注意事项：
                      仅用于开发环境
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG          2024/08/01                 创建该过程
************************************************************************************************************************************************/
-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);

-- 设置变量
set V_EVENT_NAME = '考核评价模块数据初始化补充';
set V_RUN_COMMAND = concat('call PRO_TABLE_INFO_RECORD('
                            ,IN_DATE
                            ,')'
                         );



select now() into V_START_TIME;



drop TEMPORARY table if exists tmp_table;
create TEMPORARY table tmp_table
select table_name,TABLE_COMMENT,CREATE_TIME,UPDATE_TIME 
from information_schema.tables
where (left(table_name,2) = 't_' or left(table_name,4) = 'sys_')
and table_schema = 'hr_jx'
and TABLE_ROWS > 0
and table_name not like '%copy1'
order by CREATE_TIME
;



/*
drop table if exists t_record_table_info;
CREATE TABLE `t_record_table_info` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键',
  `TABLE_NAME` varchar(64)  comment '表名',
  `TABLE_COMMENT` varchar(100) comment '表注释',
  min_create_time datetime comment '最小的创建时间（初始化时间）',
  `remark` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_TABLE_NAME` (`TABLE_NAME`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户信息记录表'
;
*/

insert into t_record_table_info(table_name,TABLE_COMMENT,min_create_time)
select 
    a.table_name
    ,a.TABLE_COMMENT
    ,a.CREATE_TIME
from tmp_table a
where not exists(select 1 from t_record_table_info b where a.table_name = b.table_name)
;





-- 获得记录数
select 0 into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;