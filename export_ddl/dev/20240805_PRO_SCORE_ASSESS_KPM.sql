DROP PROCEDURE IF EXISTS PRO_SCORE_ASSESS_KPM;
CREATE PROCEDURE `PRO_SCORE_ASSESS_KPM`(IN IN_PAR_YEAR VARCHAR(4), IN IN_ASSESS_PD_VALUE VARCHAR(3), IN IN_DEPT_ID INT, IN IN_USER_ID INT, IN IN_OPER_TYPE INT)
label:BEGIN
/************************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_SCORE_ASSESS_KPM
         功能简述：   考核评价模块KPM提醒
         参数：       IN_PAR_YEAR           考核年份
                      IN_ASSESS_PD_VALUE    考核周期值(比如 M12, Q1, H2)
                      IN_DEPT_ID            部门编号(必须指定部门)
                      IN_USER_ID            用户编码(0:代表该部门该周期下全部员工)
                      IN_OPER_TYPE          操作类型(1:启动考核, 2:一键复原, 3:打分人/调整人提交打分)
         数据源：
                      sys_dept                         部门表
                      sys_user                         用户表
                      t_assess_dept_progress           进度表
                      t_assess_dept_progress_scorer    打分人/调整人打分状态表
                      t_kpm_define                     kpm定义表
         目标表：
                      t_kpm_task                       kpm任务表
         注意事项：
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG          2024/07/26                 创建该过程
         MG          2024/07/29                 调整逻辑
************************************************************************************************************************************************/
-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);

-- 设置变量
set V_EVENT_NAME = '考核评价模块数据初始化补充';
set V_RUN_COMMAND = concat('call PRO_SCORE_ASSESS_KPM('
                            ,IN_PAR_YEAR,','
                            ,IN_DEPT_ID,','
                            ,IN_ASSESS_PD_VALUE
                            ,')'
                         );

select now() into V_START_TIME;

set @par_year = IN_PAR_YEAR;                  -- 格式： 2024
set @assess_pd_value = IN_ASSESS_PD_VALUE;    -- 格式： M3
set @dept_id = IN_DEPT_ID;                    -- 格式： 12800
set @user_id = IN_USER_ID;                    -- 格式:  11241
set @oper_type = IN_OPER_TYPE;                -- 格式： 2


-- 最近两年中，所有考核人和调整人的未打分的情况
drop TEMPORARY TABLE if exists `tmp_score_assess_kpm_dtl`;
CREATE TEMPORARY TABLE `tmp_score_assess_kpm_dtl` (
  `par_year` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '考核年份',
  `dept_id` bigint DEFAULT NULL COMMENT '部门编码',
  `assess_pd_value` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '考核周期值',
  `user_flag` int DEFAULT NULL COMMENT '用户标识(1:考核人/2:调整人)',
  `user_id` bigint DEFAULT NULL COMMENT '用户id',
  KEY idx_user_id(user_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '临时表-所有考核人和调整人的未打分的情况'
;


-- 用户任务记录数情况
drop TEMPORARY TABLE if exists `tmp_score_assess_kpm_user`;
CREATE TEMPORARY TABLE `tmp_score_assess_kpm_user` (
  `user_id` bigint NOT NULL COMMENT '用户id',
  `task_cnt` int DEFAULT NULL COMMENT '任务数',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '临时表-用户任务记录数情况'
;

insert into tmp_score_assess_kpm_dtl(par_year,dept_id,assess_pd_value,user_flag,user_id)
select par_year,dept_id,assess_pd_value,user_flag,user_id
from t_assess_dept_progress_scorer x
where par_year in (year(date_sub(CURRENT_DATE, interval 1 year)), year(CURRENT_DATE))
and exists (
                select 1 from t_assess_dept_progress p 
                where p.par_year = x.par_year 
                and p.dept_id = x.dept_id 
                and p.assess_pd_value = x.assess_pd_value
                and p.status = 1
        )
and status = 0
group by par_year,dept_id,assess_pd_value,user_flag,user_id
;



insert into tmp_score_assess_kpm_user(user_id,task_cnt)
select 
    user_id
    ,count(1) task_cnt
from tmp_score_assess_kpm_dtl
group by user_id
;



-- 需要更新数据的所有用户，包含已打分和未打分的用户
drop TEMPORARY TABLE if exists `tmp_score_assess_kpm_update_user`;
CREATE TEMPORARY TABLE `tmp_score_assess_kpm_update_user` (
  `user_id` bigint NOT NULL COMMENT '用户id',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '临时表-需要更新数据的所有用户'
;

if @user_id = 0 then
    insert into tmp_score_assess_kpm_update_user(user_id)
    select distinct p.user_id from t_assess_dept_progress_scorer p
    where p.par_year = @par_year 
    and p.dept_id = @dept_id 
    and p.assess_pd_value = @assess_pd_value
    ;
else 
    insert into tmp_score_assess_kpm_update_user(user_id)
    select distinct p.user_id from t_assess_dept_progress_scorer p
    where p.par_year = @par_year 
    and p.dept_id = @dept_id 
    and p.assess_pd_value = @assess_pd_value
    and p.user_id = @user_id
    ;
end if
;


-- kpm表数据处理
-- 插入操作，插入未打分的状态
insert into t_kpm_task(kpm_code,content,url,dept_id,assignee_user_id,status,relation_id1,relation_id2,creator,create_time,updater,update_time,remark)
select 
     b.kpm_code                                                   as    kpm_code             -- kpm事件编码
    ,b.template_content                                           as    content              -- kpm正文
    ,b.template_url                                               as    url                  -- 跳转链接
    ,d.dept_id                                                    as    dept_id              -- 部门id(所在hr部门)
    ,z.user_id                                                    as    assignee_user_id     -- 告警用户id(提醒人，一般是绩效专员)
    ,1                                                            as    status               -- 状态（1 待处理，2已处理，3 已忽略）
    ,z.user_id                                                    as    relation_id1         -- 关联id1
    ,z.task_cnt                                                   as    relation_id2         -- 关联id2
    ,1                                                            as    creator              -- 创建人
    ,now()                                                        as    create_time          -- 创建时间
    ,1                                                            as    updater              -- 修改人
    ,now()                                                        as    update_time          -- 修改时间
    ,concat(u.nick_name,'(',u.user_name,')'
        ,'有',z.task_cnt, '个待考核和调整项打分事项！')           as    remark               -- 备注
from tmp_score_assess_kpm_user z
inner join t_kpm_define b 
    on b.kpm_code = 'PerformanceScorerTask'
inner join sys_user u 
    on z.user_id = u.user_id
left join sys_dept d 
    on u.dept_id = d.dept_id
where not exists (select 1 from t_kpm_task k where z.user_id = k.assignee_user_id and k.kpm_code = b.kpm_code and k.status in (1,3))
and exists (select 1 from tmp_score_assess_kpm_update_user kuu where kuu.user_id = z.user_id)
;


-- 更新操作
update t_kpm_task a
left join tmp_score_assess_kpm_user b 
on a.assignee_user_id = b.user_id
set a.relation_id2 = ifnull(b.task_cnt,0)
    ,a.status = case when ifnull(b.task_cnt,0) = 0 then 2
                else 1 end
    ,a.remark = case when ifnull(b.task_cnt,0) = 0 then '该用户已完成考核和调整项打分事项！'
                else concat('该用户有',b.task_cnt, '个待考核和调整项打分事项！')
                end
where exists (select 1 from tmp_score_assess_kpm_update_user kuu where kuu.user_id = a.assignee_user_id)
and a.status in (1,3)  -- 状态（1 待处理，2已处理，3 已忽略）
and a.relation_id2 != cast( ifnull(b.task_cnt,0) as UNSIGNED)
;


-- 获得记录数
select 0 into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;