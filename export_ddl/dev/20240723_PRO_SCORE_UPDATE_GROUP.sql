DROP PROCEDURE IF EXISTS PRO_SCORE_UPDATE_GROUP;
CREATE PROCEDURE `PRO_SCORE_UPDATE_GROUP`(IN IN_YEAR_MONTH VARCHAR(6), IN IN_DEPT_ID INT, IN IN_PERIOD INT)
label:BEGIN
/************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_SCORE_UPDATE_GROUP
         功能简述：   考核评价模块更新考核组列表
         参数：       IN_YEAR_MONTH    考核年月
                      IN_DEPT_ID       部门编号(*必须指定)
                      IN_PERIOD        考核周期(1:月,2:半年,3:季度 不支持0)
         注意事项：
                 1. 传入日期的上个月，是考核的月份
                 eg.  今日为：202303
                      考核月份是：202303，也就是结果表中year_months的取值
                 2. 维度
                   1)dept_id(部门) 
                   2)assess_year(年份)
                   3)assess_month(月份)
                   4)assess_pd(考核周期)
                   
                   5)year_months(年月)：等同于【年份+月份】
                   6)assess_pd_value(考核周期值)：等同于【考核周期+月份】
                   
                   故常见搭配的等价组合：
                              a) (1234) dept_id + assess_year + assess_month + assess_pd（本程序未使用）
                              b) (126)  dept_id + assess_year + assess_pd_value(本程序少量使用)
                              c) (145)  dept_id + assess_pd + year_months (本程序主要使用)
                              -- 其他
                              d) (156) dept_id + year_months + assess_pd_value (冗余)
                  3. 用于考核试算模块中，如果没有对应的考核周期列定方案，会置为null
                 
         数据源：
                  t_assess_info_his
         目标源：
                  t_assess_dept_progress
         修改记录;
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2024/01/15                  创建
***************************************************************************/
-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);

-- 设置变量
set V_EVENT_NAME = '考核评价模块更新考核组列表';
set V_RUN_COMMAND = concat('call PRO_SCORE_UPDATE_GROUP('
                            ,IN_YEAR_MONTH,','
                            ,IN_DEPT_ID,','
                            ,IN_PERIOD
                            ,')'
                         );



select now() into V_START_TIME;


set @assess_year_month = IN_YEAR_MONTH; -- 格式: 202302
set @assess_month = cast(right(@assess_year_month,2) as UNSIGNED);   -- 格式：2
set @assess_year = left(@assess_year_month,4);   -- 格式：2023
set @dept_id = IN_DEPT_ID;   -- 格式： 12800
set @assess_pd = IN_PERIOD;  -- 格式： 1
set @assess_pd_value = (select assess_pd_value from t_dict_assess_pd_mqh where assess_pd = @assess_pd and assess_month = @assess_month);




DROP TEMPORARY TABLE if exists `tmp_update_assess_progress_group`;
CREATE TEMPORARY TABLE `tmp_update_assess_progress_group` (
  `par_year` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '考核年份',
  `dept_id` bigint DEFAULT NULL COMMENT '考核部门编码',
  `assess_pd_value` varchar(3) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '考核周期值(M1-M12,Q1..)',
  `assess_group` varchar(1000) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '考核组列表',
  UNIQUE KEY `unique_3` (`par_year`,`dept_id`,`assess_pd_value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='临时表-更新考核部门进度表的考核组列表'
;

insert into tmp_update_assess_progress_group(par_year,dept_id,assess_pd_value,assess_group)
select a.par_year,a.dept_id,a.assess_pd_value,group_concat(distinct a.assess_group_id order by a.assess_group_id separator ';') assess_group
from t_assess_info_his a
where par_year = @assess_year
and dept_id = @dept_id
and assess_pd_value = @assess_pd_value
group by a.par_year,a.dept_id,a.assess_pd_value
;

-- 更新考核组列表
update t_assess_dept_progress a 
left join tmp_update_assess_progress_group b
    on a.par_year = b.par_year
    and a.dept_id = b.dept_id
    and a.assess_pd_value = b.assess_pd_value
set a.assess_group = b.assess_group
where a.par_year = @assess_year
    and a.dept_id = @dept_id
    and a.assess_pd_value = @assess_pd_value
    and a.status = 0 -- 只更新未开启的进度
;



-- 获得记录数
select 0 into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;