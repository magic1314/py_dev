DROP PROCEDURE IF EXISTS PRO_QUERY_PERF_RESULT;
CREATE PROCEDURE `PRO_QUERY_PERF_RESULT`(IN IN_PAR_YEAR CHAR(4), IN IN_QUERY_TYPE INT)
label:BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_QUERY_PERF_RESULT
         功能简述：   关键绩效结果查询
         参数：       IN_PAR_YEAR    年份
                      IN_QUERY_TYPE  查询类型(1:全员查询，2:重点人群查询)
         注意事项：
                      1. *重点人群后台导入表，需要人工手动插入数据
                      2. 员工当前人事部门编码：是当前员工(即工号)的当前部门
         主要源表：
                     t_assess_info_his               考核方案列定表
                     t_annual_assess_date            判定日期表(数据手工补录)
                     t_annual_assess_emp_his         判定时间员工数据
                     t_report_performance_key_imp    重点人群后台导入表(数据手工补录)
                     t_assess_dept_progress          考核进度表
                     t_assess_member_score           人员打分数据
                     t_assess_member_score_dtl       人员打分数据明细
         结果表：
                     t_report_performance_all        全员查询主表
                     t_report_performance_all_dtl    全员查询明细
                     t_report_performance_key        重点人群查询主表
                     t_report_performance_key_dtl    重点人群查询名单
                     t_report_performance_key_imp    重点人群查询后台导入表
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG          2024/01/30                 创建
         MG          2024/01/31                 完善代码逻辑
         MG          2024/02/01                 继续完善代码逻辑
         MG          2024/02/02                 锁定，【考核得分-后台导入】指标数据，特殊处理，需要添加年份
******************************************************************************************************************************************/

-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);
DECLARE V_BATCH_NO varchar(200);
DECLARE V_TOTAL_STEP_NUM int;

-- 设置变量
set V_EVENT_NAME = '关键绩效结果查询';
set V_TOTAL_STEP_NUM = 1;  -- 设置总步骤数
set @remark = ''; -- 步骤日志中的备注，可以每步都定义
select now() into V_START_TIME;


set @par_year = IN_PAR_YEAR;
set @query_type = IN_QUERY_TYPE;  -- 格式： 1


set V_RUN_COMMAND = concat('call PRO_QUERY_PERF_RESULT('
                            ,IN_PAR_YEAR,','
                            ,IN_QUERY_TYPE
                            ,')'
                         );

set V_BATCH_NO = concat(   UNIX_TIMESTAMP(),'-'
                        ,replace(V_RUN_COMMAND,'call ',''),'-'
                        ,V_TOTAL_STEP_NUM
                     );



if @query_type >= 3 then
    select '传入参数【查询类型】不符合要求,目前只支持【0,1,2】';
    LEAVE label; -- 退出存储过程 
end if;




-- 特殊数据：后台导入指标
delete from t_report_performance_sp_index where par_year = @par_year;
insert into t_report_performance_sp_index(par_year,year_months,assess_pd,assess_pd_value,dept_id,user_id,index_code
    ,dept_appraisal_right,assess_score,adjust_score,final_assess_score)
select 
    a.par_year                                 par_year
    ,concat(a.par_year,mqh.assess_month_2str)  year_months
    ,a.assess_pd                               assess_pd
    ,a.assess_pd_value                         assess_pd_value
    ,a.dept_id                                 dept_id
    ,a.member_id                               user_id
    ,b.index_code                              index_code
    ,c.appraisal_right                         dept_appraisal_right   -- 部门考核权重
    ,a.assess_score                            assess_score           -- 人员得分
    ,a.adjust_score                            adjust_score           -- 调整分
    ,a.final_assess_score                      final_assess_score     -- 最终得分
from t_assess_member_score a
left join t_assess_member_score_dtl b
    on a.id = b.assess_member_socre_id
left join t_assess_dept_member_score c
    on a.assess_dept_member_score_id = c.id
left join t_dict_assess_pd_mqh mqh 
    on a.assess_pd_value = mqh.assess_pd_value
where a.par_year = @par_year
    and b.index_code = '1000_001'
;



/**************** 全员查询 ****************/
if @query_type = 0 or @query_type = 1 then
    -- 临时表
    drop table if exists tmp_report_performance_all_user;
    create temporary table tmp_report_performance_all_user(
    `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户ID',
    `hr_dept_id` bigint DEFAULT NULL COMMENT '部门ID',
    `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
    `account_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '状态(0正常 1离退 2废弃 3组织调整)',
    PRIMARY KEY (`user_id`) USING BTREE,
    key idx_user_name(user_name)
    ) comment '全员查询-符合条件用户-临时表'
    ;
    
    -- 临时表-灌数 
    insert into tmp_report_performance_all_user(user_id,hr_dept_id,user_name,account_status)
    select 
        user_id,hr_dept_id,user_name,account_status
    from (
        -- 由于存在离职再入职的情况，所以需要取最后一条
        select 
             user_id 
            ,dept_id as hr_dept_id
            ,user_name 
            ,account_status
            ,row_number() over(partition by user_name order by user_id desc) rn
        from sys_user 
        where del_flag = '0'
        and account_status in ('0','1','2')  -- 含正常及离退
    ) x
    where x.rn = 1
    ;
    
    -- 全员查询-主表
    delete from t_report_performance_all where par_year = @par_year;
    insert into t_report_performance_all(par_year,year_months,assess_pd,assess_pd_value,item_name,dept_id,dept_appraisal_right,hr_dept_id
        ,user_id,user_name,nick_name,object_name,assess_name,progress_status,final_assess_score,remark,creator,create_time,updater,update_time)
    select 
         distinct 
         a.par_year                                    par_year                 -- 考核年份
        ,a.year_months                                 year_months              -- 年月
        ,a.assess_pd                                   assess_pd                -- 考核周期
        ,a.assess_pd_value                             assess_pd_value          -- 考核周期值(M1-M12,Q1..)
        ,concat(a.par_year,a.assess_pd_value)          item_name                -- 考核项目(2023Q1、2024H1)
        ,a.dept_id                                     dept_id                  -- 考核部门编码
        ,a.dept_appraisal_right                        dept_appraisal_right     -- 部门考核权重
        ,tu.hr_dept_id                                 hr_dept_id               -- 员工当前人事部门编码
        ,a.user_id                                     user_id                  -- 人员ID
        ,u.user_name                                   user_name                -- 工号
        ,u.nick_name                                   nick_name                -- 姓名
        ,concat(u.nick_name,'(',u.user_name,')')       object_name              -- 考核对象 张三（000666）
        ,concat(a.par_year,'年',ag.group_name)         assess_name              -- 考核方案名称(员工：年度+考核组名称)
        ,dp_dd.dict_label                              progress_status          -- 考核进度
        ,if(dp.status in (3,5),ms.final_assess_score,null) final_assess_score   -- 本期最终得分
        ,'数据初始化'                                  remark                   -- 备注
        ,1                                             creator                  -- 创建人
        ,now()                                         create_time              -- 创建时间
        ,1                                             updater                  -- 修改人
        ,now()                                         update_time              -- 最近一次修改时间
    from t_assess_info_his a
    left join sys_user u 
        on a.user_id = u.user_id
    inner join tmp_report_performance_all_user tu 
        on u.user_name = tu.user_name
    left join t_appraisal_group ag
        on a.assess_group_id = ag.id
    left join t_assess_dept_progress dp -- 考核进度表
        on  a.par_year        = dp.par_year
        and a.assess_pd_value = dp.assess_pd_value
        and a.dept_id         = dp.dept_id
    left join t_assess_member_score ms 
        on  a.par_year         = ms.par_year 
        and a.assess_pd_value  = ms.assess_pd_value
        and a.dept_id          = ms.dept_id
        and a.user_id          = ms.member_id
    left join sys_dict_data dp_dd
        on dp_dd.dict_type = 'evaluation_status'
        and dp.status = dp_dd.dict_value
    where a.par_year = @par_year
    ;

    -- 全员查询-明细
    delete from t_report_performance_all_dtl where par_year = @par_year;
    insert into t_report_performance_all_dtl(par_year,year_months,dept_id,assess_pd,assess_pd_value
        ,user_id,index_code,index_name_unit,business_type,business_name,assess_model,assess_model_name
        ,index_appraisal_right,current_tgt,complete_info,index_score,assess_score,remark,creator,create_time,updater,update_time)
    select 
         a.par_year                                     par_year                          -- 考核年份
        ,a.year_months                                  year_months                       -- 年月
        ,a.dept_id                                      dept_id                           -- 考核部门编码
        ,a.assess_pd                                    assess_pd                         -- 考核周期
        ,a.assess_pd_value                              assess_pd_value                   -- 考核周期值(M1-M12,Q1..)
        ,a.user_id                                      user_id                           -- 人员ID
        ,a.index_code                                   index_code                        -- 指标代码
        ,concat(ip.index_name,'(',ifnull(ip_dd.dict_label,'无'),')')    index_name_unit   -- 特殊：考核指标(单位)
        ,ip.business_type                               business_type                     -- 指标类别(1-5定量,6定性)
        ,concat(ip_dd2.dict_label,if(ip.business_type!=6,'类',''))      business_name     -- 指标类型
        ,a.assess_model                                 assess_model                      -- 考核模式
        ,assess_dd.dict_label                           assess_model_name                 -- 考核方式
        ,a.appraisal_right                              index_appraisal_right             -- 指标考核权重
        ,if(dp.status in (3,5), if(ip.business_type=6, a.current_tgt, TRIM(TRAILING '.' FROM TRIM(TRAILING '0' FROM a.current_tgt))), null)
                                                       current_tgt                        -- 当期目标值
        ,if(dp.status in (3,5), if(ip.business_type=6, md.complete_info, TRIM(TRAILING '.' FROM TRIM(TRAILING '0' FROM md.complete_info))), null) 
                                                        complete_info                     -- 完成情况
        ,if(dp.status in (3,5), md.index_score, null)   index_score                       -- 指标单项得分/调整分
        ,if(dp.status in (3,5), md.assess_score , null) assess_score                      -- 计入考核得分(指标单项得分*考核权重)/调整项得分
        ,'数据初始化'                                   remark                            -- 备注
        ,1                                              creator                           -- 创建人
        ,now()                                          create_time                       -- 创建时间
        ,1                                              updater                           -- 修改人
        ,now()                                          update_time                       -- 最近一次修改时间
    from t_assess_info_his a
    left join sys_user u 
        on a.user_id = u.user_id
    inner join tmp_report_performance_all_user tu 
        on u.user_name = tu.user_name
    left join t_index_pool ip 
        on a.index_code = ip.index_code
    left join t_assess_dept_progress dp -- 考核进度表
        on  a.par_year        = dp.par_year
        and a.assess_pd_value = dp.assess_pd_value
        and a.dept_id         = dp.dept_id
    left join t_assess_member_score_dtl md -- 员工考核结果指标明细表
        on  a.par_year        = md.par_year 
        and a.assess_pd_value = md.assess_pd_value
        and a.dept_id         = md.dept_id 
        and a.user_id         = md.member_id
        and a.index_code      = md.index_code
    left join sys_dict_data ip_dd
        on ip_dd.dict_type = 'index_money_unit'
        and ip.index_unit = ip_dd.dict_value
    left join sys_dict_data ip_dd2
        on ip_dd2.dict_type = 'index_business_type'
        and ip.business_type = ip_dd2.dict_value
    left join sys_dict_data assess_dd
        on assess_dd.dict_type = 'assess_model'
        and a.assess_model = assess_dd.dict_value
    where a.par_year = @par_year
    ;


    -- 全员查询-明细补充(调整项)
    insert into t_report_performance_all_dtl(par_year,year_months,dept_id,assess_pd,assess_pd_value
        ,user_id,index_code,index_name_unit,business_type,business_name,assess_model,assess_model_name
        ,index_appraisal_right,current_tgt,complete_info,index_score,assess_score,remark,creator,create_time,updater,update_time)
    select 
         distinct
         a.par_year                                     par_year                          -- 考核年份
        ,a.year_months                                  year_months                       -- 年月
        ,a.dept_id                                      dept_id                           -- 考核部门编码
        ,a.assess_pd                                    assess_pd                         -- 考核周期
        ,a.assess_pd_value                              assess_pd_value                   -- 考核周期值(M1-M12,Q1..)
        ,a.user_id                                      user_id                           -- 人员ID
        ,null                                           index_code                        -- 指标代码
        ,'调整项'                                       index_name_unit                   -- 默认为'调整项'
        ,null                                           business_type                     -- 指标类别(1-5定量,6定性)
        ,null                                           business_name                     -- 指标类型
        ,null                                           assess_model                      -- 考核模式
        ,null                                           assess_model_name                 -- 考核方式
        ,null                                           index_appraisal_right             -- 指标考核权重
        ,null                                           current_tgt                       -- 当期目标值
        ,null                                           complete_info                     -- 完成情况
        ,if(dp.status in (3,5),ms.adjust_score,null)    index_score                       -- 指标单项得分/调整分
        ,if(dp.status in (3,5),ms.adjust_score,null)    assess_score                      -- 计入考核得分(指标单项得分*考核权重)/调整项得分
        ,'数据初始化-调整分'                            remark                            -- 备注
        ,1                                              creator                           -- 创建人
        ,now()                                          create_time                       -- 创建时间
        ,1                                              updater                           -- 修改人
        ,now()                                          update_time                       -- 最近一次修改时间
    from t_assess_info_his a
    left join sys_user u 
        on a.user_id = u.user_id
    inner join tmp_report_performance_all_user tu 
        on u.user_name = tu.user_name
    left join t_assess_dept_progress dp -- 考核进度表
        on  a.par_year        = dp.par_year
        and a.assess_pd_value = dp.assess_pd_value
        and a.dept_id         = dp.dept_id
    left join t_assess_member_score ms -- 调整分
        on  a.par_year        = ms.par_year 
        and a.assess_pd_value = ms.assess_pd_value
        and a.dept_id         = ms.dept_id 
        and a.user_id         = ms.member_id
    where a.par_year = @par_year
    ;

    
    
    /*
    注：数据已导入数据为准，已列定的数据删除
    */
    delete a from t_report_performance_all a 
    inner join t_report_performance_sp_index b 
    on a.par_year = b.par_year 
    and a.assess_pd_value = b.assess_pd_value
    and a.dept_id = b.dept_id 
    and a.user_id = b.user_id 
    where a.par_year = @par_year
    ;
    
    delete a from t_report_performance_all_dtl a 
    inner join t_report_performance_sp_index b 
    on a.par_year = b.par_year 
    and a.assess_pd_value = b.assess_pd_value
    and a.dept_id = b.dept_id 
    and a.user_id = b.user_id 
    where a.par_year = @par_year
    ;
    
    -- 全员查询-特殊情况-主表(考核得分-后台导入)
    insert into t_report_performance_all(par_year,year_months,assess_pd,assess_pd_value,item_name,dept_id,dept_appraisal_right,hr_dept_id
        ,user_id,user_name,nick_name,object_name,assess_name,progress_status,final_assess_score,remark,creator,create_time,updater,update_time)
    select 
         a.par_year                                    par_year                 -- 考核年份
        ,a.year_months                                 year_months              -- 年月
        ,a.assess_pd                                   assess_pd                -- 考核周期
        ,a.assess_pd_value                             assess_pd_value          -- 考核周期值(M1-M12,Q1..)
        ,concat(a.par_year,a.assess_pd_value)          item_name                -- 考核项目(2023Q1、2024H1)
        ,a.dept_id                                     dept_id                  -- 考核部门编码
        ,a.dept_appraisal_right                        dept_appraisal_right     -- 部门考核权重
        ,tu.hr_dept_id                                 hr_dept_id               -- 员工当前人事部门编码
        ,a.user_id                                     user_id                  -- 人员ID
        ,u.user_name                                   user_name                -- 工号
        ,u.nick_name                                   nick_name                -- 姓名
        ,concat(u.nick_name,'(',u.user_name,')')       object_name              -- 考核对象 张三（000666）
        ,null                                          assess_name              -- 考核方案名称(默认为null)
        ,dp_dd.dict_label                              progress_status          -- 考核进度
        ,a.final_assess_score                          final_assess_score       -- 本期最终得分
        ,'数据初始化-后台导入'                         remark                   -- 备注
        ,1                                             creator                  -- 创建人
        ,now()                                         create_time              -- 创建时间
        ,1                                             updater                  -- 修改人
        ,now()                                         update_time              -- 最近一次修改时间
    from t_report_performance_sp_index a
    left join sys_user u 
        on a.user_id = u.user_id
    inner join tmp_report_performance_all_user tu 
        on u.user_name = tu.user_name
    left join t_assess_dept_progress dp -- 考核进度表
        on  a.par_year        = dp.par_year
        and a.assess_pd_value = dp.assess_pd_value
        and a.dept_id         = dp.dept_id
    left join sys_dict_data dp_dd
        on dp_dd.dict_type = 'evaluation_status'
        and dp.status = dp_dd.dict_value
    where a.par_year = @par_year
    ;

    -- 全员查询-特殊情况-明细(考核得分-后台导入)
    insert into t_report_performance_all_dtl(par_year,year_months,dept_id,assess_pd,assess_pd_value
        ,user_id,index_code,index_name_unit,business_type,business_name,assess_model,assess_model_name
        ,index_appraisal_right,current_tgt,complete_info,index_score,assess_score,remark,creator,create_time,updater,update_time)
    select 
         distinct
         a.par_year                                     par_year                          -- 考核年份
        ,a.year_months                                  year_months                       -- 年月
        ,a.dept_id                                      dept_id                           -- 考核部门编码
        ,a.assess_pd                                    assess_pd                         -- 考核周期
        ,a.assess_pd_value                              assess_pd_value                   -- 考核周期值(M1-M12,Q1..)
        ,a.user_id                                      user_id                           -- 人员ID
        ,a.index_code                                   index_code                        -- 指标代码
        ,ip.index_name                                  index_name_unit                   -- 指标名称
        ,null                                           business_type                     -- 指标类别(1-5定量,6定性)
        ,null                                           business_name                     -- 指标类型
        ,null                                           assess_model                      -- 考核模式
        ,null                                           assess_model_name                 -- 考核方式
        ,100                                            index_appraisal_right             -- 指标考核权重(默认为100)
        ,null                                           current_tgt                       -- 当期目标值
        ,null                                           complete_info                     -- 完成情况
        ,a.assess_score                                 index_score                       -- 指标单项得分
        ,a.assess_score                                 assess_score                      -- 与【指标单项得分】一样
        ,'数据初始化-后台导入'                          remark                            -- 备注
        ,1                                              creator                           -- 创建人
        ,now()                                          create_time                       -- 创建时间
        ,1                                              updater                           -- 修改人
        ,now()                                          update_time                       -- 最近一次修改时间
    from t_report_performance_sp_index a
    left join t_index_pool ip 
        on a.index_code = ip.index_code
    where a.par_year = @par_year
    ;

    -- 更新关联id
    update t_report_performance_all a 
    inner join t_report_performance_all_dtl b 
        on a.par_year = b.par_year 
        and a.assess_pd_value = b.assess_pd_value
        and a.dept_id = b.dept_id 
        and a.user_id = b.user_id
    set b.report_performance_all_id = a.id
    where a.par_year = @par_year
    ;
end if
;


/**************** 重点人群查询 ****************/
if @query_type = 0 or @query_type = 2 then
    -- 临时表
    drop table if exists tmp_report_performance_key_user;
    CREATE TEMPORARY TABLE `tmp_report_performance_key_user` (
      `par_year` char(4) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '年份',
      `assess_date` date DEFAULT NULL COMMENT '数据判定时间',
      `user_id` bigint NOT NULL AUTO_INCREMENT COMMENT '用户ID',
      `hr_dept_id` bigint DEFAULT NULL COMMENT '部门ID',
      `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
      `account_status` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '0' COMMENT '状态(0正常 1离退 2废弃 3组织调整)',
      PRIMARY KEY (`user_id`) USING BTREE,
      KEY `idx_2` (par_year,`user_name`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='重点人群查询-符合条件用户-临时表'
    ;
    
    -- 均是当前结存的情况
    insert into tmp_report_performance_key_user(par_year,assess_date,user_id,hr_dept_id,user_name,account_status)
    select par_year,assess_date,user_id,hr_dept_id,user_name,account_status
    from (
        select 
            a.par_year
            ,b.assess_date
            ,a.user_id
            ,a.dept_id as hr_dept_id
            ,a.user_name 
            ,a.account_status
            ,row_number() over(partition by a.par_year,a.user_name order by a.user_id desc) rn
        from t_annual_assess_emp_his a 
        inner join t_annual_assess_date b 
            on a.par_year = b.par_year
        inner join t_report_performance_key_imp ki 
            on a.par_year = ki.par_year 
            and a.user_name = ki.user_name
        where a.par_year = @par_year
    ) x
    where x.rn = 1
    ;



    -- 重点人群查询-主表
    /* 
        1、该页面展示的数据是：某个年度内与该人员有关的所有考核方案的最后一次记录（如果考核方案没有启动打分，则完成值、得分内容为空）。
        即：2023年度张三有A.B两个考核方案，则生成张三的A.B两个考核方案的最后一次考核结果。
    */
    drop table if exists tmp_report_performance_key;
    create TEMPORARY table tmp_report_performance_key like t_report_performance_key;
    
    
    insert into tmp_report_performance_key(par_year,year_months,assess_pd,assess_pd_value,item_name,dept_id,dept_appraisal_right,hr_dept_id
        ,user_id,user_name,nick_name,object_name,assess_name,progress_status,final_assess_score,remark,creator,create_time,updater,update_time)
    select 
        par_year,year_months,assess_pd,assess_pd_value,item_name,dept_id,dept_appraisal_right,hr_dept_id
        ,user_id,user_name,nick_name,object_name
        ,concat(par_year,'年',nick_name,'考核方案',DENSE_RANK() over(partition by user_name order by assess_dtl_id)) assess_name
        ,progress_status,final_assess_score
        ,'数据初始化'                                  remark                   -- 备注
        ,1                                             creator                  -- 创建人
        ,now()                                         create_time              -- 创建时间
        ,1                                             updater                  -- 修改人
        ,now()                                         update_time              -- 最近一次修改时间
    from (
        select 
             distinct 
             a.assess_dtl_id                               assess_dtl_id            -- 考核方案id
            ,a.par_year                                    par_year                 -- 考核年份
            ,a.year_months                                 year_months              -- 年月
            ,a.assess_pd                                   assess_pd                -- 考核周期
            ,a.assess_pd_value                             assess_pd_value          -- 考核周期值(M1-M12,Q1..)
            ,concat(a.par_year,a.assess_pd_value)          item_name                -- 考核项目(2023Q1、2024H1)
            ,a.dept_id                                     dept_id                  -- 考核部门编码
            ,a.dept_appraisal_right                        dept_appraisal_right     -- 部门考核权重
            ,tu.hr_dept_id                                 hr_dept_id               -- 判定时间人事部门编码
            ,a.user_id                                     user_id                  -- 人员ID
            ,u.user_name                                   user_name                -- 工号
            ,u.nick_name                                   nick_name                -- 姓名
            ,concat(u.nick_name,'(',u.user_name,')')       object_name              -- 考核对象 张三（000666）
            ,concat(a.par_year,'年',ag.group_name)         assess_name              -- 考核方案名称(员工：年度+考核组名称) ???
            ,dp_dd.dict_label                              progress_status          -- 考核进度
            ,if(dp.status in (3,5),ms.final_assess_score,null) final_assess_score   -- 本期最终得分
        from t_assess_info_his a
        left join sys_user u 
            on a.user_id = u.user_id
        inner join tmp_report_performance_key_user tu 
            on a.par_year = tu.par_year 
            and u.user_name = tu.user_name
        left join t_appraisal_group ag
            on a.assess_group_id = ag.id
        left join t_assess_dept_progress dp -- 考核进度表
            on  a.par_year        = dp.par_year
            and a.assess_pd_value = dp.assess_pd_value
            and a.dept_id         = dp.dept_id
        left join t_assess_member_score ms 
            on  a.par_year         = ms.par_year 
            and a.assess_pd_value  = ms.assess_pd_value
            and a.dept_id          = ms.dept_id
            and a.user_id          = ms.member_id
        left join sys_dict_data dp_dd
            on dp_dd.dict_type = 'evaluation_status'
            and dp.status = dp_dd.dict_value
        where a.par_year = @par_year
    ) x
    ;
    
    
    -- 保留当年最后一条考核方案的记录
    truncate table t_report_performance_key;
    insert into t_report_performance_key(par_year,year_months,assess_pd,assess_pd_value,item_name,dept_id,dept_appraisal_right,hr_dept_id
        ,user_id,user_name,nick_name,object_name,assess_name,progress_status,final_assess_score,remark,creator,create_time,updater,update_time)
    select 
        x.par_year,x.year_months,x.assess_pd,x.assess_pd_value,x.item_name,x.dept_id,x.dept_appraisal_right,x.hr_dept_id
        ,x.user_id,x.user_name,x.nick_name,x.object_name,x.assess_name
        ,x.progress_status,x.final_assess_score,x.remark,x.creator,x.create_time,x.updater,x.update_time
    from (
        select 
            a.par_year,a.year_months,a.assess_pd,a.assess_pd_value,a.item_name,a.dept_id,a.dept_appraisal_right,a.hr_dept_id
            ,a.user_id,a.user_name,a.nick_name,a.object_name,a.assess_name
            ,a.progress_status,a.final_assess_score,a.remark,a.creator,a.create_time,a.updater,a.update_time 
            ,mqh.assess_month
            ,ROW_NUMBER() over(partition by a.par_year,a.dept_id,a.user_id,a.assess_name order by mqh.assess_month desc) rn
        from tmp_report_performance_key a
        left join t_dict_assess_pd_mqh mqh 
            on a.assess_pd_value = mqh.assess_pd_value
        where a.par_year = @par_year
    ) x
    where x.rn = 1
    ;
    
    
    

    -- 重点人群查询-明细表
    delete from t_report_performance_key_dtl where par_year = @par_year;
    insert into t_report_performance_key_dtl(par_year,year_months,dept_id,assess_pd,assess_pd_value
        ,user_id,index_code,index_name_unit,business_type,business_name,index_big_class,assess_model,assess_model_name
        ,index_appraisal_right,current_tgt,complete_info,index_score,assess_score,remark,creator,create_time,updater,update_time)
    select 
         a.par_year                                     par_year                          -- 考核年份
        ,a.year_months                                  year_months                       -- 年月
        ,a.dept_id                                      dept_id                           -- 考核部门编码
        ,a.assess_pd                                    assess_pd                         -- 考核周期
        ,a.assess_pd_value                              assess_pd_value                   -- 考核周期值(M1-M12,Q1..)
        ,a.user_id                                      user_id                           -- 人员ID
        ,a.index_code                                   index_code                        -- 指标代码
        ,concat(ip.index_name,'(',ifnull(ip_dd.dict_label,'无'),')')    index_name_unit   -- 特殊：考核指标(单位)
        ,ip.business_type                               business_type                     -- 指标类别(1-5定量,6定性)
        ,concat(ip_dd2.dict_label,if(ip.business_type!=6,'类',''))      business_name     -- 指标类型
        ,if(ip.business_type=6,'定性指标','业绩指标')   index_big_class                   -- 指标大类(业绩指标/定性指标)
        ,a.assess_model                                 assess_model                      -- 考核模式
        ,assess_dd.dict_label                           assess_model_name                 -- 考核方式
        ,a.appraisal_right                              index_appraisal_right             -- 指标考核权重
        ,if(dp.status in (3,5), if(ip.business_type=6, a.current_tgt, TRIM(TRAILING '.' FROM TRIM(TRAILING '0' FROM a.current_tgt))), null)
                                                       current_tgt                        -- 当期目标值
        ,if(dp.status in (3,5), if(ip.business_type=6, md.complete_info, TRIM(TRAILING '.' FROM TRIM(TRAILING '0' FROM md.complete_info))), null) 
                                                        complete_info                     -- 完成情况
        ,if(dp.status in (3,5), md.index_score, null)   index_score                       -- 指标单项得分/调整分
        ,if(dp.status in (3,5), md.assess_score , null) assess_score                      -- 计入考核得分(指标单项得分*考核权重)/调整项得分
        ,'数据初始化'                                   remark                            -- 备注
        ,1                                              creator                           -- 创建人
        ,now()                                          create_time                       -- 创建时间
        ,1                                              updater                           -- 修改人
        ,now()                                          update_time                       -- 最近一次修改时间
    from t_assess_info_his a
    left join sys_user u 
        on a.user_id = u.user_id
    inner join t_report_performance_key_imp ki 
        on a.par_year = ki.par_year 
        and u.user_name = ki.user_name
    left join t_index_pool ip 
        on a.index_code = ip.index_code
    left join t_assess_dept_progress dp -- 考核进度表
        on  a.par_year        = dp.par_year
        and a.assess_pd_value = dp.assess_pd_value
        and a.dept_id         = dp.dept_id
    left join t_assess_member_score_dtl md -- 员工考核结果指标明细表
        on  a.par_year        = md.par_year 
        and a.assess_pd_value = md.assess_pd_value
        and a.dept_id         = md.dept_id 
        and a.user_id         = md.member_id
        and a.index_code      = md.index_code
    left join sys_dict_data ip_dd
        on ip_dd.dict_type = 'index_money_unit'
        and ip.index_unit = ip_dd.dict_value
    left join sys_dict_data ip_dd2
        on ip_dd2.dict_type = 'index_business_type'
        and ip.business_type = ip_dd2.dict_value
    left join sys_dict_data assess_dd
        on assess_dd.dict_type = 'assess_model'
        and a.assess_model = assess_dd.dict_value
    where a.par_year = @par_year
    ;


    -- 重点人群查询-明细补充(调整项)
    insert into t_report_performance_key_dtl(par_year,year_months,dept_id,assess_pd,assess_pd_value
        ,user_id,index_code,index_name_unit,business_type,business_name,index_big_class,assess_model,assess_model_name
        ,index_appraisal_right,current_tgt,complete_info,index_score,assess_score,remark,creator,create_time,updater,update_time)
    select 
         distinct
         a.par_year                                     par_year                          -- 考核年份
        ,a.year_months                                  year_months                       -- 年月
        ,a.dept_id                                      dept_id                           -- 考核部门编码
        ,a.assess_pd                                    assess_pd                         -- 考核周期
        ,a.assess_pd_value                              assess_pd_value                   -- 考核周期值(M1-M12,Q1..)
        ,a.user_id                                      user_id                           -- 人员ID
        ,null                                           index_code                        -- 指标代码
        ,'调整项'                                       index_name_unit                   -- 默认为'调整项'
        ,null                                           business_type                     -- 指标类别(1-5定量,6定性)
        ,null                                           business_name                     -- 指标类型
        ,null                                           index_big_class                   -- 指标大类(业绩指标/定性指标)
        ,null                                           assess_model                      -- 考核模式
        ,null                                           assess_model_name                 -- 考核方式
        ,null                                           index_appraisal_right             -- 指标考核权重
        ,null                                           current_tgt                       -- 当期目标值
        ,null                                           complete_info                     -- 完成情况
        ,if(dp.status in (3,5),ms.adjust_score,null)    index_score                       -- 指标单项得分/调整分
        ,if(dp.status in (3,5),ms.adjust_score,null)    assess_score                      -- 计入考核得分(指标单项得分*考核权重)/调整项得分
        ,'数据初始化-调整分'                            remark                            -- 备注
        ,1                                              creator                           -- 创建人
        ,now()                                          create_time                       -- 创建时间
        ,1                                              updater                           -- 修改人
        ,now()                                          update_time                       -- 最近一次修改时间
    from t_assess_info_his a
    left join sys_user u 
        on a.user_id = u.user_id
    inner join t_report_performance_key_imp ki 
        on a.par_year = ki.par_year 
        and u.user_name = ki.user_name
    left join t_assess_dept_progress dp -- 考核进度表
        on  a.par_year        = dp.par_year
        and a.assess_pd_value = dp.assess_pd_value
        and a.dept_id         = dp.dept_id
    left join t_assess_member_score ms -- 调整分
        on  a.par_year        = ms.par_year 
        and a.assess_pd_value = ms.assess_pd_value
        and a.dept_id         = ms.dept_id 
        and a.user_id         = ms.member_id
    where a.par_year = @par_year
    ;


    
    
    /* 特殊处理
    注：数据已导入数据为准，已列定的数据删除
    */
    delete a from t_report_performance_key a 
    inner join t_report_performance_sp_index b 
        on a.par_year = b.par_year 
        and a.assess_pd_value = b.assess_pd_value
        and a.dept_id = b.dept_id 
        and a.user_id = b.user_id 
    where a.par_year = @par_year
    ;
    
    delete a from t_report_performance_key_dtl a 
    inner join t_report_performance_sp_index b 
        on a.par_year = b.par_year 
        and a.assess_pd_value = b.assess_pd_value
        and a.dept_id = b.dept_id 
        and a.user_id = b.user_id 
    where a.par_year = @par_year
    ;
    
    -- 全员查询-特殊情况-主表(考核得分-后台导入)
    insert into t_report_performance_key(par_year,year_months,assess_pd,assess_pd_value,item_name,dept_id,dept_appraisal_right,hr_dept_id
        ,user_id,user_name,nick_name,object_name,assess_name,progress_status,final_assess_score,remark,creator,create_time,updater,update_time)
    select 
         a.par_year                                    par_year                 -- 考核年份
        ,a.year_months                                 year_months              -- 年月
        ,a.assess_pd                                   assess_pd                -- 考核周期
        ,a.assess_pd_value                             assess_pd_value          -- 考核周期值(M1-M12,Q1..)
        ,concat(a.par_year,a.assess_pd_value)          item_name                -- 考核项目(2023Q1、2024H1)
        ,a.dept_id                                     dept_id                  -- 考核部门编码
        ,a.dept_appraisal_right                        dept_appraisal_right     -- 部门考核权重
        ,tu.hr_dept_id                                 hr_dept_id               -- 员工当前人事部门编码
        ,a.user_id                                     user_id                  -- 人员ID
        ,u.user_name                                   user_name                -- 工号
        ,u.nick_name                                   nick_name                -- 姓名
        ,concat(u.nick_name,'(',u.user_name,')')       object_name              -- 考核对象 张三（000666）
        ,null                                          assess_name              -- 考核方案名称(默认为null)
        ,dp_dd.dict_label                              progress_status          -- 考核进度
        ,a.final_assess_score                          final_assess_score       -- 本期最终得分
        ,'数据初始化-后台导入'                         remark                   -- 备注
        ,1                                             creator                  -- 创建人
        ,now()                                         create_time              -- 创建时间
        ,1                                             updater                  -- 修改人
        ,now()                                         update_time              -- 最近一次修改时间
    from t_report_performance_sp_index a
    left join sys_user u 
        on a.user_id = u.user_id
    inner join tmp_report_performance_key_user tu 
        on a.par_year = tu.par_year 
        and u.user_name = tu.user_name
    left join t_assess_dept_progress dp -- 考核进度表
        on  a.par_year        = dp.par_year
        and a.assess_pd_value = dp.assess_pd_value
        and a.dept_id         = dp.dept_id
    left join sys_dict_data dp_dd
        on dp_dd.dict_type = 'evaluation_status'
        and dp.status = dp_dd.dict_value
    where a.par_year = @par_year
    ;

    -- 全员查询-特殊情况-明细(考核得分-后台导入)
    insert into t_report_performance_all_dtl(par_year,year_months,dept_id,assess_pd,assess_pd_value
        ,user_id,index_code,index_name_unit,business_type,business_name,assess_model,assess_model_name
        ,index_appraisal_right,current_tgt,complete_info,index_score,assess_score,remark,creator,create_time,updater,update_time)
    select 
         distinct
         a.par_year                                     par_year                          -- 考核年份
        ,a.year_months                                  year_months                       -- 年月
        ,a.dept_id                                      dept_id                           -- 考核部门编码
        ,a.assess_pd                                    assess_pd                         -- 考核周期
        ,a.assess_pd_value                              assess_pd_value                   -- 考核周期值(M1-M12,Q1..)
        ,a.user_id                                      user_id                           -- 人员ID
        ,a.index_code                                   index_code                        -- 指标代码
        ,ip.index_name                                  index_name_unit                   -- 指标名称
        ,null                                           business_type                     -- 指标类别(1-5定量,6定性)
        ,null                                           business_name                     -- 指标类型
        ,null                                           assess_model                      -- 考核模式
        ,null                                           assess_model_name                 -- 考核方式
        ,100                                            index_appraisal_right             -- 指标考核权重(默认为100)
        ,null                                           current_tgt                       -- 当期目标值
        ,null                                           complete_info                     -- 完成情况
        ,a.assess_score                                 index_score                       -- 指标单项得分
        ,a.assess_score                                 assess_score                      -- 与【指标单项得分】一样
        ,'数据初始化-后台导入'                          remark                            -- 备注
        ,1                                              creator                           -- 创建人
        ,now()                                          create_time                       -- 创建时间
        ,1                                              updater                           -- 修改人
        ,now()                                          update_time                       -- 最近一次修改时间
    from t_report_performance_sp_index a
    left join sys_user u 
        on a.user_id = u.user_id
    inner join tmp_report_performance_key_user tu 
        on a.par_year = tu.par_year 
        and u.user_name = tu.user_name
    left join t_index_pool ip 
        on a.index_code = ip.index_code
    where a.par_year = @par_year
    ;
    
    -- 更新关联id
    update t_report_performance_key a 
    inner join t_report_performance_key_dtl b 
        on a.par_year = b.par_year 
        and a.assess_pd_value = b.assess_pd_value
        and a.dept_id = b.dept_id 
        and a.user_id = b.user_id
    set b.report_performance_key_id = a.id
    where a.par_year = @par_year
    ;
    
    -- 删除无效的记录，由于部分考核记录不保留
    delete from t_report_performance_key_dtl where par_year = @par_year and report_performance_key_id is null;
end if
;

/*
-- 后台导入数据
-- excel存在的列：部门名称 工号 人员姓名 职务 干部类型 需确认事项
delete from t_report_performance_key_imp where par_year = '2023';
insert into t_report_performance_key_imp(par_year,dept_name,user_name,nick_name,emp_post,Cadre_type,remark,creator,create_time,updater,update_time)
select 
     '2023'          par_year       -- 考核年份
    ,a.部门名称          dept_name       -- 部门名称
    ,a.工号          user_name       -- 工号
    ,a.人员姓名          nick_name       -- 人员姓名
    ,a.职务          emp_post       -- 职务
    ,a.干部类型          Cadre_type       -- 干部类型
    ,'数据初始化' remark      -- 备注
    ,1            creator     -- 创建人
    ,now()        create_time -- 创建时间
    ,1            updater     -- 修改人
    ,now()        update_time -- 最近一次修改时间
from tmp_20240131_1 a
;


*/



-- 获得记录数
select 0 cnt into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;