DROP PROCEDURE IF EXISTS PRO_YJGL_COMPLETED_VALUE_copy1;
CREATE PROCEDURE `PRO_YJGL_COMPLETED_VALUE_copy1`(IN IN_YEAR_MONTH VARCHAR(6), IN IN_DEPT_ID INT, IN IN_PERIOD INT)
label:BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_YJGL_COMPLETED_VALUE
         功能简述：   指标完成值表
         参数：       IN_YEAR_MONTH    考核年月
                      IN_DEPT_ID       部门id，必须指定一个部门
                      IN_PERIOD        考核周期(1:月，2：季，3：半年)
         注意事项：
                 1. 传入日期的上个月，是考核的月份
                 eg.  今日为：202303
                      考核月份是：202303，也就是结果表中year_months的取值
                 2. 由于叫法的不方便，当月、上月，当期之类实际都是指上个月或上一个周期，因为当月实际没啥意义
         数据源：
                 1、 t_emp_perf_cmplt_val_src             员工业绩完成值表_源系统
                 2、 t_index_pool                         单项指标表
                 3、 t_appraisal_member/t_appraisal_dept  考核权表
                 4、 t_assess_info_his                    考核方案归档表
         结果表：
                 t_emp_perf_cmplt_val                     指标完成值表
         后续优化：
                 1. 需要添加部门id，考核周期的参数        已完成
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2023/03/30                  创建
         MG         2023/04/03                  按照12种情况计算完成值
         MG         2023/04/18                  参数传入年月
         MG         2023/04/19                  传入部门编号、考核周期
         MG         2023/04/21                  季度、半年度考核周期字段名bug修复
         MG         2023/05/05                  考核权限制当前时间
         MG         2023/05/08                  供数周期加上季度、半年度
         MG         2023/05/17                  完成数据转换的核对和代码的编写
         MG         2023/05/29                  计算时，限制考核权部门
                                                最后关联考核方案列定时，限制dept_id
                                                考核权时间限制
         MG         2023/06/02                  多部门考核时，实际完成值不需要分摊
         MG         2023/06/05                  原指标完成值记录表不限定部门外，其他表均做限制
         MG         2023/06/08                  IN_PERIOD调整为不支持参数0；考核权限制逻辑调整；添加虚拟用户的考核信息
                                                dept_id条件bug修复；记录表改为临时表；累计供数不支持当期考核
         MG         2023/06/09                  限制指标池中非最新状态的记录
         MG         2023/06/26                  修改临时表index_value的字段长度
         MG         2023/07/11                  新增日志步骤表的逻辑
******************************************************************************************************************************************/

-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);
DECLARE V_BATCH_NO varchar(200);
DECLARE V_TOTAL_STEP_NUM int;

-- 设置变量
set V_EVENT_NAME = '指标完成值表';
set V_TOTAL_STEP_NUM = 5;  -- 设置总步骤数
set @remark = ''; -- 步骤日志中的备注，可以每步都定义
select now() into V_START_TIME;


-- 解决字符集问题：Illegal mix of collations (utf8mb4_0900_ai_ci,IMPLICIT) and (utf8mb4_general_ci,IMPLICIT) for operation
set collation_connection = utf8mb4_general_ci;

set @assess_year_month = IN_YEAR_MONTH; -- 格式: 202302
set @assess_month_2str = right(@assess_year_month,2);   -- 格式：02
set @assess_month = cast(right(@assess_year_month,2) as UNSIGNED);   -- 格式：2
set @assess_year = left(@assess_year_month,4);   -- 格式：2023
set @dept_id = IN_DEPT_ID;   -- 格式： 12800
set @assess_pd = IN_PERIOD;  -- 格式： 1


set V_RUN_COMMAND = concat('call PRO_YJGL_COMPLETED_VALUE('
                            ,IN_YEAR_MONTH,','
                            ,IN_DEPT_ID,','
                            ,IN_PERIOD
                            ,')'
                         );

set V_BATCH_NO = concat(   UNIX_TIMESTAMP(),'-'
                        ,replace(V_RUN_COMMAND,'call ',''),'-'
                        ,V_TOTAL_STEP_NUM
                     );





-- 基础检查
if @assess_month >= 13 then
    select '传入参数【考核年月】不符合要求';
    LEAVE label; -- 退出存储过程 
end if;



if @assess_pd >= 4 or @assess_pd = 0 then
    select '传入参数【考核周期】不符合要求,目前只支持【1,2,3】';
    LEAVE label; -- 退出存储过程 
end if;


-- 写步骤日志
set @step_info = '1.参数定义&基础检查:正常';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);



-- 写步骤日志
set @step_info = '2.完成值记录表:业绩完成值表添加指标的基础字段';
set @remark = '实体和虚拟用户都考核';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);
set @remark = '';

-- 刷新当期月的值,保留所有历史月份的数据
-- 原指标完成值记录表
drop TEMPORARY TABLE if exists tmp_cmplt_val_src_record;
CREATE TEMPORARY TABLE `tmp_cmplt_val_src_record` (
  `id` int NOT NULL AUTO_INCREMENT,
  `year_months` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '年月',
  `assess_year` varchar(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '考核年份',
  `pd_quarter_ym` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '季度周期值下的年月',
  `pd_half_year_ym` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '半年度周期值下的年月',
  `supply_cycle` tinyint DEFAULT NULL COMMENT '供数周期',
  `supply_frequency` tinyint DEFAULT NULL COMMENT '供数模式',
  `emp_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '工号',
  `user_id` bigint DEFAULT '0' COMMENT '用户ID',
  `dept_id` bigint DEFAULT NULL COMMENT '部门ID',
  `ident_id` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '证件编码',
  `index_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '指标代码',
  `index_source` tinyint DEFAULT NULL COMMENT '指标来源',
  `source_index_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '指标代码',
  `index_value` decimal(26,6) DEFAULT NULL COMMENT '指标值',
  `remark1` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备份1',
  `remark2` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备份2',
  `etl_create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'etl创建时间',
  `etl_update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'etl更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_year_months` (`year_months`),
  KEY `idx_assess_year` (`assess_year`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_index_code` (`index_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci
;

-- 实体用户
insert into tmp_cmplt_val_src_record(year_months,assess_year,pd_quarter_ym,pd_half_year_ym,emp_code,user_id,dept_id,ident_id,index_code,index_source,source_index_code,index_value,supply_cycle,supply_frequency)
select 
    year_months              year_months          -- 年月
    ,left(year_months,4)     assess_year          -- 考核年份
    ,case when cast(right(year_months,2) as unsigned) between 1  and 3 then  concat(left(year_months,4),'03')
          when cast(right(year_months,2) as unsigned) between 4  and 6 then  concat(left(year_months,4),'06')
          when cast(right(year_months,2) as unsigned) between 7  and 9 then  concat(left(year_months,4),'09')
          when cast(right(year_months,2) as unsigned) between 10 and 12 then concat(left(year_months,4),'12')
          else null end                       pd_quarter_ym
    ,case when cast(right(year_months,2) as unsigned) between 1 and 6  then concat(left(year_months,4),'06')
          when cast(right(year_months,2) as unsigned) between 7 and 12 then concat(left(year_months,4),'12')
          else null end                       pd_half_year_ym
    ,a.emp_code                               emp_code                   -- 工号
    ,u.user_id                                user_id                    -- 人员ID
    ,u.dept_id                                dept_id                    -- 部门(人事所在部门)   dept_id_src                     -- 原系统员工所属部门
    ,a.ident_id                               ident_id                   -- 证件编码
    ,b.index_code                             index_code                 -- 指标代码
    ,b.index_source                           index_source               -- 指标来源(1:系统，2：手工)
    ,a.index_code                             source_index_code          -- 源系统指标代码
    ,a.index_value                            index_value                -- 指标值
    ,b.supply_cycle                           supply_cycle               -- 供数周期
    ,b.supply_frequency                       supply_frequency           -- 供数模式
from t_emp_perf_cmplt_val_src a -- 员工业绩完成值表_源系统
left join t_index_pool b -- 单项指标(已录入系统，算是原始)
    on a.index_code = b.related_index_code  -- 原始的指标代码关联
    and b.deleted = 0 
    and b.status = 5 --  index_status (1:未通过|2:未启用|3:审批中|4:待提交|5:启用)
inner join t_index_pool_input c 
    on (b.related_index_code = c.index_code and c.latest_status = 0)
       -- or b.related_index_code is null   -- 正常情况下，值为null是不存在的
inner join sys_user u 
    on u.account_flag = 1 -- 实体账户
    and a.emp_code = u.user_name
    and u.account_status = 0
where a.year_months = @assess_year_month
;


-- 虚拟用户
insert into tmp_cmplt_val_src_record(year_months,assess_year,pd_quarter_ym,pd_half_year_ym,emp_code,user_id,dept_id,ident_id
    ,index_code,index_source,source_index_code,index_value,supply_cycle,supply_frequency,remark1)
select 
    year_months              year_months          -- 年月
    ,left(year_months,4)     assess_year          -- 考核年份
    ,case when cast(right(year_months,2) as unsigned) between 1  and 3 then  concat(left(year_months,4),'03')
          when cast(right(year_months,2) as unsigned) between 4  and 6 then  concat(left(year_months,4),'06')
          when cast(right(year_months,2) as unsigned) between 7  and 9 then  concat(left(year_months,4),'09')
          when cast(right(year_months,2) as unsigned) between 10 and 12 then concat(left(year_months,4),'12')
          else null end                       pd_quarter_ym
    ,case when cast(right(year_months,2) as unsigned) between 1 and 6  then concat(left(year_months,4),'06')
          when cast(right(year_months,2) as unsigned) between 7 and 12 then concat(left(year_months,4),'12')
          else null end                       pd_half_year_ym
    ,a.emp_code                               emp_code                   -- 工号
    ,u.user_id                                user_id                    -- 人员ID
    ,u.dept_id                                dept_id                    -- 部门(人事所在部门)   dept_id_src                     -- 原系统员工所属部门
    ,a.ident_id                               ident_id                   -- 证件编码
    ,b.index_code                             index_code                 -- 指标代码
    ,b.index_source                           index_source               -- 指标来源(1:系统，2：手工)
    ,a.index_code                             source_index_code          -- 源系统指标代码
    ,a.index_value                            index_value                -- 指标值
    ,b.supply_cycle                           supply_cycle               -- 供数周期
    ,b.supply_frequency                       supply_frequency           -- 供数模式
    ,'虚拟账户'                               remark1
from t_emp_perf_cmplt_val_src a -- 员工业绩完成值表_源系统
left join t_index_pool b -- 单项指标(已录入系统，算是原始)
    on a.index_code = b.related_index_code  -- 原始的指标代码关联
    and b.deleted = 0 
    and b.status = 5 --  index_status (1:未通过|2:未启用|3:审批中|4:待提交|5:启用)
inner join t_index_pool_input c 
    on (b.related_index_code = c.index_code and c.latest_status = 0)
       -- or b.related_index_code is null   -- 正常情况下，值为null是不存在的
inner join sys_user u 
    on u.account_flag = 2 -- 虚拟账户
    and a.emp_code = u.belonging_user_name
    and u.account_status = 0
where a.year_months = @assess_year_month
;


-- 写步骤日志
set @step_info = '3.数据拆分：按照供数模式将数据拆分为【当期】和【累计/时点】';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);


-- 部门考核权权重，临时表。只考虑当期的考核周期
-- 校验：cnt应该=1
drop temporary table if exists tmp_appraisal_right;
create temporary table tmp_appraisal_right
select @assess_year_month year_months,dept_id,user_id,dept_appraisal_id,max(dept_appraisal_right) dept_appraisal_right,count(distinct dept_appraisal_right) cnt
from t_assess_info_his a
inner join t_dict_assess_pd_mqh mqh 
on a.assess_pd_value = mqh.assess_pd_value
and (@assess_month_2str between mqh.assess_start_month_2str and mqh.assess_end_month_2str)
where dept_id = @dept_id
and dept_appraisal_id is not null
group by dept_id,user_id,dept_appraisal_id
;


delete from t_emp_perf_cmplt_val_month where year_months = @assess_year_month and dept_id = @dept_id;
-- 当期的数据
insert into t_emp_perf_cmplt_val_month(year_months,assess_year,pd_quarter_ym,pd_half_year_ym,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,index_value,appraisal_right,supply_cycle,supply_frequency)
select
     a.year_months
    ,a.assess_year
    ,a.pd_quarter_ym
    ,a.pd_half_year_ym
    ,a.emp_code
    ,a.user_id
    ,t.dept_id   -- 考核权部门
    ,a.ident_id
    ,a.index_code
    ,a.source_index_code
    ,a.index_value  index_value
    ,t.dept_appraisal_right appraisal_right
    ,a.supply_cycle
    ,a.supply_frequency
from tmp_cmplt_val_src_record a
left join tmp_appraisal_right t 
    on a.year_months = t.year_months
    and a.user_id = t.user_id
where a.year_months = @assess_year_month
and a.supply_frequency = 1 -- 数据模式为当期
and t.dept_id = @dept_id
;



-- 无需计算的累计/时点
delete from t_emp_perf_cmplt_val_month_accumulate where year_months = @assess_year_month and dept_id = @dept_id;
insert into t_emp_perf_cmplt_val_month_accumulate(year_months,assess_year,pd_quarter_ym,pd_half_year_ym,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,index_value,appraisal_right,supply_cycle,supply_frequency)
select
     a.year_months
    ,a.assess_year
    ,a.pd_quarter_ym
    ,a.pd_half_year_ym
    ,a.emp_code
    ,a.user_id
    ,t.dept_id   -- 考核权部门
    ,a.ident_id
    ,a.index_code
    ,a.source_index_code
    ,a.index_value  index_value
    ,t.dept_appraisal_right appraisal_right
    ,a.supply_cycle
    ,a.supply_frequency
from tmp_cmplt_val_src_record a
left join tmp_appraisal_right t 
    on a.year_months = t.year_months
    and a.user_id = t.user_id
where a.year_months = @assess_year_month
and a.supply_frequency in (2,3) -- 数据模式为累计
and t.dept_id = @dept_id
;


-- 写步骤日志
set @step_info = '4.数据聚合：按照考核模式、周期聚合数据';
set @remark = '生成所有可能性的数据';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);
set @remark = '';


/*
数据聚合

*/
-- A. 当期值计算： 季度
delete from t_emp_perf_cmplt_val_quarter where pd_quarter_ym = @assess_year_month and dept_id = @dept_id;
insert into t_emp_perf_cmplt_val_quarter(assess_year,pd_quarter_ym,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,supply_cycle,supply_frequency,index_value,month_cnt)
select assess_year,pd_quarter_ym,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,supply_cycle,supply_frequency
,sum(index_value) index_value,count(1) month_cnt
from t_emp_perf_cmplt_val_month
where pd_quarter_ym = @assess_year_month
and supply_frequency = 1 -- 当期转为其他周期当期的数据
and dept_id = @dept_id
group by assess_year,pd_quarter_ym,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,supply_cycle,supply_frequency
;




-- B. 当期值计算： 半年
delete from t_emp_perf_cmplt_val_half_year where pd_half_year_ym = @assess_year_month and dept_id = @dept_id;
insert into t_emp_perf_cmplt_val_half_year(assess_year,pd_half_year_ym,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,supply_cycle,supply_frequency,index_value,month_cnt)
select assess_year,pd_half_year_ym,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,supply_cycle,supply_frequency
,sum(index_value) index_value,count(1) month_cnt
from t_emp_perf_cmplt_val_month
where pd_half_year_ym = @assess_year_month
and year_months <= @assess_year_month
and supply_frequency = 1 -- 当期转为其他周期当期的数据
and dept_id = @dept_id
group by assess_year,pd_half_year_ym,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,supply_cycle,supply_frequency
;





-- C. 累计值(需计算)：月度+季度+半年
-- 增量粒度：月
delete from t_emp_perf_cmplt_val_all_months where year_months = @assess_year_month and dept_id = @dept_id;
insert into t_emp_perf_cmplt_val_all_months(year_months,assess_year,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,supply_cycle,supply_frequency,index_value,month_cnt)
select @assess_year_month year_months  -- 默认为当月
,assess_year,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,supply_cycle,supply_frequency
,sum(index_value) index_value,count(1) month_cnt
from t_emp_perf_cmplt_val_month
where assess_year = @assess_year  -- 注意：是当年数据的汇总
and year_months <= @assess_year_month
and supply_frequency = 1 -- 当期转为其他周期当期的数据
and dept_id = @dept_id
group by assess_year,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,supply_cycle,supply_frequency
;



-- D.累计(不计算)+时点：季度
delete from t_emp_perf_cmplt_val_all_months_accumulate_quarter where pd_quarter_ym = @assess_year_month and dept_id = @dept_id;
insert into t_emp_perf_cmplt_val_all_months_accumulate_quarter(pd_quarter_ym,assess_year,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,supply_cycle,supply_frequency,index_value,month_cnt)
select @assess_year_month pd_quarter_ym
,a.assess_year,a.emp_code,a.user_id,a.dept_id,a.ident_id,a.index_code,a.source_index_code,a.supply_cycle,a.supply_frequency
,a.index_value,1 month_cnt
from t_emp_perf_cmplt_val_month_accumulate a 
inner join (
    select assess_year,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,supply_cycle,supply_frequency
    ,max(year_months) year_months   -- 最近一个月的累计/时点数据
    from t_emp_perf_cmplt_val_month_accumulate
    where supply_frequency in (2, 3) -- 累计+时点
    and pd_quarter_ym = @assess_year_month -- 取当前符合条件的所有数据
    and dept_id = @dept_id
    group by assess_year,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,supply_cycle,supply_frequency
) aaa
on  a.assess_year             = aaa.assess_year
    and   a.user_id           = aaa.user_id
    and   a.dept_id           = aaa.dept_id
    and ifnull(a.ident_id,'') = ifnull(aaa.ident_id,'')
    and   a.index_code        = aaa.index_code
    and   a.supply_cycle      = aaa.supply_cycle
    and   a.supply_frequency  = aaa.supply_frequency
    and   a.year_months       = aaa.year_months
where a.supply_frequency in (2, 3) -- 累计+时点
;



-- E.累计(不计算)+时点：半年度
delete from t_emp_perf_cmplt_val_all_months_accumulate_half_year where pd_half_year_ym = @assess_year_month and dept_id = @dept_id;
insert into t_emp_perf_cmplt_val_all_months_accumulate_half_year(pd_half_year_ym,assess_year,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,supply_cycle,supply_frequency,index_value,month_cnt)
select @assess_year_month pd_half_year_ym
,a.assess_year,a.emp_code,a.user_id,a.dept_id,a.ident_id,a.index_code,a.source_index_code,a.supply_cycle,a.supply_frequency
,a.index_value,1 month_cnt
from t_emp_perf_cmplt_val_month_accumulate a 
inner join (
    select assess_year,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,supply_cycle,supply_frequency
    ,max(year_months) year_months   -- 最近一个月的累计/时点数据
    from t_emp_perf_cmplt_val_month_accumulate
    where supply_frequency in (2, 3) -- 累计+时点
    and pd_half_year_ym = @assess_year_month -- 取当前符合条件的所有数据
    and dept_id = @dept_id
    group by assess_year,emp_code,user_id,dept_id,ident_id,index_code,source_index_code,supply_cycle,supply_frequency
) aaa
on  a.assess_year            = aaa.assess_year
    and   a.user_id          = aaa.user_id
    and   a.dept_id          = aaa.dept_id
    and   a.index_code       = aaa.index_code
    and   a.supply_cycle     = aaa.supply_cycle
    and   a.supply_frequency = aaa.supply_frequency
    and   a.year_months      = aaa.year_months
where a.supply_frequency in (2, 3) -- 累计+时点
;



-- 写步骤日志
set @step_info = '5.生成员工完成值：根据列定的考核方案，生成最终的完成值';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);


set @where_sql = '';
if @dept_id > 0 then 
    set @where_sql = concat(' and b.dept_id = ',@dept_id);      -- 限制考核部门
end if;

if @assess_pd > 0 then 
    set @where_sql = concat(@where_sql,' and b.assess_pd = ',@assess_pd);  -- 限制考核周期
end if;




-- 删除
set @run_sql = concat('delete from t_emp_perf_cmplt_val b where b.year_months = @assess_year_month and b.index_source = 1',@where_sql,';');

-- 预处理需要执行的动态SQL，其中stmt是一个变量
PREPARE stmt FROM @run_sql;
-- 执行SQL语句
EXECUTE stmt;
-- 释放掉预处理段
DEALLOCATE PREPARE stmt;


-- 目前按照考核模式，考核周期计算，分为7种情况
set @run_sql = concat('
-- 情况(1) 月考核周期 当期考核
insert into t_emp_perf_cmplt_val(year_months,assess_year,assess_pd,assess_pd_value,user_id,dept_id,index_code,index_value,index_valuestr,index_source,index_source_cn2,creator,updater,remark2)
select 
     a.year_months                         -- 年月
    ,a.assess_year assess_year             -- 考核年份
    ,b.assess_pd                           -- 考核周期
    ,b.assess_pd_value assess_pd_value     -- 考核周期值
    ,a.user_id user_id                     -- 人员ID
    ,b.dept_id dept_id                     -- 考核部门
    ,a.index_code index_code               -- 指标代码
    ,a.index_value index_value             -- 定量指标值
    ,null index_valuestr                   -- 定性填报
    ,b.index_source                        -- 数据来源
    ,b.index_source_cn2                    -- 指标来源(用中文 业绩系统/手工录入)
    ,1 creator                             -- 创建人
    ,1 updater                             -- 修改人
    ,\'情况(1) 月考核周期 当期考核\'  remark2           -- 备注2
from t_emp_perf_cmplt_val_month a 
inner join t_assess_info_his b 
    on a.year_months = b.year_months
    and a.user_id = b.user_id 
    and a.index_code = b.index_code
    and a.dept_id = b.dept_id
where a.year_months = @assess_year_month
    and b.assess_pd = 1          -- 考核周期：月度
    and b.assess_model = 1       -- 考核模式：当期
',@where_sql,';');


PREPARE stmt FROM @run_sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt; 


set @run_sql = concat('
-- 情况(2) 季考核周期 当期考核
insert into t_emp_perf_cmplt_val(year_months,assess_year,assess_pd,assess_pd_value,user_id,dept_id,index_code,index_value,index_valuestr,index_source,index_source_cn2,creator,updater,remark2)
select 
     a.pd_quarter_ym year_months           -- 年月
    ,a.assess_year assess_year             -- 考核年份
    ,b.assess_pd                           -- 考核周期
    ,b.assess_pd_value assess_pd_value     -- 考核周期值
    ,a.user_id user_id                     -- 人员ID
    ,b.dept_id dept_id                     -- 考核部门
    ,a.index_code index_code               -- 指标代码
    ,a.index_value index_value             -- 定量指标值
    ,null index_valuestr                   -- 定性填报
    ,b.index_source                        -- 数据来源
    ,b.index_source_cn2                    -- 指标来源(用中文 业绩系统/手工录入)
    ,1 creator                             -- 创建人
    ,1 updater                             -- 修改人
    ,\'情况(2) 季考核周期 当期考核\'  remark2         -- 备注2
from t_emp_perf_cmplt_val_quarter a 
inner join t_assess_info_his b 
    on a.pd_quarter_ym = b.year_months
    and a.user_id = b.user_id 
    and a.index_code = b.index_code
    and a.dept_id = b.dept_id
where a.pd_quarter_ym = @assess_year_month
    and b.assess_pd = 2          -- 考核周期：季度
    and b.assess_model = 1       -- 考核模式：当期
',@where_sql,';');

PREPARE stmt FROM @run_sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt; 



set @run_sql = concat('
-- 情况(3) 半年考核周期 当期考核
insert into t_emp_perf_cmplt_val(year_months,assess_year,assess_pd,assess_pd_value,user_id,dept_id,index_code,index_value,index_valuestr,index_source,index_source_cn2,creator,updater,remark2)
select 
     a.pd_half_year_ym year_months         -- 年月
    ,a.assess_year assess_year             -- 考核年份
    ,b.assess_pd                           -- 考核周期
    ,b.assess_pd_value assess_pd_value     -- 考核周期值
    ,a.user_id user_id                     -- 人员ID
    ,b.dept_id dept_id                     -- 考核部门
    ,a.index_code index_code               -- 指标代码
    ,a.index_value index_value             -- 定量指标值
    ,null index_valuestr                   -- 定性填报
    ,b.index_source                        -- 数据来源
    ,b.index_source_cn2                    -- 指标来源(用中文 业绩系统/手工录入)
    ,1 creator                             -- 创建人
    ,1 updater                             -- 修改人
    ,\'情况(3) 半年考核周期 当期考核\'  remark2       -- 备注2
from t_emp_perf_cmplt_val_half_year a 
inner join t_assess_info_his b 
    on a.pd_half_year_ym = b.year_months
    and a.user_id = b.user_id 
    and a.index_code = b.index_code
    and a.dept_id = b.dept_id
where a.pd_half_year_ym = @assess_year_month
    and b.assess_pd = 3          -- 考核周期：半年度
    and b.assess_model = 1       -- 考核模式：当期
',@where_sql,';');

PREPARE stmt FROM @run_sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt; 


set @run_sql = concat('
-- 情况(4) 累计考核当期供数
insert into t_emp_perf_cmplt_val(year_months,assess_year,assess_pd,assess_pd_value,user_id,dept_id,index_code,index_value,index_valuestr,index_source,index_source_cn2,creator,updater,remark2)
select 
     a.year_months                         -- 年月
    ,a.assess_year assess_year             -- 考核年份
    ,b.assess_pd                           -- 考核周期
    ,b.assess_pd_value assess_pd_value     -- 考核周期值
    ,a.user_id user_id                     -- 人员ID
    ,b.dept_id dept_id                     -- 考核部门
    ,a.index_code index_code               -- 指标代码
    ,a.index_value index_value             -- 定量指标值
    ,null index_valuestr                   -- 定性填报
    ,b.index_source                        -- 数据来源
    ,b.index_source_cn2                    -- 指标来源(用中文 业绩系统/手工录入)
    ,1 creator                             -- 创建人
    ,1 updater                             -- 修改人
    ,\'情况(4) 累计考核当期供数\'    remark2    -- 备注2
from t_emp_perf_cmplt_val_all_months a 
inner join t_assess_info_his b 
    on a.year_months = b.year_months
    and a.user_id = b.user_id 
    and a.index_code = b.index_code
    and a.dept_id = b.dept_id
where a.year_months = @assess_year_month
    and b.assess_pd in (1,2,3)          -- 考核周期：月度、季、半年
    and b.assess_model = 2              -- 考核模式：累计
    and b.supply_frequency = 1          -- 供数模式：当期
',@where_sql,';');

PREPARE stmt FROM @run_sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;



set @run_sql = concat('
-- 情况(5) 月考核周期 双累计/双时点
insert into t_emp_perf_cmplt_val(year_months,assess_year,assess_pd,assess_pd_value,user_id,dept_id,index_code,index_value,index_valuestr,index_source,index_source_cn2,creator,updater,remark2)
select 
     a.year_months                         -- 年月
    ,a.assess_year assess_year             -- 考核年份
    ,b.assess_pd                           -- 考核周期
    ,b.assess_pd_value assess_pd_value     -- 考核周期值
    ,a.user_id user_id                     -- 人员ID
    ,b.dept_id dept_id                     -- 考核部门
    ,a.index_code index_code               -- 指标代码
    ,a.index_value index_value             -- 定量指标值
    ,null index_valuestr                   -- 定性填报
    ,b.index_source                        -- 数据来源
    ,b.index_source_cn2                    -- 指标来源(用中文 业绩系统/手工录入)
    ,1 creator                             -- 创建人
    ,1 updater                             -- 修改人
    ,\'情况(5) 月考核周期 双累计/双时点\'    remark2    -- 备注2
from t_emp_perf_cmplt_val_month_accumulate a 
inner join t_assess_info_his b 
    on a.year_months = b.year_months
    and a.user_id = b.user_id 
    and a.index_code = b.index_code
    and a.dept_id = b.dept_id
where a.year_months = @assess_year_month
    and b.assess_pd = 1          -- 考核周期：月度
    and (
            (b.assess_model = 2 and b.supply_frequency = 2)   -- 双累计
            or (b.assess_model = 3 and b.supply_frequency = 3)  -- 双时点
        )
',@where_sql,';');

PREPARE stmt FROM @run_sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;



set @run_sql = concat('
-- 情况(6) 季考核周期 双累计/双时点
insert into t_emp_perf_cmplt_val(year_months,assess_year,assess_pd,assess_pd_value,user_id,dept_id,index_code,index_value,index_valuestr,index_source,index_source_cn2,creator,updater,remark2)
select 
     a.pd_quarter_ym   year_months         -- 年月
    ,a.assess_year assess_year             -- 考核年份
    ,b.assess_pd                           -- 考核周期
    ,b.assess_pd_value assess_pd_value     -- 考核周期值
    ,a.user_id user_id                     -- 人员ID
    ,b.dept_id dept_id                     -- 考核部门
    ,a.index_code index_code               -- 指标代码
    ,a.index_value index_value             -- 定量指标值
    ,null index_valuestr                   -- 定性填报
    ,b.index_source                        -- 数据来源
    ,b.index_source_cn2                    -- 指标来源(用中文 业绩系统/手工录入)
    ,1 creator                             -- 创建人
    ,1 updater                             -- 修改人
    ,\'情况(6) 季考核周期 双累计/双时点\'    remark2    -- 备注2
from t_emp_perf_cmplt_val_all_months_accumulate_quarter a 
inner join t_assess_info_his b 
    on a.pd_quarter_ym = b.year_months
    and a.user_id = b.user_id 
    and a.index_code = b.index_code
    and a.dept_id = b.dept_id
where a.pd_quarter_ym = @assess_year_month
    and b.assess_pd = 2          -- 考核周期：季度
    and (
            (b.assess_model = 2 and b.supply_frequency = 2)   -- 双累计
            or (b.assess_model = 3 and b.supply_frequency = 3)  -- 双时点
        )
',@where_sql,';');

PREPARE stmt FROM @run_sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;


set @run_sql = concat('
-- 情况(7) 半年考核周期 双累计/双时点
insert into t_emp_perf_cmplt_val(year_months,assess_year,assess_pd,assess_pd_value,user_id,dept_id,index_code,index_value,index_valuestr,index_source,index_source_cn2,creator,updater,remark2)
select 
     a.pd_half_year_ym   year_months       -- 年月
    ,a.assess_year assess_year             -- 考核年份
    ,b.assess_pd                           -- 考核周期
    ,b.assess_pd_value assess_pd_value     -- 考核周期值
    ,a.user_id user_id                     -- 人员ID
    ,b.dept_id dept_id                     -- 考核部门
    ,a.index_code index_code               -- 指标代码
    ,a.index_value index_value             -- 定量指标值
    ,null index_valuestr                   -- 定性填报
    ,b.index_source                        -- 数据来源
    ,b.index_source_cn2                    -- 指标来源(用中文 业绩系统/手工录入)
    ,1 creator                             -- 创建人
    ,1 updater                             -- 修改人
    ,\'情况(7) 半年考核周期 双累计/双时点\'    remark2    -- 备注2
from t_emp_perf_cmplt_val_all_months_accumulate_half_year a 
inner join t_assess_info_his b 
    on a.pd_half_year_ym = b.year_months
    and a.user_id = b.user_id 
    and a.index_code = b.index_code
    and a.dept_id = b.dept_id
where a.pd_half_year_ym = @assess_year_month
    and b.assess_pd = 3          -- 考核周期：半年度
    and (
            (b.assess_model = 2 and b.supply_frequency = 2)   -- 双累计
            or (b.assess_model = 3 and b.supply_frequency = 3)  -- 双时点
        )
',@where_sql,';');

PREPARE stmt FROM @run_sql;
EXECUTE stmt;
DEALLOCATE PREPARE stmt;





-- 获得记录数
select count(1) cnt into V_TABLE_CNT from t_emp_perf_cmplt_val where index_source = 1 and year_months = @assess_year_month;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;