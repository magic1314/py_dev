DROP PROCEDURE IF EXISTS PRO_LOAD_JCYJ_EMP;
CREATE PROCEDURE `PRO_LOAD_JCYJ_EMP`(
															IN BIZ_DATE  DATE ,
															IN I_DEPT_ID varchar(10) )
BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     fi_yjhd
         过程名称:    PRO_LOAD_JCYJ_EMP
         功能简述：   人员信息表转换
         参数：       BIZ_DATE  上个工作日
         注意事项：
         sys_user:
              status: 对应前台允许登录
              account_status: 对应前台员工状态
         t_employee:
              status: 员工状态，前台不显示
         数据源：
                 1、 int_d_new_hrs_emp_jc    人员信息表
         修改记录;
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         苏彦运      2023/10/09                 创建

***************************************************************************************************************************************/


DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);



-- 设置变量
set V_EVENT_NAME = '人员信息表转换';
set V_RUN_COMMAND = concat('call PRO_LOAD_JCYJ_EMP(',BIZ_DATE,')');
select now() into V_START_TIME;


/*********************** t_employee表更新 ******************************/
/********************* 部门变更，历史数据保留 *********************/
-- 1. 员工新增的数据
insert into t_employee
(psndoc_id,-- 源系统人员id
sys_user_id, -- 系统用户id自增长
dept_id,-- 部门id
name,-- 姓名
code,-- 人员工号
status,-- 员工状态（0正常 1异常 ）
begindate,-- 入职日期
enddate,-- 离职日期
pk_org,-- 所属组织id
pk_group,-- 所属集团id
pk_hrorg,-- 人力组织id
iscadre,-- 是否干部
pk_job,-- 职务
sex,-- 性别
edu,-- 学历
birthdate,-- 出生日期
age,-- 年龄
workage,-- 工龄
marital,-- 婚姻状况
polit,-- 政治面貌
nationality,-- 民族
nativeplace,-- 籍贯
health,-- 健康状况
user_post_id,-- 人员岗位id
indutydate,-- 任职开始日期
enddutydate,-- 任职结束日期
email,-- 邮箱
ident_id,-- 证件号码
idtype,-- 证件类型
mobile,-- 手机
characterrpr,-- 户口性质
censusaddr,-- 户籍地址
create_time,-- 创建时间
Job_Rank, -- 职等
Post,  -- 岗位
Org_Sec_Code, -- 所属二级部门编码
Org_Sec_Name, -- 所属二级部门名称
remark,  -- 备注
Emp_Type_Code,          -- 人员类别编码
Emp_Type_Name,          -- 人员类别名称
Managercode,            -- 管理人员属性编码
Job_Appt_Dt,            -- 本岗位任职日期
Job_Rank_Appt_Dt,       -- 现职级任职开始日期
Enter_Type              -- 进入来源
)
select
    a.Pk_Psndoc psndoc_id,-- 源系统人员id
    (select max(greatest(m.max_user_id,n.max_user_id)) from (select max(user_id) max_user_id from sys_user) m,(select max(sys_user_id) max_user_id from t_employee) n)+row_number() over() sys_user_id,  -- 从sys_user获取最大的id
    I_DEPT_ID dept_id,-- 部门id
    a.Emp_Name name,-- 姓名
    a.Emp_Id code,-- 人员工号
    (case a.Emp_Status when '4' then 1 else 0 end) status,-- 员工状态（0正常 1异常 ）
    a.Join_Dt begindate,-- 入职日期
    (case  when a.Emp_Status != '4' then null 
    when a.Retire_Dt = '0001-01-01' and a.Emp_Status = '4' then a.Join_End_Dt 
    else a.Retire_Dt end) enddate,-- 离职日期
    null pk_org,-- 所属组织id
    null pk_group,-- 所属集团id
    null pk_hrorg,-- 人力组织id
    a.Is_Cadre iscadre,-- 是否干部
    a.Job_Name pk_job,-- 职务
    a.Sex sex,-- 性别
    a.EDU edu,-- 学历
    a.Birth_Dt birthdate,-- 出生日期
    a.AGE age,-- 年龄
    a.Work_Age workage,-- 工龄
    null marital,-- 婚姻状况
    null polit,-- 政治面貌
    a.Ethnic_Cd nationality,-- 民族
    a.Native_Place nativeplace,-- 籍贯
    null  health,-- 健康状况
    null user_post_id,-- 人员岗位id
    a.Join_Begin_Dt indutydate,-- 任职开始日期
    a.Join_End_Dt enddutydate,-- 任职结束日期
    a.Email email,-- 邮箱
    a.Cert_Id ident_id,-- 证件号码
    null idtype,-- 证件类型
    a.MOBILE mobile,-- 手机
    null characterrpr,-- 户口性质
    a.Census_Addr censusaddr,-- 户籍地址
    sysdate() create_time,-- 创建时间
    a.Job_Rank Job_Rank, -- 职等
    a.Post Post,  -- 岗位
    a.Org_Sec_Code Org_Sec_Code, -- 所属二级部门编码
    -- a.Org_Sec_Name Org_Sec_Name, -- 所属二级部门名称
    if(a.Org_Sec_Name like '%-%',SUBSTRING_INDEX(a.Org_Sec_Name, '-', -1),a.Org_Sec_Name) Org_Sec_Name, -- 所属二级部门名称
    concat('系统初始化员工信息 ',sysdate()) remark, -- 备注
    a.Emp_Type_Code,          -- 人员类别编码
    a.Emp_Type_Name,          -- 人员类别名称
    a.Managercode,            -- 管理人员属性编码
    a.Job_Appt_Dt,            -- 本岗位任职日期
    a.Job_Rank_Appt_Dt,       -- 现职级任职开始日期
    a.Enter_Type              -- 进入来源
from int_d_new_hrs_emp_jc a
left join t_employee b
on a.Pk_Psndoc = b.psndoc_id  -- 源系统人员id
  and b.current_flag = 1 -- 该条件可以不写
where b.psndoc_id is null -- 增加过滤已存在的 营业部编码，工号，证件号码 相同的账号
and   a.Org_Code = I_DEPT_ID
    AND ((
        a.Retire_Dt > '2022-12-31' 
        OR ( a.Retire_Dt = '0001-01-01' AND a.Join_End_Dt > '2022-12-31' ))
        OR a.Emp_Status != '4'
        )
group by 
a.Pk_Psndoc
,I_DEPT_ID
,a.Emp_Name
,a.Emp_Id
,(case a.Emp_Status when '4' then 1 else 0 end)
,a.Join_Dt
,a.Retire_Dt
,a.Is_Cadre
,a.Job_Name
,a.Sex
,a.EDU
,a.Birth_Dt
,a.AGE -- 年龄
,a.Work_Age -- 工龄
,a.Ethnic_Cd
,a.Native_Place
,a.Join_Begin_Dt
,a.Join_End_Dt
,a.Email
,a.Cert_Id
,a.MOBILE
,a.Census_Addr
,a.Job_Rank
,a.Post
,a.Org_Sec_Code
,if(a.Org_Sec_Name like '%-%',SUBSTRING_INDEX(a.Org_Sec_Name, '-', -1),a.Org_Sec_Name)
,a.Emp_Type_Code          -- 人员类别编码
,a.Emp_Type_Name          -- 人员类别名称
,a.Managercode            -- 管理人员属性编码
,a.Job_Appt_Dt            -- 本岗位任职日期
,a.Job_Rank_Appt_Dt       -- 现职级任职开始日期
,a.Enter_Type              -- 进入来源
;


-- 2. 将变更部门的数据插入结果表
insert into t_employee
(psndoc_id,-- 源系统人员id
sys_user_id, -- 系统用户id自增长
dept_id,-- 部门id
name,-- 姓名
code,-- 人员工号
status,-- 员工状态（0正常 1异常 ）
begindate,-- 入职日期
enddate,-- 离职日期
pk_org,-- 所属组织id
pk_group,-- 所属集团id
pk_hrorg,-- 人力组织id
iscadre,-- 是否干部
pk_job,-- 职务
sex,-- 性别
edu,-- 学历
birthdate,-- 出生日期
age,-- 年龄
workage,-- 工龄
marital,-- 婚姻状况
polit,-- 政治面貌
nationality,-- 民族
nativeplace,-- 籍贯
health,-- 健康状况
user_post_id,-- 人员岗位id
indutydate,-- 任职开始日期
enddutydate,-- 任职结束日期
email,-- 邮箱
ident_id,-- 证件号码
idtype,-- 证件类型
mobile,-- 手机
characterrpr,-- 户口性质
censusaddr,-- 户籍地址
create_time,-- 创建时间
Job_Rank, -- 职等
Post,  -- 岗位
Org_Sec_Code, -- 所属二级部门编码
Org_Sec_Name, -- 所属二级部门名称
current_flag,  -- 当前标识
remark,
Emp_Type_Code,          -- 人员类别编码
Managercode,            -- 管理人员属性编码
Job_Appt_Dt,            -- 本岗位任职日期
Job_Rank_Appt_Dt,       -- 现职级任职开始日期
Enter_Type              -- 进入来源
)
select
    a.Pk_Psndoc psndoc_id,-- 源系统人员id
    (select max(greatest(m.max_user_id,n.max_user_id)) from (select max(user_id) max_user_id from sys_user) m,(select max(sys_user_id) max_user_id from t_employee) n)+row_number() over() sys_user_id,  -- 从sys_user获取最大的id
    I_DEPT_ID dept_id,-- 部门id
    a.Emp_Name name,-- 姓名
    a.Emp_Id code,-- 人员工号
    (case a.Emp_Status when '4' then 1 else 0 end) status,-- 员工状态（0正常 1异常 ） 员工当前的状态
    a.Join_Dt begindate,-- 入职日期
    -- a.Retire_Dt enddate,-- 离职日期
    (case  when a.Emp_Status != '4' then null 
    when a.Retire_Dt = '0001-01-01' and a.Emp_Status = '4' then a.Join_End_Dt 
    else a.Retire_Dt end) enddate,-- 离职日期
    null pk_org,-- 所属组织id
    null pk_group,-- 所属集团id
    null pk_hrorg,-- 人力组织id
    a.Is_Cadre iscadre,-- 是否干部
    a.Job_Name pk_job,-- 职务
    a.Sex sex,-- 性别
    a.EDU edu,-- 学历
    a.Birth_Dt birthdate,-- 出生日期
    a.AGE age,-- 年龄
    a.Work_Age workage,-- 工龄
    null marital,-- 婚姻状况
    null polit,-- 政治面貌
    a.Ethnic_Cd nationality,-- 民族
    a.Native_Place nativeplace,-- 籍贯
    null  health,-- 健康状况
    null user_post_id,-- 人员岗位id
    a.Join_Begin_Dt indutydate,-- 任职开始日期
    a.Join_End_Dt enddutydate,-- 任职结束日期
    a.Email email,-- 邮箱
    a.Cert_Id ident_id,-- 证件号码
    null idtype,-- 证件类型
    a.MOBILE mobile,-- 手机
    null characterrpr,-- 户口性质
    a.Census_Addr censusaddr,-- 户籍地址
    sysdate() create_time,-- 创建时间
    a.Job_Rank Job_Rank, -- 职等
    a.Post Post,  -- 岗位
    a.Org_Sec_Code Org_Sec_Code, -- 所属二级部门编码
    -- a.Org_Sec_Name Org_Sec_Name, -- 所属二级部门名称
    if(a.Org_Sec_Name like '%-%',SUBSTRING_INDEX(a.Org_Sec_Name, '-', -1),a.Org_Sec_Name) Org_Sec_Name, -- 所属二级部门名称
    1 current_flag, -- 当前标识
    concat('变更部门后的员工信息 ',sysdate()) remark, -- 备注
    a.Emp_Type_Code,          -- 人员类别编码
    a.Managercode,            -- 管理人员属性编码
    a.Job_Appt_Dt,            -- 本岗位任职日期
    a.Job_Rank_Appt_Dt,       -- 现职级任职开始日期
    a.Enter_Type              -- 进入来源
from int_d_new_hrs_emp_jc a  -- 当前的
inner join t_employee b  -- 原始的
  on a.Pk_Psndoc = b.psndoc_id  -- 源系统人员id
  and   a.Org_Code = I_DEPT_ID
  -- and b.dept_id <> I_DEPT_ID -- 部门变更
  and b.current_flag = 2  -- 新增的
group by 
a.Pk_Psndoc
,I_DEPT_ID
,a.Emp_Name
,a.Emp_Id
,(case a.Emp_Status when '4' then 1 else 0 end)
,a.Join_Dt
,a.Retire_Dt
,a.Is_Cadre
,a.Job_Name
,a.Sex
,a.EDU
,a.Birth_Dt
,a.AGE -- 年龄
,a.Work_Age -- 工龄
,a.Ethnic_Cd
,a.Native_Place
,a.Join_Begin_Dt
,a.Join_End_Dt
,a.Email
,a.Cert_Id
,a.MOBILE
,a.Census_Addr
,a.Job_Rank
,a.Post
,a.Org_Sec_Code
,if(a.Org_Sec_Name like '%-%',SUBSTRING_INDEX(a.Org_Sec_Name, '-', -1),a.Org_Sec_Name)
,a.Emp_Type_Code          -- 人员类别编码
,a.Emp_Type_Name          -- 人员类别名称
,a.Managercode            -- 管理人员属性编码
,a.Job_Appt_Dt            -- 本岗位任职日期
,a.Job_Rank_Appt_Dt       -- 现职级任职开始日期
,a.Enter_Type             -- 进入来源
;

-- 3. 状态修改为0
update t_employee
set current_flag = 0
where current_flag = 2
;

-- 基于主键，更新员工表数据
update t_employee a 
inner join int_d_new_hrs_emp_jc b 
on a.psndoc_id = b.Pk_Psndoc   -- 源系统人员id
  and a.current_flag = 1 -- 只更新最新数据
  and   b.Org_Code = I_DEPT_ID  
set 
    a.name = b.Emp_Name -- 姓名
   ,a.code = b.Emp_Id -- 人员工号
   ,a.status = (case b.Emp_Status when '4' then 1 else 0 end) -- 员工状态（0正常 1异常 ） 员工当前的状态
   ,a.current_flag = (case when b.Emp_Status ='4' then -1 else 1 end)
   ,a.begindate = b.Join_Dt -- 入职日期
   -- ,a.enddate = b.Retire_Dt -- 离职日期
   ,a.enddate = (case when b.Emp_Status != '4' then null 
                when b.Retire_Dt = '0001-01-01' and b.Emp_Status = '4' then b.Join_End_Dt 
                else b.Retire_Dt end) -- 离职日期
   ,a.iscadre = b.Is_Cadre -- 是否干部
   ,a.pk_job = b.Job_Name -- 职务
   ,a.sex = b.Sex -- 性别
   ,a.edu = b.EDU -- 学历
   ,a.birthdate = b.Birth_Dt -- 出生日期
   ,a.age = b.AGE-- 年龄
   ,a.workage = b.Work_Age -- 工龄
   ,a.nationality = b.Ethnic_Cd -- 民族
   ,a.nativeplace = b.Native_Place -- 籍贯
   ,a.indutydate = b.Join_Begin_Dt -- 任职开始日期
   ,a.enddutydate = b.Join_End_Dt -- 任职结束日期
   ,a.email = b.Email -- 邮箱
   ,a.ident_id = b.Cert_Id -- 证件号码
   ,a.mobile = b.MOBILE -- 手机
   ,a.censusaddr = b.Census_Addr -- 户籍地址
   ,a.Job_Rank = b.Job_Rank -- 职等
   ,a.Post = b.Post  -- 岗位
   ,a.Org_Sec_Code = b.Org_Sec_Code -- 所属二级部门编码
   -- ,a.Org_Sec_Name= b.Org_Sec_Name  -- 所属二级部门名称
   ,a.Org_Sec_Name= if(b.Org_Sec_Name like '%-%',SUBSTRING_INDEX(b.Org_Sec_Name, '-', -1),b.Org_Sec_Name)  -- 所属二级部门名称
   ,a.Emp_Type_Code       =    b.Emp_Type_Code      -- 人员类别编码
   ,a.Emp_Type_Name       =    b.Emp_Type_Name      -- 人员类别名称
   ,a.Managercode         =    b.Managercode        -- 管理人员属性编码
   ,a.Job_Appt_Dt         =    b.Job_Appt_Dt        -- 本岗位任职日期
   ,a.Job_Rank_Appt_Dt    =    b.Job_Rank_Appt_Dt   -- 现职级任职开始日期
   ,a.Enter_Type          =    b.Enter_Type         -- 进入来源
;




/*********************** sys_user表更新 ******************************/


-- 平台的用户

-- 新增员工，初始化到用户表
insert into sys_user
    (user_id, -- 用户id
     dept_id, -- 部门id
     user_name, -- 用户账号
     nick_name, -- 用户昵称
     user_type, -- 用户类型（00系统用户）
     email, -- 用户邮箱
     phonenumber, -- 手机号码
     sex, -- 用户性别（0男 1女 2未知）
     avatar, -- 头像地址
     password, -- 密码
     status, -- 帐号状态（0正常 1停用）
     del_flag, -- 删除标志（0代表存在 2代表删除）
     login_ip, -- 最后登录ip
     login_date, -- 最后登录时间
     create_by, -- 创建者
     create_time, -- 创建时间
     update_by, -- 更新者
     update_time, -- 更新时间
     remark, -- 备注
     account_status,-- 状态(0正常 1离职 2废弃)
     account_flag,-- 工号标识(1实体 | 2虚拟)
     belonging_user_name,-- 所属用户账号
     last_chgpwdtime,-- 上次修改密码时间
     post, -- 岗位
     org_sec_code, -- 所属二级部门编码
     org_sec_name, -- 所属二级部门名称
     begindate, -- 入职日期
     enddate -- 离职日期
     )
    select a.sys_user_id user_id, -- 系统用户id
           a.dept_id dept_id, -- 部门id
           a.code user_name, -- 人员工号
           a.name nick_name, -- 姓名
           '00' user_type, -- 用户类型（00系统用户）
           null email, -- 用户邮箱
           a.mobile phonenumber, -- 手机号码
           a.sex sex, -- 用户性别（0男 1女 2未知）
           null avatar, -- 头像地址
           '$2a$10$uI4MP7TF2fqxrvJwCzJC/eY2GBY6409Lm0X09EUul/UWS2d65raiC' password, -- 密码，默认密码：Swhy1234
           if(c.user_id is null,'0','1') status, -- 帐号状态（0正常 1停用） 是否允许登录
           '0' del_flag, -- 删除标志（0代表存在 2代表删除）
           null login_ip, -- 最后登录ip
           null login_date, -- 最后登录时间
           1 create_by, -- 创建者
           sysdate() create_time, -- 创建时间
           null update_by, -- 更新者
           null update_time, -- 更新时间
           concat('系统新增用户',sysdate()) remark, -- 备注
           if(a.status='0','0','1') account_status, -- 状态(0正常 1离职(离退) 2废弃 3组织调整) 员工状态
           1 account_flag,-- 工号标识(1实体 | 2虚拟)  虚拟用户是系统新增的，初始化不处理
           null belonging_user_name,-- 所属用户账号
           null last_chgpwdtime,-- 上次修改密码时间
           a.post, -- 岗位
           a.org_sec_code, -- 所属二级部门编码
           a.org_sec_name, -- 所属二级部门名称
           a.indutydate begindate, -- 入职日期(Join_Begin_Dt)
           a.enddate -- 离职日期
      from t_employee a
      left join sys_user b
        on a.sys_user_id = b.user_id -- 增加过滤已存在的营业部编码，工号，证件号码相同的账号
      left join sys_user c   -- 相同工号允许登录至多1条，前端有限制(如果该工号不是第一次新增，允许登录默认为1)
        on a.code = c.user_name 
        and c.status = '0'
     where b.user_id is null
     and a.current_flag = 1 -- 只取当前状态的数据
;

-- 初始化用户角色信息
insert into sys_user_role(user_id,role_id)
select sys_user_id,2003 role_id
from t_employee a
where not exists(select 1 from sys_user_role b where a.sys_user_id = b.user_id)
;

-- 员工切换部门，原纪录置为状态3
update sys_user a
inner join t_employee b 
    on a.user_name = b.code -- 人员工号
    and b.current_flag = 1 -- 当前状态
    and a.dept_id <> b.dept_id -- 部门id
set a.account_status = 3, -- 状态(0正常 1离职 2废弃 3组织调整)
    -- a.status = 1, -- 帐号状态（0正常 1停用） 不主动修改运行登录标识
    a.update_by = 1, 
    a.update_time = sysdate(), 
    a.remark = concat('员工切换部门：',a.dept_id,'->',b.dept_id,'，切换时间：',sysdate())
where a.account_status = 0 -- 状态(0正常 1离职 2废弃)
;

-- 离职人员注销虚拟账户
update sys_user a -- 虚拟账户
inner join sys_user b -- 虚拟账户对应的主账户
on a.belonging_user_name = b.user_name
and a.account_flag = 2 -- 账户类型(1:实体账户,2:虚拟账户)
and b.account_flag = 1 -- 账户类型(1:实体账户,2:虚拟账户)
and b.account_status = 1 -- 账户状态(0正常 1离职 2废弃)
set a.begindate = b.begindate,
    a.enddate = b.enddate,
    a.account_status = 1, -- 状态(0正常 1离职 2废弃)
    a.status = 1, -- 帐号状态（0正常 1停用）
    a.update_by = 1, 
    a.update_time = sysdate(), 
    a.remark = concat('员工离职注销虚拟账户，注销时间：',sysdate())
;



-- 更新sys_user表
update sys_user b 
inner join t_employee a
on a.sys_user_id = b.user_id
    and a.current_flag = 1 -- 只取当前状态的数据,禁用用户不再更新
    and b.account_status = 0 -- 状态(0正常 1离职 2废弃)
set
    b.dept_id = a.dept_id,-- 部门id
    b.user_name = a.code,-- 人员工号
    b.nick_name = a.name,-- 姓名
    b.phonenumber = a.mobile,-- 手机号码
    b.sex = a.sex,-- 用户性别（0男 1女 2未知）
    b.account_status = a.status,-- 帐号状态（0正常 1停用）
    b.Post = a.Post,  -- 岗位
    b.Org_Sec_Code = a.Org_Sec_Code, -- 所属二级部门编码
    b.Org_Sec_Name = a.Org_Sec_Name, -- 所属二级部门名称
    b.begindate = a.indutydate, -- 入职日期(Join_Begin_Dt)
    b.enddate   = a.enddate -- 离职日期
;

-- 如果员工离职，用户状态改为离职
-- 切换部门后，account_status标记为1，就不再记录上次密码
-- 20220310 新增判断 account_flag = 1，只更新实体账户
-- 如果员工离职了，不允许登录(虚拟用户一定是不允许登录)
-- 离退人员添加删除标志，前端不显示
update sys_user a
set a.status = 1
    ,a.remark = concat('离退人员，操作时间:',sysdate())
where a.account_status = 1
and a.account_flag = 1
and a.status != 1
;


-- 获得记录数
select count(1) cnt into V_TABLE_CNT from sys_user;

-- 写日志
call PRO_WRITELOG_FI_YJHD(V_EVENT_NAME,V_RUN_COMMAND,BIZ_DATE,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;
END
;