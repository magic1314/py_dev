DROP PROCEDURE IF EXISTS PRO_YJGL_ASSESS_INFO_HIS_copy1;
CREATE PROCEDURE `PRO_YJGL_ASSESS_INFO_HIS_copy1`(IN IN_YEAR_MONTH VARCHAR(6), IN IN_DEPT_ID INT, IN IN_PERIOD INT)
label:BEGIN
/******************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_YJGL_ASSESS_INFO_HIS
         功能简述：   考核方案归档表
         参数：       IN_YEAR_MONTH    考核年月(当前考核周期的最后一个月份，比如2023年第二季度：202306)
                      IN_DEPT_ID       部门编号(如果是传入0是指所有的部门，其他是指定的部门)
                      IN_PERIOD        考核周期(0:全部,1:月,2:季度,3:半年)
         返回：       无
         注意事项：
                 1. 传入日期的上个月，是考核的月份
                 eg.  今日为：202303
                      考核月份是：202303，也就是结果表中year_months的取值
                 2. 定量指标供数周期、供数模式、考核周期、考核模式均不为null
                    定性指标供数周期、供数模式、考核周期、考核模式均为null
                 3. dept_appraisal_id字段：是t_appraisal_id.appraisal_id，member表id
         数据源：
                 1、 t_assess_dtl                考核方案表
                 2、 tmp_assess_dtl_statistics   考核方案临时统计表
                 3、 t_assess_dtl_idx            考核方案明细
                 4、 t_assess_dtl_idx_tgt        考核方案阶段目标值表
                 5、 t_index_pool                单项指标表
                 6、 t_dict_assess_pd_mqh        考核周期字典
         调度信息：
                 dolpin调度：每天02:00
                 前台调度：  【考核评价】->【进度管理】->点击【开始考核】按钮
         修改记录：
         -----------------------------------------------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2023/03/30                  创建
         MG         2023/04/07                  新增dept_id的传参
         MG         2023/04/08                  日期参数传入 考核的年月
         MG         2023/04/12                  新增考核周期传参，考核明细表逻辑新增
         MG         2023/04/19                  考核周期限制添加，定性和定量指标都计算，更新指标来源_中文
         MG         2023/04/24                  新增定性指标
         MG         2023/04/25                  定性指标当期考核目标为dtl_idx中年度考核目标
         MG         2023/04/27                  如果部门某考核周期开启考核，则不会再更新它的考核方案(该内容注释掉了)
         MG         2023/04/28                  当IN_DEPT_ID和IN_PERIOD都为0(也就是每天的调度)，不会删除当期已开启考核的数据
         MG         2023/05/12                  字段别名存在问题
         MG         2023/06/05                  格式化sql
         MG         2023/06/07                  新增考核权的限制；将tmp表改为临时表；新增dept_appraisal_id字段
         MG         2023/06/09                  考核权时间限制中，时间交叉的条件问题修复
         MG         2023/06/19                  只对考核方案最后时点落在考核周期上的考核
         MG         2023/07/07                  解决字符集问题；新增日志步骤表的逻辑
         MG         2023/07/20                  考核权审批中的依然是可以使用的，由于考核权审批只是针对当月及之后的月份，历史月份不受影响
         MG         2023/08/01                  锁定（修改V_RUN_COMMAND变量中的错误写法）；修改临时表的索引；添加条目：调度信息
******************************************************************************************************************************************/
-- 变量定义
DECLARE V_TABLE_CNT int; 
DECLARE V_TASK_CNT int; 
DECLARE V_START_TIME datetime; 
DECLARE V_EVENT_NAME varchar(200); 
DECLARE V_RUN_COMMAND varchar(1000);
DECLARE V_BATCH_NO varchar(200);
DECLARE V_TOTAL_STEP_NUM int;

-- 设置变量
set V_EVENT_NAME = '考核方案归档表'; 
set V_TOTAL_STEP_NUM = 5;  -- 设置总步骤数
set @remark = ''; -- 步骤日志中的备注，可以每步都定义
select now() into V_START_TIME; 


-- 解决字符集问题：Illegal mix of collations (utf8mb4_0900_ai_ci,IMPLICIT) and (utf8mb4_general_ci,IMPLICIT) for operation
set collation_connection = utf8mb4_general_ci;

set @assess_year_month = IN_YEAR_MONTH; -- 格式: 202302
set @assess_month = cast(right(@assess_year_month,2) as UNSIGNED); -- 格式：2
set @assess_year = left(@assess_year_month,4); -- 格式：2023
set @dept_id = IN_DEPT_ID; -- 格式： 12800
set @assess_pd = IN_PERIOD; -- 格式： 1
set @assess_pd_value = (select assess_pd_value from t_dict_assess_pd_mqh where assess_pd = @assess_pd and assess_month = @assess_month); -- 格式： M2


set V_RUN_COMMAND = concat('call PRO_YJGL_ASSESS_INFO_HIS('
                            ,IN_YEAR_MONTH,','
                            ,IN_DEPT_ID,','
                            ,IN_PERIOD
                            ,')'
                         );

set V_BATCH_NO = concat(   UNIX_TIMESTAMP(),'-'
                        ,replace(V_RUN_COMMAND,'call ',''),'-'
                        ,V_TOTAL_STEP_NUM
                     );

if @assess_month >= 13 then
    select '传入参数【考核年月】不符合要求'; LEAVE label; -- 退出存储过程 
end if; 
if @assess_pd >= 4 then
    select '传入参数【考核周期】不符合要求'; LEAVE label; -- 退出存储过程 
end if; 

-- 针对不同的月份，定义季度和半年的起始结束月份
if @assess_month = 3 then
    set @quarter_start = 1; 
    set @quarter_end   = 3; 
elseif @assess_month = 6 then 
    set @quarter_start = 4; 
    set @quarter_end   = 6; 
    set @half_year_start = 1; 
    set @half_year_end   = 6; 
elseif @assess_month = 9 then 
    set @quarter_start = 7; 
    set @quarter_end   = 9; 
elseif @assess_month = 12 then 
    set @quarter_start = 10; 
    set @quarter_end   = 12; 
    set @half_year_start = 6; 
    set @half_year_end   = 12; 
end if; 


-- 写步骤日志
set @step_info = '1.参数定义&基础检查:正常';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);


/***************************** 考核权的限制 ************************************/
-- 写步骤日志
set @step_info = '2.限制考核权:只有用户考核期在考核权内，才进行方案列定';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);


-- 创建临时表
drop TEMPORARY TABLE if exists `tmp_global_appraisal`;
CREATE TEMPORARY TABLE `tmp_global_appraisal` (
  `dept_appraisal_id` bigint DEFAULT NULL COMMENT '考核人员表id',
  `user_id` bigint DEFAULT NULL COMMENT '用户id',
  `user_code` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '工号',
  `user_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '姓名',
  `register_date` date DEFAULT NULL COMMENT '生效日期',
  `cancel_date` date DEFAULT NULL COMMENT '注销日期',
  `dept_id` bigint DEFAULT NULL COMMENT '部门id',
  `dept_appraisal_right` decimal(10,2) DEFAULT NULL COMMENT '部门考核权重'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci
;

drop TEMPORARY TABLE if exists `tmp_global_appraisal_mqh`;
CREATE TEMPORARY TABLE `tmp_global_appraisal_mqh` (
  `year_months` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '年月',
  `assess_pd` int DEFAULT NULL COMMENT '考核周期',
  `dept_appraisal_id` bigint DEFAULT NULL COMMENT '考核人员表id',
  `dept_id` bigint DEFAULT NULL COMMENT '部门id',
  `user_id` bigint DEFAULT NULL COMMENT '用户id',
  `user_code` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '工号',
  `user_name` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '姓名',
  `register_date` date DEFAULT NULL COMMENT '生效年月',
  `cancel_date` date DEFAULT NULL COMMENT '注销日期',
  `dept_appraisal_right` decimal(10,2) DEFAULT NULL COMMENT '部门考核权重',
  KEY `lh_4` (`year_months`,`assess_pd`,`dept_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci
;


if @dept_id > 0 then 
    insert into tmp_global_appraisal(dept_appraisal_id,user_id,user_code,user_name,register_date,cancel_date,dept_id,dept_appraisal_right)
    select 
        b.appraisal_id dept_appraisal_id
        ,a.user_id
        ,a.user_code
        ,a.user_name
        ,a.register_date
        ,ifnull(date_sub(a.cancel_date, interval 1 day),'2099-12-31') cancel_date
        ,b.dept_id
        ,b.appraisal_right dept_appraisal_right
    from t_appraisal_member a 
    inner join t_appraisal_dept b 
    on a.id = b.appraisal_id
    and a.user_status in (0,1,2)
    and a.deleted = 0
    and b.deleted = 0
    and (a.register_date != a.cancel_date or a.cancel_date is null)
    and b.dept_id = @dept_id  -- 限定部门
    order by a.user_id,a.register_date
    ;
else 
    insert into tmp_global_appraisal(dept_appraisal_id,user_id,user_code,user_name,register_date,cancel_date,dept_id,dept_appraisal_right)
    select 
        b.appraisal_id dept_appraisal_id
        ,a.user_id
        ,a.user_code
        ,a.user_name
        ,a.register_date
        ,ifnull(date_sub(a.cancel_date, interval 1 day),'2099-12-31') cancel_date
        ,b.dept_id
        ,b.appraisal_right dept_appraisal_right
    from t_appraisal_member a 
    inner join t_appraisal_dept b 
    on a.id = b.appraisal_id
    and a.user_status in (0,1,2)
    and a.deleted = 0
    and b.deleted = 0
    and (a.register_date != a.cancel_date or a.cancel_date is null)
    order by a.user_id,a.register_date
    ;
end if;


-- 按考核周期取当前考核期内最新一条考核权
delete from tmp_global_appraisal_mqh;
if @assess_pd = 1 or @assess_pd = 0 then  -- 月度
    insert into tmp_global_appraisal_mqh(year_months,assess_pd,dept_appraisal_id,user_id,user_code,user_name,register_date,cancel_date,dept_id,dept_appraisal_right)
    select @assess_year_month year_months,1 assess_pd
          ,dept_appraisal_id,user_id,user_code,user_name,register_date,cancel_date,dept_id,dept_appraisal_right
    from (
        select 
            row_number() over(partition by dept_id,user_id order by register_date desc) rn 
            ,dept_appraisal_id,user_id,user_code,user_name,register_date,cancel_date,dept_id,dept_appraisal_right
        from tmp_global_appraisal
        where @assess_year_month between date_format(register_date,'%Y%m') and date_format(cancel_date,'%Y%m') -- 需要在考核期内
    ) x
    where x.rn = 1
    ;
end if;



if @assess_pd = 2 or @assess_pd = 0 then  -- 季度 
    insert into tmp_global_appraisal_mqh(year_months,assess_pd,dept_appraisal_id,user_id,user_code,user_name,register_date,cancel_date,dept_id,dept_appraisal_right)
    select @assess_year_month year_months,2 assess_pd
          ,dept_appraisal_id,user_id,user_code,user_name,register_date,cancel_date,dept_id,dept_appraisal_right
    from (
        select 
            row_number() over(partition by dept_id,user_id order by register_date desc) rn 
            ,dept_appraisal_id,user_id,user_code,user_name,register_date,cancel_date,dept_id,dept_appraisal_right
        from tmp_global_appraisal
        where (
                    date_format(register_date,'%Y%m') <= concat(@assess_year,lpad(@quarter_end,2,'0')) 
                and concat(@assess_year,lpad(@quarter_start,2,'0')) <= date_format(cancel_date,'%Y%m')
              )            -- 两个时间交叉
    ) x
    where x.rn = 1
    ;
end if;



if @assess_pd = 3 or @assess_pd = 0 then  -- 半年度 
    insert into tmp_global_appraisal_mqh(year_months,assess_pd,dept_appraisal_id,user_id,user_code,user_name,register_date,cancel_date,dept_id,dept_appraisal_right)
    select @assess_year_month year_months,3 assess_pd
          ,dept_appraisal_id,user_id,user_code,user_name,register_date,cancel_date,dept_id,dept_appraisal_right
    from (
        select 
            row_number() over(partition by dept_id,user_id order by register_date desc) rn 
            ,dept_appraisal_id,user_id,user_code,user_name,register_date,cancel_date,dept_id,dept_appraisal_right
        from tmp_global_appraisal
        where (
                    date_format(register_date,'%Y%m') <= concat(@assess_year,lpad(@half_year_end,2,'0')) 
                and concat(@assess_year,lpad(@half_year_start,2,'0')) <= date_format(cancel_date,'%Y%m')
              )            -- 两个时间交叉
    ) x
    where x.rn = 1
    ;
end if;






/***************************** 考核方案的限制 ************************************/
-- 写步骤日志
set @step_info = '3.限制考核方案:只有用户考核期在考核方案有效期内，才进行方案列定';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);


-- 创建临时表
drop TEMPORARY TABLE if exists `tmp_assess_dtl_statistics`;
CREATE TEMPORARY TABLE `tmp_assess_dtl_statistics` (
  `year_months` varchar(6) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT '' COMMENT '年月',
  `id` bigint DEFAULT '0' COMMENT '考核方案明细id(主键)',
  `par_year` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '年份',
  `dept_id` bigint DEFAULT NULL COMMENT '部门编码',
  `user_id` bigint DEFAULT NULL COMMENT '人员ID',
  `assess_pd` int DEFAULT NULL COMMENT '考核周期',
  `dept_appraisal_id` bigint DEFAULT NULL COMMENT '考核人员表id',
  `dept_appraisal_right` decimal(10,2) DEFAULT NULL COMMENT '部门考核权重',
  `assess_group_id` bigint DEFAULT NULL COMMENT '所属考核组ID',
  `directsuperior_id` bigint DEFAULT NULL COMMENT '直接汇报上级ID',
  `emp_class` bigint DEFAULT NULL COMMENT '适用人群类别(1-前台员工 2-中后台员工)',
  `status` bigint DEFAULT NULL COMMENT '状态(1未审批；2审批中；3审批失败；4正常；5注销)',
  `effect_time` datetime DEFAULT NULL COMMENT '生效时间',
  `Logoff_time` datetime DEFAULT NULL COMMENT '注销时间',
  `creator` bigint DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updater` bigint DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '最近一次修改时间',
  `deleted` int DEFAULT '0' COMMENT '删除标识(0 有效 1删除)',
  `config_way` int DEFAULT NULL COMMENT '配置方式:1年初一次;2年中入职;3考核组转换',
  `source_way` int DEFAULT NULL COMMENT '第一次生成方案的来源:1年初批量;2年中入职',
  `remark` varchar(500) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  KEY `lh_4` (`year_months`,`assess_pd`,`dept_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='考核方案临时表'
;


-- 月度数据的处理
if @dept_id > 0 then 
    if @assess_pd = 1 or @assess_pd = 0 then  -- 月度
        insert into tmp_assess_dtl_statistics(year_months,id,par_year,dept_id,user_id,assess_group_id,directsuperior_id,assess_pd,emp_class,status,effect_time,Logoff_time,creator,create_time,updater,update_time,config_way,source_way)
        select 
            @assess_year_month year_months
            ,t1.id,t1.par_year,t1.dept_id,t1.user_id,t1.assess_group_id,t1.directsuperior_id,t1.assess_pd,t1.emp_class,t1.status,t1.effect_time,t1.Logoff_time,t1.creator,t1.create_time
            ,t1.updater,t1.update_time,t1.config_way,t1.source_way
        from t_assess_dtl t1
        where t1.deleted = 0 -- 剔除删除的考核方案
        and t1.status in (4,5)   -- assess_dtl_status员工考核明细状态(1:未审批|2:审批中|3:审批失败|4:正常|5:注销)
        and t1.par_year = @assess_year
        and @assess_month between month(t1.effect_time) and month(t1.Logoff_time)
        and t1.effect_time <> t1.Logoff_time   -- 这种数据为注销
        and t1.assess_pd = 1  -- 月度
        and t1.dept_id = @dept_id  -- 指定部门
        ; 
    end if; 
else 
    if @assess_pd = 1 or @assess_pd = 0 then  -- 月度
        insert into tmp_assess_dtl_statistics(year_months,id,par_year,dept_id,user_id,assess_group_id,directsuperior_id,assess_pd,emp_class,status,effect_time,Logoff_time,creator,create_time,updater,update_time,config_way,source_way)
        select 
            @assess_year_month year_months
            ,t1.id,t1.par_year,t1.dept_id,t1.user_id,t1.assess_group_id,t1.directsuperior_id,t1.assess_pd,t1.emp_class,t1.status,t1.effect_time,t1.Logoff_time,t1.creator,t1.create_time
            ,t1.updater,t1.update_time,t1.config_way,t1.source_way
        from t_assess_dtl t1
        where t1.deleted = 0 -- 剔除删除的考核方案
        and t1.status in (4,5)   -- assess_dtl_status员工考核明细状态(1:未审批|2:审批中|3:审批失败|4:正常|5:注销)
        and t1.par_year = @assess_year
        and @assess_month between month(t1.effect_time) and month(t1.Logoff_time)
        and t1.effect_time <> t1.Logoff_time   -- 这种数据为注销
        and t1.assess_pd = 1  -- 月度
        ; 
    end if; 
end if; 


-- 季度数据的处理
if @dept_id > 0 then 
    if @assess_pd = 2 or @assess_pd = 0 then  -- 季度 
        insert into tmp_assess_dtl_statistics(year_months,id,par_year,dept_id,user_id,assess_group_id,directsuperior_id,assess_pd,emp_class,status,effect_time,Logoff_time,creator,create_time,updater,update_time,config_way,source_way)
        select 
            @assess_year_month year_months
            ,t1.id,t1.par_year,t1.dept_id,t1.user_id,t1.assess_group_id,t1.directsuperior_id,t1.assess_pd,t1.emp_class,t1.status,t1.effect_time,t1.Logoff_time,t1.creator,t1.create_time
            ,t1.updater,t1.update_time,t1.config_way,t1.source_way
        from t_assess_dtl t1
        where t1.deleted = 0 -- 剔除删除的考核方案
        and t1.status in (4,5)   -- assess_dtl_status员工考核明细状态(1:未审批|2:审批中|3:审批失败|4:正常|5:注销)
        and t1.par_year = @assess_year
        and (month(t1.effect_time) <= @quarter_end  and @quarter_start <= month(t1.Logoff_time))  -- 两个时间交叉
        and t1.effect_time <> t1.Logoff_time   -- 这种数据为注销
        and t1.assess_pd = 2  -- 季度
        and t1.dept_id = @dept_id  -- 指定部门
        ; 
    end if; 
else 
    if @assess_pd = 2 or @assess_pd = 0 then  -- 季度 
        insert into tmp_assess_dtl_statistics(year_months,id,par_year,dept_id,user_id,assess_group_id,directsuperior_id,assess_pd,emp_class,status,effect_time,Logoff_time,creator,create_time,updater,update_time,config_way,source_way)
        select 
            @assess_year_month year_months
            ,t1.id,t1.par_year,t1.dept_id,t1.user_id,t1.assess_group_id,t1.directsuperior_id,t1.assess_pd,t1.emp_class,t1.status,t1.effect_time,t1.Logoff_time,t1.creator,t1.create_time
            ,t1.updater,t1.update_time,t1.config_way,t1.source_way
        from t_assess_dtl t1
        where t1.deleted = 0 -- 剔除删除的考核方案
        and t1.status in (4,5)   -- assess_dtl_status员工考核明细状态(1:未审批|2:审批中|3:审批失败|4:正常|5:注销)
        and t1.par_year = @assess_year
        and (month(t1.effect_time) <= @quarter_end  and @quarter_start <= month(t1.Logoff_time))  -- 两个时间交叉
        and t1.effect_time <> t1.Logoff_time   -- 这种数据为注销
        and t1.assess_pd = 2  -- 季度
        ; 
    end if; 
end if; 



-- 半年数据的处理
if @dept_id > 0 then 
    if @assess_pd = 3 or @assess_pd = 0 then  -- 半年度 
        insert into tmp_assess_dtl_statistics(year_months,id,par_year,dept_id,user_id,assess_group_id,directsuperior_id,assess_pd,emp_class,status,effect_time,Logoff_time,creator,create_time,updater,update_time,config_way,source_way)
        select 
            @assess_year_month year_months
            ,t1.id,t1.par_year,t1.dept_id,t1.user_id,t1.assess_group_id,t1.directsuperior_id,t1.assess_pd,t1.emp_class,t1.status,t1.effect_time,t1.Logoff_time,t1.creator,t1.create_time
            ,t1.updater,t1.update_time,t1.config_way,t1.source_way
        from t_assess_dtl t1
        where t1.deleted = 0 -- 剔除删除的考核方案
        and t1.status in (4,5)   -- assess_dtl_status员工考核明细状态(1:未审批|2:审批中|3:审批失败|4:正常|5:注销)
        and t1.par_year = @assess_year
        and (month(t1.effect_time) <= @half_year_end  and @half_year_start <= month(t1.Logoff_time))  -- 两个时间交叉
        and t1.effect_time <> t1.Logoff_time   -- 这种数据为注销
        and t1.assess_pd = 3  -- 半年度
        and t1.dept_id = @dept_id  -- 指定部门
        ; 
    end if; 
else 
    if @assess_pd = 3 or @assess_pd = 0 then  -- 半年度 
        insert into tmp_assess_dtl_statistics(year_months,id,par_year,dept_id,user_id,assess_group_id,directsuperior_id,assess_pd,emp_class,status,effect_time,Logoff_time,creator,create_time,updater,update_time,config_way,source_way)
        select 
            @assess_year_month year_months
            ,t1.id,t1.par_year,t1.dept_id,t1.user_id,t1.assess_group_id,t1.directsuperior_id,t1.assess_pd,t1.emp_class,t1.status,t1.effect_time,t1.Logoff_time,t1.creator,t1.create_time
            ,t1.updater,t1.update_time,t1.config_way,t1.source_way
        from t_assess_dtl t1
        where t1.deleted = 0 -- 剔除删除的考核方案
        and t1.status in (4,5)   -- assess_dtl_status员工考核明细状态(1:未审批|2:审批中|3:审批失败|4:正常|5:注销)
        and t1.par_year = @assess_year
        and (month(t1.effect_time) <= @half_year_end  and @half_year_start <= month(t1.Logoff_time))  -- 两个时间交叉
        and t1.effect_time <> t1.Logoff_time   -- 这种数据为注销
        and t1.assess_pd = 3  -- 半年度
        ; 
    end if; 
end if;



-- 剔除情况1：删除没有考核权的考核方案
update tmp_assess_dtl_statistics a 
left join tmp_global_appraisal_mqh b 
    on a.year_months = b.year_months
    and a.assess_pd = b.assess_pd
    and a.dept_id = b.dept_id
    and a.user_id = b.user_id
set a.dept_appraisal_id = b.dept_appraisal_id
    ,a.dept_appraisal_right = b.dept_appraisal_right
    ,a.deleted = if(b.year_months is not null,0,1)
    ,a.remark = if(b.year_months is not null,'正常数据','该记录无效(考核期内均没有考核权)')
;


-- 剔除情况2：如果失效月份小于当前考核月份，则记为无效
update tmp_assess_dtl_statistics a 
set a.deleted = 1
    ,a.remark = case when a.remark = '正常数据' then '该记录无效(考核方案最后时点没落在考核周期上)'
                else '该记录无效(考核期内均没有考核权,且考核方案最后时点没落在考核周期上)'
                end
where month(a.Logoff_time) < cast(right(year_months,2) as unsigned)
;


-- 写步骤日志
set @step_info = '4.列定数据:根据传入的考核周期、考核部门来列定数据';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);


-- 共有4中可能性
-- select @dept_id,@assess_pd;
if @assess_pd > 0 then 
    if @dept_id > 0 then 
        delete from t_assess_info_his where year_months = @assess_year_month and dept_id = @dept_id and assess_pd = @assess_pd; 
        insert into t_assess_info_his(year_months,assess_dtl_id,assess_dtl_idx_id,user_id,dept_id,par_year,dept_appraisal_id,dept_appraisal_right,assess_pd,assess_pd_value
            ,current_tgt,index_code,index_name,source_index_code
            ,appraisal_right,index_unit,index_source,supply_frequency,supply_cycle,business_type,assess_group_id,assess_model)
        select 
             @assess_year_month       year_months             -- 年月
            ,t1.id                    assess_dtl_id           -- 考核方案id
            ,t2.id                    assess_dtl_idx_id       -- 考核方案明细id
            ,t1.user_id               user_id                 -- 人员id
            ,t1.dept_id               dept_id                 -- 考核部门id
            ,t1.par_year              par_year                -- 考核年份 
            ,t1.dept_appraisal_id     dept_appraisal_id       -- 考核权成员表id
            ,t1.dept_appraisal_right  dept_appraisal_right    -- 考核部门权重
            ,t1.assess_pd             assess_pd               -- 考核周期(1:月度|2:季度|3:半年度)
            ,m.assess_pd_value        assess_pd_value         -- 考核周期值(M1-M12,Q1..)
            ,if(ip.business_type = 6, t2.annual_tgt, t3.current_tgt)    current_tgt             -- 考核阶段目标值
            ,t2.idx_code              index_code              -- 指标代码 
            ,ip.index_name            index_name              -- 指标名称
            ,ip.related_index_code    source_index_code       -- 源系统指标代码
            ,t2.appraisal_right       appraisal_right         -- 指标考核权重
            ,ip.index_unit            index_unit              -- 指标单位(1-13)
            ,ip.index_source          index_source            -- 指标来源
            ,ip.supply_frequency      supply_frequency        -- 供数模式(如果是源系统，取源系统供数模式) (1:当期|2:累计|3:时点)
            ,ip.supply_cycle          supply_cycle            -- 供数周期(1:每日|2:每月|3:每季度|4:每半年|5:每年)
            ,ip.business_type         business_type           -- 指标类别(1-5:定量指标|6:定性指标)
            ,t1.assess_group_id       assess_group_id         -- 考核组id
            ,t2.assess_model          assess_model            -- 考核模式(1:当期考核|2:累计考核|3:时点考核)
        from tmp_assess_dtl_statistics t1
        left join t_assess_dtl_idx t2
            on t1.id = t2.assess_dtl_id
        left join t_assess_dtl_idx_tgt t3
            on t2.id = t3.assess_dtl_idx_id
        left join t_dict_assess_pd_mqh m  -- 定性指标(assess_pd和assess_month是唯一)
            on m.assess_pd = @assess_pd
            and m.assess_month = @assess_month
        left join t_index_pool ip
            on t2.idx_code = ip.index_code 
        where t1.deleted = 0 -- 剔除无考核权的考核方案
        and t1.status in (4,5)   -- assess_dtl_status员工考核明细状态(1:未审批|2:审批中|3:审批失败|4:正常|5:注销)
        and ip.deleted = 0  -- 剔除删除的指标
        -- and ip.business_type between 1 and 6  -- 定量指标/定性指标
        and t1.par_year = @assess_year  -- 当年
        and t1.year_months = @assess_year_month -- 当月
        -- 根据当前考核月份，算出当前考核期需要考核的周期
        and (
                   ((t3.assess_pd_val between 7 and 18) and t3.assess_pd_val-6 = @assess_month) -- 月份，每月都执行
                or ((t3.assess_pd_val between 3 and 6)  and (t3.assess_pd_val-2) * 3 = @assess_month)  -- 季度，3/6/9/12月执行
                or ((t3.assess_pd_val between 1 and 2) and t3.assess_pd_val * 6 = @assess_month) -- 年度，6/12月执行
                or ip.business_type = 6  -- 定性指标
            )
                and m.assess_pd_value is not null
        and t1.dept_id = @dept_id
        and t1.assess_pd = @assess_pd
        ; 
    else 
        delete from t_assess_info_his where year_months = @assess_year_month and assess_pd = @assess_pd; 
        insert into t_assess_info_his(year_months,assess_dtl_id,assess_dtl_idx_id,user_id,dept_id,par_year,dept_appraisal_id,dept_appraisal_right,assess_pd,assess_pd_value
            ,current_tgt,index_code,index_name,source_index_code
            ,appraisal_right,index_unit,index_source,supply_frequency,supply_cycle,business_type,assess_group_id,assess_model)
        select 
             @assess_year_month       year_months             -- 年月
            ,t1.id                    assess_dtl_id           -- 考核方案id
            ,t2.id                    assess_dtl_idx_id       -- 考核方案明细id
            ,t1.user_id               user_id                 -- 人员id
            ,t1.dept_id               dept_id                 -- 考核部门id
            ,t1.par_year              par_year                -- 考核年份 
            ,t1.dept_appraisal_id     dept_appraisal_id       -- 考核权成员表id
            ,t1.dept_appraisal_right  dept_appraisal_right    -- 考核部门权重
            ,t1.assess_pd             assess_pd               -- 考核周期(1:月度|2:季度|3:半年度)
            ,m.assess_pd_value        assess_pd_value         -- 考核周期值(M1-M12,Q1..)
            ,if(ip.business_type = 6, t2.annual_tgt, t3.current_tgt)    current_tgt             -- 考核阶段目标值
            ,t2.idx_code              index_code              -- 指标代码 
            ,ip.index_name            index_name              -- 指标名称
            ,ip.related_index_code    source_index_code       -- 源系统指标代码
            ,t2.appraisal_right       appraisal_right         -- 指标考核权重
            ,ip.index_unit            index_unit              -- 指标单位(1-13)
            ,ip.index_source          index_source            -- 指标来源
            ,ip.supply_frequency      supply_frequency        -- 供数模式(如果是源系统，取源系统供数模式) (1:当期|2:累计|3:时点)
            ,ip.supply_cycle          supply_cycle            -- 供数周期(1:每日|2:每月|3:每季度|4:每半年|5:每年)
            ,ip.business_type         business_type           -- 指标类别(1-5:定量指标|6:定性指标)
            ,t1.assess_group_id       assess_group_id         -- 考核组id
            ,t2.assess_model          assess_model            -- 考核模式(1:当期考核|2:累计考核|3:时点考核)
        from tmp_assess_dtl_statistics t1
        left join t_assess_dtl_idx t2
            on t1.id = t2.assess_dtl_id
        left join t_assess_dtl_idx_tgt t3
            on t2.id = t3.assess_dtl_idx_id
        left join t_dict_assess_pd_mqh m  -- 定性指标(assess_pd和assess_month是唯一)
            on m.assess_pd = @assess_pd
            and m.assess_month = @assess_month
        left join t_index_pool ip
            on t2.idx_code = ip.index_code 
        where t1.deleted = 0 -- 剔除无考核权的考核方案
        and t1.status in (4,5)   -- assess_dtl_status员工考核明细状态(1:未审批|2:审批中|3:审批失败|4:正常|5:注销)
        and ip.deleted = 0  -- 剔除删除的指标
        -- and ip.business_type between 1 and 6  -- 定量指标/定性指标
        and t1.par_year = @assess_year  -- 当年
        and t1.year_months = @assess_year_month -- 当月
        and (
                   ((t3.assess_pd_val between 7 and 18) and t3.assess_pd_val-6 = @assess_month) -- 月份，每月都执行
                or ((t3.assess_pd_val between 3 and 6)  and (t3.assess_pd_val-2) * 3 = @assess_month)  -- 季度，3/6/9/12月执行
                or ((t3.assess_pd_val between 1 and 2) and t3.assess_pd_val * 6 = @assess_month) -- 年度，6/12月执行
                or ip.business_type = 6  -- 定性指标
            )
                and m.assess_pd_value is not null
        and t1.assess_pd = @assess_pd
        ; 
    end if; 
else 
    if @dept_id > 0 then 
        delete from t_assess_info_his where year_months = @assess_year_month and dept_id = @dept_id ; 
        insert into t_assess_info_his(year_months,assess_dtl_id,assess_dtl_idx_id,user_id,dept_id,par_year,dept_appraisal_id,dept_appraisal_right,assess_pd,assess_pd_value
            ,current_tgt,index_code,index_name,source_index_code
            ,appraisal_right,index_unit,index_source,supply_frequency,supply_cycle,business_type,assess_group_id,assess_model)
        select 
            distinct @assess_year_month       year_months             -- 年月
            ,t1.id                    assess_dtl_id           -- 考核方案id
            ,t2.id                    assess_dtl_idx_id       -- 考核方案明细id
            ,t1.user_id               user_id                 -- 人员id
            ,t1.dept_id               dept_id                 -- 考核部门id
            ,t1.par_year              par_year                -- 考核年份 
            ,t1.dept_appraisal_id     dept_appraisal_id       -- 考核权成员表id
            ,t1.dept_appraisal_right  dept_appraisal_right    -- 考核部门权重
            ,t1.assess_pd             assess_pd               -- 考核周期(1:月度|2:季度|3:半年度)
            ,m.assess_pd_value        assess_pd_value         -- 考核周期值(M1-M12,Q1..)
            ,if(ip.business_type = 6, t2.annual_tgt, t3.current_tgt)    current_tgt             -- 考核阶段目标值
            ,t2.idx_code              index_code              -- 指标代码 
            ,ip.index_name            index_name              -- 指标名称
            ,ip.related_index_code    source_index_code       -- 源系统指标代码
            ,t2.appraisal_right       appraisal_right         -- 指标考核权重
            ,ip.index_unit            index_unit              -- 指标单位(1-13)
            ,ip.index_source          index_source            -- 指标来源
            ,ip.supply_frequency      supply_frequency        -- 供数模式(如果是源系统，取源系统供数模式) (1:当期|2:累计|3:时点)
            ,ip.supply_cycle          supply_cycle            -- 供数周期(1:每日|2:每月|3:每季度|4:每半年|5:每年)
            ,ip.business_type         business_type           -- 指标类别(1-5:定量指标|6:定性指标)
            ,t1.assess_group_id       assess_group_id         -- 考核组id
            ,t2.assess_model          assess_model            -- 考核模式(1:当期考核|2:累计考核|3:时点考核)
        from tmp_assess_dtl_statistics t1
        left join t_assess_dtl_idx t2
            on t1.id = t2.assess_dtl_id
        left join t_assess_dtl_idx_tgt t3
            on t2.id = t3.assess_dtl_idx_id
        left join t_dict_assess_pd_mqh m  -- 定性指标(assess_pd和assess_month是唯一)；按月、季、半年，这里最多关联出3条冗余数据
            on m.assess_pd = t1.assess_pd
                        and m.assess_month = @assess_month
        left join t_index_pool ip
            on t2.idx_code = ip.index_code 
        where t1.deleted = 0 -- 剔除无考核权的考核方案
        and t1.status in (4,5)   -- assess_dtl_status员工考核明细状态(1:未审批|2:审批中|3:审批失败|4:正常|5:注销)
        and ip.deleted = 0  -- 剔除删除的指标
        -- and ip.business_type between 1 and 6  -- 定量指标/定性指标
        and t1.par_year = @assess_year  -- 当年
        and t1.year_months = @assess_year_month -- 当月
        -- 根据当前考核月份，算出当前考核期需要考核的周期
        and (
                   ((t3.assess_pd_val between 7 and 18) and t3.assess_pd_val-6 = @assess_month) -- 月份，每月都执行
                or ((t3.assess_pd_val between 3 and 6)  and (t3.assess_pd_val-2) * 3 = @assess_month)  -- 季度，3/6/9/12月执行
                or ((t3.assess_pd_val between 1 and 2) and t3.assess_pd_val * 6 = @assess_month) -- 年度，6/12月执行
                or ip.business_type = 6  -- 定性指标
            )
              and m.assess_pd_value is not null
        and t1.dept_id = @dept_id
        ; 
    else 
        -- 这里每天的初始化，只删除当期未开启考核的数据
        delete a from t_assess_info_his a 
        inner join t_assess_dept_progress b 
            on a.par_year = b.par_year
            and a.dept_id = b.dept_id
            and a.assess_pd_value = b.assess_pd_value
            and b.status = 0  -- 未开启考核
        where a.year_months = @assess_year_month
        ; 
        
        -- 插入未考核的数据
        insert into t_assess_info_his_111(year_months,assess_dtl_id,assess_dtl_idx_id,user_id,dept_id,par_year,dept_appraisal_id,dept_appraisal_right,assess_pd,assess_pd_value
            ,current_tgt,index_code,index_name,source_index_code
            ,appraisal_right,index_unit,index_source,supply_frequency,supply_cycle,business_type,assess_group_id,assess_model)
        select 
             distinct @assess_year_month       year_months             -- 年月
            ,t1.id                    assess_dtl_id           -- 考核方案id
            ,t2.id                    assess_dtl_idx_id       -- 考核方案明细id
            ,t1.user_id               user_id                 -- 人员id
            ,t1.dept_id               dept_id                 -- 考核部门id
            ,t1.par_year              par_year                -- 考核年份 
            ,t1.dept_appraisal_id     dept_appraisal_id       -- 考核权成员表id
            ,t1.dept_appraisal_right  dept_appraisal_right    -- 考核部门权重
            ,t1.assess_pd             assess_pd               -- 考核周期(1:月度|2:季度|3:半年度)
            ,m.assess_pd_value        assess_pd_value         -- 考核周期值(M1-M12,Q1..)
            ,if(ip.business_type = 6, t2.annual_tgt, t3.current_tgt)    current_tgt             -- 考核阶段目标值
            ,t2.idx_code              index_code              -- 指标代码 
            ,ip.index_name            index_name              -- 指标名称
            ,ip.related_index_code    source_index_code       -- 源系统指标代码
            ,t2.appraisal_right       appraisal_right         -- 指标考核权重
            ,ip.index_unit            index_unit              -- 指标单位(1-13)
            ,ip.index_source          index_source            -- 指标来源
            ,ip.supply_frequency      supply_frequency        -- 供数模式(如果是源系统，取源系统供数模式) (1:当期|2:累计|3:时点)
            ,ip.supply_cycle          supply_cycle            -- 供数周期(1:每日|2:每月|3:每季度|4:每半年|5:每年)
            ,ip.business_type         business_type           -- 指标类别(1-5:定量指标|6:定性指标)
            ,t1.assess_group_id       assess_group_id         -- 考核组id
            ,t2.assess_model          assess_model            -- 考核模式(1:当期考核|2:累计考核|3:时点考核)
        from tmp_assess_dtl_statistics t1
        left join t_assess_dtl_idx t2
            on t1.id = t2.assess_dtl_id
        left join t_assess_dtl_idx_tgt t3
            on t2.id = t3.assess_dtl_idx_id
        left join t_dict_assess_pd_mqh m  -- 定性指标(assess_pd和assess_month是唯一)；按月、季、半年，这里最多关联出3条冗余数据
            on m.assess_pd = t1.assess_pd
                        and m.assess_month = @assess_month
        left join t_index_pool ip
            on t2.idx_code = ip.index_code 
        inner join t_assess_dept_progress b 
            on t1.par_year = b.par_year
            and t1.dept_id = b.dept_id
            and m.assess_pd_value = b.assess_pd_value
            and b.status = 0  -- 未开启考核
        where t1.deleted = 0 -- 剔除无考核权的考核方案
        and t1.status in (4,5)   -- assess_dtl_status员工考核明细状态(1:未审批|2:审批中|3:审批失败|4:正常|5:注销)
        and ip.deleted = 0  -- 剔除删除的指标
        -- and ip.business_type between 1 and 6  -- 定量指标/定性指标
        and t1.par_year = @assess_year  -- 当年
        and t1.year_months = @assess_year_month -- 当月
        and (
                   ((t3.assess_pd_val between 7 and 18) and t3.assess_pd_val-6 = @assess_month) -- 月份，每月都执行
                or ((t3.assess_pd_val between 3 and 6)  and (t3.assess_pd_val-2) * 3 = @assess_month)  -- 季度，3/6/9/12月执行
                or ((t3.assess_pd_val between 1 and 2) and t3.assess_pd_val * 6 = @assess_month) -- 年度，6/12月执行
                or ip.business_type = 6  -- 定性指标
            )
                and m.assess_pd_value is not null
        ; 
    end if
    ; 
end if; 



-- 写步骤日志
set @step_info = '5.更新中文字段:根据字典表更新码值';
set @remark = '指标来源_中文 该中文不会被前台应用掉，仅作为数据展示';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);
set @remark = '';

-- 更新指标来源_中文，根据字典表
update t_assess_info_his a
inner join sys_dict_data b 
on b.dict_type = 'index_source'
    and a.index_source = b.dict_value
set a.index_source_cn2 = b.dict_label
where a.year_months = @assess_year_month
; 


-- 获得记录数
select count(1) cnt into V_TABLE_CNT from t_assess_info_his; 


-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
; END
;