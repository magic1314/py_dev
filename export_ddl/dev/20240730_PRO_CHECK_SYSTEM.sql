DROP PROCEDURE IF EXISTS PRO_CHECK_SYSTEM;
CREATE PROCEDURE `PRO_CHECK_SYSTEM`(IN IN_DATA_DATE INT)
label:BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_CHECK_SYSTEM
         功能简述：   监控一体化
         版本：       V1.6
         参数：       IN_DATA_DATE   数据日期(默认取最新日期)
         注意事项：
                 1、程序中不用用到IN_DATA_DATE参数的内容
                 2、check_id区间
                    100-199：Part1: 调度任务的监控
                    200-399：Part2: 配置类小表监控
                    400-899：Part3: 业务数据类监控
                    900-999：Part4: 系统级别监控
         数据源：
                 t_check_config  监控配置表
         结果表：
                 t_check_log     监控日志表
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG          2023/10/17                 创建
         MG          2023/10/30                 脚本优化
         MG          2023/11/03                 支持监控配置表check_sql执行多条记录sql
         MG          2023/11/13                 新增系统级别监控
         MG          2023/11/15                 执行顺序按照step_num字段
         MG          2023/12/18                 将check_sql中${check_id}替换为当前记录的check_id
                                                为了让代码重复性变低，代码逻辑不再区分check_id区间，自主分配
         MG          2024/07/23                 仅修改备注信息
******************************************************************************************************************************************/

-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);
DECLARE V_BATCH_NO varchar(200);
DECLARE V_TOTAL_STEP_NUM int;
DECLARE loop_i INT DEFAULT 1;
DECLARE input_string VARCHAR(4000);
DECLARE delimiter CHAR(1);
DECLARE array_length INT;
DECLARE i INT;
DECLARE start_locate INT;
DECLARE end_locate INT;


SET delimiter = ';';
-- SET input_string = concat(TRIM(TRAILING ';' FROM TRIM(' ' FROM 'SELECT * FROM t1; delete from t2;update t3 set age = 100')),';');
-- SET array_length = (SELECT LENGTH(input_string) - LENGTH(REPLACE(input_string, delimiter, '')));






-- 设置变量
set V_EVENT_NAME = '监控一体化';
set V_TOTAL_STEP_NUM = 2;  -- 设置总步骤数
set @remark = ''; -- 步骤日志中的备注，可以每步都定义
select now() into V_START_TIME;


set V_RUN_COMMAND = concat('call PRO_CHECK_SYSTEM('
                            ,IN_DATA_DATE
                            ,')'
                         );

set V_BATCH_NO = concat(   UNIX_TIMESTAMP(),'-'
                        ,replace(V_RUN_COMMAND,'call ',''),'-'
                        ,V_TOTAL_STEP_NUM
                     );

-- 写步骤日志
set @step_info = '1.参数定义&基础检查:正常';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);




/********************* 监控任务检查 *********************/
-- 写步骤日志
set @step_info = '2.监控任务检查';
set @remark = '检查所有的有效监控';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);
set @remark = '';

drop TEMPORARY table if exists tmp_check_config;
create TEMPORARY table tmp_check_config
select 
    row_number() over(order by check_id) seq,check_id,check_sql
from t_check_config a
where a.program_type = 'procedure'
and a.program_name = 'PRO_CHECK_SYSTEM'
and a.status = 0
order by a.check_id asc
;


select count(1) into @line_num from tmp_check_config;

SET loop_i = 1;
WHILE loop_i <= @line_num DO
    select count(1) into @cnt from tmp_check_config where seq = loop_i;
    IF @cnt = 0 THEN 
        SELECT 'No Record' INTO @info;
    ELSE
        -- 获取字段值
        select check_sql,check_id into @tmp_check_sql,@check_id from tmp_check_config where seq = loop_i;
        
        SET @check_sql = replace(@tmp_check_sql, '${check_id}', @check_id);
        SET input_string = concat(TRIM(TRAILING delimiter FROM TRIM(' ' FROM @check_sql)),delimiter);
        SET array_length = (SELECT LENGTH(input_string) - LENGTH(REPLACE(input_string, delimiter, '')));
        SET i = 1;
        SET start_locate = 1;
        
        -- select @tmp_check_sql, @check_id;
        
        WHILE i <= array_length DO
            -- 分号的位置 17
            SET end_locate = LOCATE(delimiter,input_string);
            
            -- SELECT * FROM t1;
            SET @execute_sql = SUBSTR(input_string,start_locate,end_locate);
            
            -- select concat(
            --     " 序号：",i
            --     ,"\n 共有sql数量：", array_length
            --     ,"\n 目前的sql：", input_string
            --     ,'\n ',"需要截取的起始和结束位置：", start_locate, ':', end_locate
            --     ,'\n ',"执行的sql：", @execute_sql
            --     ,'\n ',"剩下的sql：", SUBSTR(input_string,end_locate+1)
            -- ) 提醒信息;
            
            -- 【 delete from t2;update t3 set age = 100】
            SET input_string = SUBSTR(input_string,end_locate + 1);
            
            -- 预处理需要执行的动态SQL，其中stmt是一个变量
            PREPARE stmt FROM @execute_sql;
            -- 执行SQL语句
            EXECUTE stmt;
            -- 释放掉预处理段
            DEALLOCATE PREPARE stmt;
            
            SET i = i + 1;
        END WHILE;
        
    END IF;
    SET loop_i = loop_i + 1;
END WHILE
;



-- 获得记录数
select 0 cnt into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;