DROP PROCEDURE IF EXISTS PRO_LOAD_HR_ORG;
CREATE PROCEDURE `PRO_LOAD_HR_ORG`(IN BIZ_DATE DATE)
label:BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_LOAD_HR_ORG
         功能简述：   组织信息表转换
         参数：       BIZ_DATE   数据日期
         注意事项：
         数据源：
                 int_d_new_hrs_org    int组织信息表
         结果表：
                 sys_dept             部门表
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         CZB         2023/02/14                 创建
         MG          2023/06/02                 更新卫星营业部父节点及祖级列表;西部证券分公司下挂分公司
         MG          2023/07/05                 int表数据改变，dept表相应的逻辑调整
         MG          2023/07/12                 用递归的方式更新ancestors字段
         MG          2023/12/05                 相同的部门编码不会重复插入
         MG          2024/02/28                 新增部门拉链表
         MG          2024/03/14                 重新启动该存储过程
         MG          2024/03/18                 上级部门编码会变更
         MG          2024/03/19                 撤销部门的限制暂时不生效
         MG          2024/04/18                 新增注销日期，调整状态的逻辑
******************************************************************************************************************************************/

DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);
-- 设置变量
set V_EVENT_NAME = '组织信息表转换';
set V_RUN_COMMAND = concat('call PRO_LOAD_HR_ORG(',BIZ_DATE,')');
select now() into V_START_TIME;

-- 新增部门写入目标表
insert into sys_dept
(dept_id, -- 部门主键
 parent_id, -- 父部门id
 ancestors, -- 祖级列表
 dept_name, -- 部门名称
 order_num, -- 显示顺序
 leader, -- 负责人
 phone, -- 联系电话
 email, -- 邮箱
 status, -- 部门状态（0正常 1停用）
 create_by, -- 创建者
 create_time, -- 创建时间
 update_by, -- 更新者
 update_time, -- 更新时间
 dept_no, -- 部门编号
 remark, -- 备注
 dept_type, -- 部门层级 0总部 1营业部
 dept_simple_name, -- 部门简称
 dept_address, -- 部门地址
 del_flag, -- 删除标志（0代表存在 2代表删除）
 org_type, -- 组织类型代码(4总部及下挂部门  6分公司  2营业部)
 sys_id, -- 源系统唯一id
 fic_org, -- 是否卫星营业部   0：否 1：是
 center_org_code -- 中心营业部编码
 )
select replace(a.org_code, 'ZQ', '9') dept_id, -- 组织编码
       replace(ifnull(a.Center_Org_Code,a.father_org), 'ZQ', '9') parent_id, -- 上级组织编码
       null ancestors, --
       a.org_name dept_name, -- 组织名称
       1 order_num, -- 显示顺序
       null leader, -- 负责人
       null phone, -- 联系电话
       null email, -- 邮箱
       0 status, -- 部门状态（0正常 1停用） 源系统:(1未启用2已启用3已停用)
       null create_by, -- 创建者
       a.create_dt create_time, -- 创建时间
       1 update_by, -- 更新者,默认系统管理员
       sysdate() update_time, -- 更新时间，写入系统时间
       a.org_code dept_no, -- 部门编号
       null remark, -- 备注
       0 dept_type, -- 部门层级 0总部 1营业部
       a.org_shortname dept_simple_name, -- 组织简称
       null dept_address, -- 部门地址
       0 del_flag, -- 删除标志(0代表存在 2代表删除),
       null org_type, -- 组织类型代码(4总部及下挂部门  6分公司  2营业部)
       a.pk_org sys_id, -- 源系统组织主键
       ifnull(a.fic_org, 0) fic_org, -- 是否卫星营业部   0：否 1：是
       replace(a.center_org_code, 'ZQ', '9') center_org_code -- 中心营业部编码
from int_d_new_hrs_org a
left join sys_dept b
  on a.pk_org = b.sys_id
left join sys_dept b2
  on replace(a.org_code, 'ZQ', '9') = b2.dept_id
where b.sys_id is null -- 过滤已存在sys_dept的记录
  and b2.sys_id is null -- 部门编码相同不插入
  and (a.dis_flag = 0 or a.dis_flag is null) -- 撤销标识 : 0-未撤销 1-撤销  增加过滤条件撤销标识
  and a.org_code not in ('ZQ000','ZQ0002') -- 机构为：申万宏源集团，节点从申万宏源证券有限公司开始
;

-- 字段内容更新
/*
数据变更：
1. 组织名称、组织简称改变： 支持
2. 组织编码改变：不支持(如果改变所有表的dept_id值都需要同步修改)
3. 上级组织编码改变：支持
4. 中心营业部转卫星：支持
5. 卫星转中心营业部：支持
6. 中心营业部编码改变：支持
*/

update sys_dept a 
inner join int_d_new_hrs_org b 
on a.sys_id = b.pk_org
set -- a.dept_id = replace(b.org_code, 'ZQ', '9'), -- 组织编码（不会主动修改，由于系统涉及变更的内容很多）
    a.parent_id = replace(ifnull(b.Center_Org_Code,b.father_org), 'ZQ', '9'), -- 上级组织编码
    a.dept_name = b.org_name, -- 组织名称
    a.status =(case when b.Dis_Flag = '0' then 0 else 1 end), -- 部门状态（0正常 1已撤销 2隐藏） 源系统:(0 正常，1：已撤销)
    a.create_time = b.create_dt, -- 创建时间
    a.dept_no = b.org_code, -- 部门编号
    a.dept_simple_name = b.org_shortname, -- 组织简称
    a.fic_org = ifnull(b.fic_org, 0), -- 是否卫星营业部   0：否 1：是
    a.center_org_code = replace(b.center_org_code, 'ZQ', '9') -- 中心营业部编码
where b.dis_flag = '0' -- 撤销标识  增加过滤条件撤销标识
  and b.org_code not in ('ZQ000','ZQ0002') -- 机构为：申万宏源集团，节点从申万宏源证券有限公司开始
;

-- 清洗组织类型代码(1总公司 4总公司下挂各总部  6分公司  2营业部)
-- 1总公司
update sys_dept set org_type = 1 where dept_no = 'ZQ0002';

-- 总公司编码暂是改为1000
update sys_dept set dept_id = 1000 where dept_id = 90002;
update sys_dept set parent_id = 1000 where parent_id = 90002;

-- 4总公司下挂各总部
update sys_dept t
   set org_type = 4, order_num = 2,ancestors='0,1000'
where t.parent_id = 1000
and dept_name not like '%分公司%'
and ancestors is null  -- 限定未清洗祖级列表的新纪录
;

-- 6分公司
update sys_dept t
   set org_type = 6, order_num = 3,ancestors='0,1000'
 where
 t.parent_id = 1000
 and dept_name like '%分公司%'
 and ancestors is null  -- 限定未清洗祖级列表的新纪录
 ;

-- 2营业部(默认分配在parent_id下面)
update sys_dept t
   set org_type = 2, order_num = 4,ancestors=CONCAT('0,1000,',parent_id) 
 where
 t.parent_id != 1000
 and org_type is null
 and ancestors is null  -- 限定未清洗祖级列表的新纪录
 ;
-- ancestors字段通过递归生成
drop TEMPORARY table if exists tmp_sys_dept_ancestors;
CREATE TEMPORARY TABLE `tmp_sys_dept_ancestors` (
  `dept_id` bigint COLLATE utf8mb4_general_ci NOT NULL,
  `ancestors` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`dept_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '部门临时表(用于获得ancestors字段)';
insert into tmp_sys_dept_ancestors(dept_id,ancestors)
WITH RECURSIVE cte AS (
  SELECT dept_id, parent_id, CAST(parent_id AS CHAR(50)) AS ancestors
  FROM sys_dept
  WHERE parent_id = 0
  UNION ALL
  SELECT d.dept_id, d.parent_id, CONCAT(c.ancestors, ',', d.parent_id)
  FROM sys_dept d
  JOIN cte c ON d.parent_id = c.dept_id
)
SELECT dept_id, ancestors FROM cte
;

update sys_dept a 
inner join tmp_sys_dept_ancestors b 
on a.dept_id = b.dept_id 
set a.remark = concat("ancestors有变更：",a.ancestors,' -> ',b.ancestors, '-操作时间：',now())
    ,a.ancestors = b.ancestors
    ,a.update_time = now()
where a.ancestors != b.ancestors
;

/************************************************************************************************************************************************
    # 部门拉链表
    # 主键(确定一条记录的标识)：Pk_Psndoc
    # 记录变化字段：Org_Code,Org_Name,Org_Shortname,Father_Org,Create_Dt,Org_Create_Dt,Org_Cancel_Dt,Dept_Level
                    ,Dept_Type,Enablestate,Belong_Group,Dis_Flag,Fic_Org,Center_Org_Code
    
************************************************************************************************************************************************/

/******************* 情况1：部分字段内容发生变化 ***********************/
-- 1、 针对变更的数据(这里只考虑部门),做更新处理:更新结束日期、数据更新时间
update int_d_new_hrs_org_chain chain
inner join int_d_new_hrs_org org 
    on chain.PK_ORG = org.PK_ORG
set chain.end_date = date_sub(current_date, interval 1 day)
    ,chain.current_flag = 2 -- 表示需要后面更新
where chain.current_flag = 1
and ( 
       ifnull(chain.Org_Code       ,'') <> ifnull(org.Org_Code       ,'')
    or ifnull(chain.Org_Name       ,'') <> ifnull(org.Org_Name       ,'')
    or ifnull(chain.Org_Shortname  ,'') <> ifnull(org.Org_Shortname  ,'')
    or ifnull(chain.Father_Org     ,'') <> ifnull(org.Father_Org     ,'')
    or ifnull(chain.Create_Dt      ,'') <> ifnull(org.Create_Dt      ,'')
    or ifnull(chain.Org_Create_Dt  ,'') <> ifnull(org.Org_Create_Dt  ,'')
    or ifnull(chain.Org_Cancel_Dt  ,'') <> ifnull(org.Org_Cancel_Dt  ,'')
    or ifnull(chain.Dept_Level     ,'') <> ifnull(org.Dept_Level     ,'')
    or ifnull(chain.Dept_Type      ,'') <> ifnull(org.Dept_Type      ,'')
    or ifnull(chain.Enablestate    ,'') <> ifnull(org.Enablestate    ,'')
    or ifnull(chain.Belong_Group   ,'') <> ifnull(org.Belong_Group   ,'')
    or ifnull(chain.Dis_Flag       ,'') <> ifnull(org.Dis_Flag       ,'')
    or ifnull(chain.Fic_Org        ,'') <> ifnull(org.Fic_Org        ,'')
    or ifnull(chain.Center_Org_Code,'') <> ifnull(org.Center_Org_Code,'')
)
;

-- 2、针对变更的数据,做新增数据处理  
insert into int_d_new_hrs_org_chain(PK_ORG,current_flag,start_date,end_date,Org_Code,Org_Name,Org_Shortname,Father_Org,Create_Dt
    ,Org_Create_Dt,Org_Cancel_Dt,Dept_Level,Dept_Type,Enablestate,Belong_Group,Dis_Flag,Fic_Org,Center_Org_Code) 
select
    org.PK_ORG
    ,1 current_flag
    ,current_date start_date
    ,'2099-12-31' end_date
    ,org.Org_Code,org.Org_Name,org.Org_Shortname,org.Father_Org,org.Create_Dt
    ,org.Org_Create_Dt,org.Org_Cancel_Dt,org.Dept_Level,org.Dept_Type,org.Enablestate,org.Belong_Group,org.Dis_Flag,org.Fic_Org,org.Center_Org_Code
from int_d_new_hrs_org org
inner join int_d_new_hrs_org_chain chain 
ON chain.PK_ORG = org.PK_ORG
where chain.current_flag = 2
;

-- 3、已更新完成,状态转为无效  
update int_d_new_hrs_org_chain
set current_flag = 0
where current_flag = 2
;

/******************* 情况2：主键新增 ***********************/
insert into int_d_new_hrs_org_chain(PK_ORG,current_flag,start_date,end_date,Org_Code,Org_Name,Org_Shortname,Father_Org,Create_Dt
    ,Org_Create_Dt,Org_Cancel_Dt,Dept_Level,Dept_Type,Enablestate,Belong_Group,Dis_Flag,Fic_Org,Center_Org_Code) 
select
    org.PK_ORG
    ,1 current_flag
    ,current_date start_date
    ,'2099-12-31' end_date
    ,org.Org_Code,org.Org_Name,org.Org_Shortname,org.Father_Org,org.Create_Dt
    ,org.Org_Create_Dt,org.Org_Cancel_Dt,org.Dept_Level,org.Dept_Type,org.Enablestate,org.Belong_Group,org.Dis_Flag,org.Fic_Org,org.Center_Org_Code
from int_d_new_hrs_org org
left join int_d_new_hrs_org_chain chain 
    on chain.PK_ORG = org.PK_ORG
    and chain.current_flag = 1
where chain.PK_ORG is null
;
/******************* 情况3：删除 ***********************/
update int_d_new_hrs_org_chain chain
left join int_d_new_hrs_org org 
    on chain.PK_ORG = org.PK_ORG
set chain.end_date = current_date
    ,chain.current_flag = -1
where chain.current_flag = 1
    and org.PK_ORG is null
;

-- 获得记录数
select count(1) cnt into V_TABLE_CNT from sys_dept;
-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,BIZ_DATE,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0);
END
;