DROP PROCEDURE IF EXISTS PRO_LOAD_CRM_JGZB;
CREATE PROCEDURE `PRO_LOAD_CRM_JGZB`(IN `BIZ_DATE` DATE)
BEGIN
/************************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_LOAD_CRM_JGZB
         功能简述：   人力绩效指标代码表(机构CRM)转换
         参数：
                     BIZ_DATE  上一交易日
         注意事项：
                  1、
         数据源：
                  1、 INT_D_HRP_CRM_JGZB    人力绩效指标代码表(机构CRM)
         目标表：
                  1、 t_index_pool_input    指标池表
                  2、 t_index_pool          单项指标代码表

         修改记录;
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         CZB         2023/02/14                 创建
         MG          2023/02/27                 只更新不删除
         MG          2023/03/02                 默认dept_id=12800
         MG          2023/05/29                 来源系统码值修改
         MG          2023/06/08                 目标表指标代码改为联合字段(原指标代码+供数周期+供数模式+指标单位)
                                                更新语句不再更新联合字段的内容
         MG          2023/06/09                 关联条件问题修改
                                                新增latest_status字段，以index_code对比，如果单位、周期、模式变化，原记录记为1，最近记录记为0
         MG          2023/09/07                 同步更新【单项指标代码表】的指标名称
************************************************************************************************************************************************/


DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);

-- 设置变量
set V_EVENT_NAME = '机构CRM指标代码转换';
set V_RUN_COMMAND = concat('call PRO_LOAD_CRM_JGZB(',BIZ_DATE,')');
select now() into V_START_TIME;



-- 只增更新不删除
-- 1. 插入新数据
insert into t_index_pool_input
(index_name,-- 指标名称
index_code,-- 指标代码
dept_id,-- 部门id
origin_index_code,-- 原始指标代码
origin_index_name,-- 原始指标名称
origin_system,-- 来源系统
status,-- 状态
remark1,-- 备用字段1
remark2,-- 备用字段2
creator,-- 创建人
create_time,-- 创建时间
updater,-- 修改人
update_time,-- 修改时间
deleted,-- 删除标识
supply_cycle,-- 供数周期
supply_frequency,-- 供数模式
index_unit -- 指标单位
)
select 
   a.zbmc                         index_name                  -- 指标名称
  ,concat(ifnull(a.zbdm,''),'_',ifnull(a.tjzq,''),'_',ifnull(a.zbszlx,''),'_',ifnull(a.zbdw,''))
                                  index_code                  -- 指标代码
  ,12800                          dept_id                     -- 部门id   -- 对应【机构客户总部】
  ,a.zbdm                         origin_index_code           -- 原始指标代码
  ,a.zbmc                         origin_index_name           -- 原始指标名称
  ,'2'                            origin_system               -- 来源系统 记录在sys_dict_type表中
  ,a.ZT                           status                      -- 状态
  ,a.BZ                           remark1                     -- 备用字段1
  ,'新增指标'                     remark2                     -- 备用字段2
  ,a.CJR                          creator                     -- 创建人
  ,sysdate()                      create_time                 -- 创建时间
  ,a.ZHXGR                        updater                     -- 修改人
  ,now()                          update_time                 -- 修改时间
  ,0                              deleted                     -- 删除标识
  ,a.tjzq                         supply_cycle                -- 供数周期
  ,a.zbszlx                       supply_frequency            -- 供数模式
  ,a.zbdw                         index_unit                  -- 指标单位
from int_d_hrp_crm_jgzb a
left join t_index_pool_input b 
    on b.origin_system = '2'
    and a.zbdm = b.origin_index_code
    and a.tjzq = b.supply_cycle
    and a.zbszlx = b.supply_frequency
    and ifnull(a.zbdw,'') = ifnull(b.index_unit,'')
where b.index_code is null
;


-- 2. 将历史的【最新状态】改为1
update t_index_pool_input b 
inner join int_d_hrp_crm_jgzb a
    on b.origin_system = '2'
    and a.zbdm = b.origin_index_code
set b.latest_status = 1 -- 最新状态(0:最新，1：历史状态)
    ,b.index_name = a.zbmc -- 指标名称
    ,b.origin_index_name = a.zbmc -- 原始指标名称
    ,b.updater = 1
    ,b.update_time = sysdate()
    ,b.remark2 = '该指标的周期、模式或单位发生了变化，该联合指标值不统计完成值'
where b.latest_status = 0
  and (a.tjzq != b.supply_cycle
       or a.zbszlx != b.supply_frequency
       or ifnull(a.zbdw,'') != ifnull(b.index_unit,'')
       )
;


-- 3. 修改后再恢复
-- 比如 A指标，单位是1，改为2，然后再改为1
update t_index_pool_input b 
inner join int_d_hrp_crm_jgzb a
    on b.origin_system = '2'
    and a.zbdm = b.origin_index_code
set b.latest_status = 0 -- 最新状态(0:最新，1：历史状态)
    ,b.index_name = a.zbmc -- 指标名称
    ,b.origin_index_name = a.zbmc -- 原始指标名称
    ,b.updater = 1
    ,b.update_time = sysdate()
    ,b.remark2 = concat(b.remark2,'-取值恢复')
where b.latest_status = 1
  and (a.tjzq = b.supply_cycle
       and a.zbszlx = b.supply_frequency
       and ifnull(a.zbdw,'') = ifnull(b.index_unit,'')
       )
;



-- 更新【指标名称】
update t_index_pool_input b 
inner join int_d_hrp_crm_zbdm a
    on b.origin_system = '2'
    and a.zbdm = b.origin_index_code
set b.index_name = a.zbmc -- 指标名称
    ,b.origin_index_name = a.zbmc -- 原始指标名称
    ,b.updater = 1
    ,b.update_time = sysdate()
    ,b.remark2 = concat('指标名称修改,修改前指标名称为：【',a.zbmc,'】,操作时间为:',sysdate())
where b.index_name != a.zbmc
;


update t_index_pool a 
inner join t_index_pool_input b 
on a.related_index_code = b.index_code
and b.origin_system = '2'
set a.index_name = b.index_name
    ,a.update_time = sysdate()
where a.index_name != b.index_name
;




-- 获得记录数
select count(1) cnt into V_TABLE_CNT from t_index_pool_input;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,BIZ_DATE,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0);END
;