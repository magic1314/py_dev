DROP PROCEDURE IF EXISTS PRO_ANNUAL_ASSESS_EMP_HIS;
CREATE PROCEDURE `PRO_ANNUAL_ASSESS_EMP_HIS`(IN IN_ASSESS_DATE DATE)
label:BEGIN
/************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_ANNUAL_ASSESS_EMP_HIS
         功能简述：   年度考核人员信息列定
         参数：       IN_ASSESS_DATE    考核时点日期
         算法：
         注意事项：
                 1. 列定时间范围每年0915-1015，这段时间的人员数据归档
         数据源：
                 1、 sys_user                 系统用户表
                 2、 t_employee               员工表
         结果表：
                 1、t_annual_assess_emp_his   年度考核人员信息列定
         修改记录
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG          2023/11/20                 创建
         MG          2023/12/11                 提测V21版本
         MG          2023/12/12                 添加人员类别和管理人员标识过滤条件
                                                人员类别0301和0303合并为030103
         MG          2023/12/13                 剔除节假日的限制
***************************************************************************/

-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);

-- 设置变量
set V_EVENT_NAME = '年度考核人员信息列定';
set V_RUN_COMMAND = concat('call PRO_ANNUAL_ASSESS_EMP_HIS('
                            ,IN_ASSESS_DATE
                            ,')'
                         );



select now() into V_START_TIME;

set @assess_date = IN_ASSESS_DATE; -- 格式: 20231002
set @par_year = left(@assess_date,4);  -- 格式：2023
set @month_day = cast(date_format(@assess_date,'%m%d') as UNSIGNED); -- 格式1002(转成数字)


if (@month_day < 915 or @month_day > 1015) then 
    select '传入参数【考核时点日期】日期范围在【09月15日到10月15日】,执行计划跳过'; 
    LEAVE label; -- 退出存储过程 
end if;


-- -- 节假日标识(0:工作日 1:节假日)
-- select jyr_bs into @jyr_bs from t_xtjyr where zrr = @assess_date;
-- 
-- 
-- if @jyr_bs = 1 then 
--     select '传入参数【考核时点日期】为节假日,执行计划跳过'; 
--     LEAVE label; -- 退出存储过程 
-- end if;






delete from t_annual_assess_emp_his where assess_date = @assess_date;
insert into t_annual_assess_emp_his(par_year,assess_date,user_id,user_name,nick_name,account_status,account_flag,dept_id,Org_Sec_Code,Org_Sec_Name,job_rank
    ,pk_job,role_id,begindate,enddate,indutydate,enddutydate
    ,Emp_Type_Code,Emp_Type_Name,Managercode,Job_Appt_Dt,Job_Rank_Appt_Dt,Enter_Type)
select 
     @par_year par_year
    ,@assess_date assess_date
    ,b.user_id -- 员工ID
    ,b.user_name
    ,b.nick_name  -- 姓名
    ,b.account_status
    ,b.account_flag
    ,b.dept_id
    ,a.Org_Sec_Code
    ,a.Org_Sec_Name
    ,a.job_rank
    ,a.pk_job
    ,c.role_id
    ,a.begindate
    ,a.enddate
    ,a.indutydate
    ,a.enddutydate
    ,(case when a.Emp_Type_Code in ('0301','0303') then '030103' else a.Emp_Type_Code end) Emp_Type_Code
    ,a.Emp_Type_Name
    ,a.Managercode
    ,a.Job_Appt_Dt
    ,a.Job_Rank_Appt_Dt
    ,a.Enter_Type
from sys_user b 
left join t_employee a
on a.sys_user_id = b.user_id 
left join sys_user_role c 
on b.user_id = c.user_id
where b.del_flag = '0'
and a.emp_type_code in ('0101','0106','0201','0301','0303')
and (a.Managercode in ('013','014','016','018','019','020','022','023') or a.Managercode is null)
and b.account_status = '0'
and b.user_id != 1 -- admin不参与考核
;


update t_annual_assess_emp_his a
left join sys_dict_data d 
    on d.dict_type = 'emp_manager_status'
    and a.Managercode = d.dict_value
set a.Manager_status = if(d.remark = '管理人员',0,1)
where a.assess_date = @assess_date
;





-- 获得记录数
select count(1) cnt into V_TABLE_CNT from t_annual_assess_emp_his where assess_date = @assess_date;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;