DROP PROCEDURE IF EXISTS PRO_LOAD_HR_ORG;
CREATE PROCEDURE `PRO_LOAD_HR_ORG`(IN BIZ_DATE DATE)
label:BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_LOAD_HR_ORG
         功能简述：   组织信息表转换
         参数：       BIZ_DATE   数据日期
         注意事项：
                 sys_dept表dept_id=0(ZQ000/申万宏源集团)这里记录没有，由于添加可能会影响其他程序
         数据源：
                 int_d_new_hrs_org             int组织信息表
         结果表：
                 int_d_new_hrs_org_forward     过滤掉非【申万宏源证券有限公司】的部门
                 sys_dept                      部门表
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         CZB         2023/02/14                 创建
         MG          2023/06/02                 更新卫星营业部父节点及祖级列表;西部证券分公司下挂分公司
         MG          2023/07/05                 int表数据改变，dept表相应的逻辑调整
         MG          2023/07/12                 用递归的方式更新ancestors字段
         MG          2023/12/05                 相同的部门编码不会重复插入
         MG          2024/02/28                 新增部门拉链表
         MG          2024/03/14                 重新启动该存储过程
         MG          2024/03/18                 上级部门编码会变更
         MG          2024/03/19                 撤销部门的限制暂时不生效
         MG          2024/04/18                 新增注销日期，调整状态的逻辑
         MG          2024/06/12                 加入集团中非申万公司的部门；添加祖先节点遍历深度限制
         MG          2024/06/17                 分公司名称优先用简称；祖先列表字段问题修复
         MG          2024/07/08                 组织类型更新逻辑变更；新增部门黑名单表
         MG          2024/07/10                 黑名单表新增pk值字段
******************************************************************************************************************************************/

DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);


-- 设置变量
set V_EVENT_NAME = '组织信息表转换';
set V_RUN_COMMAND = concat('call PRO_LOAD_HR_ORG(',BIZ_DATE,')');
select now() into V_START_TIME;


/*
    int组织信息前置表：只保留【申万宏源证券有限公司】及下面的部门
    从【申万宏源证券有限公司】开始遍历数据
*/


truncate table int_d_new_hrs_org_forward;
insert into int_d_new_hrs_org_forward(pk_org,dept_id,parent_id,ancestors,org_code,org_name,org_shortname
    ,Center_Org_Code,father_org,create_dt,fic_org,dis_flag)
WITH RECURSIVE recursion AS (
    SELECT 
         pk_org
        ,1000 dept_id
        ,CAST('0' AS CHAR(50)) parent_id
        ,CAST('0' AS CHAR(50)) AS ancestors
        ,org_code,org_name,org_shortname,Center_Org_Code,father_org,create_dt,fic_org,dis_flag
        ,1 AS rec_depth
    FROM int_d_new_hrs_org
    WHERE Org_Code = 'ZQ0002'
    UNION ALL
    SELECT 
         b.pk_org
        ,replace(b.org_code, 'ZQ', '9') dept_id
        ,case when ifnull(b.Center_Org_Code,b.father_org) = 'ZQ0002' then 1000
         else replace(ifnull(b.Center_Org_Code,b.father_org), 'ZQ', '9') 
         end parent_id
        ,CONCAT(c.ancestors, ',', case when ifnull(b.Center_Org_Code,b.father_org) = 'ZQ0002' then 1000
                                 else replace(ifnull(b.Center_Org_Code,b.father_org), 'ZQ', '9') 
                                 end
        ) ancestors
        ,b.org_code,b.org_name,b.org_shortname,b.Center_Org_Code,b.father_org,b.create_dt,b.fic_org,b.dis_flag
        ,c.rec_depth + 1
    FROM int_d_new_hrs_org b
    INNER JOIN recursion c 
    ON ifnull(b.Center_Org_Code,b.father_org) = c.org_code
    where b.Org_Code not in (select org_code from sys_dept_black_list where org_code is not null)
    and b.PK_ORG not in (select PK_ORG from sys_dept_black_list where PK_ORG is not null)
    and c.rec_depth < 10 -- 最大深度限制
)
SELECT pk_org,dept_id,parent_id,ancestors,org_code
    ,case when org_name in ('申万宏源证券有限公司','申万宏源西部证券有限公司') then org_name
          when org_name in ('申万宏源证券承销保荐有限责任公司','申万宏源证券资产管理有限公司') then replace(org_name,'申万宏源证券','')
          when org_name like '申万宏源西部证券有限公司%' then replace(org_name,'申万宏源西部证券有限公司','')
          when org_name like '申万宏源证券有限公司%' then replace(org_name,'申万宏源证券有限公司','')
          else org_name
     end org_name
    ,org_shortname,Center_Org_Code,father_org,create_dt,fic_org,dis_flag FROM recursion
;




-- 新增部门写入目标表
insert into sys_dept
(dept_id, -- 部门主键
 parent_id, -- 父部门id
 ancestors, -- 祖级列表
 dept_name, -- 部门名称
 order_num, -- 显示顺序
 leader, -- 负责人
 phone, -- 联系电话
 email, -- 邮箱
 status, -- 部门状态（0正常 1停用）
 create_by, -- 创建者
 create_time, -- 创建时间
 update_by, -- 更新者
 update_time, -- 更新时间
 dept_no, -- 部门编号
 remark, -- 备注
 dept_type, -- 部门层级 0总部 1营业部
 dept_simple_name, -- 部门简称
 dept_address, -- 部门地址
 del_flag, -- 删除标志（0代表存在 2代表删除）
 org_type, -- 组织类型代码(4总部及下挂部门  6分公司  2营业部)
 sys_id, -- 源系统唯一id
 fic_org, -- 是否卫星营业部   0：否 1：是
 center_org_code -- 中心营业部编码
 )
select a.dept_id, -- 组织编码
       a.parent_id, -- 上级组织编码
       a.ancestors, -- 祖先列表
       a.org_name dept_name, -- 组织名称
       1 order_num, -- 显示顺序
       null leader, -- 负责人
       null phone, -- 联系电话
       null email, -- 邮箱
       0 status, -- 部门状态（0正常 1停用） 源系统:(1未启用2已启用3已停用)
       null create_by, -- 创建者
       a.create_dt create_time, -- 创建时间
       1 update_by, -- 更新者,默认系统管理员
       sysdate() update_time, -- 更新时间，写入系统时间
       a.org_code dept_no, -- 部门编号
       null remark, -- 备注
       0 dept_type, -- 部门层级 0总部 1营业部
       a.org_shortname dept_simple_name, -- 组织简称
       null dept_address, -- 部门地址
       0 del_flag, -- 删除标志(0代表存在 2代表删除),
       null org_type, -- 组织类型代码(4总部及下挂部门  6分公司  2营业部)
       a.pk_org sys_id, -- 源系统组织主键
       ifnull(a.fic_org, 0) fic_org, -- 是否卫星营业部   0：否 1：是
       replace(a.center_org_code, 'ZQ', '9') center_org_code -- 中心营业部编码
from int_d_new_hrs_org_forward a
left join sys_dept b
  on a.pk_org = b.sys_id
left join sys_dept b2
  on a.dept_id = b2.dept_id
where b.sys_id is null -- 过滤已存在sys_dept的记录
  and b2.sys_id is null -- 部门编码相同不插入
  and a.dis_flag = '0' -- 撤销标识 : 0-未撤销 1-撤销  增加过滤条件撤销标识
  and a.dept_id != 1000 -- 机构为：申万宏源集团，节点从申万宏源证券有限公司开始
;



/*** 【申万宏源证券有限公司】下部门处理 ***/

-- 清洗组织类型代码(1总公司 4总公司下挂各总部 6分公司 2营业部)
-- 1总公司(不反复更新)
-- update sys_dept set org_type = 1 where dept_no = 'ZQ0002';

-- 总公司编码暂是改为1000(不反复更新)
-- update sys_dept set dept_id = 1000 where dept_id = 90002;
-- update sys_dept set parent_id = 1000 where parent_id = 90002;
-- update sys_dept set parent_id = 0 where parent_id = 9000;


-- 4总公司下挂各总部
update sys_dept t
   set org_type = 4, order_num = 2,ancestors='0,1000'
where t.parent_id = 1000
and dept_name not like '%分公司%'
and org_type is null
;

-- 6分公司
update sys_dept t
   set org_type = 6, order_num = 3,ancestors='0,1000'
where t.parent_id = 1000
and dept_name like '%分公司%'
and org_type is null
;

-- 2营业部
update sys_dept t
   set org_type = 2, order_num = 4
where t.parent_id != 1000
and org_type is null
;



-- 字段内容更新
/*
数据变更：
1. 组织名称、组织简称改变： 支持
2. 组织编码改变：不支持(如果改变所有表的dept_id值都需要同步修改)
3. 上级组织编码改变：支持
4. 中心营业部转卫星：支持
5. 卫星转中心营业部：支持
6. 中心营业部编码改变：支持
*/

update sys_dept a 
inner join int_d_new_hrs_org_forward b 
on a.sys_id = b.pk_org
set -- a.dept_id = replace(b.org_code, 'ZQ', '9'), -- 组织编码（不会主动修改，由于系统涉及变更的内容很多）
    a.parent_id = b.parent_id, -- 上级组织编码
    a.ancestors = b.ancestors, -- 祖先列表
    a.dept_name = b.org_name, -- 组织名称
    a.status = (case when b.Dis_Flag = '0' then 0 else 1 end), -- 部门状态（0正常 1已撤销 2隐藏） 源系统:(0 正常，1：已撤销)
    a.create_time = b.create_dt, -- 创建时间
    a.dept_no = b.org_code, -- 部门编号
    a.dept_simple_name = b.org_shortname, -- 组织简称
    a.fic_org = ifnull(b.fic_org, 0), -- 是否卫星营业部   0：否 1：是
    a.center_org_code = replace(b.center_org_code, 'ZQ', '9') -- 中心营业部编码
where b.dis_flag = '0' -- 撤销标识  增加过滤条件撤销标识
  and b.dept_id != 1000 -- 机构为：申万宏源集团，节点从申万宏源证券有限公司开始
;




/************************************************************************************************************************************************
    # 部门拉链表
    # 主键(确定一条记录的标识)：Pk_Psndoc
    # 记录变化字段：Org_Code,Org_Name,Org_Shortname,Father_Org,Create_Dt,Org_Create_Dt,Org_Cancel_Dt,Dept_Level
                    ,Dept_Type,Enablestate,Belong_Group,Dis_Flag,Fic_Org,Center_Org_Code
    
************************************************************************************************************************************************/

/******************* 情况1：部分字段内容发生变化 ***********************/
-- 1、 针对变更的数据(这里只考虑部门),做更新处理:更新结束日期、数据更新时间
update int_d_new_hrs_org_chain chain
inner join int_d_new_hrs_org org 
    on chain.PK_ORG = org.PK_ORG
set chain.end_date = date_sub(current_date, interval 1 day)
    ,chain.current_flag = 2 -- 表示需要后面更新
where chain.current_flag = 1
and ( 
       ifnull(chain.Org_Code       ,'') <> ifnull(org.Org_Code       ,'')
    or ifnull(chain.Org_Name       ,'') <> ifnull(org.Org_Name       ,'')
    or ifnull(chain.Org_Shortname  ,'') <> ifnull(org.Org_Shortname  ,'')
    or ifnull(chain.Father_Org     ,'') <> ifnull(org.Father_Org     ,'')
    or ifnull(chain.Create_Dt      ,'') <> ifnull(org.Create_Dt      ,'')
    or ifnull(chain.Org_Create_Dt  ,'') <> ifnull(org.Org_Create_Dt  ,'')
    or ifnull(chain.Org_Cancel_Dt  ,'') <> ifnull(org.Org_Cancel_Dt  ,'')
    or ifnull(chain.Dept_Level     ,'') <> ifnull(org.Dept_Level     ,'')
    or ifnull(chain.Dept_Type      ,'') <> ifnull(org.Dept_Type      ,'')
    or ifnull(chain.Enablestate    ,'') <> ifnull(org.Enablestate    ,'')
    or ifnull(chain.Belong_Group   ,'') <> ifnull(org.Belong_Group   ,'')
    or ifnull(chain.Dis_Flag       ,'') <> ifnull(org.Dis_Flag       ,'')
    or ifnull(chain.Fic_Org        ,'') <> ifnull(org.Fic_Org        ,'')
    or ifnull(chain.Center_Org_Code,'') <> ifnull(org.Center_Org_Code,'')
)
;

-- 2、针对变更的数据,做新增数据处理  
insert into int_d_new_hrs_org_chain(PK_ORG,current_flag,start_date,end_date,Org_Code,Org_Name,Org_Shortname,Father_Org,Create_Dt
    ,Org_Create_Dt,Org_Cancel_Dt,Dept_Level,Dept_Type,Enablestate,Belong_Group,Dis_Flag,Fic_Org,Center_Org_Code) 
select
    org.PK_ORG
    ,1 current_flag
    ,current_date start_date
    ,'2099-12-31' end_date
    ,org.Org_Code,org.Org_Name,org.Org_Shortname,org.Father_Org,org.Create_Dt
    ,org.Org_Create_Dt,org.Org_Cancel_Dt,org.Dept_Level,org.Dept_Type,org.Enablestate,org.Belong_Group,org.Dis_Flag,org.Fic_Org,org.Center_Org_Code
from int_d_new_hrs_org org
inner join int_d_new_hrs_org_chain chain 
ON chain.PK_ORG = org.PK_ORG
where chain.current_flag = 2
;

-- 3、已更新完成,状态转为无效  
update int_d_new_hrs_org_chain
set current_flag = 0
where current_flag = 2
;

/******************* 情况2：主键新增 ***********************/
insert into int_d_new_hrs_org_chain(PK_ORG,current_flag,start_date,end_date,Org_Code,Org_Name,Org_Shortname,Father_Org,Create_Dt
    ,Org_Create_Dt,Org_Cancel_Dt,Dept_Level,Dept_Type,Enablestate,Belong_Group,Dis_Flag,Fic_Org,Center_Org_Code) 
select
    org.PK_ORG
    ,1 current_flag
    ,current_date start_date
    ,'2099-12-31' end_date
    ,org.Org_Code,org.Org_Name,org.Org_Shortname,org.Father_Org,org.Create_Dt
    ,org.Org_Create_Dt,org.Org_Cancel_Dt,org.Dept_Level,org.Dept_Type,org.Enablestate,org.Belong_Group,org.Dis_Flag,org.Fic_Org,org.Center_Org_Code
from int_d_new_hrs_org org
left join int_d_new_hrs_org_chain chain 
    on chain.PK_ORG = org.PK_ORG
    and chain.current_flag = 1
where chain.PK_ORG is null
;
/******************* 情况3：删除 ***********************/
update int_d_new_hrs_org_chain chain
left join int_d_new_hrs_org org 
    on chain.PK_ORG = org.PK_ORG
set chain.end_date = current_date
    ,chain.current_flag = -1
where chain.current_flag = 1
    and org.PK_ORG is null
;

-- 获得记录数
select count(1) cnt into V_TABLE_CNT from sys_dept;
-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,BIZ_DATE,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0);
END
;
;