DROP PROCEDURE IF EXISTS PRO_SCORE_ASSESS_INIT_AFTER;
CREATE PROCEDURE `PRO_SCORE_ASSESS_INIT_AFTER`(IN BIZ_DATE DATE)
BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_SCORE_ASSESS_INIT_AFTER
         功能简述：   考核评价模块数据初始化补充
         参数：       BIZ_DATE  上个工作日
         注意事项：
                      等java处理完成后再执行的存储过程
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG          2024/07/11                 创建该过程
         MG          2024/07/18                 新增更新result表id值的逻辑
         MG          2024/07/19                 移除更新result表id值的逻辑
***************************************************************************************************************************************/

DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);

-- 设置变量
set V_EVENT_NAME = '考核评价模块数据初始化补充';
set V_RUN_COMMAND = concat('call PRO_SCORE_ASSESS_INIT_AFTER(',BIZ_DATE,')');
select now() into V_START_TIME;






-- 获得记录数
select 0 into V_TABLE_CNT ;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,BIZ_DATE,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;