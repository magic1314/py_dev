DROP PROCEDURE IF EXISTS PRO_DELETE_SCORE_INFO;
CREATE PROCEDURE `PRO_DELETE_SCORE_INFO`(IN IN_YEAR_MONTH VARCHAR(6), IN IN_DEPT_ID INT, IN IN_PERIOD INT)
label:BEGIN

-- 传入值
set @assess_year_month = IN_YEAR_MONTH;  -- 年月
set @dept_id = IN_DEPT_ID;    -- 部门id
set @assess_pd = IN_PERIOD;     -- 周期(1:月，2：季度，3：半年)


set @assess_month = cast(right(@assess_year_month,2) as UNSIGNED);
set @assess_year = left(@assess_year_month,4); 
set @assess_pd_value = (select assess_pd_value from t_dict_assess_pd_mqh where assess_pd = @assess_pd and assess_month = @assess_month);




-- 清理数据
update t_assess_trial_calc
set status = 0
where dept_id = @dept_id 
and par_year = @assess_year
and assess_pd_value = @assess_pd_value
;


update t_assess_dept_progress
set status = 0
where dept_id = @dept_id 
and par_year = @assess_year
and assess_pd_value = @assess_pd_value
;


delete a from t_assess_dept_progress_dtl a
inner join t_assess_dept_progress b
on a.assess_dept_progress_id = b.id
where b.dept_id = @dept_id
and b.par_year = @assess_year
and b.assess_pd_value = @assess_pd_value
;

delete from t_assess_index_score_result_qt where dept_id = @dept_id and year_months = @assess_year_month and assess_pd = @assess_pd;
delete from t_assess_score_dtl where dept_id = @dept_id and year_months = @assess_year_month and assess_pd = @assess_pd;
delete from t_assess_index_score_result_qlt where dept_id = @dept_id and year_months = @assess_year_month and assess_pd = @assess_pd;
delete from t_assess_index_score_result where dept_id = @dept_id and year_months = @assess_year_month and assess_pd = @assess_pd;
delete from t_assess_emp_score_result where dept_id = @dept_id and par_year = @assess_year and assess_pd_value = @assess_pd_value;
delete from t_assess_index_score_result_adjust where dept_id = @dept_id and year_months = @assess_year_month and assess_pd = @assess_pd;
delete from t_assess_member_score_dtl where par_year = @assess_year and assess_pd_value = @assess_pd_value and dept_id = @dept_id;
delete from t_assess_member_score where par_year = @assess_year and assess_pd_value = @assess_pd_value and dept_id = @dept_id;
delete from t_assess_dept_member_score where par_year = @assess_year and assess_pd_value = @assess_pd_value and dept_id = @dept_id;

END
;