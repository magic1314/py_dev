DROP PROCEDURE IF EXISTS PRO_SCORE_ASSESS_INIT;
CREATE PROCEDURE `PRO_SCORE_ASSESS_INIT`(IN IN_YEAR_MONTH VARCHAR(6), IN IN_DEPT_ID INT, IN IN_PERIOD INT)
label:BEGIN
/************************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_SCORE_ASSESS_INIT
         功能简述：   考核评价模块数据初始化
         参数：       IN_YEAR_MONTH    考核年月
                      IN_DEPT_ID       部门编号
                      IN_PERIOD        考核周期(1:月,2:半年,3:季度)
         返回：       无
         算法：
         注意事项：
                 1. 传入日期的上个月，是考核的月份
                 eg.  今日为：202303
                      考核月份是：202303，也就是结果表中year_months的取值
                 2. 维度
                   1)dept_id(部门) 
                   2)assess_year(年份)
                   3)assess_month(月份)
                   4)assess_pd(考核周期)
                   
                   5)year_months(年月)：等同于【年份+月份】
                   6)assess_pd_value(考核周期值)：等同于【考核周期+月份】
                   
                   故常见搭配的等价组合：
                              a) (1234) dept_id + assess_year + assess_month + assess_pd（本程序未使用）
                              b) (126)  dept_id + assess_year + assess_pd_value(本程序少量使用)
                              c) (145)  dept_id + assess_pd + year_months (本程序主要使用)
                              -- 其他
                              d) (156) dept_id + year_months + assess_pd_value (冗余)
                 
         数据源：
                    1. sys_user                                用户表
                    2. t_appraisal_group                       考核组
                    3. t_appraisal_group_reviewer              考核组成员
                    4. t_assess_dept_not_assess_member         不考评名单_未开启
                    5. t_assess_dept_progress                  部门考核进度
                    6. t_assess_info_his                       考核方案列定
                    7. t_assess_work_record                    工作日志-定性指标
                    8. t_dict_assess_pd_mqh                    考核周期映射表
                    9. t_emp_perf_cmplt_val                    考核指标完成值表
         目标表：
                    1. t_assess_dept_progress_dtl              被打分人状态
                    2. t_assess_dept_progress_scorer           打分人和调整人状态
                    3. t_assess_emp_score_result               考核人打分指标明细表
                    4. t_assess_index_score_result             定性和定量指标结果表
                    5. t_assess_index_score_result_adjust      调整人打分明细
                    6. t_assess_index_score_result_qlt         定性指标打分结果表
                    7. t_assess_index_score_result_qlt_reset   考核打分指标明细表_撤回流程
                    8. t_assess_index_score_result_qt          定量指标打分结果表
                    9. t_assess_score_dtl                      打分关系表
                    
         修改记录：
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2023/04/24                  创建
         MG         2023/04/25                  部门考核进度明细表 只刷新当期考核的周期
                                                为了方便，添加必要的字段
         MG         2023/04/26                  调整项去重
         MG         2023/05/11                  考核人打分指标明细表 重跑数据清理
         MG         2023/05/16                  删除表错误
         MG         2023/05/18                  调整项 打分结果默认为0
         MG         2023/06/07                  新增dept_appraisal_id字段
         MG         2023/07/26                  新增【部门考核进度明细表_考核人】表，已经相关逻辑
         MG         2023/08/31                  删除无效的字段，新增表索引 
         MG         2023/09/08                  调整项人中部分字段遗漏，导致数据重复
         MG         2024/02/05                  添加【考核打分指标明细表_撤回流程】表的逻辑；多人平摊考核权
         MG         2024/03/13                  V27 添加不参与考核成员的功能；参数名称优化；assess_pd字段内容补充等
         MG         2024/03/21                  添加备注信息
         MG         2024/07/11                  判断定量指标逻辑调整
************************************************************************************************************************************************/
-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);

-- 设置变量
set V_EVENT_NAME = '考核评价模块数据初始化';
set V_RUN_COMMAND = concat('call PRO_SCORE_ASSESS_INIT('
                            ,IN_YEAR_MONTH,','
                            ,IN_DEPT_ID,','
                            ,IN_PERIOD
                            ,')'
                         );



select now() into V_START_TIME;


set @year_months = IN_YEAR_MONTH; -- 格式: 202302
set @assess_month = cast(right(@year_months,2) as UNSIGNED);   -- 格式：2
set @par_year = left(@year_months,4);   -- 格式：2023
set @dept_id = IN_DEPT_ID;   -- 格式： 12800
set @assess_pd = IN_PERIOD;  -- 格式： 1
set @assess_pd_value = (select assess_pd_value from t_dict_assess_pd_mqh where assess_pd = @assess_pd and assess_month = @assess_month);



-- 创建一个考核方案列定临时表
drop TEMPORARY table if exists tmp_assess_info_his;
create TEMPORARY table tmp_assess_info_his
select distinct a.* from t_assess_info_his a
left join t_assess_dept_not_assess_member b -- 未开启考核时，不参与考核的成员
    on a.par_year = b.par_year
    and a.assess_pd_value = b.assess_pd_value
    and a.dept_id = b.dept_id 
    and a.user_id = b.member_id 
    and b.deleted = 0
where a.par_year = @par_year
and a.dept_id = @dept_id
and a.assess_pd_value = @assess_pd_value
and b.id is null
;

alter table tmp_assess_info_his add index index_3(par_year,assess_pd_value,dept_id);
alter table tmp_assess_info_his add index index_assess_group_id(assess_group_id);
alter table tmp_assess_info_his add index index_year_months(year_months);


/*
步骤：部门考核进度明细表
注：点击考核方案生成,不会反复插入数据(针对部门，一次插入后，不再更新)
*/


delete from t_assess_dept_progress_dtl where par_year = @par_year and dept_id = @dept_id and assess_pd_value = @assess_pd_value;
insert into t_assess_dept_progress_dtl(par_year,dept_id,assess_pd,assess_pd_value,assess_dept_progress_id,group_id,member_id,index_status,adjust_status,creator,create_time,updater,update_time,remark)
select 
    distinct 
    @par_year par_year
    ,@dept_id dept_id
    ,@assess_pd assess_pd
    ,@assess_pd_value assess_pd_value
    ,a.id assess_dept_progress_id
    ,b.assess_group_id group_id
    ,b.user_id member_id
    ,0 index_status
    ,0 adjust_status
    ,a.creator
    ,now() create_time
    ,a.updater
    ,now() update_time
    ,'开启考核数据自动生成' remark
from t_assess_dept_progress a
left join tmp_assess_info_his b
on a.par_year = b.par_year 
    and a.dept_id = b.dept_id
    and a.assess_pd_value = b.assess_pd_value
where a.par_year = @par_year
and a.dept_id = @dept_id
and a.assess_pd_value = @assess_pd_value
;





-- 考核人
delete from t_assess_dept_progress_scorer where par_year = @par_year and dept_id = @dept_id and assess_pd_value = @assess_pd_value;
insert into t_assess_dept_progress_scorer(par_year,dept_id,assess_pd,assess_pd_value,assess_dept_progress_id,group_id,group_name,user_flag,user_id,user_name,user_code,status
    ,creator,create_time,updater,update_time,remark)
select 
    distinct 
    @par_year par_year
    ,@dept_id dept_id
    ,@assess_pd assess_pd
    ,@assess_pd_value assess_pd_value
    ,a.id assess_dept_progress_id
    ,b.assess_group_id group_id
    ,g.group_name
    ,1 user_flag  -- 用户标识(1:考核人/2:调整人)
    ,gr.appraisal_id user_id
    ,u.nick_name user_name
    ,u.user_name user_code
    ,0 status
    ,a.creator
    ,now() create_time
    ,a.updater
    ,now() update_time
    ,'开启考核数据自动生成' remark
from t_assess_dept_progress a
inner join tmp_assess_info_his b
on a.par_year = b.par_year 
    and a.dept_id = b.dept_id
    and a.assess_pd_value = b.assess_pd_value
left join t_appraisal_group g 
    on b.assess_group_id = g.id 
    and g.deleted = 0
left join t_appraisal_group_reviewer gr 
    on b.assess_group_id = gr.group_id
    and gr.deleted = 0
left join sys_user u 
    on gr.appraisal_id = u.user_id 
where a.par_year = @par_year
and a.dept_id = @dept_id
and a.assess_pd_value = @assess_pd_value
;


-- 调整人
insert into t_assess_dept_progress_scorer(par_year,dept_id,assess_pd,assess_pd_value,assess_dept_progress_id,group_id,group_name,user_flag,user_id,user_name,user_code,status
    ,creator,create_time,updater,update_time,remark)
select 
    distinct 
    @par_year par_year
    ,@dept_id dept_id
    ,@assess_pd assess_pd
    ,@assess_pd_value assess_pd_value
    ,a.id assess_dept_progress_id
    ,b.assess_group_id group_id
    ,g.group_name
    ,2 user_flag  -- 用户标识(1:考核人/2:调整人)
    ,g.adjust_id user_id
    ,u.nick_name user_name
    ,u.user_name user_code
    ,0 status
    ,a.creator
    ,now() create_time
    ,a.updater
    ,now() update_time
    ,'开启考核数据自动生成' remark
from t_assess_dept_progress a
left join tmp_assess_info_his b
on a.par_year = b.par_year 
    and a.dept_id = b.dept_id
    and a.assess_pd_value = b.assess_pd_value
left join t_appraisal_group g 
    on b.assess_group_id = g.id 
    and g.deleted = 0
left join sys_user u 
    on g.adjust_id = u.user_id 
where a.dept_id = @dept_id
and a.par_year = @par_year
and a.assess_pd_value = @assess_pd_value
;






-- 考核结果指标明细表_定量
delete from t_assess_index_score_result_qt where dept_id = @dept_id and year_months = @year_months and assess_pd = @assess_pd;
insert into t_assess_index_score_result_qt(year_months,par_year,assess_pd,assess_pd_value,dept_id,dept_appraisal_id,group_id,member_id,index_code,index_appraisal_right,business_type,current_tgt,complete_info,index_score,assess_score,creator,create_time,updater,update_time,remark)
select 
     @year_months  year_months                 -- 年月
    ,a.par_year                                -- 年份
    ,a.assess_pd                               -- 考核周期
    ,a.assess_pd_value                         -- 考核周期值
    ,a.dept_id                                 -- 部门编码
    ,a.dept_appraisal_id                       -- 考核权成员表id
    ,a.assess_group_id group_id                -- 考核组id
    ,a.user_id member_id                       -- 被考核人id
    ,a.index_code                              -- 指标代码
    ,a.appraisal_right index_appraisal_right   -- 考核权重
    ,a.business_type                           -- 指标类型
    ,a.current_tgt                             -- 考核目标
    ,b.index_value complete_info               -- 完成情况
    ,null index_score                          -- 指标单项得分
    ,null assess_score                         -- 计入考核得分
    ,null creator                              -- 创建人
    ,now() create_time                         -- 创建时间
    ,null updater                              -- 修改人
    ,now() update_time                         -- 最近一次修改时间
    ,'开启考核数据自动生成' remark             -- 备注
from tmp_assess_info_his a 
left join t_emp_perf_cmplt_val b 
on a.year_months = b.year_months
    and a.dept_id = b.dept_id
    and a.user_id = b.user_id
    and a.index_code = b.index_code
    and a.assess_pd = b.assess_pd
where a.business_type != 6
;





-- 考核打分关系表
-- 所有指标
delete from t_assess_score_dtl where dept_id = @dept_id and year_months = @year_months and assess_pd = @assess_pd;
insert into t_assess_score_dtl(year_months,par_year,assess_pd,assess_pd_value,dept_id,dept_appraisal_id,group_id,member_id
    ,appraisal_id,appraisal_group_num,appraisal_right,status,creator,create_time,updater,update_time,remark)
select 
     distinct 
     @year_months  year_months                  -- 年月
    ,a.par_year                                 -- 考核年份
    ,a.assess_pd                                -- 考核周期
    ,a.assess_pd_value                          -- 考核周期值
    ,a.dept_id                                  -- 部门编码
    ,a.dept_appraisal_id                        -- 考核权成员表id
    ,a.assess_group_id group_id                 -- 考核组id
    ,a.user_id member_id                        -- 被考核人id
    ,b.appraisal_id                             -- 考核人id
    ,b.appraisal_group_num                      -- 多人考核人标识
    ,b.appraisal_right                          -- 考核权重
    ,0 status                                   -- 打分状态(0:未打分，1：已打分，2：已锁定，3：已提交)
    ,null creator                               -- 创建人
    ,now() create_time                          -- 创建时间
    ,null updater                               -- 修改人
    ,now() update_time                          -- 最近一次修改时间
    ,'开启考核数据自动生成'  remark             -- 备注
from tmp_assess_info_his a   -- 考核方案粒度是指标
left join t_appraisal_group_reviewer b  -- 1个被考核人对应多个考核人
on a.assess_group_id = b.group_id
and b.deleted = 0
;





-- 考核打分指标明细表_定性
delete from t_assess_index_score_result_qlt where dept_id = @dept_id and year_months = @year_months and assess_pd = @assess_pd;
insert into t_assess_index_score_result_qlt(assess_rela_id,year_months,par_year,dept_id,dept_appraisal_id,assess_pd,assess_pd_value,member_id
    ,group_id,appraisal_id,appraisal_group_num,appraisal_right,index_appraisal_right,business_type,index_code,current_tgt
    ,complete_info,index_score,assess_score,creator,create_time,updater,update_time,remark,delelted)
select 
     a.id assess_rela_id                       -- 考核打分关系表id
    ,@year_months  year_months                 -- 年月
    ,a.par_year                                -- 年份
    ,a.dept_id                                 -- 考核部门
    ,a.dept_appraisal_id                       -- 考核权成员表id
    ,a.assess_pd                               -- 考核周期
    ,a.assess_pd_value                         -- 考核周期值
    ,a.member_id                               -- 被考核人id  主表存在
    ,a.group_id                                -- 考核组
    ,a.appraisal_id                            -- 考核人id  主表存在
    ,a.appraisal_group_num                     -- 多人考核人标识
    ,a.appraisal_right                         -- 打分人考核权重
    ,b.appraisal_right index_appraisal_right   -- 考核权重 主表存在
    ,b.business_type                           -- 指标类型
    ,b.index_code                              -- 指标代码
    ,b.current_tgt                             -- 指标目标
    ,w.complete_info                           -- 指标完成情况
    ,null index_score                          -- 指标单项得分
    ,null assess_score                         -- 计入考核得分
    ,null creator                              -- 创建人
    ,now() create_time                         -- 创建时间
    ,null updater                              -- 修改人
    ,now() update_time                         -- 最近一次修改时间
    ,'开启考核数据自动生成' remark             -- 备注
    ,0 delelted                                -- 删除标识
from t_assess_score_dtl a
left join tmp_assess_info_his b 
    on a.par_year = b.par_year
    and a.assess_pd_value = b.assess_pd_value
    and a.dept_id = b.dept_id
    and a.group_id = a.group_id
    and a.member_id = b.user_id
left join t_assess_work_record w   -- 在启动考核后，不再修改
    on a.par_year = w.par_year
    and a.assess_pd_value = w.assess_pd_value
    and a.dept_id = w.dept_id
    and a.member_id = w.member_id
    and b.index_code = w.index_code
    and w.status = 2    -- 状态(1:已填报，2:已提交)
where b.business_type = 6
and a.dept_id = @dept_id
and a.year_months = @year_months
and a.assess_pd = @assess_pd
;




-- 定量、定性汇总表
delete from t_assess_index_score_result where dept_id = @dept_id and year_months = @year_months and assess_pd = @assess_pd;
insert into t_assess_index_score_result(year_months,par_year,assess_pd,assess_pd_value,dept_id,dept_appraisal_id,group_id,member_id
    ,appraisal_id,appraisal_group_num,index_code,appraisal_right,index_appraisal_right,business_type,current_tgt,complete_info
    ,index_score,assess_score,assess_emp_score_result_id,creator,create_time,updater,update_time,remark)
select
     @year_months  year_months                 -- 年月
    ,a.par_year                                -- 考核年份
    ,a.assess_pd                               -- 考核周期
    ,a.assess_pd_value                         -- 考核周期值
    ,a.dept_id                                 -- 部门编码
    ,a.dept_appraisal_id                       -- 考核权成员表id
    ,a.group_id                                -- 考核组id
    ,a.member_id                               -- 被考核人id
    ,b.appraisal_id                            -- 考核人id
    ,b.appraisal_group_num                     -- 多人考核人标识
    ,a.index_code                              -- 指标代码
    ,b.appraisal_right                         -- 打分人考核权重
    ,a.index_appraisal_right                   -- 考核权重
    ,a.business_type                           -- 指标类型(1-5:定量，6：定性)
    ,a.current_tgt                             -- 考核目标
    ,a.complete_info                           -- 完成情况
    ,a.index_score                             -- 指标单项得分  细项得分
    ,a.assess_score                            -- 计入考核得分=指标单项得分*考核权重
    ,null assess_emp_score_result_id           -- 考核人打分结果表id
    ,null creator                              -- 创建人
    ,now() create_time                         -- 创建时间
    ,null updater                              -- 修改人
    ,now() update_time                         -- 最近一次修改时间
    ,'开启考核数据自动生成'  remark            -- 备注
from t_assess_index_score_result_qt a  -- 考核打分指标明细表_定量
left join t_appraisal_group_reviewer b  -- 1个被考核人对应多个考核人
on a.group_id = b.group_id
and b.deleted = 0
where a.dept_id = @dept_id
and a.year_months = @year_months
and a.assess_pd = @assess_pd
;



insert into t_assess_index_score_result(year_months,par_year,assess_pd,assess_pd_value,dept_id,dept_appraisal_id,group_id,member_id
    ,appraisal_id,appraisal_group_num,index_code,appraisal_right,index_appraisal_right,business_type,current_tgt,complete_info,index_score,assess_score
    ,assess_emp_score_result_id,creator,create_time,updater,update_time,remark)
select
     @year_months  year_months                 -- 年月
    ,a.par_year                                -- 考核年份
    ,a.assess_pd                               -- 考核周期
    ,a.assess_pd_value                         -- 考核周期值
    ,a.dept_id                                 -- 部门编码
    ,a.dept_appraisal_id                       -- 考核权成员表id
    ,a.group_id                                -- 考核组id
    ,a.member_id                               -- 被考核人id
    ,a.appraisal_id                            -- 考核人id
    ,a.appraisal_group_num                     -- 多人考核人标识
    ,a.index_code                              -- 指标代码
    ,a.appraisal_right                         -- 打分人考核权重
    ,a.index_appraisal_right                   -- 考核权重
    ,a.business_type                           -- 指标类型(1-5:定量，6：定性)
    ,a.current_tgt                             -- 考核目标
    ,a.complete_info                           -- 完成情况
    ,null index_score                          -- 指标单项得分  细项得分
    ,null assess_score                         -- 计入考核得分=指标单项得分*考核权重
    ,null assess_emp_score_result_id           -- 考核人打分结果表id
    ,null creator                              -- 创建人
    ,now() create_time                         -- 创建时间
    ,null updater                              -- 修改人
    ,now() update_time                         -- 最近一次修改时间
    ,'开启考核数据自动生成'  remark            -- 备注
from t_assess_index_score_result_qlt a  -- 考核打分指标明细表_定性
where a.dept_id = @dept_id
and a.year_months = @year_months
and a.assess_pd = @assess_pd
;


-- 考核组标识人数
drop table if exists tmp_appraisal_group_cnt;
create TEMPORARY table tmp_appraisal_group_cnt
select 
    dept_id,group_id,appraisal_group_num,count(1) appraisal_group_cnt
from t_appraisal_group_reviewer
where deleted = 0 
    and dept_id = @dept_id
group by dept_id,group_id,appraisal_group_num
;

alter table tmp_appraisal_group_cnt add index idx_3(dept_id,group_id,appraisal_group_num);


update t_assess_index_score_result a 
inner join tmp_appraisal_group_cnt b 
    on a.dept_id = b.dept_id 
    and a.group_id = b.group_id
    and a.appraisal_group_num = b.appraisal_group_num
set a.appraisal_group_cnt = b.appraisal_group_cnt
where a.dept_id = @dept_id
    and a.year_months = @year_months
    and a.assess_pd = @assess_pd
;


-- 考核打分指标明细表_撤回流程 & 考核人打分指标明细表
-- 该2张表的数据由java代码生成，由于会有重跑，这里需要清理历史数据
delete from t_assess_index_score_result_qlt_reset where dept_id = @dept_id and par_year = @par_year and assess_pd_value = @assess_pd_value;
delete from t_assess_emp_score_result where dept_id = @dept_id and par_year = @par_year and assess_pd_value = @assess_pd_value;




-- 调整项
delete from t_assess_index_score_result_adjust where dept_id = @dept_id and year_months = @year_months and assess_pd = @assess_pd;
insert into t_assess_index_score_result_adjust(year_months,par_year,assess_pd,assess_pd_value,dept_id,assess_group_id,member_id,adjust_id,adjust_score,status,creator,create_time,updater,update_time,remark)
select 
     distinct @year_months  year_months        -- 年月
    ,a.par_year                                -- 考核年份
    ,a.assess_pd                               -- 考核周期
    ,a.assess_pd_value                         -- 考核周期值
    ,a.dept_id                                 -- 部门编码
    ,a.assess_group_id                         -- 考核组ID
    ,a.user_id member_id                       -- 被考核人员ID
    ,b.adjust_id                               -- 调整项考核人员ID
    ,0 adjust_score                            -- 打分结果
    ,0 status                                  -- 状态：未打分，已打分，已锁定，已提交
    ,null creator                              -- 创建人
    ,now() create_time                         -- 创建时间
    ,null updater                              -- 修改人
    ,now() update_time                         -- 最近一次修改时间
    ,'开启考核数据自动生成'  remark            -- 备注
from tmp_assess_info_his a   -- 考核方案粒度是指标
left join t_appraisal_group b  -- 1个被考核人对应多个考核人
on a.assess_group_id = b.id
and b.deleted = 0
;




-- 获得记录数
select 0 into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;