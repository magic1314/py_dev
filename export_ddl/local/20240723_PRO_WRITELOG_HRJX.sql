DROP PROCEDURE IF EXISTS PRO_WRITELOG_HRJX;
CREATE PROCEDURE `PRO_WRITELOG_HRJX`(in I_NAME    VARCHAR(200), -- 事件名称
                                              in I_CMD     VARCHAR(1000),  -- 执行命令
                                              in I_BIZ_DATE DATE, -- 业务日期
                                              in I_ERRTYPE tinyint,  -- 错误类型 错误类型，0|提示;1|一般错误;2|严重错误
                                              in I_ERRMSG  VARCHAR(2000),IN I_ROWS int,I_TIMES int)
BEGIN
/***************************************************************************************************************************************
    
    项目名称：人力绩效-墨方自研
    创建人员： CZB
    过程名称:  PRO_WRITELOG_HRJX
    功能说明： 员工业绩收入系统日志记录
    参数：
       @para1: 事件名称
       @para2: 执行命令
       @para3: 错误类型
       @para4: 执行结果(可以得到记录数和秒)
       @para5: 记录数(写0，则从执行结果中取，否则直取)
       @para6: 执行秒数(写0，则从执行结果中取，否则直取)
    执行案例：
       call PRO_WRITELOG_HRJX('事件名称','执行命令',0,'执行成功[记录:22200],用时500秒',0,0)
       call PRO_WRITELOG_HRJX('事件名称','执行命令',0,'执行成功',11100,55)
       select * from t_procedure_log;
    修改记录:
    -----------------------------------------------------------------------------
    
    操作人         操作时间          说明
    CZB           2021/06/08        模仿原存储过程的写法
    MG            2023/02/21        存储过程改为mysql调用
    MG            2023/06/08        将最后的行数打印删除
    MG            2024/01/30        格式规范化
******************************************************************************************************************************************/

   DECLARE V_ROWS int;   -- 变量
   DECLARE V_TIMES int;   -- 变量
   -- DECLARE I_ROWS int default 0;   -- 传入的参数
   -- DECLARE I_TIMES int default 0;  -- 传入的参数 

   set V_ROWS = 1;
   
   BEGIN
       IF (I_ROWS = 0 AND INSTR(I_ERRMSG, '记录:') > 0) THEN
           set V_ROWS = SUBSTR(I_ERRMSG, INSTR(I_ERRMSG, '记录:') + 3,
                                     INSTR(I_ERRMSG, ']') - INSTR(I_ERRMSG, '记录:') - 3);
       ELSE 
           set V_ROWS = I_ROWS;
       END IF;
   END;
   
   BEGIN
       IF (I_TIMES = 0.0 AND INSTR(I_ERRMSG, '用时') > 0) THEN 
           set V_TIMES = SUBSTR(I_ERRMSG, INSTR(I_ERRMSG, '用时') + 2,
                               INSTR(I_ERRMSG, '秒') - INSTR(I_ERRMSG, '用时') - 2);
       ELSE 
           set V_TIMES = I_TIMES;
       END IF;
   END;

     -- 写入日志表记录
     INSERT INTO t_procedure_log
         (data_date, data_time, biz_date, ERRTYPE, ERRMSG, EVENT_NAME, CMD, ROWCOUNT, TIMES)
     VALUES
         (
          current_date() -- data_date -- 数据日期
         ,time(now())    -- data_time  -- 数据时间
         ,I_BIZ_DATE     -- biz_date   -- 业务日期
         ,I_ERRTYPE      -- ERRTYPE  -- 错误类型：0|提示;1|一般错误;2|严重错误
         ,I_ERRMSG       -- ERRMSG    -- 错误信息
         ,I_NAME         -- EVENT_NAME    -- 事件名称
         ,I_CMD          -- CMD    -- 执行命令
         ,V_ROWS         -- ROWCOUNT    -- 记录数
         ,V_TIMES        -- TIMES    -- 秒
      );

END
;