DROP PROCEDURE IF EXISTS PRO_LOAD_HRP_TG_ZBDM;
CREATE PROCEDURE `PRO_LOAD_HRP_TG_ZBDM`(IN `BIZ_DATE` date)
BEGIN
/************************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_LOAD_HRP_TG_ZBDM
         功能简述：   托管中心-指标代码表转换
         参数：
                     BIZ_DATE  上一交易日
         注意事项：
                  
         数据源：
                  1、 int_d_hrp_tg_element_info            指标信息表
         目标表：
                  1、 t_index_pool_input    指标池表
                  2、 t_index_pool          单项指标代码表
         修改记录;
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         苏彦运      2023/09/12                 创建
         MG          2023/09/28                 格式调整，提交V18版本
         MG          2023/10/17                 指标代码修改
************************************************************************************************************************************************/


DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);


-- 设置变量
set V_EVENT_NAME = '托管中心指标代码转换';
set V_RUN_COMMAND = concat('call PRO_LOAD_HRP_TG_ZBDM(',BIZ_DATE,')');
select now() into V_START_TIME;


-- 只增更新不删除
-- 1. 插入新数据
insert into t_index_pool_input(
    index_name,                   -- 指标名称
    index_code,                   -- 指标代码
    dept_id,                      -- 部门id
    origin_index_code,            -- 原始指标代码
    origin_index_name,            -- 原始指标名称
    origin_system,                -- 来源系统
    status,                       -- 状态
    remark1,                      -- 备用字段1
    remark2,                      -- 备用字段2
    creator,                      -- 创建人
    create_time,                  -- 创建时间
    updater,                      -- 修改人
    update_time,                  -- 修改时间
    deleted,                      -- 删除标识
    supply_cycle,                 -- 供数周期
    supply_frequency,             -- 供数模式
    index_unit,                   -- 指标单位
    latest_status                 -- 最新状态(0:最新，1：非最新)
)
select 
    a.element_name             index_name,                                     -- 指标名称
    concat(ifnull(a.element_key,''),'_',ifnull(a.stats_period,''),'_',
    ifnull(a.value_type,''),'_',ifnull(a.element_unit,''))    index_code,     -- 指标代码(联合指标值)
    13400                      dept_id,                                        -- 部门id   
    a.element_key              origin_index_code,                              -- 原始指标代码
    a.element_name             origin_index_name,                              -- 原始指标名称
    '3'                        origin_system,                                  -- 来源系统
    0                          status,                                         -- 状态
    null                       remark1,                                        -- 备用字段1
    '新增指标'                 remark2,                                        -- 备用字段2
    1                          creator,                                        -- 创建人
    sysdate()                  create_time,                                    -- 创建时间
    1                          updater,                                        -- 修改人
    sysdate()                  update_time,                                    -- 修改时间
    0                          deleted,                                        -- 删除标识
    a.stats_period             supply_cycle,                                   -- 供数周期
    a.value_type               supply_frequency,                               -- 供数模式
    a.element_unit             index_unit,                                     -- 指标单位
    0                          latest_status                                   -- 最新状态(0:最新，1：非最新)
from int_d_hrp_tg_element_info a
left join t_index_pool_input b 
    on b.origin_system = '3'
    and a.element_key = b.origin_index_code
    and a.stats_period  = b.supply_cycle
    and a.value_type  = b.supply_frequency
    and ifnull(a.element_unit ,'') = ifnull(b.index_unit,'')
where b.index_code is null
;

-- 2. 将历史的【最新状态】改为1
update t_index_pool_input b 
inner join int_d_hrp_tg_element_info a
on b.origin_system = '3' and a.element_key = b.origin_index_code
set b.latest_status = 1 -- 最新状态(0:最新，1：历史状态)
    ,b.index_name = a.element_name -- 指标名称
    ,b.origin_index_name = a.element_name -- 原始指标名称
    ,b.updater = 1
    ,b.update_time = sysdate()
    ,b.remark2 = '该指标的周期、模式或单位发生了变化，该联合指标值不统计完成值'
where b.latest_status = 0
  and (a.stats_period != b.supply_cycle
       or a.value_type != b.supply_frequency
       or ifnull(a.element_unit ,'') != ifnull(b.index_unit,'')
       )
;


-- 3. 修改后再恢复
-- 比如 A指标，单位是1，改为2，然后再改为1
update t_index_pool_input b 
inner join int_d_hrp_tg_element_info a
on b.origin_system = '3' and a.element_key = b.origin_index_code
set b.latest_status = 0 -- 最新状态(0:最新，1：历史状态)
    ,b.index_name = a.element_name -- 指标名称
    ,b.origin_index_name = a.element_name -- 原始指标名称
    ,b.updater = 1
    ,b.update_time = sysdate()
    ,b.remark2 = concat(b.remark2,'-取值恢复')
where b.latest_status = 1
  and (a.stats_period = b.supply_cycle
       and a.value_type = b.supply_frequency
       and ifnull(a.element_unit ,'') = ifnull(b.index_unit,'')
       )
;


-- 更新【指标名称】
update t_index_pool_input b 
inner join int_d_hrp_tg_element_info a
on b.origin_system = '3' and a.element_key = b.origin_index_code
set b.index_name = a.element_name -- 指标名称
    ,b.origin_index_name = a.element_name -- 原始指标名称
    ,b.updater = 1
    ,b.update_time = sysdate()
    ,b.remark2 = concat('指标名称修改,修改前指标名称为：【',a.element_name,'】,操作时间为:',sysdate())
where b.index_name != a.element_name
;


update t_index_pool a 
inner join t_index_pool_input b 
on a.related_index_code = b.index_code
and b.origin_system = '3'
set a.index_name = b.index_name
    ,a.update_time = sysdate()
where a.index_name != b.index_name
;

-- 获得记录数
select count(1) cnt into V_TABLE_CNT from t_index_pool_input;-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,BIZ_DATE,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0);


END
;