DROP PROCEDURE IF EXISTS PRO_ANNUAL_ASSESS_USER;
CREATE PROCEDURE `PRO_ANNUAL_ASSESS_USER`(IN IN_PAR_YEAR CHAR(4))
label:BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_ANNUAL_ASSESS_USER
         功能简述：   年度考核获取用户信息
         参数：       IN_PAR_YEAR    年份
         注意事项：
                 1. 列定时间范围每年0915-1015，这段时间的人员数据归档
                    如当前为20240115（数据生成时间），进行23年年度考核，选择考核时点20231001（考核判定时间），则人员名单如下：
                         20240115当天状态正常的全体员工（剔除24年以后入职的，剔除个别部门，见下图）；
                         20231002至20240115离退人员；
                    获取人员名单后，对23年考核人员名单进行列定，同时对人员打上标签
                         离退人员：考核判定时间至数据生成时间的离退人员；
                       10月后调动：数据生成时间员工人事部门与考核判定时间认识部门不一致；
                       除上述两类人在系统初始化年度考核人员名单时打上标签，其余人员默认“--”。
                 2. 不能考核当年的年度考核
                 3. 节假日不列定数据
                 
                 一般需要在次年1月15日跑数据
         数据源：
                 sys_user  用户表
         结果表：
                 t_annual_assess_user 考核人员总览(参评名单)
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG          2023/11/20                 创建
         MG          2023/11/23                 获取是针对所有的部门
         MG          2023/12/11                 提测V21版本
         MG          2023/12/12                 移除人员类型和管理人员标识过滤条件
         MG          2023/12/20                 年份参数初始化值的位置存在问题
         MG          2023/12/26                 组织调整，人员标签修改
         MG          2024/01/17                 目标表新增4个二级部门相关字段
******************************************************************************************************************************************/

-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);
DECLARE V_BATCH_NO varchar(200);
DECLARE V_TOTAL_STEP_NUM int;

-- 设置变量
set V_EVENT_NAME = '年度考核获取用户信息';
set V_TOTAL_STEP_NUM = 4;  -- 设置总步骤数
set @remark = ''; -- 步骤日志中的备注，可以每步都定义
select now() into V_START_TIME;


-- 解决字符集问题：Illegal mix of collations (utf8mb4_0900_ai_ci,IMPLICIT) and (utf8mb4_general_ci,IMPLICIT) for operation
set collation_connection = utf8mb4_general_ci;


set @par_year = IN_PAR_YEAR;   -- 格式：2023

-- 获取考核判定时间
select assess_date into @assess_date from t_annual_assess_date where par_year = @par_year;

-- 获取数据生成时间
select create_date into @create_date from t_annual_assess_date where par_year = @par_year;



-- set @current_year = date_format('2024-02-10','%Y'); -- 格式2024
-- set @current_year = date_format(current_date,'%Y'); -- 格式2024



set V_RUN_COMMAND = concat('call PRO_ANNUAL_ASSESS_USER('
                            ,IN_PAR_YEAR
                            ,')'
                         );

set V_BATCH_NO = concat(UNIX_TIMESTAMP(),'-'
                        ,replace(V_RUN_COMMAND,'call ','')
                        ,'-'
                        ,V_TOTAL_STEP_NUM
                     );


-- -- 基础检查
-- if @par_year >= @current_year then
--     select '传入参数【考核年份】必须是当年之前的年份';
--     LEAVE label; -- 退出存储过程 
-- end if;



-- 写步骤日志
set @step_info = '1.进度步骤表:初始化数据';
set @remark = '步骤表初始化数据';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);
set @remark = '';




delete from t_annual_assess_step_total_dept where par_year = @par_year;
insert into t_annual_assess_step_total_dept(
par_year
,assess_date
,create_date
,step_id
,status
,remark
,creator
,create_time
,updater
,update_time
)
-- 1. 生成人员名单
select 
    @par_year par_year
    ,@assess_date assess_date
    ,@create_date create_date
    ,1 step_id
    ,0 status
    ,'生成人员名单、生成默认考核组' remark
    ,1 creator
    ,now() create_time
    ,1 updater
    ,now() update_time
union all 
-- 2. 生成考核进度、开启查询
select 
    @par_year par_year
    ,@assess_date assess_date
    ,@create_date create_date
    ,2 step_id
    ,0 status
    ,'生成考核进度、开启查询' remark
    ,1 creator
    ,now() create_time
    ,1 updater
    ,now() update_time
;


-- 写步骤日志
set @step_info = '2.年度考核用户表:插入用户表数据';
set @remark = '用户表数据初始化';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);
set @remark = '';


/*

2、年度考核的考评对象范围需要先按照人员类别筛选、再按照管理人员属性筛选。
规则为
（1）根据人员类别，将0101在岗人员、0106外派人员、0201全日制营销人员、0301劳务派遣人员、0303外包人员，纳入名单；
（请在页面展示“人员类别”字段时，统一将0301劳务派遣人员、0303外包人员显示为"劳务人员")
（2）根据管理人员属性，将013总部二级部门经理、014分公司二级部门经理、016营业部正职（卫星）、018营业部副职、019营业部助理、020非行政职务、022干部退岗、023员工，纳入名单。


*/


delete from t_annual_assess_user where par_year = @par_year;

-- 当天状态正常的全体员工（剔除10月1号以后入职的，剔除个别部门）；
insert into t_annual_assess_user(par_year,dept_id,user_id,user_name,pd_hr_dept_id,Managercode,Manager_status,account_status,org_type
    ,pd_Org_Sec_Code,pd_Org_Sec_Name,Org_Sec_Code,Org_Sec_Name
    ,user_create_time,enddate
    ,pd_emp_type_code,emp_tag_code,remark,creator,create_time,updater,update_time)
select 
     @par_year                         par_year
    ,ifnull(d.parent_id,u.dept_id)     dept_id      -- 卫星营业部默认考核部门为其中心营业部
    ,h.user_id                         user_id
    ,h.user_name                       user_name
    ,h.dept_id                         pd_hr_dept_id
    ,h.Managercode                     managercode
    ,h.Manager_status                  Manager_status
    ,h.account_status                  account_status
    ,dd.org_type                       org_type
    ,h.Org_Sec_Code                    pd_Org_Sec_Code  -- 数据判定时间的二级部门
    ,h.Org_Sec_Name                    pd_Org_Sec_Name
    ,u.Org_Sec_Code                    Org_Sec_Code     -- 数据生成时间的二级部门
    ,u.Org_Sec_Name                    Org_Sec_Name
    ,u.create_time                     user_create_time
    ,h.enddate                         enddate
    ,h.Emp_Type_Code                   pd_emp_type_code
    ,case when u.account_status = 1 then 7  -- 20231002至20240115离退人员
          -- when u.account_status = 3 then 8  -- 10月后调动：数据生成时间员工人事部门与考核判定时间认识部门不一致；
          else 0 end                   emp_tag_code
    ,'数据初始化'                      remark
    ,1                                 creator
    ,now()                             create_time
    ,1                                 updater
    ,now()                             update_time
from t_annual_assess_emp_his h 
left join sys_user u
    on u.user_id = h.user_id
left join sys_dept d 
    on u.dept_id = d.dept_id 
    and d.Fic_Org = '1'  -- 卫星营业部
left join sys_dept dd
    on u.dept_id = dd.dept_id
where h.assess_date = @assess_date
    -- and u.account_flag = 1
    and h.account_status = 0 -- 不能是离职和组织调整
    and ifnull(d.parent_id,u.dept_id) not in (select dept_id from t_annual_assess_not_assess_dept where status = 0)
    -- and u.begindate < @assess_date
;



-- 更新当前人员HR部门和当前人员类型
update t_annual_assess_user a
left join sys_user u 
    on a.user_name = u.user_name   -- 不能用用户id
    and u.account_status = '0'
left join t_employee e 
    on a.user_name = e.code
    and e.status = 0
set a.hr_dept_id = u.dept_id
    ,a.emp_type_code = (case when e.Emp_Type_Code in ('0301','0303') then '030103' else e.Emp_Type_Code end)
where a.par_year = @par_year 
;



update t_annual_assess_user a
set a.emp_tag_code = 2 -- 组织调整
where a.par_year = @par_year 
and a.hr_dept_id != a.pd_hr_dept_id
;




-- 写步骤日志
set @step_info = '3.年度考核:更新进度表';
set @remark = '更新进度表';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);
set @remark = '';


-- 更新进度表
update t_annual_assess_step_total_dept 
set status = 1
where par_year = @par_year
and step_id = 1
;




-- 获得记录数
select 0 cnt into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;