DROP PROCEDURE IF EXISTS PRO_WRITELOG_FI_YJHD;
CREATE PROCEDURE `PRO_WRITELOG_FI_YJHD`(
IN `I_NAME` varchar(200), -- 事件名称
IN `I_CMD` varchar(1000),  -- 执行命令
IN `I_STAT_SCOPE` varchar(10), -- 统计范围
IN `I_ERRTYPE` tinyint,  -- 错误类型 错误类型，0|提示;1|一般错误;2|严重错误
IN `I_ERRMSG` varchar(2000),
IN `I_ROWS` int,
`I_TIMES` int
)
BEGIN
	 /*****************************************************************************
    *
    *项目名称：申万宏源金创业绩系统项目
    *创建人员：1
    *创建日期：20231009
    *环境：mysql库
    *功能说明：部门团队业绩指标日志记录
    * 参数说明
        @para1: 事件名称
        @para2: 执行命令
        @para3: 错误类型
        @para4: 执行结果(可以得到记录数和秒)
        @para5: 记录数(写0，则从执行结果中取，否则直取)
        @para6: 执行秒数(写0，则从执行结果中取，否则直取)
    * 执行案例
        call PRO_WRITELOG_HRJX('事件名称','执行命令',0,'执行成功[记录:22200],用时500秒',0,0)
        call PRO_WRITELOG_HRJX('事件名称','执行命令',0,'执行成功',11100,55)
        select * from t_procedure_log;
    *-----------------------------------------------------------------------------
    *
    *操作人        版本号        修改日期        说明
		 苏彦运      								 2023/10/09      创建
    * 
		*
    ******************************************************************************/
   DECLARE V_ROWS int;   -- 变量
   DECLARE V_TIMES int;   -- 变量
   -- DECLARE I_ROWS int default 0;   -- 传入的参数
   -- DECLARE I_TIMES int default 0;  -- 传入的参数 

   set V_ROWS = 1;
   
   BEGIN
       IF (I_ROWS = 0 AND INSTR(I_ERRMSG, '记录:') > 0) THEN
           set V_ROWS = SUBSTR(I_ERRMSG, INSTR(I_ERRMSG, '记录:') + 3,
                                     INSTR(I_ERRMSG, ']') - INSTR(I_ERRMSG, '记录:') - 3);
       ELSE 
           set V_ROWS = I_ROWS;
       END IF;
   END;
   
   BEGIN
       IF (I_TIMES = 0.0 AND INSTR(I_ERRMSG, '用时') > 0) THEN 
           set V_TIMES = SUBSTR(I_ERRMSG, INSTR(I_ERRMSG, '用时') + 2,
                               INSTR(I_ERRMSG, '秒') - INSTR(I_ERRMSG, '用时') - 2);
       ELSE 
           set V_TIMES = I_TIMES;
       END IF;
   END;

     -- 写入日志表记录
     INSERT INTO t_procedure_log
         (data_date, data_time, STAT_SCOPE, ERRTYPE, ERRMSG, EVENT_NAME, CMD, ROWCOUNT, TIMES)
     VALUES
         (
          current_date() -- data_date -- 数据日期
         ,time(now())    -- data_time  -- 数据时间
				 ,I_STAT_SCOPE     -- biz_date   -- 统计范围
         ,I_ERRTYPE      -- ERRTYPE  -- 错误类型：0|提示;1|一般错误;2|严重错误
         ,I_ERRMSG       -- ERRMSG    -- 错误信息
         ,I_NAME         -- EVENT_NAME    -- 事件名称
         ,I_CMD          -- CMD    -- 执行命令
         ,V_ROWS         -- ROWCOUNT    -- 记录数
         ,V_TIMES        -- TIMES    -- 秒
      );
      -- commit;
   -- 调度存储过程
   -- call PRO_WRITELOG_HRJX('事件名称','执行命令',0,'执行成功[记录:22200],用时500秒')

   -- select V_ROWS,V_TIMES;

END
;