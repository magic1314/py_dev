DROP PROCEDURE IF EXISTS PRO_ANNUAL_ASSESS_PROGRESS;
CREATE PROCEDURE `PRO_ANNUAL_ASSESS_PROGRESS`(IN IN_PAR_YEAR CHAR(4), IN IN_DEPT_ID INT, IN IN_UPDATE_TYPE INT)
label:BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_ANNUAL_ASSESS_PROGRESS
         功能简述：   考核进度初始化
         参数：       IN_PAR_YEAR     年份
                      IN_DEPT_ID      部门(0：全部门，其他值：指定目标)
                      IN_UPDATE_TYPE  更新类型(0:新增，1:回收， 2:开启查询 3:关闭查询)
         注意事项：
                      1. 【年度考核进度管理】模块，点击【0 新增】/【1 回收】/【2 开启查询】/【3 关闭查询】
                      2. 进度表和进度考核组表状态：【0 未开启】【1 考核中】【2 已提交】【3 已冻结】【4 已开放】【5 审批通过】
         数据源：
                 t_annual_assess_group                考核组
                 t_annual_assess_group_user           考核组成员
                 t_annual_assess_dept_result          部门考核等级录入结果
                 t_annual_assess_dept_proportion      考核等级主表
                 t_annual_assess_dept_proportion_dtl  考核等级明细
                 t_annual_assess_dept_proportion_sp   考核等级特殊-人数小于或等于3
         结果表：
                 t_annual_assess_user                 考核人员总览
                 t_annual_assess_progress             考核进度表
                 t_annual_assess_progress_group       考核进度考核组
                 t_annual_assess_progress_group_dtl   考核进度考核组明细
                 t_employee_kpi_info                  员工kpi表
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG          2023/11/27                 创建
         MG          2023/12/01                 进度表调整
         MG          2023/12/08                 开启查询后将员工kpi数据插入到【t_employee_kpi_info】表中
         MG          2023/12/11                 提测V21版本
         MG          2023/12/13                 初始化强制分布人数上限默认状态为不通过
                                                dept_id=1000,会初始化强制分布上限人数表的数据，且人数不会限制
         MG          2023/12/21                 回收时，重新初始化3张进度表的数据
         MG          2023/12/22                 总人数<=3或总人数>3的情况下，需要过滤掉dept_id=1000的情况
         MG          2023/12/25                 回收时，删除用户录入的数据
         MG          2024/01/17                 新增关闭查询的功能
         MG          2024/01/25                 tmp2修改命名
         MG          2024/01/26                 部分优化，百分比四舍五入
         MG          2024/01/29                 百分比四舍五入逻辑补充
         MG          2024/01/30                 回收时，删除用印上传的附件
         MG          2024/02/26                 【xx-营业部副职组】和【xx-全日制营销组】两类强制分布组的
                                                     比例后台直接按照领导班子考核结果C等次对应的比例生成人数上限。
                                                优化部分内容
         MG          2024/05/06                 系统用户表调整为年度用户表
         MG          2024/06/21                 kpi表原始考核等级用字典中的码值
******************************************************************************************************************************************/

-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);
DECLARE V_BATCH_NO varchar(200);
DECLARE V_TOTAL_STEP_NUM int;

-- 设置变量
set V_EVENT_NAME = '年度考核进度生成';
set V_TOTAL_STEP_NUM = 1;  -- 设置总步骤数
set @remark = ''; -- 步骤日志中的备注，可以每步都定义
select now() into V_START_TIME;



set @par_year = IN_PAR_YEAR;   -- 格式：2023
set @dept_id = IN_DEPT_ID;   -- 格式： 12800
set @update_type = IN_UPDATE_TYPE;  -- 格式： 1


set V_RUN_COMMAND = concat('call PRO_ANNUAL_ASSESS_PROGRESS('
                            ,IN_PAR_YEAR,','
                            ,IN_DEPT_ID,','
                            ,IN_UPDATE_TYPE
                            ,')'
                         );

set V_BATCH_NO = concat(   UNIX_TIMESTAMP(),'-'
                        ,replace(V_RUN_COMMAND,'call ',''),'-'
                        ,V_TOTAL_STEP_NUM
                     );


-- 所有部门：强制分布人数上限表
-- 注：考虑把部门考核等级加入
drop table if exists tmp_result;
CREATE TEMPORARY TABLE `tmp_result` (
  `id` bigint NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `par_year` char(4) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '年度(冗余字段)',
  `dept_id` bigint DEFAULT NULL COMMENT '考核部门id(冗余字段)',
  `group_id` bigint DEFAULT NULL COMMENT '考核组id',
  `dept_assess_level` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '部门考核等级(A/B/C/D)',
  `user_assess_level` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '员工考核等级',
  `theory_people_limit` int DEFAULT NULL COMMENT '各考核等次员工人数上限',
  `actual_people_limit` int DEFAULT null COMMENT '实际录入人数-不生成数据',
  `status` tinyint DEFAULT '0' COMMENT '强制分布校验状态(0-异常，1-正常)',
  `remark` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '备注',
  `creator` bigint DEFAULT '1' COMMENT '创建人',
  `create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updater` bigint DEFAULT '1' COMMENT '修改人',
  `update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '最近一次修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `idx_par_year` (`par_year`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='年度考核组明细-强制分布人数上限表'
;


-- 每个组的总人数
drop table if exists tmp_annual_assess_group_user;
create TEMPORARY table tmp_annual_assess_group_user
select par_year,group_id,count(distinct user_id) group_cnt 
from t_annual_assess_group_user
where par_year = @par_year
group by par_year,group_id
;


-- 每个考核组，按部门考核等级，计算出成员考核等级的百分比
drop table if exists tmp1;
create TEMPORARY table tmp1
select 
     a.par_year
    ,a.dept_id
    ,a.id group_id
    ,t3.group_cnt
    ,(case when group_name like '%营业部副职组' or group_name like '%全日制营销组' then 'C'
     else b.dept_assess_level end) dept_assess_level
    ,t2.user_assess_level
    ,t2.proportion
    ,'后台数据初始化' remark
    ,1 creator
    ,now() create_time
    ,1 updater
    ,now() update_time
from t_annual_assess_group a
left join t_annual_assess_dept_result b 
    on a.par_year = b.par_year
    and a.dept_id = b.dept_id
left join t_annual_assess_dept_proportion t1
    on a.par_year = t1.par_year
    and b.dept_assess_level = t1.dept_assess_level
left join t_annual_assess_dept_proportion_dtl t2 
    on t1.id = t2.annual_assess_dept_proportion_id
left join tmp_annual_assess_group_user t3
    on a.par_year = t3.par_year 
    and a.id = t3.group_id
where a.par_year = @par_year
;




/**************** 不超过3人，各等级的占比 ****************/
-- 方案中参与强制分布总人数小于或等于3时
insert into tmp_result(par_year,dept_id,group_id,dept_assess_level,user_assess_level,theory_people_limit,actual_people_limit
,status,remark
) 
select 
     a.par_year
    ,a.dept_id 
    ,a.group_id 
    ,a.dept_assess_level
    ,a.user_assess_level
    ,sp.people_cnt_limit theory_people_limit
    ,null actual_people_limit
    ,1 status   -- 默认不通过
    ,'后台数据初始化-强制分布总人数小于或等于3' remark
from tmp1 a
left join t_annual_assess_dept_proportion_sp sp 
    on a.group_cnt = sp.total_people_cnt
    and a.user_assess_level = sp.user_assess_level
    and a.par_year = sp.par_year
where a.group_cnt <= 3
and a.dept_id != 1000
;


-- 特殊处理：dept_id=1000不会录入部门等级，上限表不会限制人数
insert into tmp_result(par_year,dept_id,group_id,user_assess_level,theory_people_limit,actual_people_limit
,status,remark
) 
select 
     a.par_year
    ,a.dept_id 
    ,a.id group_id 
    ,b.user_assess_level
    ,null theory_people_limit
    ,null actual_people_limit
    ,0 status   -- dept_id=1000 不限制员工等级的人数
    ,'后台数据初始化-1000总公司特殊处理' remark
from t_annual_assess_group a
,(
    select 1 seq, 'A' user_assess_level union all
    select 2 seq, 'B' user_assess_level union all
    select 3 seq, 'C' user_assess_level union all
    select 4 seq, 'D/E' user_assess_level
) b
where a.par_year = @par_year
and a.dept_id = 1000
order by b.seq
;


/**************** 超过3人，各等级的占比 ****************/
-- 这里不能使用临时表，临时表不能反复调用
drop table if exists tmp_annual_assess_dept_proportion;
create TEMPORARY table tmp_annual_assess_dept_proportion
select 
    a.par_year 
    ,a.dept_assess_level
    ,b.user_assess_level
    ,b.proportion
from t_annual_assess_dept_proportion a 
left join t_annual_assess_dept_proportion_dtl b 
on a.id = b.annual_assess_dept_proportion_id
where a.par_year = @par_year
;


-- 累计占比
drop table if exists tmp_annual_assess_dept_proportion_accu;
CREATE TEMPORARY TABLE `tmp_annual_assess_dept_proportion_accu` (
  `par_year` char(4) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '年度',
  `dept_assess_level` varchar(5) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '部门考核等级(A/B/C/D)',
  `user_assess_level` varchar(5) COLLATE utf8mb4_general_ci NOT NULL DEFAULT '',
  `proportion_accu` decimal(10,2) DEFAULT NULL COMMENT '分布上限百分比'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci
;


insert into tmp_annual_assess_dept_proportion_accu(par_year,dept_assess_level,user_assess_level,proportion_accu)
select par_year,dept_assess_level,'A' user_assess_level,proportion proportion_accu
from tmp_annual_assess_dept_proportion
where user_assess_level = 'A'
;

insert into tmp_annual_assess_dept_proportion_accu
select par_year,dept_assess_level,'B' user_assess_level,sum(proportion) proportion_accu 
from tmp_annual_assess_dept_proportion
where user_assess_level in ('A','B')
group by par_year,dept_assess_level
;

insert into tmp_annual_assess_dept_proportion_accu
select par_year,dept_assess_level,'C' user_assess_level,sum(proportion) proportion_accu 
from tmp_annual_assess_dept_proportion
where user_assess_level in ('A','B','C')
group by par_year,dept_assess_level
;


insert into tmp_annual_assess_dept_proportion_accu
select par_year,dept_assess_level,'D/E' user_assess_level,sum(proportion) proportion_accu 
from tmp_annual_assess_dept_proportion
where user_assess_level in ('A','B','C','D/E')
group by par_year,dept_assess_level
;


-- 这个不能用临时表
drop table if exists tmp_annual_assess_other;
CREATE TABLE `tmp_annual_assess_other` (
  `par_year` char(4) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '年度',
  `dept_id` bigint DEFAULT NULL COMMENT '考核部门编码',
  `group_id` bigint DEFAULT NULL COMMENT '考核组id',
  `dept_assess_level` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '部门考核等级(A/B/C/D)',
  `user_assess_level` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '员工考核等级',
  `group_cnt` bigint DEFAULT '0',
  `proportion` decimal(10,2) DEFAULT NULL COMMENT '分布上限百分比',
  `theory_people_limit` int DEFAULT NULL comment '当前等级上限人数',
  accu_people_limit int comment '累计的上限人数'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '临时表-考核组超过3人'
;


insert into tmp_annual_assess_other(
    par_year
    ,dept_id
    ,group_id
    ,dept_assess_level
    ,user_assess_level
    ,group_cnt
    ,proportion
    ,theory_people_limit
    ,accu_people_limit)
-- 人数上限（A）=参与强制分布总人数*班子考核结果A
select 
     a.par_year
    ,a.dept_id 
    ,a.group_id 
    ,a.dept_assess_level
    ,a.user_assess_level
    ,a.group_cnt   -- 参与强制分布总人数
    ,a.proportion   -- 班子考核结果A
    ,round(a.group_cnt * a.proportion / 100) theory_people_limit  -- 人数上限(A)
    ,round(a.group_cnt * a.proportion / 100) accu_people_limit 
from tmp1 a
left join tmp_annual_assess_dept_proportion_accu tt
    on a.par_year = tt.par_year
    and a.dept_assess_level = tt.dept_assess_level
    and a.user_assess_level = tt.user_assess_level
where a.group_cnt > 3
and a.user_assess_level = 'A'
;

insert into tmp_annual_assess_other(
    par_year
    ,dept_id
    ,group_id
    ,dept_assess_level
    ,user_assess_level
    ,group_cnt
    ,proportion
    ,theory_people_limit
    ,accu_people_limit
)
-- 人数上限（B）=参与强制分布总人数*（班子考核结果A+B）-人数上限（A）
select 
     a.par_year
    ,a.dept_id 
    ,a.group_id 
    ,a.dept_assess_level
    ,a.user_assess_level
    ,a.group_cnt   -- 参与强制分布总人数
    ,tt.proportion_accu   -- 班子考核结果A+B
    ,round(a.group_cnt * tt.proportion_accu / 100) - x.accu_people_limit theory_people_limit  -- 人数上限(B)
    ,round(a.group_cnt * tt.proportion_accu / 100) accu_people_limit    -- 人数上限(A+B)
from tmp1 a
left join tmp_annual_assess_dept_proportion_accu tt
    on a.par_year = tt.par_year
    and a.dept_assess_level = tt.dept_assess_level
    and a.user_assess_level = tt.user_assess_level
left join tmp_annual_assess_other x 
    on a.par_year = x.par_year 
    and a.dept_id = x.dept_id 
    and a.group_id = x.group_id 
    and x.user_assess_level = 'A'
where a.group_cnt > 3
and a.user_assess_level = 'B'
;



insert into tmp_annual_assess_other(
    par_year
    ,dept_id
    ,group_id
    ,dept_assess_level
    ,user_assess_level
    ,group_cnt
    ,proportion
    ,theory_people_limit
    ,accu_people_limit)
-- 人数上限（C）=参与强制分布总人数*（班子考核结果A+B+C）-人数上限（A+B）
select 
     a.par_year
    ,a.dept_id 
    ,a.group_id 
    ,a.dept_assess_level
    ,a.user_assess_level
    ,a.group_cnt   -- 参与强制分布总人数
    ,tt.proportion_accu   -- 班子考核结果A+B+C
    ,round(a.group_cnt * tt.proportion_accu / 100) - x.accu_people_limit theory_people_limit  -- 人数上限(C)
    ,round(a.group_cnt * tt.proportion_accu / 100) accu_people_limit -- 人数上限(A+B+C)
from tmp1 a
left join tmp_annual_assess_dept_proportion_accu tt
    on a.par_year = tt.par_year
    and a.dept_assess_level = tt.dept_assess_level
    and a.user_assess_level = tt.user_assess_level
left join tmp_annual_assess_other x 
    on a.par_year = x.par_year 
    and a.dept_id = x.dept_id 
    and a.group_id = x.group_id 
    and x.user_assess_level = 'B'
where a.group_cnt > 3
and a.user_assess_level = 'C'
;




insert into tmp_annual_assess_other(
    par_year
    ,dept_id
    ,group_id
    ,dept_assess_level
    ,user_assess_level
    ,group_cnt
    ,proportion
    ,theory_people_limit
    ,accu_people_limit
)
-- 人数上限（D/E）=参与强制分布总人数-人数上限（A+B+C）
select 
     a.par_year
    ,a.dept_id 
    ,a.group_id 
    ,a.dept_assess_level
    ,a.user_assess_level
    ,a.group_cnt   -- 参与强制分布总人数
    ,tt.proportion_accu   -- 班子考核结果A+B+C
    ,round(a.group_cnt * tt.proportion_accu / 100) - x.accu_people_limit theory_people_limit  -- 人数上限(D/E)
    ,round(a.group_cnt * tt.proportion_accu / 100) accu_people_limit -- 人数上限(A+B+C+D/E)
from tmp1 a
left join tmp_annual_assess_dept_proportion_accu tt
    on a.par_year = tt.par_year
    and a.dept_assess_level = tt.dept_assess_level
    and a.user_assess_level = tt.user_assess_level
left join tmp_annual_assess_other x 
    on a.par_year = x.par_year 
    and a.dept_id = x.dept_id 
    and a.group_id = x.group_id 
    and x.user_assess_level = 'C'
where a.group_cnt > 3
and a.user_assess_level = 'D/E'
;


insert into tmp_result(
    par_year
    ,dept_id
    ,group_id
    ,dept_assess_level
    ,user_assess_level
    ,theory_people_limit
    ,actual_people_limit
    ,status
    ,remark
) 
select 
     a.par_year
    ,a.dept_id 
    ,a.group_id 
    ,a.dept_assess_level
    ,a.user_assess_level
    ,a.theory_people_limit
    ,null actual_people_limit
    ,1 status  -- 默认不通过
    ,'后台数据初始化-强制分布总人数超过3人' remark
from tmp_annual_assess_other a
where a.dept_id != 1000
;




if @update_type = 0 then 
    if @dept_id > 0 then
        delete from t_annual_assess_progress where par_year = @par_year and dept_id = @dept_id;
        insert into t_annual_assess_progress(par_year,dept_id,status,remark,creator,create_time,updater,update_time)
        select
            distinct 
            par_year
            ,dept_id
            ,0 status
            ,'数据初始化' remark
            ,1 creator
            ,now() create_time
            ,1 updater
            ,now() update_time
        from t_annual_assess_group
        where par_year = @par_year
        and dept_id = @dept_id
        ;

        delete from t_annual_assess_progress_group where par_year = @par_year and dept_id = @dept_id;
        insert into t_annual_assess_progress_group(annual_assess_progress_id,par_year,dept_id,group_id,status,remark,creator,create_time,updater,update_time)
        select 
             distinct 
             b.id annual_assess_progress_id
            ,a.par_year
            ,a.dept_id
            ,a.id group_id
            ,0 status
            ,'数据初始化' remark
            ,1 creator
            ,now() create_time
            ,1 updater
            ,now() update_time
        from t_annual_assess_group a
        left join t_annual_assess_progress b 
            on a.par_year = b.par_year
            and a.dept_id = b.dept_id
        where a.par_year = @par_year
        and a.dept_id = @dept_id
        ;
        
        
        delete from t_annual_assess_progress_group_dtl where par_year = @par_year and dept_id = @dept_id;
        insert into t_annual_assess_progress_group_dtl(annual_assess_progress_group_id,par_year,dept_id,group_id,user_assess_level,theory_people_limit
            ,actual_people_limit,status,remark,creator,create_time,updater,update_time) 
        select 
             b.id annual_assess_progress_group_id
            ,a.par_year
            ,a.dept_id 
            ,a.group_id 
            ,a.user_assess_level
            ,a.theory_people_limit
            ,a.actual_people_limit
            ,a.status
            ,a.remark
            ,a.creator
            ,a.create_time
            ,a.updater
            ,a.update_time
        from tmp_result a
        left join t_annual_assess_progress_group b 
            on a.par_year = b.par_year
            and a.dept_id = b.dept_id
            and a.group_id = b.group_id
        where a.par_year = @par_year
        and a.dept_id = @dept_id
        ;

    else
        delete from t_annual_assess_progress where par_year = @par_year;
        insert into t_annual_assess_progress(par_year,dept_id,status,remark,creator,create_time,updater,update_time)
        select
            distinct 
            par_year
            ,dept_id
            ,0 status
            ,'数据初始化' remark
            ,1 creator
            ,now() create_time
            ,1 updater
            ,now() update_time
        from t_annual_assess_group
        where par_year = @par_year
        ;

        delete from t_annual_assess_progress_group where par_year = @par_year;
        insert into t_annual_assess_progress_group(annual_assess_progress_id,par_year,dept_id,group_id,status,remark,creator,create_time,updater,update_time)
        select 
             distinct 
             b.id annual_assess_progress_id
            ,a.par_year
            ,a.dept_id
            ,a.id group_id
            ,0 status
            ,'数据初始化' remark
            ,1 creator
            ,now() create_time
            ,1 updater
            ,now() update_time
        from t_annual_assess_group a
        left join t_annual_assess_progress b 
            on a.par_year = b.par_year
            and a.dept_id = b.dept_id
        where a.par_year = @par_year
        ;
        
        delete from t_annual_assess_progress_group_dtl where par_year = @par_year;
        insert into t_annual_assess_progress_group_dtl(annual_assess_progress_group_id,par_year,dept_id,group_id,user_assess_level,theory_people_limit
            ,actual_people_limit,status,remark,creator,create_time,updater,update_time) 
        select 
             b.id annual_assess_progress_group_id
            ,a.par_year
            ,a.dept_id 
            ,a.group_id 
            ,a.user_assess_level
            ,a.theory_people_limit
            ,a.actual_people_limit
            ,a.status
            ,a.remark
            ,a.creator
            ,a.create_time
            ,a.updater
            ,a.update_time
        from tmp_result a
        left join t_annual_assess_progress_group b 
            on a.par_year = b.par_year
            and a.dept_id = b.dept_id
            and a.group_id = b.group_id
        where a.par_year = @par_year
        ;
        
    end if
    ;
elseif @update_type = 1 then    -- 回收功能
    if @dept_id > 0 then 
        update t_annual_assess_progress 
        set status = 0
            ,remark = concat('回收该部门，后台操作时间：',now())
        where par_year = @par_year 
            and dept_id = @dept_id
        ;
        
        delete from t_annual_assess_progress_group where par_year = @par_year and dept_id = @dept_id;
        insert into t_annual_assess_progress_group(annual_assess_progress_id,par_year,dept_id,group_id,status,remark,creator,create_time,updater,update_time)
        select 
             distinct 
             b.id annual_assess_progress_id
            ,a.par_year
            ,a.dept_id
            ,a.id group_id
            ,0 status
            ,'数据初始化' remark
            ,1 creator
            ,now() create_time
            ,1 updater
            ,now() update_time
        from t_annual_assess_group a
        left join t_annual_assess_progress b 
            on a.par_year = b.par_year
            and a.dept_id = b.dept_id
        where a.par_year = @par_year
        and a.dept_id = @dept_id
        ;
        
        
        delete from t_annual_assess_progress_group_dtl where par_year = @par_year and dept_id = @dept_id;
        insert into t_annual_assess_progress_group_dtl(annual_assess_progress_group_id,par_year,dept_id,group_id,user_assess_level,theory_people_limit
            ,actual_people_limit,status,remark,creator,create_time,updater,update_time) 
        select 
             b.id annual_assess_progress_group_id
            ,a.par_year
            ,a.dept_id 
            ,a.group_id 
            ,a.user_assess_level
            ,a.theory_people_limit
            ,a.actual_people_limit
            ,a.status
            ,a.remark
            ,a.creator
            ,a.create_time
            ,a.updater
            ,a.update_time
        from tmp_result a
        left join t_annual_assess_progress_group b 
            on a.par_year = b.par_year
            and a.dept_id = b.dept_id
            and a.group_id = b.group_id
        where a.par_year = @par_year
        and a.dept_id = @dept_id
        ;
        
        -- 删除用户录入的数据
        delete from t_annual_assess_user_imp where par_year = @par_year and dept_id = @dept_id;
        
        -- 删除用印上传的附件
        delete from t_upload_relation where business_type=8 and business_id in(
            select group_id from t_annual_assess_progress_group where annual_assess_progress_id in(
               select id from t_annual_assess_progress where par_year=@par_year and dept_id=@dept_id
            )
        );
    else 
        
        update t_annual_assess_progress 
        set status = 0
            ,remark = concat('部门全部回收，后台操作时间：',now())
        where par_year = @par_year 
        ;
        
        delete from t_annual_assess_progress_group where par_year = @par_year;
        insert into t_annual_assess_progress_group(annual_assess_progress_id,par_year,dept_id,group_id,status,remark,creator,create_time,updater,update_time)
        select 
             distinct 
             b.id annual_assess_progress_id
            ,a.par_year
            ,a.dept_id
            ,a.id group_id
            ,0 status
            ,'数据初始化' remark
            ,1 creator
            ,now() create_time
            ,1 updater
            ,now() update_time
        from t_annual_assess_group a
        left join t_annual_assess_progress b 
            on a.par_year = b.par_year
            and a.dept_id = b.dept_id
        where a.par_year = @par_year
        ;
        
        delete from t_annual_assess_progress_group_dtl where par_year = @par_year;
        insert into t_annual_assess_progress_group_dtl(annual_assess_progress_group_id,par_year,dept_id,group_id,user_assess_level,theory_people_limit
            ,actual_people_limit,status,remark,creator,create_time,updater,update_time) 
        select 
             b.id annual_assess_progress_group_id
            ,a.par_year
            ,a.dept_id 
            ,a.group_id 
            ,a.user_assess_level
            ,a.theory_people_limit
            ,a.actual_people_limit
            ,a.status
            ,a.remark
            ,a.creator
            ,a.create_time
            ,a.updater
            ,a.update_time
        from tmp_result a
        left join t_annual_assess_progress_group b 
            on a.par_year = b.par_year
            and a.dept_id = b.dept_id
            and a.group_id = b.group_id
        where a.par_year = @par_year
        ;
        
        -- 删除用户录入的数据
        delete from t_annual_assess_user_imp where par_year = @par_year;
        
        -- 删除用印上传的附件
        delete from t_upload_relation where business_type=8 and business_id in(
            select group_id from t_annual_assess_progress_group where annual_assess_progress_id in(
               select id from t_annual_assess_progress where par_year=@par_year
            )
        );
    end if
    ;
elseif @update_type = 2 then  -- 开启查询
    if @dept_id > 0 then
        update t_annual_assess_progress
        set status = 4
        where par_year = @par_year
        and dept_id = @dept_id
        ;
        
        update t_annual_assess_progress_group
        set status = 4
        where par_year = @par_year
        and dept_id = @dept_id
        ;
        
        delete from t_employee_kpi_info where kpi_year = @par_year and dept_id = @dept_id;
        insert into t_employee_kpi_info(Pk_Psndoc,dept_id,user_name,ident_id,kpi_year,kpi_level_code,source_kpi_level_code,kpi_level_name
            ,status,remark)
        select
             ie.Pk_Psndoc            Pk_Psndoc               -- 主键
            ,a.dept_id               dept_id                 -- 部门编码
            ,u.user_name             user_name               -- 工号
            ,ie.Cert_Id              ident_id                -- 身份证号
            ,a.par_year              kpi_year                -- 绩效年度
            ,a.user_assess_level     kpi_level_code          -- 绩效等级编码
            ,al.kpi_level_code       source_kpi_level_code   -- 原绩效等级编码
            ,al.assess_level_cn      kpi_level_name          -- 绩效等级名称
            ,0                       status                  -- 状态(0:正常，1：无效)
            ,'年度考核模块插入'      remark                  -- 备注
        from t_annual_assess_user_imp a 
        left join t_annual_assess_user u 
            on a.par_year = u.par_year 
            and a.user_id = u.user_id
        left join int_d_new_hrs_emp ie
            on u.user_name = ie.Emp_Id
        left join t_annual_assess_dict_assess_level al 
            on al.assess_flag = 2
            and a.user_assess_level = al.assess_level
        where a.par_year = @par_year
        and a.dept_id = @dept_id
        ;
        
    else
        update t_annual_assess_progress
        set status = 4
        where par_year = @par_year
        ;
        
        update t_annual_assess_progress_group
        set status = 4
        where par_year = @par_year
        ;
        
        delete from t_employee_kpi_info where kpi_year = @par_year;
        insert into t_employee_kpi_info(Pk_Psndoc,dept_id,user_name,ident_id,kpi_year,kpi_level_code,source_kpi_level_code,kpi_level_name
            ,status,remark)
        select
             ie.Pk_Psndoc            Pk_Psndoc               -- 主键
            ,a.dept_id               dept_id                 -- 部门编码
            ,u.user_name             user_name               -- 工号
            ,ie.Cert_Id              ident_id                -- 身份证号
            ,a.par_year              kpi_year                -- 绩效年度
            ,a.user_assess_level     kpi_level_code          -- 绩效等级编码
            ,al.kpi_level_code       source_kpi_level_code   -- 原绩效等级编码
            ,al.assess_level_cn      kpi_level_name          -- 绩效等级名称
            ,0                       status                  -- 状态(0:正常，1：无效)
            ,'年度考核模块插入'      remark                  -- 备注
        from t_annual_assess_user_imp a 
        left join t_annual_assess_user u 
            on a.par_year = u.par_year 
            and a.user_id = u.user_id
        left join int_d_new_hrs_emp ie
            on u.user_name = ie.Emp_Id
        left join t_annual_assess_dict_assess_level al 
            on al.assess_flag = 2
            and a.user_assess_level = al.assess_level
        where a.par_year = @par_year
        ;
        
    end if
    ;

    /*
    -- 暂时手工更新第二步为状态2
    select count(1) into @cnt from t_annual_assess_progress
    where par_year = @par_year
    and status != 4
    ;


    -- 更新进度表
    if @cnt = 0 then 
        update t_annual_assess_step_total_dept 
        set status = 2
        where par_year = @par_year
        and step_id = 2
        ;
    end if
    ;
    */
elseif @update_type = 3 then  -- 关闭查询
    if @dept_id > 0 then
        update t_annual_assess_progress
        set status = 5
        where par_year = @par_year
        and dept_id = @dept_id
        ;
        
        update t_annual_assess_progress_group
        set status = 5
        where par_year = @par_year
        and dept_id = @dept_id
        ;
        
        delete from t_employee_kpi_info where kpi_year = @par_year and dept_id = @dept_id;
        
    else
        update t_annual_assess_progress
        set status = 5
        where par_year = @par_year
        ;
        
        update t_annual_assess_progress_group
        set status = 5
        where par_year = @par_year
        ;
        
        delete from t_employee_kpi_info where kpi_year = @par_year;
        
    end if
    ;
else 
    select '【更新类型】未定义';
    LEAVE label; -- 退出存储过程
end if
;



-- 更新进度表
if (@dept_id = 0 and @update_type = 0) then 
    update t_annual_assess_step_total_dept
    set status = 1
    where par_year = @par_year
    and step_id = 2
    ;
end if
;




-- 获得记录数
select 0 cnt into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;