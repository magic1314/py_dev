DROP PROCEDURE IF EXISTS PRO_YJGL_COMPLETED_VALUE_LOOP;
CREATE PROCEDURE `PRO_YJGL_COMPLETED_VALUE_LOOP`(IN IN_YEAR_MONTH VARCHAR(6), IN IN_DEPT_ID INT, IN IN_PERIOD INT)
label:BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_YJGL_COMPLETED_VALUE_LOOP
         功能简述：   指标完成值表_循环
         参数：       IN_YEAR_MONTH    考核年月
                      IN_DEPT_ID       部门id，只能是0
                      IN_PERIOD        考核周期(1:月，2：季，3：半年 0: 全部)，目前只能为0
         注意事项：
                 1. 传入日期的上个月，是考核的月份
                 eg.  今日为：202303
                      考核月份是：202303，也就是结果表中year_months的取值
                 2. 由于叫法的不方便，当月、上月，当期之类实际都是指上个月或上一个周期，因为当月实际没啥意义
         数据源：
                 1、 t_assess_info_his                    考核方案归档表
                 2、 PRO_YJGL_COMPLETED_VALUE             指标完成值存储过程
         结果表：
                 t_emp_perf_cmplt_val                     指标完成值表
         后续优化：
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2023/08/04                  创建
******************************************************************************************************************************************/

-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);
DECLARE V_BATCH_NO varchar(200);
DECLARE V_TOTAL_STEP_NUM int;


DECLARE done INT DEFAULT FALSE;
DECLARE cur_dept_id INT;
DECLARE cur_assess_pd INT;
DECLARE cnt INT DEFAULT 0;
DECLARE cur CURSOR FOR SELECT distinct dept_id,assess_pd FROM t_assess_info_his where year_months = IN_YEAR_MONTH;
DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;


-- 设置变量
set V_EVENT_NAME = '指标完成值表_循环';
set V_TOTAL_STEP_NUM = 5;  -- 设置总步骤数
set @remark = ''; -- 步骤日志中的备注，可以每步都定义
select now() into V_START_TIME;





-- 解决字符集问题：Illegal mix of collations (utf8mb4_0900_ai_ci,IMPLICIT) and (utf8mb4_general_ci,IMPLICIT) for operation
set collation_connection = utf8mb4_general_ci;

set @assess_year_month = IN_YEAR_MONTH; -- 格式: 202302
set @assess_month_2str = right(@assess_year_month,2);   -- 格式：02
set @assess_month = cast(right(@assess_year_month,2) as UNSIGNED);   -- 格式：2
set @assess_year = left(@assess_year_month,4);   -- 格式：2023
set @dept_id = IN_DEPT_ID;   -- 格式： 12800
set @assess_pd = IN_PERIOD;  -- 格式： 1


set V_RUN_COMMAND = concat('call PRO_YJGL_COMPLETED_VALUE_LOOP('
                            ,IN_YEAR_MONTH,','
                            ,IN_DEPT_ID,','
                            ,IN_PERIOD
                            ,')'
                         );

set V_BATCH_NO = concat(   UNIX_TIMESTAMP(),'-'
                        ,replace(V_RUN_COMMAND,'call ',''),'-'
                        ,V_TOTAL_STEP_NUM
                     );





-- 基础检查
if @assess_month >= 13 then
    select '传入参数【考核年月】不符合要求';
    LEAVE label; -- 退出存储过程 
end if;



if @assess_pd != 0 then
    select '传入参数【考核周期】不符合要求,目前只支持【0】';
    LEAVE label; -- 退出存储过程 
end if;


if @dept_id != 0 then
    select '传入参数【部门id】不符合要求,目前只支持【0】';
    LEAVE label; -- 退出存储过程 
end if;



-- 写步骤日志
set @step_info = '1.完成值记录表_循环：开始循环';
set @remark = '开始循环';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);
set @remark = '';




OPEN cur;
my_loop: WHILE NOT done DO
  FETCH cur INTO cur_dept_id,cur_assess_pd;
  IF NOT done THEN
    /* 在此处执行您想要的操作 */
    call PRO_YJGL_COMPLETED_VALUE(@assess_year_month,cur_dept_id,cur_assess_pd);
    SET cnt = cnt + 1;
  END IF;
END WHILE my_loop;
CLOSE cur;


-- 写步骤日志
set @step_info = '1.完成值记录表_循环：结束循环';
set @remark = concat('结束循环，共循环【',cnt,'】次');
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);
set @remark = '';




-- 获得记录数
select count(1) cnt into V_TABLE_CNT from t_emp_perf_cmplt_val where index_source = 1 and year_months = @assess_year_month;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;