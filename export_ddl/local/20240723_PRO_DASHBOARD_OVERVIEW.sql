DROP PROCEDURE IF EXISTS PRO_DASHBOARD_OVERVIEW;
CREATE PROCEDURE `PRO_DASHBOARD_OVERVIEW`(IN BIZ_DATE DATE)
label:BEGIN
/************************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_DASHBOARD_OVERVIEW
         功能简述：   看板数据/单独执行的存储过程
         参数：       BIZ_DATE  数据日期(YYYY-MM-DD)
         注意事项：   
         数据源：
         目标表：
         修改记录:
         --------------------------------------------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2024/05/07                  改为综合执行存储过程的存储过程
         MG         2024/05/30                  新增分公司看板，需依赖看板基础数据
************************************************************************************************************************************************/
-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);

-- 设置变量
set V_EVENT_NAME = '综合执行存储过程';
select now() into V_START_TIME;



set V_RUN_COMMAND = concat('call PRO_DASHBOARD_OVERVIEW('
                            ,BIZ_DATE
                            ,')'
                         );

/************************ 可单独执行的存储过程 ******************************/
-- 全员绩效档案
call PRO_SINGLE_ARCHIVES(current_date);

-- 全员考核结果-年度综合考评结果
call PRO_SINGLE_ANNUAL_ASSESS_RESULT(current_date);

-- 看板基础数据
call PRO_SINGLE_DASHBOARD_BASE(current_date);

-- 分公司看板
call PRO_SINGLE_DASHBOARD_BRANCH(0);

-- 获得记录数(不需要记录)
select 0 into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;