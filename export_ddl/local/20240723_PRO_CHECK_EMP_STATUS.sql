DROP PROCEDURE IF EXISTS PRO_CHECK_EMP_STATUS;
CREATE PROCEDURE `PRO_CHECK_EMP_STATUS`(IN IN_DATA_DATE INT)
label:BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_CHECK_EMP_STATUS
         功能简述：   监控员工状态
         版本：       V1.0
         参数：       IN_DATA_DATE   数据日期(默认取最新日期)
         注意事项：
                本程序用于测试，仅限本地环境/开发环境
         数据源：
                int_d_new_hrs_emp              int员工表
                t_employee                     员工表
                sys_user                       用户表
                sys_user_role                  用户角色表
                t_appraisal_dept               考核权部门
                t_appraisal_dept_magr          考核权管理部门
                t_appraisal_member             考核权主表
                t_check_emp_change_predict     预期表
         结果表：
                 t_check_emp_change_log 检查日志表
         查询SQL：
                select * from int_d_new_hrs_emp where emp_id = 'case2d';
                select * from t_employee where code = 'case2d';
                select * from sys_user where user_name = 'case2d';
                select * from t_appraisal_member where user_code = 'case2d';
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG          2024/06/19                 创建
         MG          2024/06/20                 预期表新增有效状态
******************************************************************************************************************************************/

-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);
DECLARE V_BATCH_NO varchar(200);
DECLARE V_TOTAL_STEP_NUM int;



-- 设置变量
set V_EVENT_NAME = '监控员工状态';
set V_TOTAL_STEP_NUM = 6;  -- 设置总步骤数
set @remark = ''; -- 步骤日志中的备注，可以每步都定义
select now() into V_START_TIME;


set V_RUN_COMMAND = concat('call PRO_CHECK_EMP_STATUS('
                            ,IN_DATA_DATE
                            ,')'
                         );

set V_BATCH_NO = concat(   UNIX_TIMESTAMP(),'-'
                        ,replace(V_RUN_COMMAND,'call ',''),'-'
                        ,V_TOTAL_STEP_NUM
                     );

-- 写步骤日志
set @step_info = '1.参数定义&基础检查:正常';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);



-- 写步骤日志
set @step_info = '2.增量数据清理';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);

delete from int_d_new_hrs_emp where emp_id like 'case__';
delete from t_employee where code like 'case__';

delete a,b from sys_user a 
left join sys_user_role b
on a.user_id = b.user_id
where a.user_name like 'case__'
;

delete a,b,c from t_appraisal_member a 
left join t_appraisal_dept b 
on a.id = b.appraisal_id
left join t_appraisal_dept_magr c 
on a.id = c.appraisal_id
where a.user_code like 'case__'
;

-- 写步骤日志
set @step_info = '3.CASE测试数据插入';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);

-- case00-04
INSERT INTO `int_d_new_hrs_emp` (`Pk_Psndoc`, `Emp_Id`, `Emp_Name`, `Cert_Id`, `Sex`, `Ethnic_Cd`, `Native_Place`, `Birth_Dt`, `Census_Addr`, `Family_Addr`, `MOBILE`, `Email`, `EDU`, `Grad_School`, `Grad_Dt`, `Work_dt`, `Poli_Status`, `Join_Dt`, `Retire_Dt`, `Org_Code`, `Join_Begin_Dt`, `Join_End_Dt`, `Is_Cadre`, `Job_Type`, `Job_Name`, `Job_Rank`, `Emp_Status`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `AGE`, `Work_Age`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Job_Rank_Appt_Dt`, `Enter_Type`) VALUES ('000test_case00', 'case00', '案例00', '3512345612345625', '2', '汉族', NULL, NULL, NULL, NULL, '188*******', NULL, NULL, NULL, '0001-01-01', '2015-09-10', '共青团员', '2018-06-01', '0001-01-01', '22024', '2016-09-05', '0001-01-01', '否', NULL, NULL, '4', '1', NULL, NULL, NULL, '31', '7', '0101', '在岗人员', NULL, NULL, '1899-12-29', '1899-12-29', '社会招聘（自主投递）');
INSERT INTO `int_d_new_hrs_emp` (`Pk_Psndoc`, `Emp_Id`, `Emp_Name`, `Cert_Id`, `Sex`, `Ethnic_Cd`, `Native_Place`, `Birth_Dt`, `Census_Addr`, `Family_Addr`, `MOBILE`, `Email`, `EDU`, `Grad_School`, `Grad_Dt`, `Work_dt`, `Poli_Status`, `Join_Dt`, `Retire_Dt`, `Org_Code`, `Join_Begin_Dt`, `Join_End_Dt`, `Is_Cadre`, `Job_Type`, `Job_Name`, `Job_Rank`, `Emp_Status`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `AGE`, `Work_Age`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Job_Rank_Appt_Dt`, `Enter_Type`) VALUES ('000test_case01', 'case01', '案例01', '3112345612345635', '1', '汉族', NULL, NULL, NULL, NULL, '135*******', NULL, NULL, NULL, '0001-01-01', '2012-07-09', '中共党员', '2020-03-03', '0001-01-01', '10300', '2012-07-09', '0001-01-01', '否', NULL, NULL, '10', '1', NULL, '103000008', '党委组织部/人力资源总部-干部监督部', '34', '11', '0101', '在岗人员', NULL, NULL, '1899-12-29', '1899-12-29', '校园招聘');
INSERT INTO `int_d_new_hrs_emp` (`Pk_Psndoc`, `Emp_Id`, `Emp_Name`, `Cert_Id`, `Sex`, `Ethnic_Cd`, `Native_Place`, `Birth_Dt`, `Census_Addr`, `Family_Addr`, `MOBILE`, `Email`, `EDU`, `Grad_School`, `Grad_Dt`, `Work_dt`, `Poli_Status`, `Join_Dt`, `Retire_Dt`, `Org_Code`, `Join_Begin_Dt`, `Join_End_Dt`, `Is_Cadre`, `Job_Type`, `Job_Name`, `Job_Rank`, `Emp_Status`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `AGE`, `Work_Age`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Job_Rank_Appt_Dt`, `Enter_Type`) VALUES ('000test_case02', 'case02', '案例02', '3612345612345611', '1', '汉族', NULL, NULL, NULL, NULL, '139*******', NULL, NULL, NULL, '0001-01-01', '2007-08-01', '中共党员', '2020-12-01', '0001-01-01', 'ZQ1212', '2009-09-01', '0001-01-01', '否', NULL, NULL, '19', '1', NULL, NULL, NULL, '43', '14', '0101', '在岗人员', '005', '总部副职', '2020-12-01', '2020-01-23', NULL);
INSERT INTO `int_d_new_hrs_emp` (`Pk_Psndoc`, `Emp_Id`, `Emp_Name`, `Cert_Id`, `Sex`, `Ethnic_Cd`, `Native_Place`, `Birth_Dt`, `Census_Addr`, `Family_Addr`, `MOBILE`, `Email`, `EDU`, `Grad_School`, `Grad_Dt`, `Work_dt`, `Poli_Status`, `Join_Dt`, `Retire_Dt`, `Org_Code`, `Join_Begin_Dt`, `Join_End_Dt`, `Is_Cadre`, `Job_Type`, `Job_Name`, `Job_Rank`, `Emp_Status`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `AGE`, `Work_Age`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Job_Rank_Appt_Dt`, `Enter_Type`) VALUES ('000test_case03', 'case03', '案例03', '3512345612345620', '2', '汉族', NULL, NULL, NULL, NULL, '136*******', NULL, NULL, NULL, '2016-06-30', '2002-09-02', '民建会员', '2019-07-12', '0001-01-01', '22031', '2014-05-29', '0001-01-01', '否', NULL, NULL, '15', '1', NULL, NULL, NULL, '39', '9', '0101', '在岗人员', NULL, NULL, '1899-12-29', '1899-12-29', NULL);
INSERT INTO `int_d_new_hrs_emp` (`Pk_Psndoc`, `Emp_Id`, `Emp_Name`, `Cert_Id`, `Sex`, `Ethnic_Cd`, `Native_Place`, `Birth_Dt`, `Census_Addr`, `Family_Addr`, `MOBILE`, `Email`, `EDU`, `Grad_School`, `Grad_Dt`, `Work_dt`, `Poli_Status`, `Join_Dt`, `Retire_Dt`, `Org_Code`, `Join_Begin_Dt`, `Join_End_Dt`, `Is_Cadre`, `Job_Type`, `Job_Name`, `Job_Rank`, `Emp_Status`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `AGE`, `Work_Age`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Job_Rank_Appt_Dt`, `Enter_Type`) VALUES ('000test_case04', 'case04', '案例04', '3512345612345623', '2', '汉族', NULL, NULL, NULL, NULL, '137*******', NULL, NULL, NULL, '1900-01-01', '1998-09-01', '群众', '2019-06-30', '0001-01-01', '22022', '2016-09-19', '0001-01-01', '否', NULL, NULL, '5', '1', NULL, NULL, NULL, '43', '7', '0101', '在岗人员', NULL, NULL, '1899-12-29', '1899-12-29', '社会招聘（自主投递）');
INSERT INTO `t_employee` (`psndoc_id`, `sys_user_id`, `dept_id`, `current_flag`, `name`, `code`, `status`, `begindate`, `enddate`, `pk_org`, `pk_group`, `pk_hrorg`, `iscadre`, `pk_job`, `sex`, `edu`, `birthdate`, `age`, `workage`, `marital`, `polit`, `nationality`, `nativeplace`, `health`, `user_post_id`, `indutydate`, `enddutydate`, `email`, `ident_id`, `idtype`, `mobile`, `characterrpr`, `censusaddr`, `create_time`, `job_rank`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Manager_status`, `Job_Rank_Appt_Dt`, `join_group_date`, `Enter_Type`, `remark`, `etl_create_time`, `etl_update_time`) VALUES ('000test_case00', 25276, 22024, 1, '案例00', 'case00', '0', '2018-06-01 00:00:00', NULL, NULL, NULL, NULL, '否', NULL, '2', NULL, NULL, 31, 7, NULL, NULL, '汉族', NULL, NULL, NULL, '2016-09-05', '0001-01-01', NULL, '3512345612345625', NULL, '188*******', NULL, NULL, '2024-06-19 14:57:16', '4', NULL, NULL, NULL, '0101', '在岗人员', NULL, NULL, NULL, NULL, '1899-12-29', NULL, '社会招聘（自主投递）', '系统初始化员工信息 2024-06-19 14:57:16', '2024-06-19 14:57:16', '2024-06-19 14:57:16');
INSERT INTO `t_employee` (`psndoc_id`, `sys_user_id`, `dept_id`, `current_flag`, `name`, `code`, `status`, `begindate`, `enddate`, `pk_org`, `pk_group`, `pk_hrorg`, `iscadre`, `pk_job`, `sex`, `edu`, `birthdate`, `age`, `workage`, `marital`, `polit`, `nationality`, `nativeplace`, `health`, `user_post_id`, `indutydate`, `enddutydate`, `email`, `ident_id`, `idtype`, `mobile`, `characterrpr`, `censusaddr`, `create_time`, `job_rank`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Manager_status`, `Job_Rank_Appt_Dt`, `join_group_date`, `Enter_Type`, `remark`, `etl_create_time`, `etl_update_time`) VALUES ('000test_case01', 25277, 10300, 1, '案例01', 'case01', '0', '2020-03-03 00:00:00', NULL, NULL, NULL, NULL, '否', NULL, '1', NULL, NULL, 34, 11, NULL, NULL, '汉族', NULL, NULL, NULL, '2012-07-09', '0001-01-01', NULL, '3112345612345635', NULL, '135*******', NULL, NULL, '2024-06-19 14:57:16', '10', NULL, '103000008', '干部监督部', '0101', '在岗人员', NULL, NULL, NULL, NULL, '1899-12-29', NULL, '校园招聘', '系统初始化员工信息 2024-06-19 14:57:16', '2024-06-19 14:57:16', '2024-06-19 14:57:16');
INSERT INTO `t_employee` (`psndoc_id`, `sys_user_id`, `dept_id`, `current_flag`, `name`, `code`, `status`, `begindate`, `enddate`, `pk_org`, `pk_group`, `pk_hrorg`, `iscadre`, `pk_job`, `sex`, `edu`, `birthdate`, `age`, `workage`, `marital`, `polit`, `nationality`, `nativeplace`, `health`, `user_post_id`, `indutydate`, `enddutydate`, `email`, `ident_id`, `idtype`, `mobile`, `characterrpr`, `censusaddr`, `create_time`, `job_rank`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Manager_status`, `Job_Rank_Appt_Dt`, `join_group_date`, `Enter_Type`, `remark`, `etl_create_time`, `etl_update_time`) VALUES ('000test_case02', 25280, 91212, 1, '案例02', 'case02', '0', '2020-12-01 00:00:00', NULL, NULL, NULL, NULL, '否', NULL, '1', NULL, NULL, 43, 14, NULL, NULL, '汉族', NULL, NULL, NULL, '2009-09-01', '0001-01-01', NULL, '3612345612345611', NULL, '139*******', NULL, NULL, '2024-06-19 14:57:16', '19', NULL, NULL, NULL, '0101', '在岗人员', '005', NULL, '2020-12-01', NULL, '2020-01-23', NULL, NULL, '系统初始化员工信息 2024-06-19 14:57:16', '2024-06-19 14:57:16', '2024-06-19 14:57:16');
INSERT INTO `t_employee` (`psndoc_id`, `sys_user_id`, `dept_id`, `current_flag`, `name`, `code`, `status`, `begindate`, `enddate`, `pk_org`, `pk_group`, `pk_hrorg`, `iscadre`, `pk_job`, `sex`, `edu`, `birthdate`, `age`, `workage`, `marital`, `polit`, `nationality`, `nativeplace`, `health`, `user_post_id`, `indutydate`, `enddutydate`, `email`, `ident_id`, `idtype`, `mobile`, `characterrpr`, `censusaddr`, `create_time`, `job_rank`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Manager_status`, `Job_Rank_Appt_Dt`, `join_group_date`, `Enter_Type`, `remark`, `etl_create_time`, `etl_update_time`) VALUES ('000test_case03', 25278, 22031, 1, '案例03', 'case03', '0', '2019-07-12 00:00:00', NULL, NULL, NULL, NULL, '否', NULL, '2', NULL, NULL, 39, 9, NULL, NULL, '汉族', NULL, NULL, NULL, '2014-05-29', '0001-01-01', NULL, '3512345612345620', NULL, '136*******', NULL, NULL, '2024-06-19 14:57:16', '15', NULL, NULL, NULL, '0101', '在岗人员', NULL, NULL, NULL, NULL, '1899-12-29', NULL, NULL, '系统初始化员工信息 2024-06-19 14:57:16', '2024-06-19 14:57:16', '2024-06-19 14:57:16');
INSERT INTO `t_employee` (`psndoc_id`, `sys_user_id`, `dept_id`, `current_flag`, `name`, `code`, `status`, `begindate`, `enddate`, `pk_org`, `pk_group`, `pk_hrorg`, `iscadre`, `pk_job`, `sex`, `edu`, `birthdate`, `age`, `workage`, `marital`, `polit`, `nationality`, `nativeplace`, `health`, `user_post_id`, `indutydate`, `enddutydate`, `email`, `ident_id`, `idtype`, `mobile`, `characterrpr`, `censusaddr`, `create_time`, `job_rank`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Manager_status`, `Job_Rank_Appt_Dt`, `join_group_date`, `Enter_Type`, `remark`, `etl_create_time`, `etl_update_time`) VALUES ('000test_case04', 25279, 22022, 1, '案例04', 'case04', '0', '2019-06-30 00:00:00', NULL, NULL, NULL, NULL, '否', NULL, '2', NULL, NULL, 43, 7, NULL, NULL, '汉族', NULL, NULL, NULL, '2016-09-19', '0001-01-01', NULL, '3512345612345623', NULL, '137*******', NULL, NULL, '2024-06-19 14:57:16', '5', NULL, NULL, NULL, '0101', '在岗人员', NULL, NULL, NULL, NULL, '1899-12-29', NULL, '社会招聘（自主投递）', '系统初始化员工信息 2024-06-19 14:57:16', '2024-06-19 14:57:16', '2024-06-19 14:57:16');
INSERT INTO `sys_user` (`user_id`, `dept_id`, `user_name`, `nick_name`, `user_type`, `email`, `phonenumber`, `sex`, `avatar`, `password`, `status`, `del_flag`, `login_ip`, `login_date`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `account_status`, `account_flag`, `belonging_user_name`, `last_chgpwdtime`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `begindate`, `enddate`, `sys_theme_style`, `etl_create_time`, `etl_update_time`) VALUES (25276, 22024, 'case00', '案例00', '00', NULL, '188*******', '2', NULL, '$2a$10$BGw3osZD8hp8JiumC1CdB.JeNlV98SIATHLf/hUjha.muffsYWkNK', '0', '0', NULL, NULL, '1', '2024-06-19 14:57:16', NULL, NULL, '系统新增用户2024-06-19 14:57:16', '0', '1', NULL, NULL, NULL, NULL, NULL, '2016-09-05 00:00:00', NULL, 'standard', '2024-06-19 14:57:16', '2024-06-19 14:57:16');
INSERT INTO `sys_user` (`user_id`, `dept_id`, `user_name`, `nick_name`, `user_type`, `email`, `phonenumber`, `sex`, `avatar`, `password`, `status`, `del_flag`, `login_ip`, `login_date`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `account_status`, `account_flag`, `belonging_user_name`, `last_chgpwdtime`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `begindate`, `enddate`, `sys_theme_style`, `etl_create_time`, `etl_update_time`) VALUES (25277, 10300, 'case01', '案例01', '00', NULL, '135*******', '1', NULL, '$2a$10$BGw3osZD8hp8JiumC1CdB.JeNlV98SIATHLf/hUjha.muffsYWkNK', '0', '0', NULL, NULL, '1', '2024-06-19 14:57:16', NULL, NULL, '系统新增用户2024-06-19 14:57:16', '0', '1', NULL, NULL, NULL, '103000008', '干部监督部', '2012-07-09 00:00:00', NULL, 'standard', '2024-06-19 14:57:16', '2024-06-19 14:57:16');
INSERT INTO `sys_user` (`user_id`, `dept_id`, `user_name`, `nick_name`, `user_type`, `email`, `phonenumber`, `sex`, `avatar`, `password`, `status`, `del_flag`, `login_ip`, `login_date`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `account_status`, `account_flag`, `belonging_user_name`, `last_chgpwdtime`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `begindate`, `enddate`, `sys_theme_style`, `etl_create_time`, `etl_update_time`) VALUES (25280, 91212, 'case02', '案例02', '00', NULL, '139*******', '1', NULL, '$2a$10$BGw3osZD8hp8JiumC1CdB.JeNlV98SIATHLf/hUjha.muffsYWkNK', '0', '0', NULL, NULL, '1', '2024-06-19 14:57:16', NULL, NULL, '系统新增用户2024-06-19 14:57:16', '0', '1', NULL, NULL, NULL, NULL, NULL, '2009-09-01 00:00:00', NULL, 'standard', '2024-06-19 14:57:16', '2024-06-19 14:57:16');
INSERT INTO `sys_user` (`user_id`, `dept_id`, `user_name`, `nick_name`, `user_type`, `email`, `phonenumber`, `sex`, `avatar`, `password`, `status`, `del_flag`, `login_ip`, `login_date`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `account_status`, `account_flag`, `belonging_user_name`, `last_chgpwdtime`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `begindate`, `enddate`, `sys_theme_style`, `etl_create_time`, `etl_update_time`) VALUES (25278, 22031, 'case03', '案例03', '00', NULL, '136*******', '2', NULL, '$2a$10$BGw3osZD8hp8JiumC1CdB.JeNlV98SIATHLf/hUjha.muffsYWkNK', '0', '0', NULL, NULL, '1', '2024-06-19 14:57:16', NULL, NULL, '系统新增用户2024-06-19 14:57:16', '0', '1', NULL, NULL, NULL, NULL, NULL, '2014-05-29 00:00:00', NULL, 'standard', '2024-06-19 14:57:16', '2024-06-19 14:57:16');
INSERT INTO `sys_user` (`user_id`, `dept_id`, `user_name`, `nick_name`, `user_type`, `email`, `phonenumber`, `sex`, `avatar`, `password`, `status`, `del_flag`, `login_ip`, `login_date`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `account_status`, `account_flag`, `belonging_user_name`, `last_chgpwdtime`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `begindate`, `enddate`, `sys_theme_style`, `etl_create_time`, `etl_update_time`) VALUES (25279, 22022, 'case04', '案例04', '00', NULL, '137*******', '2', NULL, '$2a$10$BGw3osZD8hp8JiumC1CdB.JeNlV98SIATHLf/hUjha.muffsYWkNK', '0', '0', NULL, NULL, '1', '2024-06-19 14:57:16', NULL, NULL, '系统新增用户2024-06-19 14:57:16', '0', '1', NULL, NULL, NULL, NULL, NULL, '2016-09-19 00:00:00', NULL, 'standard', '2024-06-19 14:57:16', '2024-06-19 14:57:16');
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (25276, 2003);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (25277, 2003);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (25280, 2003);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (25278, 2003);
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (25279, 2003);
INSERT INTO `t_appraisal_member` (`id`, `dept_id`, `dept_name`, `user_name`, `user_code`, `user_id`, `job`, `user_status`, `register_date`, `cancel_date`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (38214, 22024, '石狮石龙路证券营业部', '案例00', 'case00', 25276, NULL, 0, '2023-01-01', NULL, '2024-06-19 14:57:17', 1, NULL, NULL, '考核权初始化新增', NULL, b'0');
INSERT INTO `t_appraisal_member` (`id`, `dept_id`, `dept_name`, `user_name`, `user_code`, `user_id`, `job`, `user_status`, `register_date`, `cancel_date`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (38215, 22031, '福州古田路证券营业部', '案例03', 'case03', 25278, NULL, 0, '2023-01-01', NULL, '2024-06-19 14:57:17', 1, NULL, NULL, '考核权初始化新增', NULL, b'0');
INSERT INTO `t_appraisal_member` (`id`, `dept_id`, `dept_name`, `user_name`, `user_code`, `user_id`, `job`, `user_status`, `register_date`, `cancel_date`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (38216, 91212, '承销保荐有限责任公司', '案例02', 'case02', 25280, NULL, 0, '2023-01-01', NULL, '2024-06-19 14:57:17', 1, NULL, NULL, '考核权初始化新增', NULL, b'0');
INSERT INTO `t_appraisal_member` (`id`, `dept_id`, `dept_name`, `user_name`, `user_code`, `user_id`, `job`, `user_status`, `register_date`, `cancel_date`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (38217, 22022, '泉州分公司', '案例04', 'case04', 25279, NULL, 0, '2023-01-01', NULL, '2024-06-19 14:57:17', 1, NULL, NULL, '考核权初始化新增', NULL, b'0');
INSERT INTO `t_appraisal_member` (`id`, `dept_id`, `dept_name`, `user_name`, `user_code`, `user_id`, `job`, `user_status`, `register_date`, `cancel_date`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (38218, 10300, '党委组织部/人力资源总部', '案例01', 'case01', 25277, NULL, 0, '2023-01-01', NULL, '2024-06-19 14:57:17', 1, NULL, NULL, '考核权初始化新增', NULL, b'0');
INSERT INTO `t_appraisal_dept` (`id`, `dept_id`, `appraisal_id`, `appraisal_right`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (57016, 22022, 38214, 100.00, NULL, '2024-06-19 14:57:17', 1, NULL, NULL, '初始化考核权部门', NULL, b'0');
INSERT INTO `t_appraisal_dept` (`id`, `dept_id`, `appraisal_id`, `appraisal_right`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (57017, 22031, 38215, 100.00, NULL, '2024-06-19 14:57:17', 1, NULL, NULL, '初始化考核权部门', NULL, b'0');
INSERT INTO `t_appraisal_dept` (`id`, `dept_id`, `appraisal_id`, `appraisal_right`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (57018, 91212, 38216, 100.00, NULL, '2024-06-19 14:57:17', 1, NULL, NULL, '初始化考核权部门', NULL, b'0');
INSERT INTO `t_appraisal_dept` (`id`, `dept_id`, `appraisal_id`, `appraisal_right`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (57019, 22022, 38217, 100.00, NULL, '2024-06-19 14:57:17', 1, NULL, NULL, '初始化考核权部门', NULL, b'0');
INSERT INTO `t_appraisal_dept` (`id`, `dept_id`, `appraisal_id`, `appraisal_right`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (57020, 10300, 38218, 100.00, NULL, '2024-06-19 14:57:17', 1, NULL, NULL, '初始化考核权部门', NULL, b'0');
INSERT INTO `t_appraisal_dept_magr` (`id`, `dept_id`, `appraisal_id`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (21061, 12800, 38214, NULL, '2024-02-28 14:40:10', 1, NULL, NULL, '初始化考核权管理部门', NULL, b'0');
INSERT INTO `t_appraisal_dept_magr` (`id`, `dept_id`, `appraisal_id`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (25438, 22031, 38215, NULL, '2024-06-19 14:57:17', 1, NULL, NULL, '初始化考核权管理部门', NULL, b'0');
INSERT INTO `t_appraisal_dept_magr` (`id`, `dept_id`, `appraisal_id`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (25440, 91212, 38216, NULL, '2024-06-19 14:57:17', 1, NULL, NULL, '初始化考核权管理部门', NULL, b'0');
INSERT INTO `t_appraisal_dept_magr` (`id`, `dept_id`, `appraisal_id`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (25439, 22022, 38217, NULL, '2024-06-19 14:57:17', 1, NULL, NULL, '初始化考核权管理部门', NULL, b'0');
INSERT INTO `t_appraisal_dept_magr` (`id`, `dept_id`, `appraisal_id`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (25437, 10300, 38218, NULL, '2024-06-19 14:57:17', 1, NULL, NULL, '初始化考核权管理部门', NULL, b'0');


-- case1s
INSERT INTO `int_d_new_hrs_emp` (`Pk_Psndoc`, `Emp_Id`, `Emp_Name`, `Cert_Id`, `Sex`, `Ethnic_Cd`, `Native_Place`, `Birth_Dt`, `Census_Addr`, `Family_Addr`, `MOBILE`, `Email`, `EDU`, `Grad_School`, `Grad_Dt`, `Work_dt`, `Poli_Status`, `Join_Dt`, `Retire_Dt`, `Org_Code`, `Join_Begin_Dt`, `Join_End_Dt`, `Is_Cadre`, `Job_Type`, `Job_Name`, `Job_Rank`, `Emp_Status`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `AGE`, `Work_Age`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Job_Rank_Appt_Dt`, `Enter_Type`) VALUES ('000test_case1s', 'case1s', '案例1s', '3512345612345623', '2', '汉族', NULL, NULL, NULL, NULL, '137*******', NULL, NULL, NULL, '1900-01-01', '1998-09-01', '群众', '2019-06-30', '0001-01-01', '22022', '2016-09-19', '2024-04-25', '否', NULL, NULL, '5', '4', NULL, NULL, NULL, '43', '7', '0101', '在岗人员', NULL, NULL, '1899-12-29', '1899-12-29', '社会招聘（自主投递）');
INSERT INTO `t_employee` (`psndoc_id`, `sys_user_id`, `dept_id`, `current_flag`, `name`, `code`, `status`, `begindate`, `enddate`, `pk_org`, `pk_group`, `pk_hrorg`, `iscadre`, `pk_job`, `sex`, `edu`, `birthdate`, `age`, `workage`, `marital`, `polit`, `nationality`, `nativeplace`, `health`, `user_post_id`, `indutydate`, `enddutydate`, `email`, `ident_id`, `idtype`, `mobile`, `characterrpr`, `censusaddr`, `create_time`, `job_rank`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Manager_status`, `Job_Rank_Appt_Dt`, `join_group_date`, `Enter_Type`, `remark`, `etl_create_time`, `etl_update_time`) VALUES ('000test_case1s', 25266, 22022, -1, '案例1s', 'case1s', '1', '2019-06-30 00:00:00', '2024-04-25 00:00:00', NULL, NULL, NULL, '否', NULL, '2', NULL, NULL, 43, 7, NULL, NULL, '汉族', NULL, NULL, NULL, '2016-09-19', '2024-04-25', NULL, '3512345612345623', NULL, '137*******', NULL, NULL, '2024-06-14 10:06:16', '5', NULL, NULL, NULL, '0101', '在岗人员', NULL, NULL, NULL, NULL, '1899-12-29', NULL, '社会招聘（自主投递）', '系统初始化员工信息 2024-06-14 10:06:16', '2024-06-14 10:06:16', '2024-06-14 10:10:28');
INSERT INTO `sys_user` (`user_id`, `dept_id`, `user_name`, `nick_name`, `user_type`, `email`, `phonenumber`, `sex`, `avatar`, `password`, `status`, `del_flag`, `login_ip`, `login_date`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `account_status`, `account_flag`, `belonging_user_name`, `last_chgpwdtime`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `begindate`, `enddate`, `sys_theme_style`, `etl_create_time`, `etl_update_time`) VALUES (25266, 22022, 'case1s', '案例1s', '00', NULL, '137*******', '2', NULL, '$2a$10$BGw3osZD8hp8JiumC1CdB.JeNlV98SIATHLf/hUjha.muffsYWkNK', '1', '0', NULL, NULL, '1', '2024-06-14 10:08:50', NULL, NULL, '离退人员，操作时间:2024-06-14 10:10:28', '1', '1', NULL, NULL, NULL, NULL, NULL, '2016-09-19 00:00:00', '2024-04-25 00:00:00', 'standard', '2024-06-14 10:08:50', '2024-06-14 10:10:29');
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (25266, 2003);
INSERT INTO `t_appraisal_member` (`id`, `dept_id`, `dept_name`, `user_name`, `user_code`, `user_id`, `job`, `user_status`, `register_date`, `cancel_date`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (38108, 22022, '泉州分公司', '案例1s', 'case1s', 25266, NULL, 2, '2023-01-01', '2024-05-01', '2024-06-14 10:08:50', 1, '2024-06-14 10:10:29', 1, '员工离职注销考核权信息，注销时间：2024-06-14 10:10:29', NULL, b'0');
INSERT INTO `t_appraisal_dept` (`id`, `dept_id`, `appraisal_id`, `appraisal_right`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (56917, 22022, 38108, 100.00, NULL, '2024-06-14 10:08:51', 1, NULL, NULL, '初始化考核权部门', NULL, b'0');
INSERT INTO `t_appraisal_dept_magr` (`id`, `dept_id`, `appraisal_id`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (25368, 22022, 38108, NULL, '2024-06-14 10:08:51', 1, NULL, NULL, '初始化考核权管理部门', NULL, b'0');


-- case20
INSERT INTO `int_d_new_hrs_emp` (`Pk_Psndoc`, `Emp_Id`, `Emp_Name`, `Cert_Id`, `Sex`, `Ethnic_Cd`, `Native_Place`, `Birth_Dt`, `Census_Addr`, `Family_Addr`, `MOBILE`, `Email`, `EDU`, `Grad_School`, `Grad_Dt`, `Work_dt`, `Poli_Status`, `Join_Dt`, `Retire_Dt`, `Org_Code`, `Join_Begin_Dt`, `Join_End_Dt`, `Is_Cadre`, `Job_Type`, `Job_Name`, `Job_Rank`, `Emp_Status`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `AGE`, `Work_Age`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Job_Rank_Appt_Dt`, `Enter_Type`) VALUES ('000test_case20', 'case20', '案例20', '3512345612323424', '2', '汉族', NULL, NULL, NULL, NULL, '137*******', NULL, NULL, NULL, '1900-01-01', '1998-09-01', '群众', '2019-06-30', '0001-01-01', '22022', '2016-09-19', '0001-01-01', '否', NULL, NULL, '5', '1', NULL, NULL, NULL, '43', '7', '0410', '子公司人员', NULL, NULL, '1899-12-29', '1899-12-29', '社会招聘（自主投递）');
INSERT INTO `t_employee` (`psndoc_id`, `sys_user_id`, `dept_id`, `current_flag`, `name`, `code`, `status`, `begindate`, `enddate`, `pk_org`, `pk_group`, `pk_hrorg`, `iscadre`, `pk_job`, `sex`, `edu`, `birthdate`, `age`, `workage`, `marital`, `polit`, `nationality`, `nativeplace`, `health`, `user_post_id`, `indutydate`, `enddutydate`, `email`, `ident_id`, `idtype`, `mobile`, `characterrpr`, `censusaddr`, `create_time`, `job_rank`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Manager_status`, `Job_Rank_Appt_Dt`, `join_group_date`, `Enter_Type`, `remark`, `etl_create_time`, `etl_update_time`) VALUES ('000test_case20', 25555, 22022, -2, '案例20', 'case20', '1', '2019-06-30 00:00:00', NULL, NULL, NULL, NULL, '否', NULL, '2', NULL, NULL, 43, 7, NULL, NULL, '汉族', NULL, NULL, NULL, '2016-09-19', '0001-01-01', NULL, '3512345612323424', NULL, '137*******', NULL, NULL, '2024-06-19 14:48:29', '5', NULL, NULL, NULL, '0410', '子公司人员', NULL, NULL, NULL, NULL, '1899-12-29', NULL, '社会招聘（自主投递）', '系统初始化员工信息 2024-06-19 14:48:29', '2024-06-19 14:48:29', '2024-06-19 14:50:05');
INSERT INTO `sys_user` (`user_id`, `dept_id`, `user_name`, `nick_name`, `user_type`, `email`, `phonenumber`, `sex`, `avatar`, `password`, `status`, `del_flag`, `login_ip`, `login_date`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `account_status`, `account_flag`, `belonging_user_name`, `last_chgpwdtime`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `begindate`, `enddate`, `sys_theme_style`, `etl_create_time`, `etl_update_time`) VALUES (25555, 22022, 'case20', '案例20', '00', NULL, '137*******', '2', NULL, '$2a$10$BGw3osZD8hp8JiumC1CdB.JeNlV98SIATHLf/hUjha.muffsYWkNK', '1', '0', NULL, NULL, '1', '2024-06-19 14:48:29', NULL, NULL, '注销人员(人员类别变更或移到集团其他组织)，操作时间：2024-06-19 14:50:05', '2', '1', NULL, NULL, NULL, NULL, NULL, '2016-09-19 00:00:00', NULL, 'standard', '2024-06-19 14:48:29', '2024-06-19 14:50:06');
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (25555, 2003);
INSERT INTO `t_appraisal_member` (`id`, `dept_id`, `dept_name`, `user_name`, `user_code`, `user_id`, `job`, `user_status`, `register_date`, `cancel_date`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (38199, 22022, '泉州分公司', '案例20', 'case20', 25555, NULL, 2, '2023-01-01', '2024-07-01', '2024-06-19 14:48:30', 1, '2024-06-19 14:50:06', 1, '已注销员工 注销考核权信息，注销时间：2024-06-19 14:50:06', NULL, b'0');
INSERT INTO `t_appraisal_dept` (`id`, `dept_id`, `appraisal_id`, `appraisal_right`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (57001, 22022, 38199, 100.00, NULL, '2024-06-19 14:48:30', 1, NULL, NULL, '初始化考核权部门', NULL, b'0');
INSERT INTO `t_appraisal_dept_magr` (`id`, `dept_id`, `appraisal_id`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (25430, 22022, 38199, NULL, '2024-06-19 14:48:30', 1, NULL, NULL, '初始化考核权管理部门', NULL, b'0');


-- case40
INSERT INTO `int_d_new_hrs_emp` (`Pk_Psndoc`, `Emp_Id`, `Emp_Name`, `Cert_Id`, `Sex`, `Ethnic_Cd`, `Native_Place`, `Birth_Dt`, `Census_Addr`, `Family_Addr`, `MOBILE`, `Email`, `EDU`, `Grad_School`, `Grad_Dt`, `Work_dt`, `Poli_Status`, `Join_Dt`, `Retire_Dt`, `Org_Code`, `Join_Begin_Dt`, `Join_End_Dt`, `Is_Cadre`, `Job_Type`, `Job_Name`, `Job_Rank`, `Emp_Status`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `AGE`, `Work_Age`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Job_Rank_Appt_Dt`, `Enter_Type`) VALUES ('000test_case40', 'case40', '案例40', '3512345612323333', '2', '汉族', NULL, NULL, NULL, NULL, '137*******', NULL, NULL, NULL, '1900-01-01', '1998-09-01', '群众', '2019-06-30', '0001-01-01', 'ZQ00010001', '2016-09-19', '0001-01-01', '否', NULL, NULL, '5', '1', NULL, NULL, NULL, '43', '7', '0101', '在岗人员', NULL, NULL, '1899-12-29', '1899-12-29', '社会招聘（自主投递）');
INSERT INTO `t_employee` (`psndoc_id`, `sys_user_id`, `dept_id`, `current_flag`, `name`, `code`, `status`, `begindate`, `enddate`, `pk_org`, `pk_group`, `pk_hrorg`, `iscadre`, `pk_job`, `sex`, `edu`, `birthdate`, `age`, `workage`, `marital`, `polit`, `nationality`, `nativeplace`, `health`, `user_post_id`, `indutydate`, `enddutydate`, `email`, `ident_id`, `idtype`, `mobile`, `characterrpr`, `censusaddr`, `create_time`, `job_rank`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Manager_status`, `Job_Rank_Appt_Dt`, `join_group_date`, `Enter_Type`, `remark`, `etl_create_time`, `etl_update_time`) VALUES ('000test_case40', 25285, 22022, -2, '案例40', 'case40', '1', '2019-06-30 00:00:00', NULL, NULL, NULL, NULL, '否', NULL, '2', NULL, NULL, 43, 7, NULL, NULL, '汉族', NULL, NULL, NULL, '2016-09-19', '0001-01-01', NULL, '3512345612323333', NULL, '137*******', NULL, NULL, '2024-06-19 13:26:15', '5', NULL, NULL, NULL, '0101', '在岗人员', NULL, NULL, NULL, NULL, '1899-12-29', NULL, '社会招聘（自主投递）', '移到集团其他组织的人员，状态改为注销,操作时间:2024-06-19 13:26:43', '2024-06-19 13:26:15', '2024-06-19 13:26:43');
INSERT INTO `sys_user` (`user_id`, `dept_id`, `user_name`, `nick_name`, `user_type`, `email`, `phonenumber`, `sex`, `avatar`, `password`, `status`, `del_flag`, `login_ip`, `login_date`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `account_status`, `account_flag`, `belonging_user_name`, `last_chgpwdtime`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `begindate`, `enddate`, `sys_theme_style`, `etl_create_time`, `etl_update_time`) VALUES (25285, 22022, 'case40', '案例40', '00', NULL, '137*******', '2', NULL, '$2a$10$BGw3osZD8hp8JiumC1CdB.JeNlV98SIATHLf/hUjha.muffsYWkNK', '1', '0', NULL, NULL, '1', '2024-06-19 13:26:15', NULL, NULL, '注销人员(人员类别变更或移到集团其他组织)，操作时间：2024-06-19 13:26:43', '2', '1', NULL, NULL, NULL, NULL, NULL, '2016-09-19 00:00:00', NULL, 'standard', '2024-06-19 13:26:15', '2024-06-19 13:26:43');
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (25285, 2003);
INSERT INTO `t_appraisal_member` (`id`, `dept_id`, `dept_name`, `user_name`, `user_code`, `user_id`, `job`, `user_status`, `register_date`, `cancel_date`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (38198, 22022, '泉州分公司', '案例40', 'case40', 25285, NULL, 2, '2023-01-01', '2024-07-01', '2024-06-19 13:26:16', 1, '2024-06-19 13:26:44', 1, '已注销员工 注销考核权信息，注销时间：2024-06-19 13:26:44', NULL, b'0');
INSERT INTO `t_appraisal_dept` (`id`, `dept_id`, `appraisal_id`, `appraisal_right`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (57000, 22022, 38198, 100.00, NULL, '2024-06-19 13:26:16', 1, NULL, NULL, '初始化考核权部门', NULL, b'0');
INSERT INTO `t_appraisal_dept_magr` (`id`, `dept_id`, `appraisal_id`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (21054, 10400, 38198, NULL, '2024-02-28 14:40:10', 1, NULL, NULL, '初始化考核权管理部门', NULL, b'0')
;


-- case2d
INSERT INTO `int_d_new_hrs_emp` (`Pk_Psndoc`, `Emp_Id`, `Emp_Name`, `Cert_Id`, `Sex`, `Ethnic_Cd`, `Native_Place`, `Birth_Dt`, `Census_Addr`, `Family_Addr`, `MOBILE`, `Email`, `EDU`, `Grad_School`, `Grad_Dt`, `Work_dt`, `Poli_Status`, `Join_Dt`, `Retire_Dt`, `Org_Code`, `Join_Begin_Dt`, `Join_End_Dt`, `Is_Cadre`, `Job_Type`, `Job_Name`, `Job_Rank`, `Emp_Status`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `AGE`, `Work_Age`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Job_Rank_Appt_Dt`, `Enter_Type`) VALUES ('000test_case2d', 'case2d', '案例20', '3512345612323424', '2', '汉族', NULL, NULL, NULL, NULL, '137*******', NULL, NULL, NULL, '1900-01-01', '1998-09-01', '群众', '2019-06-30', '0001-01-01', '22022', '2016-09-19', '0001-01-01', '否', NULL, NULL, '5', '1', NULL, NULL, NULL, '43', '7', '0410', '子公司人员', NULL, NULL, '1899-12-29', '1899-12-29', '社会招聘（自主投递）');
INSERT INTO `t_employee` (`psndoc_id`, `sys_user_id`, `dept_id`, `current_flag`, `name`, `code`, `status`, `begindate`, `enddate`, `pk_org`, `pk_group`, `pk_hrorg`, `iscadre`, `pk_job`, `sex`, `edu`, `birthdate`, `age`, `workage`, `marital`, `polit`, `nationality`, `nativeplace`, `health`, `user_post_id`, `indutydate`, `enddutydate`, `email`, `ident_id`, `idtype`, `mobile`, `characterrpr`, `censusaddr`, `create_time`, `job_rank`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Manager_status`, `Job_Rank_Appt_Dt`, `join_group_date`, `Enter_Type`, `remark`, `etl_create_time`, `etl_update_time`) VALUES ('000test_case2d', 25556, 22022, -2, '案例20', 'case2d', '1', '2019-06-30 00:00:00', NULL, NULL, NULL, NULL, '否', NULL, '2', NULL, NULL, 43, 7, NULL, NULL, '汉族', NULL, NULL, NULL, '2016-09-19', '0001-01-01', NULL, '3512345612323424', NULL, '137*******', NULL, NULL, '2024-06-19 14:48:29', '5', NULL, NULL, NULL, '0410', '子公司人员', NULL, NULL, NULL, NULL, '1899-12-29', NULL, '社会招聘（自主投递）', '系统初始化员工信息 2024-06-19 14:48:29', '2024-06-19 14:48:29', '2024-06-19 14:50:05');
INSERT INTO `sys_user` (`user_id`, `dept_id`, `user_name`, `nick_name`, `user_type`, `email`, `phonenumber`, `sex`, `avatar`, `password`, `status`, `del_flag`, `login_ip`, `login_date`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `account_status`, `account_flag`, `belonging_user_name`, `last_chgpwdtime`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `begindate`, `enddate`, `sys_theme_style`, `etl_create_time`, `etl_update_time`) VALUES (25556, 22022, 'case2d', '案例20', '00', NULL, '137*******', '2', NULL, '$2a$10$BGw3osZD8hp8JiumC1CdB.JeNlV98SIATHLf/hUjha.muffsYWkNK', '1', '0', NULL, NULL, '1', '2024-06-19 14:48:29', NULL, NULL, '注销人员(人员类别变更或移到集团其他组织)，操作时间：2024-06-19 14:50:05', '2', '1', NULL, NULL, NULL, NULL, NULL, '2016-09-19 00:00:00', NULL, 'standard', '2024-06-19 14:48:29', '2024-06-19 14:50:06');
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (25556, 2003);
INSERT INTO `t_appraisal_member` (`id`, `dept_id`, `dept_name`, `user_name`, `user_code`, `user_id`, `job`, `user_status`, `register_date`, `cancel_date`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (38200, 22022, '泉州分公司', '案例20', 'case2d', 25556, NULL, 2, '2023-01-01', '2024-07-01', '2024-06-19 14:48:30', 1, '2024-06-19 14:50:06', 1, '已注销员工 注销考核权信息，注销时间：2024-06-19 14:50:06', NULL, b'0');
INSERT INTO `t_appraisal_dept` (`dept_id`, `appraisal_id`, `appraisal_right`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (22022, 38200, 100.00, NULL, '2024-06-19 14:48:30', 1, NULL, NULL, '初始化考核权部门', NULL, b'0');
INSERT INTO `t_appraisal_dept_magr` (`dept_id`, `appraisal_id`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (22022, 38200, NULL, '2024-06-19 14:48:30', 1, NULL, NULL, '初始化考核权管理部门', NULL, b'0')
;


-- case4d
INSERT INTO `int_d_new_hrs_emp` (`Pk_Psndoc`, `Emp_Id`, `Emp_Name`, `Cert_Id`, `Sex`, `Ethnic_Cd`, `Native_Place`, `Birth_Dt`, `Census_Addr`, `Family_Addr`, `MOBILE`, `Email`, `EDU`, `Grad_School`, `Grad_Dt`, `Work_dt`, `Poli_Status`, `Join_Dt`, `Retire_Dt`, `Org_Code`, `Join_Begin_Dt`, `Join_End_Dt`, `Is_Cadre`, `Job_Type`, `Job_Name`, `Job_Rank`, `Emp_Status`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `AGE`, `Work_Age`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Job_Rank_Appt_Dt`, `Enter_Type`) VALUES ('000test_case4d', 'case4d', '案例4d', '3512345612323333', '2', '汉族', NULL, NULL, NULL, NULL, '137*******', NULL, NULL, NULL, '1900-01-01', '1998-09-01', '群众', '2019-06-30', '0001-01-01', 'ZQ00010001', '2016-09-19', '0001-01-01', '否', NULL, NULL, '5', '1', NULL, NULL, NULL, '43', '7', '0101', '在岗人员', NULL, NULL, '1899-12-29', '1899-12-29', '社会招聘（自主投递）');
INSERT INTO `t_employee` (`psndoc_id`, `sys_user_id`, `dept_id`, `current_flag`, `name`, `code`, `status`, `begindate`, `enddate`, `pk_org`, `pk_group`, `pk_hrorg`, `iscadre`, `pk_job`, `sex`, `edu`, `birthdate`, `age`, `workage`, `marital`, `polit`, `nationality`, `nativeplace`, `health`, `user_post_id`, `indutydate`, `enddutydate`, `email`, `ident_id`, `idtype`, `mobile`, `characterrpr`, `censusaddr`, `create_time`, `job_rank`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `Emp_Type_Code`, `Emp_Type_Name`, `Managercode`, `Managername`, `Job_Appt_Dt`, `Manager_status`, `Job_Rank_Appt_Dt`, `join_group_date`, `Enter_Type`, `remark`, `etl_create_time`, `etl_update_time`) VALUES ('000test_case4d', 25286, 22022, -2, '案例40', 'case4d', '1', '2019-06-30 00:00:00', NULL, NULL, NULL, NULL, '否', NULL, '2', NULL, NULL, 43, 7, NULL, NULL, '汉族', NULL, NULL, NULL, '2016-09-19', '0001-01-01', NULL, '3512345612323333', NULL, '137*******', NULL, NULL, '2024-06-19 13:26:15', '5', NULL, NULL, NULL, '0101', '在岗人员', NULL, NULL, NULL, NULL, '1899-12-29', NULL, '社会招聘（自主投递）', '移到集团其他组织的人员，状态改为注销,操作时间:2024-06-19 13:26:43', '2024-06-19 13:26:15', '2024-06-19 13:26:43');
INSERT INTO `sys_user` (`user_id`, `dept_id`, `user_name`, `nick_name`, `user_type`, `email`, `phonenumber`, `sex`, `avatar`, `password`, `status`, `del_flag`, `login_ip`, `login_date`, `create_by`, `create_time`, `update_by`, `update_time`, `remark`, `account_status`, `account_flag`, `belonging_user_name`, `last_chgpwdtime`, `Post`, `Org_Sec_Code`, `Org_Sec_Name`, `begindate`, `enddate`, `sys_theme_style`, `etl_create_time`, `etl_update_time`) VALUES (25286, 22022, 'case4d', '案例40', '00', NULL, '137*******', '2', NULL, '$2a$10$BGw3osZD8hp8JiumC1CdB.JeNlV98SIATHLf/hUjha.muffsYWkNK', '1', '0', NULL, NULL, '1', '2024-06-19 13:26:15', NULL, NULL, '注销人员(人员类别变更或移到集团其他组织)，操作时间：2024-06-19 13:26:43', '2', '1', NULL, NULL, NULL, NULL, NULL, '2016-09-19 00:00:00', NULL, 'standard', '2024-06-19 13:26:15', '2024-06-19 13:26:43');
INSERT INTO `sys_user_role` (`user_id`, `role_id`) VALUES (25286, 2003);
INSERT INTO `t_appraisal_member` (`id`, `dept_id`, `dept_name`, `user_name`, `user_code`, `user_id`, `job`, `user_status`, `register_date`, `cancel_date`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (38201, 22022, '泉州分公司', '案例40', 'case4d', 25286, NULL, 2, '2023-01-01', '2024-07-01', '2024-06-19 13:26:16', 1, '2024-06-19 13:26:44', 1, '已注销员工 注销考核权信息，注销时间：2024-06-19 13:26:44', NULL, b'0');
INSERT INTO `t_appraisal_dept` (`dept_id`, `appraisal_id`, `appraisal_right`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (22022, 38201, 100.00, NULL, '2024-06-19 13:26:16', 1, NULL, NULL, '初始化考核权部门', NULL, b'0');
INSERT INTO `t_appraisal_dept_magr` (`dept_id`, `appraisal_id`, `note`, `create_time`, `creator`, `update_time`, `updater`, `remark1`, `remark2`, `deleted`) VALUES (10400, 38201, NULL, '2024-02-28 14:40:10', 1, NULL, NULL, '初始化考核权管理部门', NULL, b'0')
;



-- 写步骤日志
set @step_info = '4.CASE测试数据变更';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);

-- case00

-- case01
update int_d_new_hrs_emp
set Join_End_Dt = '2024-05-23'
    ,Emp_Status = 4
where emp_id = 'case01'
;

-- case02
update int_d_new_hrs_emp
set Emp_Type_Code = '0410'
    ,Emp_Type_Name = '子公司人员'
where emp_id = 'case02'
;


-- case03
update int_d_new_hrs_emp
set org_code = '22029'
where emp_id = 'case03'
;

-- case04
update int_d_new_hrs_emp
set org_code = 'ZQ00010001'
where emp_id = 'case04'
;

-- case1s
update int_d_new_hrs_emp
set Join_End_Dt = '0001-01-01'
    ,Emp_Status = 1
where emp_id = 'case1s'
;

-- case20
update int_d_new_hrs_emp
set Emp_Type_Code = '0101'
    ,Emp_Type_Name = '在岗人员'
where emp_id = 'case20'
;


-- case40
update int_d_new_hrs_emp
set org_code = '22022'
where emp_id = 'case40'
;

-- case2d
update int_d_new_hrs_emp
set org_code = 'ZQ1920'
    ,Emp_Type_Code = '0101'
    ,Emp_Type_Name = '在岗人员'
where emp_id = 'case2d'
;


-- case4d
update int_d_new_hrs_emp
set org_code = 'ZQ5300'
where emp_id = 'case4d'
;


-- 写步骤日志
set @step_info = '5.模拟执行存储过程';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);

call PRO_LOAD_HR_EMP(CURRENT_DATE);



-- 写步骤日志
set @step_info = '6.写入检查日志表';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);

drop table if exists tmp_check_emp_change_log;
CREATE TEMPORARY TABLE `tmp_check_emp_change_log` (
  `check_date` date DEFAULT NULL COMMENT '检查日期',
  `user_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '检查用户名',
  `check_name` varchar(100) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '检查名称',
  `table_name` varchar(50) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '检查表名',
  `step_num` int DEFAULT NULL COMMENT '检查步骤',
  `status` tinyint DEFAULT '0' COMMENT '状态(0:正常，1:异常)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='检查员工变更日志表'
;

insert into tmp_check_emp_change_log(check_date,user_name,check_name,table_name,step_num,status)
select 
    current_date check_date
    ,t1.user_name
    ,t1.check_name
    ,t1.table_name
    ,1 step_num
    ,ifnull(t2._status,1) status   -- 0:正常 1:异常
from (
    select 
        user_name,current_flag,status,
        check_name
        ,table_name
    from t_check_emp_change_predict
    where user_name like 'case__'
    and table_name = 't_employee'
    and valid_status = 0
) t1
left join
(
    select code,current_flag,status,0 _status
    from t_employee
    where code like 'case__'
) t2 
on t1.user_name = t2.code
and t1.current_flag = t2.current_flag
and t1.status = t2.status
;


insert into tmp_check_emp_change_log(check_date,user_name,check_name,table_name,step_num,status)
select 
    current_date check_date
    ,t1.user_name
    ,t1.check_name
    ,t1.table_name
    ,2 step_num
    ,ifnull(t2._status,1) status   -- 0:正常 1:异常
from (
    select 
        user_name,status,account_status
        ,check_name,table_name
    from t_check_emp_change_predict
    where user_name like 'case__'
    and table_name = 'sys_user'
    and valid_status = 0
) t1
left join
(
    select 
        user_name,status,account_status,0 _status
    from sys_user
    where user_name like 'case__'
) t2 
on t1.user_name = t2.user_name
and t1.status = t2.status
and t1.account_status = t2.account_status
;



insert into tmp_check_emp_change_log(check_date,user_name,check_name,table_name,step_num,status)
select 
    current_date check_date
    ,t1.user_name
    ,t1.check_name
    ,t1.table_name
    ,3 step_num
    ,ifnull(t2._status,1) status   -- 0:正常 1:异常
from (
    select 
        user_status
        ,check_name,user_name,table_name
    from t_check_emp_change_predict
    where user_name like 'case__'
    and table_name = 't_appraisal_member'
    and valid_status = 0
) t1
left join
(
    select 
        user_code,user_status,0 _status
    from t_appraisal_member
    where user_code like 'case__'
) t2 
on t1.user_name = t2.user_code
and t1.user_name = t2.user_code
and t1.user_status = t2.user_status
;

delete from t_check_emp_change_log where check_date = current_date;
insert into t_check_emp_change_log(check_date,user_name,check_name,table_name,step_num,status)
select check_date,user_name,check_name,table_name,step_num,status from tmp_check_emp_change_log
order by user_name,step_num
;

-- 获得记录数
select 0 cnt into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;