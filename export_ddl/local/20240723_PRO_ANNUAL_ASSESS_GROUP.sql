DROP PROCEDURE IF EXISTS PRO_ANNUAL_ASSESS_GROUP;
CREATE PROCEDURE `PRO_ANNUAL_ASSESS_GROUP`(IN IN_PAR_YEAR CHAR(4))
label:BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_ANNUAL_ASSESS_GROUP
         功能简述：   年度考核生成考核组
         参数：       IN_PAR_YEAR     年份
         注意事项：
         数据源：
                 t_annual_assess_user 考核人员总览
         结果表：
                 t_annual_assess_group      考核组
                 t_annual_assess_group_user 考核组成员
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG          2023/12/05                 创建
         MG          2023/12/08                 新增【日期确认表】的逻辑
         MG          2023/12/11                 提测V21版本
         MG          2023/12/13                 考核组初始化逻辑调整
         MG          2023/12/20                 确认表确认人为绩效专员和绩效管理员
         MG          2023/12/25                 考核组成员存在问题
******************************************************************************************************************************************/

-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);
DECLARE V_BATCH_NO varchar(200);
DECLARE V_TOTAL_STEP_NUM int;

-- 设置变量
set V_EVENT_NAME = '年度考核生成考核组';
set V_TOTAL_STEP_NUM = 2;  -- 设置总步骤数
set @remark = ''; -- 步骤日志中的备注，可以每步都定义
select now() into V_START_TIME;


-- 解决字符集问题：Illegal mix of collations (utf8mb4_0900_ai_ci,IMPLICIT) and (utf8mb4_general_ci,IMPLICIT) for operation
set collation_connection = utf8mb4_general_ci;



set @par_year = IN_PAR_YEAR;   -- 格式：2023



set V_RUN_COMMAND = concat('call PRO_ANNUAL_ASSESS_GROUP('
                            ,IN_PAR_YEAR
                            ,')'
                         );

set V_BATCH_NO = concat(UNIX_TIMESTAMP(),'-'
                        ,replace(V_RUN_COMMAND,'call ','')
                        ,'-'
                        ,V_TOTAL_STEP_NUM
                     );




-- 写步骤日志
set @step_info = '1.年度考核考核组:插入考核组表数据';
set @remark = '考核组表数据初始化';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);
set @remark = '';


-- 获取考核判定时间
select assess_date into @assess_date from t_annual_assess_date where par_year = @par_year;




drop TEMPORARY table if exists tmp_dept_manager;
CREATE TEMPORARY TABLE `tmp_dept_manager` (
  `dept_id` bigint DEFAULT NULL COMMENT '部门ID',
  `manager_user_id` bigint DEFAULT null COMMENT '用户ID',
  `manager_user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
  `manager_nick_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  key idx_dept_id(dept_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '部门负责人_临时表'
;


drop TEMPORARY table if exists tmp_dept_commissioner;
CREATE TEMPORARY TABLE `tmp_dept_commissioner` (
  `dept_id` bigint DEFAULT NULL COMMENT '部门ID',
  `commissioner_user_id` bigint DEFAULT null COMMENT '用户ID',
  `commissioner_user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户账号',
  `commissioner_nick_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '用户昵称',
  `role_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '角色名称',
  key idx_dept_id(dept_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '绩效专员和管理员_临时表'
;


-- 取每个部门的一个部门负责人
insert into tmp_dept_manager(dept_id,manager_user_id,manager_user_name,manager_nick_name,role_name)
select 
    dept_id,manager_user_id,manager_user_name,manager_nick_name,role_name
from (
    select 
         a.dept_id   -- 部门
        ,a.user_id   manager_user_id -- 部门负责人用户id
        ,a.user_name manager_user_name
        ,a.nick_name manager_nick_name
        ,r.role_name
        ,row_number() over(partition by a.dept_id order by a.user_id asc) rn
    from sys_user a -- 考核部门下所有的人员
    inner join sys_user_role ur
        on a.user_id = ur.user_id 
    inner join sys_role r
        on ur.role_id = r.role_id
        and r.role_id = 2007
    where (
            (a.account_flag = 1 and a.status = 0 and a.account_status != 3) -- 实体用户
            or (a.account_flag = 2 and a.account_status = 0) -- 虚拟用户
        )
    group by  a.dept_id  
             ,a.user_id  
             ,a.user_name
             ,a.nick_name
             ,r.role_name
) x
where x.rn = 1
;

-- 取每个部门所有的绩效管理员和绩效专员
insert into tmp_dept_commissioner(dept_id,commissioner_user_id,commissioner_user_name,commissioner_nick_name,role_name)
select 
    dept_id,commissioner_user_id,commissioner_user_name,commissioner_nick_name,role_name
from (
    select 
         a.dept_id   -- 部门
        ,a.user_id   commissioner_user_id
        ,a.user_name commissioner_user_name
        ,a.nick_name commissioner_nick_name
        ,r.role_name
        ,row_number() over(partition by a.dept_id order by a.user_id asc) rn
    from sys_user a -- 考核部门下所有的人员
    inner join sys_user_role ur
        on a.user_id = ur.user_id 
    inner join sys_role r
        on ur.role_id = r.role_id
        and (r.role_name like '%绩效管理员%' or r.role_name like '%绩效专员%')
    where (
            (a.account_flag = 1 and a.status = 0 and a.account_status != 3) -- 实体用户
            or (a.account_flag = 2 and a.account_status = 0) -- 虚拟用户
        )
    group by  a.dept_id  
             ,a.user_id  
             ,a.user_name
             ,a.nick_name
             ,r.role_name
) x
;



delete from t_annual_assess_confirm where par_year = @par_year;
insert into t_annual_assess_confirm(par_year,dept_id,confirm_flag,remark,creator,create_time,updater,update_time)
select 
     distinct 
     a.par_year
    ,a.dept_id
    ,0 confirm_flag
    ,'数据初始化' remark
    ,1 creator
    ,now() create_time
    ,1 updater
    ,now() update_time
from t_annual_assess_user a
where a.par_year = @par_year
;


delete from t_annual_assess_confirm_dtl where par_year = @par_year;
insert into t_annual_assess_confirm_dtl(annual_assess_confirm_id,par_year,dept_id,user_id,creator,create_time,updater,update_time)
select
     a.id annual_assess_confirm_id
    ,a.par_year
    ,a.dept_id
    ,b.commissioner_user_id user_id
    ,1 creator
    ,now() create_time
    ,1 updater
    ,now() update_time
from t_annual_assess_confirm a
left join tmp_dept_commissioner b 
on a.dept_id = b.dept_id
where a.par_year = @par_year
;



/*
在强制分布组初始化的规则中，增补规则。
(1)“XX 分公司-营业部副职组”:以分公司为单位成组，将分公司下辖的全部营业部，考核判定时点的 HR 系统“管理人员属性”为 016营业部正职(卫星)、018 营业部副职、019 营业部助理的三类人员。
(2)“XX(部门名称) -全日制营销组”:以分公司本部、营业部 (含翻牌分公司)为单位，将考核判定时点为全日制营销人员的单独成组。
(3)“xX(部门名称)-员工组”:以事业部、总部、分公司本部、营业部/翻牌分公司为单位，将考核判定时点为除全日制营销之外的其他人员类别人员，合并成组。
(4)“分公司财务负责人组”: 将人员标签为“分公司财务负责人”的人员，纳入该强制分布组 (考核权部门为公司)
(5)“合规与风险管理负责人”: 将人员标签为“风险管理负责人”“合规管理负责人”“合规与风险管理负责人”的人员纳入该强制分
布组 (考核权部门为公司)
(6)“XX(部门)-专职合规风控组”:以部门为单位，将标签为“专职合规人员”“专职风险管理人员”“专职合规与风险管理人员”的人员纳入该组(考核权部门为所在部门)。[部门包括: 各事业部、总部西部子公司本部、承销保荐公司、资管公司]
(7)“XX 分公司-专职合规组”: 以分公司为单位，将分公司本部和下辖全部营业部中，标签为“专职合规人员”的人员纳入该组 (考核权部门为分公司)
(8)“客服组”:将人员标签为“客服人员”的人员纳入该组，考核权部门门为财富管理事业部。
[以上规则中: 如同时触发标签、人员类别规则，标签规则优先]


*/

drop temporary table if exists tmp_annual_assess_group_user;
create temporary table tmp_annual_assess_group_user
select 
     a.par_year
    ,a.dept_id
    ,a.user_id
    ,case 
        when a.emp_tag_code = 10 and a.dept_id = 1000 then '分公司财务负责人组'
        when a.emp_tag_code in (11,12,13) and a.dept_id = 1000 then '合规与风险管理负责人'
        when a.emp_tag_code in (14,15,16) and (d.dept_name like '%事业部%' or d.dept_id in (99800,91212,91213) or d.org_type = 4) then concat(ifnull(d.dept_simple_name,d.dept_name),'-专职合规风控组')
        when a.emp_tag_code = 14 and d.org_type = 6 then concat(ifnull(d.dept_simple_name,d.dept_name),'-专职合规组')
        when a.emp_tag_code = 17 and d.dept_id = 11600 then '客服组'
        when d.org_type = 2 and a.Managercode in ('016','018','019') then concat(COALESCE(p.dept_simple_name,p.dept_name),'-营业部副职组')
        when a.pd_emp_type_code = '0201' then concat(ifnull(d.dept_simple_name,d.dept_name),'-全日制营销组')
        else concat(ifnull(d.dept_simple_name,d.dept_name),'-员工组')
     end  group_name
    ,dm.manager_user_id result_importer
    ,'数据初始化' remark
    ,1 creator
    ,now() create_time
    ,1 updater
    ,now() update_time
from t_annual_assess_user a
left join sys_dept d 
    on a.dept_id = d.dept_id
left join sys_dept p
    on d.parent_id = p.dept_id
left join tmp_dept_manager dm
    on a.dept_id = dm.dept_id
where a.par_year = @par_year
;




delete from t_annual_assess_group where par_year = @par_year;
insert into t_annual_assess_group(par_year,dept_id,group_name,result_importer,remark,creator,create_time,updater,update_time)
select 
    distinct par_year
    ,dept_id
    ,group_name
    ,result_importer
    ,'数据初始化' remark
    ,1 creator
    ,now() create_time
    ,1 updater
    ,now() update_time
from tmp_annual_assess_group_user
where par_year = @par_year
;



delete from t_annual_assess_group_user where par_year = @par_year;
insert into t_annual_assess_group_user(par_year,dept_id,group_id,user_id,remark,creator,create_time,updater,update_time)
select 
     a.par_year
    ,a.dept_id
    ,a.id group_id
    ,b.user_id
    ,'数据初始化' remark
    ,1 creator
    ,now() create_time
    ,1 updater
    ,now() update_time
from t_annual_assess_group a
left join tmp_annual_assess_group_user b 
    on a.par_year = b.par_year
    and a.dept_id = b.dept_id
    and a.group_name = b.group_name
where a.par_year = @par_year
;





-- 写步骤日志
set @step_info = '2.年度考核考核组:更新进度表';
set @remark = '考核组更新进度表';
call PRO_PROCEDURE_STEP_LOG(V_BATCH_NO,V_EVENT_NAME,@step_info,@remark);
set @remark = '';


-- 更新进度表
update t_annual_assess_step_total_dept 
set status = 2
    ,group_create_date = current_date
where par_year = @par_year
and step_id = 1
;




-- 获得记录数
select 0 cnt into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;