DROP PROCEDURE IF EXISTS PRO_PROCEDURE_STEP_LOG;
CREATE PROCEDURE `PRO_PROCEDURE_STEP_LOG`(in I_BATCH_NO      VARCHAR(200),  -- 批次号  
                                     in I_PROCEDURE_CN       VARCHAR(50),   -- 存储过程中文名
                                     in I_STEP               VARCHAR(500),  -- 步骤信息
                                     in I_REMARK             VARCHAR(500)   -- 备注
                                     )
label:BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_PROCEDURE_STEP_LOG
         功能简述：   记录存储过程关键步骤，用于问题分析和监控
         参数：       I_BATCH_NO      样例：【1689045909-PRO_YJGL_COMPLETED_VALUE(202305,11230,1)-5】
                      I_PROCEDURE_CN  样例：【考核完成值】
                      I_STEP          样例：【1.参数定义:配置参数成功!】  或 【1.参数定义】
                      I_REMARK        样例：备注内容
         注意事项：
         数据源：
         结果表：
                 t_procedure_step_log                     存储过程步骤日志
         后续优化：
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2023/07/11                  创建该存储过程
******************************************************************************************************************************************/
    DECLARE V_BATCH_ID           INT UNSIGNED;  -- 存储过程名称
    DECLARE V_PROCEDURE_NAME     VARCHAR(50);  -- 存储过程名称
    DECLARE V_PARAMETER_LIST     VARCHAR(100); -- 参数列表
    DECLARE V_TOTAL_STEP_NUM     INT;          -- 总步骤数
    DECLARE V_STEP_SEQ           INT;          -- 步骤序号
    DECLARE V_STEP_NAME          VARCHAR(50);  -- 步骤名称
    DECLARE V_STEP_INFO          VARCHAR(255); -- 步骤详细信息
    
    
    -- 处理数据
    set @left_pos   = locate('-', I_BATCH_NO);  -- 第一个-的位置
    set @right_pos  = locate('-', I_BATCH_NO, @left_pos+1);  -- 第二个-的位置
    set @mid_length = @right_pos - @left_pos;
    
    set @para_info = substr(I_BATCH_NO, @left_pos+1, @mid_length-1);
    set V_BATCH_ID = left(I_BATCH_NO, @left_pos-1);
    set V_PROCEDURE_NAME = left(@para_info,locate('(',@para_info)-1);
    set V_PARAMETER_LIST = substr(@para_info
                                  ,locate('(',@para_info)+1
                                  ,locate(')',@para_info) - locate('(',@para_info)-1
                                 );
    
    set V_TOTAL_STEP_NUM = substr(I_BATCH_NO,@right_pos+1);
    -- select I_BATCH_NO,@left_pos,@right_pos,@mid_length,V_PROCEDURE_NAME,V_PARAMETER_LIST,V_TOTAL_STEP_NUM;

    
    set @dot_pos   = locate('.', I_STEP);
    set @colon_pos = locate(':', replace(I_STEP, '：', ':'));
    -- 判断是否有冒号
    if @colon_pos = 0 then 
        set V_STEP_NAME = substr(I_STEP, @dot_pos+1);
        set V_STEP_INFO = null; 
    else 
        set @mid_len   = @colon_pos - @dot_pos;
        set V_STEP_NAME = substr(I_STEP, @dot_pos+1, @mid_len-1);
        set V_STEP_INFO = substr(I_STEP, @colon_pos+1);
    end if;
    
    set V_STEP_SEQ  = left(I_STEP, @dot_pos);
    
    -- LEAVE label; -- 退出存储过程 
    -- select I_STEP,@dot_pos,@colon_pos,@mid_len,V_STEP_SEQ,V_STEP_NAME,V_STEP_INFO;
    
    
    if I_REMARK = '' then
       set I_REMARK = null;
    end if;
    
    
    -- 插入数据到结果表中
    insert into t_procedure_step_log(batch_id,batch_no,data_date,data_time,procedure_name,procedure_name_cn,parameter_list,total_step_num,step_seq,step_name,step_info,remark,create_time)
    select 
         V_BATCH_ID            as  batch_id
        ,I_BATCH_NO            as  batch_no
        ,CURRENT_DATE          as  data_date
        ,CURRENT_TIME          as  data_time
        ,V_PROCEDURE_NAME      as  procedure_name
        ,I_PROCEDURE_CN        as  procedure_name_cn
        ,V_PARAMETER_LIST      as  parameter_list
        ,V_TOTAL_STEP_NUM      as  total_step_num
        ,V_STEP_SEQ            as  step_seq
        ,V_STEP_NAME           as  step_name
        ,V_STEP_INFO           as  step_info
        ,I_REMARK              as  remark
        ,now()                 as  create_time
    ;
END
;