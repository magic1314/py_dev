DROP PROCEDURE IF EXISTS PRO_SCORE_MEMBER_CALC;
CREATE PROCEDURE `PRO_SCORE_MEMBER_CALC`(IN IN_YEAR_MONTH VARCHAR(6), IN IN_DEPT_ID INT, IN IN_PERIOD INT)
label:BEGIN
/************************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_SCORE_MEMBER_CALC
         功能简述：   考核评价模块_员工打分的计算
         参数：       IN_YEAR_MONTH    考核年月
                      IN_DEPT_ID       部门编号(必须指定部门)
                      IN_PERIOD        考核周期(1:月,2:季度,3:半年，不能为0)
         返回：       无
         算法：
         注意事项：
                 1. 传入日期的上个月，是考核的月份
                 eg.  今日为：202303
                      考核月份是：202303，也就是结果表中year_months的取值
                 2. 维度
                   1)dept_id(部门) 
                   2)assess_year(年份)
                   3)assess_month(月份)
                   4)assess_pd(考核周期)
                   
                   5)year_months(年月)：等同于【年份+月份】
                   6)assess_pd_value(考核周期值)：等同于【考核周期+月份】
                   
                   故常见搭配的等价组合：
                              a) (1234) dept_id + assess_year + assess_month + assess_pd（本程序未使用）
                              b) (126)  dept_id + assess_year + assess_pd_value(本程序少量使用)
                              c) (145)  dept_id + assess_pd + year_months (本程序主要使用)
                              -- 其他
                              d) (156) dept_id + year_months + assess_pd_value (冗余)
                 
         数据源：
                    1. t_assess_index_score_result              定性和定量指标汇总表
                    2. t_assess_index_score_result_adjust       调整项考核打分明细表
                    3. t_appraisal_dept_magr                    考核管理部门
                    4. t_assess_dept_not_assess_member_locked   不考评名单_已锁定
         目标表：
                    1. t_assess_member_score_dtl    员工考核结果指标明细表  
                    2. t_assess_member_score        员工考核结果表          
                    3. t_assess_dept_member_score   员工多部门考核结果表    
         修改记录;
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2023/04/26                  创建
         MG         2023/05/07                  t_assess_member_score_dtl中得分需要除以考核部门个数,其他问题
         dzq        2023/05/24                  关联t_appraisal_member时 需要考虑考核权的生效区间
         MG         2023/06/07                  新增dept_appraisal_id字段；删除之前无效的考核权限制；修改部门考核权权重
         MG         2023/06/08                  考核临时表问题修复
         MG         2023/06/09                  参数定义遗漏
         MG         2023/11/16                  新增【员工综合得分】表
         MG         2023/11/28                  参数说明补充
         MG         2023/12/08                  最终得分超过100，按100分计算
         MG         2024/01/15                  多个平分考核权重的逻辑调整
         MG         2024/02/26                  bug修复
         MG         2024/03/21                  已锁定状态下，添加不考评名单
         MG         2024/07/19                  有效考核权逻辑调整
************************************************************************************************************************************************/
-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);

-- 设置变量
set V_EVENT_NAME = '考核评价模块_员工打分的计算';
set V_RUN_COMMAND = concat('call PRO_SCORE_MEMBER_CALC('
                            ,IN_YEAR_MONTH,','
                            ,IN_DEPT_ID,','
                            ,IN_PERIOD
                            ,')'
                         );



select now() into V_START_TIME;


set @assess_year_month = IN_YEAR_MONTH; -- 格式: 202302
set @assess_month_2str = right(@assess_year_month,2);   -- 格式：02
set @assess_month = cast(right(@assess_year_month,2) as UNSIGNED);   -- 格式：2
set @assess_year = left(@assess_year_month,4);   -- 格式：2023
set @dept_id = IN_DEPT_ID;   -- 格式： 12800
set @assess_pd = IN_PERIOD;  -- 格式： 1
set @assess_pd_value = (select assess_pd_value from t_dict_assess_pd_mqh where assess_pd = @assess_pd and assess_month = @assess_month);


-- 最小到指标的粒度
delete from t_assess_member_score_dtl where par_year = @assess_year and assess_pd_value = @assess_pd_value and dept_id = @dept_id;
insert into t_assess_member_score_dtl(par_year,assess_pd,assess_pd_value,dept_id,dept_appraisal_id,magr_dept_id,group_id,member_id,index_code,index_appraisal_right,business_type
    ,current_tgt,complete_info,index_score,assess_score,remark,assess_member_socre_id)
select
     a.par_year                                                                         -- 考核年份
    ,a.assess_pd                                                                        -- 考核周期
    ,a.assess_pd_value                                                                  -- 考核周期值
    ,a.dept_id                                                                          -- 考核权部门编码
    ,a.dept_appraisal_id                                                                -- 考核权成员表id
    ,group_concat(distinct c.dept_id order by c.dept_id separator ',')    magr_dept_id  -- 考核管理部门编码(多部门以逗号分割)
    ,a.group_id                                                                         -- 考核组ID
    ,a.member_id                                                                        -- 被考核人员ID
    ,a.index_code                                                                       -- 指标代码
    ,a.index_appraisal_right                                                            -- 指标考核权重
    ,a.business_type                                                                    -- 指标类型(1-5:定量，6：定性)
    ,a.current_tgt                                                                      -- 考核目标   -- 对于定量，这是是否需要汇总
    ,a.complete_info                                                                    -- 完成情况   -- 对于定量，这是是否需要汇总
    ,sum(a.index_score  * a.appraisal_right/100 / a.appraisal_group_cnt)/count(distinct c.dept_id) index_score   -- 指标单项得分
    ,sum(a.assess_score * a.appraisal_right/100 / a.appraisal_group_cnt)/count(distinct c.dept_id) assess_score  -- 计入考核得分=指标单项得分*考核权重
    ,'打分后自动计算' remark                                                            -- 备注
    ,null   assess_member_socre_id                                                      -- 员工考核结果表ID
from t_assess_index_score_result a
left join t_appraisal_dept_magr c -- 一个人可能有多个考核部门
    on a.dept_appraisal_id = c.appraisal_id
    and c.deleted = 0
left join t_assess_dept_not_assess_member_locked lck  -- 已锁定状态，不参与考核名单
    on a.par_year = lck.par_year 
    and a.assess_pd_value = lck.assess_pd_value
    and a.dept_id = lck.dept_id
    and a.member_id = lck.member_id
    and lck.deleted = 0
where a.par_year = @assess_year 
    and a.assess_pd_value = @assess_pd_value 
    and a.dept_id = @dept_id
    and lck.id is null
group by a.par_year       
         ,a.assess_pd      
         ,a.assess_pd_value
         ,a.dept_id        
         ,a.dept_appraisal_id
         ,a.group_id       
         ,a.member_id      
         ,a.index_code     
         ,a.index_appraisal_right
         ,a.business_type  
         ,a.current_tgt    
         ,a.complete_info  
;




-- 分数是在本考核部门上
delete from t_assess_member_score where par_year = @assess_year and assess_pd_value = @assess_pd_value and dept_id = @dept_id;
insert into t_assess_member_score(par_year,assess_pd,assess_pd_value,dept_id,dept_appraisal_id,magr_dept_id,group_id,member_id,assess_score,adjust_score,final_assess_score,creator,updater,remark,assess_dept_member_score_id)
select 
     a.par_year                                             -- 考核年份
    ,a.assess_pd                                            -- 考核周期
    ,a.assess_pd_value                                      -- 考核周期值
    ,a.dept_id                                              -- 考核权部门编码
    ,a.dept_appraisal_id                                    -- 考核权成员表id
    ,a.magr_dept_id                                         -- 考核管理部门编码(多部门以逗号分割)
    ,a.group_id                                             -- 考核组ID
    ,a.member_id                                            -- 被考核人员ID
    ,sum(a.assess_score) assess_score                       -- 本期绩效得分=计入考核得分合计
    ,avg(b.adjust_score) adjust_score                       -- 调整项得分（针对的是人）
    ,if(sum(a.assess_score)+avg(b.adjust_score)>100,100,sum(a.assess_score)+avg(b.adjust_score)) final_assess_score  -- 考核得分=本期绩效得分+调整项得分
    ,null creator                                           -- 创建人
    ,null updater                                           -- 修改人
    ,'打分后自动计算' remark                                -- 备注
    ,null assess_dept_member_score_id                       -- 员工多部门考核结果表ID
from t_assess_member_score_dtl a
left join t_assess_index_score_result_adjust b -- 一个人员调整项得分就一个，唯一关联关系
on a.par_year = b.par_year 
    and a.assess_pd_value = b.assess_pd_value
    and a.dept_id = b.dept_id 
    and a.group_id = b.assess_group_id
    and a.member_id = b.member_id
where a.par_year = @assess_year 
    and a.assess_pd_value = @assess_pd_value 
    and a.dept_id = @dept_id
group by a.par_year       
        ,a.assess_pd      
        ,a.assess_pd_value
        ,a.dept_id        
        ,a.dept_appraisal_id
        ,a.magr_dept_id   
        ,a.group_id       
        ,a.member_id      
;

-- 部门考核权权重，临时表
-- 校验：cnt应该=1
drop temporary table if exists tmp_appraisal_right;
create temporary table tmp_appraisal_right
select @assess_year par_year,dept_id,user_id,dept_appraisal_id,max(dept_appraisal_right) dept_appraisal_right,count(distinct dept_appraisal_right) cnt
from t_assess_info_his a
inner join t_dict_assess_pd_mqh mqh 
on a.assess_pd_value = mqh.assess_pd_value
and (@assess_month_2str between mqh.assess_start_month_2str and mqh.assess_end_month_2str)
where dept_id = @dept_id
and dept_appraisal_id is not null
group by dept_id,user_id,dept_appraisal_id
;



-- 员工多部门考核结果表
delete from t_assess_dept_member_score where par_year = @assess_year and assess_pd_value = @assess_pd_value and dept_id = @dept_id;
insert into t_assess_dept_member_score(par_year,assess_pd,assess_pd_value,dept_id,dept_appraisal_id,appraisal_right,magr_dept_id,group_id,member_id,final_assess_score,creator,updater,remark)
select
     a.par_year                                 -- 考核年份
    ,a.assess_pd                                -- 考核周期
    ,a.assess_pd_value                          -- 考核周期值
    ,a.dept_id                                  -- 考核权部门编码
    ,a.dept_appraisal_id                        -- 考核权成员表id
    ,b.dept_appraisal_right appraisal_right     -- 考核权部门权重
    ,a.magr_dept_id                             -- 考核管理部门编码(多部门以逗号分割)
    ,a.group_id                                 -- 考核组ID
    ,a.member_id                                -- 被考核人员ID
    ,(a.final_assess_score) final_assess_score  -- 本期最终得分
    ,null creator                               -- 创建人
    ,null updater                               -- 修改人
    ,'打分后自动计算' remark                    -- 备注
from t_assess_member_score a
left join tmp_appraisal_right b 
    on a.member_id = b.user_id
    and a.dept_appraisal_id = b.dept_appraisal_id
where a.par_year = @assess_year 
    and a.assess_pd_value = @assess_pd_value 
    and a.dept_id = @dept_id
;


-- 更新关联的id字段
update t_assess_member_score a
inner join t_assess_dept_member_score b
    on  a.par_year        = b.par_year 
    and a.assess_pd_value = b.assess_pd_value
    and a.dept_id         = b.dept_id 
    and a.group_id        = b.group_id
    and a.member_id       = b.member_id
set a.assess_dept_member_score_id = b.id
where a.par_year = @assess_year 
    and a.assess_pd_value = @assess_pd_value 
    and a.dept_id = @dept_id
;


update t_assess_member_score_dtl a
inner join t_assess_member_score b
    on  a.par_year        = b.par_year 
    and a.assess_pd_value = b.assess_pd_value
    and a.dept_id         = b.dept_id 
    and a.group_id        = b.group_id
    and a.member_id       = b.member_id
set a.assess_member_socre_id = b.id
where a.par_year = @assess_year 
    and a.assess_pd_value = @assess_pd_value 
    and a.dept_id = @dept_id
;


/************************* 员工综合得分 *************************/
delete from t_assess_member_final_score where par_year = @assess_year and assess_pd_value = @assess_pd_value;
insert into t_assess_member_final_score(par_year,assess_pd,assess_pd_value,member_id,final_score)
select 
     @assess_year par_year
    ,a.assess_pd
    ,a.assess_pd_value
    ,a.member_id
    ,sum(a.final_assess_score*a.appraisal_right/100) final_score
from t_assess_dept_member_score a
where a.par_year = @assess_year
and a.assess_pd_value = @assess_pd_value
group by a.assess_pd
    ,a.assess_pd_value
    ,a.member_id
;




-- 获得记录数
select count(1) cnt into V_TABLE_CNT from t_assess_dept_member_score 
where par_year = @assess_year and assess_pd_value = @assess_pd_value and dept_id = @dept_id;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;