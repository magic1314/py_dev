DROP PROCEDURE IF EXISTS PRO_SCORE_ASSESS_INIT_AFTER;
CREATE PROCEDURE `PRO_SCORE_ASSESS_INIT_AFTER`(IN IN_YEAR_MONTH VARCHAR(6), IN IN_DEPT_ID INT, IN IN_PERIOD INT)
label:BEGIN
/************************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_SCORE_ASSESS_INIT_AFTER
         功能简述：   考核评价模块数据初始化补充
         参数：       IN_YEAR_MONTH    考核年月
                      IN_DEPT_ID       部门编号(必须指定部门)
                      IN_PERIOD        考核周期(1:月,2:半年,3:季度)
         返回：       无
         数据源：
         目标表：
         注意事项：
                      等java处理完成后再执行的存储过程
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG          2024/07/11                 创建该过程
         MG          2024/07/18                 新增更新result表id值的逻辑
         MG          2024/07/19                 移除更新result表id值的逻辑
************************************************************************************************************************************************/
-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);

-- 设置变量
set V_EVENT_NAME = '考核评价模块数据初始化补充';
set V_RUN_COMMAND = concat('call PRO_SCORE_ASSESS_INIT_AFTER('
                            ,IN_YEAR_MONTH,','
                            ,IN_DEPT_ID,','
                            ,IN_PERIOD
                            ,')'
                         );



select now() into V_START_TIME;


set @year_months = IN_YEAR_MONTH; -- 格式: 202302
set @assess_month = cast(right(@year_months,2) as UNSIGNED);   -- 格式：2
set @par_year = left(@year_months,4);   -- 格式：2023
set @dept_id = IN_DEPT_ID;   -- 格式： 12800
set @assess_pd = IN_PERIOD;  -- 格式： 1
set @assess_pd_value = (select assess_pd_value from t_dict_assess_pd_mqh where assess_pd = @assess_pd and assess_month = @assess_month);




-- 获得记录数
select 0 into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;