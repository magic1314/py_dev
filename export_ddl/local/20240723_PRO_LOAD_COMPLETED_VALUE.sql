DROP PROCEDURE IF EXISTS PRO_LOAD_COMPLETED_VALUE;
CREATE PROCEDURE `PRO_LOAD_COMPLETED_VALUE`(IN BIZ_DATE DATE)
label:BEGIN
/**********************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_LOAD_COMPLETED_VALUE
         功能简述：   导入指标完成值表
         参数：       BIZ_DATE    业务日期
         注意事项：
                 * 1、如果有新增数据源，需要修改sys_dept中origin_system字段
                 2、增量插入的方式，对于同一个来源系统，同个周期不会重复插入
         数据源：
                 1、 int_d_hrp_crm_zbz               关键绩效指标值表_零售CRM
                 2、 int_d_hrp_crm_jgzbz             关键绩效指标值表_机构CRM
                 3、 int_d_hrp_tg_achieve_detail     关键绩效指标值表_托管中心
                 4、 t_dict_assess_pd_mqh            周期字典表
         结果表：
                 t_emp_perf_cmplt_val_src            指标完成值原始数据汇总表
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2023/04/25                  创建
         MG         2023/04/26                  目标表完成值小数位数从2位调整为6位
         MG         2023/08/16                  全删全插
         MG         2023/08/17                  增量插入，如果某个月份数据插入后，将不再插入数据
        苏彦运      2023/09/26                  插入托管中心数据
         MG         2023/09/28                  数据格式调整
         MG         2024/01/15                  【业绩系统取数时间】字段新增
**********************************************************************************************************************************/

-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);

-- 设置变量
set V_EVENT_NAME = '导入指标完成值表';
set V_RUN_COMMAND = concat('call PRO_LOAD_COMPLETED_VALUE(',BIZ_DATE,')');
select now() into V_START_TIME;


insert into t_emp_perf_cmplt_val_src(year_months,emp_code,ident_id,unique_code,dept_id_src,origin_system
    ,index_code,index_value,index_dept_id,remark )
select
     concat(a.nf,b.assess_month_2str) year_months                      -- 年月
    ,a.yggh                           emp_code                         -- 工号
    ,a.zjbh                           ident_id                         -- 证件编码
    ,concat(a.nf,'_',a.gszqz,'_',a.yggh,'_',a.zbdm) unique_code       -- 唯一的组合编码
    ,null                             dept_id_src                      -- 原系统员工所属部门
    ,1                                origin_system                    -- 指标来源
    ,a.zbdm                           index_code                       -- 指标代码
    ,a.zbz                            index_value                      -- 指标值
    ,null                             index_dept_id                    -- 指标所属部门
    ,'数据初始化_零售CRM'             remark                           -- 备注
from int_d_hrp_crm_zbz a
inner join t_dict_assess_pd_mqh b
on a.gszqz = b.assess_pd_value
where concat(a.nf,b.assess_month_2str) not in (select distinct year_months from t_emp_perf_cmplt_val_src where origin_system=1 )
;


insert into t_emp_perf_cmplt_val_src(year_months,emp_code,ident_id,unique_code,dept_id_src,origin_system
    ,index_code,index_value,index_dept_id,remark )
select
     concat(a.nf,b.assess_month_2str) year_months                      -- 年月
    ,a.yggh                           emp_code                         -- 工号
    ,a.zjbh                           ident_id                         -- 证件编码(机构CRM没有证件编号,这里不约束)
    ,concat(a.nf,'_',a.gszqz,'_',a.yggh,'_',a.zbdm) unique_code        -- 唯一的组合编码
    ,12800                            dept_id_src                      -- 原系统员工所属部门
    ,2                                origin_system                    -- 指标来源
    ,a.zbdm                           index_code                       -- 指标代码
    ,a.zbz                            index_value                      -- 指标值
    ,null                             index_dept_id                    -- 指标所属部门
    ,'数据初始化_机构CRM'             remark                           -- 备注
from int_d_hrp_crm_jgzbz a
inner join t_dict_assess_pd_mqh b
on a.gszqz = b.assess_pd_value
where concat(a.nf,b.assess_month_2str) not in (select distinct year_months from t_emp_perf_cmplt_val_src where origin_system=2 )
;

insert into t_emp_perf_cmplt_val_src(year_months,emp_code,ident_id,unique_code,dept_id_src,origin_system
    ,index_code,index_value,index_dept_id,remark)
select
     a.stats_month                             year_months                              -- 年月
    ,a.emp_id                                  emp_code                                 -- 工号
    ,a.cert_no                                 ident_id                                 -- 证件编码
    ,concat(a.kpi_year,'_',a.gszqz,'_',a.emp_id,'_',a.element_key) unique_code          -- 唯一的组合编码
    ,13400                                     dept_id_src                              -- 原系统员工所属部门
    ,3                                         origin_system                            -- 指标来源
    ,a.element_key                             index_code                               -- 指标代码
    ,a.achieve_value                           index_value                              -- 指标值
    ,null                                      index_dept_id                            -- 指标所属部门
    ,'数据初始化_托管中心'                     remark                                   -- 备注
from int_d_hrp_tg_achieve_detail a
inner join t_dict_assess_pd_mqh b
on a.gszqz = b.assess_pd_value
where a.stats_month not in (select distinct year_months from t_emp_perf_cmplt_val_src where origin_system=3 )
;


-- 业绩系统取数时间字段
insert into t_emp_perf_generate_time(year_months,origin_system,perf_generate_time,min_time,max_time)
select 
    a.year_months,a.origin_system,min(a.create_time) perf_generate_time,min(a.create_time) min_time,max(a.create_time) max_time
from t_emp_perf_cmplt_val_src a
left join t_emp_perf_generate_time b 
    on a.year_months = b.year_months
    and a.origin_system = b.origin_system
where b.id is null
group by a.year_months,a.origin_system
;


-- 获得记录数
select count(1) cnt into V_TABLE_CNT from t_emp_perf_cmplt_val_src;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,BIZ_DATE,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;
END
;