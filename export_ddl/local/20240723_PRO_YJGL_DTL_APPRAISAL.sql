DROP PROCEDURE IF EXISTS PRO_YJGL_DTL_APPRAISAL;
CREATE PROCEDURE `PRO_YJGL_DTL_APPRAISAL`(IN IN_PAR_YEAR INT,IN IN_DEPT_ID INT)
label:BEGIN
/***************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_YJGL_DTL_APPRAISAL
         功能简述：   业绩管理-考核方案审批后列定
         参数：       IN_PAR_YEAR     年份(1月到上月)
                      IN_DEPT_ID      部门编码(指定部门,不支持0)
         注意事项：
                 1. 第一个月为上月，不会轮询当月
                 2. 不计算202305及之前周期的数据
         数据源：
                 1、 PRO_YJGL_ASSESS_INFO_HIS             存储过程-考核方案列定
         结果表：
                 t_assess_info_his                        考核方案列定
         修改记录:
         ------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG          2023/10/11                 创建
         MG          2023/10/12                 部门编码做限制
         MG          2023/11/13                 审批后同时生成业绩数据
         MG          2023/01/17                 如果考核进度已开启，不再计算业绩数据
******************************************************************************************************************************************/

-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);
DECLARE V_BATCH_NO varchar(200);
DECLARE loop_i INT DEFAULT 1;
DECLARE loop_j INT DEFAULT 1;


-- 设置变量
set V_EVENT_NAME = '考核方案审批后列定';

select now() into V_START_TIME;


-- 解决字符集问题：Illegal mix of collations (utf8mb4_0900_ai_ci,IMPLICIT) and (utf8mb4_general_ci,IMPLICIT) for operation
-- set collation_connection = utf8mb4_general_ci;

set @par_year = IN_PAR_YEAR; -- 格式: 2023
set @dept_id = IN_DEPT_ID; -- 格式：25612


set V_RUN_COMMAND = concat('call PRO_YJGL_DTL_APPRAISAL('
                            ,IN_PAR_YEAR,','
                            ,IN_DEPT_ID
                            ,')'
                         );


if @dept_id = 0 then
    select '传入参数【部门编码】不能为0'; LEAVE label; -- 退出存储过程 
end if; 



/************ 业绩管理-完成值计算轮询 ************/
drop TEMPORARY table if exists tmp_pro_loop_months;
CREATE TEMPORARY TABLE `tmp_pro_loop_months` (
  `seq` bigint NOT NULL DEFAULT '0',
  `months` varchar(6) COLLATE utf8mb4_general_ci DEFAULT NULL,
  status tinyint default '0' comment '状态(0:正常，1：无效)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '临时表-用于轮询月份'
;


insert into tmp_pro_loop_months(seq,months)
select seq,months
from (
    SELECT 1 seq,concat(@par_year,'01') months
    UNION ALL
    SELECT 2 seq,concat(@par_year,'02') months
    UNION ALL
    SELECT 3 seq,concat(@par_year,'03') months
    UNION ALL
    SELECT 4 seq,concat(@par_year,'04') months
    UNION ALL
    SELECT 5 seq,concat(@par_year,'05') months
    UNION ALL
    SELECT 6 seq,concat(@par_year,'06') months
    UNION ALL
    SELECT 7 seq,concat(@par_year,'07') months
    UNION ALL
    SELECT 8 seq,concat(@par_year,'08') months
    UNION ALL
    SELECT 9 seq,concat(@par_year,'09') months
    UNION ALL
    SELECT 10 seq,concat(@par_year,'10') months
    UNION ALL
    SELECT 11 seq,concat(@par_year,'11') months
    UNION ALL
    SELECT 12 seq,concat(@par_year,'12') months
) x
where months < DATE_FORMAT(current_date,'%Y%m')
;

-- 如果是营业部，202305及之前的数据，不会列定数据
update tmp_pro_loop_months a
inner join sys_dept b 
on b.dept_id = @dept_id
and b.org_type = 2 
set a.status = 1
where a.months <= '202305'
;



drop TEMPORARY table if exists tmp_pro_loop_assess_pd;
CREATE TEMPORARY TABLE `tmp_pro_loop_assess_pd` (
  `seq` bigint NOT NULL DEFAULT '0',
  `assess_pd` int comment '考核周期值',
  status tinyint default '0' comment '状态(0:正常，1：无效)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci comment '临时表-用于轮询月份'
;



-- 循环遍历固定次数
SET loop_i = 1;
WHILE loop_i <= 12 DO  -- 最多12次
    select count(1) into @cnt from tmp_pro_loop_months where seq = loop_i and status = 0;
    IF @cnt = 0 THEN 
        SELECT '@year_months is NULL' into @info; -- 不输出内容
    ELSE
        -- 获取字段值
        select months into @year_months from tmp_pro_loop_months where seq = loop_i and status = 0;
        call PRO_YJGL_ASSESS_INFO_HIS(@year_months,@dept_id,0);
        
        truncate table tmp_pro_loop_assess_pd;
        insert into tmp_pro_loop_assess_pd(seq,assess_pd)
        select row_number() over(order by assess_pd) seq,assess_pd
        from (select distinct assess_pd from t_assess_dept_progress where par_year = @par_year and dept_id = @dept_id) x
        ;
        
        SET loop_j = 1;
        WHILE loop_j <= 3 DO
            select count(1) into @cnt from tmp_pro_loop_assess_pd where seq = loop_j and status = 0;
            IF @cnt = 0 THEN 
                SELECT 'It is not impossible' into @info; -- 不输出内容
            ELSE 
                select assess_pd into @assess_pd from tmp_pro_loop_assess_pd where seq = loop_j and status = 0;
                
                -- 只有未开启考核，才会取更新业绩数据
                select count(1) into @is_run from t_assess_dept_progress a
                left join t_dict_assess_pd_mqh b 
                on a.assess_pd_value = b.assess_pd_value
                where concat(a.par_year,b.assess_month_2str) = @year_months
                and a.dept_id = @dept_id
                and a.assess_pd = @assess_pd
                and a.status = 0
                ;
                IF @is_run = 1 THEN
                    call PRO_YJGL_COMPLETED_VALUE(@year_months,@dept_id,@assess_pd);
                END IF
                ;
            END IF;
            
            SET loop_j = loop_j + 1;
        END WHILE
        ;
    END IF;

    SET loop_i = loop_i + 1;
END WHILE
;





-- 获得记录数
select 0 cnt into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;