#encoding: utf-8
import sys
import os
import requests
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
import urllib.parse
import time
from public import write_log
from module.browser_base import BB

# 模块信息
from module.utils import access_url


# 配置
from conf.get_param import main as get_param


# 目标网页URL
index_url = get_param('index_url')

# 打开方式，min,normal,backgroup
browser_type = get_param('browser_type')

key_id = get_param('key_id')
key_name = get_param('key_name')
key_word = get_param('key_word')



# 标题头
heading_format = '/html/body/div/div[2]/div[2]/div[XXX]/div[1]/h3/a'
heading = {
    'h1': heading_format.replace('XXX', '2'),
    'h2': heading_format.replace('XXX', '3'),
    'h3': heading_format.replace('XXX', '4'),
    'h4': heading_format.replace('XXX', '5'),
    'h5': heading_format.replace('XXX', '6'),
    'h6': heading_format.replace('XXX', '7'),
    'h7': heading_format.replace('XXX', '8'),
    'h8': heading_format.replace('XXX', '9'),
    'h9': heading_format.replace('XXX', '10'),
    'h10': heading_format.replace('XXX', '11'),
}

# 链接
bt_magnet = '/html/body/div/div/div[2]/div[4]/div[2]/a'

# 搜索关键字
def search_keyword_to_detail():
    browser_type = 'special'
    bb = BB(url=index_url, browser_type=browser_type, ignore_errors=True, after_sleep_time=2) # default min backgroup
    # 1111 吴签磁力 - 最懂你的磁力链接搜索引擎 <class 'str'> 5858    多次handshake failed; returned -1, SSL error code 1, net_error -100
    # 1111 吴签磁力 - 最懂你的磁力链接搜索引擎 <class 'str'> 5858    上面的【handshake failed ...】会忽略
    # 1111  <class 'str'> 290  20秒依旧不能显示出内容

    # 1111 吴签磁力 - 最懂你的磁力链接搜索引擎 <class 'str'> 4185

    # 1111 百度一下，你就知道 <class 'str'> 452603

    print(1111, bb.get_title(), type(bb.get_title()), len(bb.get_driver().page_source)) 

    # sys.exit(0)
    # bb.get_current_page(before_sleep_time=2, after_sleep_time=1)
    bb.input_key(key_id=key_id, key_name=key_name,key_word=key_word, before_sleep_time=1, after_sleep_time=1)
    # bb.input_key(key_id='kw', key_name=None,key_word=key_word, before_sleep_time=1, after_sleep_time=1)
    print(2222, bb.get_title())
    bb.switch_newest_page(after_sleep_time=1)
    print(33333, bb.get_title())
    bb.get_current_page()
    # bb.get_current_page(before_sleep_time=2, after_sleep_time=1)
    bb.click_xpath(xpath=heading['h1'], before_sleep_time=1, after_sleep_time=1)
    print(44444, bb.get_title())
    bb.switch_newest_page(before_sleep_time=2, after_sleep_time=1)
    print(55555, bb.get_title())
    # bb.get_current_page(before_sleep_time=2, after_sleep_time=1)
    # bb.click_xpath(xpath=bt_magnet, before_sleep_time=2, after_sleep_time=180)
    magnet = bb.get_xpath_text(xpath=bt_magnet, before_sleep_time=1, after_sleep_time=180)
    write_log(magnet)
    bb.close()


if __name__ == '__main__':
    # access_url(url='https://wuqianmx.top/detail/99315/tPwIVeKJmtAw6pbMe04T0XCfrh8')
    # write_log("Program execution complete!")
    # sys.exit(0)
    search_keyword_to_detail()







