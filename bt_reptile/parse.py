import bencodepy
import os
TORRENT_SAVE_PATH = "torrents"

def build_tree(dictionary, parent_key=''):
    tree = []
    for key, value in dictionary.items():
        new_key = f"{parent_key}.{key}" if parent_key else key
        if isinstance(value, dict):
            tree.extend(build_tree(value, new_key))
        else:
            tree.append(new_key)
    # print("tree: {}".format(tree))
    return tree


def parse_torrent_file(torrent_file_path):
    with open(torrent_file_path, 'rb') as file:
        metadata = bencodepy.decode(file.read())
    return metadata


def parse_torrent(bt_path):
    for _, _, files in os.walk(bt_path):
        for file in files:
            data = parse_torrent_file(os.path.join(bt_path, file))
            info = data[b'info']
            tree = build_tree(info)
            print(5555, tree, type(tree))
            info_list = list()
            # 文件列表
            print(1111, type(info[b'files']),len(info[b'files']))

            # 其中1个
            record = info[b'files'][0]
            # print(1122, type(info[b'files'][0]),len(info[b'files'][0]), info[b'files'][0])
            print(1122, type(record), len(record))

            record_length = record[b'length']
            record_paths = record[b'path']

            print(1123, record_length, type(record_length), (record_length))
            print(1124, record_paths, type(record_paths), len(record_paths))

            for record_path in record_paths:
                print(1125, record_path.decode('utf-8'))


            # 标题
            print(2222, type(info[b'name']),len(info[b'name']), info[b'name'], info[b'name'].decode('utf-8'))



parse_torrent(TORRENT_SAVE_PATH)


