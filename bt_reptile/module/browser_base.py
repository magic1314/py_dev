from selenium import webdriver
import time
import sys
import os
from selenium.webdriver.common.by import By
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import TimeoutException
from bs4 import BeautifulSoup


# 公共库
from public import write_log
from public import get_parameter


class BB(object):
    def __init__(self, url='https://www.baidu.com', browser_type='default', after_sleep_time = 0, ignore_errors = True):
        write_log(f"Now, Open Brower & Start access url: 【{url}】,browser_type=【{browser_type}】")
        # 配置选项
        chrome_options = Options()

        if ignore_errors:
            # 忽略证书错误
            chrome_options.add_argument('--ignore-certificate-errors')
            # 忽略 Bluetooth: bluetooth_adapter_winrt.cc:1075 Getting Default Adapter failed. 错误
            chrome_options.add_experimental_option('excludeSwitches', ['enable-automation'])
            # 忽略 DevTools listening on ws://127.0.0.1... 提示
            chrome_options.add_experimental_option('excludeSwitches', ['enable-logging'])

        self.browser_type = browser_type
        if browser_type == 'default':
            # 初始化webdriver，这里以Chrome为例
            self.driver = webdriver.Chrome(options=chrome_options)
        elif self.browser_type == 'min':
            # 初始化webdriver，这里以Chrome为例
            self.driver = webdriver.Chrome(options=chrome_options)
            # 将浏览器窗口最小化
            self.driver.minimize_window()
        elif browser_type == 'backgroup':
            chrome_options.add_argument('--headless')
            chrome_options.add_argument('--disable-gpu')  # 如果使用的是Windows系统，这行代码可以帮助解决一些问题
            self.driver = webdriver.Chrome(options=chrome_options)
        elif browser_type == 'special':
            chrome_options.add_argument('--window-position=-100,-100')
            chrome_options.add_argument('window-size=100x100')  # 设置窗口大小为 1920x1080
            self.driver = webdriver.Chrome(options=chrome_options)
        else:
            write_log("无效参数！", tip='ERROR')

        # 设置页面加载超时时间（单位：秒）
        self.driver.set_page_load_timeout(30)

        # 打开网页
        self.driver.get(url)

        # 增加隐式等待时间
        self.driver.implicitly_wait(10)  # seconds


        if after_sleep_time: time.sleep(after_sleep_time)

    def min(self):
        # 将浏览器窗口最小化
        self.driver.minimize_window()


    # 在输入框内输入关键字
    def input_key(self, key_id=None, key_name=None, key_word=None, before_sleep_time=0, after_sleep_time = 0):
        if before_sleep_time: time.sleep(before_sleep_time)
        # try:
        if True:
            write_log(f"<<DEF input_key>> key_id=【{key_id}】,key_name=【{key_name}】,key_word=【{key_word}】")

            if key_id:
                # 找到搜索框元素
                search_box = self.driver.find_element(By.ID, key_id)
            elif key_name:
                # 找到搜索框元素
                search_box = self.driver.find_element(By.NAME, key_name)
            
            # 输入搜索关键词
            search_box.send_keys(key_word)

            # 提交搜索（点击搜索按钮或按回车键）
            search_box.submit()  # 或使用 search_box.send_keys(Keys.RETURN)


            if self.browser_type == 'min': self.min()

            # 等待页面加载完成，这里简单等待5秒，实际情况可能需要更复杂的等待条件
        
        if after_sleep_time: time.sleep(after_sleep_time)
        # except Exception as e:
        #     write_log(f"出现异常：{e}")
        #     self.driver.close()
        #     write_log("the Browser quit!")
    
    # 获得当前网页的内容
    def get_current_page(self, is_save_file=True, encoding='utf-8', before_sleep_time=0, after_sleep_time = 0):
        if before_sleep_time: time.sleep(before_sleep_time)
        
        write_log(f"<<DEF get_current_page>> is_save_file=【{True}】,encoding=【{encoding}】")
        # try:
        if True:
            # 获取并打印页面源代码
            page_source = self.driver.page_source
            # print(page_source)

            # 使用BeautifulSoup解析并美化
            soup = BeautifulSoup(page_source, 'html.parser')
            formatted_html = soup.prettify()

            # 定义保存响应内容的文件名
            # script_name = sys.argv[0].split('.')[0]
            title_name = self.driver.title
            download_file_name = os.path.abspath(os.path.join('download', title_name + '_' + get_parameter('YYYYMMDD') + '.html'))

            if is_save_file:
                # 都将响应内容保存到文件
                with open(download_file_name, 'w', encoding=encoding) as file:
                    file.write(formatted_html)
                    write_log(f"A html file named 【{os.path.abspath(download_file_name)}】 has been written")
        
        if after_sleep_time: time.sleep(after_sleep_time)
        # except Exception as e:
        #     write_log(f"出现异常：{e}")
        #     self.driver.close()
        #     write_log("the Browser quit!")

    # 获得网页标题
    def get_title(self):
        return self.driver.title

    # 获得当前driver
    def get_driver(self):
        return self.driver

    # 点击通过xpath
    def click_xpath(self, xpath=None, before_sleep_time=0, after_sleep_time = 0):
        if before_sleep_time: time.sleep(before_sleep_time)
        
        write_log(f"<<DEF click_xpath>> xpath=【{xpath}】")
        # 使用XPath定位元素
        element_to_click = self.driver.find_element(by=By.XPATH, value=xpath)

        # 对定位到的元素执行点击操作
        element_to_click.click()

        if self.browser_type == 'min': self.min()
        if after_sleep_time: time.sleep(after_sleep_time)

    # 获得xpath定位的文字
    def get_xpath_text(self, xpath=None, before_sleep_time=0, after_sleep_time = 0):
        if before_sleep_time: time.sleep(before_sleep_time)
        
        write_log(f"<<DEF get_xpath_text>> xpath=【{xpath}】")
        # 使用XPath定位元素
        element = self.driver.find_element(by=By.XPATH, value=xpath)

        # 对定位到的元素执行点击操作
        return element.text

    # 切换到最新的页面
    def switch_newest_page(self, before_sleep_time=0, after_sleep_time = 0):
        if before_sleep_time: time.sleep(before_sleep_time)
        
        write_log(f"<<DEF switch_newest_page>>")
        # 获取所有窗口句柄
        handles = self.driver.window_handles

        # 切换到最新的窗口
        self.driver.switch_to.window(handles[-1])
        if self.browser_type == 'min': self.min()
        if after_sleep_time: time.sleep(after_sleep_time)

    # 关闭浏览器
    def close(self):
        write_log(f"<<DEF close>>")
        self.driver.quit()
        write_log("the Browser quit!")


if __name__ == '__main__':
    bb = BB(url='https://www.baidu.com', browser_type='backgroup')
    bb.input_key(key_id='kw', key_word='Python')
    bb.switch_newest_page(after_sleep_time=3)
    bb.get_current_page(is_save_file=True)