

def click_xpath(driver, xpath=None):
    # 使用XPath定位元素
    element_to_click = driver.find_element(By.XPATH, '/html/body/div/div[2]/div[2]/div[2]/div[1]/h3/a')

    # 对定位到的元素执行点击操作
    element_to_click.click()

    return driver