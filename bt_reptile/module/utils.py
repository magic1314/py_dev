import requests
import os
import sys
from public import write_log
from public import get_parameter
from bs4 import BeautifulSoup


# 定义保存响应内容的文件名
script_name = sys.argv[0].split('.')[0]
download_file_name = os.path.join('download', script_name + '_' + get_parameter('YYYYMMDD') + '.html')

# 登录index url
def access_url(url='https://www.baidu.com/', is_save_file=True, encoding='utf-8'):
    # 发送HTTP请求
    write_log(f"Now, Start access url: 【{url}】")
    response = requests.get(url)

    # 确保网页请求成功
    if response.status_code == 200:
        # 使用BeautifulSoup解析网页内容
        soup = BeautifulSoup(response.text, 'html.parser')
        formatted_html = soup.prettify()

        if is_save_file:
            # 都将响应内容保存到文件
            with open(download_file_name, 'w', encoding=encoding) as file:
                file.write(formatted_html)
                write_log(f"【{os.path.abspath(download_file_name)}】html文件已写入")
    else:
        write_log(f"Request failed, status code: {response.status_code}")