def build_tree(dictionary, parent_key=''):
    tree = []
    for key, value in dictionary.items():
        new_key = f"{parent_key}.{key}" if parent_key else key
        if isinstance(value, dict):
            tree.extend(build_tree(value, new_key))
        else:
            tree.append(new_key)
    return tree

# 示例字典
example_dict = {
    "level1": {
        "level2a": {
            "level3a": "value1",
            "level3b": "value2"
        },
        "level2b": "value3"
    },
    "level1b": "value4"
}

# 构建树状结构
tree_structure = build_tree(example_dict)
print(tree_structure)