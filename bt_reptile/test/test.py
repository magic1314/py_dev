def my_decorator(func):
    def wrapper():
        print("Something is happening before the function is called.")
        try:
            func()
        except Exception as e:
            print(f"出现异常：{e}")
        print("Something is happening after the function is called.")
    return wrapper

@my_decorator
def say_hello():
    print(0 / 0)

# 调用say_hello时，实际上会调用经过修饰后的版本
say_hello()