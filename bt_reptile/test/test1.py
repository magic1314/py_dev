import libtorrent as lt


# 创建一个session对象
ses = lt.session()


# MAGNET链接
magnet_uri = "magnet:?xt=urn:btih:0DE184C9FDA613315B1FA5D7EBCC364168F9C254"

# 解析MAGNET链接
params = lt.parse_magnet_uri(magnet_uri)

# 创建一个torrent_info对象
ti = lt.torrent_info(params)

# 获取torrent信息
torrent_name = ti.name()
total_size = ti.total_size()

# 打印信息
print("Torrent Name:", torrent_name)
print("Total Size:", total_size, "bytes")