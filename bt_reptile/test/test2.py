import requests

url = "http://localhost:8080/"
username = "admin"
password = "admin"

data = {
    "username": username,
    "password": password
}

response = requests.post(url + "login", data=data)

if response.json()["result"] == "Ok.":
    print("Logged in successfully")

    magnet_link = "magnet:?xt=urn:btih:1234567890123456789012345678901234567890"
    data = {
        "urls": magnet_link
    }

    headers = {
        "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
        "Cookie": f"SID={response.cookies['SID']}"
    }

    response = requests.post(url + "command/download", headers=headers, data=data)

    if response.json()["result"] == "Ok.":
        print("Torrent added successfully")
    else:
        print("Failed to add torrent")
else:
    print("Failed to login")