import re
import os

current_file_path = os.path.abspath(__file__)
parent_dir_path = os.path.dirname(current_file_path)

def remove_invisible_chars(text):
    pattern = r'\s+'  # 匹配所有不可见字符的正则表达式
    return re.sub(pattern, '', text)

# 示例用法
# string = 'Hello\tWorld\n'  # 包含制表符和换行符的字符串
# clean_string = remove_invisible_chars(string)
# print(clean_string)

param_file_name = os.path.join(parent_dir_path, 'param.txt')
def main(select_key, convert='str'):
    param_dict = dict()
    with open(param_file_name, encoding='utf-8') as f:
        for line in f:
            if '=' in line:
                # line = line.replace('=', ' ')
                # print(remove_invisible_chars(line))
                _line = remove_invisible_chars(line)
                key = _line.split('=')[0]
                value = _line.split('=')[1]
                # print(key, value, type(value))
                # 去掉引号
                param_dict[key] = value.replace('\'','').replace('\"','')
        
    if convert == 'bool':
        if param_dict[select_key] == 'False':
            result = False
        else:
            result = True
    # 赋值为None
    elif convert == 'str' and param_dict[select_key] == 'None':
        result = None
    # 字符串
    elif convert in ('str', 'int', 'float'):
        result = eval(convert + '(param_dict[select_key])')
    else:
        print('[Warn] The type of convert is ERROR!')
        result = None

    return result


if __name__ == '__main__':
    aaa = main('key_id', convert='str')
    print('测试内容：', aaa, type(aaa))
#   print(main('None', 'int'))