#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@Created time：2023年07月04日
@author：xuelian

"""

import os
import openpyxl


class ExcelUtil:

    #获取项目路径
    def get_object_path(self):
        return os.path.abspath(os.path.dirname(__file__)).split("common")[0]

    def read_excel(self):
        #加载excel工作簿
        wb = openpyxl.load_workbook(self.get_object_path()+"data/login_user.xlsx")
        #获得sheet对象
        sheet = wb['login']
        #循环
        all_list = []
        id = ['index','dept','role','username','password','name']
        for rows in range(2,sheet.max_row+1):
            temp_list = []
            for cols in range(1,sheet.max_column+1):
                if cols >=4:
                    temp_list.append(sheet.cell(rows,cols).value)
            all_list.append(temp_list)
        return all_list
