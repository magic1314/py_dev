#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@Created time：2023年07月04日
@author：xuelian

"""

import time
from selenium.webdriver.common.keys import Keys

class BasePage:

    def __init__(self,driver):
        self.driver = driver

    #定义页面
    def visit(self,url):
        """

        :param url:访问地址
        :return:
        """
        self.driver.get(url)

    #定位元素的关键字
    def locator_element(self,loc):
        """

        :param loc:定位元素的关键字
        :return:
        """
        return self.driver.find_element(*loc)

    #定位多个元素的关键字
    def locator_elements(self,loc):
        """

        :param loc: 定位多个元素的关键字
        :return:
        """
        return self.driver.find_elements(*loc)

    #设置值的关键字
    def send_keys(self,loc,value):
        """

        :param loc:
        :param value:
        :return:
        """
        self.locator_element(loc).send_keys(value)

    #单击的关键字
    def click(self,loc):
        """

        :param loc:
        :return:
        """
        time.sleep(1)
        self.locator_element(loc).click()

    #清空文本的值
    def clear(self,loc):
        """

        :param loc:
        :return:
        """
        # self.locator_element(loc).clear()
        self.locator_element(loc).send_keys(Keys.CONTROL,"a")
        self.locator_element(loc).send_keys(Keys.DELETE)

    #获取文本的值
    def get_value(self,loc):
        """

        :param loc:
        :return:
        """
        return self.locator_element(loc).text