#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@Created time：2023年07月04日
@author：xuelian

"""

from selenium import webdriver


class BaseUtil():
    def __init__(self) -> None:
        global driver  # 全局变量，没有的话页面会自动关闭
        # 打开浏览器
        self.driver = webdriver.Chrome()
        # 浏览器页面最大化
        self.driver.maximize_window()
        # 加载网页
        self.driver.get("https://192.168.154.136/index")
    
    # def setup(self) -> None:
    #     global driver  # 全局变量，没有的话页面会自动关闭
    #     # 打开浏览器
    #     self.driver = webdriver.Chrome()
    #     # 浏览器页面最大化
    #     self.driver.maximize_window()
    #     # 加载网页
    #     self.driver.get("https://192.168.154.136/index")

    # def teardown(self) -> None:
    #     time.sleep(3)
    #     self.driver.quit()
