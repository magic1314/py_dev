#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@Created time：2023年07月04日
@author：xuelian

"""

from selenium.webdriver.common.by import By
from base.base_page import BasePage
import time


# 登录
class LoginPage(BasePage):

    # 页面的元素
    senior_loc = (By.XPATH, '//*[@id="details-button"]')  # https协议验证-高级选项
    link_loc = (By.ID, 'proceed-link')  # 前往链接
    username_loc = (By.XPATH, '//*[@id="app"]/div/form/div[1]/div/div/input')  # 用户名
    password_loc = (By.XPATH, '//*[@id="app"]/div/form/div[2]/div/div/input')  # 密码
    submit_loc = (By.CLASS_NAME, 'el-button')  # 登录按钮
    loginname_loc = (By.XPATH,'/html/body/div[2]/div/div[2]/div/div[1]/div/div[4]/div/span')  # 首页的用户名

    # 页面的动作
    def login_assess(self, username, password):
        self.click(LoginPage.senior_loc)
        self.click(LoginPage.link_loc)
        self.send_keys(LoginPage.username_loc,username)
        self.send_keys(LoginPage.password_loc,password)
        self.click(LoginPage.submit_loc)
        time.sleep(5)

    #断言
    def get_except_result(self):
        return self.get_value(LoginPage.loginname_loc)