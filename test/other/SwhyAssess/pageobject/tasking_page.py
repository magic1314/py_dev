#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@Created time：2023年08月09日
@author：xuelian

"""

from selenium.webdriver.common.by import By
from pageobject.login_page import LoginPage
from base.base_page import BasePage
import time

#  首页的待办任务
class TaskingPage(BasePage):

    # 页面的元素
    tasking01_loc = (By.XPATH, '//*[@id="pane-todo"]/ul/li/div[2]')  # 首页待办任务的第一条
    advise_loc = (By.CLASS_NAME, 'el-textarea__inner')  # 输入审批意见
    passtask_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div[2]/div/div/button[1]')  # 确认审批

    # 页面的动作
    # # 审批通过
    def tasking_pass(self, username, password):
        # 登录
        lp = LoginPage(self.driver)
        lp.login_assess(username, password)
        self.click(TaskingPage.tasking01_loc)
        time.sleep(2)
        self.send_keys(TaskingPage.advise_loc, "pass")
        self.click(TaskingPage.passtask_loc)