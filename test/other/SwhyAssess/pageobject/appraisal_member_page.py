#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@Created time：2023年08月08日
@author：xuelian

"""

from selenium.webdriver.common.by import By
from pageobject.login_page import LoginPage
from base.base_page import BasePage
import time


# 单项指标
class AppraisalMemberPage(BasePage):

    # 页面的元素
    appraisal_loc = (By.XPATH, '//*[@id="app"]/div/div[1]/div[3]/div[1]/div/ul/div[3]/li/div/span')  # 考核关系管理
    appraisalmeb_loc = (By.XPATH, '//*[@id="app"]/div/div[1]/div[3]/div[1]/div/ul/div[3]/li/ul/div[1]/a/li/span')  # 考核权查询/维护
    appraisalmodify_loc = (By.XPATH, '//*[contains(text(),"考核权维护")]')  # 考核权维护
    choose01_loc = (By.XPATH, '/html/body/div[4]/div/div[2]/div/form/div[1]/div/div/div[2]/div[1]/div/div[2]/div[1]/div/div/div[1]/div/label/span/span')  # 选择第一个人
    choose02_loc = (By.XPATH, '/html/body/div[4]/div/div[2]/div/form/div[1]/div/div/div[2]/div[1]/div/div[2]/div[1]/div/div/div[2]/div/label/span/span')  # 选择第二个人
    choose03_loc = (By.XPATH, '/html/body/div[4]/div/div[2]/div/form/div[1]/div/div/div[2]/div[1]/div/div[2]/div[1]/div/div/div[3]/div/label/span/span')  # 选择第三个人
    submit_loc = (By.XPATH, '//*[contains(text(),"提交审批")]')  # 提交审批
    taskappraisal_loc = (By.XPATH, '//*[@id="pane-todo"]/ul/li/div[2]')  # 人事部门负责人审批考核权批量变更流程
    advise_loc = (By.CLASS_NAME, 'el-textarea__inner')  # 输入审批意见
    passindex_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div[2]/div/div/button[1]')  # 确认审批

    # 页面的动作
    # # 考核权维护
    def modify_appraisal(self, username, password):
        # 登录
        lp = LoginPage(self.driver)
        lp.login_assess(username,password)
        # 选择菜单
        self.click(AppraisalMemberPage.appraisal_loc)
        self.click(AppraisalMemberPage.appraisalmeb_loc)
        time.sleep(1)
        self.click(AppraisalMemberPage.appraisalmodify_loc)
        self.click(AppraisalMemberPage.choose01_loc )
        self.click(AppraisalMemberPage.choose02_loc )
        self.click(AppraisalMemberPage.choose03_loc )
        self.click(AppraisalMemberPage.submit_loc)

    # # 审批指标
    def task_appraisal(self, username, password):
        # 登录
        lp = LoginPage(self.driver)
        lp.login_assess(username,password)
        self.click(AppraisalMemberPage.taskappraisal_loc)
        time.sleep(2)
        self.send_keys(AppraisalMemberPage.advise_loc, "pass")
        self.click(AppraisalMemberPage.passindex_loc)