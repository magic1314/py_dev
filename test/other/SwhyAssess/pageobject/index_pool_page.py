#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@Created time：2023年08月01日
@author：xuelian

"""

from selenium.webdriver.common.by import By
from pageobject.login_page import LoginPage
from base.base_page import BasePage
import time


# 单项指标
class IndexPoolPage(BasePage):

    # 页面的元素
    index_loc = (By.XPATH, '//*[@id="app"]/div/div[1]/div[3]/div[1]/div/ul/div[2]/li/ul/div[2]/li/div')  # 考核指标库
    indexpool_loc = (By.XPATH, '//*[@id="app"]/div/div[1]/div[3]/div[1]/div/ul/div[2]/li/ul/div[2]/li/ul/div[1]/a/li/span')  # 单项指标
    newsup_loc = (By.XPATH,'//*[@id="app"]/div/div[2]/section/div/div/div[2]/div/div[2]/div[2]/button/span')  # 新增补充指标
    dept_loc = (By.XPATH, '/html/body/div[4]/div/div[2]/form/div[1]/div[1]/div/div/div/div[1]/input')  # 新增选择部门名称输入框
    deptname_loc = (By.CLASS_NAME, 'el-radio__inner')  # 新增选择部门
    businesstype_loc = (By.XPATH, '/html/body/div[4]/div/div[2]/form/div[1]/div[2]/div/div/div/div/input')  # 新增选择指标类别输入框
    choosetype_loc = (By.XPATH, '/html/body/div[6]/div[1]/div[1]/ul/li[1]')  # 新增选择指标类别-营收利润
    source_loc = (By.XPATH, '/html/body/div[4]/div/div[2]/form/div[2]/div[1]/div/div/div/div/input')  # 新增选择指标来源输入框
    handindex_loc = (By.XPATH, '//*[text()="手工录入"]')  # 新增选择指标来源-手动录入
    indexname_loc = (By.XPATH, '/html/body/div[4]/div/div[2]/form/div[2]/div[2]/div/div/div/input')  # 新增输入指标名称
    indexunit_loc = (By.XPATH, '/html/body/div[4]/div/div[2]/form/div[3]/div[1]/div/div/div/div/input')  # 新增选择指标单位输入框
    indexyuan_loc = (By.XPATH, '/html/body/div[8]/div[1]/div[1]/ul/li[2]')  # 新增选择指标单位-元
    supplyfreq_loc = (By.XPATH, '/html/body/div[4]/div/div[2]/form/div[3]/div[2]/div/div/div/div/input')  # 新增选择供数模式输入框
    currentsupply_loc = (By.XPATH, '//*[text()="当期"]')  # 新增选择供数模式-当期
    category_loc = (By.XPATH, '/html/body/div[4]/div/div[2]/form/div[4]/div[1]/div/div/div/div/input')  # 新增选择指标类型输入框
    process_loc = (By.XPATH, '//*[text()="过程性指标"]')  # 新增选择指标类型-过程性指标
    iftotal_loc = (By.XPATH, '/html/body/div[4]/div/div[2]/form/div[4]/div[2]/div/div/div/div/input')  # 新增选择是否属于总额管控输入框
    istotal_loc = (By.XPATH, '/html/body/div[11]/div[1]/div[1]/ul/li[2]')  # 新增选择是否属于总额管控-是
    ruletype_loc = (By.XPATH, '/html/body/div[4]/div/div[2]/form/div[5]/div/div/div/div/div/input')  # 新增选择打分规则输入框
    assessor_loc = (By.XPATH, '//*[text()="按考核人评价打分"]')  # 新增选择按考核人评价打分
    remarks_loc = (By.XPATH, '/html/body/div[4]/div/div[2]/form/div[6]/div/div/div/div/textarea')  # 新增填写备注
    indexsubmit_loc = (By.XPATH, '//*[text()="保存"]')  # 新增点击保存
    modifysup_loc = (By.CLASS_NAME, 'el-icon-edit')  # 修改补充指标
    modifycof_loc = (By.XPATH, '/html/body/div[3]/div/div[3]/button[2]')  # 确认修改
    modifyname_loc = (By.XPATH, '/html/body/div[5]/div/div[2]/form/div[2]/div[2]/div/div/div/input')  # 修改指标名称
    summit_loc = (By.XPATH, '/html/body/div[5]/div/div[3]/div/button')  # 提交审批
    taskindex_loc = (By.XPATH, '//*[@id="pane-todo"]/ul/li/div[2]')  # 营业部部门经理审批单项指标
    advise_loc = (By.CLASS_NAME, 'el-textarea__inner')  # 输入审批意见
    passindex_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div[1]/div[2]/div/div/button[1]')  # 确认审批
    selectindex_loc = (By.XPATH, '//*[@id="app"]/div/div[2]/section/div/div/div[2]/div/div[3]/div[3]/table/tbody/tr[1]/td[8]/div/button[1]/i')  # 点击查看
    deleteindex_loc = (By.CLASS_NAME, 'el-icon-delete')  # 删除指标
    deletedindex_loc = (By.XPATH,'//*[contains(text(),"确定")]')  # 确认删除指标

    # 页面的动作
    # # 新增指标
    def add_index(self, username, password):
        # 登录
        lp = LoginPage(self.driver)
        lp.login_assess(username,password)
        # 选择菜单
        self.click(IndexPoolPage.index_loc)
        self.click(IndexPoolPage.indexpool_loc)
        time.sleep(1)
        # 新增补充指标
        self.click(IndexPoolPage.newsup_loc)
        time.sleep(1)
        self.click(IndexPoolPage.dept_loc)
        self.click(IndexPoolPage.deptname_loc)
        self.click(IndexPoolPage.businesstype_loc)
        self.click(IndexPoolPage.choosetype_loc)
        self.click(IndexPoolPage.source_loc)
        self.click(IndexPoolPage.handindex_loc)
        self.send_keys(IndexPoolPage.indexname_loc, "自动化测试新增指标001")
        self.click(IndexPoolPage.indexunit_loc)
        self.click(IndexPoolPage.indexyuan_loc)
        self.click(IndexPoolPage.supplyfreq_loc)
        self.click(IndexPoolPage.currentsupply_loc)
        self.click(IndexPoolPage.category_loc)
        self.click(IndexPoolPage.process_loc)
        self.click(IndexPoolPage.iftotal_loc)
        self.click(IndexPoolPage.istotal_loc)
        self.click(IndexPoolPage.ruletype_loc)
        self.click(IndexPoolPage.assessor_loc)
        self.send_keys(IndexPoolPage.remarks_loc, "自动化生成的备注备注备注")
        self.click(IndexPoolPage.indexsubmit_loc)

    # # 修改指标
    def modify_index(self, username, password):
        # 登录
        lp = LoginPage(self.driver)
        lp.login_assess(username,password)
        # 选择菜单
        self.click(IndexPoolPage.index_loc)
        self.click(IndexPoolPage.indexpool_loc)
        time.sleep(1)
        # 修改补充指标
        self.click(IndexPoolPage.modifysup_loc)
        self.click(IndexPoolPage.modifycof_loc)
        time.sleep(2)
        self.clear(IndexPoolPage.modifyname_loc)
        self.send_keys(IndexPoolPage.modifyname_loc, "自动化测试新增指标002")
        self.click(IndexPoolPage.summit_loc)

    # # 审批指标
    def task_index(self, username, password):
        # 登录
        lp = LoginPage(self.driver)
        lp.login_assess(username,password)
        self.click(IndexPoolPage.taskindex_loc)
        time.sleep(2)
        self.send_keys(IndexPoolPage.advise_loc, "pass")
        self.click(IndexPoolPage.passindex_loc)

    # # 查询指标
    def select_index(self, username, password):
        # 登录
        lp = LoginPage(self.driver)
        lp.login_assess(username,password)
        # 选择菜单
        self.click(IndexPoolPage.index_loc)
        self.click(IndexPoolPage.indexpool_loc)
        time.sleep(1)
        self.click(IndexPoolPage.selectindex_loc)
        # 断言
        # assert lp.get_except_result() == expected_output

    # # 删除指标
    def delete_index(self, username, password):
        # 登录
        lp = LoginPage(self.driver)
        lp.login_assess(username,password)
        # 选择菜单
        self.click(IndexPoolPage.index_loc)
        self.click(IndexPoolPage.indexpool_loc)
        time.sleep(1)
        self.click(IndexPoolPage.deleteindex_loc)
        self.click(IndexPoolPage.deletedindex_loc)



    # #断言
    # def get_except_result(self):
    #     return self.get_value(IndexPoolPage.loginname_loc)


