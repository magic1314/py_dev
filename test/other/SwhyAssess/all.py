#!/usr/bin/env python3.8.5
# -*- coding:utf-8 -*-
"""
@Created time：2023年06月13日
@author：huanzi

"""
import pytest
from HTMLTestRunner import HTMLTestRunner


if __name__ == '__main__':
    #执行需要的用例，并且生成HTML格式的自动化的测试报告
    #使用pytest默认的测试用例加载器去发现testcase目录下以py结尾的所有的测试用例
    suite = pytest.defaultTestLoader.discover("testcase2", "*.py")
    #生成html报告文件
    report_file = open("./report/reports.html","wb")
    #生成一个HTMLTestRunner运行器对象(必须下载一个文件HTMLTestRunner.py，放在python的lib目录）
    runner = HTMLTestRunner(stream=report_file, title="DecisionEngine自动化测试报告", description="报告详情如下：")
    #通过运行器运行测试用例
    # runner.run(suite)