#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@Created time：2023年08月08日
@author：xuelian

没有使用到
"""

from base.base_util import BaseUtil
from pageobject.appraisal_member_page import AppraisalMemberPage
from pageobject.tasking_page import TaskingPage

global driver


class TestAppraisalMember (BaseUtil):

    def test_0301_appraisal(self):
        """ 考核权维护 """
        ip = AppraisalMemberPage(self.driver)
        ip.modify_appraisal(username="004526", password="Swhy1234")


    def test_0302_task_appraisal(self,username,password):
        """ 审批考核权 """
        username = "004812"
        username2 = "004526"
        password = "Swhy1234"
        tp = TaskingPage(self.driver)
        tp.tasking_pass(username,password)





    def test_0203_task_index(self):
        """ 审批补充指标 """
        ip = IndexPoolPage(self.driver)
        ip.task_index(username="004812",password="Swhy1234")

    def test_020301_task_index(self):
        """ 审批补充指标 """
        ip = IndexPoolPage(self.driver)
        ip.task_index(username="101053",password="Swhy1234")

    def test_0204_select_index(self):
        """ 查询补充指标 """
        ip = IndexPoolPage(self.driver)
        ip.select_index(username="004526",password="Swhy1234")
        # 断言
        # assert lp.get_except_result() == expected_output

    def test_0205_delete_index(self):
        """ 删除补充指标 """
        ip = IndexPoolPage(self.driver)
        ip.delete_index(username="004526",password="Swhy1234")
        # 断言
        # assert lp.get_except_result() == expected_output


