#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@Created time：2023年07月14日
@author：xuelian


还没有好

"""

from base.base_util import BaseUtil
from pageobject.index_pool_page import IndexPoolPage
global driver


class TestIndexPool (BaseUtil):

    def test_0201_add_index(self):
        """ 新增补充指标 """
        ip = IndexPoolPage(self.driver)
        ip.add_index(username="004526", password="Swhy1234")

    def test_0202_modify_index(self):
        """ 修改补充指标 """
        ip = IndexPoolPage(self.driver)
        ip.modify_index(username="004526",password="Swhy1234")

    def test_0203_task_index(self):
        """ 审批补充指标 """
        ip = IndexPoolPage(self.driver)
        ip.task_index(username="004812",password="Swhy1234")

    def test_020301_task_index(self):
        """ 审批补充指标 """
        ip = IndexPoolPage(self.driver)
        ip.task_index(username="101053",password="Swhy1234")

    def test_task_index(self, username):
        """ 审批补充指标 """
        ip = IndexPoolPage(self.driver)
        ip.task_index(username=username, password="Swhy1234")


    def test_0204_select_index(self):
        """ 查询补充指标 """
        ip = IndexPoolPage(self.driver)
        ip.select_index(username="004526",password="Swhy1234")
        # 断言
        # assert lp.get_except_result() == expected_output

    def test_0205_delete_index(self):
        """ 删除补充指标 """
        ip = IndexPoolPage(self.driver)
        ip.delete_index(username="004526",password="Swhy1234")
        # 断言
        # assert lp.get_except_result() == expected_output


if __name__ == '__main__':
    aa = TestIndexPool()
    aa.test_0201_add_index()
