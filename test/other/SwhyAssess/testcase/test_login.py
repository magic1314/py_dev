#!/usr/bin/env python
# -*- coding:utf-8 -*-
"""
@Created time：2023年06月13日
@author：xuelian

"""

import pytest
from base.base_util import BaseUtil
from common.excel_util import ExcelUtil
from pageobject.login_page import LoginPage
global driver


class TestLogin (BaseUtil):

    @pytest.mark.parametrize("username,password,expected_output", [*ExcelUtil().read_excel()])
    def test_01_login(self,username,password,expected_output):
        print(username, password, expected_output)
        """ 登录 """
        lp = LoginPage(self.driver)
        lp.login_assess(username,password)
        # 断言
        assert lp.get_except_result() == expected_output




    # def test_01_login(self,*username,password):
    #     # print(username, password)
    #     """ 登录 """
    #     lp = LoginPage(self.driver)
    #     lp.login_assess(username="004812",password="Swhy1234";username="101053",password="Swhy1234")