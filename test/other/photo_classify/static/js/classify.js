function judge_number()
{
      var json_data = JSON.parse('{{ json_data|safe }}');
      console.log(json_data);
      // window.alert(json_data)
      var current_image = document.getElementById('current_image');
      // current_image.src = json_data.p1;
      current_image.src = '../static/images/IMG_1580.JPG'
      current_image.alt = "发了房间埃弗拉发"
      console.log("图片：" + current_image.src);
};



// 切换图片
var seq = 1;
function change_image(up_down){
    console.log("触发了change_image btn_submit函数！");

    // 在 JavaScript 中，获取 JSON 数据
    // var init_data = JSON.parse(document.getElementById("init_data").innerHTML);
    var init_data, init_data_list;
    
    if (seq == 1) {
        var init_data = document.getElementById("init_data").innerHTML;
        var init_data_list = init_data.split(',')
        window.current_id = Number(init_data_list[1].replace(' ',''));
        console.log(window.current_id)
    }


    var current_photo_src = document.getElementById("current_image")
  
    var chk1 = document.getElementById("option1").checked;
    var chk2 = document.getElementById("option2").checked;
    var chk3 = document.getElementById("option3").checked;

    request_data = ({
    "current_image": [window.current_id, current_photo_src.src]
    ,"up_down": up_down
    ,"my_choice": {
                    "checkbox1":  chk1,
                    "checkbox2":  chk2,
                    "checkbox3":  chk3,
                }
    });

      
    $.ajax({
        type: 'POST',
        url: '/change_image',
        contentType: 'application/json',
        data: JSON.stringify(request_data),
        success: function(response) {
            console.log("classify.js接受到app.py切换图片的指示！");
            if (response == null){
                console.log("返回内容为null！ 不处理")
            }else{
                console.log(response);
                window.current_id = response[0];
                current_photo_src.src = response[1];
            }

        },
        error: function(xhr, status, error) {
        console.log('Ajax请求失败：' + error);
        } 
    });

    seq = seq + 1;

  }