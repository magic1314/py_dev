$(function() {
    var audio = $('#audio')[0];
    var playlist = $('#playlist');
    var playPauseBtn = $('#play-pause-btn');
    var stopBtn = $('#stop-btn');
    var volumeRange = $('#volume-range');
    var progressBar = $('#progress-bar .progress-bar');

    var currentSong = null;
    var isPlaying = false;
    var isStop = false;
    var isMuted = false;

    function playSong(song) {
        if (currentSong !== song) {
            audio.src = song.data('src');
            audio.load();
            currentSong = song;
            $('#song-title').text(song.data('title'));
            $('#song-artist').text(song.data('artist'));
        }
        audio.play();
        isPlaying = true;
        isStop = false;
        playPauseBtn.html('<i class="fas fa-pause"></i>');
    }

    function pauseSong() {
        audio.pause();
        isPlaying = false;
        playPauseBtn.html('<i class="fas fa-play"></i>');
    }

    function stopSong() {
        audio.pause();
        audio.currentTime = 0;
        isPlaying = false;
        isStop = true;
        playPauseBtn.html('<i class="fas fa-play"></i>');
    }

    function setVolume(volume) {
        audio.volume = volume;
        volumeRange.val(volume);
    }

    function setProgress(progress) {
        progressBar.css('width', progress + '%');
        progressBar.attr('aria-valuenow', progress);
    }

    function updateProgress() {
        if (!isStop) {
            var progress = audio.currentTime / audio.duration * 100;
            setProgress(progress);
        }
    }

    audio.addEventListener('ended', function() {
        setProgress(0);
        isPlaying = false;
        playPauseBtn.html('<i class="fas fa-play"></i>');
    });

    playPauseBtn.click(function() {
        if (isPlaying) {
            pauseSong();
        } else {
            playSong(currentSong);
        }
    });

    stopBtn.click(function() {
        stopSong();
    });

    volumeRange.on('input', function() {
        var volume = $(this).val();
        setVolume(volume);
    });

    playlist.find('li').click(function() {
        var song = $(this);
        playSong(song);
    });

    setInterval(updateProgress, 1000);
});