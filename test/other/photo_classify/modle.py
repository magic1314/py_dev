# encoding: utf-8
import os
import sys
import json
import shutil
import public

# 删除文件
def del_file(file):
    if os.path.exists(file):
        os.remove(file)



# 复制文件
def move_file_to_dir(file, dir):
    sep = os.path.sep
    print(666, file, dir)

    # sys.exit(0)

    file_name = file
    target_dir = dir
    # if not os.path.isdir(target_dir):
    #     os.makedirs(target_dir)

    if os.path.exists(file_name) and os.path.exists(target_dir):
        shutil.copy(file_name, target_dir)
        print('{}已移动到{}'.format(file_name, target_dir))
        del_file(file_name)
    else:
        print('[INFO] {}文件或{}目录不存在'.format(file_name, target_dir))



# 读取json
def read_file_json(file):
    with open(file, 'r') as f:
        content = f.readlines()
        # print("类型", type(content))
        # print(content)

    photo_dict = dict()
    for line in content:
        if line:
            (seq,catalog,filename) = line.split("\t")
            photo_dict[seq] = './static/images/init/' + filename.strip('\n')
    
    json_data = json.dumps(photo_dict)
    print("返回字典，有{}个值！".format(len(photo_dict)))
    # print(json_data)
        
    return json_data

def get_all_photos(dir_path, is_cover=False):
    # 指定目录路径
    # dir_path = '/path/to/your/directory'
    write_file = 'data/photo.txt'

    if not is_cover and (os.path.exists(write_file) and os.path.getsize(write_file)):
        print("文件存在则不处理")
    else:
        # 定义图片文件类型
        img_exts = ['.jpg', '.jpeg', '.png', '.gif', '.bmp']

        # 遍历目录下的所有文件
        print(dir_path)
        i = 1
        with open(write_file, 'w', encoding='utf-8') as f:
            for root, dirs, files in os.walk(dir_path):
                for file in files:
                    print(file)
                    # 获取文件名和扩展名
                    file_name, ext = os.path.splitext(file)
                    # 判断文件扩展名是否为图片类型
                    if ext.lower() in img_exts:
                        # 记录文件名到 photo.txt 文件
                        f.write("p{}\t{}\t{}\n".format(i, dir_path, file))
                        i = i + 1
        # print("已写入！")

    # 将文件内容写入到json中
    return read_file_json(write_file)

# 仅首次使用
# 获得图片的信息到数据库中
def get_photos_info(dir_path,base_dir_path, is_cover=False):

    sql = """select count(1) cnt from t_photo_storage_info"""
    cnt = public.run_sql(sql,dbFile='photo.txt',needResult='oneValue',executemany=False,parameter_list=None,group_concat_max_len=0,display=False)
    # 存在记录，并且【不覆盖】，数据就不再生成
    if cnt and not is_cover:
        print("[INFO] 数据已存在，不生成！")
        return None
    elif is_cover:
        print("[WARN] 开启数据覆盖模式，清理数据！")
        sql = """truncate table t_photo_storage_info"""
        public.run_sql(sql,dbFile='photo.txt',needResult=False,executemany=False,parameter_list=None,group_concat_max_len=0,display=False)
    else:
        print("[INFO] 数据初始化")


    # 定义图片文件类型
    img_exts = ['.jpg', '.jpeg', '.png', '.gif', '.bmp']

    # 遍历目录下的所有文件
    # print(dir_path)
    sql = """delete from t_photo_storage_info"""
    public.run_sql(sql,dbFile='photo.txt',needResult=False,executemany=False,parameter_list=None,group_concat_max_len=0,display=False)
    seq = 1
    base_dir_path = base_dir_path.replace('\\','\\\\')
    for root, dirs, files in os.walk(dir_path):
        for file in files:
            # 获取文件名和扩展名
            file_name, ext = os.path.splitext(file)
            # 判断文件扩展名是否为图片类型
            if ext.lower() in img_exts:
                # 记录文件名到 photo.txt 文件
                # print("p{}\t{}\t{}\n".format(i, dir_path, file))
                parent_dir = os.path.basename(os.path.normpath(dir_path))
                abs_catalog = dir_path.replace('\\','\\\\')
                rel_catalog = abs_catalog.replace(base_dir_path, '').replace('\\\\','/')
                # print(seq, abs_catalog, base_dir_path, rel_catalog)
                sql = """insert into t_photo_storage_info(seq,photo_name,parent_dir,abs_catalog,rel_catalog,status,remark)
                values({},'{}','{}','{}','{}',0,'初始化数据')
                """.format(seq,file,parent_dir,abs_catalog,rel_catalog)


                public.run_sql(sql,dbFile='photo.txt',needResult=False,executemany=False,parameter_list=None,group_concat_max_len=0,display=False)
                seq = seq + 1

    # 更新previous_id和next_id值
    sql = """
        drop table if exists tmp_max_min_id;
        create TEMPORARY table tmp_max_min_id as
        select min(id) min_id,max(id) max_id from t_photo_storage_info
        ;

        
        update t_photo_storage_info a,tmp_max_min_id b
        set  previous_id = (case when a.id = b.min_id then a.id else a.id -1 end)
        ,next_id = (case when a.id = b.max_id then a.id else a.id +1 end)
        ;
        
        -- 默认为最小的id
        insert into t_photo_current_id(current_id)
        select min_id from tmp_max_min_id;
    """
    public.run_sql(sql,dbFile='photo.txt',needResult=False,executemany=False,parameter_list=None,group_concat_max_len=0,display=False)


# 仅首次使用
def get_current_photo():
    sql = """
    select 
        concat(b.rel_catalog,'/',b.photo_name) photo_path
        ,a.current_id
        ,b.previous_id
        ,b.next_id
    from t_photo_current_id a 
    inner join t_photo_storage_info b
    on a.current_id = b.id 
    where a.status = 0
    and b.status = 0
    """
    result = public.run_sql(sql,dbFile='photo.txt',needResult=True,executemany=False,parameter_list=None,group_concat_max_len=0,display=False)
    print("get_current_photo查询结果：", result)
    new_result = list()
    if result:
        for e in result[0]:
            new_result.append(e)
        return new_result
    else:
        return None


"""
    输入：
        current_id: 当前图片id,
        up_or_down: 1:上一张；2:下一张
        choice_type: 选择的内容
                    1: 选了checkBox1
                    2: 选了checkBox2
                    3: 选了checkBox3
                    4: 什么也没有选择
    操作：
         刷新当前值
         移动图片到choice_type对应的目录下
         返回切换后的当前图片id，图片路径
"""
def change_photo(current_id, up_or_down, choice):
    print("change_photo函数传入参数：",current_id, up_or_down, choice)
    if up_or_down == 1:
        # 上一张
        sql = """
        select 
            b.id current_id
            ,concat(b.rel_catalog,'/',b.photo_name) photo_path
        from t_photo_storage_info a
        inner join t_photo_storage_info b
        on a.previous_id = b.id
        where a.id = {}
        and a.status = 0
        and b.status = 0
        """.format(current_id)
    elif up_or_down == 2:
        # 下一张
        sql = """
        select 
            b.id current_id
            ,concat(b.rel_catalog,'/',b.photo_name) photo_path
        from t_photo_storage_info a
        inner join t_photo_storage_info b
        on a.next_id = b.id
        where a.id = {}
        and a.status = 0
        and b.status = 0
        """.format(current_id)
    else:
        print("[ERROR] up_or_down={} 值有问题".format(up_or_down))
        return None
    result = public.run_sql(sql,dbFile='photo.txt',needResult=True,executemany=False,parameter_list=None,group_concat_max_len=0,display=False)
    print(result)
    if result:
        # 切换后的图片信息
        new_current_id = result[0][0]
        new_current_photo_path = result[0][1]
    else:
        print("[ERROR] sql={} 查无结果！".format(sql))
        return None
    
    if choice:
        # 移动当前图片
        sql = """
        select concat(abs_catalog,'\\\\','photo_name') file 
        ,replace(abs_catalog,'init','{}') dir
        from t_photo_storage_info
        where id = {}
        """.format(choice, current_id)
        result = public.run_sql(sql,dbFile='photo.txt',needResult=True,executemany=False,parameter_list=None,group_concat_max_len=0,display=False)
        # move_file_to_dir(result[0][0], result[0][1])

        # 更新【当前id值表】的取值
        sql = """
        update t_photo_storage_info
        set parent_dir = '{0}'
        ,abs_catalog = replace(abs_catalog,'init','{0}')
        ,rel_catalog = replace(rel_catalog,'init','{0}')
        ,status = 1
        where id = {1}
        ;

        -- 跳过失效的id
        update t_photo_storage_info a
        inner join t_photo_storage_info b 
        on a.previous_id = b.id
        and b.status = 1
        set  a.previous_id = b.previous_id
        where a.previous_id = {1}
        ;

        -- 跳过失效的id
        update t_photo_storage_info a
        inner join t_photo_storage_info b 
        on a.next_id = b.id
        and b.status = 1
        set  a.next_id = b.next_id
        where a.next_id = {1}
        ;
        """.format(choice, current_id)
        public.run_sql(sql,dbFile='photo.txt',needResult=False,executemany=False,parameter_list=None,group_concat_max_len=0,display=True)

    # 更新为移动后的图片id
    sql = """update t_photo_current_id
        set current_id = {}  
        where status = 0
        """.format(new_current_id)
    public.run_sql(sql,dbFile='photo.txt',needResult=False,executemany=False,parameter_list=None,group_concat_max_len=0,display=True)



    # 将切换后的信息返回，用于页面切换为下一张、上一张图片
    return [new_current_id, new_current_photo_path]



if __name__ == '__main__':
    # copy_file_to_dir('IMG_1600.JPG', '目录1')
    catalog = r"E:\project\py_dev\photo_classify\static\images\init"
    base_catalog = r"E:\project\py_dev\photo_classify"
    # get_photos_info(catalog, base_catalog, is_cover=False)
    # get_current_photo()
    # change_photo(current_id=2, up_or_down=2, choice = '目录2')
    move_file_to_dir(r"E:\project\py_dev\photo_classify\static\images\init\IMG_1594.JPG", r"E:\project\py_dev\photo_classify\static\images\目录2")

