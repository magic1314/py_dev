from flask import Flask, request, render_template, jsonify
from modle import *
import public



app = Flask(__name__)
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/classify', methods=['GET', 'POST'])
def classify():
    if request.method == 'GET':
        print("-------------- 发送了classify get请求 ---------------------")
        catalog = r"E:\project\py_dev\photo_classify\static\images\init"
        base_catalog = r"E:\project\py_dev\photo_classify"
        get_photos_info(catalog, base_catalog, is_cover=False)
        init_data = get_current_photo()
        return render_template('classify.html', init_data=init_data)

    elif request.method == 'POST':
        print("发送了classify post请求")
        result = ''
        return render_template('classify.html', result=result)  # 将结果写在同一页面的下方


# @app.route('/my_route', methods=['POST'])
# def my_route():
#     print('my_route 获得html的post请求')
#     my_data = request.json.get('my_data')
#     # process data and return response
#     response_data = {'result': 'success', 'data': '[134,151,151]'}
#     return jsonify(response_data)



@app.route('/change_image', methods=['GET', 'POST'])
def change_image():
    if request.method == 'GET':
        print('btn_submit 获得html的get请求')
        return render_template('classify.html')
    
    elif request.method == 'POST':
        print('btn_submit 获得html的post请求')
        my_choice = request.json.get('my_choice')
        current_image = request.json['current_image']
        up_down = request.json['up_down']
        print("my_choice: {}\ncurrent_image: {}\nup_down: {}".format(my_choice, current_image, up_down))

        ch_1 = my_choice['checkbox1']
        ch_2 = my_choice['checkbox2']
        ch_3 = my_choice['checkbox3']

        if ch_1:
            ch = '目录1'
        elif ch_2:
            ch = '目录2'
        elif ch_3:
            ch = '目录3'
        else:
            ch = None

        # 切换上一张，下一张图片
        """
        输入：当前图片id,下一张或下一张,选择的内容
        操作：切换图片
        输出：新当前图片id
        """
        current_id = current_image[0]
        response_data = change_photo(current_id, up_down, ch)


        return jsonify(response_data)



if __name__ == '__main__':
    app.run(port=5001,debug=False, host='0.0.0.0')
    # get_all_photos(catalog, is_cover=False)