-- 创建数据库
CREATE DATABASE if not exists photo CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;


-- 图片存储信息表
drop table if exists t_photo_storage_info;
CREATE TABLE `t_photo_storage_info` (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `seq` int DEFAULT NULL COMMENT '批次的生成序号',
  `photo_name` varchar(50) DEFAULT NULL COMMENT '图片名称',
  `previous_id` int DEFAULT NULL COMMENT '上一张图片的id',
  `next_id` int DEFAULT NULL COMMENT '下一张图片的id',
  `parent_dir` varchar(50) DEFAULT NULL COMMENT '父级的目录名称',
  `abs_catalog` varchar(255) DEFAULT NULL COMMENT '图片绝对路径',
  `rel_catalog` varchar(255) DEFAULT NULL COMMENT '图片相对路径',
  `status` int DEFAULT '0' COMMENT '状态(0：正常，1：失效)',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `config_create_time` datetime DEFAULT CURRENT_TIMESTAMP COMMENT 'etl创建时间',
  `config_update_time` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'etl更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=DYNAMIC COMMENT='图片存储信息表'
;




