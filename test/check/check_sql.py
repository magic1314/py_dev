# encoding: utf-8
import sys
import os
import re
import fnmatch


# 检查规则表
def check_rules(filename, rule):
    if rule == 'double_semicolon':
        with open(filename, 'r', encoding='utf-8') as file:
            # content = file.read().replace('\n','').replace('\r','').replace(' ','')
            content = re.sub('\s+', '', file.read())
            # print(f"内容：【{content}】")
            if ';;' in content:
                print(f"【{filename}】存在双分号")



def find_sql_files(directory):
    check_dict = dict()
    for root, dirs, files in os.walk(directory):
        for _filename in fnmatch.filter(files, '*.sql'):
            file_name = os.path.join(root, _filename)
            check_rules(file_name, rule='double_semicolon')
            # yield file_name

    return check_dict


if len(sys.argv) != 2:
    print("Usage: python check_sql.py [directory_name]")
    sys.exit(1)


dir_name = sys.argv[1]
# print(dir_name)

try:
    abs_dir_name = os.path.abspath(dir_name)
    assert os.path.isdir(abs_dir_name), f"【{abs_dir_name}】目录不存在！"
except BaseException as e:
    print(f"Error: {str(e)}")
    sys.exit(1)

print(f"abs_dir_name=【{abs_dir_name}】")

# 使用示例
for sql_file in find_sql_files(abs_dir_name):
    print(sql_file)




