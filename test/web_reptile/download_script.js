var keyword = "电信下载";

// 找到包含关键字的文本节点
var textNodes = document.evaluate("//text()[contains(., '" + keyword + "')]", document, null, XPathResult.ORDERED_NODE_SNAPSHOT_TYPE, null);

// 循环遍历找到的文本节点并进行点击操作
for (var i = 0; i < textNodes.snapshotLength; i++) {
  var node = textNodes.snapshotItem(i);
  var parentNode = node.parentNode;
  
  // 点击操作，可以根据实际情况修改
  parentNode.click();
}