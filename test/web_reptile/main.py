#encoding: utf-8
import sys
# from module import base
from module import base_simple
from module import page_index
# from module import bs_parse
"""
资源url：https://registry.npmmirror.com/binary.html
浏览器版本：114.0.5735.16/
驱动版本：114.0.5735.16/
"""

if __name__ == '__main__':

    # 用户输入
    browser = base_simple.user_input()
    # print(browser)

    # 点击第一行的下载
    page_index.deal(browser)


    browser.keep()


    browser.close()
    