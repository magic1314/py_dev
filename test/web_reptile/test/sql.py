import public
import asyncio


import mysql.connector



async def my_function(param):
    # 函数的代码，使用参数param
    # 建立数据库连接
    # conn = mysql.connector.connect(
    #     host="192.168.154.241",
    #     user="HRJX",
    #     password="Swhy_qyjx2022$",
    #     database="HRJX",
    #     port="15206"
    # )
    conn = mysql.connector.connect(
        host="127.0.0.1",
        user="root",
        password="123456",
        database="hrjx",
        port="3306"
    )

    # 创建游标
    cursor = conn.cursor()
    # print("已连接数据库！")

    # 执行 SQL 查询
    query = param
    cursor.execute(query)

    # print(query)

    # 获取查询结果
    # result = cursor.fetchall()

    # 处理结果
    # for row in result:
        # 处理每一行数据

    # 关闭游标和连接
    cursor.close()
    conn.close()

    print(query, '执行完成！')



params = (
 'call PRO_YJGL_COMPLETED_VALUE(202302,10000,1)'
,'call PRO_YJGL_COMPLETED_VALUE(202302,21931,1)'
,'call PRO_YJGL_COMPLETED_VALUE(202302,21930,1)'
,'call PRO_YJGL_COMPLETED_VALUE(202302,21689,1)'
,'call PRO_YJGL_COMPLETED_VALUE(202302,21675,1)'
,'call PRO_YJGL_COMPLETED_VALUE(202302,21658,1)'
,'call PRO_YJGL_COMPLETED_VALUE(202302,21653,1)'
,'call PRO_YJGL_COMPLETED_VALUE(202302,21652,1)'
,'call PRO_YJGL_COMPLETED_VALUE(202302,21629,1)'
,'call PRO_YJGL_COMPLETED_VALUE(202302,21628,1)'
)


async def main():
    # 创建任务列表
    tasks = []
    for param in params:
        task = asyncio.create_task(my_function(param))
        tasks.append(task)

    # 并发运行任务
    await asyncio.wait(tasks)

# 创建事件循环并运行主函数
loop = asyncio.get_event_loop()
loop.run_until_complete(main())
loop.close()

# 其他操作