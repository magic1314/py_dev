from selenium import webdriver
from selenium.webdriver.common.by import By
import time

browser = webdriver.Chrome()
# 浏览器页面最大化
# browser.maximize_window()

# 最小化
browser.minimize_window()

# 指定大小
# browser.set_window_size(500,500)  

# 加载网页
# browser.get("https://tools.liumingye.cn/music/#/search/M/song/%E6%82%AC%E6%BA%BA")
browser.get("https://tools.liumingye.cn/music/#/search/M/song/悬溺")

# time.sleep(2)


time.sleep(10)


# # 后退到百度页面
# browser.back()  
# time.sleep(2)

# # 前进的淘宝页面
# browser.forward() 
# time.sleep(2)

'''
# 网页标题
print(browser.title)
# 当前网址
print(browser.current_url)
# 浏览器名称
print(browser.name)
# 网页源码
print(browser.page_source)
'''

# element = browser.find_element(by=By.CLASS_NAME,value='s_ipt')
# kw=browser.find_element(by=By.ID,value='su')

# print(kw)
# print(2222, kw.text, kw.tag_name, kw.id)
# print(kw.location)

print(browser.page_source)
browser.close()