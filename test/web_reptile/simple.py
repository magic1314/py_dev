#encoding: utf-8
import sys
import os
import requests
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.common.exceptions import NoSuchElementException
import urllib.parse
import time


# if not len(sys.argv) >= 1:
#     print("至少有1个参数")
#     sys.exit(0)





# 用户输入
song_name = '曾经的你'
# browser_options = Options()
# browser_options.add_argument("--headless")  # 启用无头模式
# browser = webdriver.Firefox(options=browser_options)
browser = webdriver.Firefox()
url = "https://tools.liumingye.cn/music/#/search/M/song/" + urllib.parse.unquote(song_name)
browser.get(url)
print("浏览器已输入：" + url)

# 等待页面加载完成
wait = WebDriverWait(browser, timeout=10)
wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))
print("主窗口内容已加载完成！")


# # h1 = browser.find_element(by=By.XPATH, value='/html/body/div/div/div/main/div/div/div/div/div[3]/div[2]/div[1]/div/div[last()]')
# h2 = browser.find_element(by=By.XPATH, value='//*[@id="content"]/div/div/div[3]/div[2]/div[1]/div/div[4]/div')
# # print(3333, h1.text)
# print(4444, h2.text)

# browser.quit()

# 第一行点击【...】

# 点击下载的类型
try:
    # total = browser.find_element(by=By.XPATH, value='/html/body/div/div/div/main/div/div/div/div/div[3]/div[2]')
    # print("【歌曲详情】：【{}】".format(total.text))
    line_1 = browser.find_element(by=By.XPATH, value='/html/body/div/div/div/main/div/div/div/div/div[3]/div[2]/div[1]')
    print("第1行【歌曲详情】：【{}】".format(line_1.text.replace('\n','\t')))
    line_2 = browser.find_element(by=By.XPATH, value='/html/body/div/div/div/main/div/div/div/div/div[3]/div[2]/div[2]')
    print("第2行【歌曲详情】：【{}】".format(line_2.text.replace('\n','\t')))
    line_3 = browser.find_element(by=By.XPATH, value='/html/body/div/div/div/main/div/div/div/div/div[3]/div[2]/div[3]')
    print("第3行【歌曲详情】：【{}】".format(line_3.text.replace('\n','\t')))

    hh = browser.find_element(by=By.XPATH, value='/html/body/div/div/div/main/div/div/div/div/div[3]/div[2]/div[1]/div/div[last()]')
    hh.click()
    print("已点击【...】")
except NoSuchElementException:
    print("未找到第一行的【...】")
    sys.exit(1)




# 选择【...】下拉框中【↓下载】
download = browser.find_element(By.XPATH, '//*[contains(text(),"下载")]') 
download.click()
print("已点击【↓下载】")

# 【下载歌曲】弹框
page_download_music = browser.find_element(by=By.CLASS_NAME, value='arco-modal-body')
print("【下载歌曲】弹框文字有：【{}】".format(page_download_music.text))

if not page_download_music.text:
    print("没有文字结束！")
    sys.exit(1)


# 点击下载的类型
try:
    music_type = '标准mp3'
    bz_mp3 = page_download_music.find_element(by=By.LINK_TEXT, value=music_type)
    bz_mp3.click()
    print("已点击【{}】".format(music_type))
except NoSuchElementException:
    print("无法找到链接文本为【{}】的超链接元素".format(music_type))
    sys.exit(1)



# 切换到下载的窗口
browser.switch_to.window(browser.window_handles[1])


# 等待页面加载完成
wait = WebDriverWait(browser, timeout=10)
wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))
print("下载窗口内容已加载完成！")


time.sleep(5)

def get_windows_info():
    info = """
    窗口数：{0}
    窗口句柄：{1}
    当前窗口句柄：{2}
    当前窗口url：{3}
    当前窗口标题:{4}
    """.format(len(browser.window_handles),browser.window_handles,browser.current_window_handle,browser.current_url,browser.title)
    print(info)

get_windows_info()


# 情况1： https://m704.music.12...70f78c42f4250.mp3?_authSecret=0000018b6f8f909313b40aaba02e0515
# 情况2： https://m.lanzouy.com/im7tC0gjfbmj
if 'm704.music' in browser.current_url:
    url = browser.current_url
    response = requests.get(url)

    filename = 'music' + os.path.sep + song_name + '.mp3'
    if os.path.exists(filename):
        os.remove(filename)

    if response.status_code == 200:
        with open(filename, "wb") as file:
            file.write(response.content)
        print("MP3 文件下载成功！")
        browser.close()
        browser.switch_to.window(browser.window_handles[0])
        print("已关闭下载页面")
        browser.execute_script("document.getElementsByClassName('arco-modal')[0].style.display = 'none';")
        print("主窗口关闭下载选项弹框")
    else:
        print("无法下载 MP3 文件。")
        sys.exit(1)
elif 'm.lanzouy.com' in browser.current_url:
    # print("【下载歌曲】弹框文字有：【{}】".format(browser.text))
    get_windows_info()
    # channel_type = '电信下载'
    
    # iframe = browser.find_element(By.CLASS_NAME, 'ifr2')
    # browser.switch_to.frame(iframe)

    # # 等待iframe内的元素可见
    # # wait = WebDriverWait(browser, 10)
    # # element = wait.until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'your_element_selector')))

    # browser.page_source

    # 切换回默认上下文
    # browser.switch_to.default_content()
    
    # dx = browser.find_element(by=By.ID, value='tourl')
    # link = dx.find_element(by=By.LINK_TEXT, value=channel_type)

    with open('E:\\project\\py_dev\\web_reptile\\download_script.js', 'r', encoding='utf-8') as file:
        content = file.read()

    # 点击【电信下载】
    browser.execute_script(content)

else:
    print("未知情况")
    browser.quit()




# print("浏览器准备退出！")
# browser.quit()
# page_download_music.find_element(by=By.TAG_NAME, value='标准mp3')


