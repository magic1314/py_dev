## 功能介绍
抓取网页上的数据

---

## 模块
### 脚本
#### mian.py
    主程序
    引用：moudule/base
#### module/page_index.py
    首页 https://tools.liumingye.cn/music/#/search/M/song/%E8%B6%8A%E8%BF%87%E5%B1%B1%E4%B8%98

#### module/page_index_download.py
    首页-弹出下载页面

#### module/page_download_choice.py
    + 下载的选择(分流)，对应不同类型的页面
        1. 解析出是哪种页面

#### module/page_download_1.py
    下载1

#### module/page_download_2.py
    下载2

#### module/page_download_3.py
    下载3

#### common/base.py
    + 基础
    + 鼠标操作
        1. 操作的方式(单击、双击、右击)
        2. 等待方式（直到加载完成、等待超时）
        3. 超时时长
        4. 返回 selenium.webdriver.remote.webelement.WebElement
    + 元素定位模块(不单独写，在每个模块中)
        1. 通过driver.find_element找到对应的元素()

### 数据库