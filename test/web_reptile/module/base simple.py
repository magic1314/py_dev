#encoding: utf-8
import sys
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import urllib.parse


import time


# 用户输入
def user_input():
    song_name = '平凡之路'
    browser_options = Options()
    browser_options.add_argument("--headless")  # 启用无头模式
    browser = webdriver.Firefox(options=browser_options)
    browser.get("https://tools.liumingye.cn/music/#/search/M/song/" + urllib.parse.unquote(song_name))

    # 等待页面加载完成
    wait = WebDriverWait(browser, timeout=10)
    wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))

    browser.find_element(by=By.XPATH, value='/html/body/div/div/div/main/div/div/div/div/div[3]/div[2]/div[1]/div/div[last()]')









class Browser:
    def __init__(self, url = "",show=0, timeout=10):
        # timeout设置最长等待时间，单位为秒
        print("正在访问：", url)
        browser_options = Options()
        # chrome_options.binary_location = 'D:/application/chrome-win64/chrome.exe'  # 替换为你的 Chrome 可执行文件路径
        if show == 0:
            # 创建Chrome浏览器选项
            
            browser_options.add_argument("--headless")  # 启用无头模式
            print('to start headless window')
            
            self.browser = webdriver.Firefox(options=browser_options)
        elif show == 1:
            self.browser = webdriver.Firefox()
            print('to start minimize window')
            self.browser.minimize_window()
        elif show == 2:
            self.browser = webdriver.Firefox()
            print('to start maximize window')
            self.browser.maximize_window() # 浏览器页面最大化
        else:
            # self.browser.set_window_size(500,500)  
            sys.exit(0)

        # 加载网页
        # browser.get("https://tools.liumingye.cn/music/#/search/M/song/%E6%82%AC%E6%BA%BA")
        if not url:
            print("url为空")
            sys.exit(1)
        try:
            self.browser.get(url)

            # 等待页面加载完成
            wait = WebDriverWait(self.browser, timeout)
            wait.until(EC.presence_of_element_located((By.TAG_NAME, 'body')))


            print(77777777, self.browser)
            return self.browser
        except WebDriverException as err:
            print("error:", err)
            print("Lost connection!")



        self.page_source = self.browser.page_source

    def print_page_source(self):
        print(self.page_source)
    
    # 将源码写入到文件中
    def write_txt_page_source(self, filename):
        # filename = "url.txt"
        with open(filename, "w", encoding="utf-8") as file:
            file.write(self.page_source)

    def keep(self, sleep=30*60):
        time.sleep(sleep)

    def close(self):
        print("关闭页面！")
        self.browser.close()
    

    def parse(self):
        if self.page_source:
            # bs_parse.bs(self.page_source)
            pass
        else:
            print("未获取到内容")
    

    def mouse(self):
        # 找到按钮元素并模拟单击
        # button = self.browser.find_element_by_id('button_id')  # 根据按钮的ID进行定位，可以根据需要修改定位方式
        # button = self.browser.find_elements_by_css_selector('.item.relative.rounded-md.transition-colors')
        button = self.browser.find_element(by=By.CSS_SELECTOR, value='.item.relative.rounded-md.transition-colors')

        print(1111, button)
        print(2222, len(button))
        # button.click()
        # 判断是否至少有两个符合条件的元素
        if len(button) >= 2:
            # 模拟右击第二个符合条件的元素
            target_element = button[1]  # 第二个符合条件的元素
            actions = ActionChains(self.browser)
            actions.context_click(target_element).perform()



if __name__ == '__main__':
    pass