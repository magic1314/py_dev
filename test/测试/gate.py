import numpy as np

# 与门
def and_gate(x1,x2):
    w1 = 0.5
    w2 = 0.5
    theta = -0.8
    tmp = x1 * w1 + x2 * w2 + theta

    print("与门  x1:【{0}】, x2:【{1}】, tmp:【{2}】".format(x1,x2,tmp))
    if tmp >= 0:
        return 1
    else:
        # print(0)
        return 0

# 与非门
def and_not_gate(x1,x2):
    x = np.array([x1, x2])
    w = np.array([-0.5, -0.5])
    b = 0.7
    tmp = np.sum(x * w) + b

    print("与非门  x1:【{0}】, x2:【{1}】, tmp:【{2}】".format(x1,x2,tmp))
    if tmp >= 0:
        # print(1)
        return 1
    else:
        # print(0)
        return 0



# 或门
def or_gate(x1,x2):
    x = np.array([x1, x2])
    w = np.array([0.5, 0.5])
    b = -0.3
    tmp = np.sum(x * w) + b

    print("或门  x1:【{0}】, x2:【{1}】, tmp:【{2}】".format(x1,x2,tmp))
    if tmp >= 0:
        # print(1)
        return 1
    else:
        # print(0)
        return 0


def xor_gate(x1, x2):
    tmp1 = and_not_gate(x1, x2)
    tmp2 = or_gate(x1, x2)
    tmp = and_gate(tmp1, tmp2)

    print("异或门  x1:【{0}】, x2:【{1}】, tmp:【{2}】".format(x1,x2,tmp))
    if tmp >= 0:
        return 1
    else:
        return 0


xor_gate(0, 0)
xor_gate(1, 0)
xor_gate(0, 1)
xor_gate(1, 1)