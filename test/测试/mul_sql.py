import pymysql.cursors
from pymysql.constants import CLIENT
import public
 
# 连接到MySQL服务器
print(6666666, CLIENT.MULTI_STATEMENTS)
# connection = pymysql.connect(host='localhost', user='root', password='123456', db='hrjx',autocommit=True,client_flag=CLIENT.MULTI_STATEMENTS, port=3306)
connection = pymysql.connect(host='127.0.0.1',database='hrjx',user='root',password='123456',charset='utf8mb4',autocommit=True,client_flag=65536,port=3306)
cursor = connection.cursor()
 
# 定义存储过程的名称、参数和逻辑
mul_sql = '''
drop table if exists table1;
CREATE TABLE table1 (id INT, name VARCHAR(255));
INSERT INTO table1 (id, name) VALUES (1, 'John');
INSERT INTO table1 (id, name) VALUES (2, 'Jane');
INSERT INTO table1 (id, name) VALUES (3, 'Tom');
'''
print(mul_sql)

# 执行存储过程的创建语句
cursor.execute(mul_sql)

# 关闭数据库连接
connection.close()

print('执行完成！')