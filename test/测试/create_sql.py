# encoding:utf-8
import public
import sys

public.write_log("...")
print(sys.path)

table_name = 't_dashboard_dept_level'

sql_txt = """
select 
    concat(
         'truncate table {0} $de$'
        ,'insert into {0}('
        ,group_concat(column_name order by ORDINAL_POSITION SEPARATOR ',')
        ,')'
        ,'select','$!'
        ,' '
		,substring( 
			group_concat(concat(',a.', column_name,'     ',column_name,'$!') order by ORDINAL_POSITION SEPARATOR '')
			,2
		)
        ,'from {0} a'
    )   column_list
from information_schema.columns 
where table_name = 't_dashboard_dept_level'
and EXTRA not in ('auto_increment','DEFAULT_GENERATED','DEFAULT_GENERATED on update CURRENT_TIMESTAMP')
""".format(table_name)

print(sql_txt)

column_list = public.run_sql(sql_txt,dbFile='localhost_hrjx.txt',needResult=True,executemany=False,parameter_list=None,group_concat_max_len=0,display=False)
# print(6666666, column_list)

# print(7777777, type(column_list[0]))


result = column_list[0][0].replace('$!','\n').replace('$de$',';\n')
print(result)
