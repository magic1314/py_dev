def remove_unnecessary_brackets(expression):
    new_expression = ''
    bracket_stack = []
    for char in expression:
        if char == '(':
            bracket_stack.append(char)
        elif char == ')':
            # 只有当栈顶元素为'('时，才能弹出并忽略这个')'
            if bracket_stack and bracket_stack[-1] == '(':
                bracket_stack.pop()
            else:
                new_expression += char
        else:
            if not bracket_stack:
                new_expression += char
            else:
                new_expression += char

    # 将剩余未匹配的左括号添加回新表达式
    new_expression += ''.join(bracket_stack)

    return new_expression

expression = '((10 * (9 - 1)) - (7 * 8))'
new_expression = remove_unnecessary_brackets(expression)
print(new_expression)  # 输出：'(10 * (9 - 1)) - 7 * 8'