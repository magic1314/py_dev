'''
Lambda函数是一种匿名函数，它可以在Python中快速定义简单的函数。
Lambda函数的语法形式为lambda arguments: expression，其中arguments是函数的参数，expression是函数的返回值。

'''

# 定义一个接收两个参数并返回它们之和的Lambda函数
add = lambda x, y: x + y

# 调用Lambda函数
result = add(3, 5)
print(result)  # 输出: 8


