import re

# 创建正则表达式模式
# pattern = re.compile(r'\d+')
# pattern = re.compile(r')

# 使用 match 函数
# result = pattern.match('123abc')
# if result:
#     print("Match found:", result.group())
# else:
#     print("No match")

# 使用 search 函数
pattern = re.compile(r'1.*fa')
result = pattern.search('abc12341afab')
if result:
    print("Search found:", result.group())

# # 使用 findall 函数
# result = pattern.findall('123abc456def')
# print("All matches:", result)

# # 使用 sub 函数
# new_string = pattern.sub('number', '123abc456def')
# print("Replaced string:", new_string)