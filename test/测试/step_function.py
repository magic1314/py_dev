import numpy as np
# import matplotlib.pylab as plt
import matplotlib.pyplot as plt
# def step_function(x):
#     if x > 0:
#         return 1
#     else:
#         return 0

# def step_function(x):
#     y = x > 0
#     return y.astype(np.int)


# x = np.array([0, 8, 5])
# y = step_function(x)
# print("结果：", y)


def step_function(x):
    return np.array(x>0, dtype=np.int)


def sigmoid(x):
    return 1 / (1 + np.exp(-x))

x = np.arange(-5, 5, 0.001)
y1 = sigmoid(x)
y2 = step_function(x)
plt.plot(x,y1,label='sigmoid')
plt.plot(x,y2,linestyle = "--",label='step')
# print(x,y)
# print(np.max(x),np.min(x))
# print(np.max(y),np.min(y))
# plt.ylim(-1,2)
plt.show()








