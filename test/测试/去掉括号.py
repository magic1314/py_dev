import re

def remove_unnecessary_parentheses(expression):
    pattern = r"\(([^()]+)\)"
    result = expression

    while True:
        match = re.search(pattern, result)
        if not match:
            break

        inner_expression = match.group(1)
        without_parentheses = remove_unnecessary_parentheses(inner_expression)

        result = result.replace(match.group(), without_parentheses)

    return result

# 原始表达式
expression = "((10 * (9 - 1)) - (7 * 8))"

# 去除不必要括号后的表达式
result = remove_unnecessary_parentheses(expression)

print(result)