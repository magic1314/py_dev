import numpy as np
import matplotlib.pyplot as plt

x = np.arange(0, 6, 0.1)
y1 = np.sin(x)
y2 = np.cos(x)
y3 = np.sin(x*2)


# print(x, y)


plt.plot(x, y1, label='sin')
plt.plot(x, y2, linestyle = "--", label='cos')
plt.plot(x, y3, linestyle = "dotted", label='tan')

plt.xlabel("x")
plt.ylabel("y")
plt.title("Sin & Cos & Tan")
plt.legend()
plt.show()