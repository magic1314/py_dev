
def remove_duplicates(lst):
    # 将每个子列表转换为元组，并放入一个集合中
    unique_set = set(tuple(sublist) for sublist in lst)
    
    # 将唯一的子列表转换回列表形式
    unique_lst = [list(sublist) for sublist in unique_set]
    
    return unique_lst

def generate_combinations(numbers):
    if len(numbers) == 1:
        return [numbers]

    combinations = []
    for i, num in enumerate(numbers):
        remaining_nums = numbers[:i] + numbers[i+1:]
        sub_combinations = generate_combinations(remaining_nums)

        for sub_combination in sub_combinations:
            combinations.append([num] + sub_combination)

    return remove_duplicates(combinations)





# 输入数字列表
numbers = [1, 2, 2]

# 生成所有可能性的列表
possibilities = generate_combinations(numbers)

print(possibilities)
# print(type(possibilities))
# 打印所有可能性的列表
# for possibility in possibilities:
#     print(possibility)