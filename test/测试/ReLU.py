import numpy as np

def relu(x):
    # return np.maximum(0, x)
    new_x = list()
    for i in x:
        if i >= 0:
            new_x.append(i)
        else:
            new_x.append(0)
    # print(new_x)

    return np.array(new_x)


x = np.array([3,3,-1,-2,5])
y = relu(x)
print("结果：", y)