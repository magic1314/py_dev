from public import run_sql

sql_txt = """
drop table if exists table1;
CREATE TABLE table1 (id INT, name VARCHAR(255));
INSERT INTO table1 (id, name) VALUES (1, 'John');
INSERT INTO table1 (id, name) VALUES (2, 'Jane');
INSERT INTO table1 (id, name) VALUES (3, 'Tom');
INSERT INTO table1 (id, name) VALUES (4, 'Tom');
"""
run_sql(sql_txt,dbFile='localhost_hrjx.txt',mul_sql=True,needResult=False,executemany=False,parameter_list=None,group_concat_max_len=0,display=True)

