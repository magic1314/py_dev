import sys
import os
import tensorflow as tf


sys.path.append(os.pardir)

# from dataset.mnist import load_mnist

mnist = tf.keras.datasets.mnist


(x_train, y_train),(x_test, t_test) = mnist(flatten=True, normalize=False)

# 查看数据集的维度
print("训练集图像维度:", x_train.shape)  # (60000, 28, 28)
print("训练集标签维度:", y_train.shape)  # (60000,)
print("测试集图像维度:", x_test.shape)  # (10000, 28, 28)
print("测试集标签维度:", y_test.shape)  # (10000,)