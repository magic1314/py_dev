import hashlib

def calculate_md5(file_path):
    chunk_size = 2 * 1024 * 1024  # 2MB
    with open(file_path, "rb") as file:
        md5_hash = hashlib.md5()
        while chunk := file.read(chunk_size):
            md5_hash.update(chunk)
            if file.tell() >= chunk_size:
                break
    return md5_hash.hexdigest()

# 用法示例
file_path = "man.py"
md5_value = calculate_md5(file_path)
print("MD5:", md5_value)