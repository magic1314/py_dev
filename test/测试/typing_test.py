# encoding:utf-8
import time

def typing_test():
    text = "The quick brown fox jumps over the lazy dog."
    print("请尽快准确地输入以下文本：")
    print(text)
    print("开始！")
    
    start_time = time.time()
    user_input = input()
    end_time = time.time()
    
    elapsed_time = end_time - start_time
    words_per_minute = calculate_words_per_minute(user_input, elapsed_time)
    accuracy = calculate_accuracy(user_input, text)
    
    print("时间：{:.2f}秒".format(elapsed_time))
    print("速度：{:.2f}字/分钟".format(words_per_minute))
    print("准确率：{:.2f}%".format(accuracy * 100))

def calculate_words_per_minute(user_input, elapsed_time):
    words = user_input.split()
    num_words = len(words)
    minutes = elapsed_time / 60
    words_per_minute = num_words / minutes
    return words_per_minute

def calculate_accuracy(user_input, text):
    correct_characters = 0
    total_characters = len(text)
    
    for i in range(min(len(user_input), len(text))):
        if user_input[i] == text[i]:
            correct_characters += 1
    
    accuracy = correct_characters / total_characters
    return accuracy

typing_test()