import numpy as np




def softmax(x):
    c = np.max(x)
    return np.exp(x-c) / np.sum(np.exp(x-c))


a = np.array([0.3, 2.9, 4.0, 5.8, 6.3])
y = softmax(a)
print(y) # [0.01821127 0.24519181 0.73659691]


print(np.sum(y))