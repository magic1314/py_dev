def remove_duplicates(lst):
    # 将每个子列表转换为元组，并放入一个集合中
    unique_set = set(tuple(sublist) for sublist in lst)
    
    # 将唯一的子列表转换回列表形式
    unique_lst = [list(sublist) for sublist in unique_set]
    
    return unique_lst

# 原始的二维列表
original_lst = [[1, 2, 3], [2, 2, 3], [1, 2, 3]]

# 去重后的二维列表
unique_lst = remove_duplicates(original_lst)

# 打印去重后的二维列表
for sublist in unique_lst:
    print(sublist)


# # 原始二维列表
# original_list = [[1, 2, 3], [2, 2, 3], [1, 2, 3]]

# # 将子列表转换为frozenset并放入一个新set中，从而去除重复
# unique_sublists = set(frozenset(sublist) for sublist in original_list)

# # 再将frozenset转换回list形式
# final_list = [list(subset) for subset in unique_sublists]

# print(final_list)