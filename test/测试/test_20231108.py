'''
【迭代器】是一种基于循环的控制结构，通过不断调用 __next__() 方法获取下一个元素，直到遍历完整个数据集合。
迭代器通常使用 for 循环来进行迭代，可以遍历各种可迭代对象，如列表、元组、字典等。


迭代器在Python中有很多用途，下面列举了几个主要的用途：
遍历序列：通过迭代器，你可以逐个访问序列（如列表、元组、字符串等）中的元素，而不需要显式地使用索引。这样可以简化代码，并提高可读性。
节省内存：迭代器允许你逐个处理大型数据集合，而不需要将整个数据集加载到内存中。这对于处理大型文件或数据库结果集等情况非常有用。
延迟计算：迭代器支持惰性计算，即只在需要时才计算和返回下一个元素。这在处理大型或无限序列时非常有用，可以节省计算资源。
无限序列：迭代器可以用于生成无限序列，例如斐波那契数列、无限自然数序列等。由于迭代器一次只生成一个元素，因此可以无限地生成序列而不会耗尽内存。
自定义迭代器：通过实现自定义的迭代器对象，你可以按照自己的需求定义迭代行为和规则。这使得你可以根据特定需求进行灵活地迭代操作。

'''



# 迭代器示例
class MyIterator:
    # 初始化
    def __init__(self, limit):
        self.limit = limit
        self.counter = 0

    # 返回迭代器对象本身
    def __iter__(self):
        return self

    # 返回迭代器中的下一个元素
    def __next__(self):
        if self.counter < self.limit:
            self.counter += 1
            return self.counter
        else:
            raise StopIteration

# 通过迭代器遍历元素
my_iter = MyIterator(5)
# for num in my_iter:
#     print(num)

print(my_iter)

# 输出：
# 1
# 2
# 3

# # 生成器示例
# def my_generator(limit):
#     counter = 0
#     while counter < limit:
#         counter += 1
#         yield counter

# # 通过生成器遍历元素
# gen = my_generator(3)
# for num in gen:
#     print(num)
# 输出：
# 1
# 2
# 3