import numpy as np
import matplotlib.pyplot as plt

def sigmoid(x):
    return 1 / 1 + np.exp(-x)



A1 = np.array([0.3, 0.7, 1])

Z1 = sigmoid(A1)
# plt.plot(A1, Z1)
# plt.show()


W2 = np.array([[0.1, 0.4],[0.2, 0.5],[0.3, 0.6]])
B2 = np.array([0.2, 0.4])

A2 = np.dot(Z1, W2) + B2
Z2 = sigmoid(A2)

print("Z2:", Z2)
