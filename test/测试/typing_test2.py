import time

def main():
    text = "This is a typing test. Type this sentence and see how accurate and fast you can type."
    print(text)
    
    input("Press Enter to start...")
    
    start_time = time.time()
    user_input = input("Type the above sentence: ")
    end_time = time.time()
    
    time_taken = end_time - start_time
    accuracy = calculate_accuracy(text, user_input)
    
    print(f"Time taken: {time_taken} seconds")
    print(f"Accuracy: {accuracy}%")

def calculate_accuracy(original, user_input):
    original_words = original.split()
    input_words = user_input.split()
    
    correct = 0
    for i in range(len(original_words)):
        if i < len(input_words) and original_words[i] == input_words[i]:
            correct += 1
    
    accuracy = (correct / len(original_words)) * 100
    return round(accuracy, 2)

if __name__ == "__main__":
    main()