# -*- coding: UTF-8 -*-
filename = 'test.pdm'  # 大文件名
new_content = b'\x01\x01\x00'  # 新内容

# 打开大文件以进行读写操作
with open(filename, 'br+') as file:
    # 定位到文件的开头位置
    file.seek(0)

    # 读取前1万个字符
    content = file.read(20)

    print(content)

    # 定位到文件的开头位置
    file.seek(0)

    # 写入新内容
    file.write(new_content)

# 输出修改后的文件内容
# with open(filename, 'r') as file:
#     modified_content = file.read()
#     print(modified_content)


with open(filename, 'br+') as file:
    file.seek(0)
    print(file.read(20))
    file.seek(0)


