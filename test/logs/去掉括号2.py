def remove_unnecessary_parentheses(expression):
    stack = []
    result = ""
    operators = set(["+", "-", "*", "/"])

    for char in expression:
        if char == "(":
            stack.append(result)
            result = ""
        elif char == ")":
            if stack and stack[-1] == "(" and result and result[-1] in operators:
                stack.pop()
                result = stack.pop() + "(" + result + ")"
            else:
                result = stack.pop() + result + ")"
        else:
            result += char

    return result

# 原始表达式
expression = "((10 * (9 - 1)) - (7 * 8))"

# 去除不必要括号后的表达式
result = remove_unnecessary_parentheses(expression)

print(result)