# encoding:utf-8
import os
import shutil
import hashlib
import time
import chardet
import random
from public import write_log



# 拆分文件，将文件1，拆分成文件2和文件3
def split_file(file_path, first_file_path, second_file_path, chunk_size=4096):
    with open(file_path, "rb") as file:
        # 读取前1000~9999个字符
        first_data = file.read(random.randint(1000, 9999))
        
        # 创建第一个文件并写入前1000~9999个字符
        with open(first_file_path, "wb") as first_file:
            first_file.write(first_data)
        
        # 读取剩余内容
        second_data = file.read()
        
        # 创建第二个文件并写入剩余内容
        with open(second_file_path, "wb") as second_file:
            second_file.write(second_data)


# 合并文件到file2
def concat_file(small_file_path, large_file_path):
    # 读取小文件内容
    with open(small_file_path, 'rb') as small_file:
        small_content = small_file.read()

    # 读取大文件内容
    with open(large_file_path, 'rb') as large_file:
        large_content = large_file.read()

    # 合并内容
    merged_content = small_content + large_content

    # 将合并后的内容写入大文件
    with open(large_file_path, 'wb') as large_file:
        large_file.write(merged_content)



# 删除文件
def del_file(filename):
    if os.path.isfile(filename):
        os.remove(filename)

# 更换文件名
def rename_file(old_file_path, new_file_path):
    if os.path.isfile(new_file_path):
        os.remove(new_file_path)
    os.rename(old_file_path, new_file_path)



# 复制文件
def copy_file(source_file_path, destination_file_path):
    if os.path.isfile(destination_file_path):
        os.remove(destination_file_path)
    shutil.copy2(source_file_path, destination_file_path)


# 计算md5
def calculate_md5(file_path):
    with open(file_path, "rb") as file:
        md5_hash = hashlib.md5()
        chunk_size = 4096  # 每次读取的数据块大小
        while True:
            chunk = file.read(chunk_size)
            if not chunk:
                break
            md5_hash.update(chunk)
        return md5_hash.hexdigest()


# 比较文件是否一致
def compare_files(file1_path, file2_path):
    md5_file1 = calculate_md5(file1_path)
    md5_file2 = calculate_md5(file2_path)
    if md5_file1 == md5_file2:
        print("两个文件相同")
    else:
        print("两个文件不同")


# 获取文件信息
def get_file_info(file_path):
    # 获取文件大小（以字节为单位）
    file_size = os.path.getsize(file_path)

    # 打印文件信息
    print("文件路径: {}".format(file_path))
    print("文件大小: {} 字节".format(file_size))
    # print("文件编码: {}".format(file_encoding))

# 用户交互
def user_input():
    print("""------------- 大文件拆分/合并程序 -------------
    注：输入中带有'part'表示合并，其他为拆分
    案例： E:\project\py_dev\test\1.data.part1
          test001.data
          test\test001.data.part2
""")
    file_name = input("请输入文件名(带有'part'表示合并，其他为拆分)：")
    if not os.path.isfile(file_name):
        write_log("文件不存在【{}】".format(file_name), tip = "ERROR")
    
    if '.' not in file_name:
        write_log("必须文件名包含【.】", tip = "ERROR")
    
    new_file_name = file_name.replace('.part1','').replace('.part2','').replace('.part','')

    # 拆分和合并的文件名规范
    part1_file = new_file_name + '.part1'
    part2_file = new_file_name + '.part2'
    merge_file = new_file_name.split('.')[0] + "_merged." + new_file_name.split('.')[-1]


    if 'part' in file_name:
        write_log("开始删除拆分的文件")
        del_file(merge_file) 
        write_log("开始合并文件")
        rename_file(part2_file, merge_file)
        concat_file(part1_file, merge_file)
    else:
        write_log("开始删除合并的文件")
        del_file(part1_file)
        del_file(part2_file)
        
        write_log("开始拆分文件")
        split_file(file_name, part1_file, part2_file)


if __name__ == '__main__':
    # 记录开始时间
    start_time = time.time()

    # 用户交互
    user_input()

    # write_log("程序开始")

    # # 删除生成的文件
    # write_log("开始删除需生成的文件")
    # del_file(first_file_path)
    # del_file(second_file_path)
    # del_file(target_file_path)
    

    # # 拆分
    # write_log("开始拆分文件")
    # split_file(file_path, first_file_path, second_file_path)
    


    # # 合并
    # write_log("开始合并文件")
    # rename_file(second_file_path, target_file_path)
    # concat_file(first_file_path, target_file_path)
    

    # # 比较
    # compare_files(file_path, target_file_path)
    # get_file_info(file_path)
    # get_file_info(target_file_path)
    # write_log("比较完成")


    # 记录结束时间
    end_time = time.time()
    # 计算执行时间
    execution_time = end_time - start_time
    print("脚本执行时间: {:.2f} 秒".format(execution_time))


    print("执行完成！")