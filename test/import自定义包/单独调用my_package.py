"""
自定义 Python 包 代码示例
"""
import my_package
from my_package import my_module1
from my_package import my_module2

my_module1.my_module1_print()
my_module2.my_module2_print()

print(my_package.__all__)

# """
# 自定义 Python 包 代码示例
# """

# from my_package.my_module1 import my_module1_print
# from my_package.my_module2 import my_module2_print

# my_module1_print()
# my_module2_print()
