# 先拉取最新的代码
echo "---------- 更新最新的代码，有问题请终止程序 -------------"
git pull
sleep 2

# 上传文件到远程 
echo "---------- 上传文件到gitee远程服务器 -------------"

today1=`date +'%Y/%m/%d'`
today2=`date +'%H:%M:%S'`
#commit_info='提交代码 on '${today}

echo '------- 查看状态 --------'
git status

echo '------- git add . --------'
git add .


echo '------- git commit --------'
#echo '$commit_info'
# $1可以作为备注的信息
git commit -m $today1' '$today2'_提交代码 '$1



echo '------- git push --------'
git push origin master:master


echo '------- 查看状态 --------'
git status
echo '------- 查看日志 --------'
git log --pretty=oneline --color=always | head -6


echo '------- 查看分支对应情况 --------'
git branch -vv
echo "[提示]上传完成!"
