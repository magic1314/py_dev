import schedule
import time
import datetime
from plyer import notification
import winsound
import calendar
import threading
import public

"""
# 参数配置
# minute  hour  day  month  week  command
minute： 表示分钟，可以是从0到59之间的任何整数。
hour：表示小时，可以是从0到23之间的任何整数。
day：表示日期，可以是从1到31之间的任何整数。
month：表示月份，可以是从1到12之间的任何整数。
week：表示星期几，可以是从0到7之间的任何整数，这里的0或7代表星期日。
command：要执行的命令，可以是系统命令，也可以是自己编写的脚本文件。

# 特殊字符
星号（*）：代表所有可能的值，例如month字段如果是星号，则表示在满足其它字段的制约条件后每月都执行该命令操作。
逗号（,）：可以用逗号隔开的值指定一个列表范围，例如，“1,2,5,7,8,9”
中杠（-）：可以用整数之间的中杠表示一个整数范围，例如“2-6”表示“2,3,4,5,6”
正斜线（/）：可以用正斜线指定时间的间隔频率，例如“0-23/2”表示每两小时执行一次。同时正斜线可以和星号一起使用，例如*/10，如果用在minute字段，表示每十分钟执行一次。
"""

CLOSING_TIME = '18:03'
REPORT_WEEKLY_TIME = '18:04'

CRONTAB_CLOSING_TIME = '03 17 * * 1-5'
CRONTAB_REPORT_WEEKLY_TIME = ''


def dismantle_crontab(crontab_time: str):
    if len(crontab_time.split()) != 5:
        public.write_log(f"crontab_time: {crontab_time} 格式错误！", tip='ERROR')
    minute,hour,day,month,week = crontab_time.split()

    # * *
    if not (day == "*" and month == "*"):
        public.write_log(f"crontab_time: {crontab_time} 格式错误！day和month必须为【*】", tip='ERROR')

    if '*' in hour or '*' in minute:
        public.write_log(f"crontab_time: {crontab_time} 格式错误！hour和minute不能有【*】", tip='ERROR')
    print(minute,hour,day,month,week)

    hm = hour + ':' + minute

    if '/' in week:
        public.write_log(f"crontab_time: {crontab_time} 格式错误！week中不能有【/】", tip='ERROR')
    elif ',' in week:
        pass
    elif '-' in week:
        pass
    else:
        pass



def is_weekday():
    """判断当前日期是否为工作日（周一至周五）"""
    today = datetime.datetime.now()
    return calendar.day_name[today.weekday()] not in ['Saturday', 'Sunday']

def is_friday():
    """判断当前日期是否为周五"""
    today = datetime.datetime.now()
    return calendar.day_name[today.weekday()] == 'Friday'



def close_notification_after_delay():
    """模拟通知关闭的延时操作"""
    time.sleep(10)  # 等待10秒
    print("提醒消息已自动关闭（模拟操作）")


# 设定下班提醒时间，例如工作日18:05
def check_out():
    if is_weekday():
        # 发送桌面通知
        notification.notify(
            title='打卡提醒',
            message='请记得打卡, 把充电器！',
            app_name='提醒助手',
            timeout=1,  # 尝试设置通知自动关闭时间，但不一定所有系统都支持注：无效
        )
        # 播放声音提醒（Windows系统下的默认提示音）
        # winsound.MessageBeep()

        public.write_log(f"打卡提醒已发送！下班提醒时间：{CLOSING_TIME}")
    else:
        public.write_log("今天是非工作日，不发送打卡提醒。")

# 周报
def report_weekly():
    if is_friday():
        # 发送桌面通知
        notification.notify(
            title='打卡提醒',
            message='请记得开始写周报！',
            app_name='提醒助手',
            timeout=30,  # 尝试设置通知自动关闭时间，但不一定所有系统都支持 注：无效
        )
        # 播放声音提醒（Windows系统下的默认提示音）
        # winsound.MessageBeep()

        public.write_log(f"打卡提醒已发送！周报提醒时间：{REPORT_WEEKLY_TIME}")
    else:
        public.write_log("今天是非工作日，不发送打卡提醒。")

# 多线程
def run_threaded(job_func):
    job_thread = threading.Thread(target=job_func)
    job_thread.start()



def start():
    # 每天早上9点执行检查是否为工作日并提醒的任务
    schedule.every().day.at(CLOSING_TIME).do(run_threaded, check_out)
    schedule.every().day.at(REPORT_WEEKLY_TIME).do(run_threaded, report_weekly)


    public.write_log(f"--------------------------- 提醒程序开始 ---------------------------")
    # 主循环，保持程序运行并按计划执行任务
    while True:
        schedule.run_pending()
        time.sleep(5)


if __name__ == '__main__':
    start()