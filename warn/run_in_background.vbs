Set WshShell = CreateObject("WScript.Shell")
Set objWMIService = GetObject("winmgmts:\\.\root\cimv2")

' 替换这里的 "notepad.exe" 为你想要管理的程序名称
strProgramName = "run.bat"

' 查询并终止所有与目标程序名称匹配的进程
strQuery = "SELECT * FROM Win32_Process WHERE Name = '" & strProgramName & "'"
Set colProcesses = objWMIService.ExecQuery(strQuery)

set len_col = len(colProcesses)
'Msgbox("个数：" & len_col)

For Each objProcess In colProcesses
    objProcess.Terminate()
Next

' 等待一段时间确保进程已被终止（可根据需要调整延迟时间）
WScript.Sleep 1000 ' 等待1秒，可根据实际情况调整

' 重新启动程序
' 我们使用了0作为WshShell.Run方法的第二个参数来隐藏窗口。如果你希望程序前台可见，应将这个参数改为1或省略它（默认值为1，即窗口可见）
WshShell.Run "cmd /c start " & strProgramName, 1, False

Set WshShell = Nothing
Set objWMIService = Nothing