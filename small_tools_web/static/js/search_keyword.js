// 单击【提交】按钮
function btn_submit(event){
  console.log("触发了search_keyword.html btn_submit函数！")
  // 点击触发的事件
  var x = document.getElementById("btn_submit");

  // console.log('66666:' + document.getElementById('search_path').value)
  var search_path = document.getElementById('search_path').value;
  var search_content = document.getElementById('search_content').value;
  if ( search_path.trim() === '' || search_content.trim() === '') {
    alert('路径/关键字不能为空！');
    return;
  }

  
  $.ajax({
      type: 'POST',
      url: '/search_keyword',
      contentType: 'application/json',
      data: JSON.stringify(
        {
          "search_path": document.getElementById('search_path').value,
          "search_file_name": document.getElementById('search_file_name').value,
          "search_content": document.getElementById('search_content').value
        }
      ),
      success: function(response) {
          // console.log("返回成功！");
          // console.log(response);
          var data = response; // 假设后端返回的 JSON 包含一个 'data' 字段
          window.search_keyword_data = response
          console.log("返回的记录数：" + data.length)

          if (data){
            // console.log("11111：存在记录 if(data)")
            var w0 = 30;
            var w1 = 200;
            var w2 = 350;
            var w3 = 80;
            var w4 = 50;
            var w5 = 50;
  
            // var html = document.getElementById('dataList'); // 初始化一个空字符串来构建 HTML
            var header_width = '<col name="el-table_1_column_0" width="' + w0 + '">'
            + '                                    <col name="el-table_1_column_1" width="' + w1 + '">'
            + '                                    <col name="el-table_1_column_2" width="' + w2 + '">'
            + '                                    <col name="el-table_1_column_3" width="' + w3 + '">'
            + '                                    <col name="el-table_1_column_4" width="' + w4 + '">'
            + '                                    <col name="el-table_1_column_5" width="' + w5 + '">'
            + '                                    <col name="gutter" width="0">               '
            $("#header_width").html(header_width); // 将生成的 HTML 插入到页面中
  
  
  
            var html =   '<colgroup>                                                                      '
            // + '<span>ceshi</span>'
            + '                                    <col name="el-table_1_column_0" width="' + w0 + '">'
            + '                                    <col name="el-table_1_column_1" width="' + w1 + '">'
            + '                                    <col name="el-table_1_column_2" width="' + w2 + '">'
            + '                                    <col name="el-table_1_column_3" width="' + w3 + '">'
            + '                                    <col name="el-table_1_column_4" width="' + w4 + '">'
            + '                                    <col name="el-table_1_column_5" width="' + w5 + '">'
            + '                                </colgroup>                                     '
  
            // console.log("记录数：" + response.length);
            // console.log(html)

            for (var i = 0; i < data.length; i++) {
                // 假设 data[i] 是一个对象，包含 'name' 和 'value' 属性
                // html += "<div>" + data[i].file_name + ": " + data[i].dir_name + "</div>";
                html += '                                <tbody>'
                + '                                        \<tr class=\"el-table__row\"\>                                                           '

                + '                                        \<td rowspan=\"1\" colspan=\"1\" class=\"el-table_1_column_0   el-table__cell\"\>    '
                + '                                            <div class="cell el-tooltip" style="width: auto; cursor: pointer; text-align: center;">' 
                                                                + '<span>' + data[i].seq + '</span>'
                                                                + '\</div\>'
                + '                                        \</td\>                                                                              '



                + '                                        \<td rowspan=\"1\" colspan=\"1\" class=\"el-table_1_column_1   el-table__cell\"\>    '
                + '                                        \<div class=\"cell el-tooltip\" title="' + data[i].file_name + '" style=\"width: ' + 'auto' + ';\"\>' + data[i].file_name + '\</div\>'
                + '                                        \</td\>                                                                              '

                + '                                        \<td rowspan=\"1\" colspan=\"1\" class=\"el-table_1_column_2   el-table__cell\"\>    '
                + '                                            \<div class=\"cell el-tooltip\" title="' + data[i].file_path + '" style=\"width: ' + 'auto' + '; cursor: pointer;\"\>' 
                                                                + '<span onclick="copy_source_to_html(event, 2);">' + data[i].file_path + '</span>'
                                                                + '\</div\>'
                + '                                        \</td\>                                                                              '

                + '                                        \<td rowspan=\"1\" colspan=\"1\" class=\"el-table_1_column_3   el-table__cell\"\>    '
                + '                                            \<div class=\"cell el-tooltip\" style=\"width: ' + 'auto' + ';\"\>' + data[i].modified_time + '\</div\>'
                + '                                        \</td\>                                                                              '
                + '                                        \<td rowspan=\"1\" colspan=\"1\" class=\"el-table_1_column_4   el-table__cell\"\>    '
                + '                                            \<div class=\"cell el-tooltip\" style=\"width: ' + 'auto' + ';\"\>' + data[i].file_size_h + '\</div\>'
                + '                                        \</td\>                                                                              '
                + '                                        \<td rowspan=\"1\" colspan=\"1\" class=\"el-table_1_column_5   el-table__cell" style="text-align: center;">    '
                // + '                                            <div class="cell el-tooltip" style="width: auto;">' + data[i].file_type + '</div>'
                                                                  // 小眼睛图标-查看
                +  '                                              <i class="fas fa-eye view-button i_button_style" onclick="copy_source_to_html(event, -1)" title="查看文件"></i>'
                                                                  // 复制图标-复制
                +  '                                              <i class="fas fa-copy copy-button i_button_style" onclick="copy_file_name(event)" title="复制文件名"></i>'         
                                                                  // 下载图标--下载
                +  '                                              <i class="fas fa-download download-button i_button_style" onclick="downloadFile(event)" title="下载文件"></i>'
                + '                                        \</td\>                                                                              '




                + '                                     \</tr\>                                                                                 '
                + '                                \</tbody\>                                                                                   '
            }

            if (data.length == 0){
              html += '<tbody><tr><td rowspan="1" colspan="6" style="text-align: center; font-size: 18px;">无查询结果</td></tr></tbody>';
            }
          }

          // console.log("html:" + html);
          $("#dataList").html(html); // 将生成的 HTML 插入到页面中
          // console.log(html); // 结果
      },
      error: function(xhr, status, error) {
          console.log('Ajax请求失败：' + error);
      }
  });
}

// 复制本地文件到项目html文件中
function copy_source_to_html(event, colSeq){
  console.log("触发了search_keyword.html copy_source_to_html函数！");
  // console.log(1, window.search_keyword_data);
  // console.log(2, event.target);
  // console.log(3, event.target.id);
  // console.log(4, event.target.tagName);
  if (colSeq == 2){
    click_text = event.target.innerHTML;
  }else if(colSeq == -1){
    click_text = event.target.parentNode.parentNode.querySelectorAll('td')[2].querySelector('div').title
  }else{
    click_text = none
  }

  $.ajax({
    type: 'POST',
    url: '/search_keyword/deal_file',
    contentType: 'application/json',
    data: JSON.stringify({
      "search_keyword_data": window.search_keyword_data,
      "click_text": click_text,
      "key_word": document.getElementById('search_content').value
    }),
    success: function(response) {
        console.log("返回成功！");
        // console.log(response)
        // 打开新页面，展示文件内容
        window.open('/search_keyword/show_file', '_blank');
    },
    error: function(xhr, status, error) {
        console.log('Ajax请求失败：' + error);
    }
  });
}

// 获得bytes，用于排序
function get_bytes(str){
  // console.log("输入内容：" + str);
  str = str.toUpperCase()
  if (str.includes(' B')){
    tmp = str.replace(/ B/g, "");
    result = parseInt(tmp, 10);
  }else if(str.includes('KB')){
    tmp = str.replace(/KB/g, "");
    result = parseInt(tmp, 10) * 1024;
  }else if(str.includes('MB')){
    tmp = str.replace(/MB/g, "");
    result = parseInt(tmp, 10) * 1024 * 1024;
  }else if(str.includes('GB')){
    tmp = str.replace(/GB/g, "");
    result = parseInt(tmp, 10) * 1024 * 1024 * 1024;
  }else if(str.includes('TB')){
    tmp = str.replace(/TB/g, "");
    result = parseInt(tmp, 10) * 1024 * 1024 * 1024 * 1024;
  }else{
    result = 0;
  }

  return result;
}


// 排序
// columnIndex：列索引，sortType：排序方式(asc, desc)
function sortTable(columnIndex, sortPos) {
  console.log("[js脚本]用户点击sortTable(" + columnIndex + "," + sortPos + ")");
  var table, rows, switching, i, x, y, shouldSwitch;
  table = document.getElementById("dataList");
  // console.log("table:" + table);
  // console.log("行数：" + table.rows);
  switching = true;
  loop = 1;
  window.sortSeq = {};
  window.sortSeq[columnIndex] = sortPos;
  
  // 下次排序的
  if (window.sortSeq === undefined){
    window.sortSeq[columnIndex] = 'desc';
  }else if(window.sortSeq[columnIndex] == 'desc'){
    window.sortSeq[columnIndex] = 'asc';
  }else{
    window.sortSeq[columnIndex] = 'desc';
  }

  while (switching) {
      // console.log("轮询次数：" + loop);
      loop++;
      switching = false;
      rows = table.rows;

      // 开始遍历所有的TD数据，两两判断是否需要排序
      // 直到没有排序，结束整个循环
      // for (i = 1; i < (rows.length - 1); i++) {
      for (i = 0; i < (rows.length - 1); i++) {
          shouldSwitch = false;
          x1 = rows[i].getElementsByTagName("TD")[columnIndex].getElementsByClassName('cell')[0].innerHTML.toLowerCase();
          y1 = rows[i + 1].getElementsByTagName("TD")[columnIndex].getElementsByClassName('cell')[0].innerHTML.toLowerCase();
          
          if (window.sortSeq[columnIndex] == 'asc'){
            // columnIndex为4，是【大小】列
            if (columnIndex == 4){
              // console.log("------------ x1=" + x1 + " y1=" + y1);
              bytes_x = get_bytes(x1);
              bytes_y = get_bytes(y1);
              if (bytes_x < bytes_y) {shouldSwitch = true;break;}
            }else{
              if (x1 < y1) {shouldSwitch = true;break;}
            }
          }else{
            // columnIndex为4，是【大小】列
            if (columnIndex == 4){
              bytes_x = get_bytes(x1);
              bytes_y = get_bytes(y1);
              if (bytes_x > bytes_y) {shouldSwitch = true;break;}
            }else{
              if (x1 > y1) {shouldSwitch = true;break;}
            }
          }
      }

      // console.log("x:" + x + ",y:" + y + ",window.sortSeq[columnIndex]=" + window.sortSeq[columnIndex] + ",shouldSwitch=" + shouldSwitch);
      
      // 元素i和i+1调换
      if (shouldSwitch) {
          // console.log("66666666666" + rows[i].parentNode);
          rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
          switching = true;
      }
  }
}



// 复制文件名
function copy_file_name(event) {
    file_name = event.target.parentNode.parentNode.querySelectorAll('td')[2].querySelector('div').title;
    // console.log("file_name=" + file_name);

    // 创建一个新的 textarea 元素
    var textarea = document.createElement("textarea");
    // 赋值
    textarea.value = file_name;

    // 将 textarea 添加到文档中
    document.body.appendChild(textarea);
    // 选中 textarea 中的文本
    textarea.select();
    // 复制文本到粘贴板
    document.execCommand("copy");
    // 从文档中移除 textarea 元素
    document.body.removeChild(textarea);

    console.log("已复制【" + textarea.value + "】到粘贴板上！");

}

// 下载文件
function downloadFile(event) {
  console.log("暂时没有实现【下载】功能！");

  return;


  // file_name = event.target.parentNode.parentNode.querySelectorAll('td')[2].querySelector('div').title;
  // const fs = require('fs');
  // fs.writeFile('./newfile.txt', '', err => {
  //     if (err) {
  //         console.error('Error writing file', err);
  //     } else {
  //         console.log('Empty file created successfully!');
  //     }
  // });
}