// 函数定义
function btn_submit(){
    console.log("触发了backup_table_sql.html btn_submit函数！")
    // 点击触发的事件
    var x = document.getElementById("btn_submit");
    var input_content = document.getElementById("input_content").value;
    
    $.ajax({
        type: 'POST',
        url: '/backup_table_sql',
        contentType: 'application/json',
        data: JSON.stringify({input_content: input_content}),
        success: function(response) {
            console.log("返回成功！");
            console.log(response);
            document.getElementById("result1").innerHTML = '结果：';
            // 定义一个变量
            var x = response['element_cnt'];

            // 判断变量是否为 undefined
            if (typeof x === 'undefined') {
                console.log('变量 x 未定义或赋值为 undefined');
                document.getElementById("result2").innerHTML = response['formatted_id'];
                document.getElementById("result2").style.color = 'red'; // 红色问题提醒
                document.getElementById("output_content").style.display = "none"; //不显示
            } else {
                document.getElementById("result2").innerHTML = '提示：共有 ' + x + '个元素(粘贴到notepad)';
                document.getElementById("result2").style.color = 'blue'; // 和原html中颜色保持一致
                document.getElementById("output_content").style.display = "block";
                document.getElementById("output_content").innerHTML = response['formatted_id'];
            }

            // 复制输出的结果
            copy_button();

        },
        error: function(xhr, status, error) {
            console.log('Ajax请求失败：' + error);
        }
    });
}




