// 公共函数区域
// 函数定义
function copy_button(){
    console.log("触发了copy_button函数！")

    // var copyButton = document.getElementById("copy_button");
    var ouputContent = document.getElementById("output_content");
    var copied = document.getElementById("copied");

    // 创建一个新的 textarea 元素
    var textarea = document.createElement("textarea");
    // 设置 textarea 的内容为要复制的文本
    if (typeof ouputContent.value === "undefined") {
      console.log("ouputContent的值为空！设置为：" + ouputContent.innerText);
      textarea.value = ouputContent.innerText;
    }else{
      textarea.value = ouputContent.value;
    }

    
    // 将 textarea 添加到文档中
    document.body.appendChild(textarea);
    // 选中 textarea 中的文本
    textarea.select();
    // 复制文本到粘贴板
    document.execCommand("copy");
    // 从文档中移除 textarea 元素
    document.body.removeChild(textarea);

    console.log("已复制【" + textarea.value + "】到粘贴板上！");
    copied.style.display = "inline-block";
}




