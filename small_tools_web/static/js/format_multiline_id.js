// 函数定义
function btn_submit(){
    console.log("触发了format_multiline_id.html btn_submit函数！")
    // 点击触发的事件
    var x = document.getElementById("btn_submit");
    var input_content = document.getElementById("input_content").value;

    var chk1 = document.getElementById("checkbox1").checked;
    var chk2 = document.getElementById("checkbox2").checked;
    var chk3 = document.getElementById("checkbox3").checked;
    var chk4 = document.getElementById("checkbox4").checked;

    var txt1 = document.getElementById("checkbox1").nextSibling.textContent;
    var txt2 = document.getElementById("checkbox2").nextSibling.textContent;
    var txt3 = document.getElementById("checkbox3").nextSibling.textContent;
    var txt4 = document.getElementById("checkbox4").nextSibling.textContent;
    
    $.ajax({
        type: 'POST',
        url: '/format_multiline_id',
        contentType: 'application/json',
        data: JSON.stringify({input_content: input_content,
                            my_choice: {
                                            "checkbox1":  [txt1, chk1],
                                            "checkbox2":  [txt2, chk2],
                                            "checkbox3":  [txt3, chk3],
                                            "checkbox4":  [txt4, chk4],
                                        }
                            }),
        success: function(response) {
            console.log("返回成功！");
            console.log(response);
            document.getElementById("result1").innerHTML = '结果：';
            // 定义一个变量
            var x = response['element_cnt'];

            // 判断变量是否为 undefined
            if (typeof x === 'undefined') {
                console.log('变量 x 未定义或赋值为 undefined');
                document.getElementById("result2").innerHTML = response['formatted_id'];
                document.getElementById("result2").style.color = 'red'; // 红色问题提醒
                document.getElementById("output_content").style.display = "none"; //不显示
            } else {
                document.getElementById("result2").innerHTML = '提示：共有 ' + x + '个元素';
                document.getElementById("result2").style.color = 'blue'; // 和原html中颜色保持一致
                document.getElementById("output_content").style.display = "block";
                document.getElementById("output_content").innerHTML = response['formatted_id'];
            }

            // 复制输出的结果
            copy_button();
        

        },
        error: function(xhr, status, error) {
            console.log('Ajax请求失败：' + error);
        }
    });



}




