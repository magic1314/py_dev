// 函数定义
function btn_submit(){
    console.log("触发了generate_insert_sql.html btn_submit函数！")
    // 点击触发的事件
    var x = document.getElementById("btn_submit");
    var input_content = document.getElementById("input_content").value;
    
    $.ajax({
        type: 'POST',
        url: '/generate_insert_sql',
        contentType: 'application/json',
        data: JSON.stringify({input_content: input_content}),
        success: function(response) {
            console.log("返回成功！");
            console.log(response);
            
            // // 显示内容 
            // document.getElementById("result1").innerHTML = '结果：';
            // document.getElementById("result2").style.color = 'blue'; // 和原html中颜色保持一致
            // document.getElementById("output_content").style.display = "block";
            // document.getElementById("output_content").innerHTML = response['result'];

            // 定义一个变量
            var x = response['element_cnt'];

            // 判断变量是否为 undefined
            if (typeof x === 'undefined') {
                document.getElementById("result2").innerHTML = '未知错误！';
                document.getElementById("result2").style.color = 'red'; // 红色问题提醒
                document.getElementById("output_content").style.display = "none"; //不显示
            } else {
                document.getElementById("result2").innerHTML = '生成结果';
                document.getElementById("result2").style.color = 'blue'; // 和原html中颜色保持一致
                document.getElementById("output_content").style.display = "block";
                document.getElementById("output_content").innerHTML = response['result'];
            }



            // 复制输出的结果
            copy_button();

        },
        error: function(xhr, status, error) {
            console.log('Ajax请求失败：' + error);
        }
    });
}




