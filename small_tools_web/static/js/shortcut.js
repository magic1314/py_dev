// shortcut.js
document.addEventListener("keydown", function(event) {
  if(event.altKey && event.key === "`"){
    console.log("接受到快捷键【alt+" + event.key + "】");
    window.location.href = '/';
  }else if (event.altKey && event.key === '1') {
    // 发送请求到服务器端，触发相应的操作
    // fetch('/get_table', {
    //   method: 'POST',
    //   headers: {
    //     'Content-Type': 'application/json'
    //   },
    //   body: JSON.stringify({ shortcut: 'alt+1' })
    // })
    // .then(function(response) {
    //   // 处理服务器端的响应
    //   console.log("接受到快捷键【alt+1】");
    // });

    // 发送GET请求并跳转到指定页面
    console.log("接受到快捷键【alt+" + event.key + "】");
    window.location.href = '/get_table';
  }else if(event.altKey && event.key === '2'){
    console.log("接受到快捷键【alt+" + event.key + "】");
    window.location.href = '/format_multiline_id';
  }else if(event.altKey && event.key === '3'){
    console.log("接受到快捷键【alt+" + event.key + "】");
    window.location.href = '/backup_table_sql';
  }else if(event.altKey && event.key === '4'){
    console.log("接受到快捷键【alt+" + event.key + "】");
    window.location.href = '/generate_insert_sql';
  }else if(event.altKey && event.key === '5'){
    console.log("接受到快捷键【alt+" + event.key + "】");
    window.location.href = '/search_keyword';
  }
});
