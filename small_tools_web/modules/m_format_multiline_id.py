# encoding:utf-8
import sys
import os
import re


# 去掉空白格
def remove_space(s):
    # 使用正则表达式去掉字符串中的所有空白符号
    s_stripped = re.sub(r'[\t ]', '', s)

    # 输出去掉空白符号后的字符串
    # print(s_stripped)  # 输出：helloworld
    return s_stripped

def format_id(content,is_one_line=True, is_quotation_mark=True, bracket_type=2, separator=',', duplicate_removal=False):
    """
    content:需要处理的内容 
    is_quotation_mark：是否需要带单引号
    bracket_type：
            0：不带括号
            1：带括号，不换行
            2：带括号，右括号换行
    
    """


    # print(content)

    data = dict()

    content = remove_space(content)
    result = content.replace('\r\n',',').replace('\n',',')
    result = re.sub(',+',',',result) # 去掉多个,
    result = result.strip(',')
    # print(666, result)

    # 元素列表
    element_list = result.split(',')
    # print(777, element_list)

    if duplicate_removal:
        element_list = list(set(element_list))
        # print(888, element_list)


    data['element_cnt'] = len(element_list)
    result = ','.join(element_list)
    if is_quotation_mark:
        result = '\'' + result.replace(',','\',\'') + '\''
    

    if bracket_type == 1:
        result = '(' + result + ')'
    elif bracket_type == 2:
        result = '(' + result + '\n)'
    
    # 不是选择【不换行】
    if not is_one_line:
        result = result.replace(',','\n,')
    
    # 替换分隔符
    result = result.replace(',', separator)

    data['formatted_id'] = result
    return data



if __name__ == '__main__':
    str = '''
11
22
333
11
11
222
    
    '''


    print(format_id(str, duplicate_removal=True))
