import os
import time
import re
import shutil
from datetime import datetime
from public import write_log


# 获得文件size
def get_file_size(file_path, units=[' B', 'KB', 'MB', 'GB', 'TB']):
    size = os.path.getsize(file_path)
    # size = 88999999
    for unit in units:
        if size < 1024.0 or unit == units[-1]:
            break
        size /= 1024.0
    
    formatted_number = f"{size:.2f}".rstrip('0').rstrip('.')
    return f"{formatted_number}{unit}"


# 获得文件的创建和更新时间
def get_file_times(file_path):
    stat_info = os.stat(file_path)
    
    # 获取最后修改时间
    mtime = stat_info.st_mtime
    modified_time = datetime.fromtimestamp(mtime).strftime('%Y-%m-%d %H:%M:%S')
    
    # 获取创建时间
    if hasattr(os, 'statvfs'):
        # Unix-like系统可能需要使用其他方法获取创建时间
        ctime = stat_info.st_ctime
    else:
        # Windows系统
        ctime = stat_info.st_ctime
    created_time = datetime.fromtimestamp(ctime).strftime('%Y-%m-%d %H:%M:%S')
    
    return created_time, modified_time


# 获得文件类型
def get_file_type(file_name):
    mapping = {
        '.sql': 'SQL脚本',
        '.txt': '文本文件',
        '.py': 'Python脚本',
        '.java': 'Java源码',
        '.html': 'HTML文件',
        '.css': 'CSS文件',
    }
    simple_file_name = os.path.basename(file_name)
    for k in mapping.keys():
        if k in simple_file_name:
            return mapping[k]
    
    return None


# 找关键字
def m_search_keyword(directory=None, target_string=None, file_filter_condition=None, regex=True):
    if not file_filter_condition:
        file_filter_condition = '.*sql$'
    if not directory or not os.path.isdir(directory):
        print(f'The directory {directory} does not exist.')
        return None
    
    
    results_list = []
    seq = 1
    for root, dirs, files in os.walk(directory):
        for file in files:
            # print(111, file_path_pattern, file)
            try:
                # 匹配的表达式
                file_path_pattern = re.compile(file_filter_condition)
                result = file_path_pattern.search(file)
            except re.error as e:
                result = None
            
            if result:
                # print(123456, result.group(), file)
                file_path = os.path.abspath(os.path.join(root, file))
                try:
                    with open(file_path, mode='r' ,encoding='utf-8') as f:
                        f_read = f.read()
                        result1 = target_string in f_read

                        try:
                            content_pattern = re.compile(target_string, re.DOTALL)
                            result2 = content_pattern.search(f_read)
                            # if result2:
                            #     print("3333 result2: ", result2.group(), file)
                        except re.error as e:
                            result2 = None
                            print(f"result2: {result2} error: {e}")
                            

                        # print(444, result1, result2, file, target_string)
                        if result1 or result2:
                            file_size_h = get_file_size(file_path)
                            file_size_b = os.path.getsize(file_path)
                            file_name = os.path.basename(file_path)
                            dir_name = os.path.dirname(file_path)
                            created_time, modified_time = get_file_times(file_path)
                            file_type = get_file_type(file)
                            record_dict = {
                                'seq': seq,
                                'file_name': file_name,
                                'dir_name': dir_name,
                                'file_size_h': file_size_h,
                                'file_size_b': file_size_b,
                                'created_time': created_time,
                                'modified_time': modified_time,
                                'file_path': file_path,
                                'file_type': file_type,
                                # 'file_content': f.read()[:500] + '...' if len(f.read()) > 500 else f.read()  # 仅打印前500个字符以节省输出量
                            }
                            # 如果文件大小的超过1MB，则不展示该文件
                            if file_size_b >= 1024 * 100:
                                write_log(f"[提醒]：文件【{file_path}】大小为{file_size_h}，文件大小超过100KB，不展示该文件！")
                                continue
                            seq += 1
                            results_list.append(record_dict)
                except Exception as e:
                    print(f'Error occurred while reading file {file_path}: {str(e)}')    
    return results_list


"""
搜索路径   E:\工作文件中心\人力绩效系统\生产环境
文件名     .sql
文件内容   alter table int_d_new_hrs_emp_chain add index idx_

"""

# 复制文件
def copy_file(source_abs_file_name, target_abs_file_name):
    source_file_name = os.path.basename(source_abs_file_name)
    target_abs_dirname = os.path.dirname(target_abs_file_name)
    new_source_abs_file_name = os.path.join(target_abs_dirname, source_file_name)
    # 复制文件
    shutil.copy2(source_abs_file_name, target_abs_dirname)  # 使用copy2会保留文件元数据，如权限和时间戳

    # 删除目标文件
    if os.path.exists(target_abs_file_name):
        os.remove(target_abs_file_name)
    
    # 移动文件
    os.rename(new_source_abs_file_name, target_abs_file_name)

# 替换内容
def replace_content(template, data, target, key_word, source_file_name):
    write_log(f"replace_content函数，参数为template={template}, data={data}, target={target}, key_word={key_word}, source_file_name={source_file_name}")
    with open(template, mode='r', encoding='utf-8') as template, \
        open(data, mode='r', encoding='utf-8') as data, \
        open(target, mode='w', encoding='utf-8') as target:
        
        for line in template.readlines():
            # print(66666666, line)
            if line.startswith(r'${content}'):
                # target.write(line.replace(r'${content}', data.read()))
                for d in data.readlines():
                    target.write(d.replace(key_word, f'''<strong style="background-color: yellow; color: blue; font-size:20px">{key_word}</strong>'''))
            elif r'${title_name}' in line:
                # print(6666666666666, f'出现:{line}')
                target.write(line.replace(r'${title_name}', source_file_name))
            elif line.startswith(r'${header}'):
                pass
            else:
                # 不处理，内容直接复制过去
                target.write(line)


# 处理数据
def m_search_keyword_deal_data(response_data):
    # 前台获取的需要处理的文件
    click_text = response_data['click_text']
    source_abs_file_name = click_text
    source_file_name = os.path.basename(source_abs_file_name)

    # 后台需要的模板路径
    script_dir = os.path.dirname(os.path.abspath(__file__))
    templates_dir = os.path.dirname(script_dir)

    # 后台需要的需要处理的数据的路径
    data_abs_file_name = os.path.join(templates_dir, 'templates', 'search_file_data.data')
    template_abs_file_name = os.path.join(templates_dir, 'templates', 'search_file_data.module')
    target_abs_file_name = os.path.join(templates_dir, 'templates', 'template_search_file_data.html')

    # 关键字
    key_word = response_data['key_word']

    # 后台需要的需要处理的数据，暂时不需要
    search_keyword_data = response_data['search_keyword_data']
    file_name = None
    for line in search_keyword_data:
        # print(line)
        if line['file_path'] == click_text:
            file_name = line['file_name']
            break
    
    # 将本地的文件复制到项目文件中
    copy_file(source_abs_file_name, data_abs_file_name)

    # 将【search_file_data_module.html】中参数替换
    replace_content(template_abs_file_name, data_abs_file_name, target_abs_file_name, key_word, source_file_name)

    write_log(f"m_search_keyword_deal_data函数，需要处理的数据：【{response_data}】")


if __name__ == '__main__':
    # 使用示例
    directory_to_search = r'E:\工作文件中心\人力绩效系统\生产环境\20240712'  # 替换为你要搜索的目录
    # print(directory_to_search)
    target_string = """t_dict_assess_pd_mqh"""  # 替换为你想要查找的字符串
    result = m_search_keyword(directory=directory_to_search, target_string=target_string, regex=False)
    # print(result)
    for record in result:
        print(record['file_path'])
        print(get_file_size(record['file_path']))