# encoding:utf-8
import sys
import os
import re
from public import write_log
from modules.m_backup_table_sql import backup_sql

# 获取依赖表
# 新增update关键字的查找
def get_sql_table_name(sqlResult, is_choice1: bool, is_choice2: bool, is_choice3 = False):

    # print(666666666666, len(sqlResult))

    # 16进制，20是空格
    SEPARTOR = '\x20'

    # 处理--的注释
    sql_content = re.sub(r'--.*\n', SEPARTOR, sqlResult)

    # 处理/**/的注释, 注意：存在多行，且需要非贪婪模式
    sql_content = re.sub(r'/\*.*?\*/', SEPARTOR, sql_content, flags=re.DOTALL)

    # 剔除反引号
    if is_choice1:
        sql_content = re.sub(r'`', '', sql_content)

    # print("格式化后：", sql_content)
    # sys.exit(0)


    # 处理sql
    # 忽略一种笛卡尔积的写法 select * from a,b;  这里便是b表检索不出来
    # 检索from、join、update关键字
    """
    原：
        select * from a  -- 主表
        left join b 
        on a.id = b.id
        ;
        UPDATE a set id = 2 where id = 1; 
    
    sql_content格式化：
        select * from|a left join|b on a.id = b.id update|a set id = 2 where id = 1

    list_sql_file列表格式：
        ['select', '*', 'from|a' ...]
    
    
    """
    # 括号替换成分隔符；换行符、制表符和分号替换成分隔符
    sql_content = re.sub('[\(\)\n\r;\t]', SEPARTOR, sql_content).lower()
    # 多个分隔符替换成一个
    sql_content = re.sub(SEPARTOR + '+', SEPARTOR, sql_content)
    # from join 和 update
    sql_content = sql_content.replace('from' + SEPARTOR, 'from|').replace('join' + SEPARTOR, 'join|').replace('update' + SEPARTOR,'update|')
    # insert into
    sql_content = sql_content.replace('insert' + SEPARTOR,'insert|').replace('into' + SEPARTOR,'into|')

    #write_log("[解析表模块]格式化后sql: 【{}】".format(sql_content))

    list_sql_file = sql_content.split(SEPARTOR)

    # print(list_sql_file)
    if __name__ == '__main__':
        write_log("列表：{}".format(list_sql_file))


    # 递归表
    list_recursive_table = []
    for i in range(len(list_sql_file)):
        # print(666, sqlStr)
        try:
            if list_sql_file[i] == 'with' and list_sql_file[i+1] == 'recursive':
                list_recursive_table.append(list_sql_file[i+2])
        except Exception as e:
            write_log("出现了一个异常：{e}".format(e), tip='WARN')
    # print('-------------------------------')
    # print(9999,list_recursive_table)
    if list_recursive_table:
        write_log("发现{}个递归临时表：【{}】".format(len(list_recursive_table),list_recursive_table))

    list_depend_table = []
    for sqlStr in list_sql_file:
        # join|'改为只能在最前面
        # 所有符合条件的表名
        if (sqlStr.startswith('join|') or sqlStr.startswith('from|')  or sqlStr.startswith('update|') or sqlStr.startswith('insert|into|')):
            if sqlStr in ('update|current_timestamp','from|trim'):
                # 这些是不需要的表
                continue
            elif '|select' in sqlStr:
                # 这些是不需要的表
                continue
            elif (sqlStr.startswith('join|') or sqlStr.startswith('from|')):
                depend_table_name = sqlStr[len('join|'):]
            elif sqlStr.startswith('update|'):
                depend_table_name = sqlStr[len('update|'):]
            elif sqlStr.startswith('insert|into|'):
                depend_table_name = sqlStr[len('insert|into|'):]
            else:
                pass
            
            # 隐藏临时表
            if is_choice2 and (depend_table_name.startswith('tmp') or depend_table_name.startswith('temp')):
                continue
            write_log("找到符合条件的值，检索的值：【{}】，保留后的表名：【{}】".format(sqlStr, depend_table_name))
            list_depend_table.append(depend_table_name)




    write_log("[解析表模块]共有{}个表被查询到！".format(len(list_depend_table)))

    # 剔除递归临时表名
    print(1,list_depend_table)
    print(2,list_recursive_table)
    _table = [item for item in list_depend_table if item not in list_recursive_table]

    list_result_table = removal_duplicate_and_sort(filter_list(_table))

    write_log(f"[解析表模块] 通过去重排序操作，共有{len(list_result_table)}个表符合条件！")

    data = dict()
    if is_choice3:
        data = backup_sql('\n'.join(list_result_table))
    else:
        if list_result_table:
            data['formatted_id'] = '\n'.join(list_result_table) # 表清单
            data['element_cnt'] = len(list_result_table) # 计算表的个数
    # 返回一个字典 
    return data



    # return list_depend_table


# 读取sql脚本的内容
def read_txt(file_name):
    if not os.path.isfile(file_name):
        print(file_name, '文件不存在')
        sys.exit(8)

    with open(file_name, 'r', encoding='utf-8') as file:
        all_sql_content = file.read()
        # print(all_sql_content)


    return all_sql_content



def removal_duplicate_and_sort(list_name):
    if list_name:
        result = list(set(list_name))
        result.sort()
        # print(66666666, result)
    else:
        print("空")
        # sys.exit(6)
        result = None
    return result


def filter_list(list_name):
    new_list = list()
    for full_table_name in list_name:

        # print(full_table_name)
        # 剔除252库中的临时表
        if full_table_name.startswith('#') :
           continue

        # 剔除子查询匹配不准确的情况
        # select * from (select * from a) t1
        if full_table_name.startswith('('):
           continue
        
        # 剔除@开头的表
        if full_table_name.startswith('@'):
            continue

        # 去掉[和]
        full_table_name = full_table_name.replace('[','').replace(']','')


        # 表名只有一个.；替换数仓的库名
        if full_table_name.count('.') == 1:
            (database_name, table_name) = full_table_name.split('.')
            # if database_name == 'test':
            #     database_name = 'Test'
            # elif 'shwl' in database_name:
            #     database_name = database_name.upper()
            # else:
            #     pass
            full_table_name = database_name + '.' + table_name

        new_list.append(full_table_name)
    return new_list





if __name__ == '__main__':
    # file_name = r'D:\work\20-库存\t_odoostock.sql'
    # while True:
    #     file_name = input("请输入sql脚本的位置: ")
    #     if file_name:
    #         break
    #     else:
    #         print("输入不正确!")

    # result = read_txt(file_name)
    # result = get_sql_table_name(result)
    # result = filter_list(result)
    # result = removal_duplicate_and_sort(result)

    # print('-----------------------------------list of table--------------------------------------')
    # print('----- 共%s张表-----' % str(len(result)))
    # for i in result:
    #     print(i)

    #print(result)
    sqlResult = """
drop procedure if exists PRO_DASHBOARD_OVERVIEW;
CREATE PROCEDURE `PRO_DASHBOARD_OVERVIEW`(IN BIZ_DATE DATE)
label:BEGIN
/************************************************************************************************************************************************
         项目名称：   人力绩效-墨方自研
         用户名：     HRJX
         过程名称:    PRO_DASHBOARD_OVERVIEW
         功能简述：   看板数据/单独执行的存储过程
         参数：       BIZ_DATE  数据日期(YYYY-MM-DD)
         注意事项：   
         数据源：
         目标表：
         修改记录:
         --------------------------------------------------------------------------------------------------------------
         操作人      操作时间                   操作
         MG         2024/05/07                  改为综合执行存储过程的存储过程
************************************************************************************************************************************************/
-- 变量定义
DECLARE V_TABLE_CNT int;
DECLARE V_START_TIME datetime;
DECLARE V_EVENT_NAME varchar(200);
DECLARE V_RUN_COMMAND varchar(1000);

-- 设置变量
set V_EVENT_NAME = '综合执行存储过程';
select now() into V_START_TIME;



set V_RUN_COMMAND = concat('call PRO_DASHBOARD_OVERVIEW('
                            ,BIZ_DATE
                            ,')'
                         );

/************************ 可单独执行的存储过程 ******************************/
-- 全员绩效档案
call PRO_SINGLE_ARCHIVES(current_date);

-- 全员考核结果-年度综合考评结果
call PRO_SINGLE_ANNUAL_ASSESS_RESULT(current_date);

-- 看板基础数据
call PRO_SINGLE_DASHBOARD_BASE(current_date);


-- 获得记录数(不需要记录)
select 0 into V_TABLE_CNT;

-- 写日志
call PRO_WRITELOG_HRJX(V_EVENT_NAME,V_RUN_COMMAND,current_date,0
,concat('执行成功[记录:',V_TABLE_CNT,'],用时',timestampdiff(second,V_START_TIME,now()),'秒')
,0,0)
;END
;

"""
    print(sqlResult)
    get_sql_table_name(sqlResult)