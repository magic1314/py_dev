# encoding:utf-8
import sys
import os
import re
import time
from public import run_sql
from public import test
'''
# 功能介绍
  本程序生成全删全插的格式化sql语句，需要提供目标表
  生成的数据格式为：
  truncate table xxx;
  insert into xxx(x,x,x,x)
  select 
      a.字段1 别名1 -- 备注1
     ,a.字段2 别名2 -- 备注2
     ...
     ,a.字段n 别名n -- 备注n
  from xxx a
  ;
# 处理逻辑
  目标表的字段是从数据库的数据字典中获取的,
  其中自增字段不处理,remark字段默认为null,创建和更新字段默认为now()
# 更新日志
2024-06-19 数据库改为开发库,目标表写成"[表名]"
'''


# 添加逗号
# a,bc,c,d
def add_comma(sql,max_len=110):
    sql_list = sql.split(',')
    pos = 0
    new_element = list()
    # print(sql_list)
    for element in sql_list:
        # 长度
        pos = pos + len(element) + 1
        # print(pos)
        if pos >= max_len:
            element = f"{element}\n    "
            pos = 0

        new_element.append(element)
    
    return ','.join(new_element)



# 生成insert插入语句
def m_generate_insert_sql(table_name):
    """
        content：表名
    
    """
    sql_txt = """
select column_name,column_comment from information_schema.columns 
where table_name = '{}'
and not ( COLUMN_KEY = 'PRI' and EXTRA = 'auto_increment' )
and not (
        (column_name like '%create_time' or column_name like '%update_time')
        and column_default = 'CURRENT_TIMESTAMP'
        and extra like 'DEFAULT_GENERATED%'
    )
order by ORDINAL_POSITION
""".format(table_name)
    sql_result = run_sql(sql_txt,dbFile='hrjx_develop.txt',needResult=True,executemany=False,parameter_list=None,group_concat_max_len=0,display=True)
    # print(sql_result)

    if not sql_result:
        return sql_result

    # 转成列表
    column_name = list(item[0] for item in sql_result)
    # print(column_name)

    # 列表中元素最大值，默认最小值为15
    list_max_value = max(max([len(i) for i in column_name])+8,20)

    # 字段列表
    column_name_list = ','.join(column_name)
    # test(column_name_list)

    # 备注信息
    column_comment = list(item[1] for item in sql_result)
    # print(column_comment)

    column_cnt = len(column_name)
    mid = list()
    for i in range(column_cnt):
        if column_name[i] == "remark":
            field_name = 'null'
        elif column_name[i] in ('create_time','update_time'):
            field_name = 'now()'
        elif column_name[i] in ('creator','creater'):
            field_name = '1'
        else:
            field_name = "a.{}".format(column_name[i])

        line = "{0}{1}{2}{3}-- {4}".format( \
                field_name \
                , " " * (list_max_value-len(field_name)) \
                , column_name[i] \
                , " " * (list_max_value-len(column_name[i])) \
                , column_comment[i] \
                )

        if i == column_cnt:
            mid.append("\n    ,{0}".format(line))
        elif i:
            mid.append("\n    ,{0}".format(line))
        else:
            mid.append("     {0}".format(line))

    mid_list = ''.join(mid)

    insert_header_sql = f"insert into {table_name}({column_name_list})"

    insert_header_sql = add_comma(insert_header_sql,109)
    # print(666666, insert_header_sql)

    result = """truncate table {0};
{1}
select 
{2}
from [表名] a
;""".format(table_name,insert_header_sql,mid_list)
    
    print(result)

    data = dict()
    data['result'] = result
    data['element_cnt'] = len(result)
    return data



if __name__ == '__main__':
    # str = 'insert into t_appraisal_member(dept_id,dept_name,user_name,user_code,user_id,job,user_status,register_date,cancel_date,create_time,creator,updater,remark1,remark2,deleted)'
    # a = add_comma(str)
    # print(a)
    m_generate_insert_sql('sys_user')
