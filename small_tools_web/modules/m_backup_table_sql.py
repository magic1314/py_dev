# encoding:utf-8
import sys
import os
import re
import time

# 去掉空白格
def formated(s):
    # 使用正则表达式去掉字符串中的所有空白符号
    s_stripped = re.sub(r'[\t ]', '', s)

    # 输出去掉空白符号后的字符串
    # print(s_stripped)  # 输出：helloworld
    return s_stripped

def backup_sql(sql):
    """
    sql:需要处理的内容 
    
    """
    # print(sql)
    yyyymmdd = time.strftime("%Y%m%d", time.localtime())
    data = dict()
    result = sql.replace('\t',' ').replace('\r', ' ')
    result = re.sub(' +', '\n', result)
    result = re.sub('\n+', '\n', result)
    result = result.strip('\n')

    table_list = result.split('\n')

    # print(6666, table_list, type(table_list))
    max_len = max(len(x) for x in table_list)
    # print("最大长度：", max_len)

    result_sql = list()
    result_sql.append("-- 表备份")
    for table_name in table_list:
        remain_space = (max_len - len(table_name)) * ' '
        # print("预留长度：", len(remain_space))
        # create table if not exists backup_20230721_t_appraisal_dept      as select * from t_appraisal_dept     ;
        sql = 'create table if not exists backup_{0}_{1} {2}as select * from {1}{2};'.format(yyyymmdd, table_name, remain_space)
        result_sql.append(sql)
    
    data['formatted_id'] = '\n'.join(result_sql)
    data['element_cnt'] = len(table_list)
    return data


if __name__ == '__main__':
    str = '''
t_appraisal_dept
t_appraisal_member_2
t_appraisal_dept_magr sys_user
t_appraisal_member
sys_dept
t_employee
    '''


    r = backup_sql(str)
    print(r['formatted_id'])
