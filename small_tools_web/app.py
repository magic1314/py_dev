import sys
import os
from datetime import datetime
from flask import Flask, request, jsonify, redirect, url_for, abort
from flask import render_template # jinjia2模板引擎
from flask import send_from_directory
import typing

# from flask_restful import Resource, Api
# from flask_sqlalchemy import SQLAlchemy
from modules.m_get_table import *
from modules.m_format_multiline_id import *
from modules.m_backup_table_sql import *
from modules.m_generate_insert_sql import *
from modules.m_search_keyword import *
from public import write_log
from public import test


# if sys.platform == 'linux':
#     import subprocess

"""
# 程序名：small tools(小工具web)
# 功能：
    # 功能1：解析sql中表名
    # 详细说明：通过用户传入的sql语句(不限制长度和语句个数)，解析sql中包含的所有表(不支持笛卡尔积的关联解析)
            最后将解析结果的表名返回到页面；如果解析错误，输入内容错误，程序会有提醒
    # 功能2：格式化多行id值
    # 详细说明：将非规范化的字符串(数字、多字符 比如很多用户值，id值等)，规范化输出
# 运行环境：
    * 目前用linux运行，之前windows10是可以运行的
# 目录：
    * /home/dev/python
    * 对目录没有要求
# 运行命令：
    * nohup python /home/dev/python/small_tools_web/app.py >> /home/dev/python/log/small_tools_web.log 2>&1 &
# 注意事项：
   ................................................................
   更新日志放在【README.md】文件中
"""

# print("退出！")
# sys.exit(0)

app = Flask(__name__)

# 根目录
@app.route('/')
def index():
    return render_template('index.html')


# 解析出SQL中的表
@app.route('/get_table', methods=['GET', 'POST'])
def get_table():
    data = dict()

    if request.method == 'GET':
        data['input'] = ''
        data['result'] = ''
        return render_template('template_get_table.html')
    
    elif request.method == 'POST':
        write_log('服务器获得html的post请求')
        # input_content = request.form['input_content']  # 获取前台输入的内容
        write_log("后端路由app.py接受到的内容：【{}】".format(request.json))
        input_content = request.json['input_content']
        # print(55555555555, request.json)
        my_choice = request.json['my_choice']
        # print(6666661111, my_choice)
        if my_choice:
            is_choice1 = my_choice.get('checkbox1')[-1]
            is_choice2 = my_choice.get('checkbox2')[-1]
            is_choice3 = my_choice.get('checkbox3')[-1]
            # print(7777771222, checkbox1, type(checkbox1))

        data = dict()
        if input_content:
            # 调用后台脚本执行操作，获取结果
            data = get_sql_table_name(input_content, is_choice1, is_choice2, is_choice3)  
            data['input'] = input_content
            if not data:
                data['formatted_id'] = '[001]解析有问题!'
            # print(66666, data)
        else:
            if not input_content:
                data['formatted_id'] = '[000]输入为空，请重新输入'
            else:
                data['formatted_id'] = '[002]解析有问题!'
    
        write_log("data={}".format(data))
        # 返回数据给js脚本，数据也是字典
        return jsonify(data)


# 格式化id
@app.route('/format_multiline_id', methods=['GET', 'POST'])
def format_multiline_id():
    if request.method == 'GET':
        return render_template('template_format_multiline_id.html')
    
    elif request.method == 'POST':
        write_log('服务器获得html的post请求')
        input_content = request.json['input_content']
        my_choice = request.json.get('my_choice')
        write_log("用户输入内容{}，选择按钮：{}".format(input_content,my_choice))

        ch_1 = my_choice['checkbox1'][1]
        ch_2 = my_choice['checkbox2'][1]
        ch_3_tmp = my_choice['checkbox3'][1]
        ch_4 = my_choice['checkbox4'][1]
        if ch_3_tmp:
            ch_3 = 2
        else:
            ch_3 = 0

        data = dict()
        if input_content:
            data = format_id(input_content, is_one_line=ch_1, is_quotation_mark=ch_2, bracket_type=ch_3, duplicate_removal=ch_4)  # 调用后台脚本执行操作，获取结果
            data['input'] = input_content
            if not data:
                data['formatted_id'] = '[001]解析有问题!'
            # print(66666, data)
        else:
            if not input_content:
                data['formatted_id'] = '[000]输入为空，请重新输入'
            else:
                data['formatted_id'] = '[002]解析有问题!'
    
        write_log("data={}".format(data))

        # deal_class(my_choice)
        # response_data = {'result': 'success', 'data': '[134,151,151]'}
        return jsonify(data)
        # return render_template('template_format_multiline_id.html', data=data)  # 将结果写在同一页面的下方


# 备份表
@app.route('/backup_table_sql', methods=['GET', 'POST'])
def backup_table_sql():
    if request.method == 'GET':
        return render_template('template_backup_table_sql.html')
    
    elif request.method == 'POST':
        write_log('服务器获得html的post请求')
        input_content = request.json['input_content']
        # my_choice = request.json.get('my_choice')
        print(6666, request.json)
        write_log("用户输入内容：{}".format(input_content))


        data = dict()
        if input_content:
            data = backup_sql(input_content)  # 调用后台脚本执行操作，获取结果
            data['input'] = input_content
            if not data:
                data['formatted_id'] = '[001]解析有问题!'
            # print(66666, data)
        else:
            if not input_content:
                data['formatted_id'] = '[000]输入为空，请重新输入'
            else:
                data['formatted_id'] = '[002]解析有问题!'
    
        write_log("data={}".format(data))

        # deal_class(my_choice)
        # response_data = {'result': 'success', 'data': '[134,151,151]'}
        return jsonify(data)
        # return render_template('format_multiline_id.html', data=data)  # 将结果写在同一页面的下方


# 生成插入sql语句
@app.route('/generate_insert_sql', methods=['GET', 'POST'])
def generate_insert_sql():
    write_log('开始执行app.py中generate_insert_sql函数 request_method={}'.format(request.method))
    if request.method == 'GET':
        return render_template('template_generate_insert_sql.html')
    
    elif request.method == 'POST':
        write_log('服务器获得html的post请求, post的内容为：{}'.format(request.json))
        # test(request)
        # test(request.json)
        # request.json返回的是一个字典
        input_content = request.json['input_content']
        write_log("用户输入内容：【{}】".format(input_content))

        data = dict()
        if input_content:
            # 字典里有【result】键
            data = m_generate_insert_sql(input_content)  # 调用后台脚本执行操作，获取结果
        else:
            data['result'] = '[000]输入为空，请重新输入'
    
        # write_log("data={}".format(data))

        # deal_class(my_choice)
        # response_data = {'result': 'success', 'data': '[134,151,151]'}
        # test(data)
        # test(jsonify(data))
        # jsonify函数将该字典转换为JSON格式，设置响应的Content-Type为application/json，并作为响应返回给客户端
        return jsonify(data)
        # return render_template('template_format_multiline_id.html', data=data)  # 将结果写在同一页面的下方


# 查找关键字
@app.route('/search_keyword', methods=['GET', 'POST'])
def search_keyword():
    data = dict()

    if request.method == 'GET':
        data['input'] = ''
        data['result'] = ''
        return render_template('template_search_keyword.html')
    
    elif request.method == 'POST':
        write_log('服务器获得html的post请求')
        write_log("后端路由app.服务器获得请求数据{}】".format(request.json))
        search_path = request.json['search_path']
        search_file_name = request.json['search_file_name']
        search_content = request.json['search_content']

        # 调用后台脚本执行操作，获取结果
        deal_data = m_search_keyword(directory=search_path,target_string=search_content,file_filter_condition=search_file_name)
        # write_log(f"后端计算完成后，需发送给前端的数据：{deal_data}")
        # 返回数据给js脚本，数据也是字典
        return jsonify(deal_data)


# 处理数据
@app.route('/search_keyword/deal_file', methods=['POST'])
def deal_file():
    # print("后端获得json数据：", request.json)
    # 处理数据
    m_search_keyword_deal_data(request.json)
    return jsonify({'status': 200})


# 展示数据
@app.route('/search_keyword/show_file', methods=['GET'])
def show_file():
    return render_template('template_search_file_data.html')


# 解决在路由地址后面带/的问题
@app.errorhandler(404)
def page_not_found(e):
    if request.path.endswith('/') and len(request.path) > 1:
        return redirect(request.path[:-1]), 301
    elif not request.path.endswith('/') and len(request.path) > 1:
        return redirect(request.path + '/'), 301
    return render_template('404.html'), 404


##################### 下面均是测试的数据 #####################

# 提交表单
@app.route('/submit_form', methods=['POST'])
def submit_form():
    text_input = request.form.get('text_input')
    radio_option = request.form.get('radio_group')
    checkboxes = request.form.getlist('checkboxes')

    # 这里可以进行进一步的数据处理或存储
    print(f'Text Input: {text_input}')
    print(f'Radio Option: {radio_option}')
    print(f'Checkboxes: {checkboxes}')

    return 'Form submitted successfully!'

# 存在问题，不能跨平台
# html调用：<a href="{{ url_for('copy') }}">复制</a>
# @app.route('/copy')
# def copy():
#     if sys.platform == 'linux':
#         content = '内容12345'
#         p = subprocess.Popen(['xclip', '-selection', 'clipboard'], stdin=subprocess.PIPE)
#         write_log(str(p))
#         p.stdin.write(content.encode('utf-8'))
#         write_log("已写入！")
#         p.stdin.close()
#         # return '已将内容复制到粘贴板'
#         return jsonify(None)
#     else:
#         return None




def get_data():
    # 示例数据，你可以替换成实际的数据源
    data = [
        {'id': 1, 'name': 'John Doe', 'age': 30},
        {'id': 2, 'name': 'Jane Doe', 'age': 28},
        {'id': 3, 'name': 'Alice Smith', 'age': 25}
    ]
    # return jsonify(data)
    # return jsonify(data)
    return data



# jiaja2引擎，可以创建动态页面
# GET
@app.route('/test2', methods=['POST', 'GET'])
def index_test2():
    if request.method == 'POST':
        # 假设你已经定义了get_data函数，这里调用它并获取数据
        request_data = get_data()  # 这里应该是get_data()的实际调用，返回的是json数据
        response_data = request.json
        print(f"------------- request_data={request_data}")
        print(f"------------- response_data={response_data}")
        return render_template('index_test2.html', data=response_data)
    elif request.method == 'GET':
        return render_template('index_test2.html')





# 重定向，url_for带函数名，会跳转到对应的路由链接上
@app.route('/greet', methods=['POST'])
def greet():
    name = request.form['name']
    # return 'Hello, {}!'.format(name)
    # print(6666666, url_for('index'))
    return redirect(url_for('index'))


@app.route('/welcome', methods=['POST', 'GET'])
def show():
    return jsonify([
        {'name': 'xiaoming', 'age': 28}
        ,{'name': 'xiaohong', 'age': 21}]
    )


@app.route('/test')
def test():
    print("-------------- /test 路由--------------------------------")
    return render_template('test.html')

# jsonify库实现：返回json数据给前端
@app.route("/test_json")
def test_json():
   data = {
       'name':'张三'
   }
   print(5555, type(jsonify(data)), jsonify(data))
   abort(402)
   return jsonify(data)

# Flask高级视图
def my_test():
    return '这是测试页面'
app.add_url_rule(rule='/test_gj',endpoint='test_gj',view_func=my_test)


# 给前端模板传参
@app.route("/test1")
def test1():
    title='python键值对'   # 定义键值1
    author='li'      # 定义键值2
    name='2'
    return render_template('index_test2.html',**locals()) # 渲染模型并传值


# --------------视图函数------------------
@app.route('/login2')
def login2():
    # return render_template("index_test2.html")
    print(555, request.method)
    if request.method == 'GET':
        return render_template("flash.html")
        # return render_template("index_test2.html")
    else:
        username = request.form.get('username')
        password = request.form.get('password')
        # user = User.query.filter(User.username == username, User.password == password).first()
        user = User.query.filter(User.username == username).first()
        if user and user.check_password(password):
            session['user_id'] = user.id
            session['user_name'] = user.username
            session.permanent = True
            print(6666666666, url_for("index"))
            return redirect(url_for("index"))
        else:
            flash('用户名或密码不正确,请检查!')
            return render_template('flash.html')



if __name__ == '__main__':
    # 当前网站域名设置为example.com，端口号为5000
    # app.config['SERVER_NAME'] = 'smalltools.com:5000'
    app.run(port=5000,debug=True,host='0.0.0.0')

    
    # deal_class([1,2432,4])